﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class VerbruikPage
    Inherits System.Web.UI.Page
    Protected Sub vertaal(taalid)
        If taalid = 1 Then
            Literal2.Text = "Registreer verbruik"

            ASPxButton1.Text = "Verbruik doorsturen"
            ASPxGridView1.Columns("Article").Caption = "Artikel"
            ASPxGridView1.Columns("Description").Caption = "Omschrijving"
        Else
            Literal2.Text = "Passer un commande"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        ASPxLabelSuccess.Text = Session("success")
        Session("success") = ""
        Dim voocontext As New VooEntities
        If Not Page.IsCallback Then


            If Not Session("isingelogd") Then
                Session("url") = Request.Url.AbsoluteUri
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If

            Dim mag As Magazijn
            Dim magid As Integer
            magid = ASPxComboBoxMagazijn.ClientVisible = True
            ASPxComboBoxMagazijn.ClientVisible = True

            Dim userid As Integer = Session("userid")
            Dim opdrachtgeverid As Integer = Session("opdrachtgever")
            Dim opdrachtgever = voocontext.Opdrachtgever.Find(opdrachtgeverid)
            If voocontext.Verbruik.Where(Function(x) x.gebruiker = userid And x.status = 1).Any Then
                'open verbruik aanwezig, gebruik deze en verwijder 0waarden, Plus voeg alles toe

                Session("verbruikId") = voocontext.Verbruik.Where(Function(x) x.gebruiker = userid And x.status = 1).FirstOrDefault.id
                Dim verbid As Integer = Session("verbruikId")
                'Dim verbruiken0 = voocontext.Verbruiklijn.Where(Function(x) x.verbruikId = verbid And x.hoeveelheid = 0)
                'Dim verbruikenniet0 = voocontext.Verbruiklijn.Where(Function(x) x.verbruikId = verbid And x.hoeveelheid <> 0)
                'Dim idlist As New List(Of Integer)
                'For Each verbr In verbruikenniet0
                '    idlist.Add(verbr.materiaalId)
                'Next
                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()
                    Dim qDeleteZero = "DELETE from voo.dbo.Verbruiklijn where VerbruikId = @verbruikId and hoeveelheid = 0"
                    Dim delete_cmd As New SqlCommand(qDeleteZero, cn)
                    Dim delete_par As New SqlParameter("@verbruikId", SqlDbType.Int, -1) With {.Value = verbid}
                    delete_cmd.Parameters.Add(delete_par)
                    delete_cmd.ExecuteNonQuery()

                    Dim qInsertNotUsedAlready = "Insert into voo.dbo.Verbruiklijn (materiaalId, VerbruikId, Hoeveelheid, verwerkt, prijs) select id, @verbruikId, 0, 0, prijs from Voo.dbo.basismateriaal where actief = 1 and id not in (select id from voo.dbo.Verbruiklijn where VerbruikId = @verbruikId) and id in (select materiaalId from voo.dbo.MateriaalOpdrachtgevers where opdrachtgeverId = @opdrachtgever  )"
                    Dim insert_cmd As New SqlCommand(qInsertNotUsedAlready, cn)
                    Dim insert_par As New SqlParameter("@verbruikId", SqlDbType.Int, -1) With {.Value = verbid}
                    insert_cmd.Parameters.Add(insert_par)
                    insert_par = New SqlParameter("@opdrachtgever", SqlDbType.Int, -1) With {.Value = opdrachtgeverid}
                    insert_cmd.Parameters.Add(insert_par)
                    insert_cmd.ExecuteNonQuery()
                End Using

                'voocontext.Verbruiklijn.RemoveRange(verbruiken0)
                'voocontext.SaveChanges()


                'Dim basismat = voocontext.Basismateriaal.Where(Function(x) x.MateriaalOpdrachtgevers.Where(Function(y) y.opdrachtgeverId = opdrachtgeverid).Any And x.actief = True And x.bobijnArtikel = False).ToList
                'Dim listverbrlijn As New List(Of Verbruiklijn)
                'For Each mat In basismat.Where(Function(x) idlist.Contains(x.id) = False)

                '    'If verbruikenniet0.Where(Function(x) x.materiaalId = stockmag.MateriaalId).Any = False Then
                '    Dim lijn As New Verbruiklijn
                '    lijn.verbruikId = verbid
                '    lijn.hoeveelheid = 0
                '    lijn.materiaalId = mat.id
                '    lijn.prijs = mat.prijs
                '    listverbrlijn.Add(lijn)
                '    ' End If
                '    'actief materiaal, stocktellinglijn toevoegen
                'Next
                'voocontext.Verbruiklijn.AddRange(listverbrlijn)
            Else
                Dim verbruik As New Verbruik
                verbruik.gebruiker = userid
                verbruik.status = 1
                verbruik.ordernummer = ASPxTextBoxOrdernummer.Value
                verbruik.datum = DateTime.Now
                voocontext.Verbruik.Add(verbruik)
                voocontext.SaveChanges()
                Session("verbruikId") = verbruik.id


                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()
                    Dim qInsertNotUsedAlready = "Insert into voo.dbo.Verbruiklijn (materiaalId, VerbruikId, Hoeveelheid, verwerkt, prijs) select id, @verbruikId, 0, 0, prijs from Voo.dbo.basismateriaal where actief = 1  and id in (select materiaalId from voo.dbo.MateriaalOpdrachtgevers where opdrachtgeverId = @opdrachtgever  )"
                    Dim insert_cmd As New SqlCommand(qInsertNotUsedAlready, cn)
                    Dim insert_par As New SqlParameter("@verbruikId", SqlDbType.Int, -1) With {.Value = verbruik.id}
                    insert_cmd.Parameters.Add(insert_par)
                    insert_par = New SqlParameter("@opdrachtgever", SqlDbType.Int, -1) With {.Value = opdrachtgeverid}
                    insert_cmd.Parameters.Add(insert_par)
                    insert_cmd.ExecuteNonQuery()
                End Using

                'For Each stockmag In voocontext.Magazijn.Where(Function(x) x.OpdrachtgeverId = opdrachtgeverid And x.Hoofd = True).FirstOrDefault.StockMagazijn
                '    If stockmag.Basismateriaal.actief = True And Not stockmag.Basismateriaal.bobijnArtikel Then
                '        'actief materiaal, stocktellinglijn toevoegen
                '        Dim lijn As New Verbruiklijn
                '        lijn.verbruikId = verbruik.id
                '        lijn.hoeveelheid = 0
                '        lijn.materiaalId = stockmag.MateriaalId
                '        lijn.prijs = voocontext.Basismateriaal.Find(stockmag.MateriaalId).prijs
                '        voocontext.Verbruiklijn.Add(lijn)
                '    End If
                'Next
            End If
            voocontext.SaveChanges()




            If Session("isadmin") Then
                If opdrachtgever.tag = "SYN" Then
                    ASPxComboBoxMagazijn.DataSourceID = "SqlDataSourceMagazijnenSYNadmin"
                    ASPxComboBoxMagazijn.DataBind()
                    ASPxComboBoxMagazijnNaar.DataSourceID = "SqlDataSourceMagazijnenVerbruikSYN"
                    ASPxComboBoxMagazijnNaar.DataBind()

                Else
                    ASPxComboBoxMagazijn.DataSourceID = "SqlDataSourceMagazijnen0"
                    ASPxComboBoxMagazijn.DataBind()
                End If

            Else

                If opdrachtgever.tag = "SYN" Then
                    ASPxComboBoxMagazijn.DataSourceID = "SqlDataSourceMagazijnenSYNtech"
                    ASPxComboBoxMagazijn.DataBind()
                    ASPxComboBoxMagazijnNaar.DataSourceID = "SqlDataSourceMagazijnenVerbruikSYN"
                    ASPxComboBoxMagazijnNaar.DataBind()
                Else
                    ASPxComboBoxMagazijn.DataSourceID = "SqlDataSourceMagazijnen"
                    ASPxComboBoxMagazijn.DataBind()
                End If

            End If


            Dim verbruikreader = voocontext.Verbruik.Find(Convert.ToInt32(Session("verbruikId")))
            ASPxTextboxStad.Value = verbruikreader.stad
            ASPxTextboxStraat.Value = verbruikreader.straat



            If Not verbruikreader.Magazijn Is Nothing Then


                ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.Where(Function(x) x.Value = verbruikreader.Magazijn.id).FirstOrDefault
                Session("MagazijnId") = ASPxComboBoxMagazijn.Value

            Else

            End If

            If Not verbruikreader.Magazijn1 Is Nothing Then
                ASPxComboBoxMagazijnNaar.SelectedItem = ASPxComboBoxMagazijnNaar.Items.Where(Function(x) x.Value = verbruikreader.Magazijn1.id).FirstOrDefault
            End If


            ASPxGridView1.DataSourceID = "SqlDataSourceMateriauxADMIN"
            ASPxGridView1.DataBind()
        End If





        If ASPxComboBoxMagazijn.Items.Count() = 1 Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items(0)
        End If
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
        If ASPxComboBoxMagazijnNaar.Items.Count() = 1 Then
            ASPxComboBoxMagazijnNaar.SelectedItem = ASPxComboBoxMagazijnNaar.Items(0)
        End If
        Dim opId As Integer = Session("opdrachtgever")
        If voocontext.Opdrachtgever.Find(opId).naam <> "Fluvius - Moffen" Then
            ASPxLabel1.ClientVisible = False

            ASPxMemo1.ClientVisible = False
            ASPxLabel2.ClientVisible = False
        End If


    End Sub

    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender


    End Sub

    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        'Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
        '    cn.Open()
        '    Dim editingRowVisibleIndex As Integer = ASPxGridView1.FindVisibleIndexByKeyValue(e.Keys.Values(0))
        '    Dim context As New VooEntities
        '    Try
        '        Dim check_sql As String = "select id from [Stocktellinglijn] where materiaalId= @materiaalId and [stocktellingId] = @StockTellingId"
        '        Dim check_cmd As New SqlCommand(check_sql, cn)
        '        Dim check_par As New SqlParameter("@StockTellingId", SqlDbType.NVarChar, -1) With {.Value = Session("StockTellingId")}
        '        check_cmd.Parameters.Add(check_par)
        '        check_par = New SqlParameter("@materiaalId", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "materiaalId").ToString()}
        '        check_cmd.Parameters.Add(check_par)
        '        Dim dr As SqlDataReader = check_cmd.ExecuteReader()
        '        If dr.HasRows Then
        '            dr.Read()
        '            If Not dr.IsDBNull(0) Then
        '                'update

        '                Dim id As Decimal = dr.GetInt32(0)
        '                dr.Close()
        '                Dim s_SQL As String = "UPDATE Bestellijn set hoeveelheid = hoeveelheid + @aantal where id = @id"

        '                Dim cmd As New SqlCommand(s_SQL, cn)
        '                Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = id}
        '                cmd.Parameters.Add(par)
        '                par = New SqlParameter("@aantal", SqlDbType.Decimal) With {.Value = e.NewValues(0)}
        '                cmd.Parameters.Add(par)
        '                If e.NewValues(0) > 0 Then
        '                    cmd.ExecuteNonQuery()
        '                End If
        '            End If

        '        Else
        '            dr.Close()
        '            Dim s_SQL As String = "Insert into Bestellijn (materiaalId, bestellingId, hoeveelheid ) VALUES (@materiaalid, @bestellingId, @hoeveelheid)"

        '            Dim cmd As New SqlCommand(s_SQL, cn)
        '            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = Session("BestellingId")}
        '            cmd.Parameters.Add(par)
        '            par = New SqlParameter("@materiaalId", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Id").ToString()}
        '            cmd.Parameters.Add(par)
        '            par = New SqlParameter("@hoeveelheid", SqlDbType.Decimal) With {.Value = e.NewValues(0)}
        '            cmd.Parameters.Add(par)
        '            If e.NewValues(0) > 0 Then
        '                cmd.ExecuteNonQuery()
        '            End If
        '        End If



        '    Catch ex As Exception
        '        Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
        '        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
        '        cmd.Parameters.Add(par)
        '        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij update post aan postengroep door {0} {1} {2}", Session("naam"), Session("voornaam"), ex.Message)}
        '        cmd.Parameters.Add(par)
        '        cmd.ExecuteNonQuery()
        '    End Try





        'End Using

    End Sub


    Protected Sub ASPxGridView1_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles ASPxGridView1.BatchUpdate
        'Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
        '    cn.Open()

        '    Dim tempId As New Integer
        '    Try
        '        Dim check_sql As String = "select id from Bestellingen where GebruikerId= @gebruikerId and Status = 1"
        '        Dim check_cmd As New SqlCommand(check_sql, cn)
        '        Dim check_par As New SqlParameter
        '        If Not Session("isadmin") Then
        '            check_par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = Session("userid")}

        '        Else
        '            check_par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
        '        End If
        '        check_cmd.Parameters.Add(check_par)
        '        Dim dr As SqlDataReader = check_cmd.ExecuteReader()
        '        If dr.HasRows Then
        '            dr.Read()
        '            If Not dr.IsDBNull(0) Then
        '                tempId = dr.GetInt32(0)
        '            End If
        '            dr.Close()
        '        Else
        '            dr.Close()
        '            Dim s_SQL As String = "Insert into Bestellingen (Datum, GebruikerId, Status) VALUES (GetDate(), @gebruikerId, 1); SELECT SCOPE_IDENTITY()"

        '            Dim cmd As New SqlCommand(s_SQL, cn)
        '            Dim par As New SqlParameter
        '            If Not Session("isadmin") Then
        '                par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = Session("userid")}

        '            Else
        '                par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
        '            End If
        '            cmd.Parameters.Add(par)
        '            tempId = cmd.ExecuteScalar()

        '        End If





        '        Session("BestellingId") = tempId
        '    Catch ex As Exception
        '        Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
        '        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
        '        cmd.Parameters.Add(par)
        '        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij update post aan postengroep door {0} {1} {2}", Session("naam"), Session("voornaam"), ex.Message)}
        '        cmd.Parameters.Add(par)
        '        cmd.ExecuteNonQuery()
        '    End Try




        'End Using
    End Sub



    Protected Sub ASPxGridView1_AfterPerformCallback(sender As Object, e As ASPxGridViewAfterPerformCallbackEventArgs) Handles ASPxGridView1.AfterPerformCallback
        ' MAIL VERZENDEN
        'Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
        '    cn.Open()
        '    Dim s_SQL As String = ""
        '    Dim cmd As SqlCommand = Nothing
        '    Dim par As SqlParameter = Nothing
        '    Dim varBericht As String = ""
        '    Dim varOnderwerp As String = ""
        '    Dim varBeheerder As String = ""
        '    Dim varEmail As String = ""
        '    s_SQL = "SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)"
        '    cmd = New SqlCommand(s_SQL, cn)
        '    par = New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Melding bestelling aangevraagd"}
        '    cmd.Parameters.Add(par)
        '    Dim dr As SqlDataReader = cmd.ExecuteReader
        '    If dr.HasRows Then
        '        dr.Read()
        '        If Not dr.IsDBNull(0) Then
        '            varBericht = dr.GetString(0)
        '            varOnderwerp = dr.GetString(1)
        '        End If
        '    End If
        '    dr.Close()
        '    s_SQL = "SELECT vwWerkn.WERKNEMER, Gebruikers_1.Email FROM Gebruikers INNER JOIN Gebruikers Gebruikers_1 ON Gebruikers.VerlofGoedkeuring=Gebruikers_1.id " _
        '        & "INNER JOIN (SELECT NAAM + ' ' + VNAAM as WERKNEMER, [ON],NR FROM Elly_SQL.dbo.WERKN) vwWerkn ON Gebruikers_1.Werkg=vwWerkn.[ON] " _
        '        & "AND Gebruikers_1.Werkn=vwWerkn.NR WHERE (Gebruikers.id=@gebruiker)"
        '    cmd = New SqlCommand(s_SQL, cn)
        '    par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Session("userid")}
        '    cmd.Parameters.Add(par)
        '    dr = cmd.ExecuteReader
        '    If dr.HasRows Then
        '        dr.Read()
        '        varBeheerder = dr.GetString(0)
        '        varEmail = dr.GetString(1)
        '    End If
        '    dr.Close()
        '    Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2010_SP2)
        '    exch.Url = New Uri("https://mail.apkintern.be/EWS/Exchange.asmx")
        '    exch.UseDefaultCredentials = False
        '    exch.Credentials = New System.Net.NetworkCredential("Administrator", "E01ab107%", "apk")
        '    TrustAllCertificatePolicy.OverrideCertificateValidation()
        '    exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.PrincipalName, Session("mailaccount"))
        '    Dim message As New EmailMessage(exch)
        '    message.Subject = varOnderwerp
        '    Dim varBody As String = varBericht.Replace("[WERKNEMER]", String.Format("{0} {1}", Session("naam"), Session("voornaam")))
        '    varBody = varBody.Replace("[BEHEERDER]", varBeheerder)
        '    varBody = varBody.Replace("[URL]", String.Format("{0}/VerlofaanvragenVerwerken.aspx?g={1}", Session("domein"), Session("userid")))
        '    varBody = varBody.Replace("[SITETITEL]", Session("sitetitel"))
        '    Dim sBody As String = "<x-html>" & vbCrLf
        '    sBody = sBody & varBody
        '    sBody = sBody & "</x-html>"
        '    message.Body = sBody
        '    message.ToRecipients.Add(varEmail)
        '    message.Send()
        'End Using


        'If Session("BestellingId") <> 0 And Not Session("BestellingId") Is Nothing Then
        '    Dim tempid As String = Session("BestellingId")
        '    ASPxWebControl.RedirectOnCallback(VirtualPathUtility.ToAbsolute("~/BestellingCompleet.aspx?id=" + tempid))
        'End If



    End Sub

    Protected Sub ASPxGridView1_CellEditorInitialize(sender As Object, e As ASPxGridViewEditorEventArgs) Handles ASPxGridView1.CellEditorInitialize

    End Sub

    Protected Sub ASPxGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles ASPxGridView1.SelectionChanged

    End Sub

    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        If Not (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue() Is Nothing Then
            Session("serienummerlijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        End If

    End Sub

    Protected Sub ASPxGridView1_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBinding
        DoSelect(SqlDataSourceMateriauxADMIN.ConnectionString)
    End Sub

    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [verbruiklijnid] from [VerbruikSerienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("verbruiklijnid"))
        Next row
        Session("SelectResult") = result
    End Sub

    Protected Sub ASPxGridView1_DetailRowGetButtonVisibility(sender As Object, e As ASPxGridViewDetailRowButtonEventArgs) Handles ASPxGridView1.DetailRowGetButtonVisibility


        If Not ASPxGridView1.GetRowValuesByKeyValue(e.KeyValue, "HeeftSerienummer") Then
            e.ButtonState = GridViewDetailRowButtonState.Hidden
        End If


    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click, ASPxButton2.Click
        Dim voocontext As New VooEntities
        Dim id As Integer = Session("verbruikId")
        Dim stockt = voocontext.Verbruik.Find(id)
        Dim aantalniet0 As Integer = 0
        Dim fouten As New List(Of String)
        If ASPxComboBoxMagazijnNaar.Value Is Nothing Then
            fouten.Add("Geen doelmagazijn!")
        End If
        stockt.verbruiktop = Convert.ToInt32(ASPxComboBoxMagazijnNaar.Value)
        stockt.verbruiktvan = Convert.ToInt32(ASPxComboBoxMagazijn.Value)
        stockt.ordernummer = ASPxTextBoxOrdernummer.Value
        Try
            For Each verbruiklijn In stockt.Verbruiklijn.ToList
                If verbruiklijn.hoeveelheid <> 0 Then
                    aantalniet0 = aantalniet0 + 1
                    If voocontext.StockMagazijn.Where(Function(x) x.MateriaalId = verbruiklijn.materiaalId And x.MagazijnId = stockt.verbruiktvan).FirstOrDefault.Aantal - verbruiklijn.hoeveelheid < 0 Then
                        fouten.Add("Verbruik zou " & verbruiklijn.Basismateriaal.Description & " onder 0 brengen voor magazijn " & verbruiklijn.Verbruik.Magazijn.Naam)
                    End If
                    voocontext.StockMagazijn.Where(Function(x) x.MateriaalId = verbruiklijn.materiaalId And x.MagazijnId = stockt.verbruiktvan).FirstOrDefault.Aantal -= verbruiklijn.hoeveelheid
                        If voocontext.Basismateriaal.Find(verbruiklijn.materiaalId).HeeftSerienummer Then
                            If verbruiklijn.hoeveelheid <> Convert.ToDecimal(verbruiklijn.VerbruikSerienummer.ToList.Count) Then
                                fouten.Add("Aantal serienummers komt niet overeen met gebruik!")
                            End If
                        End If

                        If verbruiklijn.VerbruikSerienummer.Any Then
                            For Each sr In verbruiklijn.VerbruikSerienummer.ToList
                                If Not voocontext.Serienummer.Where(Function(x) x.serienummer1 = sr.serienummer).Any Then
                                    fouten.Add("Serienummer " & sr.serienummer & " bestaat niet")

                                Else
                                    Dim serienummer = voocontext.Serienummer.Where(Function(x) x.serienummer1 = sr.serienummer).FirstOrDefault
                                    If serienummer.StockMagazijn.MagazijnId = stockt.verbruiktvan Then
                                        serienummer.datumUsed = Today
                                        serienummer.statusId = 5
                                        serienummer.uitgeboekt = True
                                    Else
                                        fouten.Add("Serienummer " & serienummer.serienummer1 & " staat niet op het geselecteerde magazijn (" & stockt.Magazijn1.Naam & ") controleer en probeer opnieuw.")
                                    End If
                                End If



                            Next
                        End If
                    End If
            Next

        Catch ex As Exception
            fouten.Add("Fout bij registreren verbruik.")
        End Try

        If fouten.Count > 0 Then
            voocontext.Dispose()
            For Each fout In fouten
                Session("fout") = Session("fout") + fout + "<br/>"
            Next
            ASPxLabelFout.Text = Session("fout")
            Return

        End If
        If aantalniet0 > 0 Then


            For Each verbruiklijn In stockt.Verbruiklijn.ToList
                If verbruiklijn.hoeveelheid <> 0 Then
                    Dim stockmag = voocontext.StockMagazijn.Where(Function(x) x.MateriaalId = verbruiklijn.materiaalId And x.MagazijnId = stockt.verbruiktvan).FirstOrDefault
                    Dim sb As New Stockbeweging
                    sb.beweging = -verbruiklijn.hoeveelheid
                    sb.gebruiker = Session("userid")
                    sb.stockmagazijnId = stockmag.id
                    sb.opmerking = "Materiaal verbruikt op project " & stockt.Magazijn1.Naam
                    sb.datum = DateTime.Now()
                    voocontext.Stockbeweging.Add(sb)
                    voocontext.SaveChanges()
                    If verbruiklijn.VerbruikSerienummer.Any Then
                        For Each sr In verbruiklijn.VerbruikSerienummer.ToList
                            Dim serienummer = voocontext.Serienummer.Where(Function(x) x.serienummer1 = sr.serienummer).FirstOrDefault

                            Dim sbsr As New StockbewegingSerienummer
                            sbsr.serienummerId = serienummer.id
                            sbsr.stockbewegingid = sb.id
                            voocontext.StockbewegingSerienummer.Add(sbsr)

                        Next
                    End If
                End If
            Next


            stockt.status = 2
            stockt.datumdoorgestuurd = DateTime.Now
            Dim opId As Integer = Session("opdrachtgever")
            If Not ASPxDateEdit1.Value Is Nothing Then
                Try
                    stockt.datum = ASPxDateEdit1.Value
                Catch ex As Exception

                End Try

            End If

            If voocontext.Opdrachtgever.Find(opId).naam = "Fluvius - Moffen" Then

                stockt.opmerking = ASPxMemo1.Value
            End If
            voocontext.SaveChanges()
            Session("success") = Session("success") + "Verbruik op " & stockt.Magazijn1.Naam & "succesvol geregistreerd" + "<br/>"
            ASPxLabelSuccess.Text = Session("success")
        Else
            For Each fout In fouten
                Session("fout") = "Geen verbruik geregistreerd. Gelieve een aantal bij een verbruiksmateriaal in te geven."
            Next
            ASPxLabelFout.Text = Session("fout")
            Return
        End If

        Response.Redirect("~/VerbruikPage.aspx", False)


    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub

    Protected Sub ASPxCallback_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback.Callback
        Dim voocontext As New VooEntities

        Dim verbruik = voocontext.Verbruik.Find(Convert.ToInt32(Session("verbruikId")))

        If e.Parameter.Contains("Straat|") Then
            verbruik.straat = e.Parameter.ToString.Replace("Straat|", "")
        ElseIf e.Parameter.Contains("Stad|") Then
            verbruik.stad = e.Parameter.ToString.Replace("Stad|", "")
        ElseIf e.Parameter.Contains("Van|") Then
            verbruik.verbruiktvan = e.Parameter.ToString.Replace("Van|", "")
        ElseIf e.Parameter.Contains("Naar|") Then
            verbruik.verbruiktop = e.Parameter.ToString.Replace("Naar|", "")
        End If
        voocontext.SaveChanges()

    End Sub
End Class