﻿Imports System.Data.SqlClient

Public Class BestellingCompleet
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Session("taal") = 1 Then
            ASPxLabel1.Text = "Uw bestelling is verwerkt"
        Else
            ASPxLabel1.Text = "Votre commande est traitée correctement"
        End If
        Session("BestellingId") = 0
    End Sub

End Class