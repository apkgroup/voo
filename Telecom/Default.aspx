﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Default.aspx.vb" Inherits="Telecom._Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        td {
    padding:5px;
}
        </style>
    <script type="text/javascript">
        // <![CDATA[
        var curTailElement = null;
        var loadingDivText = '<div style="vertical-align: middle; text-align: center;">Bezig met laden&hellip;</div>';
        function OnTailClick(newsID, htmlElement) {
            if (!NewsCallback.InCallback() && !IsCurrentNews(htmlElement)) {
                curTailElement = htmlElement;
                ShowPopup(htmlElement, loadingDivText);
                NewsCallback.PerformCallback(newsID);
            }
        }

        function OnbtnGelezen(s, e) {

            popup3.hide;
        }

        function OnCallbackComplete(result) {
            if (GetPopupControl().IsVisible())
                ShowPopup(curTailElement, result);
        }
        function OnCallback2Complete(s, e) {
            SetPCVisible3(false);
        }
        function OnNewsControlBeginCallback() {
            GetPopupControl().Hide();
        }
        function IsCurrentNews(htmlElement) {
            return (curTailElement == htmlElement) && GetPopupControl().IsVisible();
        }
        function GetPopupControl() {
            return ASPxPopupClientControl;
        }
        function ShowPopup(element, contentText) {
            GetPopupControl().Hide();
            GetPopupControl().SetContentHTML(contentText);
            GetPopupControl().ShowAtElement(element);
        }

        function SetPCVisible3(value) {
            var popupControl = GetPopupControl3();
            if (value) {
                popupControl.Show();
            }
            else {
                popupControl.Hide();
            }
        }
        function GetPopupControl3() {
            return popup3;
        }
       

        // ]]>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>  
    <br />  
  
     <dx:ASPxPopupControl PopupAction="None" ClientInstanceName="ASPxPopupClientControl"
        PopupHorizontalAlign="OutsideRight" EnableViewState="False" ID="ASPxPopupControl1"
        runat="server" PopupHorizontalOffset="5" AllowDragging="True" PopupAnimationType="None"
        PopupVerticalAlign="TopSides" CloseAction="CloseButton" HeaderText="Details" Width="900px">
    </dx:ASPxPopupControl>
    <dx:ASPxNewsControl ID="ASPxNewsControl1" runat="server" DataSourceID="SqlDataSourceNieuws" NameField="id" DateField="Datum" HeaderTextField="Titel" TextField="Tekst" Width="800px" NavigateUrlFormatString="javascript:void('{0}');" ImageUrlField="Afbeeldinglink" EmptyDataText="Geen nieuws gevonden" >
        <Items>
            <dx:NewsItem Date="2015-03-03" HeaderText="Eerste stappen zijn gezet" Name="1" Text="De eerste stappen richting telecom webapplicatie zijn gezet. Nieuws ivm sms functionaliteit: Ivm je vraag naar een modem om mee te SMS’en:

Wij hebben een 3G stick liggen van TP-Link (model MA260).
Hiermee kan je ook SMS’en en hij ondersteunt dus ook GPRS.
Wat ik niet weet is of hij de AT-commando’s ondersteunt die jij nodig hebt.

Als je wilt mag je hem wel proberen en als hij niet werkt terugbrengen.
 ">
            </dx:NewsItem>
        </Items>
        <ItemSettings MaxLength="120" TailText="Details" />
        <ClientSideEvents TailClick="function(s, e) { OnTailClick(e.name, e.htmlElement); }"
            BeginCallback="function(s, e) { OnNewsControlBeginCallback(); }" />
    </dx:ASPxNewsControl>
    <asp:SqlDataSource ID="SqlDataSourceNieuws" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT id, Titel, Datum, Detail, Afbeeldinglink, Tekst FROM Nieuws WHERE (isnull(Actief,0)=1) and opdrachtgeverId=@opdrachtgeverId ORDER BY Datum DESC">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="0" Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="NewsCallback"
        OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { OnCallbackComplete(e.result); }" />
    </dx:ASPxCallback>

    <dx:ASPxPopupControl ID="popup3" ClientInstanceName="popup3" runat="server" CloseAction="CloseButton" HeaderText="Il y a de nouvelles ajoutées" Modal="True" Height="203px" Width="558px" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        <HeaderStyle Font-Bold="True" />
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <table>
                    <tr>
                        
                        <td>
                            L'administrateur a ajouté de nouvelles fonctionnalités. Lisez-les à coup sûr!
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <dx:ASPxButton ID="ASPxButtonGelezen" runat="server" Text="Je vais lire les nouvelles." AutoPostBack="False">
                                
                                <ClientSideEvents Click="OnbtnGelezen" />
                                
                                <Image Url="~/images/Apply_16x16.png">
                                </Image>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
         </ContentCollection>
    </dx:ASPxPopupControl>



   


</asp:Content>
