﻿Imports System.Data.SqlClient
Imports DevExpress.Web
Imports Microsoft.ajax

Public Class UitlezenStock
    Inherits System.Web.UI.Page
    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property

    'Protected Overrides Sub Initializeculture()
    '    SetCulture()
    '    MyBase.InitializeCulture()

    'End Sub
    'Private Sub SetCulture()
    '    Culture = Session("globalize").ToString
    '    UICulture = Session("globalize").ToString
    '    Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(Session("globalize").ToString)
    '    Threading.Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo(Session("globalize").ToString)
    'End Sub
    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then
            ASPxLabel1.Text = "Artikel"
            ASPxLabel2.Text = "Aantal Retour"
            ASPxLabelLotnummer.Text = "Lotnummer"

            ASPxGridView2.Columns("Article").Caption = "Artikel"
            ASPxGridView2.Columns("Description").Caption = "Omschrijving"
            ASPxGridView2.Columns("AantalKapot").Caption = "Aantal"

        Else
            ASPxLabelLotnummer.Text = "Numéro de lot"



        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))


        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer






        voocontext.Dispose()

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()

        End If




        If Page.IsPostBack Then
            ASPxLabelFout.Text = Session("fout")




            Dim i As Int32 = 1
            For Each textboxId As String In TextBoxIdCollection
                If i <= ASPxSpinEditAantal.Value Then


                    Dim label As New Label
                    If Session("taal") = 1 Then
                        label.Text = "Serienummer " & i & ":  "
                    Else
                        label.Text = "Numéro de série " & i & ":  "
                    End If

                    PanelSerie.Controls.Add(label)
                    Dim textbox = New TextBox With {
                .ID = textboxId
            }

                    Dim test As Integer = ASPxSpinEditAantal.Value
                    Dim br As New HtmlGenericControl("br")
                    If i = TextBoxIdCollection.Count Then
                        AddHandler textbox.TextChanged, AddressOf Me.txt_changed
                        textbox.AutoPostBack = True

                    Else
                        RemoveHandler textbox.TextChanged, AddressOf Me.txt_changed
                        textbox.AutoPostBack = False
                    End If
                    PanelSerie.Controls.Add(textbox)
                    PanelSerie.Controls.Add(br)
                    i += 1
                End If
            Next
        Else
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using

            Session("fout") = ""
            Session("Gelukt") = ""
            ASPxLabelFout.Text = Session("fout")
            ASPxLabelGelukt.Text = Session("Gelukt")
        End If





    End Sub




    Protected Sub ASPxComboBoxArtikel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxArtikel.SelectedIndexChanged
        ASPxSpinEditAantal.Number = 0
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

            cn.Open()
            Dim s_SQL As String = "select HeeftSerienummer, [SerieVerplichtBijOpboeken] from basismateriaal where id=@id"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
            cmd.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()
                If Not IsDBNull(dr2.Item(0)) Then
                    If dr2.Item(0) = True Then
                        ASPxSpinEditAantal.Value = 1

                        HiddenArtikel("heeftSerienummer") = dr2.Item(0)
                        HiddenArtikel("SerieVerplichtBijOpboeken") = dr2.Item(1)

                        ASPxTextBoxLotnummer.Visible = False
                        ASPxLabelLotnummer.Visible = False
                        ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)

                    Else
                        HiddenArtikel("heeftSerienummer") = dr2.Item(0)
                        HiddenArtikel("SerieVerplichtBijOpboeken") = dr2.Item(1)

                        ASPxTextBoxLotnummer.Visible = False
                        ASPxLabelLotnummer.Visible = False
                        ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)
                    End If
                Else
                    HiddenArtikel("heeftSerienummer") = False
                    HiddenArtikel("SerieVerplichtBijOpboeken") = False
                    ASPxTextBoxLotnummer.Visible = False
                    ASPxLabelLotnummer.Visible = False
                    ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)

                End If
            End If

            dr2.Close()

            cn.Close()
        End Using
        'Dim context As New VooEntities
        'Dim magid As Integer = Session("MagazijnId")
        'Dim artid As Integer = ASPxComboBoxArtikel.Value
        'Dim inStock As Decimal = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = artid).FirstOrDefault.Aantal
        'HiddenArtikel("artikelMax") = inStock

    End Sub

    Protected Sub txt_changed(sender As Object, e As EventArgs)
        ASPxSpinEditAantal.Value += 1

        ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)
    End Sub
    Protected Sub ASPxSpinEditAantal_NumberChanged(sender As Object, e As EventArgs) Handles ASPxSpinEditAantal.ValueChanged
        If HiddenArtikel.Count <> 0 Then



            Dim context As New VooEntities

                Dim newlist As New List(Of String)
                For i As Integer = 1 To ASPxSpinEditAantal.Value
                    If Not TextBoxIdCollection.Contains("Serienummer_" & ASPxComboBoxArtikel.Value & "_" & i) Then


                        Dim txt = New TextBox With {.ID = "Serienummer_" & ASPxComboBoxArtikel.Value & "_" & i}




                        'txt.ClientInstanceName = bestellijn.materiaalId & "_" & i
                        'ASPxPanelSerie.Controls.Add(txt)

                        Dim br As New HtmlGenericControl("br")
                        Dim label As New Label
                        If Session("taal") = 1 Then
                            label.Text = "Serienummer " & i & ":  "
                        Else
                            label.Text = "Numéro de série " & i & ":  "
                        End If


                        PanelSerie.Controls.Add(label)
                        PanelSerie.Controls.Add(txt)
                        PanelSerie.Controls.Add(br)
                        If i = ASPxSpinEditAantal.Value Then
                            'Bij Max event voor nieuwe textbox te maken
                            AddHandler txt.TextChanged, AddressOf Me.txt_changed

                            txt.AutoPostBack = True
                            txt.Focus()

                        Else
                            txt.AutoPostBack = False
                            RemoveHandler txt.TextChanged, AddressOf Me.txt_changed
                        End If


                    End If
                    newlist.Add("Serienummer_" & ASPxComboBoxArtikel.Value & "_" & i)
                Next
                TextBoxIdCollection = newlist

            End If

    End Sub




    Protected Sub ASPxComboBoxArtikel_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxArtikel.PreRender
        Dim cb As ASPxComboBox = CType(sender, ASPxComboBox)
        cb.TextFormatString = "{0}"
    End Sub


    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [StockMagazijnId] from [Serienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("StockMagazijnId"))
        Next row
        Session("SelectResult") = result
    End Sub


    Protected Sub ASPxGridView2_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView2.DataBinding
        DoSelect(SqlDataSourceStockMagazijn.ConnectionString)
    End Sub

    Protected Sub ASPxGridView2_DetailRowGetButtonVisibility(sender As Object, e As ASPxGridViewDetailRowButtonEventArgs) Handles ASPxGridView2.DetailRowGetButtonVisibility
        If Not (CType(Session("SelectResult"), ArrayList)).Contains(e.KeyValue) Then
            e.ButtonState = GridViewDetailRowButtonState.Hidden
        End If
    End Sub

    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("stockmagazijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub

    Protected Sub ASPxButtonVerminderen_Click(sender As Object, e As EventArgs) Handles ASPxButtonVerminderen.Click
        Dim Aantalinserted As Int32 = ASPxSpinEditAantal.Value
        Dim voocontext As New VooEntities
        Dim magid As Integer = Session("MagazijnId")
        Dim desc As String = ""
        If String.IsNullOrWhiteSpace(ASPxComboBoxArtikel.Value) Then
            If Session("taal") = 1 Then
                Session("fout") = "Geen artikel geselecteerd!"

            Else
                Session("fout") = "Aucun article sélectionné!"

            End If
            ASPxLabelFout.Text = Session("fout")
            Session("Gelukt") = ""
            ASPxLabelGelukt.Text = Session("Gelukt")
            Return
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim s_SQL As String = "select description from basismateriaal where id=@id"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not IsDBNull(dr.Item(0)) Then
                    desc = dr.Item(0)

                End If
            End If

            dr.Close()
            cn.Close()
        End Using


        Aantalinserted = 0

        Dim nrs As New List(Of String)
        For Each textboxId As String In TextBoxIdCollection
            Dim tb As TextBox = PanelSerie.FindControl(textboxId)
            Dim value As String = tb.Text
            If nrs.Contains(value) And Not String.IsNullOrWhiteSpace(value) Then
                If Session("taal") = 1 Then
                    Session("fout") = "Serienummer " & value & " Staat 2 keer in de lijst om in te lezen! Serienummer kan niet worden toegevoegd. Wijzigingen zijn niet doorgevoerd."

                Else
                    Session("fout") = "Numéro de série " & value & " Peut être lu deux fois dans la liste! Le numéro de série ne peut pas être ajouté. Les changements n'ont pas été mis en œuvre."

                End If
                ASPxLabelFout.Text = Session("fout")
                Session("Gelukt") = ""
                ASPxLabelGelukt.Text = Session("Gelukt")
                Return
            End If
            nrs.Add(value)
        Next
        ASPxTextBoxLotnummer.Visible = True
            ASPxLabelLotnummer.Visible = True
            For Each textboxId As String In TextBoxIdCollection

                Dim tb As TextBox = PanelSerie.FindControl(textboxId)
                Dim value As String = tb.Text

                Dim id As String = (CType(tb, TextBox)).ID
                Dim parts As String() = id.Split(New Char() {"_"c})
                Dim matId As Integer = parts(1).ToString

                If Not String.IsNullOrWhiteSpace(value) Then


                    Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault


                Dim serienummer = voocontext.Serienummer.Where(Function(x) x.serienummer1 = value And x.uitgeboekt <> True).FirstOrDefault
                If serienummer.statusId <> 1 And serienummer.statusId <> 2 Then
                    If Session("taal") = 1 Then

                        ASPxLabelFout.Text = "Artikel " & value & " heeft niet de status defect of retour."


                    Else
                        ASPxLabelFout.Text = "Numéro de série inexistant " & value & " annulé. Veuillez vérifier et réessayer."


                    End If
                    Return
                End If

                If Not IsNothing(serienummer) And serienummer.StockMagazijn.MagazijnId = magid Then
                        serienummer.uitgeboekt = 1
                        serienummer.statusId = 6
                        serienummer.lotNr = ASPxTextBoxLotnummer.Text
                        Dim logSerie As New Log
                        logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " uitgeboekt van hoofdmagazijn "
                        logSerie.Tijdstip = Today
                        logSerie.Gebruiker = Session("userid")
                        voocontext.Log.Add(logSerie)
                        voocontext.SaveChanges()
                        Aantalinserted += 1
                    Else

                        ASPxLabelFout.Visible = True
                    If Session("taal") = 1 Then

                        Session("Gelukt") = ""
                        ASPxLabelGelukt.Text = Session("Gelukt")

                        If IsNothing(serienummer) Then
                            ASPxLabelFout.Text = "Onbestaande serienummer " & value & " uitgeboekt. Gelieve te controleren en opnieuw te proberen."
                        Else
                            ASPxLabelFout.Text = "Serienummer " & value & " behoort niet tot het hoofdmagazijn. Gelieve te controleren en opnieuw te proberen."

                        End If
                    Else
                        If IsNothing(serienummer) Then
                                ASPxLabelFout.Text = "Numéro de série inexistant " & value & " annulé. Veuillez vérifier et réessayer."
                            Else
                                ASPxLabelFout.Text = "Le numéro de série " & value & " n'appartient pas à l'entrepôt principal. Veuillez vérifier et réessayer."

                            End If
                        End If

                        Return

                    End If
                End If
            Next


            'For Each ctr As Control In PlaceHolderSerie.Controls
            '    If TypeOf (ctr) Is TextBox Then
            '        Dim id As String = (CType(ctr, TextBox)).ID
            '        Dim value As String = (CType(ctr, TextBox)).Text
            '        Dim parts As String() = id.Split(New Char() {"_"c})
            '        Dim matId As Integer = Convert.ToInt32(parts(0))

            '        Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
            '        Dim serienummer As New Serienummer
            '        serienummer.StockMagazijnId = Stockmat.id
            '        serienummer.serienummer1 = value
            '        serienummer.uitgeboekt = 0
            '        voocontext.Serienummer.Add(serienummer)

            '        Dim logSerie As New Log
            '        logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " ingelezen op hoofdmagazijn "
            '        logSerie.Tijdstip = Today
            '        logSerie.Gebruiker = Session("userid")
            '        voocontext.Log.Add(logSerie)
            '        voocontext.SaveChanges()
            '    End If
            'Next
            voocontext.SaveChanges()




            'UPDATE STOCKARTIKEL
            Dim context As New VooEntities

        Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
        Dim stockart = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
        stockart.Aantal = stockart.Aantal - Aantalinserted

        Session("Gelukt") = Aantalinserted & " van artikel " & desc & " uitgeboekt."
        ASPxLabelGelukt.Text = Session("Gelukt")
        ASPxLabelFout.Text = ""
        'LOG
        Dim log As New Log
        log.Actie = "Gebruiker " & Session("userid") & " Heeft " & Aantalinserted & " Van artikel " & desc & "uitgeboekt van hoofdmagazijn " & Session("uitgevoerdwerk")
        log.Tijdstip = Today
        log.Gebruiker = Session("userid")
        context.Log.Add(log)
        context.SaveChanges()

        Dim sb As New Stockbeweging
        sb.beweging = -Aantalinserted
        sb.datum = DateTime.Now
        sb.gebruiker = Session("userid")
        sb.opmerking = "Uitlezen Stock"
        sb.stockmagazijnId = stockart.id
        context.Stockbeweging.Add(sb)

        Dim smid As Int32 = stockart.id
        If stockart.Basismateriaal.SerieVerplichtBijOpboeken And stockart.Aantal <> context.Serienummer.Where(Function(x) x.StockMagazijnId = stockart.id).Count() Then
            'Bij mat met serienummer, als aantal niet matched
            Dim addresses As New List(Of String)
            addresses.Add("conan.dufour@apkgroup.eu")
            mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                       "noreply@apkgroup.eu",
                       addresses,
                       "Fout is opgetreden bij uitlezen stock. Raadpleeg stockbeweging" & sb.id,
                       New Dictionary(Of String, Byte()))
        End If

        context.Dispose()
        ASPxSpinEditAantal.Value = 0
        ASPxComboBoxArtikel.Value = ""
        ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)
    End Sub


End Class