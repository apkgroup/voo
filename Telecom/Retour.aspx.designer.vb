﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Retour
    
    '''<summary>
    '''ASPxLabel11 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel11 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''SqlDataSourceMagazijnen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceMagazijnen As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''ASPxComboBoxMagazijn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxMagazijn As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''ASPxLabelResult control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabelResult As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''TableArtikel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TableArtikel As Global.System.Web.UI.HtmlControls.HtmlTable
    
    '''<summary>
    '''ASPxLabel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel1 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''HiddenArtikel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HiddenArtikel As Global.DevExpress.Web.ASPxHiddenField
    
    '''<summary>
    '''ASPxComboBoxArtikel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxArtikel As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''ASPxLabel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel2 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxSpinEditAantal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxSpinEditAantal As Global.DevExpress.Web.ASPxSpinEdit
    
    '''<summary>
    '''ASPxLabel10 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel10 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxComboBoxRetour control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxRetour As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''ASPxLabel3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel3 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxTextBoxSerie1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxSerie1 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxLabel4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel4 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxTextBoxSerie2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxSerie2 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxLabel5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel5 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxTextBoxSerie3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxSerie3 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxLabel6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel6 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxTextBoxSerie4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxSerie4 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxLabel7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel7 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxTextBoxSerie5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxSerie5 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxLabel8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel8 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxTextBoxSerie6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxSerie6 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxLabel9 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel9 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxTextBoxSerie7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxSerie7 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxCallback1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxCallback1 As Global.DevExpress.Web.ASPxCallback
    
    '''<summary>
    '''ASPxButtonToevoegen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxButtonToevoegen As Global.DevExpress.Web.ASPxButton
    
    '''<summary>
    '''SqlDataSourceStockMagazijn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceStockMagazijn As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''ASPxGridView1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxGridView1 As Global.DevExpress.Web.ASPxGridView
    
    '''<summary>
    '''SqlDataSourceSerienummers control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceSerienummers As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''SqlDataSourceArtikelen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceArtikelen As Global.System.Web.UI.WebControls.SqlDataSource
End Class
