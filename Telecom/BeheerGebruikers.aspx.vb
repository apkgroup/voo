﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data
Imports DevExpress.Web

Public Class BeheerGebruikers
    Inherits System.Web.UI.Page
    Sub Vertaal(taalId As Integer)
        If taalId = 1 Then
            ASPxGridView1.Columns("Werkg").Caption = "Werkgever"
            ASPxGridView1.Columns("Werkn").Caption = "Nummer"
            ASPxGridView1.Columns("Naam").Caption = "Naam"
            ASPxGridView1.Columns("Actief").Caption = "Actief"
            ASPxGridView1.Columns("VerlofGoedkeuring").Caption = "Keurt verlof goed"
            ASPxGridView1.Columns("AangemaaktDoor").Caption = "Aanmaker"
            ASPxGridView1.Columns("AangemaaktOp").Caption = "Aangemaakt op"
            ASPxGridView1.Columns("WachtwoordVerstuurd").Caption = "Wachtwoord verstuurd"
            ASPxGridView1.Columns("WachtwoordVerstuurdOp").Caption = "Wachtwoord verstuurd op"
            ASPxGridView1.Columns("OpdrachtgeverId").Caption = "Administratie"
            Literal2.Text = "Gebruikers"
        Else
            Literal2.Text = "Utilisateur"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))
        If Not Request.QueryString("reset") Is Nothing Then
            ASPxLabel1.Text = "Wachtwoord voor gebruiker " & Request.QueryString("reset") & " is gereset."
        ElseIf Not Request.QueryString("transfer") Is Nothing Then
            ASPxLabel1.Text = "Gebruiker " & Request.QueryString("transfer") & " is overgezet naar een nieuwe opdrachtgever."
        Else
            ASPxLabel1.Text = ""
        End If

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub

    Private Sub ASPxGridView1_RowUpdated(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()



            If (e.OldValues("Naam") <> e.NewValues("Naam")) Then
                Try

                    Dim cmd As New SqlCommand("UPDATE Gebruikers set exnaam=@exnaam where id = @id", cn)
                    Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = e.NewValues("id")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@exnaam", SqlDbType.NVarChar, -1) With {.Value = e.NewValues("Naam")}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()

                    Dim cmd2 As New SqlCommand("UPDATE magazijn set naam=@exnaam where id = (select magazijnId from gebruikers where id =@id)", cn)
                    Dim par2 As New SqlParameter("@id", SqlDbType.Int) With {.Value = e.NewValues("id")}
                    cmd2.Parameters.Add(par2)
                    par2 = New SqlParameter("@exnaam", SqlDbType.NVarChar, -1) With {.Value = e.NewValues("Naam")}
                    cmd2.Parameters.Add(par2)
                    cmd2.ExecuteNonQuery()


                Catch ex As Exception

                End Try




            End If
        End Using
    End Sub

    Private Sub ASPxCallback1_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback1.Callback
        If String.IsNullOrEmpty(e.Parameter.ToString) Then Return

        Dim arrwaarden As String() = e.Parameter.Split(New Char() {";"})
        Dim dtvan As Date = Date.Parse(arrwaarden(0))
        Dim dttot As Date = Date.Parse(arrwaarden(1))
        Session("wachtbegin") = dtvan
        Session("wachteinde") = dttot
    End Sub
End Class