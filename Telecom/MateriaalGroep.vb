'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class MateriaalGroep
    Public Property id As Integer
    Public Property omschrijving As String
    Public Property opdrachtgever As Nullable(Of Integer)

    Public Overridable Property Basismateriaal As ICollection(Of Basismateriaal) = New HashSet(Of Basismateriaal)
    Public Overridable Property Basismateriaal1 As ICollection(Of Basismateriaal) = New HashSet(Of Basismateriaal)
    Public Overridable Property Basismateriaal2 As ICollection(Of Basismateriaal) = New HashSet(Of Basismateriaal)
    Public Overridable Property Opdrachtgever1 As Opdrachtgever

End Class
