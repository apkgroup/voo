﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class StockTellingPage
    Inherits System.Web.UI.Page
    Protected Sub vertaal(taalid)
        If taalid = 1 Then
            Literal2.Text = "Doe een stocktelling"

            ASPxButton1.Text = "Stocktelling doorsturen"
            ASPxGridView1.Columns("Article").Caption = "Artikel"
            ASPxGridView1.Columns("Description").Caption = "Omschrijving"
        Else
            Literal2.Text = "Passer un commande"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Dim voocontext As New VooEntities
        Dim mag As Magazijn
        Dim magid As Integer
        If Session("isadmin") Then
            ASPxComboBoxMagazijn.ClientVisible = True
            If Not Page.IsPostBack Then
                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                    cn.Open()
                    Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                    cmd.Parameters.Add(par)
                    Dim dr2 As SqlDataReader = cmd.ExecuteReader
                    If dr2.HasRows Then
                        dr2.Read()
                        Session("MagazijnId") = dr2.GetInt32(0)
                    End If
                    dr2.Close()
                    cn.Close()
                End Using
                magid = Session("MagazijnId")
                mag = voocontext.Magazijn.Find(magid)
            Else
                magid = ASPxComboBoxMagazijn.Value
                mag = voocontext.Magazijn.Find(magid)
            End If
        Else
            ASPxComboBoxMagazijn.ClientVisible = False
            Dim userid As Integer = Session("userid")
            mag = voocontext.Gebruikers.Find(userid).Magazijn
            vertaal(Session("taal"))
            Session("Magazijnid") = mag.id
        End If







        If voocontext.Stocktelling.Where(Function(x) x.magazijnId = mag.id And x.status = 1).Any Then
            'open stockstelling aanwezig, gebruik deze

            Session("StockTellingId") = voocontext.Stocktelling.Where(Function(x) x.magazijnId = mag.id And x.status = 1).FirstOrDefault.id
        Else
            Dim stocktelling As New Stocktelling
            stocktelling.magazijnId = mag.id
            stocktelling.status = 1
            stocktelling.datum = Today
            voocontext.Stocktelling.Add(stocktelling)
            Session("StockTellingId") = stocktelling.id
            For Each stockmag In mag.StockMagazijn.Where(Function(x) x.Aantal > 0)
                Dim magmee As Boolean = False
                Dim bobijn As Boolean = True
                If stockmag.Basismateriaal.bobijnArtikel Is Nothing Or stockmag.Basismateriaal.bobijnArtikel = False Then
                    bobijn = False
                End If

                If stockmag.Basismateriaal.actief = True And Not bobijn Then
                    For Each opdrachtgever In stockmag.Basismateriaal.MateriaalOpdrachtgevers.ToList
                        If opdrachtgever.opdrachtgeverId = mag.OpdrachtgeverId Then
                            magmee = True

                        End If
                    Next

                    If magmee Then
                        'actief materiaal, stocktellinglijn toevoegen
                        Dim lijn As New Stocktellinglijn
                        lijn.stocktellingId = stocktelling.id
                        lijn.hoeveelheid = 0
                        lijn.materiaalId = stockmag.MateriaalId
                        voocontext.Stocktellinglijn.Add(lijn)
                    End If

                End If
            Next
        End If
        voocontext.SaveChanges()

        ASPxGridView1.DataSourceID = "SqlDataSourceMateriauxADMIN"
        ASPxGridView1.DataBind()

    End Sub

    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender
        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub
    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        'Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
        '    cn.Open()
        '    Dim editingRowVisibleIndex As Integer = ASPxGridView1.FindVisibleIndexByKeyValue(e.Keys.Values(0))
        '    Dim context As New VooEntities
        '    Try
        '        Dim check_sql As String = "select id from [Stocktellinglijn] where materiaalId= @materiaalId and [stocktellingId] = @StockTellingId"
        '        Dim check_cmd As New SqlCommand(check_sql, cn)
        '        Dim check_par As New SqlParameter("@StockTellingId", SqlDbType.NVarChar, -1) With {.Value = Session("StockTellingId")}
        '        check_cmd.Parameters.Add(check_par)
        '        check_par = New SqlParameter("@materiaalId", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "materiaalId").ToString()}
        '        check_cmd.Parameters.Add(check_par)
        '        Dim dr As SqlDataReader = check_cmd.ExecuteReader()
        '        If dr.HasRows Then
        '            dr.Read()
        '            If Not dr.IsDBNull(0) Then
        '                'update

        '                Dim id As Decimal = dr.GetInt32(0)
        '                dr.Close()
        '                Dim s_SQL As String = "UPDATE Bestellijn set hoeveelheid = hoeveelheid + @aantal where id = @id"

        '                Dim cmd As New SqlCommand(s_SQL, cn)
        '                Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = id}
        '                cmd.Parameters.Add(par)
        '                par = New SqlParameter("@aantal", SqlDbType.Decimal) With {.Value = e.NewValues(0)}
        '                cmd.Parameters.Add(par)
        '                If e.NewValues(0) > 0 Then
        '                    cmd.ExecuteNonQuery()
        '                End If
        '            End If

        '        Else
        '            dr.Close()
        '            Dim s_SQL As String = "Insert into Bestellijn (materiaalId, bestellingId, hoeveelheid ) VALUES (@materiaalid, @bestellingId, @hoeveelheid)"

        '            Dim cmd As New SqlCommand(s_SQL, cn)
        '            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = Session("BestellingId")}
        '            cmd.Parameters.Add(par)
        '            par = New SqlParameter("@materiaalId", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Id").ToString()}
        '            cmd.Parameters.Add(par)
        '            par = New SqlParameter("@hoeveelheid", SqlDbType.Decimal) With {.Value = e.NewValues(0)}
        '            cmd.Parameters.Add(par)
        '            If e.NewValues(0) > 0 Then
        '                cmd.ExecuteNonQuery()
        '            End If
        '        End If



        '    Catch ex As Exception
        '        Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
        '        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
        '        cmd.Parameters.Add(par)
        '        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij update post aan postengroep door {0} {1} {2}", Session("naam"), Session("voornaam"), ex.Message)}
        '        cmd.Parameters.Add(par)
        '        cmd.ExecuteNonQuery()
        '    End Try





        'End Using

    End Sub


    Protected Sub ASPxGridView1_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles ASPxGridView1.BatchUpdate
        'Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
        '    cn.Open()

        '    Dim tempId As New Integer
        '    Try
        '        Dim check_sql As String = "select id from Bestellingen where GebruikerId= @gebruikerId and Status = 1"
        '        Dim check_cmd As New SqlCommand(check_sql, cn)
        '        Dim check_par As New SqlParameter
        '        If Not Session("isadmin") Then
        '            check_par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = Session("userid")}

        '        Else
        '            check_par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
        '        End If
        '        check_cmd.Parameters.Add(check_par)
        '        Dim dr As SqlDataReader = check_cmd.ExecuteReader()
        '        If dr.HasRows Then
        '            dr.Read()
        '            If Not dr.IsDBNull(0) Then
        '                tempId = dr.GetInt32(0)
        '            End If
        '            dr.Close()
        '        Else
        '            dr.Close()
        '            Dim s_SQL As String = "Insert into Bestellingen (Datum, GebruikerId, Status) VALUES (GetDate(), @gebruikerId, 1); SELECT SCOPE_IDENTITY()"

        '            Dim cmd As New SqlCommand(s_SQL, cn)
        '            Dim par As New SqlParameter
        '            If Not Session("isadmin") Then
        '                par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = Session("userid")}

        '            Else
        '                par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
        '            End If
        '            cmd.Parameters.Add(par)
        '            tempId = cmd.ExecuteScalar()

        '        End If





        '        Session("BestellingId") = tempId
        '    Catch ex As Exception
        '        Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
        '        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
        '        cmd.Parameters.Add(par)
        '        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij update post aan postengroep door {0} {1} {2}", Session("naam"), Session("voornaam"), ex.Message)}
        '        cmd.Parameters.Add(par)
        '        cmd.ExecuteNonQuery()
        '    End Try




        'End Using
    End Sub



    Protected Sub ASPxGridView1_AfterPerformCallback(sender As Object, e As ASPxGridViewAfterPerformCallbackEventArgs) Handles ASPxGridView1.AfterPerformCallback
        ' MAIL VERZENDEN
        'Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
        '    cn.Open()
        '    Dim s_SQL As String = ""
        '    Dim cmd As SqlCommand = Nothing
        '    Dim par As SqlParameter = Nothing
        '    Dim varBericht As String = ""
        '    Dim varOnderwerp As String = ""
        '    Dim varBeheerder As String = ""
        '    Dim varEmail As String = ""
        '    s_SQL = "SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)"
        '    cmd = New SqlCommand(s_SQL, cn)
        '    par = New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Melding bestelling aangevraagd"}
        '    cmd.Parameters.Add(par)
        '    Dim dr As SqlDataReader = cmd.ExecuteReader
        '    If dr.HasRows Then
        '        dr.Read()
        '        If Not dr.IsDBNull(0) Then
        '            varBericht = dr.GetString(0)
        '            varOnderwerp = dr.GetString(1)
        '        End If
        '    End If
        '    dr.Close()
        '    s_SQL = "SELECT vwWerkn.WERKNEMER, Gebruikers_1.Email FROM Gebruikers INNER JOIN Gebruikers Gebruikers_1 ON Gebruikers.VerlofGoedkeuring=Gebruikers_1.id " _
        '        & "INNER JOIN (SELECT NAAM + ' ' + VNAAM as WERKNEMER, [ON],NR FROM Elly_SQL.dbo.WERKN) vwWerkn ON Gebruikers_1.Werkg=vwWerkn.[ON] " _
        '        & "AND Gebruikers_1.Werkn=vwWerkn.NR WHERE (Gebruikers.id=@gebruiker)"
        '    cmd = New SqlCommand(s_SQL, cn)
        '    par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Session("userid")}
        '    cmd.Parameters.Add(par)
        '    dr = cmd.ExecuteReader
        '    If dr.HasRows Then
        '        dr.Read()
        '        varBeheerder = dr.GetString(0)
        '        varEmail = dr.GetString(1)
        '    End If
        '    dr.Close()
        '    Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2010_SP2)
        '    exch.Url = New Uri("https://mail.apkintern.be/EWS/Exchange.asmx")
        '    exch.UseDefaultCredentials = False
        '    exch.Credentials = New System.Net.NetworkCredential("Administrator", "E01ab107%", "apk")
        '    TrustAllCertificatePolicy.OverrideCertificateValidation()
        '    exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.PrincipalName, Session("mailaccount"))
        '    Dim message As New EmailMessage(exch)
        '    message.Subject = varOnderwerp
        '    Dim varBody As String = varBericht.Replace("[WERKNEMER]", String.Format("{0} {1}", Session("naam"), Session("voornaam")))
        '    varBody = varBody.Replace("[BEHEERDER]", varBeheerder)
        '    varBody = varBody.Replace("[URL]", String.Format("{0}/VerlofaanvragenVerwerken.aspx?g={1}", Session("domein"), Session("userid")))
        '    varBody = varBody.Replace("[SITETITEL]", Session("sitetitel"))
        '    Dim sBody As String = "<x-html>" & vbCrLf
        '    sBody = sBody & varBody
        '    sBody = sBody & "</x-html>"
        '    message.Body = sBody
        '    message.ToRecipients.Add(varEmail)
        '    message.Send()
        'End Using


        'If Session("BestellingId") <> 0 And Not Session("BestellingId") Is Nothing Then
        '    Dim tempid As String = Session("BestellingId")
        '    ASPxWebControl.RedirectOnCallback(VirtualPathUtility.ToAbsolute("~/BestellingCompleet.aspx?id=" + tempid))
        'End If



    End Sub

    Protected Sub ASPxGridView1_CellEditorInitialize(sender As Object, e As ASPxGridViewEditorEventArgs) Handles ASPxGridView1.CellEditorInitialize

    End Sub

    Protected Sub ASPxGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles ASPxGridView1.SelectionChanged

    End Sub

    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        If Not (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue() Is Nothing Then
            Session("serienummerlijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        End If

    End Sub

    Protected Sub ASPxGridView1_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBinding
        DoSelect(SqlDataSourceMateriauxADMIN.ConnectionString)
    End Sub

    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [stocktellinglijnid] from [StocktellingSerienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("stocktellinglijnid"))
        Next row
        Session("SelectResult") = result
    End Sub

    Protected Sub ASPxGridView1_DetailRowGetButtonVisibility(sender As Object, e As ASPxGridViewDetailRowButtonEventArgs) Handles ASPxGridView1.DetailRowGetButtonVisibility

        e.ButtonState = GridViewDetailRowButtonState.Hidden

    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        Dim voocontext As New VooEntities
        Dim id As Integer = Session("StockTellingId")
        Dim stockt = voocontext.Stocktelling.Find(id)
        stockt.status = 2
        stockt.datumdoorgestuurd = Today
        voocontext.SaveChanges()
        Response.Redirect("~/StocktellingCompleet.aspx", False)


    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub
End Class