﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data
Imports DevExpress.XtraBars
Imports System.IO
Imports Google.Apis.Admin.Directory.directory_v1
Imports Google.Apis.Gmail.v1
Imports System.Net.Mail
Imports Google.Apis.Services
Imports System.Text
Imports Google.Apis.Auth.OAuth2
Imports System.Net

Module mdlAlgemeen

    Friend Scopes As String() = {"https://mail.google.com/", "https://www.googleapis.com/auth/admin.directory.user"}
    Friend serviceAccountEmail As String

    Friend credential
    Friend ext

    Public Foutgemeld As DateTime = Nothing
    Public idfilter As String = ""
    Public HuidigeGebruiker As String = 0
    Public HuidigeGebruikerVolledig As String = ""
    Public HuidigeTaal As String = ""
    Public HuidigeNiveau As String = ""
    Public HuidigeLevel As Integer = 0
    Public HuidigeLogin As String = ""
    Public HuidigPersoonId As Integer = 0
    Public HuidigPersoonLimiet As Decimal = 0.0
    Public HuidigPersoonVerantwoordelijke As Integer = 0
    Public HuidigeTaalinstelling As String = "nl-BE"






    Public Sub mailGoogle(onderwerp As String, userEmail As String, toAddresses As List(Of String), body As String, bijlagen As Dictionary(Of String, Byte()))
        'Dim username As String
        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
        Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2013_SP1) With {.Url = New Uri("https://outlook.office365.com/EWS/Exchange.asmx"), .UseDefaultCredentials = False, .Credentials = New WebCredentials("apk.admin@apkgroup.onmicrosoft.com", "boGmU5ElS2gGKjyy")}

        exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.SmtpAddress, "no.reply@apkgroup.eu")
        TrustAllCertificatePolicy.OverrideCertificateValidation()

        'end
        Dim message As New EmailMessage(exch) With {.Subject = onderwerp}
        Dim sBody As String = body
        message.Body = sBody
        'message.ToRecipients.Add("conan.dufour@apk.be")
        For Each email In toAddresses
            message.ToRecipients.Add(email.Trim())
        Next
        For Each s In bijlagen
            message.Attachments.AddFileAttachment(s.Key, s.Value)
        Next


        message.Body.BodyType = BodyType.HTML
        message.Send()


    End Sub



    Private Function Base64UrlEncode(ByVal input As String) As String
        Dim inputBytes = System.Text.Encoding.UTF8.GetBytes(input)
        Return Convert.ToBase64String(inputBytes).Replace("+"c, "-"c).Replace("/"c, "_"c).Replace("=", "")
    End Function




End Module
