﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.IO
Imports DevExpress.Web

Public Class OpdrachtgeverBeheer
    Inherits System.Web.UI.Page

    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then

            Literal2.Text = "Opdrachtgeverbeheer"

        Else

        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        Session("opdrachtgeverSession") = Session("opdrachtgever")

        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer
        If Not Page.IsPostBack And String.IsNullOrEmpty(Session("MagazijnId")) Then



            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using

            If Not Session("isingelogd") Then
                Session("url") = Request.Url.AbsoluteUri
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
            If Not Session("isadmin") And Not Session("level") = 10 Then
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
        End If

        vertaal(Session("taal"))


    End Sub



    Protected Sub ASPxGridView1_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles ASPxGridView1.RowInserted



    End Sub

    Protected Sub ASPxUploadControl1_FileUploadComplete(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs) Handles ASPxUploadControlLogo.FileUploadComplete
        If e.IsValid Then
            Dim FileInfo = New System.IO.FileInfo(e.UploadedFile.FileName)
            'Dim resfilename = MapPath(System.Web.Configuration.WebConfigurationManager.AppSettings("DocumentPath").ToString()) & "\" + FileInfo.Name
            'e.UploadedFile.SaveAs(resfilename)
        End If
    End Sub

    Public Shared Function ResizeImage(img As Image, width As Integer, height As Integer) As Image
        Dim newImage = New Bitmap(width, height)
        Using gr = Graphics.FromImage(newImage)
            gr.SmoothingMode = SmoothingMode.HighQuality
            gr.InterpolationMode = InterpolationMode.HighQualityBicubic
            gr.PixelOffsetMode = PixelOffsetMode.HighQuality
            gr.DrawImage(img, New Rectangle(0, 0, width, height))
        End Using
        Return newImage
    End Function

    Protected Sub ASPxButtonToevoegen0_Click(sender As Object, e As EventArgs) Handles ASPxButtonToevoegen0.Click
        ASPxLabelFout.Text = ""
        Dim voocontext As New VooEntities
        Dim fields As String() = {"opdrachtgeverId"}
        If String.IsNullOrEmpty(ASPxTextBoxNaamOpdrachtgever.Value) Then

            ASPxLabelFout.Text = "Naam moet ingevuld zijn"
            Return
        End If

        Dim naam As String = ASPxTextBoxNaamOpdrachtgever.Value
        If voocontext.Opdrachtgever.Where(Function(x) x.naam = naam).Any Then
            ASPxLabelFout.Text = "Opdrachtgever met deze naam bestaat al"
            Return
        End If

        Dim imageBytes = ASPxUploadControlLogo.UploadedFiles.FirstOrDefault.FileBytes
        Dim stream As New MemoryStream(imageBytes)

        Dim returnImage As System.Drawing.Image = System.Drawing.Image.FromStream(stream)
        returnImage = ResizeImage(returnImage, 310, 140)

        Dim intFileWidth = returnImage.PhysicalDimension.Width
        Dim intFileHeight = returnImage.PhysicalDimension.Height


        If ASPxUploadControlLogo.HasFile = True Then
            Dim bestandsnaam As String = IO.Path.GetFileName(ASPxUploadControlLogo.UploadedFiles.FirstOrDefault.FileName)
            If IO.File.Exists(bestandsnaam) Then IO.File.Delete(bestandsnaam)
            Dim nieuwenaam As String = Server.MapPath("images/") & ASPxTextBoxNaamOpdrachtgever.Value.Replace(" ", "").ToLower & ".png"
            If IO.File.Exists(nieuwenaam) Then IO.File.Delete(nieuwenaam)
            returnImage.Save(nieuwenaam)
        End If





        Dim opdrachtgever As New Opdrachtgever
        opdrachtgever.naam = ASPxTextBoxNaamOpdrachtgever.Value
        opdrachtgever.tag = ASPxTextBoxTag.Value
        voocontext.Opdrachtgever.Add(opdrachtgever)
        Try
            voocontext.SaveChanges()
        Catch ex As NullReferenceException

        End Try

        Dim hoofdmagazijn As New Magazijn
        hoofdmagazijn.OpdrachtgeverId = opdrachtgever.id
        hoofdmagazijn.Naam = opdrachtgever.naam
        hoofdmagazijn.Hoofd = True
        voocontext.Magazijn.Add(hoofdmagazijn)

        Dim AdminKoppeling As New AdminOpdrachtgevers
        AdminKoppeling.gebruikerId = Session("userid")
        AdminKoppeling.opdrachtgeverId = opdrachtgever.id
        voocontext.AdminOpdrachtgevers.Add(AdminKoppeling)
        voocontext.SaveChanges()
        voocontext.Dispose()




        Response.Redirect("~/OpdrachtgeverBeheer.aspx", False)
    End Sub
End Class