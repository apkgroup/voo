﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Gebruikershandtekening.aspx.vb" Inherits="Telecom.Gebruikershandtekening" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Mijn handtekening (komt op bestelbon)</h2>   
    <br />
     <table>
                    <tr>
                        <td>
                             <dx:ASPxBinaryImage ID="ASPxBinaryImage1" runat="server" EnableServerResize="True" Height="120px" Width="290px">
                                <EmptyImage Url="~/images/legehandtekening.png">
                                </EmptyImage>
                            </dx:ASPxBinaryImage>
                        </td>
                         <td style="vertical-align:bottom;">
                            <dx:ASPxButton ID="ASPxButtonUpload" runat="server" ToolTip="Klik hier om de geselecteerde scan van je handtekening te uploaden">
                                <Image Url="~/images/image_add.png" Height="20px" Width="20px">
                                 </Image>
                             </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                             <asp:FileUpload ID="FileUpload1" runat="server" Width="200px" />
                        </td>
                     </tr>
                </table>
</asp:Content>
