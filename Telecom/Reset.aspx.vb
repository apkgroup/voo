﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data
Imports DevExpress.Web



Public Class Reset
    Inherits System.Web.UI.Page
    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)
        Dim voocontext As New VooEntities
        Dim id As Integer = Request.QueryString("id")
        Dim naam As String = voocontext.Gebruikers.Find(id).Login
        If taalId = 1 Then
            Literal2.Text = "Wachtwoord resetten voor " & naam
            Literal3.Text = "Bent u zeker dat u het wachtwoord wilt resetten?"
            ASPxButton1.Text = "Nee"
            ASPxButton2.Text = "Ja"
        Else
            Literal2.Text = "Reset mot de passe pour " & naam
            Literal3.Text = "Êtes-vous sûr de vouloir réinitialiser le mot de passe?"
            ASPxButton1.Text = "Non"
            ASPxButton2.Text = "Oui"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub

    Protected Sub ASPxButton2_Click(sender As Object, e As EventArgs) Handles ASPxButton2.Click
        Dim voocontext As New VooEntities

        Dim m As New QueryStringModule
        Dim KeyGen As New RandomKeyGenerator With {.KeyLetters = "abcdefghijklmnopqrstuvwxyz", .KeyNumbers = "0123456789", .KeyChars = 12}
        Dim RandomKey As String = KeyGen.Generate()
        Dim wachtwoord As String = m.Encrypt(RandomKey, venc)

        Dim id As Integer = Request.QueryString("id")
        Dim gebruiker = voocontext.Gebruikers.Find(id)
        gebruiker.Paswoord = wachtwoord
        gebruiker.WachtwoordVerstuurdOp = Today
        voocontext.SaveChanges()


        Dim toadd As New List(Of String)
        toadd.Add(gebruiker.Email)
        Dim sBody As String = "<p>Beste, </p><p>Uw wachtwoord voor APK WMS is aangepast. U kan inloggen met gebruikersnaam [LOGIN] en wachtwoord [WACHTWOORD]</p><p>&nbsp;</p><p>Met vriendelijke groeten,</p><p>APK Group</p>"
        sBody = sBody.Replace("[LOGIN]", gebruiker.Login)
        sBody = sBody.Replace("[WACHTWOORD]", RandomKey)
        mailGoogle("Account WMS APK", "noreply@apk.be", toadd, sBody, New Dictionary(Of String, Byte()))

        voocontext.Dispose()

        Response.Redirect("~/BeheerGebruikers.aspx?reset=" & gebruiker.Login, False)
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        Response.Redirect("~/BeheerGebruikers.aspx", False)
    End Sub
End Class