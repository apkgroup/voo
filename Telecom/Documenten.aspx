﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Documenten.aspx.vb" Inherits="Telecom.Documenten" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <dx:ASPxFileManager ID="ASPxFileManager1" runat="server" DataSourceID="SqlDataSourceDocumenten" Height="600px" Width="1200px">
        <Settings InitialFolder="Voo" ThumbnailFolder="~\images\Thumbnails\" />
         <SettingsFileList View="Details">
            <ThumbnailsViewSettings ThumbnailWidth="100" ThumbnailHeight="100" />
        </SettingsFileList>
    
        <SettingsEditing AllowCopy="True" AllowCreate="True" AllowDelete="True" AllowDownload="True" AllowMove="True" AllowRename="True" />
        <SettingsToolbar>
            <Items>
                <dx:FileManagerToolbarCreateButton ToolTip="Create (F7)">
                </dx:FileManagerToolbarCreateButton>
                <dx:FileManagerToolbarRenameButton ToolTip="Rename (F2)">
                </dx:FileManagerToolbarRenameButton>
                <dx:FileManagerToolbarMoveButton ToolTip="Move (F6)">
                </dx:FileManagerToolbarMoveButton>
                <dx:FileManagerToolbarCopyButton ToolTip="Copy">
                </dx:FileManagerToolbarCopyButton>
                <dx:FileManagerToolbarDeleteButton ToolTip="Delete (Del)">
                </dx:FileManagerToolbarDeleteButton>
                <dx:FileManagerToolbarRefreshButton ToolTip="Refresh">
                </dx:FileManagerToolbarRefreshButton>
                <dx:FileManagerToolbarDownloadButton ToolTip="Download">
                </dx:FileManagerToolbarDownloadButton>
                <dx:FileManagerToolbarUploadButton ToolTip="Upload">
                </dx:FileManagerToolbarUploadButton>
            </Items>
        </SettingsToolbar>
        <SettingsUpload AutoStartUpload="True" ShowUploadPanel="False">
            <AdvancedModeSettings EnableMultiSelect="True">
            </AdvancedModeSettings>
        </SettingsUpload>
        <SettingsDataSource KeyFieldName="Id" FileBinaryContentFieldName="Data" IsFolderFieldName="IsFolder" LastWriteTimeFieldName="LastWriteTime" NameFieldName="Name" ParentKeyFieldName="ParentId" />
    </dx:ASPxFileManager>
    <asp:SqlDataSource ID="SqlDataSourceDocumenten" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [FileSystem] WHERE [Id] = @Id" InsertCommand="INSERT INTO [FileSystem] ([Name], [IsFolder], [ParentId], [Data], [LastWriteTime], [Gebruiker],[OpdrachtgeverId]) VALUES (@Name, @IsFolder, @ParentId, @Data, @LastWriteTime, @Gebruiker,@opdrachtgeverId)" SelectCommand="SELECT [Id], [Name], [IsFolder], [ParentId], [LastWriteTime], [Gebruiker] FROM [FileSystem] where(isFolder=1) OR(isfolder = 0 and opdrachtgeverId=@opdrachtgeverId)" UpdateCommand="UPDATE [FileSystem] SET [Name] = @Name, [IsFolder] = @IsFolder, [ParentId] = @ParentId, [Data] = @Data, [LastWriteTime] = @LastWriteTime, [Gebruiker] = @Gebruiker WHERE [Id] = @Id">
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="IsFolder" Type="Boolean" />
            <asp:Parameter Name="ParentId" Type="Int32" />
            <asp:Parameter Name="Data" DbType="Binary" />
            <asp:Parameter Name="LastWriteTime" Type="DateTime" />
            <asp:Parameter Name="Gebruiker" Type="String" />
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="IsFolder" Type="Boolean" />
            <asp:Parameter Name="ParentId" Type="Int32" />
             <asp:Parameter Name="Data" DbType="Binary" />
            <asp:Parameter Name="LastWriteTime" Type="DateTime" />
            <asp:Parameter Name="Gebruiker" Type="String" />
            <asp:Parameter Name="Id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
