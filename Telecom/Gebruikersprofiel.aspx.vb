﻿Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web

Public Class Gebruikersprofiel
    Inherits System.Web.UI.Page

    Public varid As String = ""
    Sub Vertaal(taalId As Integer)



        If taalId = 1 Then
            btnBewaren.Text = "Opslaan"
            Literal2.Text = "Gebruikersprofiel"
            Literal3.Text = "Hieronder vind je je persoonlijke info terug."

        Else

            Literal2.Text = "Profil utilisateur"
            Literal3.Text = "Vous trouverez ci-dessous vos informations personnelles. Veuillez compléter les informations manquantes."
            For Each item As LayoutItem In ASPxFormLayout1.Items
                If item.Caption = "Taal" Then
                    item.Caption = "Langue"
                End If

            Next

        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
        popup3.ShowOnPageLoad = False
        If Not Page.IsPostBack Then
            If (Session("isadmin")) Then
                
                cboWijziggebruiker.Visible = True
                lblWijziggebruiker.Visible = True
                cboWijziggebruiker.Value = Session("userid")
    
                ASPxFormLayout1.DataSourceID = "SqlDataSourceGebruiker"




                btnAfdrukken.Visible = True
            Else
               
                ASPxFormLayout1.DataSourceID = "SqlDataSourceGebruikeruserid"



                cboWijziggebruiker.Visible = False
                lblWijziggebruiker.Visible = False

                btnAfdrukken.Visible = False

            End If
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

            cn.Open()
            Dim cmd As New SqlCommand("SELECT Pasfoto FROM Gebruikers WHERE (id=@id)", cn)
            If (cboWijziggebruiker.Visible = True) Then
                Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = cboWijziggebruiker.Value}
                cmd.Parameters.Add(par)
                varid = cboWijziggebruiker.Value.ToString
            Else
                Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                varid = Session("userid").ToString
            End If
           
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()

                If Not dr.IsDBNull(0) Then

                End If
            End If
            dr.Close()
        End Using

    End Sub

    Protected Function GetByteArrayFromImage(varbestand) As Byte()
        Dim byteArray() As Byte = Nothing
        Using stream As New FileStream(varbestand, FileMode.Open, FileAccess.Read)
            byteArray = New Byte(stream.Length - 1) {}
            stream.Read(byteArray, 0, CInt(Fix(stream.Length)))
        End Using
        Return byteArray
    End Function


    Protected Sub ASPxCallback1_Callback(ByVal sender As Object, ByVal e As CallbackEventArgs)
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Try
                If (cboWijziggebruiker.Visible = True) Then
                    SqlDataSourceGebruiker.UpdateParameters("id").DefaultValue = cboWijziggebruiker.Value
                Else
                    SqlDataSourceGebruiker.UpdateParameters("id").DefaultValue = Session("userid")
                End If

                SqlDataSourceGebruiker.UpdateParameters("GSM").DefaultValue = txtGSM.Text
                SqlDataSourceGebruiker.UpdateParameters("Email").DefaultValue = txtEmail.Text
                SqlDataSourceGebruiker.UpdateParameters("TaalId").DefaultValue = ASPxFormLayout1_E2.Value

                SqlDataSourceGebruiker.Update()
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                If (cboWijziggebruiker.Visible = True) Then
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Gebruiker {0} {1} {2} heeft het gebruikersprofiel bijgewerkt van {3}.", Session("userid"), Session("naam"), Session("voornaam"), cboWijziggebruiker.Text)}
                    cmd.Parameters.Add(par)
                Else
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Gebruiker {0} {1} {2} heeft zijn gebruikersprofiel bijgewerkt.", Session("userid"), Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)
                End If

                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                If (cboWijziggebruiker.Visible = True) Then
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij bijwerken gebruikersprofiel Gebruiker {0} door {1} {2}.", cboWijziggebruiker.Text, Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)
                Else
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij bijwerken gebruikersprofiel Gebruiker {0} {1} {2}.", Session("userid"), Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)
                End If

                cmd.ExecuteNonQuery()
            End Try
        End Using
        Session("taal") = ASPxFormLayout1_E2.Value
        Vertaal(Session("taal"))

    End Sub
    Protected Sub ASPxCallback2_Callback(ByVal sender As Object, ByVal e As CallbackEventArgs)
        Session("eerstelogin") = False
        popup3.ShowOnPageLoad = False
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("UPDATE Gebruikers SET EersteLogin=1 WHERE (id={0})", Session("userid")), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Public Function GetRender(data As Object) As String
        Try
            Dim arrwaarden As String() = data.ToString.Split(New Char() {";"})
          
            If arrwaarden(1) = "DOC ZIEN" Then
                Return String.Format("<a href=""javascript:void(0);"" onclick=""DocumentZien({0})"">{1}</a>", arrwaarden(0), arrwaarden(1))

            Else
                Return String.Empty
            End If
        Catch ex As Exception
            Return String.Empty
        End Try


    End Function

    Protected Sub btnAfdrukken_Click(sender As Object, e As EventArgs) Handles btnAfdrukken.Click
        Session("afdrukrapport") = "Gebruikersprofiel"
        Session("afdrukgebruikersprofiel") = cboWijziggebruiker.Value
        Response.Redirect("~/rapporten/Afdruk.aspx", False)
        Context.ApplicationInstance.CompleteRequest()
    End Sub

    Protected Sub ASPxButton2_Click(sender As Object, e As EventArgs) Handles ASPxButton2.Click
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand("delete from Serienummer where shipmentId is not null; delete from Shipment;", cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand("delete from Serienummer;
delete from Shipment;
delete from VOO_Posten;
delete from gelezen;
delete from Stockbeweging;
delete from werk;
delete from StockbewegingSerienummer;
delete from Locatie;
delete from StockMagazijn where MagazijnId not in (select id from Magazijn where hoofd = 1);
delete from Bestellijn;
delete from Bestellingen;
delete from RetourbestellijnSerienummer;
delete from RetourBestellijn;
delete from RetourBestelling
delete from StocktellingSerienummer;
delete from Stocktelling;
delete from GebruikersgroepenLeden;
delete from AdminOpdrachtgevers where gebruikerId <> 86;
delete from minmax;
delete from Gebruikers where [login] <> 'TestAdmin';
delete from Magazijn where hoofd <> 1 and  id <> 128;
update StockMagazijn set aantal = 0;", cn)
            cmd.ExecuteNonQuery()
        End Using

    End Sub
End Class