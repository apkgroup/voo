﻿Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web
Imports Microsoft.ajax

Imports DevExpress.Utils
Imports System.Runtime.Serialization.Formatters.Binary

Public Class UploadDTOBoeking
    Public Property datumboeking As Date
    Public Property artikelnummer As String
    Public Property magazijn As String
    Public Property hoeveelheid As Decimal
    Public Property serienummer As String

    Public Property ordernummer As String
    Public Property adres As String
    Public Property techniekernummer As String
    Public Property bobijnnummer As String


End Class
Public Class InlezenBoekingen
    Inherits System.Web.UI.Page
    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property

    'Protected Overrides Sub Initializeculture()
    '    SetCulture()
    '    MyBase.InitializeCulture()

    'End Sub
    'Private Sub SetCulture()
    '    Culture = Session("globalize").ToString
    '    UICulture = Session("globalize").ToString
    '    Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(Session("globalize").ToString)
    '    Threading.Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo(Session("globalize").ToString)
    'End Sub
    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then

            Literal2.Text = "Kies een boekingen file om te uploaden en druk op de upload knop. <br/>Het uploaden van het bestand kan enige tijd in beslag nemen. Gelieve de pagina ondertussen niet te sluiten.<br/>Er worden ongeveer 23 lijnen per seconde verwerkt..."


        Else

            Literal2.Text = "Materiaux"


        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If


        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer






        voocontext.Dispose()

        'If Not Session("isingelogd") Then
        '    Session("url") = Request.Url.AbsoluteUri
        '    Response.Redirect("~/Login.aspx", False)
        '    Context.ApplicationInstance.CompleteRequest()

        'End If




        If Page.IsPostBack Then
            ASPxLabelFout.Text = Session("fout")
            ASPxLabelGelukt.Text = Session("Gelukt")



            Dim i As Int32 = 1

        Else
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using
            Session("fout") = ""
            Session("Gelukt") = ""

            ASPxLabelFout.Text = Session("fout")
            ASPxLabelGelukt.Text = Session("Gelukt")
        End If
        Session("fout") = ""
        Session("Gelukt") = ""




    End Sub


    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct shipmentId from [Serienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("shipmentId"))
        Next row
        Session("SelectResult") = result
    End Sub



    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("shipment") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub


    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender

        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxFormLayout1_E1.Click
        Session("MagazijnId") = 129
        Session("userid") = 86
        Session("fout") = ""
        Session("Gelukt") = ""
        Dim shipmentnumber As String

        Dim sb As New StringBuilder

        Dim fouten As New List(Of String)

        'Dim stockmags = entities.StockMagazijn.ToList
        ' Dim mats = entities.Basismateriaal.ToList
        Dim magId As Int32 = Convert.ToInt32(Session("MagazijnId"))
        Dim opdrachtgeverId As Int32 = Convert.ToInt32(Session("opdrachtgever"))


        For Each f In ASPxUploadControlShipment.UploadedFiles



            Dim byteData() As Byte
            byteData = f.FileBytes
            Dim guidstring = Guid.NewGuid.ToString
            Using wfile As New FileStream("/temp/" + guidstring + ".csv", FileMode.Append)
                wfile.Write(byteData, 0, byteData.Length)
                wfile.Close()
            End Using



            Dim fi As FileInfo

            Try
                fi = New FileInfo(f.FileName)
            Catch ex As Exception

                fouten.Add("Geen boekingen file gekozen")
                For Each fout In fouten
                    Session("fout") = Session("fout") + fout + "<br/>"
                Next
                ASPxLabelFout.Text = Session("fout")
                ASPxLabelGelukt.Text = Session("Gelukt")
                Return

            End Try



            Dim artikelen As New List(Of UploadDTOBoeking)

            Dim wb As New DevExpress.Spreadsheet.Workbook

            Using stream As New FileStream("/temp/" + guidstring + ".csv", FileMode.Open)
                wb.LoadDocument(stream)
            End Using



            Dim ws As DevExpress.Spreadsheet.Worksheet = wb.Worksheets.FirstOrDefault
            Dim bereik As String = ws.GetUsedRange.GetReferenceA1.Replace("A1:", "A2:")
            Dim range As DevExpress.Spreadsheet.CellRange = ws.Range(bereik)
            Dim test = range.RowCount


            For i As Integer = 1 To range.RowCount + 1
                Dim artikeldto As New UploadDTOBoeking
                artikeldto.artikelnummer = ws.Cells("E" + i.ToString).Value.ToString
                Try
                    artikeldto.datumboeking = ws.Cells("D" + i.ToString).Value.ToString
                Catch ex As Exception

                End Try

                artikeldto.magazijn = ws.Cells("H" + i.ToString).Value.ToString
                artikeldto.techniekernummer = ws.Cells("L" + i.ToString).Value.ToString
                artikeldto.serienummer = ws.Cells("M" + i.ToString).Value.ToString
                artikeldto.ordernummer = ws.Cells("N" + i.ToString).Value.ToString
                artikeldto.adres = ws.Cells("O" + i.ToString).Value.ToString



                Try
                    artikeldto.hoeveelheid = ws.Cells("I" + i.ToString).Value.ToString
                Catch ex As Exception


                End Try
                If artikeldto.ordernummer = "O" And f.FileName.Contains("xlsx") = False Then
                    artikeldto.hoeveelheid = artikeldto.hoeveelheid / 1000
                End If

                ' artikeldto.prijs = Convert.ToDecimal(ws.Cells("I" + i.ToString).Value.ToString)
                If ws.Cells("P" + i.ToString).Value.ToString <> "VOORS" Then
                    artikelen.Add(artikeldto)
                End If

            Next
            Dim entities As New VooEntities
            Dim opid As Integer = Session("opdrachtgever")
            Using transaction = entities.Database.BeginTransaction()
                Try




                    Dim bmatid As Integer
                    For Each artikel In artikelen
                        Dim magazijn As Magazijn
                        If artikel.ordernummer <> "O" Then
                            If Not String.IsNullOrWhiteSpace(artikel.serienummer) Then
                                Dim serie = entities.Serienummer.Where(Function(x) x.serienummer1 = artikel.serienummer).FirstOrDefault

                                If serie Is Nothing Then
                                    Dim log As New Log
                                    log.Actie = "Serienummer " + artikel.serienummer + " niet gevonden bij afboeking fluvius"
                                    log.Gebruiker = Session("user")
                                    log.Tijdstip = Date.Now
                                    entities.Log.Add(log)
                                    Continue For
                                Else
                                    magazijn = serie.StockMagazijn.Magazijn
                                End If

                            End If


                            Dim verbid As Integer

                            Dim opdrid As Integer = Session("opdrachtgever")


                            magazijn = entities.Magazijn.Where(Function(x) x.Hoofd = True And x.OpdrachtgeverId = opdrid).FirstOrDefault


                            If Not entities.Verbruik.Where(Function(x) x.ordernummer = artikel.ordernummer).Any Then
                                Dim verb As New Verbruik
                                verb.ordernummer = artikel.ordernummer
                                verb.straat = artikel.adres

                                verb.verbruiktop = magazijn.id
                                verb.verbruiktvan = magazijn.id
                                verb.opmerking = "Via upload file fluvius"
                                Try
                                    verb.datum = artikel.datumboeking
                                Catch ex As Exception

                                End Try

                                verb.status = 2
                                entities.Verbruik.Add(verb)
                                entities.SaveChanges()

                                verbid = verb.id

                            Else
                                verbid = entities.Verbruik.Where(Function(x) x.ordernummer = artikel.ordernummer).FirstOrDefault.id
                            End If
                            Dim verblijn As New Verbruiklijn
                            verblijn.hoeveelheid = -artikel.hoeveelheid
                            Try

                                Dim mat As Basismateriaal

                                Dim gevondenMats = entities.Basismateriaal.Where(Function(x) x.Article = artikel.artikelnummer)
                                If gevondenMats.Count > 1 Then
                                    For Each gevonden In gevondenMats
                                        If gevonden.MateriaalOpdrachtgevers.Where(Function(x) x.opdrachtgeverId = opid).Any Then
                                            mat = gevonden
                                        End If
                                    Next
                                Else
                                    mat = gevondenMats.FirstOrDefault
                                End If


                                verblijn.materiaalId = mat.id
                            Catch ex As Exception
                                'fouten.Add("Artikelnummer " + artikel.artikelnummer + " niet gevonden.")
                                Continue For
                            End Try
                            verblijn.verbruikId = verbid
                            'bobijne moet ni - Robby
                            entities.Verbruiklijn.Add(verblijn)
                            If Not String.IsNullOrWhiteSpace(artikel.serienummer) Then
                                Dim serie = entities.Serienummer.Where(Function(x) x.serienummer1 = artikel.serienummer).FirstOrDefault

                                If serie Is Nothing Then

                                    fouten.Add("Serienummer " + artikel.serienummer + " niet gevonden in WMS ")
                                End If

                                If serie.uitgeboekt = True Then
                                    fouten.Add("Serienummer " + artikel.serienummer + " is al uitgeboekt")
                                End If

                                If serie.StockMagazijn.Magazijn.OpdrachtgeverId <> opdrid Then
                                    fouten.Add("Serienummer " + artikel.serienummer + " zit onder een andere opdrachtgever als het gekozen magazijn")
                                End If

                                serie.uitgeboekt = True
                                serie.datumUsed = Today
                                serie.statusId = 5
                                Dim verblsr As New VerbruikSerienummer
                                verblsr.serienummer = artikel.serienummer
                                verblsr.verbruiklijnId = verblijn.id
                                entities.VerbruikSerienummer.Add(verblsr)
                                entities.SaveChanges()


                            End If

                            Dim sm = entities.StockMagazijn.Where(Function(x) x.MateriaalId = verblijn.materiaalId And x.MagazijnId = magazijn.id).FirstOrDefault

                            Try
                                sm.Aantal = sm.Aantal - verblijn.hoeveelheid
                            Catch ex As Exception
                                fouten.Add("Stockmazijncombinatie voor " + artikel.artikelnummer + " niet gevonden in magazijn " + magazijn.Naam)
                                Continue For
                            End Try

                            Dim stockb As New Stockbeweging
                            stockb.beweging = artikel.hoeveelheid
                            stockb.datum = Today
                            Dim gebruiker = entities.Gebruikers.Where(Function(x) x.Techniekernummer.Contains(artikel.techniekernummer)).FirstOrDefault
                            If gebruiker IsNot Nothing Then
                                stockb.gebruiker = gebruiker.id
                            Else
                                stockb.gebruiker = artikel.techniekernummer
                            End If
                            stockb.stockmagazijnId = sm.id
                            stockb.opmerking = "Afboeking via boekingsheet Fluvius"

                            entities.Stockbeweging.Add(stockb)
                            entities.SaveChanges()
                            If Not String.IsNullOrWhiteSpace(artikel.serienummer) Then
                                Dim stockbsr As New StockbewegingSerienummer
                                stockbsr.stockbewegingid = stockb.id
                                Dim serie = entities.Serienummer.Where(Function(x) x.serienummer1 = artikel.serienummer).FirstOrDefault
                                stockbsr.serienummerId = serie.id
                                entities.StockbewegingSerienummer.Add(stockbsr)
                            End If



                        Else
                            'inlezen
                            If Not String.IsNullOrWhiteSpace(artikel.serienummer) Then
                                If entities.Serienummer.Where(Function(x) x.serienummer1 = artikel.serienummer).Any Then
                                    'serienummer bestaat al geen opboeking doen
                                    Continue For
                                End If
                            End If
                            magazijn = entities.Magazijn.Where(Function(x) x.Naam = artikel.magazijn).FirstOrDefault
                            Dim opdrid As Integer = Session("opdrachtgever")
                            If magazijn Is Nothing Then
                                magazijn = entities.Magazijn.Where(Function(x) x.Hoofd = 1 And x.OpdrachtgeverId = opdrid).FirstOrDefault
                            End If

                            Dim mat As Basismateriaal

                            Dim gevondenMats = entities.Basismateriaal.Where(Function(x) x.Article = artikel.artikelnummer)
                            If gevondenMats.Count > 1 Then
                                For Each gevonden In gevondenMats
                                    If gevonden.MateriaalOpdrachtgevers.Where(Function(x) x.opdrachtgeverId = opid).Any Then
                                        mat = gevonden
                                    End If
                                Next
                            Else
                                mat = gevondenMats.FirstOrDefault
                            End If

                            Dim stockm = entities.StockMagazijn.Where(Function(x) x.Basismateriaal.Article = mat.Article And x.MagazijnId = magazijn.id).FirstOrDefault
                            If stockm IsNot Nothing Then
                                stockm.Aantal = stockm.Aantal + artikel.hoeveelheid


                                Dim stockb As New Stockbeweging
                                stockb.beweging = artikel.hoeveelheid
                                stockb.datum = Today
                                Dim gebruiker = entities.Gebruikers.Where(Function(x) x.Techniekernummer = artikel.techniekernummer).FirstOrDefault
                                If gebruiker IsNot Nothing Then
                                    stockb.gebruiker = gebruiker.id
                                Else
                                    stockb.gebruiker = artikel.techniekernummer
                                End If
                                stockb.stockmagazijnId = stockm.id
                                stockb.opmerking = "Opboeking via boekingsheet Fluvius"
                                entities.Stockbeweging.Add(stockb)
                                entities.SaveChanges()
                                If Not String.IsNullOrWhiteSpace(artikel.serienummer) Then

                                    Dim serie As New Serienummer
                                    serie.datumGemaakt = Today
                                    serie.datumOntvangst = Today
                                    serie.serienummer1 = artikel.serienummer
                                    serie.StockMagazijnId = stockm.id
                                    serie.statusId = 3
                                    serie.uitgeboekt = False
                                    serie.FIFO = False
                                    entities.Serienummer.Add(serie)
                                    entities.SaveChanges()
                                    Dim sbserie As New StockbewegingSerienummer
                                    sbserie.serienummerId = serie.id
                                    sbserie.stockbewegingid = stockb.id
                                    entities.StockbewegingSerienummer.Add(sbserie)

                                End If
                            Else
                                fouten.Add("Stockmazijncombinatie voor " + artikel.artikelnummer + " niet gevonden in magazijn " + magazijn.Naam)

                            End If
                        End If
                    Next

                    If fouten.Count = 0 Then
                        entities.SaveChanges()
                        transaction.Commit()
                        ASPxLabelGelukt.Text = "Boekingfile successvol ingelezen"

                    Else
                        transaction.Rollback()
                        For Each fout In fouten
                            Session("fout") = Session("fout") + fout + "<br/>"
                        Next

                    End If
                Catch ex As Exception
                    transaction.Rollback()
                    fouten.Add("Er is een probleem opgetreden: " + ex.Message)
                    For Each fout In fouten

                        Session("fout") = Session("fout") + fout + "<br/>"
                    Next
                End Try

            End Using
            entities.Dispose()


        Next

        ASPxLabelFout.Text = Session("fout")
        'ASPxLabelGelukt.Text = Session("Gelukt")
    End Sub


End Class