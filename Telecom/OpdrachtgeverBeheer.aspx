﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="OpdrachtgeverBeheer.aspx.vb" Inherits="Telecom.OpdrachtgeverBeheer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 38px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
    <p>&nbsp;</p>
     <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" Text="" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
                </dx:ASPxLabel>

                <dx:ASPxButton ID="ASPxButtonToevoegen" runat="server" AutoPostBack="false" Width="50px" Text="Nieuwe opdrachtgever toevoegen" CssClass="minimargin">
                     <ClientSideEvents Click="function(s, e) { pnlForm.SetVisible(true); }" />  
                </dx:ASPxButton>

    <dx:ASPxPanel ID="pnlForm" runat="server"  ClientVisible="false" ClientInstanceName="pnlForm" CssClass="minimargin" Paddings-PaddingLeft="5px" Paddings-PaddingBottom="5px" Width="400px">  
        <Paddings PaddingBottom="5px" />
    <PanelCollection>  
        <dx:PanelContent>  
            &nbsp;<table style="width:50%;">
                <tr>
                    <td style="text-align: right" class="auto-style1">Opdrachtgever naam:</td>
                    <td class="auto-style1">
                        <dx:ASPxTextBox ID="ASPxTextBoxNaamOpdrachtgever" runat="server" Width="280px">
                        </dx:ASPxTextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td style="text-align: right" class="auto-style1">Opdrachtgever groepering:</td>
                    <td class="auto-style1">
                        <dx:ASPxTextBox ID="ASPxTextBoxTag" runat="server" Width="280px">
                        </dx:ASPxTextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td style="text-align: right">Logo (liefst transparant):</td>
                    <td>
                        <dx:ASPxUploadControl ID="ASPxUploadControlLogo" runat="server" Width="280px" OnFileUploadComplete="ASPxUploadControl1_FileUploadComplete" FileUploadMode="OnPageLoad">
                            <ValidationSettings AllowedFileExtensions=".png">
                            </ValidationSettings>
                        </dx:ASPxUploadControl>
                    </td>
                </tr>
                  
          
            </table>
            <h2>&nbsp;</h2>
            <dx:ASPxButton ID="ASPxButtonToevoegen0" runat="server" Width="50px">
                <Image IconID="actions_add_32x32office2013">
                </Image>
            </dx:ASPxButton>
        </dx:PanelContent>  
    </PanelCollection>  
        <Border BorderColor="Silver" BorderWidth="1px" />
</dx:ASPxPanel>  
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceLocatie" KeyFieldName="id">
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="naam" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
           
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceLocatie" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Opdrachtgever]">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) and hoofd =1">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>