﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="VerbruikBobijn.aspx.vb" Inherits="Telecom.VerbruikBobijn" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        <script type="text/javascript">
            var timerHandle = -1;
        var indexrow
            function saveChangesBtn_Click(s, e) {
            
            ASPxGridView1.UpdateEdit();
            
            
        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();
           
        }

                        function OnBatchEditStartEditing(s, e) {
            currentRowIndex = e.visibleIndex;
            currentRowIndex2 = e.visibleIndex;
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("Hoeveelheid");
        }
        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);
            
            indexrow = e.visibleIndex;
            console.log("Index row set to " + indexrow);
        }
        
    </script>
        </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceMagazijnenVerbruik" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId and (g.Actief = 1 or hoofd=1 or type is not null)) and (m.type=1) and (m.af is null or m.af = 0)">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnenVerbruikSYN" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] in (select id from Opdrachtgever where tag = 'SYN') and (m.type=1) and (m.af is null or m.af = 0))">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceBobijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT b.id,
[bobijnnummer]
  FROM [Voo].[dbo].[Bobijn] b
  inner join [Voo].[dbo].StockMagazijn sm on b.stockmagazijnId = sm.id
   inner join [Voo].[dbo].Magazijn m on sm.MagazijnId = m.id
    inner join [Voo].[dbo].Basismateriaal ba on ba.id = sm.MateriaalId
where m.opdrachtgeverId = @opdrachtgeverId and (b.AF is null or b.AF = 0)">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="SqlDataSourceBobijnenTech" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT b.id,
[bobijnnummer]
  FROM [Voo].[dbo].[Bobijn] b
  inner join [Voo].[dbo].StockMagazijn sm on b.stockmagazijnId = sm.id
   inner join [Voo].[dbo].Magazijn m on sm.MagazijnId = m.id
    inner join [Voo].[dbo].Basismateriaal ba on ba.id = sm.MateriaalId
where m.id =(select magazijnId from Gebruikers where id = @userid)">
        <SelectParameters>
            <asp:SessionParameter Name="userid" SessionField="userid" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <br />

     <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="80%">
            <Paddings PaddingBottom="5px" />
    <PanelCollection>  
        <dx:PanelContent>  
            &nbsp;<table style="width:100%;">
                <tr>
                    <td style="text-align: right; width: 40px;">Bobijn:</td>
                    <td >
                         <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceBobijnen" TextField="bobijnnummer" ValueField="id" ClientInstanceName="bron" Width="100%" AutoPostBack="True">
    </dx:ASPxComboBox>
    

                    </td>
                     <td style="text-align: right; width: 40px;">Huidig magazijn:</td>
                    <td >
                        <dx:ASPxLabel ID="ASPxLabelMagazijn" runat="server"></dx:ASPxLabel>
                    </td>
                   
                </tr>
                <tr>
                    <td style="text-align: right; width: 40px;">Artikel:</td><td ><dx:ASPxLabel ID="ASPxLabelArtikel" runat="server"></dx:ASPxLabel></td>
                    <td style="text-align: right; width: 40px;">Resterende hoeveelheid:</td><td ><dx:ASPxLabel ID="ASPxLabelHoeveelheid" runat="server"></dx:ASPxLabel></td>
                </tr>
                <tr>
                         <td style="text-align: right; width: 40px;">Ordernummer:</td><td><dx:ASPxTextBox ID="ASPxTextBoxOrdernummer" runat="server" Width="100%"></dx:ASPxTextBox>
                         </td>
                    <td style="text-align: right; width: 40px;"></td><td ></td>
                
                     </tr>
                <tr>
                    <td style="text-align: right ;width: 40px;">Stad:</td>
                    <td>
                        <dx:aspxtextbox ID="ASPxTextboxStad" runat="server"  CheckState="Unchecked" ClientInstanceName="stad" Width="100%">

                        </dx:aspxtextbox>
                    </td>
                    <td style="text-align: right ;width: 40px;">Straat</td>
                    <td style="max-width: 100px;">
                        <dx:aspxtextbox ID="ASPxTextboxStraat" runat="server"  CheckState="Unchecked" ClientInstanceName="straat">

                        </dx:aspxtextbox>
                    </td>
                </tr>
                    <tr>
                    <td style="text-align: right ;width: 40px;">Hoeveelheid afboeken:</td>
                    <td>
                        <dx:aspxspinedit ID="ASPxHoeveelheid" runat="server"  ClientInstanceName="hoeveelheid" Width="100%">

                        </dx:aspxspinedit>
                    </td>
                     <td style="text-align: right ;width: 40px;" >Verbruiksmagazijn:</td>
                    <td style="max-width: 100px;">
                        <dx:ASPxComboBox ID="ASPxComboBoxMagazijnNaar"   runat="server" DataSourceID="SqlDataSourceMagazijnenVerbruik" TextField="Naam" ValueField="id" ClientInstanceName="verbruiks">

    </dx:ASPxComboBox>
                    </td>

                </tr>

            </table>
                

        </dx:PanelContent>  
    </PanelCollection>  
        <Border BorderColor="Silver" BorderWidth="1px" />
     </dx:ASPxCallbackPanel>
    
  
    
    
  
   
   
    
  
    
    
  
    <br />
     <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
                </dx:ASPxLabel>
    <dx:ASPxLabel ID="ASPxLabelSuccess" CssClass="enableMultiLine" runat="server" EncodeHtml="false" Font-Bold="True" ForeColor="#00CC00">
                </dx:ASPxLabel>

    

    <br />

    &nbsp;<dx:ASPxButton ID="ASPxButton1" runat="server" Text="Verbruik boeken"  ClientInstanceName="saveChangesBtn" CssClass="inline" Width="75px">
                        
                    </dx:ASPxButton>
        
    <br />
                
    </asp:Content>
