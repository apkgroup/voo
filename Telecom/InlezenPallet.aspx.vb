﻿Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web
Imports Microsoft.ajax

Imports DevExpress.Utils

Public Class InlezenPallet
    Inherits System.Web.UI.Page
    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property

    'Protected Overrides Sub Initializeculture()
    '    SetCulture()
    '    MyBase.InitializeCulture()

    'End Sub
    'Private Sub SetCulture()
    '    Culture = Session("globalize").ToString
    '    UICulture = Session("globalize").ToString
    '    Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(Session("globalize").ToString)
    '    Threading.Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo(Session("globalize").ToString)
    'End Sub
    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then

            Literal2.Text = "Scan een Pallet en klik op 'upload' om de serienummers in te brengen. Het systeem zal aanwijzen in welke rek dit pallet geladen moet worden."


        Else

            Literal2.Text = "Materiaux"


        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        vertaal(Session("taal"))


        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer






        voocontext.Dispose()

        'If Not Session("isingelogd") Then
        '    Session("url") = Request.Url.AbsoluteUri
        '    Response.Redirect("~/Login.aspx", False)
        '    Context.ApplicationInstance.CompleteRequest()

        'End If




        If Page.IsPostBack Then
            ASPxLabelFout.Text = Session("fout")




            Dim i As Int32 = 1

        Else
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using

            Session("fout") = ""
            Session("Gelukt") = ""
            ASPxLabelFout.Text = Session("fout")
            ASPxLabelGelukt.Text = Session("Gelukt")
        End If





    End Sub


    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [StockMagazijnId] from [Serienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("StockMagazijnId"))
        Next row
        Session("SelectResult") = result
    End Sub






    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("stockmagazijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub


    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender

        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxFormLayout1_E1.Click


        Session("fout") = ""
        Session("Gelukt") = ""

        Dim fouten As New List(Of String)

        Dim sb As New StringBuilder
        Dim entities As New VooEntities


        'Dim stockmags = entities.StockMagazijn.ToList
        'Dim mats = entities.Basismateriaal.ToList
        Dim magId As Int32 = Convert.ToInt32(Session("MagazijnId"))
        Dim shipmentIds As New List(Of String)


        Dim PalletNo As String = ASPxFormLayoutPalletNo.Text
        If Not String.IsNullOrEmpty(PalletNo) Then
            Dim srnrs = entities.Serienummer.Where(Function(x) x.palletNo = PalletNo).ToList
            If srnrs.Count > 0 Then
            Else
                fouten.Add("Geen serienummers gevonden met dit palletnummer.")

            End If


            For Each srnr As Serienummer In srnrs
                If srnr.StockMagazijn.MagazijnId <> Session("MagazijnId") Then
                    fouten.Add("Shipment waar deze serienummer (" + srnr.serienummer1 + ") toe behoort is voor een ander magazijn bestemd. (" + srnr.StockMagazijn.Magazijn.Naam + ")")
                Else
                    If srnr.statusId <> 7 Then
                        fouten.Add("Serienummer (" + srnr.serienummer1 + ") heeft niet de status 'to be received'")
                    Else
                        If Not shipmentIds.Contains(srnr.shipmentId) Then
                            shipmentIds.Add(srnr.shipmentId)
                        End If
                    End If
                End If
            Next
            If shipmentIds.Count > 1 Then
                fouten.Add("Deze pallet (" + PalletNo + ") is verspreid geraakt over meerdere shipments. Gelieve shipments na te kijken met nummers: <br/>")
                For Each shipmentId In shipmentIds
                    fouten.Add(shipmentId + "<br/>")
                Next

            End If

            If fouten.Count > 0 Then


                For Each fout In fouten
                    Session("fout") = Session("fout") + fout + "<br/>"
                    Session("Gelukt") = ""
                    ASPxLabelFout.Text = Session("fout")
                    ASPxLabelGelukt.Text = Session("Gelukt")
                Next
                Return
            End If

            'berekenen locatieId
            Dim tobeloc As Integer = 0
            Dim lijstserie As New List(Of String)
            Dim stockmagazijnId As String
            Dim locatiesMagazijn = entities.Locatie.Where(Function(x) x.magazijnid = magId).ToList
            'welke serienummers zaten er in dit shipment en staan in het magazijn
            Dim SerienummersShipment = entities.Serienummer.Where(Function(x) x.statusId = 3 And x.shipmentId = shipmentIds.FirstOrDefault)
            'welke locaties zijn gebruikt voor serienummer van dit shipment
            Dim gebruiktelocatiesShipment = SerienummersShipment.Where(Function(x) x.locatieId IsNot Nothing).GroupBy(Function(x) x.locatieId)
            Dim gebruiktelocatiesAll = entities.Serienummer.Where(Function(x) x.locatieId IsNot Nothing And x.uitgeboekt = False).GroupBy(Function(x) x.locatieId)
            For Each locatie In gebruiktelocatiesShipment
                'haal capaciteit op van elke gebruikte locatie van dit shipment
                Dim locatieId As Integer = locatie.Key
                Dim cap = entities.Locatie.Find(locatieId).capaciteit
                'als er nog plaats is -> rek gevonden
                If SerienummersShipment.Where(Function(x) x.locatieId = locatieId).GroupBy(Function(x) x.palletNo).Count < cap Then
                    tobeloc = locatieId
                    Exit For
                End If
            Next
            'geen locatie gevonden -> Zoek de eerte lege locatie
            If tobeloc = 0 Then
                Dim listgebruiktelocaties As New List(Of Integer)
                For Each gebruikteloc In gebruiktelocatiesAll
                    listgebruiktelocaties.Add(gebruikteloc.Key)
                Next
                For Each locatie In locatiesMagazijn
                    If Not listgebruiktelocaties.Contains(locatie.id) Then
                        tobeloc = locatie.id
                        Exit For
                    End If
                Next

            End If

            If tobeloc = 0 Then
                fouten.Add("Geen rekken meer vrij.")
            Else
                For Each srnr As Serienummer In srnrs
                    If srnr.StockMagazijn.MagazijnId <> Session("MagazijnId") Then
                        fouten.Add("Shipment file voor deze serienummer (" + srnr.serienummer1 + ") is voor een ander magazijn bestemd.")
                    Else
                        If srnr.statusId <> 7 Then
                            fouten.Add("Serienummer (" + srnr.serienummer1 + ") heeft niet de status 'to be received'")
                        Else
                            'alles oké!
                            srnr.statusId = 3
                            srnr.locatieId = tobeloc
                            stockmagazijnId = srnr.StockMagazijnId
                            srnr.StockMagazijn.Aantal = srnr.StockMagazijn.Aantal + 1
                            srnr.datumOntvangstMagazijn = Today.Date
                            lijstserie.add(srnr.serienummer1)
                            If Not shipmentIds.Contains(srnr.shipmentId) Then
                                shipmentIds.Add(srnr.shipmentId)
                            End If
                        End If
                    End If
                Next
            End If

            If fouten.Count = 0 Then

                entities.SaveChanges()


                Dim stb As New Stockbeweging
                stb.beweging = lijstserie.Count
                stb.datum = Today
                stb.gebruiker = Session("userid")
                stb.opmerking = "Inlezen Stock"
                stb.stockmagazijnId = stockmagazijnId
                entities.Stockbeweging.Add(stb)

                entities.SaveChanges()

                For Each serie In lijstserie
                    Dim sbsr As New StockbewegingSerienummer
                    sbsr.stockbewegingid = stb.id
                    sbsr.serienummerId = entities.Serienummer.Where(Function(x) x.serienummer1 = serie).FirstOrDefault.id
                    entities.StockbewegingSerienummer.Add(sbsr)
                Next

                entities.SaveChanges()

                Session("Gelukt") = "Pallet met palletnummer " + PalletNo + " successvol ingeladen. Dit pallet bevat " + srnrs.Count.ToString + " serienummer(s). Gelieve dit pallet op plaats: " + locatiesMagazijn.Where(Function(x) x.id = tobeloc).FirstOrDefault.naam + " te plaatsen."
                ASPxLabelGelukt.Text = Session("Gelukt")
            Else
                For Each fout In fouten
                    Session("fout") = Session("fout") + fout + "<br/>"
                    Session("Gelukt") = ""
                    ASPxLabelFout.Text = Session("fout")
                    ASPxLabelGelukt.Text = Session("Gelukt")
                Next

            End If



        End If

        ASPxLabelFout.Text = Session("fout")
        ASPxLabelGelukt.Text = Session("Gelukt")
    End Sub


End Class