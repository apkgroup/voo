﻿Public Class MijnWerken
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()

        End If
    End Sub

    Protected Sub ASPxGridView1_Load(sender As Object, e As EventArgs) Handles ASPxGridView1.Load

        If (Session("level") = 1) Then
            ASPxGridView1.Columns("Werknemer").Visible = True
            ASPxGridView1.DataSourceID = "SqlDataSource2"
        End If
    End Sub
End Class