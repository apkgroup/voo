'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class Gebruikers
    Public Property id As Integer
    Public Property Login As String
    Public Property Paswoord As String
    Public Property Werkg As String
    Public Property Werkn As Nullable(Of Integer)
    Public Property GSM As String
    Public Property Actief As Nullable(Of Boolean)
    Public Property Niveau As Nullable(Of Integer)
    Public Property Email As String
    Public Property AangemaaktDoor As Nullable(Of Integer)
    Public Property AangemaaktOp As Nullable(Of Date)
    Public Property WachtwoordVerstuurd As Nullable(Of Boolean)
    Public Property WachtwoordVerstuurdOp As Nullable(Of Date)
    Public Property VerlofGoedkeuring As Nullable(Of Integer)
    Public Property EersteLogin As Nullable(Of Boolean)
    Public Property Pasfoto As Byte()
    Public Property Handtekening As Byte()
    Public Property VanWacht As Nullable(Of Boolean)
    Public Property OpdrachtgeverId As Nullable(Of Integer)
    Public Property MagazijnId As Nullable(Of Integer)
    Public Property TaalId As Nullable(Of Integer)
    Public Property ExNaam As String
    Public Property Techniekernummer As String
    Public Property Nummerplaat As String

    Public Overridable Property AdminOpdrachtgevers As ICollection(Of AdminOpdrachtgevers) = New HashSet(Of AdminOpdrachtgevers)
    Public Overridable Property BestellingBobijn As ICollection(Of BestellingBobijn) = New HashSet(Of BestellingBobijn)
    Public Overridable Property Bestellingen As ICollection(Of Bestellingen) = New HashSet(Of Bestellingen)
    Public Overridable Property Gebruikersniveaus As Gebruikersniveaus
    Public Overridable Property Magazijn As Magazijn
    Public Overridable Property Opdrachtgever As Opdrachtgever
    Public Overridable Property Taal As Taal
    Public Overridable Property GebruikersgroepenLeden As ICollection(Of GebruikersgroepenLeden) = New HashSet(Of GebruikersgroepenLeden)
    Public Overridable Property gelezen As ICollection(Of gelezen) = New HashSet(Of gelezen)
    Public Overridable Property Verbruik As ICollection(Of Verbruik) = New HashSet(Of Verbruik)

End Class
