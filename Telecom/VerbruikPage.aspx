﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="VerbruikPage.aspx.vb" Inherits="Telecom.VerbruikPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        var timerHandle = -1;
        var indexrow
        function saveChangesBtn_Click(s, e) {

            ASPxGridView1.UpdateEdit();


        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();

        }

        function checkFilters() {
            if (CheckNulwaarden.GetCheckState() == 'Checked' && CheckGroepen.GetCheckState() == 'Unchecked') {
                ASPxGridView1.ApplyFilter('huidig > 0');
            } else if (CheckNulwaarden.GetCheckState() == 'Unchecked' && CheckGroepen.GetCheckState() == 'Unchecked') {
                ASPxGridView1.ClearFilter();
            }
            else if (CheckNulwaarden.GetCheckState() == 'Unchecked' && CheckGroepen.GetCheckState() == 'Checked') {
                ASPxGridView1.ApplyFilter('MateriaalGroep = ' + GroepFilter.GetValue() + ' or MateriaalGroep2 = ' + GroepFilter.GetValue() + ' or MateriaalGroep3 = ' + GroepFilter.GetValue());
            }
            else if (CheckNulwaarden.GetCheckState() == 'Checked' && CheckGroepen.GetCheckState() == 'Checked') {
                ASPxGridView1.ApplyFilter('huidig > 0 and (MateriaalGroep = ' + GroepFilter.GetValue() + ' or MateriaalGroep2 = ' + GroepFilter.GetValue() + ' or MateriaalGroep3 = ' + GroepFilter.GetValue() + ')');
            }
        }

        function OnBatchEditStartEditing(s, e) {
            currentRowIndex = e.visibleIndex;
            currentRowIndex2 = e.visibleIndex;
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("Hoeveelheid");
        }
        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);

            indexrow = e.visibleIndex;
            console.log("Index row set to " + indexrow);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceMagazijnenVerbruik" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE ((m.[OpdrachtgeverId] = @OpdrachtgeverId and (g.Actief = 1 or hoofd=1 or type is not null)) and (m.type=1) and (m.af is null or m.af = 0)) or m.id = (select magazijnId from gebruikers g2 where g2.id = @gebruikersid)">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
            <asp:SessionParameter Name="gebruikersid" SessionField="userid" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnenVerbruikSYN" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] in (select id from Opdrachtgever where tag = 'SYN') and (m.type=1) and (m.af is null or m.af = 0))"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId and (g.Actief = 1 or hoofd=1 or type is not null)) and (m.hoofd=1 or m.id =(select magazijnId from Gebruikers where id = @userid))">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
            <asp:SessionParameter Name="userid" SessionField="userid" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSourceMagazijnenSYNtech" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT distinct m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE  m.id =(select magazijnId from Gebruikers where id = @userid)">
        <SelectParameters>
            <asp:SessionParameter Name="userid" SessionField="userid" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSourceMagazijnenSYNadmin" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE  m.opdrachtgeverId = @opdrachtgever and (m.af = 0 or m.af is null)">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="opdrachtgever" SessionField="opdrachtgever" />
        </SelectParameters>
    </asp:SqlDataSource>

    <br />
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen0" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE m.id =(select magazijnId from Gebruikers where id = @userid)">
        <SelectParameters>
            <asp:SessionParameter Name="userid" SessionField="userid" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSourceStockMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [StockMagazijn] WHERE [id] = @id" InsertCommand="INSERT INTO [StockMagazijn] ([MateriaalId], [MagazijnId], [Aantal]) VALUES (@MateriaalId, @MagazijnId, @Aantal)" SelectCommand="SELECT s.[id],[naam],b.Article,b.Description, [MateriaalId], [MagazijnId], [Aantal] as 'Totaal aantal'
,
(select count(sr.id) from Serienummer sr inner join StockMagazijn sm on sr.StockMagazijnId = sm.id where sm.MateriaalId = s.MateriaalId and (sr.statusId = 1 or sr.statusId=2) and [MagazijnId] = @MagazijnId ) as AantalKapot,
(Select omschrijving from voo.dbo.MateriaalGroep mg where mg.id =b.[MateriaalGroep]) as 'Materiaalgroep'
,[Aantal]-(select count(sr.id) from Serienummer sr inner join StockMagazijn sm on sr.StockMagazijnId = sm.id where sm.MateriaalId = s.MateriaalId and  (sr.statusId = 1 or sr.statusId=2) and [MagazijnId] = @MagazijnId ) as Beschikbaar
 FROM [StockMagazijn] s
INNER JOIN Magazijn m on s.MagazijnId = m.id 
INNER JOIN Basismateriaal b on s.MateriaalId = b.id WHERE ([MagazijnId] = @MagazijnId)
AND b.id in (select mo.materiaalId from [MateriaalOpdrachtgevers] mo where opdrachtgeverId = @opdrachtgeverId and (actief = 1 or actief is null))
"
        UpdateCommand="UPDATE [StockMagazijn] SET [MateriaalId] = @MateriaalId, [MagazijnId] = @MagazijnId, [Aantal] = @Aantal WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="MagazijnId" SessionField="MagazijnId" Type="Int32" />
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <br />

    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="80%">
        <Paddings PaddingBottom="5px" />
        <PanelCollection>
            <dx:PanelContent>
                &nbsp;<table style="width: 100%;">
                    <tr>
                        <td style="text-align: right">Bronmagazijn:</td>
                        <td>
                            <dx:ASPxComboBox AutoPostBack="true" ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" ClientInstanceName="bron">
                                <ClientSideEvents ValueChanged="function() {ASPxCallback.PerformCallback('Van'+'|'+bron.GetValue());}"></ClientSideEvents>
                            </dx:ASPxComboBox>


                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">Verbruiksmagazijn:</td>
                        <td>
                            <dx:ASPxComboBox ID="ASPxComboBoxMagazijnNaar" runat="server" DataSourceID="SqlDataSourceMagazijnenVerbruik" TextField="Naam" ValueField="id" ClientInstanceName="verbruiks">
                                <ClientSideEvents ValueChanged="function() {ASPxCallback.PerformCallback('Naar'+'|'+verbruiks.GetValue());}"></ClientSideEvents>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">Ordernummer:</td>
                        <td>
                            <dx:ASPxTextBox ID="ASPxTextBoxOrdernummer" runat="server" Width="100%"></dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Datum:">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server">
                            </dx:ASPxDateEdit>
                        </td>

                    </tr>
                    <tr>
                        <td style="text-align: right">Stad:</td>
                        <td>
                            <dx:ASPxTextBox ID="ASPxTextboxStad" runat="server" CheckState="Unchecked" ClientInstanceName="stad">
                                <ClientSideEvents ValueChanged="function() {ASPxCallback.PerformCallback('Stad'+'|'+stad.GetValue());}"></ClientSideEvents>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">Straat</td>
                        <td>
                            <dx:ASPxTextBox ID="ASPxTextboxStraat" runat="server" ClientSideEvents-ValueChanged="function() {ASPxCallback.PerformCallback('Straat'+'|'+straat.GetValue());}" CheckState="Unchecked" ClientInstanceName="straat">
                                <ClientSideEvents ValueChanged="function() {ASPxCallback.PerformCallback('Straat'+'|'+straat.GetValue());}"></ClientSideEvents>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>


                    <tr>
                        <td style="text-align: right">
                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Opmerking:">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxMemo ID="ASPxMemo1" runat="server" Height="71px" Width="525px">
                            </dx:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                </table>


            </dx:PanelContent>
        </PanelCollection>
        <Border BorderColor="Silver" BorderWidth="1px" />
    </dx:ASPxCallbackPanel>


    <dx:ASPxCallback ID="ASPxCallback" runat="server" ClientInstanceName="ASPxCallback">
    </dx:ASPxCallback>












    <asp:SqlDataSource ID="SqlDataSourceGroepen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [MateriaalGroep]">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>












    <br />
    <h3>Materiaal</h3>
    <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
    </dx:ASPxLabel>
    <dx:ASPxLabel ID="ASPxLabelSuccess" CssClass="enableMultiLine" runat="server" EncodeHtml="false" Font-Bold="True" ForeColor="#00CC00">
    </dx:ASPxLabel>



    <br />
    <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Doorsturen" ClientInstanceName="saveChangesBtn" CssClass="inline" Width="75px">
    </dx:ASPxButton>

    <br />
    <table>
        <tr>
            <td>Nulwaarden verbergen</td>
            <td>
                <dx:ASPxCheckBox ID="ASPxCheckBoxNulwaarden" ClientInstanceName="CheckNulwaarden" runat="server" CheckState="Unchecked">
                    <ClientSideEvents CheckedChanged="function(s, e) {
checkFilters();
}" />
                </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>Groepen filteren
                <dx:ASPxCheckBox ID="ASPxCheckBoxGroepen" ClientInstanceName="CheckGroepen" runat="server" CheckState="Unchecked">
                    <ClientSideEvents CheckedChanged="function(s, e) {
checkFilters();
}" />
                </dx:ASPxCheckBox>
            </td>
            <td>
                <dx:ASPxComboBox ID="ASPxComboBox2" runat="server" ClientInstanceName="GroepFilter" DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                    <ClientSideEvents ValueChanged="function(s, e) {
checkFilters();
}" />

                </dx:ASPxComboBox>
            </td>
        </tr>
        </table>
  
    
    <table>
        <tr>
            <td>&nbsp;</td>
            <td>Verbruik</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" ClientInstanceName="ASPxGridView1" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMateriauxADMIN" KeyFieldName="id">
                    <SettingsDetail ShowDetailRow="True" />
                    <Templates>
                        <DetailRow>
                            <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                                <SettingsDetail AllowOnlyOneMasterRowExpanded="True" />
                                <SettingsEditing Mode="Batch">
                                </SettingsEditing>
                                <Columns>
                                    <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Serie" FieldName="serienummer" VisibleIndex="1">
                                        <PropertiesComboBox DataSourceID="SqlDataSourceSerienummersStock" IncrementalFilteringMode="Contains" TextField="serienummer" ValueField="serienummer">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </DetailRow>
                        <StatusBar>
                            <dx:ASPxButton ID="saveChangesBtn" runat="server" Text="Commander" AutoPostBack="false"
                                RenderMode="Link" ClientInstanceName="saveChangesBtn" ClientEnabled="false">
                                <ClientSideEvents Click="saveChangesBtn_Click" />
                            </dx:ASPxButton>
                            <dx:ASPxButton ID="cancelChangesBtn" runat="server" Text="Annuler" AutoPostBack="false"
                                RenderMode="Link">
                                <ClientSideEvents Click="cancelChangesBtn_Click" />
                            </dx:ASPxButton>

                        </StatusBar>
                    </Templates>
                    <SettingsPager Mode="ShowAllRecords">
                    </SettingsPager>
                    <SettingsEditing Mode="Batch">
                        <BatchEditSettings StartEditAction="Click" />
                    </SettingsEditing>
                    <Settings ShowStatusBar="Hidden" ShowHeaderFilterButton="True" />



                    <SettingsBehavior AllowSort="False" AllowFocusedRow="True" />



                    <SettingsSearchPanel Visible="True" />
                    <ClientSideEvents BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing" />


                    <Columns>
                        <dx:GridViewCommandColumn VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="False">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="2">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="3">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Hoeveelheid" VisibleIndex="8">
                            <PropertiesTextEdit Size="100" Width="50px">
                                <MaskSettings Mask="&lt;0..9999&gt;&lt;,|.&gt;&lt;00..99&gt;" AllowMouseWheel="False" />
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="True" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="HeeftSerienummer" Visible="False" VisibleIndex="9">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Materiaalgroep 1" FieldName="MateriaalGroep" VisibleIndex="4" Visible="False">
                            <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                            </PropertiesComboBox>
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Materiaalgroep 2" FieldName="MateriaalGroep2" VisibleIndex="5" Visible="False">
                            <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                            </PropertiesComboBox>
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Materiaalgroep 3" FieldName="MateriaalGroep3" VisibleIndex="6" Visible="False">
                            <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                            </PropertiesComboBox>
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataSpinEditColumn Caption="Aantal in verbruiksmagazijn" FieldName="huidig" VisibleIndex="7">
                            <PropertiesSpinEdit DisplayFormatString="g">
                            </PropertiesSpinEdit>
                        </dx:GridViewDataSpinEditColumn>
                    </Columns>
                    <SettingsAdaptivity AdaptivityMode="HideDataCells">
                    </SettingsAdaptivity>
                    <Templates>
                        <StatusBar>
                            <dx:ASPxButton ID="saveChangesBtn" runat="server" Text="Commander" AutoPostBack="false"
                                RenderMode="Link" ClientInstanceName="saveChangesBtn" ClientEnabled="false">
                                <ClientSideEvents Click="saveChangesBtn_Click" />
                            </dx:ASPxButton>
                            <dx:ASPxButton ID="cancelChangesBtn" runat="server" Text="Annuler" AutoPostBack="false"
                                RenderMode="Link">
                                <ClientSideEvents Click="cancelChangesBtn_Click" />
                            </dx:ASPxButton>
                        </StatusBar>
                    </Templates>
                    <Styles>
                        <SelectedRow BackColor="#FFCC99">
                        </SelectedRow>
                        <FocusedRow Font-Bold="True" ForeColor="Black">
                        </FocusedRow>
                        <InlineEditRow BackColor="#FFCC99">
                        </InlineEditRow>
                    </Styles>
                </dx:ASPxGridView>
            </td>
        </tr>
    </table>

    &nbsp;<dx:ASPxButton ID="ASPxButton1" runat="server" Text="Doorsturen" ClientInstanceName="saveChangesBtn" CssClass="inline" Width="75px">
    </dx:ASPxButton>
    <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT sl.[id]
      ,[materiaalId]
	  ,m.Article
	  ,m.Description
,m.MateriaalGroep
,m.MateriaalGroep2
,m.MateriaalGroep3
      ,[VerbruikId]
      ,[hoeveelheid]  as Hoeveelheid
,m.[HeeftSerienummer],
(select aantal from stockmagazijn sm where sm.materiaalId = m.id and sm.magazijnid = @magazijnid) as huidig
  FROM [Voo].[dbo].[Verbruiklijn] sl
  inner join Basismateriaal m on sl.materiaalId = m.id
  where m.[bobijnArtikel]=0 and VerbruikId = (select top 1 id from Verbruik where gebruiker= @gebruikerId and status = 1)"
        ID="SqlDataSourceMateriauxADMIN" UpdateCommand="UPDATE [Verbruiklijn] SET  Hoeveelheid = @Hoeveelheid WHERE [id] = @id">
        <SelectParameters>
            <asp:SessionParameter Name="magazijnid" SessionField="MagazijnId" />
            <asp:SessionParameter Name="gebruikerId" SessionField="userid" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Hoeveelheid" />
            <asp:Parameter Name="id" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT sl.[id]
      ,[materiaalId]
	  ,m.Article
	  ,m.Description
      ,[VerbruikId]
      ,[hoeveelheid]  as Hoeveelheid
,m.[HeeftSerienummer],
(select aantal from stockmagazijn sm where sm.materiaalId = m.id and sm.magazijnid = @magazijnid) as huidig
  FROM [Voo].[dbo].[Verbruiklijn] sl
  inner join Basismateriaal m on sl.materiaalId = m.id
  where VerbruikId = (select top 1 id from Verbruik where gebruiker= @gebruikerId and status = 1)"
        ID="SqlDataSourceMateriauxADMIN0" UpdateCommand="UPDATE [Verbruiklijn] SET  Hoeveelheid = @Hoeveelheid WHERE [id] = @id">
        <SelectParameters>
            <asp:SessionParameter Name="magazijnid" SessionField="MagazijnId" />
            <asp:SessionParameter Name="gebruikerId" SessionField="userid" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Hoeveelheid" />
            <asp:Parameter Name="id" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <br />
    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT id, serienummer  FROM [VerbruikSerienummer] WHERE [verbruiklijnid]= @verbruiklijnid" InsertCommand="Insert into [VerbruikSerienummer] ([verbruiklijnid],[serienummer]) values(@verbruiklijnid,@serienummer);
Update [Voo].[dbo].[Verbruiklijn] set hoeveelheid = (select count(id) from [VerbruikSerienummer] where verbruiklijnId=@verbruiklijnid ) where id = @verbruiklijnid"
        UpdateCommand="Update [VerbruikSerienummer] set serienummer=@serienummer where id=@id;
Update [Voo].[dbo].[Verbruiklijn] set hoeveelheid = (select count(id) from [VerbruikSerienummer] where verbruiklijnId=@verbruiklijnid ) where id = @verbruiklijnid
">
        <InsertParameters>
            <asp:SessionParameter Name="verbruiklijnid" SessionField="serienummerlijn" />
            <asp:Parameter Name="serienummer" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="verbruiklijnid" SessionField="serienummerlijn" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="serienummer" />
            <asp:Parameter Name="id" />
            <asp:SessionParameter Name="verbruiklijnid" SessionField="serienummerlijn" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSourceSerienummersStock" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT id, serienummer  FROM [Serienummer] Where stockmagazijnid in (select id from stockmagazijn where MagazijnId = @MagazijnId)" InsertCommand="Insert into [VerbruikSerienummer] ([verbruiklijnid],[serienummer]) values(@verbruiklijnid,@serienummer);
Update [Voo].[dbo].[Verbruiklijn] set hoeveelheid = (select count(id) from [VerbruikSerienummer] where verbruiklijnId=@verbruiklijnid ) where id = @verbruiklijnid"
        UpdateCommand="Update [VerbruikSerienummer] set serienummer=@serienummer where id=@id;
Update [Voo].[dbo].[Verbruiklijn] set hoeveelheid = (select count(id) from [VerbruikSerienummer] where verbruiklijnId=@verbruiklijnid ) where id = @verbruiklijnid
">
        <InsertParameters>
            <asp:SessionParameter Name="verbruiklijnid" SessionField="serienummerlijn" />
            <asp:Parameter Name="serienummer" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="MagazijnId" SessionField="MagazijnId" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="serienummer" />
            <asp:Parameter Name="id" />
            <asp:SessionParameter Name="verbruiklijnid" SessionField="serienummerlijn" />
        </UpdateParameters>
    </asp:SqlDataSource>

</asp:Content>
