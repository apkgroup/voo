﻿
Imports System.Data.SqlClient

Public Class ArtikelPakket
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Session("level") > 5 Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Protected Sub btnNieuw_Click(sender As Object, e As EventArgs) Handles btnNieuw.Click
        If String.IsNullOrEmpty(txtPakket.Text) Then
            txtPakket.ErrorText = "Gelieve een naam in te vullen voor het pakket dat u wil aanmaken."
            txtPakket.IsValid = False
            Return
        End If
        Dim iPakket As Integer = 0
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Try
                Dim cmd As New SqlCommand("INSERT INTO Artikelpaketten (Naam, Actief) VALUES (@naam, 1); SELECT SCOPE_IDENTITY()", cn)
                Dim par As New SqlParameter("@Naam", SqlDbType.NVarChar, 50) With {.Value = txtPakket.Text}
                cmd.Parameters.Add(par)
                iPakket = cmd.ExecuteScalar
                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Artikelpakket {0} aangemaakt door {1} {2}", txtPakket.Text, Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand With {.Connection = cn, .CommandText = "prNieuwArtikelPakket", .CommandType = CommandType.StoredProcedure}
                par = New SqlParameter("@pakket", SqlDbType.Int) With {.Value = iPakket}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
                cboPakket.DataBind()
                cboPakket.Value = iPakket
                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Actieve artikels gekopieerd naar artikelpakket {0} door {1} {2}", txtPakket.Text, Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij aanmaken artikelpakket door {0} {1} : {2}", Session("naam"), Session("voornaam"), ex.Message)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End Try
            
        End Using
    End Sub

    Protected Sub callbackPanel_Callback(ByVal source As Object, ByVal e As DevExpress.Web.CallbackEventArgsBase)
        edBinaryImage.Value = FindImage(grid.GetRowValues(grid.FocusedRowIndex, "Artikel"))

        litText.Text = String.Format("{0} - {1}", grid.GetRowValuesByKeyValue(grid.GetRowValues(grid.FocusedRowIndex, "id"), "ArtikelERP"), grid.GetRowValuesByKeyValue(grid.GetRowValues(grid.FocusedRowIndex, "id"), "Omschrijving"))
    End Sub

    Private Function FindImage(ByVal id As String) As Byte()
        Dim btImage As Byte() = Nothing
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("SELECT Foto FROM stock_Artikels WHERE (id={0})", id), cn)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not dr.IsDBNull(0) Then
                    btImage = CType(dr.GetValue(0), Byte())
                End If
            End If
            dr.Close()
        End Using
        Return btImage
    End Function
    Public Function GetRender(data As Object) As String
        Try
            If data.ToString = ";" Then
                Return String.Empty
            Else
                Dim arrwaarden As String() = data.ToString.Split(New Char() {";"})
                Dim str As Int32 = Convert.ToInt32(arrwaarden(0))

                If str = 1 Then

                    Return String.Format("<a href=""javascript:void(0);"" onclick=""OnMoreInfoClick(this)"">{0}</a>", arrwaarden(1))
                Else
                    Return arrwaarden(1)
                End If
            End If


        Catch ex As Exception
            Return String.Empty
        End Try




    End Function

    Protected Sub btnConfirmatie_Click(sender As Object, e As EventArgs) Handles btnConfirmatie.Click
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Try
                Dim cmd As New SqlCommand(String.Format("DELETE FROM Artikelpaketten WHERE (id={0})", cboPakket.Value), cn)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Artikelpakket {0} verwijderd door {1} {2}", cboPakket.Text, Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()

            Catch ex As Exception
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij verwijderen artikelpakket {0} door {1} {2} : {3}", cboPakket.Text, Session("naam"), Session("voornaam"), ex.Message)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End Try
        End Using
        Response.Redirect("Artikelpakket.aspx", False)
        Context.ApplicationInstance.CompleteRequest()
    End Sub
End Class