﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="MijnBestellingen.aspx.vb" Inherits="Telecom.MijnBestellingen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu></h2> 
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceBestellingen" KeyFieldName="id">
        <SettingsPager PageSize="20">
        </SettingsPager>
        <Settings ShowHeaderFilterButton="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Werknemer" ReadOnly="True" VisibleIndex="0" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="beschrijving" VisibleIndex="1" Caption="Status" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="Datum" VisibleIndex="2" ShowInCustomizationForm="True" Caption="Datum aangevraagd">
                <Settings AllowHeaderFilter="False" ShowInFilterControl="False" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataHyperLinkColumn FieldName="id" VisibleIndex="4">
            <PropertiesHyperLinkEdit NavigateUrlFormatString="../Bestelling.aspx?id={0}" Text="details" TextFormatString="" >
            </PropertiesHyperLinkEdit>
                <Settings AllowHeaderFilter="False" ShowInFilterControl="False" />
            <EditFormSettings Visible="False" />
        </dx:GridViewDataHyperLinkColumn>
            <dx:GridViewDataTextColumn FieldName="groep" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="DatumGoedkeuring" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <br />
    <h2>&nbsp;</h2>
    <p>&nbsp;</p>
    <p>
        &nbsp;</p>
    <asp:SqlDataSource ID="SqlDataSourceBestellingen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT (
SELECT 
CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam
ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)
END
) as Werknemer, 

CASE 
WHEN [Bestellingstatus].beschrijving = 'demandé' AND @taal =1 THEN 'Aangevraagd'
WHEN [Bestellingstatus].beschrijving = 'demandé' AND @taal =2 THEN 'Demandé'
WHEN [Bestellingstatus].beschrijving = 'approuvé' AND @taal =1 THEN 'Goedgekeurd'
WHEN [Bestellingstatus].beschrijving = 'approuvé' AND @taal =2 THEN 'Approuvé'
WHEN [Bestellingstatus].beschrijving = 'rejeté' AND @taal =1 THEN 'Afgekeurd'
WHEN [Bestellingstatus].beschrijving = 'rejeté' AND @taal =2 THEN 'Rejeté'
        WHEN [Bestellingstatus].beschrijving = 'voorbereid' AND @taal =1 THEN 'Gereserveerd'
WHEN [Bestellingstatus].beschrijving = 'voorbereid' AND @taal =2 THEN 'Reservé'
END as beschrijving,

[Datum], [Bestellingen].[id],  [Bestellingen].[DatumGoedkeuring],
(select top 1 g.Naam from GebruikersgroepenLeden l inner join Gebruikersgroepen g on g.id = l.Groep where l.Gebruiker = [Bestellingen].GebruikerId) as groep 
FROM [Bestellingen]
INNER JOIN 
[Bestellingstatus] on [Bestellingstatus].id = [Bestellingen].[status] 
INNER JOIN
[Gebruikers] on [Bestellingen].GebruikerId = [Gebruikers].id
WHERE Gebruikers.id in (select g3.id from Gebruikers g3 where g3.MagazijnId= (select g2.MagazijnId from voo.dbo.gebruikers g2 where g2.id = @gebruikersid) )
AND Gebruikers.opdrachtgeverId = @opdrachtgever 
and (Gebruikers.Actief= 1 or Gebruikers.Actief is null)
ORDER BY [Datum] DESC">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="1" Name="taal" SessionField="taal" />
            <asp:SessionParameter DefaultValue="" Name="gebruikersid" SessionField="userid" />
            <asp:SessionParameter DefaultValue="" Name="opdrachtgever" SessionField="opdrachtgever" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
    </dx:ASPxGridViewExporter>
      </asp:Content>
