﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Adresboek.aspx.vb" Inherits="Telecom.Adresboek" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Adresboek</h2>   
    <br />
    <dx:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceAdresboek" ClientInstanceName="grid">
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Gebruikersgroep" VisibleIndex="0">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Werknemer" ReadOnly="True" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="GSM" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Email" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
        </Columns>
         <SettingsBehavior SortMode="DisplayText" AllowFocusedRow="True" />
       <SettingsPager Mode="ShowAllRecords" Visible="False">
        </SettingsPager>
         <Settings ShowHeaderFilterButton="True" ShowStatusBar="Hidden" />
        <SettingsText EmptyDataRow="Geen data gevonden..." />
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <Styles>           
             <FocusedRow ForeColor="Black">
            </FocusedRow>
        </Styles>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceAdresboek" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT GG.Naam As Gebruikersgroep, W.Werknemer, [GSM] ,[Email]
  FROM [dbo].[Gebruikers] G INNER JOIN (SELECT [ON],NR,NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN) W 
  ON G.Werkg=W.[ON] AND G.Werkn=W.NR 
  INNER JOIN GebruikersgroepenLeden L ON G.id=L.Gebruiker INNER JOIN Gebruikersgroepen GG ON L.Groep=GG.id 
  WHERE (isnull(G.Actief,0)=1)
  ORDER BY GG.Naam, W.Werknemer"></asp:SqlDataSource>
    <br />

</asp:Content>
