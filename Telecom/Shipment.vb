'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class Shipment
    Public Property id As Integer
    Public Property datereceived As Date
    Public Property vrijgegeven As Boolean
    Public Property uploadedby As Integer
    Public Property vrijgavedatum As Nullable(Of Date)
    Public Property shipmentNumber As String
    Public Property accepted As Nullable(Of Boolean)
    Public Property opdrachtgeverId As Nullable(Of Integer)
    Public Property magazijnId As Nullable(Of Integer)
    Public Property opmerking As String

    Public Overridable Property Magazijn As Magazijn
    Public Overridable Property Opdrachtgever As Opdrachtgever
    Public Overridable Property Serienummer As ICollection(Of Serienummer) = New HashSet(Of Serienummer)

End Class
