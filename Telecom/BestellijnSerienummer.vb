'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class BestellijnSerienummer
    Public Property id As Integer
    Public Property bestellijnId As Integer
    Public Property serienummerId As Integer

    Public Overridable Property Bestellijn As Bestellijn
    Public Overridable Property Serienummer As Serienummer

End Class
