﻿Imports System.Data.SqlClient

Public Class Fout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Foutpagina voor gebruiker {0} {1} : {2}", Session("naam"), Session("voornaam"), Request.UrlReferrer.AbsoluteUri)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()

            End Using
            Dim addresses As New List(Of String)
            addresses.Add("apk-monitoring@apkgroup.eu")
            mailGoogle("WMS Melding", "noreply@apkgroup.eu", addresses, String.Format("Foutpagina voor gebruiker {0} {1} : {2}", Session("naam"), Session("voornaam"), Request.UrlReferrer.AbsoluteUri), New Dictionary(Of String, Byte()))
        Catch ex As Exception

        End Try
   
    End Sub

End Class