'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class MateriaalOpdrachtgevers
    Public Property id As Integer
    Public Property opdrachtgeverId As Nullable(Of Integer)
    Public Property materiaalId As Nullable(Of Integer)
    Public Property min As Nullable(Of Decimal)
    Public Property max As Nullable(Of Decimal)
    Public Property minhoofd As Nullable(Of Decimal)
    Public Property maxhoofd As Nullable(Of Decimal)
    Public Property actief As Nullable(Of Boolean)
    Public Property volgorde As Nullable(Of Integer)
    Public Property locatie As String

    Public Overridable Property Basismateriaal As Basismateriaal
    Public Overridable Property Opdrachtgever As Opdrachtgever

End Class
