﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class Login
    Inherits System.Web.UI.Page

    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)
        If taalId = 1 Then
            For Each item As LayoutItem In ASPxFormLayout1.Items
                If item.Caption = "Nom d'utilisateur:" Then
                    item.Caption = "Gebruikersnaam:"
                End If
                If item.Caption = "Mot de passe:" Then
                    item.Caption = "Wachtwoord:"
                End If
            Next

        Else

        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Default.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If


        If Not Page.IsPostBack Then
            txtGebruiker.Focus()
        End If

    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click

        Dim m As New QueryStringModule
        Dim blOk As Boolean = False

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "SELECT Gebruikers.id, Gebruikers.GSM, Gebruikers.Email, Gebruikersniveaus.Niveau, Gebruikers.EersteLogin, Gebruikers.opdrachtgeverId, Gebruikers.TaalId FROM Gebruikers " _
                & "INNER JOIN Gebruikersniveaus ON Gebruikers.Niveau=Gebruikersniveaus.id " _
                & "WHERE ((Gebruikers.Actief=1) AND (Gebruikers.Login=UPPER(@login)) AND (Gebruikers.Paswoord=@paswoord)) OR ((Gebruikers.Actief=1) AND (Gebruikers.Email=UPPER(@login)) AND (Gebruikers.Paswoord=@paswoord))"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@login", SqlDbType.NVarChar, 100) With {.Value = txtGebruiker.Text.ToUpper}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@paswoord", SqlDbType.NVarChar, 150) With {.Value = m.Encrypt(txtPaswoord.Text, venc)}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                Session("userid") = dr.GetInt32(0)
                Session("isingelogd") = True
                Session("level") = IIf(dr.IsDBNull(3), 9, dr.GetInt32(3))
                If (dr.IsDBNull(2)) Then
                    Session("email") = ""
                Else
                    Session("email") = dr.GetString(2)
                End If

                If dr.IsDBNull(6) Then
                    Session("taal") = 1
                Else
                    Session("taal") = dr.GetInt32(6)
                End If

                'Session("email") = IIf(dr.IsDBNull(2), "", dr.GetString(2))
                If Session("level") = 1 Then
                    Session("isadmin") = True
                Else
                    Session("false") = True
                End If

                If Session("level") = 2 Then
                    Session("isadmin") = True
                Else
                    Session("false") = True
                End If

                If Session("level") = 3 Then
                    Session("isadmin") = True
                    Session("KLV") = True
                Else
                    Session("false") = True
                End If

                If dr.IsDBNull(4) Then
                    Session("eerstelogin") = True
                End If
                If Not dr.IsDBNull(1) Then
                    Session("gsm") = dr.GetString(1)
                End If
                Session("opdrachtgever") = dr.GetInt32(5)
                Dim opdr As Integer = Session("opdrachtgever")
                Dim voocontext As New VooEntities
                Session("TAG") = voocontext.Opdrachtgever.Find(opdr).tag
                blOk = True
            End If
            dr.Close()
            If blOk = True Then
                s_SQL = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                cmd = New SqlCommand(s_SQL, cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Login geslaagd: {0} {1} {2} {3}", Session("userid"), txtGebruiker.Text, Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            Else
                s_SQL = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                cmd = New SqlCommand(s_SQL, cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Foutieve login: {0} {1} {2} {3}", Session("userid"), txtGebruiker.Text, Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End If
        End Using

        Debug.Write(Session("eerstelogin"))
        If blOk = True Then
            'If Not String.IsNullOrEmpty(Session("url")) Then

            '    Response.Redirect("~/Default.aspx", False)
            '    Context.ApplicationInstance.CompleteRequest()
            'Else
            '    Response.Redirect("~/Default.aspx", False)
            '    Context.ApplicationInstance.CompleteRequest()
            'End If


            If (Session("isadmin") = True) Then
                Session("isingelogd") = False
                Response.Redirect("~/Beheer/OpdrachtgeverSelect.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            Else
                '      Session("pickinglijst") = 3491
                ' Response.Redirect("~/rapporten/Afdruk_Pickinglijst.aspx")
                Response.Redirect("~/Default.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
        Else
            Me.txtGebruiker.ValidationSettings.ErrorText = "Foutieve login of wachtwoord."
            Me.txtGebruiker.IsValid = False
            Me.txtPaswoord.ValidationSettings.ErrorText = "Foutieve login of wachtwoord."
            Me.txtPaswoord.IsValid = False
        End If


    End Sub

End Class