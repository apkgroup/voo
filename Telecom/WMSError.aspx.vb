﻿Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web

Public Class WMSError
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        TextContent.InnerHtml = File.ReadAllText(Server.MapPath("~/App_Data/QError.html")).Replace("ERRORMESSAGE", Session("LastErrorMsg")).Replace("STACKTRACE", Session("LastErrorStack"))

    End Sub



End Class