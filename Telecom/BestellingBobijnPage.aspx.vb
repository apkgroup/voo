﻿Imports System.Data.SqlClient
Imports DevExpress.Web
Imports Microsoft.Exchange.WebServices.Data
Imports System.IO
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.Drawing


Public Class BestellingBobijnPage


    Inherits System.Web.UI.Page
    Private headerImage As Image
    Dim vorige As Integer
    Dim volgende As Integer

    Protected json As New System.Web.Script.Serialization.JavaScriptSerializer
    Protected jsvars As New Hashtable




    Protected Sub vertaal(taalid)
        If taalid = 1 Then

            ASPxGridView1.Columns("Article").Caption = "Artikel"
            ASPxGridView1.Columns("Description").Caption = "Omschrijving"
            ASPxGridView1.Columns("hoeveelheid").Caption = "Aantal"


            Labelexport.Text = "Opgelet! Bij het klikken op exporteren moet u pop-ups in uw browser toelaten"
            ASPxButton3.Text = "Exporteren"
            ASPxButton1.Text = "Picken en overdragen"
            ASPxButton2.Text = "Afkeuren"

        Else
            ASPxGridView1.Columns("Werknemer").Caption = "Empl."
            ASPxGridView1.Columns("beschrijving").Caption = "Statut"
            ASPxGridView1.Columns("Datum").Caption = "Date"
            Labelexport.Text = "Attention! En cliquant sur Exporter vous devez autoriser les pop ups en haut de votre navigateur"


        End If
    End Sub



    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim vooentities As New VooEntities
        Dim userid As Integer = Convert.ToInt32(Session("userid"))
        Dim bestid As Integer = Convert.ToInt32(Request.QueryString("id"))
        Dim allowed As Boolean = False
        Dim opdrbestelling As Integer = Convert.ToInt32(vooentities.BestellingBobijn.Find(bestid).Gebruikers.OpdrachtgeverId)
        If Not Session("isadmin") Then
            For Each opdr In vooentities.AdminOpdrachtgevers.Where(Function(x) x.gebruikerId = userid)
                If opdr.opdrachtgeverId = opdrbestelling Then
                    allowed = True
                End If
            Next
            If Not allowed Then
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
            Session("opdrachtgever") = vooentities.BestellingBobijn.Find(bestid).Gebruikers.OpdrachtgeverId
        End If


    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        Dim serienrs As New Dictionary(Of String, String)

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        Dim entities As New VooEntities
        Dim bestid As Integer = CInt(Request.QueryString("id"))
        ASPxTextBoxReden.Value = entities.BestellingBobijn.Find(bestid).RedenAfkeuring

        Dim bobbest As Integer = CInt(Request.QueryString("id"))
        If entities.BestellingBobijn.Find(bobbest).Status = 1 Then
            If Not Page.IsPostBack Then
                Session.Remove("textboxes")
                vulbobijn()
            Else
                RestoreTxts()
            End If

        End If





        Initialiseer()



        If Not Session("isadmin") Then


            ASPxPanel2.Visible = False
            ASPxPanel1.Visible = False
            ASPxGridView1.DataColumns.Item("hoeveelheid").EditFormSettings.Visible = 1

            ASPxTextBoxReden.Visible = False
            ASPxButton6.Visible = False

        Else


            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = "SELECT min(b.id) as 'vorige', max (b.id) as 'volgende' FROM [BestellingBobijn] b inner join Gebruikers g on b.GebruikerId = g.id WHERE g.opdrachtgeverId = @opdrachtgever and b.id <> @huidig and b.[Status] = 1"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@huidig", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@opdrachtgever", SqlDbType.Int) With {.Value = CInt(Session("opdrachtgever"))}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader

                If dr.HasRows Then
                    dr.Read()
                    vorige = IIf(IsDBNull(dr.GetValue(0)), 0, dr.GetValue(0))
                    volgende = IIf(IsDBNull(dr.GetValue(1)), 0, dr.GetValue(1))
                End If
                cn.Close()

            End Using

            If volgende = 0 Then
                ASPxButtonVorige.Visible = False
                ASPxButtonVolgende.Visible = False
            Else
                If volgende < Request.QueryString("id") Then
                    ASPxButtonVolgende.Visible = False
                    vorige = volgende
                ElseIf vorige > Request.QueryString("id") Then
                    ASPxButtonVorige.Visible = False
                    volgende = vorige
                End If
            End If
        End If



        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

            cn.Open()
            Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
            cmd.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()
                Session("MagazijnId") = dr2.GetInt32(0)
            End If
            dr2.Close()
            cn.Close()
        End Using


        Dim magazijnid As Integer = Session("MagazijnId")

    End Sub
    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender
        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijnDoel_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijnDoel.PreRender
        Dim entities As New VooEntities
        Dim bestid As Integer = CInt(Request.QueryString("id"))

        If ASPxComboBoxMagazijnDoel.SelectedIndex = -1 Then
            Dim magid As String = entities.BestellingBobijn.Find(bestid).Gebruikers.MagazijnId
            ASPxComboBoxMagazijnDoel.SelectedItem = ASPxComboBoxMagazijnDoel.Items.FindByValue(magid)
        End If


    End Sub
    Protected Sub Initialiseer()

        'vulhaspel()
        Dim status As Integer
        Dim statusText As String
        LabelFoutmelding.Text = Session("Bestellingfout")
        ASPxLabelGoed.Text = Session("BestellingGelukt")
        Session("Bestellingfout") = ""
        Session("BestellingGelukt") = ""

        If Not String.IsNullOrEmpty(Request.QueryString("id")) Then






            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = "SELECT (SELECT CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn) END) as Naam, Status, deadline, opmerking FROM BestellingBobijn Inner join Gebruikers on BestellingBobijn.GebruikerId = Gebruikers.id where BestellingBobijn.id = @id"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()


                    status = dr.GetInt32(1)

                    If Session("taal") = 1 Then
                        Select Case status
                            Case 1
                                statusText = "Aangevraagd"
                            Case 3
                                statusText = "Afgekeurd"
                            Case 2
                                statusText = "Goedgekeurd"
                            Case 4
                                statusText = "Gereserveerd"
                        End Select
                        Literal2.Text = "Bobijn-Bestelling " & Request.QueryString("id") & " van " + dr.GetString(0) + ": " + statusText
                        ASPxGridView1.SettingsText.Title = "Bobijn-Bestelling: " & Request.QueryString("id") & " " & dr.GetString(0)
                        ASPxMemoOpmerking.Value = IIf(IsDBNull(dr.GetValue(3)), "", dr.GetValue(3))
                        ASPxDateEditDeadline.Value = IIf(IsDBNull(dr.GetValue(2)), "", dr.GetValue(2))
                    Else
                        Select Case status
                            Case 1
                                statusText = "demandé"
                            Case 3
                                statusText = "rejeté"
                            Case 2
                                statusText = "approuvé"
                        End Select
                        Literal2.Text = "Commande " & Request.QueryString("id") & " de " + dr.GetString(0) + ": " + statusText
                        ASPxGridView1.SettingsText.Title = "Commande: " & Request.QueryString("id") & " " & dr.GetString(0)

                    End If




                End If
                dr.Close()
            End Using

            If Not status = 1 Then
                ASPxPanel1.Visible = False

                ASPxGridView1.DataColumns.Item("hoeveelheid").EditFormSettings.Visible = 1
                ASPxLabelDoel.Visible = False
                ASPxGridView1.DataColumns.Item("hoeveelheid").EditFormSettings.Visible = 1
            Else
                ASPxPanel1.Visible = True
                ASPxComboBoxMagazijnDoel.Visible = True
                ASPxLabelDoel.Visible = True
            End If

            If Not Session("isadmin") Then
                If status = 1 Then
                    PlaceHolderHaspel.Visible = False
                End If
            End If
        End If


    End Sub
    Protected Sub ASPxButton2_Click(sender As Object, e As EventArgs) Handles ASPxButton2.Click
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "UPDATE [BestellingBobijn] SET [Status] = 3  WHERE ([BestellingBobijn].[id] = @bestellingId)"

            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteReader()
            Dim voocontext As New VooEntities

        End Using
        Dim username As String
        Dim userid As String
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim s_SQL As String = "SELECT(SELECT CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)END) as Naam, [gebruikers].id FROM [BestellingBobijn] inner join [Gebruikers] on [BestellingBobijn].[GebruikerId] = [Gebruikers].[id] where [BestellingBobijn].id = @bestellingId"

            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                username = dr.GetValue(0)
                userid = dr.GetValue(1)
            End If
            dr.Close()
        End Using

        Verwittigen(username, userid, "rejeté")

        Session("BestellingGelukt") = "Bestelling Afgekeurd"
        Response.Redirect("~/BestellingBobijnPage.aspx?id=" + Request.QueryString("id"))
        Context.ApplicationInstance.CompleteRequest()



    End Sub
    Protected Sub ASPxGridView1_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles ASPxGridView1.CustomCallback

        Dim s As ASPxGridView = sender

        If e.Parameters = "CEXP" Then

        End If

    End Sub
    Private Sub Header_CreateDetailHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        e.Graph.BorderWidth = 0
        Dim r As New Rectangle(300, 0, headerImage.Width, headerImage.Height)
        e.Graph.DrawImage(headerImage, r)
        'r = New Rectangle(0, headerImage.Height, 200, 50)
        'e.Graph.DrawString("Additional Header information here....", r)
    End Sub
    Private Sub Link1_CreateReportFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        e.Graph.BorderWidth = 0
        Dim r As New Rectangle(1, 20, 250, 50)
        e.Graph.Font = New Font("Arial", 12, FontStyle.Bold)
        e.Graph.ForeColor = Color.DimGray
        e.Graph.DrawString("Handtekening magazijnier", r)

        r = New Rectangle(350, 20, 250, 50)
        e.Graph.Font = New Font("Arial", 12, FontStyle.Bold)
        e.Graph.ForeColor = Color.DimGray
        e.Graph.DrawString("Handtekening technieker", r)

        Dim rF As New RectangleF(1, 75, 250, 100)

        e.Graph.DrawRect(rF, BorderSide.All, Color.Transparent, Color.DimGray).BorderWidth = 1
        rF = New RectangleF(350, 75, 250, 100)
        e.Graph.DrawRect(rF, BorderSide.All, Color.Transparent, Color.DimGray).BorderWidth = 1

        r = New Rectangle(350, 200, 250, 50)
        e.Graph.Font = New Font("Arial", 12, FontStyle.Bold)
        e.Graph.ForeColor = Color.DimGray
        e.Graph.DrawString("Datum: " & Today.ToShortDateString, r)

    End Sub

    Protected Sub ConfirmButton_Click(sender As Object, e As EventArgs)
        For Each ctr As System.Web.UI.Control In PlaceHolderHaspel.Controls
            If TypeOf (ctr) Is ASPxTextBox Then
                Dim value As String = (CType(ctr, TextBox)).Text
            End If
        Next
    End Sub

    Private Sub Verwittigen(ByVal varGebruiker As String, ByVal Gebruikerid As Integer, ByVal Status As String)

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim varBericht As String = ""
            Dim varOnderwerp As String = ""
            Dim varBeheerder As String = String.Format("{0} {1}", Session("naam"), Session("voornaam"))
            Dim varEmail As String = ""
            Dim s_SQL As String = "SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Melding bestelling verwerkt" & Session("taal")}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not dr.IsDBNull(0) Then
                    varBericht = dr.GetString(0)
                    varOnderwerp = dr.GetString(1)
                End If
            End If
            dr.Close()
            s_SQL = "SELECT Email FROM Gebruikers WHERE (Gebruikers.id=@gebruiker)"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Gebruikerid}
            cmd.Parameters.Add(par)
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                varEmail = dr.GetString(0)
            End If
            dr.Close()
            Dim adressen As New List(Of String)
            adressen.Add(varEmail)

            Dim varBody As String = varBericht.Replace("[WERKNEMER]", varGebruiker)
            varBody = varBody.Replace("[STATUS]", Status)
            varBody = varBody.Replace("[BEHEERDER]", varBeheerder)
            varBody = varBody.Replace("[URL]", String.Format("{0}/MijnOrders.aspx", Session("domein")))
            varBody = varBody.Replace("[SITETITEL]", "APK WMS")
            Dim sBody As String = "<x-html>" & vbCrLf
            sBody = sBody & varBody
            sBody = sBody & "</x-html>"

            ' mailGoogle(varOnderwerp, "noreply@apkgroup.eu", adressen, sBody, New Dictionary(Of String, Byte()))


        End Using
    End Sub



    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating


        Dim editingRowVisibleIndex As Integer = ASPxGridView1.FindVisibleIndexByKeyValue(e.Keys.Values(0))
        Session("bestelling") = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Poste")









    End Sub



    Protected Sub ASPxGridView4_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("bestellijnId") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub


    Protected Sub ASPxGridView1_RowUpdated(sender As Object, e As Data.ASPxDataUpdatedEventArgs) Handles ASPxGridView1.RowUpdated

        Session("isAangepast") = True

    End Sub

    Protected Sub ASPxButtonVorige_Click(sender As Object, e As EventArgs) Handles ASPxButtonVorige.Click
        Response.Redirect("~/BestellingBobijnPage.aspx?id=" & vorige, False)
    End Sub

    Protected Sub ASPxButtonVolgende_Click(sender As Object, e As EventArgs) Handles ASPxButtonVolgende.Click
        Response.Redirect("~/BestellingBobijnPage.aspx?id=" & volgende, False)
    End Sub
    Private Sub vulbobijn()

        Dim voocontext As New VooEntities
        Dim bestellingId As Integer = Request.QueryString("id")
        Dim bestelling = voocontext.BestellingBobijn.Find(bestellingId)

        For Each bestellijn In bestelling.BestellijnBobijn

            If bestellijn.Basismateriaal.bobijnArtikel And bestellijn.hoeveelheid > 0 Then
                CreateDatasource(bestellijn.bobijnmateriaalId)
                CreateTitle(Today.Date.ToShortDateString & "_" & bestellijn.bobijnmateriaalId & "_" & "1", bestellijn.bobijnmateriaalId)
                CreateTxt(Today.Date.ToShortDateString & "_" & bestellijn.bobijnmateriaalId & "_" & GenUID(bestellijn.bobijnmateriaalId), bestellijn.bobijnmateriaalId)

            End If
        Next


    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        Dim username As String
        Dim userid As String
        Dim Voocontext As New VooEntities
        Dim bestellingid As Integer = Request.QueryString("id")

        LabelFoutmelding.Text = ""




        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim s_SQL As String = "SELECT (SELECT CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn) END) as Naam, gebruikerId FROM BestellingBobijn Inner join Gebruikers on BestellingBobijn.GebruikerId = Gebruikers.id where BestellingBobijn.id = @bestellingId"

            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                username = dr.GetValue(0)
                userid = dr.GetValue(1)
            End If
            dr.Close()
        End Using

        Dim lijst = Voocontext.BestellijnBobijn.Where(Function(x) x.bobijnbestellingId = bestellingid).ToList
        Dim intuserId As Integer = CInt(userid)

        'Neem magazijn van overschreven waarde
        Dim magid As Integer = ASPxComboBoxMagazijnDoel.Value
        Dim opdrachtgeverId As Integer = Session("opdrachtgever")
        Dim hoofdmagId As Integer = Session("MagazijnId")

        Dim fouten As New List(Of String)
        Dim stockmat As StockMagazijn

        Dim matidvorig As Integer = 0

        Dim AantalPerType As New Dictionary(Of Integer, Decimal)
        Dim bobijnenlijst As New List(Of Bobijn)
        For Each ctr As System.Web.UI.Control In ASPxCallbackPanelbob.Controls
            If TypeOf (ctr) Is ASPxComboBox Then
                Dim id As String = (CType(ctr, ASPxComboBox)).ID
                Dim value As String = (CType(ctr, ASPxComboBox)).Value
                Dim parts As String() = id.Split(New Char() {"_"c})
                Dim matId As Integer = Convert.ToInt32(parts(1))
                'Hier alles doen?
                If Not String.IsNullOrEmpty(value) Then
                    If Voocontext.Bobijn.Where(Function(x) x.bobijnnummer = value).Any Then
                        Dim bobijn = Voocontext.Bobijn.Where(Function(x) x.bobijnnummer = value And x.AF = False).FirstOrDefault
                        bobijnenlijst.Add(bobijn)
                        If AantalPerType.ContainsKey(bobijn.StockMagazijn.MateriaalId) Then
                            AantalPerType(bobijn.StockMagazijn.MateriaalId) += bobijn.aantal
                        Else
                            AantalPerType.Add(bobijn.StockMagazijn.MateriaalId, bobijn.aantal)
                        End If
                    Else
                        fouten.Add("Geen actieve bobijn gevonden met nummer " & value)
                    End If
                End If




            End If

        Next
        For Each bobijn In bobijnenlijst
            If bobijnenlijst.Where(Function(x) x.id = bobijn.id).Count > 1 Then
                fouten.Add("Bobijn " & bobijn.bobijnnummer & " is meer dan 1x geselecteerd.")
            End If
        Next
        For Each bestellijn In lijst
            If AantalPerType.ContainsKey(bestellijn.bobijnmateriaalId) Then
                'Heb bobijn(en) voor deze bestellijn
                If bestellijn.hoeveelheid <= AantalPerType(bestellijn.bobijnmateriaalId) Then
                    'hoeveelheid is voldoende
                Else
                    fouten.Add("Niet genoeg meter resterend voor artikel " & bestellijn.Basismateriaal.Description & " op opgegeven bobijnen")
                End If
            Else
                fouten.Add("Geen bobijn opgegeven voor " & bestellijn.Basismateriaal.Description & "")
            End If
        Next

        For Each combi In AantalPerType
            If Not lijst.Where(Function(x) x.bobijnmateriaalId = combi.Key).Any Then
                fouten.Add("Bobijn opgegeven voor artikel niet in bestelling: " & Voocontext.Basismateriaal.Find(combi.Key).Description)

            End If
        Next


        matidvorig = 0

        If fouten.Count = 0 Then
            'Voocontext.SaveChanges()

        Else
            For Each fout In fouten
                LabelFoutmelding.Text = LabelFoutmelding.Text & fout & "<br/>"
            Next

            Session("Bestellingfout") = LabelFoutmelding.Text
            Session("BestellingGelukt") = ""
            LabelFoutmelding.Visible = True
            Return
        End If
        Dim stringbobijnen = ""
        For Each item In lijst
            Dim lijstserie As New List(Of String)
            Dim StockmatBesteller = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = item.bobijnmateriaalId).FirstOrDefault
            stringbobijnen = "<br/>" & stringbobijnen & item.Basismateriaal.Article & ":<br/>"
            For Each bobijn In bobijnenlijst.Where(Function(x) x.StockMagazijn.MateriaalId = item.bobijnmateriaalId)
                bobijn.StockMagazijn.Aantal = bobijn.StockMagazijn.Aantal - bobijn.aantal

                Dim bobijnperbest As New BobijnPerBestellijn
                bobijnperbest.bestellijnBobijnId = item.id
                bobijnperbest.bobijnId = bobijn.id
                Voocontext.BobijnPerBestellijn.Add(bobijnperbest)

                Dim sbbob As New Stockbeweging
                sbbob.beweging = -bobijn.aantal
                sbbob.datum = Today
                sbbob.gebruiker = Session("userid")
                stringbobijnen = stringbobijnen & bobijn.bobijnnummer & " met aantal meters " & bobijn.aantal & "<br/>"
                sbbob.opmerking = "Bobijn-Bestelling " & bobijn.bobijnnummer & " (van)"
                sbbob.stockmagazijnId = bobijn.StockMagazijn.id
                Voocontext.Stockbeweging.Add(sbbob)
                Voocontext.SaveChanges()

                bobijn.stockmagazijnId = StockmatBesteller.id
                StockmatBesteller.Aantal = StockmatBesteller.Aantal + bobijn.aantal

                Dim sb As New Stockbeweging
                sb.beweging = bobijn.aantal
                sb.datum = DateTime.Now
                sb.gebruiker = Session("userid")
                sb.opmerking = "Bobijn-Bestelling " & bobijn.bobijnnummer & " (naar)"
                sb.stockmagazijnId = StockmatBesteller.id
                Voocontext.Stockbeweging.Add(sb)
                Voocontext.SaveChanges()


            Next


        Next
        Voocontext.SaveChanges()


        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "UPDATE [BestellingBobijn] SET [Status] = 2, [DatumGoedkeuring]=GETDATE()  WHERE ([BestellingBobijn].[id] = @bestellingId)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteReader()

            cn.Close()

        End Using


        Dim gebruiker = Voocontext.BestellingBobijn.Where(Function(x) x.id = bestellingid).FirstOrDefault.Gebruikers
        Dim addresslist As New List(Of String)
        addresslist.Add(gebruiker.Email)
        Dim sbody As String = "Beste " & gebruiker.ExNaam & ", <br/>"
        Dim magbesteller = Voocontext.Magazijn.Where(Function(x) x.id = magid).FirstOrDefault.Naam
        sbody = sbody & "Uw bestelling voor bobijnen is goedgekeurd <br/>" & ASPxMemoOpmerking.Value & "<br/>De bestelling is naar het magazijn gezet: " & magbesteller & "  <br/> De volgende bobijnen zitten in de bestelling voor de volgende materialen: <br/>"
        sbody = sbody & stringbobijnen
        mailGoogle("Bobijnbestelling goedgekeurd", "no.reply@apkgroup.eu", addresslist, sbody, New Dictionary(Of String, Byte()))



        ASPxLabelGoed.Text = "Bestelling Goedgekeurd"
        ASPxLabelGoed.Visible = True

        ASPxCallbackPanelbob.Controls.Clear()




        Initialiseer()
        'ASPxWebControl.RedirectOnCallback(VirtualPathUtility.ToAbsolute("~/Bestelling.aspx?id=" + Request.QueryString("id")))
        'Context.ApplicationInstance.CompleteRequest()

    End Sub

    Protected Sub ASPxButton3_Click(sender As Object, e As EventArgs) Handles ASPxButton3.Click

        HideColumns(True)



        Using headerImage
            Select Case Session("opdrachtgever")
                Case 1
                    headerImage = Image.FromFile(Server.MapPath("~/images/logo_brutele.png"))
                Case 2
                    headerImage = Image.FromFile(Server.MapPath("~/images/Logo_Nethys.png"))
                Case 3
                    headerImage = Image.FromFile(Server.MapPath("~/images/fluvius.png"))
                Case 4, 1003
                    headerImage = Image.FromFile(Server.MapPath("~/images/fluvius.png"))
                Case 12
                    headerImage = Image.FromFile(Server.MapPath("~/images/nstLogo.png"))
                Case Else
                    headerImage = Image.FromFile(Server.MapPath("~/images/apklogo.png"))
            End Select

            Dim ps As New PrintingSystemBase()

            Dim link1 As New PrintableComponentLinkBase(ps)
            link1.Component = ASPxGridViewExporter1
            Dim u As New Unit(2)
            Dim margins As New Printing.Margins(10, 10, 10, 10)
            link1.Margins = margins


            Dim link2 As New PrintableComponentLinkBase(ps)
            link2.Component = ASPxGridViewExporter2
            link2.Margins = margins

            'Dim link3 As New PrintableComponentLinkBase(ps)
            'link3.Component = ASPxGridViewExporter3
            'link3.Margins = margins
            'Dim footer As New PrintableComponentLinkBase()



            Dim compositeLink As New CompositeLinkBase(ps)




            Dim header As New LinkBase()
            AddHandler header.CreateDetailHeaderArea, AddressOf Header_CreateDetailHeaderArea






            compositeLink.BreakSpace = 200

            compositeLink.Links.Add(link1)
            'compositeLink.Links.Add(link2)
            'compositeLink.Links.Add(link3)

            compositeLink.CreateDocument()



            Using stream As New MemoryStream()
                compositeLink.ExportToPdf(stream)


                ps.Dispose()

                Response.AddHeader("content-disposition", "attachment;filename=Bestelling" + Request.QueryString("id") + ".pdf")
                Response.ContentType = "application/octectstream"
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using
        End Using
        HideColumns(False)
    End Sub

    Private Sub HideColumns(v As Boolean)
        ASPxGridView1.Columns("Materiaalgroep").Visible = Not v
        ASPxGridView1.Columns("Eenheid").Visible = Not v
    End Sub




    Protected Sub ASPxButton6_Click(sender As Object, e As EventArgs) Handles ASPxButton6.Click
        Dim voocontext As New VooEntities
        Dim bestId As Integer = Request.QueryString("id")



        Dim addresslist As New List(Of String)
        addresslist.Add(voocontext.BestellingBobijn.Find(bestId).Gebruikers.Email)
        mailGoogle("Melding voor uw WMS bestelling met referentie " & bestId, "remo.ciarlariello@apkgroup.eu", addresslist, ASPxTextBoxReden.Text, New Dictionary(Of String, Byte()))
        voocontext.BestellingBobijn.Find(bestId).RedenAfkeuring = ASPxTextBoxReden.Text
        voocontext.SaveChanges()


    End Sub

    Protected Sub ASPxCallbackPanelbob_Callback(sender As Object, e As CallbackEventArgsBase) Handles ASPxCallbackPanelbob.Callback

        Dim parameters() As String = e.Parameter.Split(":"c)
        Dim bobMatId As String = parameters(0)



        Dim id As String = Today.Date.ToShortDateString & "_" & bobMatId & "_" & GenUID(bobMatId)
        CreateTxt(id, bobMatId, 1)
        'SavBobInfo(id, bobMatId)
        'RestoreTxts()
    End Sub
    Private Sub CreateDatasource(ByVal bobMatId As String)
        Dim entities As New VooEntities

        Dim ds As New SqlDataSource
        ds.SelectCommand = "SELECT [bobijnnummer], concat([bobijnnummer],' | ',b.[aantal],'m | ',DATEDIFF(day,[datumingelezen],GETDATE()),' dagen oud') as bobijnnummertext  FROM [Voo].[dbo].[Bobijn] b inner join StockMagazijn sm on b.stockmagazijnId = sm.id where b.aantal > 0 and sm.MagazijnId = @magazijnId and sm.MateriaalId=@matId and (b.af is null or b.af=0)"
        Dim opid As Integer = Session("opdrachtgever")
        Dim hoofdmagid As Integer = entities.Magazijn.Where(Function(x) x.Hoofd = True And x.OpdrachtgeverId = opid).FirstOrDefault.id
        ds.ConnectionString = ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString

        ds.SelectParameters.Add("magazijnId", hoofdmagid)
        ds.SelectParameters.Add("matId", bobMatId)

        ds.ID = "DS_" & bobMatId
        ds.DataBind()
        Dim matid As Integer = Convert.ToInt32(bobMatId)

        ASPxCallbackPanelbob.Controls.Add(ds)

    End Sub
    Private Sub CreateTitle(ByVal id As String, ByVal bobMatId As String)
        Dim labelbob As New ASPxLabel
        Dim entities As New VooEntities
        Dim matid As Integer = Convert.ToInt32(bobMatId)

        labelbob.Text = "Bobijnnummers voor " & entities.Basismateriaal.Find(matid).Description
        labelbob.ID = matid
        ASPxCallbackPanelbob.Controls.Add(labelbob)

        Dim br As New HtmlGenericControl("br")
        ASPxCallbackPanelbob.Controls.Add(br)

    End Sub
    Private Sub CreateTxt(ByVal id As String, ByVal bobMatId As String, Optional ByVal index As Integer = -1)



        Dim txt = New ASPxComboBox With {.ID = id}
        txt.Width = 300
        txt.DataSourceID = "DS_" & bobMatId
        txt.TextField = "bobijnnummertext"
        txt.ValueField = "bobijnnummer"
        txt.ClientSideEvents.ValueChanged = "function(s, e) { AddTxt(" & bobMatId & "); }"
        If index <> -1 Then
            Dim parameters = id.Split("_"c)
            Dim tevindenid = Convert.ToInt64(parameters(3))
            Dim tevindenidstring = parameters(0) & "_" & parameters(1) & "_" & parameters(2) & "_" & tevindenid - 1
            Dim indexVorige = ASPxCallbackPanelbob.Controls.IndexOf(ASPxCallbackPanelbob.FindControl(tevindenidstring))

            ASPxCallbackPanelbob.Controls.AddAt(indexVorige + 2, txt)
            Dim br As New HtmlGenericControl("br")
            ASPxCallbackPanelbob.Controls.AddAt(indexVorige + 3, br)
        Else
            'ASPxCallbackPanelbob.Controls.Add(labelbob)
            ASPxCallbackPanelbob.Controls.Add(txt)
            Dim br As New HtmlGenericControl("br")
            ASPxCallbackPanelbob.Controls.Add(br)
        End If


        SavBobInfo(txt.ID, bobMatId)
    End Sub

    Private Sub RestoreTxts()
        Dim txtsInfo = TryCast(Session("textboxes"), OrderedDictionary)
        Dim listtitels As New List(Of String)
        If txtsInfo IsNot Nothing Then
            Dim normalOrderedDictionary = txtsInfo.Cast(Of DictionaryEntry)().OrderBy(Function(r) r.Value).ToDictionary(Function(c) c.Key, Function(d) d.Value)
            For Each txtInfo In normalOrderedDictionary
                If Not listtitels.Contains(txtInfo.Value) Then
                    CreateDatasource(txtInfo.Value)
                    CreateTitle(DirectCast(txtInfo.Key, String), DirectCast(txtInfo.Value, String))
                    listtitels.Add(txtInfo.Value)
                End If
                CreateTxt(DirectCast(txtInfo.Key, String), DirectCast(txtInfo.Value, String))
            Next txtInfo
        End If
    End Sub

    Private Sub SavBobInfo(ByVal id As String, ByVal bobMatId As String)
        Dim tabsInfo = TryCast(Session("textboxes"), OrderedDictionary)

        If tabsInfo Is Nothing Then
            tabsInfo = New OrderedDictionary()
        End If
        If Not tabsInfo.Contains(id) Then
            tabsInfo.Add(id, bobMatId)
        End If


        Session("textboxes") = tabsInfo
    End Sub


    Private Function GenUID(bobid) As String
        Dim uid As Integer = CInt(If(Session(bobid & "uid"), 0))
        uid += 1
        Session(bobid & "uid") = uid

        Return "TabPageUID_" & uid
    End Function
End Class