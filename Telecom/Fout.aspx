﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Fout.aspx.vb" Inherits="Telecom.Fout" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Foutmelding</title>
    <style>
        body {
  text-align: center;
  }
        #container {
  margin: 0 auto;
  width: 600px;
  text-align: left;
  }
    </style>
</head>
<body>
    <form id="form1" runat="server">
     <div id="container">
         <table>
             <tr>
                 <td>
                        <img src="images/apklogo.png" />
                 </td>
                 <td style="vertical-align:bottom;">
                    <span style="font-family:Calibri;font-size:larger;padding:5px;"> <%=Server.HtmlEncode(Session("sitetitel"))%> Foutmelding</span>
                 </td>
             </tr>
         </table>
      
         <br /><br />
    <span style="font-family:Calibri;font-size:medium;">Er heeft zich een fout voorgedaan bij het uitvoeren van uw verzoek.<br />
         Gelieve een beheerder te contacteren en probeer zo bondig mogelijk uit te leggen wat je aan het doen was toen deze fout zich voordeed.<br />
        <ul>
            <li>
                 Klik <a href="mailto:conan.dufour@apk.be">hier </a> als u nu een e-mailbericht wil verzenden via uw standaard e-mailclient.
            </li>
            <li>
                Klik <a href="http://voo.apk.be">hier </a> om terug te keren naar de index pagina.
            </li>
        </ul>
        <br />
         
    
    </div>
    </form>
</body>
</html>
