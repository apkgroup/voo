﻿Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web
Imports Microsoft.ajax

Imports DevExpress.Utils
Public Class UploadDTO
    Public Property artikelnummer As String
    Public Property omschrijving As String
    Public Property hoeveelheid As Integer
    Public Property bobijn As Boolean
    Public Property prijs As Decimal

    Public Property eenheid As String

    Public Property locatie As String
    Public Property groep As String
    Public Property typeArtikel As String
End Class
Public Class InlezenStockFile
    Inherits System.Web.UI.Page
    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property

    'Protected Overrides Sub Initializeculture()
    '    SetCulture()
    '    MyBase.InitializeCulture()

    'End Sub
    'Private Sub SetCulture()
    '    Culture = Session("globalize").ToString
    '    UICulture = Session("globalize").ToString
    '    Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(Session("globalize").ToString)
    '    Threading.Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo(Session("globalize").ToString)
    'End Sub
    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then

            Literal2.Text = "Kies een stock file om te uploaden en druk op de upload knop. <br/>Het uploaden van het bestand kan enige tijd in beslag nemen. Gelieve de pagina ondertussen niet te sluiten.<br/>Er worden ongeveer 23 lijnen per seconde verwerkt..."


        Else

            Literal2.Text = "Materiaux"


        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If


        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer






        voocontext.Dispose()

        'If Not Session("isingelogd") Then
        '    Session("url") = Request.Url.AbsoluteUri
        '    Response.Redirect("~/Login.aspx", False)
        '    Context.ApplicationInstance.CompleteRequest()

        'End If




        If Page.IsPostBack Then
            ASPxLabelFout.Text = Session("fout")
            ASPxLabelGelukt.Text = Session("Gelukt")



            Dim i As Int32 = 1

        Else
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using
            Session("fout") = ""
            Session("Gelukt") = ""

            ASPxLabelFout.Text = Session("fout")
            ASPxLabelGelukt.Text = Session("Gelukt")
        End If
        Session("fout") = ""
        Session("Gelukt") = ""




    End Sub


    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct shipmentId from [Serienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("shipmentId"))
        Next row
        Session("SelectResult") = result
    End Sub



    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("shipment") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub


    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender

        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxFormLayout1_E1.Click
        Dim Voocontext As New VooEntities
        'For Each sku In Voocontext.Overzettenstock
        '    Voocontext.StockMagazijn.Find(sku.nieuwstockmagazijn).Aantal += sku.Aantal
        '    Voocontext.StockMagazijn.Find(sku.oudstockmagazijnId).Aantal -= sku.Aantal

        'Next
        'Voocontext.SaveChanges()
        Session("fout") = ""
        Session("Gelukt") = ""
        Dim shipmentnumber As String

        Dim sb As New StringBuilder

        Dim fouten As New List(Of String)

        'Dim stockmags = entities.StockMagazijn.ToList
        ' Dim mats = entities.Basismateriaal.ToList

        Dim opdrachtgeverId As Int32 = Convert.ToInt32(Session("opdrachtgever"))
        Dim entities As New VooEntities


        For Each f In ASPxUploadControlShipment.UploadedFiles
            Dim fi As FileInfo

            Try
                fi = New FileInfo(f.FileName)
            Catch ex As Exception

                fouten.Add("Geen shipment file gekozen")
                For Each fout In fouten
                    Session("fout") = Session("fout") + fout + "<br/>"
                Next
                ASPxLabelFout.Text = Session("fout")
                ASPxLabelGelukt.Text = Session("Gelukt")
                Return

            End Try



            Dim artikelen As New List(Of UploadDTO)

            Dim wb As New DevExpress.Spreadsheet.Workbook
            wb.LoadDocument(f.FileBytes)
            Dim ws As DevExpress.Spreadsheet.Worksheet = wb.Worksheets.FirstOrDefault
            Dim bereik As String = ws.GetUsedRange.GetReferenceA1.Replace("A1:", "A2:")
            Dim range As DevExpress.Spreadsheet.CellRange = ws.Range(bereik)
            Dim test = range.RowCount


            For i As Integer = 2 To range.RowCount
                Try


                    Dim artikeldto As New UploadDTO
                    artikeldto.artikelnummer = ws.Cells("C" + i.ToString).Value.ToString
                    artikeldto.omschrijving = ws.Cells("A" + i.ToString).Value.ToString


                    artikeldto.hoeveelheid = 0
                    artikeldto.groep = "WATER"
                    artikeldto.prijs = Decimal.Parse(ws.Cells("B" + i.ToString).Value.ToString)


                    artikeldto.bobijn = False


                    artikelen.Add(artikeldto)

                Catch ex As Exception

                End Try
            Next


            Dim bmatid As Integer

            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()


                'Also verzamel alle magazijnen, hebben we vaker nodig
                Dim magazijnIdList As New List(Of Integer)
                Dim s_SQL As String = "select  id from voo.dbo.Magazijn where OpdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = opdrachtgeverId}
                cmd.Parameters.Add(par)
                Dim dr4 As SqlDataReader = cmd.ExecuteReader
                If dr4.HasRows Then
                    Do While dr4.Read
                        If Not dr4.IsDBNull(0) Then
                            magazijnIdList.Add(dr4.GetInt32(0))
                        End If
                    Loop
                    dr4.Close()

                End If
                For Each artikel In artikelen



                    'Bestaat het al?
                    Dim matId = 0
                    s_SQL = "select top 1 id from voo.dbo.Basismateriaal where Article = @Article"
                    cmd = New SqlCommand(s_SQL, cn)
                    par = New SqlParameter("@Article", SqlDbType.NVarChar) With {.Value = artikel.artikelnummer}
                    cmd.Parameters.Add(par)
                    Dim dr2 As SqlDataReader = cmd.ExecuteReader
                    If dr2.HasRows Then
                        dr2.Read()
                        matId = dr2.GetInt32(0)

                    End If
                    dr2.Close()
                    If matId <> 0 Then
                        'Als het bestaat, is het gelinkt?
                        Dim blbestaatonderopdrachtgever As Boolean = False
                        s_SQL = "select top 1 id from voo.dbo.MateriaalOpdrachtgevers where materiaalId = @matId and opdrachtgeverId = @opdrachtgeverId"
                        cmd = New SqlCommand(s_SQL, cn)
                        par = New SqlParameter("@matId", SqlDbType.Int) With {.Value = matId}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = opdrachtgeverId}
                        cmd.Parameters.Add(par)
                        Dim dr3 As SqlDataReader = cmd.ExecuteReader
                        If dr3.HasRows Then
                            blbestaatonderopdrachtgever = True
                        End If
                        dr3.Close()
                        If blbestaatonderopdrachtgever Then
                            'als het gelinkt id, verzamel alle magazijnen


                            For Each magazijnid In magazijnIdList
                                Dim blbestaatStockMagazijnLijn As Boolean = False
                                'bestaat het al in het magazijn?
                                s_SQL = "select top 1 id from voo.dbo.StockMagazijn where materiaalId = @matId and magazijnId = @magId"
                                cmd = New SqlCommand(s_SQL, cn)
                                par = New SqlParameter("@matId", SqlDbType.Int) With {.Value = matId}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@magId", SqlDbType.Int) With {.Value = magazijnid}
                                cmd.Parameters.Add(par)
                                Dim dr5 As SqlDataReader = cmd.ExecuteReader
                                If dr5.HasRows Then
                                    blbestaatStockMagazijnLijn = True
                                End If
                                dr5.Close()

                                If blbestaatStockMagazijnLijn Then
                                    'bestaat al, alles is oke
                                Else
                                    'maak het aan in het magazijn
                                    s_SQL = "INSERT INTO voo.dbo.StockMagazijn (MateriaalId,MagazijnId,Aantal) VALUES (@MateriaalId,@MagazijnId,0)"
                                    cmd = New SqlCommand(s_SQL, cn)
                                    par = New SqlParameter("@MateriaalId", SqlDbType.Int) With {.Value = matId}
                                    cmd.Parameters.Add(par)
                                    par = New SqlParameter("@MagazijnId", SqlDbType.Int) With {.Value = magazijnid}
                                    cmd.Parameters.Add(par)
                                    cmd.ExecuteNonQuery()
                                End If
                            Next
                        Else
                            ' Is nog niet gelinkt, eerst linken
                            s_SQL = "INSERT INTO voo.dbo.MateriaalOpdrachtgevers (materiaalId,min,max, minhoofd, maxhoofd, actief, [opdrachtgeverId]) VALUES (@materiaalId,0,0, 0, 0, 1, @opdrachtgeverId)"
                            cmd = New SqlCommand(s_SQL, cn)
                            par = New SqlParameter("@MateriaalId", SqlDbType.Int) With {.Value = matId}

                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = opdrachtgeverId}
                            cmd.Parameters.Add(par)
                            cmd.ExecuteNonQuery()


                            For Each magazijnid In magazijnIdList
                                Dim blbestaatStockMagazijnLijn As Boolean = False
                                'bestaat het al in het magazijn?
                                s_SQL = "select top 1 id from voo.dbo.StockMagazijn where materiaalId = @matId and magazijnId = @magId"
                                cmd = New SqlCommand(s_SQL, cn)
                                par = New SqlParameter("@matId", SqlDbType.Int) With {.Value = matId}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@magId", SqlDbType.Int) With {.Value = magazijnid}
                                cmd.Parameters.Add(par)
                                Dim dr5 As SqlDataReader = cmd.ExecuteReader
                                If dr5.HasRows Then
                                    blbestaatStockMagazijnLijn = True
                                End If
                                dr5.Close()

                                If blbestaatStockMagazijnLijn Then
                                    'bestaat al, alles is oke
                                Else
                                    'maak het aan in het magazijn
                                    s_SQL = "INSERT INTO voo.dbo.StockMagazijn (MateriaalId,MagazijnId,Aantal) VALUES (@MateriaalId,@MagazijnId,0)"
                                    cmd = New SqlCommand(s_SQL, cn)
                                    par = New SqlParameter("@MateriaalId", SqlDbType.Int) With {.Value = matId}
                                    cmd.Parameters.Add(par)
                                    par = New SqlParameter("@MagazijnId", SqlDbType.Int) With {.Value = magazijnid}
                                    cmd.Parameters.Add(par)
                                    cmd.ExecuteNonQuery()
                                End If
                            Next



                        End If

                    Else
                        'Bestaat niet
                        Dim basis As New Basismateriaal
                        s_SQL = "INSERT INTO voo.dbo.Basismateriaal ([Article] ,[Description] ,[actief] ,[HeeftSerienummer] ,[min] ,[max] ,[bobijnArtikel]  ,[MateriaalGroep] ,[eenheid],[prijs]) VALUES (@Article ,@Description ,1 ,0 ,0 ,0 ,0  ,@MateriaalGroep ,@eenheid,@prijs);select CAST(scope_identity() AS int);"
                        cmd = New SqlCommand(s_SQL, cn)

                        par = New SqlParameter("@Article", SqlDbType.NVarChar) With {.Value = artikel.artikelnummer}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@Description", SqlDbType.NVarChar) With {.Value = artikel.omschrijving}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@MateriaalGroep", SqlDbType.Int) With {.Value = 35}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@prijs", SqlDbType.Int) With {.Value = artikel.prijs}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@eenheid", SqlDbType.NVarChar) With {.Value = "Stuk"}
                        cmd.Parameters.Add(par)


                        matId = cmd.ExecuteScalar()


                        s_SQL = "INSERT INTO voo.dbo.MateriaalOpdrachtgevers (materiaalId,min,max, minhoofd, maxhoofd, actief, [opdrachtgeverId]) VALUES (@materiaalId,0,0, 0, 0, 1, @opdrachtgeverId)"
                        cmd = New SqlCommand(s_SQL, cn)
                        par = New SqlParameter("@MateriaalId", SqlDbType.Int) With {.Value = matId}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = opdrachtgeverId}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()


                        For Each magazijnid In magazijnIdList
                            Dim blbestaatStockMagazijnLijn As Boolean = False
                            'bestaat het al in het magazijn?
                            s_SQL = "select top 1 id from voo.dbo.StockMagazijn where materiaalId = @matId and magazijnId = @magId"
                            cmd = New SqlCommand(s_SQL, cn)
                            par = New SqlParameter("@matId", SqlDbType.Int) With {.Value = matId}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@magId", SqlDbType.Int) With {.Value = magazijnid}
                            cmd.Parameters.Add(par)
                            Dim dr5 As SqlDataReader = cmd.ExecuteReader
                            If dr5.HasRows Then
                                blbestaatStockMagazijnLijn = True
                            End If
                            dr5.Close()

                            If blbestaatStockMagazijnLijn Then
                                'bestaat al, alles is oke
                            Else
                                'maak het aan in het magazijn
                                s_SQL = "INSERT INTO voo.dbo.StockMagazijn (MateriaalId,MagazijnId,Aantal) VALUES (@MateriaalId,@MagazijnId,0)"
                                cmd = New SqlCommand(s_SQL, cn)
                                par = New SqlParameter("@MateriaalId", SqlDbType.Int) With {.Value = matId}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@MagazijnId", SqlDbType.Int) With {.Value = magazijnid}
                                cmd.Parameters.Add(par)
                                cmd.ExecuteNonQuery()
                            End If
                        Next
                    End If
                Next
                cn.Close()
            End Using
            entities.Dispose()
            If fouten.Count = 0 Then



                Response.Redirect("~/Beheer/ArtikelenBeheer.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            Else
                For Each fout In fouten
                    Session("fout") = Session("fout") + fout + "<br/>"
                Next

            End If




        Next

        ASPxLabelFout.Text = Session("fout")
        ASPxLabelGelukt.Text = Session("Gelukt")
    End Sub


End Class