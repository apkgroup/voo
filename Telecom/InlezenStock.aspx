﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="InlezenStock.aspx.vb" Inherits="Telecom.InlezenStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #TextArea1 {
            height: 51px;
            width: 102px;
        }


        .auto-style1 {
            width: 48px;
        }

        .auto-style2 {
            width: 299px;
        }

        .auto-style3 {
            width: 37px;
        }
        .auto-style4 {
            height: 46px;
        }
    </style>
    <script>
        var currentRowIndex = undefined;
        var currentRowIndex2 = undefined;

    </script>
    <script type="text/javascript">






        window.onload = function () {

            console.log("onload");
            $("input[name*='Serienummer']").each(function () {
                console.log(this.id);
                if (this.id.split("_").pop() != document.getElementById("ctl00_ContentPlaceHolder1_ASPxSpinEditAantal_I").value) {
                    this.removeAttribute("onchange");
                }
                else {
                    this.focus();
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_ASPxSpinEditAantal_I").value == 0) {
                    console.log("Het is 0!");
                    document.getElementById("ctl00_ContentPlaceHolder1_PanelSerie").style.display = "none";
                }

            });


            $('input').keypress(function (e) {
                console.log("KEYPRESS");
                if (e.which == 13) {
                    e.preventDefault();
                    console.log("ISENTER");
                    var self = $(this)
                    var form = self.parents('form:eq(0)');
                    var focusable;
                    var next;
                    var prev;

                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {

                    }
                    return false;

                }
            });

            ASPxGridView2.Refresh();
            var i;

            $('input').keypress(function (e) {
                console.log("KEYPRESS");
                if (e.which == 13) {
                    e.preventDefault();
                    console.log("ISENTER");
                    var self = $(this)
                    var form = self.parents('form:eq(0)');
                    var focusable;
                    var next;
                    var prev;

                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {

                    }
                    return false;

                }
            });
        };

        var timerHandle = -1;
        var indexrow
        function OnBatchEditStartEditing(s, e) {
            currentRowIndex = e.visibleIndex;
            currentRowIndex2 = e.visibleIndex;
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("Nombre");
        }

        function OnEndCallBack(s, e) {
            if (s.cpIsUpdated != '') {


                clientText.GetMainElement().style.display = 'block';
                console.log('Waarde komt uit');
                //console.log(e); 
                //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);
                console.log('Temps de Regie updated! Waarde is...');
                //console.log(e); 
                //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);

                console.log(clientButton);
                //console.log(Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML));
                if (ASPxGridView1.GetRow(indexrow).children[3].firstElementChild == null) {
                    if (Number(ASPxGridView1.GetRow(indexrow).children[3].innerHTML.replace(",", ".")) > 0) {
                        console.log("enabled=false");
                        clientButton.SetEnabled(false);
                        clientLabel2.GetMainElement().style.display = 'block'
                    }
                    else {
                        console.log("enabled=true");
                        clientButton.SetEnabled(true);
                        clientLabel2.GetMainElement().style.display = 'none'
                    }
                }
                else {
                    if (Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML.replace(",", ".")) > 0) {
                        console.log("enabled=false");
                        clientButton.SetEnabled(false);
                        clientLabel2.GetMainElement().style.display = 'block'
                    }
                    else {
                        console.log("enabled=true");
                        clientButton.SetEnabled(true);
                        clientLabel2.GetMainElement().style.display = 'none'
                    }
                }


                //console.log(s.cpIsUpdated);


            }
            else {
                clientText.SetText('');
                console.log('Anders updated');
            }
        }

        function OnKeyDown(s, e) {
            var key = e.htmlEvent.keyCode;
            console.log("key pressed!");
            if (key == 38 || key == 40) {
                console.log("Up or down key pressed!");
                ASPxClientUtils.PreventEvent(e.htmlEvent);
                ASPxGridView1.batchEditApi.EndEdit();
                console.log(currentRowIndex);
                if (currentRowIndex !== undefined && currentRowIndex !== null) {
                    var visibleIndices = ASPxGridView1.batchEditApi.GetRowVisibleIndices();
                    var indicesArrayLength = visibleIndices.length;
                    var rowIndex = visibleIndices.indexOf(currentRowIndex);
                    if (!(rowIndex === 0 && key === 38) || !(rowIndex === indicesArrayLength - 1 && key === 40)) {
                        switch (key) {
                            case 38:
                                ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex - 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            case 40:
                                ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex + 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        function OnKeyDownMat(s, e) {
            var key = e.htmlEvent.keyCode;
            console.log("key pressed!");
            if (key == 38 || key == 40) {
                console.log("Up or down key pressed");
                ASPxClientUtils.PreventEvent(e.htmlEvent);
                ASPxGridView2.batchEditApi.EndEdit();
                console.log(currentRowIndex2);
                if (currentRowIndex2 !== undefined && currentRowIndex2 !== null) {
                    var visibleIndices2 = ASPxGridView2.batchEditApi.GetRowVisibleIndices();
                    var indicesArrayLength2 = visibleIndices2.length;
                    var rowIndex2 = visibleIndices2.indexOf(currentRowIndex2);
                    if (!(rowIndex2 === 0 && key === 38) || !(rowIndex2 === indicesArrayLength2 - 1 && key === 40)) {
                        console.log("ERIN");
                        switch (key) {
                            case 38:
                                ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 - 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            case 40:
                                ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 + 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }



        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);

            indexrow = e.visibleIndex;
            console.log("Index row set to " + indexrow);
            //if (s.cpIsUpdated != '') {

            //    console.log(clientText.GetMainElement());
            //    console.log(clientText);
            //    clientText.GetMainElement().style.display = 'none';
            //    console.log('Temps de Regie updated! Waarde is...');
            //    console.log(e);
            //    //console.log(ASPxGridView1.GetRow(e.visibleIndex).children[3].innerHTML);
            //    console.log(clientButton);
            //    clientButton.setEnabled(true);
            //    console.log(s.cpIsUpdated);


            //}
            //else {
            //    clientText.SetText('');
            //    console.log('Anders updated');
            //}

        }

        function saveChangesBtn_Click(s, e) {
            ASPxGridView1.UpdateEdit();
            ASPxGridView2.UpdateEdit();
        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();
            ASPxGridView2.CancelEdit();
        }




        function saveChangesBtnMat_Click(s, e) {
            ASPxGridView2.UpdateEdit();
        }

        function cancelChangesBtnMat_Click(s, e) {
            ASPxGridView2.CancelEdit();



        }


        function insertArticle() {
            console.log("IETest");
            Callback1.PerformCallback();
            console.log("IEAfterCallback");
        }



        function OnCallbackComplete(s, e) {
            console.log("cbCOMPLET");
            ASPxGridView2.Refresh();
            //location.reload();

        }

        function ZoekArtikel(s, e) {
            
        
            for (let i = 0; i < comboboxArtikel.GetItemCount(); i++) {
                if (comboboxArtikel.GetItem(i).text.split("|")[0].trim() == ASPxTextBoxScan.GetText()) {
                    console.log("DitIsHem");
                    comboboxArtikel.SetSelectedIndex(i);
                }
            }

        }




    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Stock</h2>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId and (g.Actief = 1 or hoofd=1 or type is not null) )">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
    <p>
        &nbsp; 
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </p>

    <dx:ASPxLabel ID="ASPxLabelGelukt" runat="server" Text="ASPxLabelOK" Font-Bold="True" ForeColor="#009900">
    </dx:ASPxLabel>

    <p></p>







    <table id="TableArtikel" runat="server">
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabelScan" runat="server" Text="Scan hier om te zoeken:">
                </dx:ASPxLabel>
            </td>

            <td class="auto-style2">
                <dx:ASPxTextBox ID="ASPxTextBoxScan" runat="server" Width="323px" ClientInstanceName="ASPxTextBoxScan">
                    <ClientSideEvents TextChanged="ZoekArtikel" />
                </dx:ASPxTextBox>




            </td>
            <td class="auto-style1"></td>
            <td class="auto-style3"></td>
        </tr>

        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Article">
                </dx:ASPxLabel>
            </td>

            <td class="auto-style2">
                <dx:ASPxHiddenField ID="HiddenArtikel" runat="server" ClientInstanceName="HiddenArtikel">
                </dx:ASPxHiddenField>

                <dx:ASPxComboBox ID="ASPxComboBoxArtikel" runat="server" AutoPostBack="True" ClientInstanceName="comboboxArtikel" DataSourceID="SqlDataSourceArtikelen" DropDownWidth="450px" Height="20px" ValueField="id" Width="323px" TextField="naam">
                    <Columns>
                        <dx:ListBoxColumn FieldName="naam" Name="0" Width="50px">
                        </dx:ListBoxColumn>

                    </Columns>
                </dx:ASPxComboBox>


            </td>
            <td class="auto-style1">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Nombre">
                </dx:ASPxLabel>
            </td>
            <td class="auto-style3">

                <dx:ASPxSpinEdit ID="ASPxSpinEditAantal" runat="server" AutoPostBack="True" MaxValue="9999999" Number="0" Width="97px">
                </dx:ASPxSpinEdit>

            </td>
        </tr>
        <tr>
            <td colspan="3">
                <dx:ASPxLabel ID="ASPxLabelLotnummer" runat="server" Text="Lotnummer" Visible="False">
                </dx:ASPxLabel>
                <dx:ASPxTextBox ID="ASPxTextBoxLotnummer" runat="server" Visible="False" Width="170px">
                </dx:ASPxTextBox>
            </td>    
            <td>
                 <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Locatie" Visible="True">
                </dx:ASPxLabel>
                 <dx:ASPxComboBox ID="ASPxComboBox2" runat="server" DataSourceID="SqlDataSourceLocaties" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
                 <asp:SqlDataSource ID="SqlDataSourceLocaties" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="select naam, id from voo.dbo.locatie where magazijnid = @magazijnid">
                     <SelectParameters>
                         <asp:SessionParameter Name="magazijnid" SessionField="MagazijnId" />
                     </SelectParameters>
                 </asp:SqlDataSource>
            </td>
            <td class="auto-style3">
                &nbsp;</td>
        </tr>
    </table>

    <table id="TableSerie">
        <tr>
            <td colspan="2" class="auto-style4">

                <asp:PlaceHolder ID="PlaceHolderSerie" runat="server"></asp:PlaceHolder>

                <asp:Panel ID="PanelSerie" runat="server">
                </asp:Panel>

            </td>
        </tr>
        <tr>
            <td>

                <dx:ASPxButton ID="ASPxButtonToevoegen" runat="server" Width="50px">

                    <Image IconID="actions_add_32x32office2013">
                    </Image>
                </dx:ASPxButton>
            </td>
            <td>&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" Text="ASPxLabel" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
                </dx:ASPxLabel>
            </td>
        </tr>
    </table>





    <dx:ASPxGridView ID="ASPxGridView2" ClientInstanceName="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMateriaux" KeyFieldName="id">

        <SettingsDetail ShowDetailRow="True" />
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView3" ClientInstanceName="ASPxGridView3" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                    <SettingsSearchPanel Visible="True" />
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="StockMagazijnId" Visible="False" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Numéro de série" FieldName="serienummer" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="uitgeboekt" Visible="False" VisibleIndex="3">
                        </dx:GridViewDataCheckColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>

        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsEditing Mode="Batch">
            <BatchEditSettings StartEditAction="Click" />
        </SettingsEditing>
        <Settings ShowStatusBar="Hidden" />
        <SettingsDataSecurity AllowDelete="False" AllowInsert="False" AllowEdit="False" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ShowInCustomizationForm="True" VisibleIndex="0" ReadOnly="True" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="naam" ShowInCustomizationForm="True" VisibleIndex="1" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" ShowInCustomizationForm="True" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MateriaalId" ShowInCustomizationForm="True" VisibleIndex="4" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MagazijnId" ShowInCustomizationForm="True" Visible="False" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Nombre" FieldName="Aantal" ShowInCustomizationForm="True" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>

    <asp:SqlDataSource ID="SqlDataSourceArtikelen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT distinct article, description, CONCAT(article, ' | ', description) as naam, b.id FROM [Basismateriaal] b
INNER JOIN MateriaalOpdrachtgevers o ON b.id =o.materiaalId
WHERE o.opdrachtgeverId = @OpdrachtgeverId and (o.actief = 1 or o.actief is null) and (b.bobijnartikel is null or b.bobijnartikel = 0)">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" DefaultValue="1" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceMateriaux" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [VOO_Materiaal] WHERE [id] = @id" InsertCommand="INSERT INTO [VOO_Materiaal] ([Werk], [Article], [Description], [Nombre]) VALUES (@Werk, @Article, @Description, @Nombre)" SelectCommand="SELECT distinct s.[id],[naam],b.Article,b.Description, s.[MateriaalId], [MagazijnId], [Aantal] FROM [StockMagazijn] s
INNER JOIN Magazijn m on s.MagazijnId = m.id 
INNER JOIN Basismateriaal b on s.MateriaalId = b.id 
INNER JOIN  [Voo].[dbo].[MateriaalOpdrachtgevers] mo on mo.opdrachtgeverId = m.opdrachtgeverId and mo.materiaalId = b.id
WHERE ([MagazijnId] = @MagazijnId) and (mo.actief = 1 or mo.actief is null) and (b.bobijnartikel is null or b.bobijnartikel = 0)"
        UpdateCommand="UPDATE [VOO_Materiaal] SET  [Nombre] = @Nombre WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Werk" Type="Int32" />
            <asp:Parameter Name="Article" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Nombre" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ASPxComboBoxMagazijn" Name="MagazijnId" PropertyName="Value" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Nombre" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>


    <br />
    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Serienummer] WHERE ([StockMagazijnId] = @StockMagazijnId) and (uitgeboekt &lt;&gt; 1)">
        <SelectParameters>
            <asp:SessionParameter Name="StockMagazijnId" SessionField="stockmagazijn" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceStockMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [StockMagazijn] WHERE [id] = @id" InsertCommand="INSERT INTO [StockMagazijn] ([MateriaalId], [MagazijnId], [Aantal]) VALUES (@MateriaalId, @MagazijnId, @Aantal)" SelectCommand="SELECT s.[id],[naam],b.Article,b.Description, [MateriaalId], [MagazijnId], [Aantal] FROM [StockMagazijn] s
INNER JOIN Magazijn m on s.MagazijnId = m.id 
INNER JOIN Basismateriaal b on s.MateriaalId = b.id WHERE ([MagazijnId] = @MagazijnId)"
        UpdateCommand="UPDATE [StockMagazijn] SET [MateriaalId] = @MateriaalId, [MagazijnId] = @MagazijnId, [Aantal] = @Aantal WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="MagazijnId" SessionField="MagazijnId" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    &nbsp;

</asp:Content>
