﻿Imports System.Data.SqlClient
Imports DevExpress.Spreadsheet
Imports DevExpress.Web

Imports EntityFramework.Extensions
Imports Microsoft.ajax

Public Class AanpassenStockViaSheet
    Inherits System.Web.UI.Page
    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property

    'Protected Overrides Sub Initializeculture()
    '    SetCulture()
    '    MyBase.InitializeCulture()

    'End Sub
    'Private Sub SetCulture()
    '    Culture = Session("globalize").ToString
    '    UICulture = Session("globalize").ToString
    '    Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(Session("globalize").ToString)
    '    Threading.Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo(Session("globalize").ToString)
    'End Sub
    Protected Sub vertaal(taalid As Integer)


    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))


        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer
        Dim worksheet As Worksheet


        worksheet = ASPxSpreadsheet1.Document.Worksheets(0)
        worksheet.Cells("A1").Value = "Artikelnummer"
        worksheet.Cells("B1").Value = "Hoeveelheid"
        worksheet.Cells("C1").Value = "TechniekerId"


        worksheet.Columns().AutoFit(0, 3)





        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()

        End If


        Dim opId As Integer = Session("opdrachtgever")



        voocontext.Dispose()







    End Sub




    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("stockmagazijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub





    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click

        Dim fouten As New List(Of String)
        Using context As New VooEntities()

            Using transaction = context.Database.BeginTransaction()



                Try


                    Dim worksheet As Worksheet


                    worksheet = ASPxSpreadsheet1.Document.Worksheets(0)
                    Dim usedRange As CellRange = worksheet.GetUsedRange()





                    Dim opId As Integer = Session("opdrachtgever")




                    context.SaveChanges()


                    For i As Integer = 1 To usedRange.RowCount - 1
                        Dim Artikelcell As Cell = usedRange(i, 0)
                        Dim HoeveelheidCell As Cell = usedRange(i, 1)
                        Dim Techniekercell As Cell = usedRange(i, 2)

                        Dim aantal As Decimal = HoeveelheidCell.Value.NumericValue
                        Dim Artikelnummer As String = Artikelcell.Value.ToString
                        Dim techId As Integer = Techniekercell.Value.NumericValue
                        Dim Tech As Gebruikers
                        If techId = 0 Then
                            Continue For
                        End If


                        Tech = context.Gebruikers.Find(techId)
                        If Tech Is Nothing Then
                            fouten.Add("Technieker niet gevonden voor id  " & techId)

                        End If

                        If Not HoeveelheidCell.Value.IsEmpty Then
                            If Artikelcell.Value.IsEmpty Then
                                fouten.Add("geen hoeveelheid voor " & Artikelnummer)

                            End If
                        End If


                        Dim materiaal As Basismateriaal
                        Dim materiaallist = context.Basismateriaal.Where(Function(x) x.Article = Artikelnummer).ToList
                        If materiaallist.Count > 1 Then
                            For Each mat In materiaallist
                                If context.MateriaalOpdrachtgevers.Where(Function(x) x.opdrachtgeverId = opId And x.materiaalId = mat.id).Any Then
                                    materiaal = context.Basismateriaal.Find(context.MateriaalOpdrachtgevers.Where(Function(x) x.opdrachtgeverId = opId And x.materiaalId = mat.id).FirstOrDefault.materiaalId)
                                End If
                            Next

                        ElseIf materiaallist.Count = 0 Then

                        Else
                            materiaal = materiaallist.FirstOrDefault
                        End If

                        If materiaal Is Nothing Then
                            fouten.Add("geen materiaal voor " & Artikelnummer)
                            Continue For
                        End If

                        If materiaal.HeeftSerienummer Then
                            fouten.Add("materiaal " & Artikelnummer & " is serienummermateriaal")
                            Continue For
                        End If

                        If materiaal.bobijnArtikel Then
                            fouten.Add("materiaal " & Artikelnummer & " is bobijnmateriaal")
                            Continue For
                        End If

                        Dim sm = context.StockMagazijn.Where(Function(x) x.MagazijnId = Tech.MagazijnId And x.MateriaalId = materiaal.id).FirstOrDefault
                        If sm Is Nothing Then
                            fouten.Add("geen stock-magazijn combinatie voor " & Artikelnummer & " en " & Tech.ExNaam)
                            Continue For

                        End If
                        sm.Aantal = sm.Aantal + aantal
                        Dim sb As New Stockbeweging
                        sb.stockmagazijnId = sm.id
                        sb.opmerking = "Stock aanpassen via sheet"
                        sb.gebruiker = Session("userid")
                        sb.beweging = aantal
                        sb.datum = DateTime.Now
                        context.Stockbeweging.Add(sb)
                        context.SaveChanges()



                    Next



                    'begin



                Catch ex As Exception
                    transaction.Rollback()
                    ASPxLabelFout.ForeColor = Drawing.Color.Red
                    ASPxLabelFout.Text = "Er is een fout opgetreden bij het inlezen van de sheet: " & ex.Message
                    Return

                End Try

                If fouten.Any Then
                    transaction.Rollback()
                    ASPxLabelFout.ForeColor = Drawing.Color.Red
                    For Each fout In fouten
                        ASPxLabelFout.Text = ASPxLabelFout.Text & fout & "<br/>"

                    Next
                    Return
                Else
                    ASPxLabelFout.ForeColor = Drawing.Color.Green
                    ASPxLabelFout.Text = "Successvol ingelezen"
                End If


                'transaction.Commit()
                context.SaveChanges()

                transaction.Commit()


            End Using
        End Using



    End Sub

    Protected Sub ASPxCallback1_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback1.Callback

    End Sub
End Class