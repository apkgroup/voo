﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class WachtwoordWijzigen
    Inherits System.Web.UI.Page

    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"

    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then
            Literal2.Text = "Wachtwoord wijzigen"
            For Each item As LayoutGroup In ASPxFormLayout1.Items
                If item.Caption = "Changement mot de passe" Then
                    item.Caption = "Nieuw wachtwoord instellen"
                End If
                For Each subitem As LayoutItem In item.Items
                    If subitem.Caption = "Nouveau mot de passe" Then
                        subitem.Caption = "Nieuw wachtwoord"
                    End If
                    If subitem.Caption = "Repeter nouveau mot de passe" Then
                        subitem.Caption = "Herhaal nieuw wachtwoord"
                    End If
                Next
            Next
            btnBevestigen.Text = "Bevestigen"
        Else

            Literal2.Text = "Changement mot de passe"


        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Protected Sub btnBevestigen_Click(sender As Object, e As EventArgs) Handles btnBevestigen.Click
        If Not Session("isingelogd") Then

            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Dim blok As Boolean = False

        If txtWachtwoord1.Text <> txtWachtwoord2.Text Then
            txtWachtwoord2.ValidationSettings.ErrorText = "De 2 wachtwoorden zijn niet gelijk."
            txtWachtwoord2.IsValid = False
        Else
            Dim m As New QueryStringModule
            Dim wachtwoord As String = m.Encrypt(txtWachtwoord1.Text, venc)
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Try
                    Dim s_SQL As String = "UPDATE Gebruikers SET Paswoord=@wachtwoord WHERE (id=@id)"
                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter("@wachtwoord", SqlDbType.NVarChar, 150) With {.Value = wachtwoord}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@id", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                    blok = True
                    s_SQL = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                    cmd = New SqlCommand(s_SQL, cn)
                    par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Wachtwoord gewijzigd door {0} {1} {2}", Session("userid"), Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                    Dim s_SQL As String = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij wachtwoord wijzigen door {0} {1} {2}", Session("userid"), Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                    txtWachtwoord2.ValidationSettings.ErrorText = "Er is een fout opgetreden, het wachtwoord is niet gewijzigd."
                    txtWachtwoord2.IsValid = False
                End Try

            End Using
        End If
        If blok Then
            Session.Abandon()
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub
End Class