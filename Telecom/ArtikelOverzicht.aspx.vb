﻿Imports System.Data.SqlClient
Imports DevExpress.Web
Imports Microsoft.ajax

Public Class ArtikelOverzicht
    Inherits System.Web.UI.Page
    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property

    'Protected Overrides Sub Initializeculture()
    '    SetCulture()
    '    MyBase.InitializeCulture()

    'End Sub
    'Private Sub SetCulture()
    '    Culture = Session("globalize").ToString
    '    UICulture = Session("globalize").ToString
    '    Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(Session("globalize").ToString)
    '    Threading.Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo(Session("globalize").ToString)
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer






        voocontext.Dispose()

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()

        End If






    End Sub












    'Protected Sub ASPxCallback1_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback1.Callback
    '    Dim Aantalinserted As Int32 = ASPxSpinEditAantal.Value
    '    Dim voocontext As New VooEntities
    '    Dim magid As Integer = Session("MagazijnId")
    '    Dim desc As String = ""
    '    Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
    '        cn.Open()

    '        Dim s_SQL As String = "select description from basismateriaal where id=@id"
    '        Dim cmd As New SqlCommand(s_SQL, cn)
    '        Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
    '        cmd.Parameters.Add(par)
    '        Dim dr As SqlDataReader = cmd.ExecuteReader
    '        If dr.HasRows Then
    '            dr.Read()
    '            If Not IsDBNull(dr.Item(0)) Then
    '                desc = dr.Item(0)

    '            End If
    '        End If

    '        dr.Close()
    '        cn.Close()
    '    End Using

    '    If HiddenArtikel("SerieVerplichtBijOpboeken") Then
    '        Aantalinserted = 0
    '        ASPxTextBoxLotnummer.Visible = True
    '        ASPxLabelLotnummer.Visible = True
    '        Dim nrs As New List(Of String)
    '        For Each textboxId As String In TextBoxIdCollection
    '            Dim tb As TextBox = PanelSerie.FindControl(textboxId)
    '            Dim value As String = tb.Text
    '            If nrs.Contains(value) Then
    '                If Session("taal") = 1 Then
    '                    Session("fout") = "Serienummer " & value & " Staat 2 keer in de lijst om in te lezen! Serienummer kan niet worden toegevoegd. Wijzigingen zijn niet doorgevoerd."

    '                Else
    '                    Session("fout") = "Numéro de série " & value & " Peut être lu deux fois dans la liste! Le numéro de série ne peut pas être ajouté. Les changements n'ont pas été mis en œuvre."

    '                End If
    '                Return
    '            End If
    '            nrs.Add(value)
    '        Next

    '        For Each textboxId As String In TextBoxIdCollection
    '            Dim tb As TextBox = PanelSerie.FindControl(textboxId)
    '            Dim value As String = tb.Text
    '            'enkel actie uitvoeren als textbox ingevuld is
    '            If Not String.IsNullOrWhiteSpace(value) Then
    '                Dim id As String = (CType(tb, TextBox)).ID
    '                Dim parts As String() = id.Split(New Char() {"_"c})
    '                Dim matId As Integer = parts(1).ToString
    '                If voocontext.Serienummer.Where(Function(x) x.serienummer1 = value).Any Then
    '                    If Session("taal") = 1 Then
    '                        Session("fout") = "Serienummer " & value & " bestaat al! Serienummer kan niet worden toegevoegd. Wijzigingen zijn niet doorgevoerd."

    '                    Else
    '                        Session("fout") = "Le numéro de série " & value & " existe déjà! Le numéro de série ne peut pas être ajouté. Aucune modification n'a été apportée."
    '                    End If
    '                    Return
    '                End If
    '                Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '                Dim serienummer As New Serienummer

    '                serienummer.StockMagazijnId = Stockmat.id
    '                serienummer.serienummer1 = value
    '                serienummer.uitgeboekt = 0
    '                serienummer.lotNr = ASPxTextBoxLotnummer.Text
    '                serienummer.datumGemaakt = Today()
    '                serienummer.statusId = 3
    '                voocontext.Serienummer.Add(serienummer)
    '                Aantalinserted += 1

    '                Dim logSerie As New Log
    '                logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " ingelezen op hoofdmagazijn "
    '                logSerie.Tijdstip = Today
    '                logSerie.Gebruiker = Session("userid")
    '                voocontext.Log.Add(logSerie)
    '            End If

    '        Next
    '        voocontext.SaveChanges()


    '        'For Each ctr As Control In PlaceHolderSerie.Controls
    '        '    If TypeOf (ctr) Is TextBox Then
    '        '        Dim id As String = (CType(ctr, TextBox)).ID
    '        '        Dim value As String = (CType(ctr, TextBox)).Text
    '        '        Dim parts As String() = id.Split(New Char() {"_"c})
    '        '        Dim matId As Integer = Convert.ToInt32(parts(0))

    '        '        Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '        '        Dim serienummer As New Serienummer
    '        '        serienummer.StockMagazijnId = Stockmat.id
    '        '        serienummer.serienummer1 = value
    '        '        serienummer.uitgeboekt = 0
    '        '        voocontext.Serienummer.Add(serienummer)

    '        '        Dim logSerie As New Log
    '        '        logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " ingelezen op hoofdmagazijn "
    '        '        logSerie.Tijdstip = Today
    '        '        logSerie.Gebruiker = Session("userid")
    '        '        voocontext.Log.Add(logSerie)
    '        '        voocontext.SaveChanges()
    '        '    End If
    '        'Next
    '        voocontext.SaveChanges()
    '    End If


    '    'UPDATE STOCKARTIKEL
    '    Dim context As New VooEntities

    '    Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
    '    Dim stockart = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
    '    stockart.Aantal = stockart.Aantal + Aantalinserted


    '    'LOG
    '    Dim log As New Log
    '    log.Actie = "Gebruiker " & Session("userid") & " Heeft " & Aantalinserted & " Van artikel " & desc & "ingelezen op hoofdmagazijn " & Session("uitgevoerdwerk")
    '    log.Tijdstip = Today
    '    log.Gebruiker = Session("userid")
    '    context.Log.Add(log)
    '    context.SaveChanges()

    '    context.Dispose()
    '    Session("Gelukt") = Aantalinserted & " van artikel " & desc & " toegevoegd."
    '    Session("GeluktCB") = True
    '    Session("Fout") = ""
    '    ASPxLabelGelukt.Visible = True
    '    ASPxSpinEditAantal.Value = 0
    '    ASPxComboBoxArtikel.Value = 0
    '    ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)


    'End Sub


    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [StockMagazijnId] from [Serienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("StockMagazijnId"))
        Next row
        Session("SelectResult") = result
    End Sub


    'Protected Sub ASPxCallback2_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback2.Callback
    '    Dim voocontext As New VooEntities
    '    Dim magid As Integer = Session("MagazijnId")
    '    Dim desc As String = ""
    '    Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
    '        cn.Open()

    '        Dim s_SQL As String = "select description from basismateriaal where id=@id"
    '        Dim cmd As New SqlCommand(s_SQL, cn)
    '        Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
    '        cmd.Parameters.Add(par)
    '        Dim dr As SqlDataReader = cmd.ExecuteReader
    '        If dr.HasRows Then
    '            dr.Read()
    '            If Not IsDBNull(dr.Item(0)) Then
    '                desc = dr.Item(0)

    '            End If
    '        End If

    '        dr.Close()
    '        cn.Close()
    '    End Using

    '    If HiddenArtikel("SerieVerplichtBijOpboeken") Then
    '        ASPxTextBoxLotnummer.Visible = True
    '        ASPxLabelLotnummer.Visible = True
    '        For Each textboxId As String In TextBoxIdCollection
    '            Dim tb As TextBox = PanelSerie.FindControl(textboxId)
    '            Dim value As String = tb.Text

    '            Dim id As String = (CType(tb, TextBox)).ID
    '            Dim parts As String() = id.Split(New Char() {"_"c})
    '            Dim matId As Integer = parts(1).ToString


    '            Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '            Dim serienummer = voocontext.Serienummer.Where(Function(x) x.serienummer1 = value And x.uitgeboekt <> True).FirstOrDefault
    '            If Not IsNothing(serienummer) And serienummer.StockMagazijn.MagazijnId = magid Then
    '                serienummer.uitgeboekt = 1
    '                serienummer.statusId = 6
    '                serienummer.lotNr = ASPxTextBoxLotnummer.Text
    '                Dim logSerie As New Log
    '                logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " uitgeboekt van hoofdmagazijn "
    '                logSerie.Tijdstip = Today
    '                logSerie.Gebruiker = Session("userid")
    '                voocontext.Log.Add(logSerie)
    '                voocontext.SaveChanges()
    '            Else

    '                ASPxLabelFout.Visible = True
    '                If Session("taal") = 1 Then
    '                    If IsNothing(serienummer) Then
    '                        ASPxLabelFout.Text = "Onbestaande serienummer " & value & " uitgeboekt. Gelieve te controleren en opnieuw te proberen."
    '                    Else
    '                        ASPxLabelFout.Text = "Serienummer " & value & " behoort niet tot het hoofdmagazijn. Gelieve te controleren en opnieuw te proberen."

    '                    End If
    '                Else
    '                    If IsNothing(serienummer) Then
    '                        ASPxLabelFout.Text = "Numéro de série inexistant " & value & " annulé. Veuillez vérifier et réessayer."
    '                    Else
    '                        ASPxLabelFout.Text = "Le numéro de série " & value & " n'appartient pas à l'entrepôt principal. Veuillez vérifier et réessayer."

    '                    End If
    '                End If

    '                Return

    '            End If
    '        Next


    '        'For Each ctr As Control In PlaceHolderSerie.Controls
    '        '    If TypeOf (ctr) Is TextBox Then
    '        '        Dim id As String = (CType(ctr, TextBox)).ID
    '        '        Dim value As String = (CType(ctr, TextBox)).Text
    '        '        Dim parts As String() = id.Split(New Char() {"_"c})
    '        '        Dim matId As Integer = Convert.ToInt32(parts(0))

    '        '        Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '        '        Dim serienummer As New Serienummer
    '        '        serienummer.StockMagazijnId = Stockmat.id
    '        '        serienummer.serienummer1 = value
    '        '        serienummer.uitgeboekt = 0
    '        '        voocontext.Serienummer.Add(serienummer)

    '        '        Dim logSerie As New Log
    '        '        logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " ingelezen op hoofdmagazijn "
    '        '        logSerie.Tijdstip = Today
    '        '        logSerie.Gebruiker = Session("userid")
    '        '        voocontext.Log.Add(logSerie)
    '        '        voocontext.SaveChanges()
    '        '    End If
    '        'Next
    '        voocontext.SaveChanges()
    '    End If



    '    'UPDATE STOCKARTIKEL
    '    Dim context As New VooEntities

    '    Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
    '    Dim stockart = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
    '    stockart.Aantal = stockart.Aantal - ASPxSpinEditAantal.Number


    '    'LOG
    '    Dim log As New Log
    '    log.Actie = "Gebruiker " & Session("userid") & " Heeft " & ASPxSpinEditAantal.Number & " Van artikel " & desc & "uitgeboekt van hoofdmagazijn " & Session("uitgevoerdwerk")
    '    log.Tijdstip = Today
    '    log.Gebruiker = Session("userid")
    '    context.Log.Add(log)
    '    context.SaveChanges()

    '    context.Dispose()
    '    ASPxSpinEditAantal.Value = 0
    '    ASPxComboBoxArtikel.Value = 0
    'End Sub





    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("stockmagazijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub


    Protected Sub ASPxGridView2_BeforePerformDataSelect1(sender As Object, e As EventArgs)
        Session("GekozenArtikel") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub
End Class