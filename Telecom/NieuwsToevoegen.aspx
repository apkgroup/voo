﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="NieuwsToevoegen.aspx.vb" Inherits="Telecom.NieuwsToevoegen" %>
<%@ Register assembly="DevExpress.Web.ASPxHtmlEditor.v19.2, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHtmlEditor" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v19.2, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>

 

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        </h2>  
    <br /> 
    <dx:ASPxMenu ID="ASPxMenu1" runat="server" ShowAsToolbar="True" AutoPostBack="True">
                    <Items>
                        <dx:MenuItem ToolTip="Cliquez ici pour enregistrer les données" Name="Bewaren">
                            <Image Url="~/images/save32x32.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                    </Items>
                </dx:ASPxMenu>
    <table>
        <tr><td colspan="4"></td></tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabelTital" runat="server" Text="ASPxLabel">
                </dx:ASPxLabel>
&nbsp;</td><td>
            <asp:TextBox ID="aspTextBoxTitel" runat="server"></asp:TextBox>
            </td> <td>
                <dx:ASPxLabel ID="ASPxLabelAfbeelding" runat="server" Text="ASPxLabel">
                </dx:ASPxLabel>
&nbsp;</td>
            <td>
                 <asp:FileUpload ID="FileUpload1" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <dx:ASPxHtmlEditor ID="ASPxHtmlEditor1" runat="server">
        <SettingsDialogs>
<InsertImageDialog>
<SettingsImageUpload UploadImageFolder="~/images/nieuws/" UploadFolder="~/images/nieuws/"></SettingsImageUpload>
</InsertImageDialog>
</SettingsDialogs>
        </dx:ASPxHtmlEditor>
</asp:Content>
