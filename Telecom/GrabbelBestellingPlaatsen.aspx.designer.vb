﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class GrabbelBestellingPlaatsen
    
    '''<summary>
    '''Literal2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal2 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''ASPxLabel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel1 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxComboBox1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBox1 As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''ASPxCheckBoxNieuw control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxCheckBoxNieuw As Global.DevExpress.Web.ASPxCheckBox
    
    '''<summary>
    '''SqlDataSourceGebruikers control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceGebruikers As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''ASPxGridViewExporter1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxGridViewExporter1 As Global.DevExpress.Web.ASPxGridViewExporter
    
    '''<summary>
    '''saveChangesBtn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents saveChangesBtn As Global.DevExpress.Web.ASPxButton
    
    '''<summary>
    '''cancelChangesBtn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cancelChangesBtn As Global.DevExpress.Web.ASPxButton
    
    '''<summary>
    '''ASPxGridView1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxGridView1 As Global.DevExpress.Web.ASPxGridView
    
    '''<summary>
    '''ASPxButton1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxButton1 As Global.DevExpress.Web.ASPxButton
    
    '''<summary>
    '''ASPxButton2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxButton2 As Global.DevExpress.Web.ASPxButton
    
    '''<summary>
    '''SqlDataSourceMateriauxADMIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceMateriauxADMIN As Global.System.Web.UI.WebControls.SqlDataSource
End Class
