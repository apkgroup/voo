﻿Imports System.Data.SqlClient

Public Class Uren
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        If Not Session("isadmin") Then
            ASPxComboBox1.Visible = False
            ASPxLabel1.Visible = False
        End If


        ASPxDateEdit1.MaxDate = Date.Now




        Literal2.Text = "Enregister de heures"
        ASPxDateEdit1.Value = Date.Now
    End Sub


    Protected Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click
        Dim ONnr As String
        Dim WN As Integer
        Dim urenTS As TimeSpan
        Dim uren As Decimal

        If ASPxCheckBox1.Checked = False Then
            If ASPxTimeEditBegin.Value Is Nothing Or ASPxTimeEditEind.Value Is Nothing Then
                Return
            End If
            urenTS = Convert.ToDateTime(ASPxTimeEditEind.Value).Subtract(ASPxTimeEditBegin.Value)
        Else
            urenTS = New TimeSpan
        End If








        'uren = urendt.Hour + urendt.Minute / 60

        'ON en WN ophalen voor gebruiker of geselecteerde
        Using cn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn2.Open()
            Dim s_SQL2 As String = "SELECT Werkg, Werkn FROM Gebruikers WHERE (id=@id)"
            Dim cmd2 As New SqlCommand(s_SQL2, cn2)

            Dim par2 As SqlParameter
            If Not Session("isadmin") Then
                par2 = New SqlParameter("@id", SqlDbType.Int, -1) With {.Value = Session("userid")}

            Else
                par2 = New SqlParameter("@id", SqlDbType.Int, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
            End If
            cmd2.Parameters.Add(par2)
            Dim dr2 As SqlDataReader = cmd2.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()

                ONnr = dr2.GetString(0)
                WN = dr2.GetInt32(1)
            End If
            dr2.Close()
            cn2.Close()
        End Using

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("QFormzConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = ""
            Dim tempId As Integer



            'controleren of al bestaat
            s_SQL = "SELECT id FROM DagUren WHERE ([ON]=@ON) and (NR = @NR) and (datum = @datum)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@ON", SqlDbType.NVarChar, 25) With {.Value = ONnr}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@NR", SqlDbType.Int, -1) With {.Value = WN}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@datum", SqlDbType.DateTime) With {.Value = Me.ASPxDateEdit1.Value}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            Dim blbestaat As Boolean = dr.HasRows
            dr.Close()
            If blbestaat Then
                ASPxDateEdit1.ErrorText = "Vous avez déjà entré ce jour."
                ASPxDateEdit1.IsValid = False
                Return
            End If

            s_SQL = "INSERT INTO DagUren ([ON], [NR], [uren],datum, bron, kilometers) VALUES (@on,@nr,@uren,@datum,'Voo', @kilometers)"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@on", SqlDbType.NVarChar, 100) With {.Value = ONnr}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@nr", SqlDbType.Int, -1) With {.Value = WN}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@uren", SqlDbType.Decimal) With {.Value = urenTS.TotalHours}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@datum", SqlDbType.DateTime) With {.Value = Me.ASPxDateEdit1.Value}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@kilometers", SqlDbType.Int, -1) With {.Value = Me.ASPxSpinEditKm.Value}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()
            cn.Close()
        End Using
        Response.Redirect("~/Default.aspx", False)
        Context.ApplicationInstance.CompleteRequest()



    End Sub


    Protected Sub ASPxComboBox1_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBox1.PreRender

        If Not Session.Count = 0 Then
            ASPxComboBox1.SelectedItem = ASPxComboBox1.Items.FindByValue(Session("userid").ToString)
        End If


    End Sub


End Class