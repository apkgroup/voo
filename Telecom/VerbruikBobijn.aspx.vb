﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class VerbruikBobijn
    Inherits System.Web.UI.Page
    Protected Sub vertaal(taalid)

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim voocontext As New VooEntities
        ASPxLabelSuccess.Text = Session("success")
        Session("success") = ""

        Dim opdrachtgeverid As Integer = Session("opdrachtgever")
        Dim opdrachtgever = voocontext.Opdrachtgever.Find(opdrachtgeverid)

        If Session("isadmin") Then
            If Opdrachtgever.tag = "SYN" Then

                ASPxComboBoxMagazijnNaar.DataSourceID = "SqlDataSourceMagazijnenVerbruikSYN"
                ASPxComboBoxMagazijnNaar.DataBind()

            Else

            End If

        Else

            If Opdrachtgever.tag = "SYN" Then
                ASPxComboBoxMagazijn.DataSourceID = "SqlDataSourceBobijnenTech"
                ASPxComboBoxMagazijn.DataBind()
                ASPxComboBoxMagazijnNaar.DataSourceID = "SqlDataSourceMagazijnenVerbruikSYN"
                ASPxComboBoxMagazijnNaar.DataBind()
            Else

            End If

        End If

    End Sub

    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender


    End Sub



    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        If Not (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue() Is Nothing Then
            Session("serienummerlijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        End If

    End Sub



    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [verbruiklijnid] from [VerbruikSerienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("verbruiklijnid"))
        Next row
        Session("SelectResult") = result
    End Sub



    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click


        If String.IsNullOrEmpty(ASPxComboBoxMagazijn.Value) Or String.IsNullOrEmpty(ASPxHoeveelheid.Value) Then
            ASPxLabelFout.Text = "Geen bobijn of hoeveelheid ingevuld"
            Return
        End If
        Dim voocontext As New VooEntities
        Dim verbruik As New Verbruik
        Dim bobid As Integer = ASPxComboBoxMagazijn.Value

        Dim bobijn = voocontext.Bobijn.Find(bobid)
        verbruik.verbruiktvan = bobijn.StockMagazijn.MagazijnId
        verbruik.verbruiktop = Convert.ToInt32(ASPxComboBoxMagazijnNaar.Value)
        verbruik.stad = ASPxTextboxStad.Value
        verbruik.straat = ASPxTextboxStraat.Value
        verbruik.datum = DateTime.Now
        verbruik.datumdoorgestuurd = DateTime.Now
        verbruik.status = 2
        verbruik.gebruiker = Session("userid")
        verbruik.ordernummer = ASPxTextBoxOrdernummer.Value

        If bobijn.aantal < ASPxHoeveelheid.Value Then

            ASPxLabelFout.Text = "Verbruik is hoger dan resterend aantal op bobijn"
            Return

        End If

        voocontext.Verbruik.Add(verbruik)
        voocontext.SaveChanges()

        Dim verbruiklijn As New Verbruiklijn
        verbruiklijn.hoeveelheid = ASPxHoeveelheid.Value
        verbruiklijn.materiaalId = bobijn.StockMagazijn.MateriaalId
        verbruiklijn.bobijnId = bobid
        verbruiklijn.verbruikId = verbruik.id
        verbruiklijn.prijs = voocontext.Basismateriaal.Find(bobijn.StockMagazijn.MateriaalId).prijs

        voocontext.Verbruiklijn.Add(verbruiklijn)
        bobijn.StockMagazijn.Aantal = bobijn.StockMagazijn.Aantal - verbruiklijn.hoeveelheid
        bobijn.aantal = bobijn.aantal - verbruiklijn.hoeveelheid
        Dim sb As New Stockbeweging
        sb.beweging = -verbruiklijn.hoeveelheid
        sb.gebruiker = Session("userid")
        sb.stockmagazijnId = bobijn.StockMagazijn.id
        sb.opmerking = "Bobijnmateriaal verbruikt van bobijn " & bobijn.bobijnnummer & " op project " & voocontext.Magazijn.Find(verbruik.verbruiktop).Naam
        sb.datum = DateTime.Now()
        voocontext.Stockbeweging.Add(sb)
        voocontext.SaveChanges()

        voocontext.SaveChanges()
        Session("success") = Session("success") + "Verbruik op " & voocontext.Magazijn.Find(verbruik.verbruiktop).Naam & " succesvol geregistreerd" + "<br/>"
        ASPxLabelSuccess.Text = Session("success")
        Response.Redirect("~/VerbruikBobijn.aspx", False)


    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub

    Protected Sub ASPxComboBoxMagazijn_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.SelectedIndexChanged
        Dim voocontext As New VooEntities
        Dim bobid As Integer = ASPxComboBoxMagazijn.Value
        Dim bobijn = voocontext.Bobijn.Find(bobid)

        ASPxLabelHoeveelheid.Text = bobijn.aantal
        ASPxLabelArtikel.Text = bobijn.StockMagazijn.Basismateriaal.Article & " " & bobijn.StockMagazijn.Basismateriaal.Description
        ASPxLabelMagazijn.Text = bobijn.StockMagazijn.Magazijn.Naam
    End Sub
End Class