'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class BestellingBobijn
    Public Property id As Integer
    Public Property Datum As Nullable(Of Date)
    Public Property GebruikerId As Integer
    Public Property Status As Nullable(Of Integer)
    Public Property DatumGoedkeuring As Nullable(Of Date)
    Public Property Opmerking As String
    Public Property Deadline As Nullable(Of Date)
    Public Property RedenAfkeuring As String

    Public Overridable Property BestellijnBobijn As ICollection(Of BestellijnBobijn) = New HashSet(Of BestellijnBobijn)
    Public Overridable Property Gebruikers As Gebruikers

End Class
