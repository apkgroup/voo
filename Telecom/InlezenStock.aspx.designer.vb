﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class InlezenStock

    '''<summary>
    '''SqlDataSourceMagazijnen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceMagazijnen As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''ASPxComboBoxMagazijn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxMagazijn As Global.DevExpress.Web.ASPxComboBox

    '''<summary>
    '''Literal2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal2 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''ASPxLabelGelukt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabelGelukt As Global.DevExpress.Web.ASPxLabel

    '''<summary>
    '''TableArtikel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TableArtikel As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''ASPxLabelScan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabelScan As Global.DevExpress.Web.ASPxLabel

    '''<summary>
    '''ASPxTextBoxScan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxScan As Global.DevExpress.Web.ASPxTextBox

    '''<summary>
    '''ASPxLabel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel1 As Global.DevExpress.Web.ASPxLabel

    '''<summary>
    '''HiddenArtikel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HiddenArtikel As Global.DevExpress.Web.ASPxHiddenField

    '''<summary>
    '''ASPxComboBoxArtikel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxArtikel As Global.DevExpress.Web.ASPxComboBox

    '''<summary>
    '''ASPxLabel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel2 As Global.DevExpress.Web.ASPxLabel

    '''<summary>
    '''ASPxSpinEditAantal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxSpinEditAantal As Global.DevExpress.Web.ASPxSpinEdit

    '''<summary>
    '''ASPxLabelLotnummer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabelLotnummer As Global.DevExpress.Web.ASPxLabel

    '''<summary>
    '''ASPxTextBoxLotnummer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxLotnummer As Global.DevExpress.Web.ASPxTextBox

    '''<summary>
    '''ASPxLabel3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel3 As Global.DevExpress.Web.ASPxLabel

    '''<summary>
    '''ASPxComboBox2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBox2 As Global.DevExpress.Web.ASPxComboBox

    '''<summary>
    '''SqlDataSourceLocaties control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceLocaties As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''PlaceHolderSerie control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PlaceHolderSerie As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''PanelSerie control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PanelSerie As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ASPxButtonToevoegen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxButtonToevoegen As Global.DevExpress.Web.ASPxButton

    '''<summary>
    '''ASPxLabelFout control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabelFout As Global.DevExpress.Web.ASPxLabel

    '''<summary>
    '''ASPxGridView2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxGridView2 As Global.DevExpress.Web.ASPxGridView

    '''<summary>
    '''SqlDataSourceArtikelen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceArtikelen As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''SqlDataSourceMateriaux control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceMateriaux As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''SqlDataSourceSerienummers control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceSerienummers As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''SqlDataSourceStockMagazijn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceStockMagazijn As Global.System.Web.UI.WebControls.SqlDataSource
End Class
