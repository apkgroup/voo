﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data
Imports DevExpress.Web



Public Class Transfer
    Inherits System.Web.UI.Page
    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)
        Dim voocontext As New VooEntities
        Dim id As Integer = Request.QueryString("id")
        Dim naam As String = voocontext.Gebruikers.Find(id).Login
        If taalId = 1 Then
            Literal2.Text = "Wisselen van opdrachtgever " & naam
            Literal3.Text = "Opdrachtgever wisselen naar:"
            ASPxButton1.Text = "Annuleren"
            ASPxButton2.Text = "Bevestigen"
        Else
            Literal2.Text = "Transfer admin " & naam
            Literal3.Text = "Nouveau administration:"
            ASPxButton1.Text = "Annuler"
            ASPxButton2.Text = "Confirmer"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub

    Protected Sub ASPxButton2_Click(sender As Object, e As EventArgs) Handles ASPxButton2.Click
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim mag_SQL As String = "DECLARE @magId int; SELECT @magId = g.magazijnId FROM Gebruikers G where g.id = @techId; UPDATE Gebruikers set opdrachtgeverId = @nieuweOpdrachtgever where id = @techId; UPDATE Magazijn set opdrachtgeverId = @nieuweOpdrachtgever where id = @magId;"
            Dim cmd = New SqlCommand(mag_SQL, cn)
            Dim par = New SqlParameter("@techId", SqlDbType.Int, -1) With {.Value = Request.QueryString("id")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@nieuweOpdrachtgever", SqlDbType.Int, -1) With {.Value = ASPxComboBox1.Value}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()

        End Using
        Response.Redirect("~/BeheerGebruikers.aspx?transfer=" & Request.QueryString("id"), False)
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        Response.Redirect("~/BeheerGebruikers.aspx", False)
    End Sub
End Class