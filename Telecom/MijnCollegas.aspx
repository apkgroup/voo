﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="MijnCollegas.aspx.vb" Inherits="Telecom.MijnCollegas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .row {
    float:left;
    width: 96%;
    margin-right: 10px;
    height: 115px;
    overflow: auto;
    position: static;
}

td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
.fixed {    
    position: fixed;   
    top:0px;
    background-color: #ffffff ! important; 
}
    </style>
     
    <script type="text/javascript">
      
        function SetPCVisible(value) {
            var popupControl = GetPopupControl();
            if (value) {
                popupControl.Show();
            }
            else {
                popupControl.Hide();
            }
        }

        function GetPopupControl() {
            return popup;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:right; margin-bottom:10px;margin-left:10px;"></div><h2>
    <asp:Literal ID="Literal2" runat="server"></asp:Literal>
&nbsp;</h2>
    
    <br />
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGebruikers" KeyFieldName="id">
        <Columns>
            <dx:GridViewCommandColumn VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Naam" ReadOnly="True" VisibleIndex="5" Caption="Nom">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="GSM" VisibleIndex="6">
                <PropertiesTextEdit MaxLength="20">
                    <MaskSettings ErrorText="Enkel formaat +32499123456 toegelaten" Mask="+00000000000" />
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn Caption="En garde?" FieldName="VanWacht" VisibleIndex="7" Visible="False">
                <PropertiesCheckEdit ClientInstanceName="checkVanWacht">
                    <ClientSideEvents CheckedChanged="function(s, e) {
	if (s.GetChecked()==true) {
            SetPCVisible(true);
                        }
}" />
                </PropertiesCheckEdit>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="Actief" VisibleIndex="8" Caption="Actif" Visible="False">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="Email" VisibleIndex="10">
                <PropertiesTextEdit MaxLength="150">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="Approuver congé?" FieldName="VerlofGoedkeuring" VisibleIndex="11" Visible="False">
                <PropertiesComboBox DataSourceID="SqlDataSourceVerlof" TextField="WERKNEMER" ValueField="id" ValueType="System.Int32">
                    <Columns>
                        <dx:ListBoxColumn FieldName="WERKNEMER" />
                        <dx:ListBoxColumn FieldName="id" Visible="False" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="Niveau" VisibleIndex="9" Visible="False">
                <PropertiesComboBox DataSourceID="SqlDataSourceNiveau" TextField="Naam" ValueField="id" ValueType="System.Int32">
                    <Columns>
                        <dx:ListBoxColumn FieldName="Naam" />
                        <dx:ListBoxColumn FieldName="Niveau" />
                        <dx:ListBoxColumn FieldName="id" Visible="False" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Administration" FieldName="OpdrachtgeverId" VisibleIndex="16" Visible="False">
                <PropertiesComboBox DataSourceID="SqlDataSourceOpdrachtgever" TextField="naam" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
        <SettingsPager PageSize="20">
            <PageSizeItemSettings Caption="Items per pagina:" Items="10, 20, 50" ShowAllItem="True" Visible="True">
            </PageSizeItemSettings>
        </SettingsPager>
        <Settings ShowHeaderFilterButton="True" ShowStatusBar="Hidden" />
        <SettingsSearchPanel Visible="true" />
        <SettingsDataSecurity AllowDelete="False" AllowInsert="False" AllowEdit="False" />
          <SettingsCommandButton>
             <NewButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Nieuw" Url="~/images/Add_32x32.png" Width="16px">
                 </Image>
             </NewButton>
             <UpdateButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewaren" Url="~/images/Save_16x16.png" Width="16px">
                 </Image>
             </UpdateButton>
             <CancelButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Annuleren" Url="~/images/annuleren.png" Width="16px">
                 </Image>
             </CancelButton>
             <EditButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewerken" Url="~/images/Edit.png" Width="16px">
                 </Image>
             </EditButton>
             <DeleteButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Verwijderen" Url="~/images/Folders-OS-Recycle-Bin-Full-Metro-icon.png" Width="16px">
                 </Image>
             </DeleteButton>
         </SettingsCommandButton>
    </dx:ASPxGridView>
<asp:SqlDataSource ID="SqlDataSourceGebruikers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Gebruikers] WHERE [id] = @id" InsertCommand="INSERT INTO [Gebruikers] ([Login], [Werkg], [Werkn], [GSM], [Actief], [Niveau], [Email], [AangemaaktDoor], [AangemaaktOp], [WachtwoordVerstuurd], [WachtwoordVerstuurdOp], [VerlofGoedkeuring], VanWacht) VALUES (@Login, @Werkg, @Werkn, @GSM, @Actief, @Niveau, @Email, @AangemaaktDoor, @AangemaaktOp, @WachtwoordVerstuurd, @WachtwoordVerstuurdOp, @VerlofGoedkeuring, @VanWacht)" SelectCommand="SELECT Gebruikers.id, Gebruikers.Login, 
Gebruikers.Werkg, 
Gebruikers.Werkn, 
(
SELECT 
CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam
ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)
END
) as Naam,
Gebruikers.GSM, 
Gebruikers.Actief , 
Gebruikers.Niveau, 
Gebruikers.Email, 
Gebruikers.AangemaaktDoor , 
Gebruikers.AangemaaktOp , 
Gebruikers.WachtwoordVerstuurd, 
Gebruikers.WachtwoordVerstuurdOp, 
Gebruikers.VerlofGoedkeuring, 
Gebruikers.VanWacht,
Gebruikers.OpdrachtgeverId
FROM Gebruikers WHERE Gebruikers.OpdrachtgeverId = @OpdrachtgeverId" UpdateCommand="UPDATE [Gebruikers] SET [Login] = @Login, [Werkg] = @Werkg, [Werkn] = @Werkn, [GSM] = @GSM, [Actief] = @Actief, [Niveau] = @Niveau, [Email] = @Email, [AangemaaktDoor] = @AangemaaktDoor, [AangemaaktOp] = @AangemaaktOp, [WachtwoordVerstuurd] = @WachtwoordVerstuurd, [WachtwoordVerstuurdOp] = @WachtwoordVerstuurdOp, [VerlofGoedkeuring] = @VerlofGoedkeuring, VanWacht=@VanWacht WHERE [id] = @id">
    <DeleteParameters>
        <asp:Parameter Name="id" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="Login" Type="String" />
        <asp:Parameter Name="Werkg" Type="String" />
        <asp:Parameter Name="Werkn" Type="Int32" />
        <asp:Parameter Name="GSM" Type="String" />
        <asp:Parameter Name="Actief" Type="Boolean" />
        <asp:Parameter Name="Niveau" Type="Int32" />
        <asp:Parameter Name="Email" Type="String" />
        <asp:Parameter Name="AangemaaktDoor" Type="Int32" />
        <asp:Parameter Name="AangemaaktOp" Type="DateTime" />
        <asp:Parameter Name="WachtwoordVerstuurd" Type="Boolean" />
        <asp:Parameter Name="WachtwoordVerstuurdOp" Type="DateTime" />
        <asp:Parameter Name="VerlofGoedkeuring" Type="Int32" />
        <asp:Parameter Name="VanWacht" />
    </InsertParameters>
    <SelectParameters>
        <asp:SessionParameter DefaultValue="3" Name="OpdrachtgeverId" SessionField="opdrachtgever" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="Login" Type="String" />
        <asp:Parameter Name="Werkg" Type="String" />
        <asp:Parameter Name="Werkn" Type="Int32" />
        <asp:Parameter Name="GSM" Type="String" />
        <asp:Parameter Name="Actief" Type="Boolean" />
        <asp:Parameter Name="Niveau" Type="Int32" />
        <asp:Parameter Name="Email" Type="String" />
        <asp:Parameter Name="AangemaaktDoor" Type="Int32" />
        <asp:Parameter Name="AangemaaktOp" Type="DateTime" />
        <asp:Parameter Name="WachtwoordVerstuurd" Type="Boolean" />
        <asp:Parameter Name="WachtwoordVerstuurdOp" Type="DateTime" />
        <asp:Parameter Name="VerlofGoedkeuring" Type="Int32" />
        <asp:Parameter Name="VanWacht" />
        <asp:Parameter Name="id" Type="Int32" />
    </UpdateParameters>
</asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceNiveau" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Naam], [Niveau] FROM [Gebruikersniveaus]"></asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSourceVerlof" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Gebruikers.id, vwWerkn.WERKNEMER FROM Gebruikers INNER JOIN (SELECT NAAM + ' ' + VNAAM AS WERKNEMER, [ON], NR FROM Elly_SQL.dbo.WERKN) AS vwWerkn ON Gebruikers.Werkg = vwWerkn.[ON] AND Gebruikers.Werkn = vwWerkn.NR WHERE (Gebruikers.Niveau = 1) AND (ISNULL(Gebruikers.Actief, 0) = 1)"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceBeheerder" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Gebruikers.id, vwWerkn.WERKNEMER FROM Gebruikers INNER JOIN 
(SELECT [ON], NR, Naam + ' ' + VNAAM as WERKNEMER FROM Elly_SQL.dbo.Werkn) vwWerkn
ON Gebruikers.Werkn=vwWerkn.NR AND Gebruikers.Werkg=vwWerkn.[ON] "></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceOpdrachtgever" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [naam] FROM [Opdrachtgever]"></asp:SqlDataSource>
    <dx:ASPxCallback ID="ASPxCallback1" ClientInstanceName="ASPxCallback1" runat="server">
    </dx:ASPxCallback>
</asp:Content>
