﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="VerlofaanvraagOverzicht.aspx.vb" Inherits="Telecom.VerlofaanvraagOverzicht" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
         
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
.fixed {    
    position: fixed;   
    top:0px;
    background-color: #ffffff ! important; 
}
    </style>
   	<script type="text/javascript" src="../js/fib-jquery.js"></script>
           
          <script type="text/javascript" charset="utf-8">
              $(document).ready(function () {

                  var offset = $('#gridmenu').offset();

                  $(window).scroll(function () {
                      var scrollTop = $(window).scrollTop();
                      if (offset.top < scrollTop) {
                          $('#gridmenu').addClass('fixed');
                      } else {
                          $('#gridmenu').removeClass('fixed');
                      };
                  });

              });

		</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div style="float:right; margin-bottom:10px;margin-left:10px;"><div id="gridmenu"><dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu></div></div><table><tr><td><h2>Demande de conge</h2></td><td><h2><dx:ASPxComboBox ID="ASPxComboBoxGebruiker" runat="server" DataSourceID="SqlDataSourceAanvragen" ValueType="System.Int32" TextField="WERKNEMER" ValueField="Gebruiker" AutoPostBack="True">
            <ClientSideEvents ValueChanged="function(s, e) {
	lpanel.Show();
}" />
            <Columns>
                <dx:ListBoxColumn FieldName="WERKNEMER" />
                <dx:ListBoxColumn FieldName="Gebruiker" Visible="False" />
            </Columns>
        </dx:ASPxComboBox></h2></td><td>Anée: </td><td>
          <dx:ASPxSpinEdit ID="ASPxSpinEditJaar" runat="server" HorizontalAlign="Right" Number="0" NumberType="Integer" />
          </td></tr></table>   
                <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server" ClientInstanceName="lpanel" Modal="True" Text="Bezig met laden gegevens ...">

                        </dx:ASPxLoadingPanel>
        
        <asp:SqlDataSource ID="SqlDataSourceAanvragen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Verlofdagen.Gebruiker, vwWerkn.WERKNEMER FROM Verlofdagen INNER JOIN Gebruikers 
ON Verlofdagen.Gebruiker=Gebruikers.id INNER JOIN (SELECT NAAM + ' ' + VNAAM as WERKNEMER, [ON],NR FROM Elly_SQL.dbo.WERKN) vwWerkn 
ON Gebruikers.Werkg=vwWerkn.[ON] AND Gebruikers.Werkn=vwWerkn.NR WHERE (Isnull(Gebruikers.Actief,0)=1) AND Gebruikers.opdrachtgeverId = @opdrachtgeverId
GROUP BY Verlofdagen.Gebruiker, vwWerkn.WERKNEMER order by vwWerkn.WERKNEMER">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="1" Name="opdrachtgeverId" SessionField="opdrachtgever" />
            </SelectParameters>
         </asp:SqlDataSource><br />
    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceVerlofaanvragen" KeyFieldName="id">
        <SettingsPager Visible="False">
        </SettingsPager>
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="GebruikerID" VisibleIndex="1" Caption="ID Utilisateur">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Utilisateur" ReadOnly="True" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Employeur" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Employé" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="GebruikerUitDienst" VisibleIndex="5" Caption="Du service">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataDateColumn FieldName="Date" VisibleIndex="6">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataCheckColumn FieldName="Toutejour" VisibleIndex="8">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="Total" VisibleIndex="9">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="Approuvé" ReadOnly="True" VisibleIndex="10">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="Rejeté" ReadOnly="True" VisibleIndex="11">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataDateColumn FieldName="Demandé sur" VisibleIndex="12">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataDateColumn FieldName="Décidé sur" VisibleIndex="13">
            </dx:GridViewDataDateColumn>
        </Columns>
          <Settings ShowHeaderFilterButton="True" ShowStatusBar="Hidden" />
         <SettingsPager Mode="ShowAllRecords" Visible="False">
        </SettingsPager>
      </dx:ASPxGridView>
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="grid">
    </dx:ASPxGridViewExporter>
      <asp:SqlDataSource ID="SqlDataSourceVerlofaanvragen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Verlofdagen.[id], Verlofdagen.Gebruiker as GebruikerID
      ,vwWerkn.WERKNEMER as Utilisateur
	  , Gebruikers.Werkg as Employeur, 
Gebruikers.Werkn as Employé,
	  vwWerkn.uitdienst as GebruikerUitDienst
      ,[Datum] as Date
      ,[HeleDag] as Toutejour
      ,[Startuur] as Heuredébut
      ,[Einduur] as Heurefin
      ,[TotaalUur] as Total
      ,cast(isnull([Goedgekeurd],0) as bit) as Approuvé
      ,cast(isnull([Afgekeurd],0) as bit) as Rejeté
      ,[AangevraagdOp] as 'Demandé sur'
      ,[BeslistOp] as 'Décidé sur'
      
  FROM [dbo].[Verlofdagen] INNER JOIN 
  Gebruikers  ON Verlofdagen.Gebruiker=Gebruikers.id INNER JOIN
  (SELECT [ON], NR, Naam + ' ' + VNAAM as WERKNEMER, uitdienst FROM Elly_SQL.dbo.Werkn) vwWerkn
  ON Gebruikers.Werkg=vwWerkn.[ON] AND Gebruikers.Werkn=vwWerkn.NR
WHERE (Verlofdagen.Gebruiker=@gebruiker) order by Datum">
          <SelectParameters>
              <asp:ControlParameter ControlID="ASPxComboBoxGebruiker" Name="gebruiker" PropertyName="Value" DefaultValue="" />
          </SelectParameters>
      </asp:SqlDataSource>
</asp:Content>
