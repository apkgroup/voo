﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="DashboardFIFO.aspx.vb" Inherits="Telecom.DashboardFIFO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #TextArea1 {
            height: 51px;
            width: 102px;
        }
    </style>
    <script>
        var currentRowIndex = undefined;
        var currentRowIndex2 = undefined;

    </script>
    <script type="text/javascript">


        window.onload = function () {

            console.log("onload");
            $("input[name*='Serienummer']").each(function () {
                console.log(this.id);
                if (this.id.split("_").pop() != document.getElementById("ctl00_ContentPlaceHolder1_ASPxSpinEditAantal_I").value) {
                    this.removeAttribute("onchange");
                }
                else {
                    this.focus();
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_ASPxSpinEditAantal_I").value == 0) {
                    console.log("Het is 0!");
                    document.getElementById("ctl00_ContentPlaceHolder1_PanelSerie").style.display = "none";
                }
            });


            $('input').keypress(function (e) {
                console.log("KEYPRESS");
                if (e.which == 13) {
                    e.preventDefault();
                    console.log("ISENTER");
                    var self = $(this)
                    var form = self.parents('form:eq(0)');
                    var focusable;
                    var next;
                    var prev;

                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {

                    }
                    return false;

                }
            });

            ASPxGridView2.Refresh();
            var i;

            $('input').keypress(function (e) {
                console.log("KEYPRESS");
                if (e.which == 13) {
                    e.preventDefault();
                    console.log("ISENTER");
                    var self = $(this)
                    var form = self.parents('form:eq(0)');
                    var focusable;
                    var next;
                    var prev;

                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {

                    }
                    return false;

                }
            });
        };

        var timerHandle = -1;
        var indexrow
        function OnBatchEditStartEditing(s, e) {
            currentRowIndex = e.visibleIndex;
            currentRowIndex2 = e.visibleIndex;
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("Nombre");
        }

        function OnEndCallBack(s, e) {
            if (s.cpIsUpdated != '') {


                clientText.GetMainElement().style.display = 'block';
                console.log('Waarde komt uit');
                //console.log(e); 
                //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);
                console.log('Temps de Regie updated! Waarde is...');
                //console.log(e); 
                //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);

                console.log(clientButton);
                //console.log(Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML));
                if (ASPxGridView1.GetRow(indexrow).children[3].firstElementChild == null) {
                    if (Number(ASPxGridView1.GetRow(indexrow).children[3].innerHTML.replace(",", ".")) > 0) {
                        console.log("enabled=false");
                        clientButton.SetEnabled(false);
                        clientLabel2.GetMainElement().style.display = 'block'
                    }
                    else {
                        console.log("enabled=true");
                        clientButton.SetEnabled(true);
                        clientLabel2.GetMainElement().style.display = 'none'
                    }
                }
                else {
                    if (Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML.replace(",", ".")) > 0) {
                        console.log("enabled=false");
                        clientButton.SetEnabled(false);
                        clientLabel2.GetMainElement().style.display = 'block'
                    }
                    else {
                        console.log("enabled=true");
                        clientButton.SetEnabled(true);
                        clientLabel2.GetMainElement().style.display = 'none'
                    }
                }


                //console.log(s.cpIsUpdated);


            }
            else {
                clientText.SetText('');
                console.log('Anders updated');
            }
        }

        function OnKeyDown(s, e) {
            var key = e.htmlEvent.keyCode;
            console.log("key pressed!");
            if (key == 38 || key == 40) {
                console.log("Up or down key pressed!");
                ASPxClientUtils.PreventEvent(e.htmlEvent);
                ASPxGridView1.batchEditApi.EndEdit();
                console.log(currentRowIndex);
                if (currentRowIndex !== undefined && currentRowIndex !== null) {
                    var visibleIndices = ASPxGridView1.batchEditApi.GetRowVisibleIndices();
                    var indicesArrayLength = visibleIndices.length;
                    var rowIndex = visibleIndices.indexOf(currentRowIndex);
                    if (!(rowIndex === 0 && key === 38) || !(rowIndex === indicesArrayLength - 1 && key === 40)) {
                        switch (key) {
                            case 38:
                                ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex - 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            case 40:
                                ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex + 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        function OnKeyDownMat(s, e) {
            var key = e.htmlEvent.keyCode;
            console.log("key pressed!");
            if (key == 38 || key == 40) {
                console.log("Up or down key pressed");
                ASPxClientUtils.PreventEvent(e.htmlEvent);
                ASPxGridView2.batchEditApi.EndEdit();
                console.log(currentRowIndex2);
                if (currentRowIndex2 !== undefined && currentRowIndex2 !== null) {
                    var visibleIndices2 = ASPxGridView2.batchEditApi.GetRowVisibleIndices();
                    var indicesArrayLength2 = visibleIndices2.length;
                    var rowIndex2 = visibleIndices2.indexOf(currentRowIndex2);
                    if (!(rowIndex2 === 0 && key === 38) || !(rowIndex2 === indicesArrayLength2 - 1 && key === 40)) {
                        console.log("ERIN");
                        switch (key) {
                            case 38:
                                ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 - 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            case 40:
                                ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 + 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }



        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);

            indexrow = e.visibleIndex;
            console.log("Index row set to " + indexrow);
            //if (s.cpIsUpdated != '') {

            //    console.log(clientText.GetMainElement());
            //    console.log(clientText);
            //    clientText.GetMainElement().style.display = 'none';
            //    console.log('Temps de Regie updated! Waarde is...');
            //    console.log(e);
            //    //console.log(ASPxGridView1.GetRow(e.visibleIndex).children[3].innerHTML);
            //    console.log(clientButton);
            //    clientButton.setEnabled(true);
            //    console.log(s.cpIsUpdated);


            //}
            //else {
            //    clientText.SetText('');
            //    console.log('Anders updated');
            //}

        }

        function saveChangesBtn_Click(s, e) {
            ASPxGridView1.UpdateEdit();
            ASPxGridView2.UpdateEdit();
        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();
            ASPxGridView2.CancelEdit();
        }




        function saveChangesBtnMat_Click(s, e) {
            ASPxGridView2.UpdateEdit();
        }

        function cancelChangesBtnMat_Click(s, e) {
            ASPxGridView2.CancelEdit();



        }


        function insertArticle() {
            console.log("IETest");
            Callback1.PerformCallback();
            console.log("IEAfterCallback");
        }



        function OnCallbackComplete(s, e) {
            console.log("cbCOMPLET");
            ASPxGridView2.Refresh();
            //location.reload();

        }

        function OnClick() {
            //cp.PerformCallback();  
            ASPxLoadingPanel2.Show();
        }




    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>FIFO Dashboard</h2>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) and hoofd =1">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
    <p>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </p>
    <p>
        <asp:SqlDataSource ID="SqlDataSourceFIFO" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand=" with fifo as 
 (select top 100 percent * from 
 (select ROW_NUMBER() OVER(ORDER BY subq.[Description], subq.Oudstedatum ASC) AS Row#, subq.opmerking as 'locatienaam', subq.shipmentNumber, subq.aantal as 'aantal in rek', subq.Oudstedatum as 'Datum inlezing',
 subq.Description as 'Artikel' from (select * from (
 select top 100 percent
sh.shipmentNumber, sh.opmerking,count(s.serienummer) as 'aantal',min(sh.datereceived) as 'Oudstedatum',b.Description
 from voo.dbo.Serienummer s
 inner join  voo.dbo.StockMagazijn sm on s.StockMagazijnId = sm.id
 inner join  voo.dbo.Basismateriaal b on sm.MateriaalId = b.id
 inner join voo.dbo.Shipment sh on s.shipmentId = sh.id
 where statusId = 3 and sm.MagazijnId = @magazijnId and (fifo = 1)
 group by sh.opmerking, sh.shipmentNumber,b.Description
 order by Oudstedatum,[Description]) as q
 WHERE
 (
			SELECT 	COUNT(*) 
			FROM 	(
 select top 100 percent
sh.shipmentNumber, sh.opmerking,count(s.serienummer) as 'aantal',min(sh.datereceived) as 'Oudstedatum',b.Description
 from voo.dbo.Serienummer s

 inner join  voo.dbo.StockMagazijn sm on s.StockMagazijnId = sm.id
 inner join  voo.dbo.Basismateriaal b on sm.MateriaalId = b.id
 inner join voo.dbo.Shipment sh on s.shipmentId = sh.id
 where statusId = 3 and sm.MagazijnId = @magazijnId and (fifo = 1)
 group by sh.opmerking, sh.shipmentNumber,b.Description
 order by Oudstedatum, Description ) as f
			WHERE f.Description = q.description AND 
				  f.Oudstedatum &lt;= q.Oudstedatum
		) &lt;= 2) as subq
	) as subq2
	order by subq2.[Datum inlezing], Artikel
	)

	select f1.Row#, f1.Artikel, f1.locatienaam as 'FIFO Locatie', f1.shipmentNumber as 'FIFO Shipment',
	f2.locatienaam as 'volgende locatie',
	f2.shipmentNumber as 'Volgende Shipment',
	f1.[aantal in rek] 'aantal in Shipment',
	f2.[aantal in rek] 'aantal in volgende Shipment'
	
	from fifo f1 left join fifo f2 on f1.Artikel = f2.Artikel and f1.Row# +1 = f2.Row#
  where f1.shipmentNumber not in  (select f4.shipmentNumber from fifo f3 left join fifo f4 on f3.Artikel = f4.Artikel and f3.Row# +1 = f4.Row# where f4.shipmentNumber is not null)">
            <SelectParameters>
                <asp:ControlParameter ControlID="ASPxComboBoxMagazijn" Name="magazijnId" PropertyName="Value" />
            </SelectParameters>
        </asp:SqlDataSource>
    </p>
    <p>&nbsp;</p>
    <p>
        <dx:ASPxCardView ID="ASPxCardViewFIFO" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceFIFO">
              <Styles>  
        <Card Width="420px" Height="100px" />  
    </Styles> 
            <SettingsPager Visible="False">
            </SettingsPager>
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
            <SettingsExport ExportSelectedCardsOnly="False"></SettingsExport>

            <Columns>
                <dx:CardViewTextColumn FieldName="Row#" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="0">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="Artikel" ShowInCustomizationForm="True" VisibleIndex="1">
                    <PropertiesTextEdit>
                        <Style Font-Bold="True" Font-Size="Larger">
                        </Style>
                    </PropertiesTextEdit>
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="FIFO Shipment" ShowInCustomizationForm="True" VisibleIndex="3">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="volgende locatie" ShowInCustomizationForm="True" VisibleIndex="5">
                    <PropertiesTextEdit>
                        <Style Font-Italic="True">
                        </Style>
                    </PropertiesTextEdit>
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="aantal in Shipment" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="4">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="aantal in volgende Shipment" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="7">
                    <PropertiesTextEdit>
                        <Style Font-Italic="True">
                        </Style>
                    </PropertiesTextEdit>
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="FIFO Locatie" VisibleIndex="2">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="Volgende Shipment" VisibleIndex="6">
                </dx:CardViewTextColumn>
            </Columns>
            <CardLayoutProperties ColCount="3" ColumnCount="3">
                <Items>
                    <dx:CardViewColumnLayoutItem ColSpan="3" ColumnName="Artikel" ColumnSpan="3" ShowCaption="False">
                        <Template>
                            <dx:ASPxLabel runat="server" Text='<%# Eval("Artikel") %>' Font-Bold="True" Font-Size="Large" />
                        </Template>
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem ColSpan="1" ColumnName="FIFO Shipment">
                                       <Template>
                            <dx:ASPxLabel runat="server" Text='<%# Eval("FIFO Shipment") %>' Font-Bold="True" Font-Size="Larger" />
                        </Template>
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem ColSpan="1" ColumnName="FIFO Locatie">
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem Caption="Aantal in Shipment" ColSpan="1" ColumnName="aantal in Shipment">
                    </dx:CardViewColumnLayoutItem>
                    <dx:EmptyLayoutItem ColSpan="3" ColumnSpan="3" Height="4px">
                        <BorderBottom BorderColor="#CC3300" BorderStyle="Dashed" BorderWidth="2px" />
                    </dx:EmptyLayoutItem>
                    <dx:CardViewColumnLayoutItem Caption="Volgende Shipment" ColSpan="1" ColumnName="Volgende Shipment">
                                                               <Template>
                            <dx:ASPxLabel runat="server" Text='<%# Eval("Volgende Shipment") %>' Font-Bold="False" ForeColor="#828282" Font-Size="Larger" />
                        </Template>
                        <CaptionStyle Font-Italic="True">
                        </CaptionStyle>
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem ColSpan="1" ColumnName="volgende locatie">
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem Caption="Aantal in volgende rek" ColSpan="1" ColumnName="aantal in volgende Shipment">
                        <CaptionStyle Font-Italic="True">
                        </CaptionStyle>
                    </dx:CardViewColumnLayoutItem>
                </Items>
            </CardLayoutProperties>

            <StylesExport>
                <Card BorderSize="1" BorderSides="All"></Card>

                <Group BorderSize="1" BorderSides="All"></Group>

                <TabbedGroup BorderSize="1" BorderSides="All"></TabbedGroup>

                <Tab BorderSize="1"></Tab>
            </StylesExport>
        </dx:ASPxCardView>
    </p>

    <h2>Artikelen onder minimum voor locatie</h2>
    <p>&nbsp;</p>
    <dx:ASPxGridView ID="ASPxGridViewMinMax" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMinMax">
        <Columns>
            <dx:GridViewDataTextColumn FieldName="article" ReadOnly="True" VisibleIndex="0">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" ReadOnly="True" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="min" ReadOnly="True" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="huidig" ReadOnly="True" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>

    <asp:SqlDataSource ID="SqlDataSourceMinMax" runat="server" ConnectionString="<%$ ConnectionStrings:VooConnectionString %>" SelectCommand="select article, [Description], [min],
(select Aantal from StockMagazijn s1 where s1.MateriaalId = minmaxmag.materiaalid and s1.MagazijnId = @magazijnid) as huidig
 from (SELECT DISTINCT
mo.opdrachtgeverId,
      [Article]
	  ,b.id as 'materiaalid'
      ,[Description]
      ,mo.[min]
      ,mo.[max]
,1 as standaard
  FROM [Voo].[dbo].[Basismateriaal] b
  inner join [Voo].[dbo].StockMagazijn s on b.id = s.MateriaalId
  inner join [Voo].[dbo].magazijn m on m.id = s.MagazijnId
 inner join voo.dbo.MateriaalOpdrachtgevers mo on mo.materiaalId = b.id
  where (mo.actief = 1 or mo.actief is null) and b.[min] is not null and m.id = @magazijnid
  and b.id not in (select materiaalId from voo.dbo.minmax where magazijnId = @magazijnid ) and mo.opdrachtgeverId = m.OpdrachtgeverId
      UNION
  SELECT DISTINCT mo.opdrachtgeverId,
      [Article]
	   ,b.id as 'materiaalid'
      ,[Description]
      ,mm.[min]
      ,mm.[max]
      ,    CASE
  when (select [min] from [Voo].[dbo].MateriaalOpdrachtgevers b1 where  b1.materiaalId = b.id and b1.opdrachtgeverId = m.OpdrachtgeverId)  = 
 (select [min] from [Voo].[dbo].minmax mm1 where  mm1.materiaalId = b.id and mm1.magazijnId  = @magazijnid )
  AND (select [max] from [Voo].[dbo].MateriaalOpdrachtgevers b1 where  b1.materiaalId = b.id and b1.opdrachtgeverId = m.OpdrachtgeverId)  = 
  (select [max] from [Voo].[dbo].minmax mm1 where  mm1.materiaalId = b.id  and mm1.magazijnId  = @magazijnid  ) THEN 1
  else 0
  end as altered
  FROM [Voo].[dbo].[Basismateriaal] b
  inner join [Voo].[dbo].StockMagazijn s on b.id = s.MateriaalId
  inner join [Voo].[dbo].magazijn m on m.id = s.MagazijnId
  inner join voo.dbo.minmax mm on mm.materiaalId = b.id and mm.magazijnId = m.id
 inner join voo.dbo.MateriaalOpdrachtgevers mo on mo.materiaalId = b.id
  where  mm.[min] is not null and m.id = @magazijnid and  (mo.actief = 1 or mo.actief is null)   and mo.opdrachtgeverId = m.OpdrachtgeverId) as minmaxmag
  where [min] &gt; (select Aantal from StockMagazijn s2 where s2.MateriaalId = minmaxmag.materiaalid and s2.MagazijnId = @magazijnid)">
        <SelectParameters>
            <asp:ControlParameter ControlID="ASPxComboBoxMagazijn" Name="magazijnid" PropertyName="Value" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxLabel ID="ASPxLabelGelukt" runat="server" Text="ASPxLabelOK" Font-Bold="True" ForeColor="#009900">
    </dx:ASPxLabel>

    <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" Text="ASPxLabel" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
    </dx:ASPxLabel>



    <br />

    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel2" runat="server" ClientInstanceName="ASPxLoadingPanel2">
    </dx:ASPxLoadingPanel>

    <br />

</asp:Content>
