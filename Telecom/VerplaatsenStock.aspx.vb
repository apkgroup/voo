﻿Imports System.Data.SqlClient
Imports DevExpress.Web
Imports Microsoft.ajax

Public Class VerplaatsenStock
    Inherits System.Web.UI.Page
    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property



    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then
            ASPxLabel1.Text = "Artikel"
            ASPxLabel2.Text = "Aantal"
            ASPxLabelLotnummer.Text = "Lotnummer"

            ASPxGridView2.Columns("Article").Caption = "Artikel"
            ASPxGridView2.Columns("Description").Caption = "Omschrijving"
            ASPxGridView2.Columns("Aantal").Caption = "Aantal"

        Else
            ASPxLabelLotnummer.Text = "Numéro de lot"



        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))


        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer



        voocontext.Dispose()

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()

        End If




        If Page.IsPostBack Then
            ASPxLabelFout.Text = Session("fout")
            Session("fout") = ""
            Session("Gelukt") = ""


            Dim i As Int32 = 1
            For Each textboxId As String In TextBoxIdCollection
                If i <= ASPxSpinEditAantal.Value Then


                    Dim label As New Label
                    If Session("taal") = 1 Then
                        label.Text = "Serienummer " & i & ":  "
                    Else
                        label.Text = "Numéro de série " & i & ":  "
                    End If

                    PanelSerie.Controls.Add(label)
                    Dim textbox = New TextBox With {
                .ID = textboxId
            }

                    Dim test As Integer = ASPxSpinEditAantal.Value
                    Dim br As New HtmlGenericControl("br")
                    If i = TextBoxIdCollection.Count Then
                        AddHandler textbox.TextChanged, AddressOf Me.txt_changed
                        textbox.AutoPostBack = True

                    Else
                        RemoveHandler textbox.TextChanged, AddressOf Me.txt_changed
                        textbox.AutoPostBack = False
                    End If
                    PanelSerie.Controls.Add(textbox)
                    PanelSerie.Controls.Add(br)
                    i += 1
                End If
            Next
        Else
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using

            Session("fout") = ""
            Session("Gelukt") = ""
            ASPxLabelFout.Text = Session("fout")
            ASPxLabelGelukt.Text = Session("Gelukt")
        End If





    End Sub




    Protected Sub ASPxComboBoxArtikel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxArtikel.SelectedIndexChanged
        ASPxSpinEditAantal.Number = 1
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            Session("changed") = True
            cn.Open()
            Dim s_SQL As String = "select HeeftSerienummer, [SerieVerplichtBijOpboeken] from basismateriaal where id=@id"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
            cmd.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()
                If Not IsDBNull(dr2.Item(0)) Then
                    If dr2.Item(0) = True Then
                        ASPxSpinEditAantal.Number = 1

                        HiddenArtikel("heeftSerienummer") = dr2.Item(0)
                        HiddenArtikel("SerieVerplichtBijOpboeken") = dr2.Item(1)

                        ASPxTextBoxLotnummer.Visible = True
                        ASPxLabelLotnummer.Visible = True
                        ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)

                    Else
                        ASPxSpinEditAantal.Number = 1
                        HiddenArtikel("heeftSerienummer") = dr2.Item(0)
                        HiddenArtikel("SerieVerplichtBijOpboeken") = dr2.Item(1)

                        ASPxTextBoxLotnummer.Visible = False
                        ASPxLabelLotnummer.Visible = False
                    End If
                Else
                    ASPxSpinEditAantal.Number = 1
                    HiddenArtikel("heeftSerienummer") = False
                    HiddenArtikel("SerieVerplichtBijOpboeken") = False
                    ASPxTextBoxLotnummer.Visible = False
                    ASPxLabelLotnummer.Visible = False

                End If
            End If

            dr2.Close()

            cn.Close()

        End Using


        'Dim voocontext As New VooEntities
        'Dim matid As Integer = ASPxComboBoxArtikel.Value
        'Dim stockmagazijnen = voocontext.Basismateriaal.Find(matid).StockMagazijn
        'For Each serienummer In voocontext.Serienummer.ToList
        '    If stockmagazijnen.Where(Function(x) x.id = serienummer.StockMagazijn.id).Any Then
        '        'er bestaat een serienummer met de stockmagazijn van dit materiaalId
        '        HiddenArtikel("heeftSerienummer") = True
        '        HiddenArtikel("SerieVerplichtBijOpboeken") = True

        '        ASPxTextBoxLotnummer.Visible = False
        '        ASPxLabelLotnummer.Visible = False
        '    End If

        'Next


        'Dim magid As Integer = Session("MagazijnId")
        'Dim artid As Integer = ASPxComboBoxArtikel.Value
        'Dim inStock As Decimal = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = artid).FirstOrDefault.Aantal
        'HiddenArtikel("artikelMax") = inStock
        ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)






    End Sub

    Protected Sub txt_changed(sender As Object, e As EventArgs)
        ASPxSpinEditAantal.Value += 1

        ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)
    End Sub
    Protected Sub ASPxSpinEditAantal_NumberChanged(sender As Object, e As EventArgs) Handles ASPxSpinEditAantal.ValueChanged
        If ASPxComboBoxArtikel.Value IsNot Nothing Then


            Dim context As New VooEntities
            Dim artid As Integer = ASPxComboBoxArtikel.Value

            If HiddenArtikel.Count <> 0 Then


                If Not IsNothing(context.Basismateriaal.Find(artid).SerieVerplichtBijOpboeken) AndAlso context.Basismateriaal.Find(artid).SerieVerplichtBijOpboeken Then



                    Dim newlist As New List(Of String)
                    For i As Integer = 1 To ASPxSpinEditAantal.Value
                        If Not TextBoxIdCollection.Contains(Today.Date.ToShortDateString & "_Serienummer_" & ASPxComboBoxArtikel.Value & "_" & i) Then

                            Dim txt = New TextBox With {.ID = Today.Date.ToShortDateString & "_Serienummer_" & ASPxComboBoxArtikel.Value & "_" & i}

                            'txt.ClientInstanceName = bestellijn.materiaalId & "_" & i
                            'ASPxPanelSerie.Controls.Add(txt)

                            Dim br As New HtmlGenericControl("br")
                            Dim label As New Label
                            If Session("taal") = 1 Then
                                label.Text = "Serienummer " & i & ":  "
                            Else
                                label.Text = "Numéro de série " & i & ":  "
                            End If
                            If Session("changed") Then
                                PanelSerie.Controls.Clear()
                            End If
                            Session("changed") = False
                            PanelSerie.Controls.Add(label)
                            PanelSerie.Controls.Add(txt)
                            PanelSerie.Controls.Add(br)

                            If i = ASPxSpinEditAantal.Value Then
                                'Bij Max event voor nieuwe textbox te maken
                                AddHandler txt.TextChanged, AddressOf Me.txt_changed

                                txt.AutoPostBack = True
                                txt.Focus()

                            Else
                                txt.AutoPostBack = False
                                RemoveHandler txt.TextChanged, AddressOf Me.txt_changed
                            End If


                        End If

                        newlist.Add(Today.Date.ToShortDateString & "_Serienummer_" & ASPxComboBoxArtikel.Value & "_" & i)
                    Next

                    TextBoxIdCollection = newlist
                End If
            Else

            End If

            If Not context.Basismateriaal.Find(artid).HeeftSerienummer Then
                PanelSerie.Controls.Clear()
            End If
        End If
    End Sub




    Protected Sub ASPxComboBoxArtikel_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxArtikel.PreRender
        Dim cb As ASPxComboBox = CType(sender, ASPxComboBox)
        cb.TextFormatString = "{0}"
    End Sub

    'Protected Sub ASPxCallback1_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback1.Callback
    '    Dim Aantalinserted As Int32 = ASPxSpinEditAantal.Value
    '    Dim voocontext As New VooEntities
    '    Dim magid As Integer = Session("MagazijnId")
    '    Dim desc As String = ""
    '    Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
    '        cn.Open()

    '        Dim s_SQL As String = "select description from basismateriaal where id=@id"
    '        Dim cmd As New SqlCommand(s_SQL, cn)
    '        Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
    '        cmd.Parameters.Add(par)
    '        Dim dr As SqlDataReader = cmd.ExecuteReader
    '        If dr.HasRows Then
    '            dr.Read()
    '            If Not IsDBNull(dr.Item(0)) Then
    '                desc = dr.Item(0)

    '            End If
    '        End If

    '        dr.Close()
    '        cn.Close()
    '    End Using

    '    If HiddenArtikel("SerieVerplichtBijOpboeken") Then
    '        Aantalinserted = 0
    '        ASPxTextBoxLotnummer.Visible = True
    '        ASPxLabelLotnummer.Visible = True
    '        Dim nrs As New List(Of String)
    '        For Each textboxId As String In TextBoxIdCollection
    '            Dim tb As TextBox = PanelSerie.FindControl(textboxId)
    '            Dim value As String = tb.Text
    '            If nrs.Contains(value) Then
    '                If Session("taal") = 1 Then
    '                    Session("fout") = "Serienummer " & value & " Staat 2 keer in de lijst om in te lezen! Serienummer kan niet worden toegevoegd. Wijzigingen zijn niet doorgevoerd."

    '                Else
    '                    Session("fout") = "Numéro de série " & value & " Peut être lu deux fois dans la liste! Le numéro de série ne peut pas être ajouté. Les changements n'ont pas été mis en œuvre."

    '                End If
    '                Return
    '            End If
    '            nrs.Add(value)
    '        Next

    '        For Each textboxId As String In TextBoxIdCollection
    '            Dim tb As TextBox = PanelSerie.FindControl(textboxId)
    '            Dim value As String = tb.Text
    '            'enkel actie uitvoeren als textbox ingevuld is
    '            If Not String.IsNullOrWhiteSpace(value) Then
    '                Dim id As String = (CType(tb, TextBox)).ID
    '                Dim parts As String() = id.Split(New Char() {"_"c})
    '                Dim matId As Integer = parts(1).ToString
    '                If voocontext.Serienummer.Where(Function(x) x.serienummer1 = value).Any Then
    '                    If Session("taal") = 1 Then
    '                        Session("fout") = "Serienummer " & value & " bestaat al! Serienummer kan niet worden toegevoegd. Wijzigingen zijn niet doorgevoerd."

    '                    Else
    '                        Session("fout") = "Le numéro de série " & value & " existe déjà! Le numéro de série ne peut pas être ajouté. Aucune modification n'a été apportée."
    '                    End If
    '                    Return
    '                End If
    '                Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '                Dim serienummer As New Serienummer

    '                serienummer.StockMagazijnId = Stockmat.id
    '                serienummer.serienummer1 = value
    '                serienummer.uitgeboekt = 0
    '                serienummer.lotNr = ASPxTextBoxLotnummer.Text
    '                serienummer.datumGemaakt = Today()
    '                serienummer.statusId = 3
    '                voocontext.Serienummer.Add(serienummer)
    '                Aantalinserted += 1

    '                Dim logSerie As New Log
    '                logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " ingelezen op hoofdmagazijn "
    '                logSerie.Tijdstip = Today
    '                logSerie.Gebruiker = Session("userid")
    '                voocontext.Log.Add(logSerie)
    '            End If

    '        Next
    '        voocontext.SaveChanges()


    '        'For Each ctr As Control In PlaceHolderSerie.Controls
    '        '    If TypeOf (ctr) Is TextBox Then
    '        '        Dim id As String = (CType(ctr, TextBox)).ID
    '        '        Dim value As String = (CType(ctr, TextBox)).Text
    '        '        Dim parts As String() = id.Split(New Char() {"_"c})
    '        '        Dim matId As Integer = Convert.ToInt32(parts(0))

    '        '        Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '        '        Dim serienummer As New Serienummer
    '        '        serienummer.StockMagazijnId = Stockmat.id
    '        '        serienummer.serienummer1 = value
    '        '        serienummer.uitgeboekt = 0
    '        '        voocontext.Serienummer.Add(serienummer)

    '        '        Dim logSerie As New Log
    '        '        logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " ingelezen op hoofdmagazijn "
    '        '        logSerie.Tijdstip = Today
    '        '        logSerie.Gebruiker = Session("userid")
    '        '        voocontext.Log.Add(logSerie)
    '        '        voocontext.SaveChanges()
    '        '    End If
    '        'Next
    '        voocontext.SaveChanges()
    '    End If


    '    'UPDATE STOCKARTIKEL
    '    Dim context As New VooEntities

    '    Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
    '    Dim stockart = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
    '    stockart.Aantal = stockart.Aantal + Aantalinserted


    '    'LOG
    '    Dim log As New Log
    '    log.Actie = "Gebruiker " & Session("userid") & " Heeft " & Aantalinserted & " Van artikel " & desc & "ingelezen op hoofdmagazijn " & Session("uitgevoerdwerk")
    '    log.Tijdstip = Today
    '    log.Gebruiker = Session("userid")
    '    context.Log.Add(log)
    '    context.SaveChanges()

    '    context.Dispose()
    '    Session("Gelukt") = Aantalinserted & " van artikel " & desc & " toegevoegd."
    '    Session("GeluktCB") = True
    '    Session("Fout") = ""
    '    ASPxLabelGelukt.Visible = True
    '    ASPxSpinEditAantal.Value = 0
    '    ASPxComboBoxArtikel.Value = 0
    '    ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)


    'End Sub


    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [StockMagazijnId] from [Serienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("StockMagazijnId"))
        Next row
        Session("SelectResult") = result
    End Sub


    'Protected Sub ASPxCallback2_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback2.Callback
    '    Dim voocontext As New VooEntities
    '    Dim magid As Integer = Session("MagazijnId")
    '    Dim desc As String = ""
    '    Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
    '        cn.Open()

    '        Dim s_SQL As String = "select description from basismateriaal where id=@id"
    '        Dim cmd As New SqlCommand(s_SQL, cn)
    '        Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
    '        cmd.Parameters.Add(par)
    '        Dim dr As SqlDataReader = cmd.ExecuteReader
    '        If dr.HasRows Then
    '            dr.Read()
    '            If Not IsDBNull(dr.Item(0)) Then
    '                desc = dr.Item(0)

    '            End If
    '        End If

    '        dr.Close()
    '        cn.Close()
    '    End Using

    '    If HiddenArtikel("SerieVerplichtBijOpboeken") Then
    '        ASPxTextBoxLotnummer.Visible = True
    '        ASPxLabelLotnummer.Visible = True
    '        For Each textboxId As String In TextBoxIdCollection
    '            Dim tb As TextBox = PanelSerie.FindControl(textboxId)
    '            Dim value As String = tb.Text

    '            Dim id As String = (CType(tb, TextBox)).ID
    '            Dim parts As String() = id.Split(New Char() {"_"c})
    '            Dim matId As Integer = parts(1).ToString


    '            Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '            Dim serienummer = voocontext.Serienummer.Where(Function(x) x.serienummer1 = value And x.uitgeboekt <> True).FirstOrDefault
    '            If Not IsNothing(serienummer) And serienummer.StockMagazijn.MagazijnId = magid Then
    '                serienummer.uitgeboekt = 1
    '                serienummer.statusId = 6
    '                serienummer.lotNr = ASPxTextBoxLotnummer.Text
    '                Dim logSerie As New Log
    '                logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " uitgeboekt van hoofdmagazijn "
    '                logSerie.Tijdstip = Today
    '                logSerie.Gebruiker = Session("userid")
    '                voocontext.Log.Add(logSerie)
    '                voocontext.SaveChanges()
    '            Else

    '                ASPxLabelFout.Visible = True
    '                If Session("taal") = 1 Then
    '                    If IsNothing(serienummer) Then
    '                        ASPxLabelFout.Text = "Onbestaande serienummer " & value & " uitgeboekt. Gelieve te controleren en opnieuw te proberen."
    '                    Else
    '                        ASPxLabelFout.Text = "Serienummer " & value & " behoort niet tot het hoofdmagazijn. Gelieve te controleren en opnieuw te proberen."

    '                    End If
    '                Else
    '                    If IsNothing(serienummer) Then
    '                        ASPxLabelFout.Text = "Numéro de série inexistant " & value & " annulé. Veuillez vérifier et réessayer."
    '                    Else
    '                        ASPxLabelFout.Text = "Le numéro de série " & value & " n'appartient pas à l'entrepôt principal. Veuillez vérifier et réessayer."

    '                    End If
    '                End If

    '                Return

    '            End If
    '        Next


    '        'For Each ctr As Control In PlaceHolderSerie.Controls
    '        '    If TypeOf (ctr) Is TextBox Then
    '        '        Dim id As String = (CType(ctr, TextBox)).ID
    '        '        Dim value As String = (CType(ctr, TextBox)).Text
    '        '        Dim parts As String() = id.Split(New Char() {"_"c})
    '        '        Dim matId As Integer = Convert.ToInt32(parts(0))

    '        '        Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '        '        Dim serienummer As New Serienummer
    '        '        serienummer.StockMagazijnId = Stockmat.id
    '        '        serienummer.serienummer1 = value
    '        '        serienummer.uitgeboekt = 0
    '        '        voocontext.Serienummer.Add(serienummer)

    '        '        Dim logSerie As New Log
    '        '        logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " ingelezen op hoofdmagazijn "
    '        '        logSerie.Tijdstip = Today
    '        '        logSerie.Gebruiker = Session("userid")
    '        '        voocontext.Log.Add(logSerie)
    '        '        voocontext.SaveChanges()
    '        '    End If
    '        'Next
    '        voocontext.SaveChanges()
    '    End If



    '    'UPDATE STOCKARTIKEL
    '    Dim context As New VooEntities

    '    Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
    '    Dim stockart = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
    '    stockart.Aantal = stockart.Aantal - ASPxSpinEditAantal.Number


    '    'LOG
    '    Dim log As New Log
    '    log.Actie = "Gebruiker " & Session("userid") & " Heeft " & ASPxSpinEditAantal.Number & " Van artikel " & desc & "uitgeboekt van hoofdmagazijn " & Session("uitgevoerdwerk")
    '    log.Tijdstip = Today
    '    log.Gebruiker = Session("userid")
    '    context.Log.Add(log)
    '    context.SaveChanges()

    '    context.Dispose()
    '    ASPxSpinEditAantal.Value = 0
    '    ASPxComboBoxArtikel.Value = 0
    'End Sub

    Protected Sub ASPxGridView2_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView2.DataBinding
        DoSelect(SqlDataSourceStockMagazijn.ConnectionString)
    End Sub

    Protected Sub ASPxGridView2_DetailRowGetButtonVisibility(sender As Object, e As ASPxGridViewDetailRowButtonEventArgs) Handles ASPxGridView2.DetailRowGetButtonVisibility
        If Not (CType(Session("SelectResult"), ArrayList)).Contains(e.KeyValue) Then
            e.ButtonState = GridViewDetailRowButtonState.Hidden
        End If
    End Sub

    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("stockmagazijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub

    Protected Sub ASPxButtonVerminderen_Click(sender As Object, e As EventArgs) Handles ASPxButtonVerminderen.Click
        If String.IsNullOrEmpty(ASPxComboBoxMagazijnNaar.Value) Then
            If Session("taal") = 1 Then
                Session("fout") = "Geen doelmagazijn geselecteerd!"

            Else
                Session("fout") = "Aucun Magasin sélectionné!"

            End If

            Session("Gelukt") = ""
            ASPxLabelGelukt.Text = Session("Gelukt")
            ASPxLabelFout.Text = Session("fout")
            Return
        End If

        Dim Aantalinserted As Int32 = ASPxSpinEditAantal.Value
        Dim voocontext As New VooEntities
        Dim magid As Integer = Session("MagazijnId")
        Dim magazijnIdNaar As Integer = ASPxComboBoxMagazijnNaar.Value
        Dim desc As String = ""
        If String.IsNullOrWhiteSpace(ASPxComboBoxArtikel.Value) Then
            If Session("taal") = 1 Then
                Session("fout") = "Geen artikel geselecteerd!"

            Else
                Session("fout") = "Aucun article sélectionné!"

            End If

            Session("Gelukt") = ""
            ASPxLabelGelukt.Text = Session("Gelukt")
            ASPxLabelFout.Text = Session("fout")
            Return
        End If

        If ASPxComboBoxMagazijn.Value = ASPxComboBoxMagazijnNaar.Value Then
            Session("fout") = "Magazijnen mogen niet gelijk zijn"
            Session("Gelukt") = ""
            ASPxLabelGelukt.Text = Session("Gelukt")
            ASPxLabelFout.Text = Session("fout")
            Return
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim LinkLeggenSQL As String = "select id from [voo].[dbo].stockmagazijn where materiaalId = @materiaalId and magazijnId =@doelmagazijn"

            Dim cmd As New SqlCommand(LinkLeggenSQL, cn)
            Dim par As New SqlParameter("@materiaalId", SqlDbType.Int, -1) With {.Value = ASPxComboBoxArtikel.Value}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@doelmagazijn", SqlDbType.Int, -1) With {.Value = ASPxComboBoxMagazijnNaar.Value}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If Not dr.HasRows Then
                dr.Close()
                If Session("taal") = 1 Then
                    Session("fout") = "Artikel bestaat niet bij doelmagazijn!"

                Else
                    Session("fout") = "Article ne pas existé chez l'autre magasin!"

                End If

                Session("Gelukt") = ""
                ASPxLabelGelukt.Text = Session("Gelukt")
                ASPxLabelFout.Text = Session("fout")
                Return
            Else
                dr.Close()

            End If

            Dim s_SQL As String = "select description from basismateriaal where id=@id"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
            cmd.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()
                If Not IsDBNull(dr2.Item(0)) Then
                    desc = dr2.Item(0)

                End If
            End If

            dr2.Close()
            cn.Close()
        End Using
        Dim lijstserie As New List(Of String)
        If HiddenArtikel("SerieVerplichtBijOpboeken") Then
            Aantalinserted = 0
            ASPxTextBoxLotnummer.Visible = True
            ASPxLabelLotnummer.Visible = True

            For Each textboxId As String In TextBoxIdCollection

                Dim tb As TextBox = PanelSerie.FindControl(textboxId)
                Dim value As String = tb.Text.Trim()

                Dim id As String = (CType(tb, TextBox)).ID
                Dim parts As String() = id.Split(New Char() {"_"c})
                Dim matId As Integer = parts(2).ToString

                If Not String.IsNullOrWhiteSpace(value) Then

                    'origineel stockmagazijn
                    Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault


                    Dim serienummer = voocontext.Serienummer.Where(Function(x) x.serienummer1 = value).FirstOrDefault
                    If Not IsNothing(serienummer) AndAlso serienummer.uitgeboekt = False Then
                        If Not serienummer.statusId = 3 And Not serienummer.statusId = 4 And Not serienummer.statusId = 2 Then
                            ASPxLabelFout.Text = "Serienummer is al gebruikt, defect of gereserveerd . Kan niet verplaatst worden!"
                            Return
                        End If

                        Dim materiaalId As Integer = serienummer.StockMagazijn.MateriaalId
                        If Not serienummer.StockMagazijnId = Stockmat.id Then
                            ASPxLabelFout.Text = "Serienummer " & serienummer.serienummer1 & " staat niet op bronmagazijn!"
                            Return
                        End If

                        serienummer.StockMagazijnId = voocontext.StockMagazijn.Where(Function(x) x.MateriaalId = materiaalId And x.MagazijnId = magazijnIdNaar).FirstOrDefault.id

                        If voocontext.StockMagazijn.Where(Function(x) x.MateriaalId = materiaalId And x.MagazijnId = magazijnIdNaar).FirstOrDefault.Magazijn.Hoofd = True Then
                            serienummer.statusId = 3
                        Else
                            serienummer.statusId = 4
                        End If
                        serienummer.lotNr = ASPxTextBoxLotnummer.Text
                        Dim logSerie As New Log
                        logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " verplaats van magazijn" & ASPxComboBoxMagazijn.Text & " naar  " & ASPxComboBoxMagazijnNaar.Text
                        logSerie.Tijdstip = Today
                        logSerie.Gebruiker = Session("userid")
                        voocontext.Log.Add(logSerie)

                        Aantalinserted += 1
                        lijstserie.Add(serienummer.serienummer1)
                    Else

                        ASPxLabelFout.Visible = True
                        If Session("taal") = 1 Then
                            Session("Gelukt") = ""
                            ASPxLabelGelukt.Text = Session("Gelukt")
                            If IsNothing(serienummer) Then
                                ASPxLabelFout.Text = "Onbestaande serienummer " & value & " uitgeboekt. Gelieve te controleren en opnieuw te proberen."
                            Else
                                ASPxLabelFout.Text = "Serienummer " & value & " is al uitgeboekt."

                            End If
                        Else
                            If IsNothing(serienummer) Then
                                ASPxLabelFout.Text = "Numéro de série inexistant " & value & " annulé. Veuillez vérifier et réessayer."
                            Else
                                ASPxLabelFout.Text = "Le numéro de série " & value & " n'appartient pas à l'entrepôt principal. Veuillez vérifier et réessayer."

                            End If
                        End If

                        Return

                    End If
                End If
            Next



            'voocontext.SaveChanges()
        End If



        'UPDATE STOCKARTIKELs


        Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
        Dim stockart = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
        stockart.Aantal = stockart.Aantal - Aantalinserted
        Dim opid As Integer = Session("Opdrachtgever")
        Dim aantalgereserveerd As Integer = 0
        If voocontext.Bestellijn.Where(Function(x) x.Bestellingen.Status = 4 AndAlso x.materiaalId = stockart.MateriaalId And x.Bestellingen.Gebruikers.OpdrachtgeverId = opid).Any Then
            aantalgereserveerd = voocontext.Bestellijn.Where(Function(x) x.Bestellingen.Status = 4 AndAlso x.materiaalId = stockart.MateriaalId And x.Bestellingen.Gebruikers.OpdrachtgeverId = opid).Sum(Function(y) y.hoeveelheid)

        End If

        If stockart.Aantal - aantalgereserveerd < 0 Then
            Session("Fout") = "Bronmagazijn heeft niet genoeg stock voor deze boeking!"
            ASPxLabelFout.Text = Session("Fout")
            voocontext.Dispose()
            ASPxSpinEditAantal.Value = 0
            ASPxComboBoxArtikel.Value = ""
            ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)
        Else
            Dim stockartNaar = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magazijnIdNaar And x.MateriaalId = basisId).FirstOrDefault
            stockartNaar.Aantal = stockartNaar.Aantal + Aantalinserted
            Session("Gelukt") = Aantalinserted & " van artikel " & desc & " veplaats van magazijn " & ASPxComboBoxMagazijn.Text & " Naar magazijn " & ASPxComboBoxMagazijnNaar.Text
            ASPxLabelGelukt.Text = Session("Gelukt")
            ASPxLabelFout.Text = ""
            'LOG
            Dim log As New Log
            log.Actie = "Gebruiker " & Session("userid") & " Heeft " & Aantalinserted & " van artikel " & desc & " veplaats van magazijn " & ASPxComboBoxMagazijn.Text & " Naar magazijn " & ASPxComboBoxMagazijnNaar.Text
            log.Tijdstip = Today
            log.Gebruiker = Session("userid")
            voocontext.Log.Add(log)
            voocontext.SaveChanges()

            Dim sb As New Stockbeweging
            sb.beweging = -Aantalinserted
            sb.datum = DateTime.Now
            sb.gebruiker = Session("userid")
            sb.opmerking = "Verplaatsen stock (van)"
            sb.stockmagazijnId = stockart.id
            voocontext.Stockbeweging.Add(sb)

            voocontext.SaveChanges()

            For Each serie In lijstserie
                Dim sbsr As New StockbewegingSerienummer
                sbsr.stockbewegingid = sb.id
                sbsr.serienummerId = voocontext.Serienummer.Where(Function(x) x.serienummer1 = serie).FirstOrDefault.id
                voocontext.StockbewegingSerienummer.Add(sbsr)
                voocontext.SaveChanges()
            Next

            Dim smid As Int32 = stockart.id
            If stockart.Basismateriaal.SerieVerplichtBijOpboeken And stockart.Aantal <> voocontext.Serienummer.Where(Function(x) x.StockMagazijnId = stockart.id And x.uitgeboekt = False And (x.statusId = 1 Or x.statusId = 2 Or x.statusId = 3 Or x.statusId = 4)).Count() Then
                'Bij mat met serienummer, als aantal niet matched
                Dim addresses As New List(Of String)
                addresses.Add("conan.dufour@apkgroup.eu")
                mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                       "noreply@apkgroup.eu",
                       addresses,
                       "Fout is opgetreden bij verplaatsen stock. Raadpleeg stockbeweging" & sb.id,
                       New Dictionary(Of String, Byte()))
            End If


            Dim sbnaar As New Stockbeweging
            sbnaar.beweging = Aantalinserted
            sbnaar.datum = DateTime.Now
            sbnaar.gebruiker = Session("userid")
            sbnaar.opmerking = "verplaatsen Stock (naar)"
            sbnaar.stockmagazijnId = stockartNaar.id
            voocontext.Stockbeweging.Add(sbnaar)


            For Each serie In lijstserie
                Dim sbsr As New StockbewegingSerienummer
                sbsr.stockbewegingid = sbnaar.id
                sbsr.serienummerId = voocontext.Serienummer.Where(Function(x) x.serienummer1 = serie).FirstOrDefault.id
                voocontext.StockbewegingSerienummer.Add(sbsr)
            Next


            Dim smnaarid As Int32 = stockartNaar.id
            If stockartNaar.Basismateriaal.SerieVerplichtBijOpboeken And stockartNaar.Aantal <> voocontext.Serienummer.Where(Function(x) x.StockMagazijnId = smnaarid And x.uitgeboekt = False And (x.statusId = 1 Or x.statusId = 2 Or x.statusId = 3 Or x.statusId = 4)).Count() Then
                'Bij mat met serienummer, als aantal niet matched
                Dim addresses As New List(Of String)
                addresses.Add("conan.dufour@apkgroup.eu")
                mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                       "noreply@apkgroup.eu",
                       addresses,
                       "Fout is opgetreden bij verplaatsen stock. Raadpleeg stockbeweging" & sbnaar.id,
                       New Dictionary(Of String, Byte()))
            End If
            voocontext.SaveChanges()
            voocontext.Dispose()
            ASPxSpinEditAantal.Value = 0
            ASPxComboBoxArtikel.Value = ""
            ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)
        End If

        ASPxGridView2.DataBind()
        ASPxGridView4.DataBind()
    End Sub

    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender

        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) And ASPxComboBoxMagazijn.SelectedIndex = -1 Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        If Not String.IsNullOrWhiteSpace(ASPxComboBoxMagazijn.Value) And Not String.IsNullOrWhiteSpace(ASPxComboBoxMagazijnNaar.Value) Then


            Dim magazijnNaarId As Integer = ASPxComboBoxMagazijnNaar.Value
            Dim magazijnVanId As Integer = ASPxComboBoxMagazijn.Value

            If magazijnNaarId = magazijnVanId Then
                Return
            End If

            Dim entities As New VooEntities
            Dim MagVan = entities.Magazijn.Find(magazijnVanId)

            If MagVan.Hoofd Then
                Session("fout") = "Kan hoofdmagazijn niet in zijn geheel verplaatsen"
                Session("Gelukt") = ""
                ASPxLabelGelukt.Text = Session("Gelukt")
                ASPxLabelFout.Text = Session("fout")
                Return
            End If
            Dim MagNaar = entities.Magazijn.Find(magazijnNaarId)

            For Each stockmagazijn In MagVan.StockMagazijn.ToList
                If stockmagazijn.Aantal > 0 Then
                    If entities.Basismateriaal.Find(stockmagazijn.MateriaalId).bobijnArtikel Then
                        Continue For
                    End If

                    Dim listserie As New List(Of String)
                    Dim verplaatsing = stockmagazijn.Aantal
                    MagNaar.StockMagazijn.Where(Function(x) x.MateriaalId = stockmagazijn.MateriaalId).FirstOrDefault.Aantal += stockmagazijn.Aantal
                    stockmagazijn.Aantal = 0

                    For Each serienummer In stockmagazijn.Serienummer
                        serienummer.StockMagazijnId = MagNaar.StockMagazijn.Where(Function(x) x.MateriaalId = stockmagazijn.MateriaalId).FirstOrDefault.id
                        listserie.Add(serienummer.id)
                    Next

                    Dim sbvan As New Stockbeweging
                    sbvan.datum = Now
                    sbvan.beweging = -verplaatsing
                    sbvan.gebruiker = Session("userid")
                    sbvan.stockmagazijnId = stockmagazijn.id
                    sbvan.opmerking = "Verplaatsen stock (van) ALL"
                    entities.Stockbeweging.Add(sbvan)


                    Dim sbnaar As New Stockbeweging
                    sbnaar.datum = Now
                    sbnaar.gebruiker = Session("userid")
                    sbnaar.beweging = verplaatsing
                    sbvan.opmerking = "Verplaatsen stock (naar) ALL"
                    sbnaar.stockmagazijnId = MagNaar.StockMagazijn.Where(Function(x) x.MateriaalId = stockmagazijn.MateriaalId).FirstOrDefault.id
                    entities.Stockbeweging.Add(sbnaar)
                    entities.SaveChanges()

                    If listserie.Count > 0 Then
                        For Each serieId In listserie
                            Dim sbvansr As New StockbewegingSerienummer
                            sbvansr.serienummerId = serieId
                            sbvansr.stockbewegingid = sbvan.id
                            entities.StockbewegingSerienummer.Add(sbvansr)

                            Dim sbnaarsr As New StockbewegingSerienummer
                            sbnaarsr.serienummerId = serieId
                            sbnaarsr.stockbewegingid = sbnaar.id
                            entities.StockbewegingSerienummer.Add(sbnaarsr)
                        Next
                    End If
                End If
            Next
            entities.SaveChanges()
            ASPxGridView2.DataBind()
            ASPxGridView4.DataBind()
        End If
    End Sub
End Class