﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Logs.aspx.vb" Inherits="Telecom.Logs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Raadplegen gelogde transacties</h2>    
    
    <br />
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceLogs" KeyFieldName="id">
        <SettingsText EmptyDataRow="Geen data gevonden... " />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Gebruiker" FieldName="Column1" ReadOnly="True" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="Tijdstip" VisibleIndex="2">
                <PropertiesDateEdit DisplayFormatString="g">
                </PropertiesDateEdit>
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="Actie" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
        </Columns>
        <Settings ShowHeaderFilterButton="True" />
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <SettingsText SearchPanelEditorNullText="Typ hier tekst om te zoeken..." />
        <SettingsSearchPanel Visible="true" />
        <SettingsPager PageSize="20">
            <PageSizeItemSettings Caption="Items per pagina:" Items="10, 20, 50" ShowAllItem="True" Visible="True">
            </PageSizeItemSettings>
        </SettingsPager>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceLogs" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id]
      ,(select top 1 vwWn.Naam + ' ' + vwWn.VNaam as Gebruiker from Gebruikers inner join Elly_SQL.dbo.Werkn vwWn ON Gebruikers.Werkg=vwWn.[ON] AND Gebruikers.Werkn=vwWn.NR WHERE Gebruikers.id=Log.[Gebruiker])
      ,[Tijdstip]
      ,[Actie]
  FROM [dbo].[Log] where (Log.Gebruiker>1) order by Tijdstip desc"></asp:SqlDataSource>
</asp:Content>
