﻿Imports System.Data.SqlClient

Public Class Documenten
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If


        If Session("level") = 1 Then
            ASPxFileManager1.SettingsUpload.Enabled = True


        Else
            ASPxFileManager1.SettingsUpload.Enabled = False
            ASPxFileManager1.SettingsToolbar.ShowCreateButton = False
            ASPxFileManager1.SettingsToolbar.ShowDeleteButton = False
            ASPxFileManager1.SettingsToolbar.ShowRenameButton = False
            ASPxFileManager1.SettingsToolbar.ShowMoveButton = False
            ASPxFileManager1.SettingsToolbar.ShowCopyButton = False

        End If




    End Sub

    Protected Sub ASPxFileManager1_CustomCallback(sender As Object, e As DevExpress.Web.CallbackEventArgsBase) Handles ASPxFileManager1.CustomCallback

    End Sub

    Protected Sub ASPxFileManager1_FileDownloading(source As Object, e As DevExpress.Web.FileManagerFileDownloadingEventArgs) Handles ASPxFileManager1.FileDownloading
        Dim id As Integer = ASPxFileManager1.SelectedFile.Id
        Dim bytes As Byte()
        Dim fileName As String, contentType As String
        Dim constr As String = ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand()
                cmd.CommandText = "select Name, Data from [dbo].[FileSystem] where Id=@Id"
                cmd.Parameters.AddWithValue("@Id", id)
                cmd.Connection = con
                con.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    sdr.Read()
                    bytes = DirectCast(sdr("Data"), Byte())
                    'contentType = sdr("ContentType").ToString()
                    fileName = sdr("Name").ToString()
                End Using
                con.Close()
            End Using
        End Using
        Response.Clear()
        Response.Buffer = True
        Response.Charset = ""
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.ContentType = contentType
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub


End Class