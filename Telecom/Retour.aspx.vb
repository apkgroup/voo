﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports DevExpress.Web

Public Class Retour
    Inherits System.Web.UI.Page
    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then
            ASPxLabel1.Text = "Artikel"
            ASPxLabel2.Text = "Aantal"
            ASPxLabel10.Text = "Reden:"
            ASPxLabel3.Text = "Serienummer"
            ASPxLabel4.Text = "Serienummer"
            ASPxLabel5.Text = "Serienummer"
            ASPxLabel6.Text = "Serienummer"
            ASPxLabel7.Text = "Serienummer"
            ASPxLabel8.Text = "Serienummer"
            ASPxLabel9.Text = "Serienummer"

        Else



        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not String.IsNullOrEmpty(Session("RetourKlaar")) Then
            ASPxLabelResult.ForeColor = Color.DarkGreen
            ASPxLabelResult.Text = "Retour OK"
        ElseIf Not String.IsNullOrEmpty(Session("RetourFout")) Then
            ASPxLabelResult.ForeColor = Color.DarkRed
            ASPxLabelResult.Text = Session("RetourFout")
        Else
            ASPxLabelResult.Text = ""
        End If
        Session("RetourFout") = ""
        Session("RetourKlaar") = ""


    End Sub

    Protected Sub ASPxCallback1_Callback(source As Object, e As DevExpress.Web.CallbackEventArgs) Handles ASPxCallback1.Callback
        'UPDATE STOCKARTIKEL
        'Dim context As New VooEntities
        'Dim magid As Integer = context.Gebruikers.Find(Session("userid")).MagazijnId
        'Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
        'Dim stockart = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
        'stockart.Aantal = stockart.Aantal - ASPxSpinEditAantal.Number
        Dim voocontext As New VooEntities
        Dim magId As Integer = ASPxComboBoxMagazijn.Value
        Dim matId As Int32 = ASPxComboBoxArtikel.Value
        Dim aantal As Decimal = ASPxSpinEditAantal.Value

        If voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magId And x.MateriaalId = matId).FirstOrDefault.Aantal < aantal Then
            'HOW MANNEKE GIJ ZIJT TE VEEL AAN T RETOURNEREN HE DOET EENS RUSTIG
            Session("RetourFout") = "Kan geen retour van een groter aantal dan huidig in stock uitvoeren. Controleer en probeer opnieuw."
            If Session("taal") = 1 Then
                Session("RetourFout") = "Kan geen retour van een groter aantal dan huidig in stock uitvoeren. Controleer en probeer opnieuw."
            Else
                Session("RetourFout") = " Impossible d'effectuer un retour d'un nombre plus important que celui actuellement en stock. Vérifiez et essayez à nouveau."

            End If
            Return

        End If

        Using cn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim trans As SqlTransaction = cn.BeginTransaction()

            Try
                Dim cmdR As SqlCommand = New SqlCommand("insert into [Voo].[dbo].RetourBestelling (gebruikerId, datum, status) values (@techId, @datenow, 1);SELECT SCOPE_IDENTITY(); ", cn)
                cmdR.Parameters.Add(New SqlParameter("@techId", SqlDbType.Int) With {
                    .Value = voocontext.Gebruikers.Where(Function(x) x.MagazijnId = magId).FirstOrDefault.id
                })
                cmdR.Parameters.Add(New SqlParameter("@datenow", SqlDbType.DateTime) With {
                    .Value = DateTime.Now
                })
                cmdR.Transaction = trans
                Dim retourId As Integer = Convert.ToInt32(cmdR.ExecuteScalar())

                cmdR = New SqlCommand("insert into [Voo].[dbo].RetourBestellijn (materiaalId, hoeveelheid, bestellingId) values (@matId, @hoeveelheid, @bestellingId);SELECT SCOPE_IDENTITY(); ", cn)
                cmdR.Parameters.Add(New SqlParameter("@matId", SqlDbType.Int) With {
                        .Value = Convert.ToInt32(ASPxComboBoxArtikel.Value)
                    })
                cmdR.Parameters.Add(New SqlParameter("@hoeveelheid", SqlDbType.Int) With {
                        .Value = ASPxSpinEditAantal.Number
                    })
                cmdR.Parameters.Add(New SqlParameter("@bestellingId", SqlDbType.Int) With {
                        .Value = retourId
                    })
                cmdR.Transaction = trans
                Dim bestellijnId As Integer = Convert.ToInt32(cmdR.ExecuteScalar())
                Dim cmdS As SqlCommand = New SqlCommand("Select HeeftSerienummer from voo.dbo.Basismateriaal where id = @matid", cn)
                cmdS.Parameters.Add(New SqlParameter("@matid", SqlDbType.Int) With {
                        .Value = Convert.ToInt32(ASPxComboBoxArtikel.Value)
                    })
                cmdS.Transaction = trans
                Dim drS As SqlDataReader = cmdS.ExecuteReader()
                Dim heeftSerienummer As Boolean = False

                While drS.Read()
                    heeftSerienummer = drS.GetBoolean(0)
                End While

                drS.Close()
                Dim cmdST As SqlCommand = New SqlCommand("Select Id from voo.dbo.Stockmagazijn where materiaalId = @matid and MagazijnId = (select MagazijnId from voo.dbo.Gebruikers where id = @techId)", cn)
                cmdST.Parameters.Add(New SqlParameter("@matid", SqlDbType.Int) With {
                        .Value = Convert.ToInt32(ASPxComboBoxArtikel.Value)
                    })
                cmdST.Parameters.Add(New SqlParameter("@techId", SqlDbType.Int) With {
                        .Value = Session("userid")
                    })
                cmdST.Transaction = trans
                Dim drST As SqlDataReader = cmdST.ExecuteReader()
                Dim stockmagazijnIdTech As Integer = 0

                While drST.Read()
                    stockmagazijnIdTech = drST.GetInt32(0)
                End While

                drST.Close()

                If heeftSerienummer Then
                    ''star
                    Dim nrs As New List(Of String)
                    For i = 1 To ASPxSpinEditAantal.Value
                        If nrs.Contains(ASPxSpinEditAantal.Value) Then
                            If Session("taal") = 1 Then
                                Session("RetourFout") = "Serienummer " & ASPxSpinEditAantal.Value & " Staat 2 keer in de lijst om in te lezen! Serienummer kan niet worden toegevoegd. Wijzigingen zijn niet doorgevoerd."

                            Else
                                Session("RetourFout") = "Numéro de série " & ASPxSpinEditAantal.Value & " Peut être lu deux fois dans la liste! Le numéro de série ne peut pas être ajouté. Les changements n'ont pas été mis en œuvre."

                            End If
                            Return
                        End If
                        nrs.Add(ASPxSpinEditAantal.Value)
                    Next
                    For i = 1 To ASPxSpinEditAantal.Value
                        Dim serienummer As String
                        Dim serienummerObject As Serienummer
                        Select Case i
                            Case 1
                                serienummer = ASPxTextBoxSerie1.Value
                            Case 2
                                serienummer = ASPxTextBoxSerie2.Value
                            Case 3
                                serienummer = ASPxTextBoxSerie3.Value
                            Case 4
                                serienummer = ASPxTextBoxSerie4.Value
                            Case 5
                                serienummer = ASPxTextBoxSerie5.Value
                            Case 6
                                serienummer = ASPxTextBoxSerie6.Value
                            Case 7
                                serienummer = ASPxTextBoxSerie7.Value
                        End Select
                        ''sta2
                        Dim cmdS2 As SqlCommand = New SqlCommand("Select serienummer, stockmagazijnId from [Voo].[dbo].[Serienummer] where serienummer = @serienummer", cn)
                        cmdS2.Parameters.Add(New SqlParameter("@serienummer", SqlDbType.NVarChar) With {
                                .Value = serienummer
                            })
                        cmdS2.Transaction = trans
                        Dim drS2 As SqlDataReader = cmdS2.ExecuteReader()
                        Dim stockmagazijnId As Integer = 0

                        While drS2.Read()
                            stockmagazijnId = drS2.GetInt32(1)
                        End While

                        drS2.Close()

                        If stockmagazijnId <> stockmagazijnIdTech Then

                            If stockmagazijnId = 0 Then
                                'Bestaat niet
                                If voocontext.Basismateriaal.Where(Function(x) x.id = ASPxComboBoxArtikel.Value).FirstOrDefault.retourItem Then
                                    'Is retouritem, mag toegevoegd worden
                                Else
                                    Session("RetourFout") = "Serienummer moet bestaan voor retour tenzij het item een oude meter is."
                                    trans.Rollback()
                                    Return
                                End If

                            Else
                                Dim logVerkeerdeStock As New Log
                                logVerkeerdeStock.Actie = "Stock geretourneerd van het verkeerde magazijn"
                                voocontext.Log.Add(logVerkeerdeStock)
                            End If
                        End If

                        Dim cmd2 As SqlCommand = New SqlCommand("Insert into [Voo].[dbo].[RetourBestellijnSerienummer] (serienummer, status, bestellijnId) VALUES (@serienummer, @status, @bestellijnId)", cn)
                        cmd2.Parameters.Add(New SqlParameter("@serienummer", SqlDbType.NVarChar) With {
                                .Value = serienummer
                            })
                        Dim statusId As Integer = ASPxComboBoxRetour.Value




                        cmd2.Parameters.Add(New SqlParameter("@status", SqlDbType.NVarChar) With {
                                .Value = statusId
                            })
                        cmd2.Parameters.Add(New SqlParameter("@bestellijnId", SqlDbType.NVarChar) With {
                                .Value = bestellijnId
                            })
                        cmd2.Transaction = trans
                        cmd2.ExecuteNonQuery()
                        ''sto2

                        ''sto
                    Next
                End If


                trans.Commit()
            Catch ex As Exception

                If Session("taal") = 1 Then
                    Session("RetourFout") = "Onbekende fout. Controleer en probeer opnieuw."
                Else
                    Session("RetourFout") = "Erreur inconnue. Vérifiez et essayez à nouveau."

                End If
                trans.Rollback()
                Return
            End Try


        End Using

        'UPDATE STOCKARTIKEL
        'Dim context As New VooEntities
        'Dim magid As Integer = context.Gebruikers.Find(Session("userid")).MagazijnId
        'Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
        'Dim stockart = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
        'stockart.Aantal = stockart.Aantal - ASPxSpinEditAantal.Number


        'UPDATE HOOFDMAGAZIJN
        'Dim opdrachtid As Integer = Session("opdrachtgever")
        'Dim magid2 As Integer = context.Magazijn.Where(Function(x) x.Hoofd = True And x.OpdrachtgeverId = opdrachtid).FirstOrDefault.id
        'Dim stockart2 = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid2 And x.MateriaalId = basisId).FirstOrDefault
        'stockart2.Aantal = stockart.Aantal + ASPxSpinEditAantal.Number

        Dim context As New VooEntities





        'LOG
        Dim log As New Log
        log.Actie = "Gebruiker " & Session("userid") & " Heeft " & ASPxSpinEditAantal.Number & " Van artikel " & ASPxComboBoxArtikel.Text & " geretourneerd naar Hoofdmagazijn " & Session("opdrachtgever")
        log.Tijdstip = Today
        log.Gebruiker = Session("userid")
        context.Log.Add(log)
        context.SaveChanges()

        context.Dispose()
        Session("RetourKlaar") = ASPxComboBoxArtikel.Text
        ASPxWebControl.RedirectOnCallback(VirtualPathUtility.ToAbsolute("~/Retour.aspx"))

    End Sub

    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("stockmagazijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub

    Protected Sub ASPxGridView1_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBinding
        DoSelect(SqlDataSourceStockMagazijn.ConnectionString)
    End Sub

    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [StockMagazijnId] from [Serienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("StockMagazijnId"))
        Next row
        Session("SelectResult") = result
    End Sub

    Protected Sub ASPxGridView1_DetailRowGetButtonVisibility(sender As Object, e As ASPxGridViewDetailRowButtonEventArgs) Handles ASPxGridView1.DetailRowGetButtonVisibility
        If Not (CType(Session("SelectResult"), ArrayList)).Contains(e.KeyValue) Then
            e.ButtonState = GridViewDetailRowButtonState.Hidden
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender
        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxArtikel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxArtikel.SelectedIndexChanged
        ASPxSpinEditAantal.Number = 0
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

            cn.Open()
            Dim s_SQL As String = "select HeeftSerienummer from basismateriaal where id=@id"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
            cmd.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()
                If Not IsDBNull(dr2.Item(0)) Then
                    If dr2.Item(0) = True Then


                        HiddenArtikel("heeftSerienummer") = dr2.Item(0)

                    Else
                        HiddenArtikel("heeftSerienummer") = dr2.Item(0)
                    End If
                Else
                    HiddenArtikel("heeftSerienummer") = False
                    'buttonKlaar.Enabled = True
                    'clientLabel2.Text = ""
                End If
            End If

            dr2.Close()

            cn.Close()
        End Using
        'Dim context As New VooEntities
        'Dim magid As Integer = context.Gebruikers.Find(Session("userid")).MagazijnId
        'Dim artid As Integer = ASPxComboBoxArtikel.Value
        'Dim inStock As Decimal = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = artid).FirstOrDefault.Aantal
        'HiddenArtikel("artikelMax") = inStock

    End Sub

    Protected Sub ASPxComboBoxArtikel_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxArtikel.PreRender
        Dim cb As ASPxComboBox = CType(sender, ASPxComboBox)
        cb.TextFormatString = "{0}"
    End Sub

    Protected Sub ASPxButtonToevoegen_Click(sender As Object, e As EventArgs)

    End Sub
End Class