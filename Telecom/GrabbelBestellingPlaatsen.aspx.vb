﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports DevExpress.Export
Imports DevExpress.Web
Imports Microsoft.Exchange.WebServices.Data

Public Class GrabbelBestellingPlaatsen
    Inherits System.Web.UI.Page
    Protected Sub vertaal(taalid)
        If taalid = 1 Then
            Literal2.Text = "Plaats een bestelling (Grabbelmateriaal)"
            saveChangesBtn.Text = "Bestellen"
            ASPxButton1.Text = "Bestellen"
            ASPxLabel1.Text = "Bestel voor"
            ASPxButton2.Text = "Annuleren"
            cancelChangesBtn.Text = "Annuleren"
        Else
            Literal2.Text = "Passer un commande"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            ASPxComboBox1.Visible = False
            ASPxLabel1.Visible = False
        Else

        End If
        If Not Page.IsPostBack Then
            Session("mailBodyBestelling") = "<br/> <table>"
        End If




    End Sub
    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim editingRowVisibleIndex As Integer = ASPxGridView1.FindVisibleIndexByKeyValue(e.Keys.Values(0))
            Dim context As New VooEntities
            Try
                Dim checkmax As String = "select hoeveelheid from grabbelmateriaal where Id= @materiaalId"
                Dim maxxmd As New SqlCommand(checkmax, cn)
                Dim check_par1 As New SqlParameter("@materiaalId", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Id").ToString()}
                maxxmd.Parameters.Add(check_par1)
                Dim max = maxxmd.ExecuteScalar()
                If Not ASPxCheckBoxNieuw.Checked Then




                    Dim check_sql As String = "select id from BestellijnGrabbel where grabbelmateriaalId= @materiaalId and grabbelbestellingId = @BestellingId"
                    Dim check_cmd As New SqlCommand(check_sql, cn)
                    Dim check_par As New SqlParameter("@BestellingId", SqlDbType.NVarChar, -1) With {.Value = Session("GrabbelBestellingId")}
                    check_cmd.Parameters.Add(check_par)
                    check_par = New SqlParameter("@materiaalId", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Id").ToString()}
                    check_cmd.Parameters.Add(check_par)
                    Dim dr As SqlDataReader = check_cmd.ExecuteReader()
                    If dr.HasRows Then
                        dr.Read()
                        If Not dr.IsDBNull(0) Then
                            'update

                            Dim id As Decimal = dr.GetInt32(0)
                            dr.Close()
                            Dim s_SQL As String = "UPDATE BestellijnGrabbel set hoeveelheid = CASE WHEN hoeveelheid + @aantal >= @max THEN @max ELSE hoeveelheid + @aantal END where id = @id"

                            Dim cmd As New SqlCommand(s_SQL, cn)
                            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = id}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@max", SqlDbType.Decimal) With {.Value = max}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@aantal", SqlDbType.Decimal) With {.Value = e.NewValues(3)}
                            cmd.Parameters.Add(par)
                            If e.NewValues(3) > 0 Then
                                cmd.ExecuteNonQuery()
                            End If
                        End If

                    Else
                        dr.Close()
                        Dim s_SQL As String = "Insert into BestellijnGrabbel (GrabbelmateriaalId, GrabbelbestellingId, hoeveelheid ) VALUES (@materiaalid, @bestellingId, CASE WHEN @hoeveelheid >= @max THEN @max ELSE @hoeveelheid END)"

                        Dim cmd As New SqlCommand(s_SQL, cn)
                        Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = Session("GrabbelBestellingId")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@materiaalId", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Id").ToString()}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@hoeveelheid", SqlDbType.Decimal) With {.Value = e.NewValues(3)}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@max", SqlDbType.Decimal) With {.Value = max}
                        cmd.Parameters.Add(par)
                        If e.NewValues(3) > 0 Then
                            cmd.ExecuteNonQuery()
                        End If
                    End If
                Else
                    Dim s_SQL As String = "Insert into BestellijnGrabbel (GrabbelmateriaalId, GrabbelbestellingId, hoeveelheid ) VALUES (@materiaalid, @bestellingId, CASE WHEN @hoeveelheid >= @max THEN @max ELSE @hoeveelheid END)"

                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = Session("GrabbelBestellingId")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@materiaalId", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Id").ToString()}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@hoeveelheid", SqlDbType.Decimal) With {.Value = e.NewValues(3)}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@max", SqlDbType.Decimal) With {.Value = max}
                    cmd.Parameters.Add(par)
                    If e.NewValues(3) > 0 Then
                        cmd.ExecuteNonQuery()
                    End If
                End If

                Session("mailBodyBestelling") = Session("mailBodyBestelling") & "<tr><td>" & ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Artikel").ToString() & "</td><td> " & ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Description").ToString() & "</td><td> " & e.NewValues(3) & "</td></tr> "



            Catch ex As Exception

            End Try





        End Using

    End Sub

    Public Sub VerzendenBevestiging()
        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12


        Dim entities As New VooEntities
        Using memoryStream As New MemoryStream()
            memoryStream.Seek(0, SeekOrigin.Begin)
            Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2013_SP1) With {.Url = New Uri("https://outlook.office365.com/EWS/Exchange.asmx"), .UseDefaultCredentials = False, .Credentials = New WebCredentials("apk.admin@apkgroup.onmicrosoft.com", "boGmU5ElS2gGKjyy")}

            Dim message As New EmailMessage(exch)
            ASPxGridViewExporter1.WriteXlsx(memoryStream, New DevExpress.XtraPrinting.XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG, .AllowConditionalFormatting = DevExpress.Utils.DefaultBoolean.True})
            Dim opid As Integer = Session("opdrachtgever")
            If entities.Opdrachtgever.Find(opid).naam = "Synductis Kempen" Then
                exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.SmtpAddress, "synductisKEMPEN@apkgroup.eu")
                message.Body = "Hieronder het overzicht van de bestelling die u geplaatst heeft"

            ElseIf entities.Opdrachtgever.Find(opid).naam = "Synductis Limburg" Then
                exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.SmtpAddress, "synductisLIMBURG@apkgroup.eu")
                message.Body = "Hieronder het overzicht van de bestelling die u geplaatst heeft"

            Else
                exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.SmtpAddress, "no.reply@apkgroup.eu")
                message.Body = "Hieronder het overzicht van de bestelling die u geplaatst heeft"

            End If





            TrustAllCertificatePolicy.OverrideCertificateValidation()
            Dim m As New QueryStringModule


            message.Subject = "Overzicht Bestelling grabbelmateriaal"

            Session("mailBodyBestelling") = Session("mailBodyBestelling") & "</table>"
            message.Body.Text = message.Body.Text & Session("mailBodyBestelling")
            Dim gebruikerid As Integer = 0

            If Not Session("isadmin") Then
                gebruikerid = Session("userid")

            Else
                gebruikerid = ASPxComboBox1.SelectedItem.Value
            End If

            message.ToRecipients.Add(entities.Gebruikers.Find(gebruikerid).Email)
            'message.ToRecipients.Add("conan.dufour@apkgroup.eu")
            message.SendAndSaveCopy()
        End Using
    End Sub

    Protected Sub ASPxGridView1_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles ASPxGridView1.BatchUpdate
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim tempId As New Integer
            Try
                If Not ASPxCheckBoxNieuw.Checked Then


                    Dim check_sql As String = "select id from Bestellingen where GebruikerId= @gebruikerId and Status = 1"
                    Dim check_cmd As New SqlCommand(check_sql, cn)
                    Dim check_par As New SqlParameter
                    If Not Session("isadmin") Then
                        check_par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = Session("userid")}

                    Else
                        check_par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
                    End If
                    check_cmd.Parameters.Add(check_par)
                    Dim dr As SqlDataReader = check_cmd.ExecuteReader()
                    If dr.HasRows Then
                        dr.Read()
                        If Not dr.IsDBNull(0) Then
                            tempId = dr.GetInt32(0)
                        End If
                        dr.Close()

                        Dim s_SQL As String = "UPDATE  Bestellingen set Datum = GetDate() where id = @id"

                        Dim cmd As New SqlCommand(s_SQL, cn)
                        Dim par As New SqlParameter

                        par = New SqlParameter("@id", SqlDbType.Int) With {.Value = tempId}

                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    Else
                        dr.Close()
                        Dim s_SQL As String = "Insert into Bestellingen (Datum, GebruikerId, Status) VALUES (GetDate(), @gebruikerId, 1); SELECT SCOPE_IDENTITY()"

                        Dim cmd As New SqlCommand(s_SQL, cn)
                        Dim par As New SqlParameter
                        If Not Session("isadmin") Then
                            par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = Session("userid")}

                        Else
                            par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
                        End If
                        cmd.Parameters.Add(par)
                        tempId = cmd.ExecuteScalar()

                    End If
                Else
                    Dim s_SQL As String = "Insert into Bestellingen (Datum, GebruikerId, Status) VALUES (GetDate(), @gebruikerId, 1); SELECT SCOPE_IDENTITY()"

                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter
                    If Not Session("isadmin") Then
                        par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = Session("userid")}

                    Else
                        par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
                    End If
                    cmd.Parameters.Add(par)
                    tempId = cmd.ExecuteScalar()
                End If





                Session("GrabbelBestellingId") = tempId
            Catch ex As Exception

            End Try




        End Using
    End Sub



    Protected Sub ASPxGridView1_AfterPerformCallback(sender As Object, e As ASPxGridViewAfterPerformCallbackEventArgs) Handles ASPxGridView1.AfterPerformCallback
        ' MAIL VERZENDEN
        'Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
        '    cn.Open()
        '    Dim s_SQL As String = ""
        '    Dim cmd As SqlCommand = Nothing
        '    Dim par As SqlParameter = Nothing
        '    Dim varBericht As String = ""
        '    Dim varOnderwerp As String = ""
        '    Dim varBeheerder As String = ""
        '    Dim varEmail As String = ""
        '    s_SQL = "SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)"
        '    cmd = New SqlCommand(s_SQL, cn)
        '    par = New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Melding bestelling aangevraagd"}
        '    cmd.Parameters.Add(par)
        '    Dim dr As SqlDataReader = cmd.ExecuteReader
        '    If dr.HasRows Then
        '        dr.Read()
        '        If Not dr.IsDBNull(0) Then
        '            varBericht = dr.GetString(0)
        '            varOnderwerp = dr.GetString(1)
        '        End If
        '    End If
        '    dr.Close()
        '    s_SQL = "SELECT vwWerkn.WERKNEMER, Gebruikers_1.Email FROM Gebruikers INNER JOIN Gebruikers Gebruikers_1 ON Gebruikers.VerlofGoedkeuring=Gebruikers_1.id " _
        '        & "INNER JOIN (SELECT NAAM + ' ' + VNAAM as WERKNEMER, [ON],NR FROM Elly_SQL.dbo.WERKN) vwWerkn ON Gebruikers_1.Werkg=vwWerkn.[ON] " _
        '        & "AND Gebruikers_1.Werkn=vwWerkn.NR WHERE (Gebruikers.id=@gebruiker)"
        '    cmd = New SqlCommand(s_SQL, cn)
        '    par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Session("userid")}
        '    cmd.Parameters.Add(par)
        '    dr = cmd.ExecuteReader
        '    If dr.HasRows Then
        '        dr.Read()
        '        varBeheerder = dr.GetString(0)
        '        varEmail = dr.GetString(1)
        '    End If
        '    dr.Close()
        '    Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2010_SP2)
        '    exch.Url = New Uri("https://mail.apkintern.be/EWS/Exchange.asmx")
        '    exch.UseDefaultCredentials = False
        '    exch.Credentials = New System.Net.NetworkCredential("Administrator", "E01ab107%", "apk")
        '    TrustAllCertificatePolicy.OverrideCertificateValidation()
        '    exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.PrincipalName, Session("mailaccount"))
        '    Dim message As New EmailMessage(exch)
        '    message.Subject = varOnderwerp
        '    Dim varBody As String = varBericht.Replace("[WERKNEMER]", String.Format("{0} {1}", Session("naam"), Session("voornaam")))
        '    varBody = varBody.Replace("[BEHEERDER]", varBeheerder)
        '    varBody = varBody.Replace("[URL]", String.Format("{0}/VerlofaanvragenVerwerken.aspx?g={1}", Session("domein"), Session("userid")))
        '    varBody = varBody.Replace("[SITETITEL]", Session("sitetitel"))
        '    Dim sBody As String = "<x-html>" & vbCrLf
        '    sBody = sBody & varBody
        '    sBody = sBody & "</x-html>"
        '    message.Body = sBody
        '    message.ToRecipients.Add(varEmail)
        '    message.Send()
        'End Using


        If Session("GrabbelBestellingId") <> 0 And Not Session("GrabbelBestellingId") Is Nothing Then
            VerzendenBevestiging()
            Dim tempid As String = Session("GrabbelBestellingId")
            Session("GrabbelBestellingId") = Nothing
            ASPxWebControl.RedirectOnCallback(VirtualPathUtility.ToAbsolute("~/BestellingCompleet.aspx?id=" + tempid))
        End If



    End Sub


    Protected Sub ASPxGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles ASPxGridView1.SelectionChanged

    End Sub
End Class