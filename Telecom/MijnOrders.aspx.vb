﻿Public Class MijnOrders
    Inherits System.Web.UI.Page

    Protected Sub vertaal(taalid)
        If taalid = 1 Then
            Literal2.Text = "Mijn bestellingen"
            ASPxGridView1.Columns("beschrijving").Caption = "Status"
            ASPxGridView1.Columns("Datum").Caption = "Datum"

        Else
            ASPxGridView1.Columns("Datum").Caption = "Date"
            ASPxGridView1.Columns("groep").Caption = "Groupe"
            Literal2.Text = "Mon commandes"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))

    End Sub

End Class