﻿Imports System.Data.SqlClient

Public Class WebForm2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        If Not Session("isadmin") Then
            ASPxComboBox1.Visible = False
            ASPxLabel1.Visible = False
        End If


        ASPxDateEdit1.MaxDate = Date.Now

        If Not Page.IsPostBack Then


            If Not String.IsNullOrEmpty(Request.QueryString("id")) Then
                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()
                    Dim s_SQL As String = "SELECT KlantNr, WerkNr, Opmerking, Tijd, Postcode, TariefId FROM werk WHERE (id=@id)"
                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                    cmd.Parameters.Add(par)
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    If dr.HasRows Then
                        dr.Read()
                        Me.ASPxTextBoxOperatieNr.Text = dr.GetString(1)
                        Me.TextBoxOpmerking.Text = dr.GetString(2)
                        ASPxDateEdit1.Value = dr.GetDateTime(3)
                        If Not dr.IsDBNull(4) Then
                            ASPxComboBoxPostcode.Value = dr.GetString(4)
                        End If

                        If Not dr.IsDBNull(5) Then
                            ASPxComboBox2.SelectedIndex = dr.GetInt32(5)
                        End If


                    End If
                    dr.Close()
                    cn.Close()
                End Using
                If Session("level") = 1 Then

                    Literal2.Text = "Éditer opération"

                Else
                    Literal2.Text = "Regarder opération"
                    ASPxTextBoxOperatieNr.ReadOnly = True
                    ASPxDateEdit1.ReadOnly = True

                End If
                ASPxComboBox1.Visible = False
                ASPxLabel1.Visible = False

            Else
                Literal2.Text = "Enregister opération"
                ASPxComboBox2.SelectedIndex = 0
                ASPxDateEdit1.Value = Date.Now
            End If
        End If
    End Sub


    Protected Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click


        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = ""
            Dim tempId As Integer

            'Textboxen zijn ingevuld
            If Not (String.IsNullOrEmpty(Me.ASPxTextBoxOperatieNr.Text)) Then

                'bewerkingsactie
                If Not String.IsNullOrEmpty(Request.QueryString("id")) Then
                    s_SQL = "UPDATE Werk SET  WerkNr=@werknr, Postcode=@postcode, TariefId=@TariefId, Opmerking=@opmerking, Tijd=@tijd WHERE (id=@id); SELECT SCOPE_IDENTITY()"
                    Dim par As New SqlParameter()
                    Dim cmd As New SqlCommand(s_SQL, cn)

                    par = New SqlParameter("@postcode", SqlDbType.NVarChar, -1) With {.Value = Me.ASPxComboBoxPostcode.Value}
                    If ASPxComboBoxPostcode.Value Is Nothing Then
                        par.Value = DBNull.Value
                    End If
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@werknr", SqlDbType.NVarChar, -1) With {.Value = Me.ASPxTextBoxOperatieNr.Text}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@opmerking", SqlDbType.NVarChar, -1) With {.Value = Me.TextBoxOpmerking.Text}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@tijd", SqlDbType.DateTime, -1) With {.Value = Me.ASPxDateEdit1.Value}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@TariefId", SqlDbType.Int) With {.Value = CInt(ASPxComboBox2.Value)}
                    If ASPxComboBox2.Value Is Nothing Then
                        par.Value = DBNull.Value
                    End If
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@id", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                    cmd.Parameters.Add(par)



                    cmd.ExecuteNonQuery()

                    Session("uitgevoerdwerk") = par.Value


                    cn.Close()
                Else
                    'toevoegactie

                    'controleren of al bestaat
                    s_SQL = "SELECT id FROM werk WHERE (WerkNr=@werknr)"
                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter("@werknr", SqlDbType.NVarChar, 25) With {.Value = ASPxTextBoxOperatieNr.Text}
                    cmd.Parameters.Add(par)
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    Dim blbestaat As Boolean = dr.HasRows
                    dr.Close()
                    If blbestaat Then
                        ASPxTextBoxOperatieNr.ErrorText = "Cette opération existe déjà."
                        ASPxTextBoxOperatieNr.IsValid = False
                        Return
                    End If

                    s_SQL = "INSERT INTO Werk (Tijd, GebruikerId,Postcode,TariefId, WerkNr, opmerking) VALUES (@tijd,@gebruikerid, @postcode,@TariefId, @werknr, @opmerking); SELECT SCOPE_IDENTITY()"
                    par = New SqlParameter()
                    cmd = New SqlCommand(s_SQL, cn)
                    par = New SqlParameter()

                    If Not Session("isadmin") Then
                        par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = Session("userid")}

                    Else
                        par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
                    End If
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@postcode", SqlDbType.NVarChar, -1) With {.Value = Me.ASPxComboBoxPostcode.Value}
                    If ASPxComboBoxPostcode.Value Is Nothing Then
                        par.Value = DBNull.Value
                    End If
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@opmerking", SqlDbType.NVarChar, -1) With {.Value = Me.TextBoxOpmerking.Text}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@tijd", SqlDbType.DateTime) With {.Value = Me.ASPxDateEdit1.Value}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@werknr", SqlDbType.NVarChar, -1) With {.Value = Me.ASPxTextBoxOperatieNr.Text}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@TariefId", SqlDbType.Int) With {.Value = CInt(ASPxComboBox2.Value)}
                    cmd.Parameters.Add(par)


                    tempId = cmd.ExecuteScalar()
                    Session("uitgevoerdwerk") = tempId


                    cn.Close()
                End If
            Else
                ASPxTextBoxOperatieNr.ErrorText = "Remplir un Numero d'opération s'il vous plait"
                ASPxTextBoxOperatieNr.IsValid = False

                Return
            End If


        End Using


        If Not String.IsNullOrEmpty(Request.QueryString("id")) Then
            Response.Redirect("~/ArtikelenPosten.aspx?id=" + Request.QueryString("id"), False)
            Context.ApplicationInstance.CompleteRequest()
        Else
            Response.Redirect("~/ArtikelenPosten.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If


        


        'If Not (String.IsNullOrEmpty(Me.ASPxTextBoxNieuweGroep.Text)) Then
        '    Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
        '        cn.Open()
        '        Dim s_SQL As String = "SELECT id FROM Gebruikersgroepen WHERE (Naam=@naam)"
        '        Dim cmd As New SqlCommand(s_SQL, cn)
        '        Dim par As New SqlParameter("@naam", SqlDbType.NVarChar, 25) With {.Value = ASPxTextBoxNieuweGroep.Text}
        '        cmd.Parameters.Add(par)
        '        Dim dr As SqlDataReader = cmd.ExecuteReader
        '        Dim blbestaat As Boolean = dr.HasRows
        '        dr.Close()
        '        If blbestaat Then
        '            ASPxTextBoxNieuweGroep.ErrorText = "Deze groep bestaat al."
        '            ASPxTextBoxNieuweGroep.IsValid = False
        '            Return
        '        End If
        '        Try
        '            s_SQL = "INSERT INTO Gebruikersgroepen (Naam) VALUES (@naam)"
        '            cmd = New SqlCommand(s_SQL, cn)
        '            par = New SqlParameter("@naam", SqlDbType.NVarChar, 25) With {.Value = ASPxTextBoxNieuweGroep.Text}
        '            cmd.Parameters.Add(par)
        '            cmd.ExecuteNonQuery()

        '            cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
        '            par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
        '            cmd.Parameters.Add(par)
        '            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Gebruikersgroep {0} toegevoegd door {1} {2}", ASPxTextBoxNieuweGroep.Text, Session("naam"), Session("voornaam"))}
        '            cmd.Parameters.Add(par)
        '            cmd.ExecuteNonQuery()
        '            ASPxTextBoxNieuweGroep.Text = ""
        '        Catch ex As Exception
        '            cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
        '            par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
        '            cmd.Parameters.Add(par)
        '            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij toevoeging gebruikersgroep door {0} {1}, omschrijving: {2}", Session("naam"), Session("voornaam"), ex.Message)}
        '            cmd.Parameters.Add(par)
        '            cmd.ExecuteNonQuery()
        '        End Try

        '    End Using
        'End If
        'cmbGroepen.DataBind()
    End Sub

    
    Protected Sub ASPxComboBox1_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBox1.PreRender

        If Not Session.Count = 0 Then
            ASPxComboBox1.SelectedItem = ASPxComboBox1.Items.FindByValue(Session("userid").ToString)
        End If


    End Sub


End Class