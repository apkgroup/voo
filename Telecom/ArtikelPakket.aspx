﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="ArtikelPakket.aspx.vb" Inherits="Telecom.ArtikelPakket" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
  .fixed {    
    position: fixed;   
    top:0px;
    background-color: #ffffff ! important; 
}
    </style>
     <script type="text/javascript" src="../js/jquery-1.4.2.min.js">
</script>
		<script type="text/javascript" src="../js/fib-jquery.js"></script>
           
          <script type="text/javascript" charset="utf-8">
              $(document).ready(function () {

                  var offset = $('#gridmenu').offset();

                  $(window).scroll(function () {
                      var scrollTop = $(window).scrollTop();
                      if (offset.top < scrollTop) {
                          $('#gridmenu').addClass('fixed');
                      } else {
                          $('#gridmenu').removeClass('fixed');
                      };
                  });

              });

		</script>
   <script type="text/javascript">
        
        function OnMoreInfoClick(element, key) {
            callbackPanel.SetContentHtml("");
            popupFoto.ShowAtElement(element);

        }
        function popup_Shown(s, e) {
            callbackPanel.PerformCallback();
        }
        function SetButtonsVisibility(s) {
            if (!s.batchEditApi.HasChanges()) {
                toolbarrechts.GetItem(0).SetVisible(false);
                toolbarrechts.GetItem(1).SetVisible(false);
            }
            else {
                toolbarrechts.GetItem(0).SetVisible(true);
                toolbarrechts.GetItem(1).SetVisible(true);

            }
        }

        function SetPCVisible(value) {
            var popupControl = GetPopupControl();
            if (value) {
                popupControl.Show();
            }
            else {
                popupControl.Hide();
            }
        }

        function GetPopupControl() {
            return popup;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:right; margin-bottom:10px;margin-left:10px;"><div id="gridmenu"><dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Klik hier om de wijzingen te bewaren (wijzing=groen gemarkeerd) - je kan meerdere keren bewaren tijdens een ingave, als je dan verbinding verliest, ben je de gegevens niet kwijt. Vergeet wel niet de gegevens definitief door te sturen als je klaar bent." Name="Bewaren" Text="">
                            <Image Url="~/images/save32x32.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                         <dx:MenuItem ToolTip="Klik hier om de wijzingen te annuleren (wijzing=groen gemarkeerd)" Name="Annuleren" Text="">
                            <Image Url="~/images/cancellen.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
        
                        <dx:MenuItem ToolTip="Klik hier als je het geselecteerde artikelpakket wil verwijderen" Name="Verwijderen" Text="">
                            <Image Url="~/images/Folders-OS-Recycle-Bin-Full-Metro-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;Bewaren&quot;) {
		grid.UpdateEdit();
             SetButtonsVisibility(grid);
} else {
	if (e.item.name==&quot;Annuleren&quot;) {
		grid.CancelEdit(); 
SetButtonsVisibility(grid);
} else {
            
                if (e.item.name==&quot;Verwijderen&quot;) {
                    
                   
                    SetPCVisible(true);

             }
             }
             
} 

 
}" />
                </dx:ASPxMenu></div></div><h2>Een basis artikel pakket samenstellen</h2>
   
    <br />
     <p><span style="font-family:Calibri;font-size:small;">Hier kan je een vaak voorkomend artikelpakket samenstellen zodat overhandigen makkelijker wordt (voorbeeld gereedschappen technieker).</span></p>
    <p><span style="font-family:Calibri;font-size:small;">Je kan best regelmatig op de bewaarknop drukken na een wijziging (gewijzigd=groen). Ingeval je verbinding verliest, ben je je gegevens dan niet kwijt.</span></p>
 
    <br />
    <table>
        <tr>
            <td>
                Bewerk een bestaand pakket:
            </td>
             <td>

                 <dx:ASPxComboBox ID="cboPakket" runat="server" ClientInstanceName="cboPakket" DataSourceID="SqlDataSourcePaketten" TextField="Naam" ValueField="id" ValueType="System.Int32" AutoPostBack="True">
                 </dx:ASPxComboBox>
                 
            </td>
             <td>
                Of maak een nieuw pakket:
            </td>
             <td>

                 <dx:ASPxTextBox ID="txtPakket" runat="server" ClientInstanceName="txtPakket" MaxLength="50" Width="170px">
                 </dx:ASPxTextBox>

            </td>
            <td>

                <dx:ASPxButton ID="btnNieuw" runat="server" ClientInstanceName="btnNieuw">
                    <Image Height="20px" Url="~/images/nieuw.png" Width="20px">
                    </Image>
                </dx:ASPxButton>

            </td>
        </tr>
    </table>


    <asp:SqlDataSource ID="SqlDataSourceInhoudPakket" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [ArtikelpakketSamenstelling] WHERE [id] = @id" InsertCommand="INSERT INTO [ArtikelpakketSamenstelling] ([Pakket], [Artikel], [Aantal]) VALUES (@Pakket, @Artikel, @Aantal)" SelectCommand="SELECT P.[id], P.[Pakket], P.[Artikel], P.[Aantal] 
, A.Artikel as ArtikelERP, A.Omschrijving, isnull(T.Omschrijving, 'Onbekend') as ArtType 
, cast(isnull(A.HeeftFoto,0) as int) as HeeftFoto, isnull(A.Verpaktper,1) as Verpaktper
FROM [ArtikelpakketSamenstelling] P 
INNER JOIN stock_Artikels A ON P.Artikel = A.id LEFT OUTER JOIN stock_Artikels_Types T 
ON A.TypeArtikel=T.id WHERE ([Pakket] = @Pakket) ORDER BY A.Artikel" UpdateCommand="UPDATE [ArtikelpakketSamenstelling] SET [Pakket] = @Pakket, [Artikel] = @Artikel, [Aantal] = @Aantal WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Pakket" Type="Int32" />
            <asp:Parameter Name="Artikel" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="cboPakket" Name="Pakket" PropertyName="Value" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Pakket" Type="Int32" />
            <asp:Parameter Name="Artikel" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Int32" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <dx:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid" DataSourceID="SqlDataSourceInhoudPakket" KeyFieldName="id">
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Pakket" VisibleIndex="1" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Artikel" VisibleIndex="2" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataColumn Caption="Artikel ERP" FieldName="ArtikelERP" VisibleIndex="3">
                <EditFormSettings Visible="False" />
                <DataItemTemplate>
                    <%# GetRender(Eval("[HeeftFoto]") & ";" & Eval("[ArtikelERP]"))%>
                    
                </DataItemTemplate>
            </dx:GridViewDataColumn>
            <dx:GridViewDataTextColumn Caption="Type" FieldName="ArtType" VisibleIndex="5">
                  <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Omschrijving" VisibleIndex="4">
                  <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="HeeftFoto" ReadOnly="True" Visible="False" VisibleIndex="6">
                  <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Verpaktper" FieldName="Verpaktper" VisibleIndex="7">
                  <HeaderStyle HorizontalAlign="Right" />
                <EditFormSettings Visible="False" />
          
             </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Aantal" VisibleIndex="8">
                <HeaderStyle HorizontalAlign="Right" />
                <CellStyle BackColor="#FFFFCC">
                </CellStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Styles>           
             <BatchEditModifiedCell ForeColor="Black">
            </BatchEditModifiedCell>
             <FocusedRow ForeColor="Black">
            </FocusedRow>
        </Styles>
       <SettingsPager Mode="ShowAllRecords" Visible="False">
        </SettingsPager>
        <SettingsEditing Mode="Batch">
        </SettingsEditing>
        <SettingsBehavior SortMode="DisplayText" AllowFocusedRow="True" />
  
        <SettingsText EmptyDataRow="Geen data gevonden..." />
        <Settings ShowHeaderFilterButton="True" ShowStatusBar="Hidden" />
        <SettingsText CommandBatchEditCancel="Annuleren" CommandBatchEditUpdate="Wijzigen opslaan" ConfirmOnLosingBatchChanges="Er zijn wijzigingen aangebracht maar deze werden nog niet bewaard (onderaan wijzigingen opslaan klikken). Ben u zeker dat u de wijzigingen niet wil bewaren?" />
        <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
        <ClientSideEvents Init="function(s, e){ SetButtonsVisibility(s); }" EndCallback="function(s, e){ 
SetButtonsVisibility(s); 
 
 }"  BatchEditEndEditing="function(s, e){ 
            
window.setTimeout(function(){ SetButtonsVisibility(s); }, 10); 
}"
 />
    </dx:ASPxGridView>

    <br />
     <dx:ASPxPopupControl ID="popupFoto" ClientInstanceName="popupFoto" runat="server" AllowDragging="true"
        PopupHorizontalAlign="OutsideLeft" HeaderText="Foto">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxCallbackPanel ID="callbackPanel" ClientInstanceName="callbackPanel" runat="server"
                    Width="400px" Height="300px" OnCallback="callbackPanel_Callback" RenderMode="Table">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <table>
                                <tr>
                                    <td><span style="font-family:Calibri;font-size:medium;font-weight:bold;"><asp:Literal ID="litText" runat="server" Text=""></asp:Literal></span></td>
                                </tr>
                                <tr>
                                    <td><dx:ASPxBinaryImage ID="edBinaryImage" runat="server" AlternateText="Bezig met laden..." ImageAlign="Left" CssClass="Image">
                                        </dx:ASPxBinaryImage></td>
                                </tr>
                            </table>
                            
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents Shown="popup_Shown" />
    </dx:ASPxPopupControl>
     <dx:ASPxPopupControl ID="popup" ClientInstanceName="popup" runat="server" 
        CloseAction="None" HeaderText="Bevestiging" Modal="True" 
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" 
        Width="314px" MinWidth="300px" CloseAnimationType="Fade">
        <HeaderImage Url="~/images/Info_32x32.png" Height="20px" Width="20px">
        </HeaderImage>
         <ContentCollection>
<dx:PopupControlContentControl runat="server">

    <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server">
        <Items>
            <dx:LayoutGroup ShowCaption="False">
                <Items>
                    <dx:LayoutItem ShowCaption="False">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxLabel ID="ASPxFormLayout1_E2" runat="server" Text="Bent u zeker dat u dit pakker wil verwijderen?">
                                </dx:ASPxLabel>
                                <br />
                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Dit kan niet meer ongedaan gemaakt worden.">
                                </dx:ASPxLabel>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                   
                    <dx:LayoutItem ShowCaption="False">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Klik op de knop 'Pakket verwijderen' om dit pakket te wissen.">
                                </dx:ASPxLabel>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:EmptyLayoutItem>

                    </dx:EmptyLayoutItem>
                    <dx:LayoutItem ShowCaption="False">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxButton ID="btnConfirmatie" runat="server" Text="Pakket verwijderen">
                                    <Image Url="images/Folders-OS-Recycle-Bin-Full-Metro-icon.png" Width="20px" Height="20px"></Image>
                                </dx:ASPxButton>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
        </Items>
    </dx:ASPxFormLayout>
    </dx:PopupControlContentControl>
</ContentCollection>
     </dx:ASPxPopupControl>
      <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server" ClientInstanceName="lpanel" Modal="True" Text="Bezig met laden gegevens ...">

                        </dx:ASPxLoadingPanel>
    <asp:SqlDataSource ID="SqlDataSourcePaketten" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Naam] FROM [Artikelpaketten] ORDER BY [Naam]"></asp:SqlDataSource>

</asp:Content>
