﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="ArtikelOverzicht.aspx.vb" Inherits="Telecom.ArtikelOverzicht" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #TextArea1 {
            height: 51px;
            width: 102px;
        }

        
        .auto-style1 {
            width: 48px;
        }
        .auto-style2 {
            width: 299px;
        }
        .auto-style3 {
            width: 37px;
        }

        
        .auto-style4 {
            height: 30px;
        }
        .auto-style5 {
            width: 37px;
            height: 30px;
        }

        
        </style>
    <script>
        var currentRowIndex = undefined;
        var currentRowIndex2 = undefined;
  
    </script>
    <script type="text/javascript">

        

            
        
       
        window.onload = function () {
            console.log("onload");
            $("input[name*='Serienummer']").each(function () {
                console.log(this.id);
                if (this.id.split("_").pop() != document.getElementById("ctl00_ContentPlaceHolder1_ASPxSpinEditAantal_I").value) {
                    this.removeAttribute("onchange");
                }
                else {
                    this.focus();
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_ASPxSpinEditAantal_I").value == 0) {
                    console.log("Het is 0!");
                    document.getElementById("ctl00_ContentPlaceHolder1_PanelSerie").style.display = "none";
                }
              
            });


            $('input').keypress(function (e) {
                console.log("KEYPRESS");
                if (e.which == 13) {
                    e.preventDefault();
                    console.log("ISENTER");
                    var self = $(this)
                    var form = self.parents('form:eq(0)');
                    var focusable;
                    var next;
                    var prev;

                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {

                    }
                    return false;

                }
            });

            ASPxGridView2.Refresh();
            var i;
        
            $('input').keypress(function (e) {
                console.log("KEYPRESS");
                if (e.which == 13) {
                    e.preventDefault();
                    console.log("ISENTER");
                    var self = $(this)
                    var form = self.parents('form:eq(0)');
                    var focusable;
                    var next;
  var prev;

                     focusable = form.find('input,a,select,button,textarea').filter(':visible');
                     next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {

                    }
                    return false;
                    
                }
            });
        };

        var timerHandle = -1;
        var indexrow
        function OnBatchEditStartEditing(s, e) {
            currentRowIndex = e.visibleIndex;
            currentRowIndex2 = e.visibleIndex;
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("Nombre");
        }

        function OnEndCallBack(s, e) {
            if (s.cpIsUpdated != '') {
               

                clientText.GetMainElement().style.display = 'block';
                console.log('Waarde komt uit');
                //console.log(e); 
                //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);
                console.log('Temps de Regie updated! Waarde is...');
                //console.log(e); 
                //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);

                console.log(clientButton);
                //console.log(Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML));
                if (ASPxGridView1.GetRow(indexrow).children[3].firstElementChild == null) {
                    if (Number(ASPxGridView1.GetRow(indexrow).children[3].innerHTML.replace(",", ".")) > 0) {
                        console.log("enabled=false");
                        clientButton.SetEnabled(false);
                        clientLabel2.GetMainElement().style.display = 'block'
                    }
                    else {
                        console.log("enabled=true");
                        clientButton.SetEnabled(true);
                        clientLabel2.GetMainElement().style.display = 'none'
                    }
                }
                else {
                    if (Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML.replace(",", ".")) > 0) {
                        console.log("enabled=false");
                        clientButton.SetEnabled(false);
                        clientLabel2.GetMainElement().style.display = 'block'
                    }
                    else {
                        console.log("enabled=true");
                        clientButton.SetEnabled(true);
                        clientLabel2.GetMainElement().style.display = 'none'
                    }
                }
                
                
                //console.log(s.cpIsUpdated);

                
            }
            else {
                clientText.SetText('');
                console.log('Anders updated');
            }
        }

        function OnKeyDown(s, e) {
            var key = e.htmlEvent.keyCode;
            console.log("key pressed!");
            if (key == 38 || key == 40) {
                console.log("Up or down key pressed!");
                ASPxClientUtils.PreventEvent(e.htmlEvent);
                ASPxGridView1.batchEditApi.EndEdit();
                console.log(currentRowIndex);
                if (currentRowIndex !== undefined && currentRowIndex !== null) {
                    var visibleIndices = ASPxGridView1.batchEditApi.GetRowVisibleIndices();
                    var indicesArrayLength = visibleIndices.length;
                    var rowIndex = visibleIndices.indexOf(currentRowIndex);
                    if (!(rowIndex === 0 && key === 38) || !(rowIndex === indicesArrayLength - 1 && key === 40)) {
                        switch (key) {
                            case 38:
                                ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex - 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            case 40:
                                ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex + 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        function OnKeyDownMat(s, e) {
            var key = e.htmlEvent.keyCode;
            console.log("key pressed!");
            if (key == 38 || key == 40) {
                console.log("Up or down key pressed");
                ASPxClientUtils.PreventEvent(e.htmlEvent);
                ASPxGridView2.batchEditApi.EndEdit();
                console.log(currentRowIndex2);
                if (currentRowIndex2 !== undefined && currentRowIndex2 !== null) {
                    var visibleIndices2 = ASPxGridView2.batchEditApi.GetRowVisibleIndices();
                    var indicesArrayLength2 = visibleIndices2.length;
                    var rowIndex2 = visibleIndices2.indexOf(currentRowIndex2);
                    if (!(rowIndex2 === 0 && key === 38) || !(rowIndex2 === indicesArrayLength2 - 1 && key === 40)) {
                        console.log("ERIN");
                        switch (key) {
                            case 38:
                                ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 - 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            case 40:
                                ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 + 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

       
        
        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);
            
            indexrow = e.visibleIndex;
            console.log("Index row set to " + indexrow);
            //if (s.cpIsUpdated != '') {

            //    console.log(clientText.GetMainElement());
            //    console.log(clientText);
            //    clientText.GetMainElement().style.display = 'none';
            //    console.log('Temps de Regie updated! Waarde is...');
            //    console.log(e);
            //    //console.log(ASPxGridView1.GetRow(e.visibleIndex).children[3].innerHTML);
            //    console.log(clientButton);
            //    clientButton.setEnabled(true);
            //    console.log(s.cpIsUpdated);


            //}
            //else {
            //    clientText.SetText('');
            //    console.log('Anders updated');
            //}

        }

        function saveChangesBtn_Click(s, e) {
            ASPxGridView1.UpdateEdit();
            ASPxGridView2.UpdateEdit();
        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();
            ASPxGridView2.CancelEdit();
        }




        function saveChangesBtnMat_Click(s, e) {
            ASPxGridView2.UpdateEdit();
        }

        function cancelChangesBtnMat_Click(s, e) {
            ASPxGridView2.CancelEdit();


            
        }


        function insertArticle() {
            console.log("IETest");
            Callback1.PerformCallback();
            console.log("IEAfterCallback");
        }



        function OnCallbackComplete(s, e) {
            console.log("cbCOMPLET");
            ASPxGridView2.Refresh();
            //location.reload();
            
        }



        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Overzicht artikelen per magazijn</h2>
    <p>&nbsp;  <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </p>

    <p></p>


     
                       



    <dx:ASPxGridView ID="ASPxGridView4" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceArtikelen" KeyFieldName="Id">
        <SettingsDetail ShowDetailRow="True" />
        <Templates>
            <DetailRow>
                
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" ClientInstanceName="ASPxGridView2" DataSourceID="SqlDataSourceMateriaux" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect1">
                    <Templates>
                        <DetailRow>
                            <dx:ASPxGridView ID="ASPxGridView3" runat="server" AutoGenerateColumns="False" ClientInstanceName="ASPxGridView3" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                                <SettingsSearchPanel Visible="True" />
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="StockMagazijnId" Visible="False" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Numéro de série" FieldName="serienummer" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn FieldName="uitgeboekt" Visible="False" VisibleIndex="3">
                                    </dx:GridViewDataCheckColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </DetailRow>
                    </Templates>
                    <SettingsPager Mode="ShowAllRecords">
                    </SettingsPager>
                    <SettingsEditing Mode="Batch">
                        <BatchEditSettings StartEditAction="Click" />
                    </SettingsEditing>
                    <Settings ShowStatusBar="Hidden" />
                    <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                    <SettingsSearchPanel Visible="True" />
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Magazijn" FieldName="Naam" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Aantal" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <Settings ShowHeaderFilterButton="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Id" ReadOnly="True" VisibleIndex="0" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="locatie" ReadOnly="True" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="bobijnArtikel" VisibleIndex="4">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="ArticleInternal" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="Consignatie" VisibleIndex="10">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataComboBoxColumn Caption="Groep 1" FieldName="MateriaalGroep" VisibleIndex="5">
                <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Groep 2" FieldName="MateriaalGroep2" VisibleIndex="6">
                <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Groep 3" FieldName="MateriaalGroep3" VisibleIndex="7">
                <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Type" FieldName="MateriaalType" VisibleIndex="9">
                <PropertiesComboBox DataSourceID="SqlDataSourceTypes" TextField="omschrijving" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn FieldName="Totaal" VisibleIndex="11">
                <SettingsHeaderFilter Mode="CheckedList">
                </SettingsHeaderFilter>
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>

                        <asp:SqlDataSource ID="SqlDataSourceArtikelen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT b.[Id],[Article], [Description],
(SELECT [locatie] from [MateriaalOpdrachtgevers] m where b.id = m.[materiaalId] and m.opdrachtgeverId =@OpdrachtgeverId) as 'locatie',
[bobijnArtikel],[MateriaalGroep],[MateriaalGroep2],[MateriaalGroep3],[ArticleInternal]
      ,[MateriaalType]
      ,[Consignatie]
,(SELECT sum([Aantal])
  FROM [Voo].[dbo].[StockMagazijn] sm
  inner join voo.dbo.Magazijn m on m.id = sm.MagazijnId
  where materiaalid = b.id and m.OpdrachtgeverId = @OpdrachtgeverId) as 'Totaal'
	  FROM [Basismateriaal] b
	  where b.id in (select mo.materiaalId from [MateriaalOpdrachtgevers] mo where opdrachtgeverId = @OpdrachtgeverId)  and (SELECT sum([Aantal])
  FROM [Voo].[dbo].[StockMagazijn] sm
  inner join voo.dbo.Magazijn m on m.id = sm.MagazijnId
  where materiaalid = b.id and m.OpdrachtgeverId = @OpdrachtgeverId) > 0
	
">
                            <SelectParameters>
                                <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" DefaultValue="1" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="SqlDataSourceMateriaux" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [VOO_Materiaal] WHERE [id] = @id" InsertCommand="INSERT INTO [VOO_Materiaal] ([Werk], [Article], [Description], [Nombre]) VALUES (@Werk, @Article, @Description, @Nombre)" SelectCommand="SELECT sm.[id]
	  ,m.Naam
      ,[Aantal]
  FROM [Voo].[dbo].[StockMagazijn] sm
  inner join voo.dbo.Magazijn m on m.id = sm.MagazijnId
  where materiaalid = @materiaalid and m.OpdrachtgeverId = @opdrachtgeverid and aantal>0" UpdateCommand="UPDATE [VOO_Materiaal] SET  [Nombre] = @Nombre WHERE [id] = @id">
                            <DeleteParameters>
                                <asp:Parameter Name="id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Werk" Type="Int32" />
                                <asp:Parameter Name="Article" Type="String" />
                                <asp:Parameter Name="Description" Type="String" />
                                <asp:Parameter Name="Nombre" Type="Decimal" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:SessionParameter Name="materiaalid" SessionField="GekozenArtikel" />
                                <asp:SessionParameter Name="opdrachtgeverid" SessionField="opdrachtgever" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Nombre" Type="Decimal" />
                                <asp:Parameter Name="id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>


    <br />
    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Serienummer] WHERE ([StockMagazijnId] = @StockMagazijnId) and (uitgeboekt &lt;&gt; 1)">
        <SelectParameters>
            <asp:SessionParameter Name="StockMagazijnId" SessionField="stockmagazijn" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceStockMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [StockMagazijn] WHERE [id] = @id" InsertCommand="INSERT INTO [StockMagazijn] ([MateriaalId], [MagazijnId], [Aantal]) VALUES (@MateriaalId, @MagazijnId, @Aantal)" SelectCommand="SELECT s.[id],[naam],b.Article,b.Description, [MateriaalId], [MagazijnId], [Aantal] FROM [StockMagazijn] s
INNER JOIN Magazijn m on s.MagazijnId = m.id 
INNER JOIN Basismateriaal b on s.MateriaalId = b.id WHERE ([MagazijnId] = @MagazijnId)" UpdateCommand="UPDATE [StockMagazijn] SET [MateriaalId] = @MateriaalId, [MagazijnId] = @MagazijnId, [Aantal] = @Aantal WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="MagazijnId" SessionField="MagazijnId" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    &nbsp;<asp:SqlDataSource ID="SqlDataSourceGroepen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [MateriaalGroep] ORDER BY [omschrijving]">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceTypes" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [MateriaalType] ORDER BY [omschrijving]">
    </asp:SqlDataSource>
&nbsp;<asp:SqlDataSource ID="SqlDataSourceRedenen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="select * from Voo.dbo.Status where id in (5,6)"></asp:SqlDataSource>

</asp:Content>
