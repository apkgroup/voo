﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data
Imports DevExpress.Web

Public Class Verlofaanvraag
    Inherits System.Web.UI.Page

    Private dicVerlof As Dictionary(Of Date, typeVerlof)

    Enum typeVerlof As Integer
        Voorbij = 0
        Goedgekeurd = 1
        Afgekeurd = 2
        Aangevraagd = 3
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()

        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
        If Not Page.IsPostBack Then
            ASPxCalendar1.VisibleDate = DateSerial(DateTime.Now.Year, 1, 1)
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim cmd As New SqlCommand With {.Connection = cn, .CommandText = "prVerlofElly", .CommandType = CommandType.StoredProcedure}
                Dim par As New SqlParameter("@Jaar", SqlDbType.Int) With {.Value = DateTime.Now.Year}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@Werkg", SqlDbType.NVarChar, 2) With {.Value = Session("werkg")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@Werkn", SqlDbType.Int) With {.Value = Session("werkn")}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()
                    Literal2.Text = String.Format("<i><b>Samenvatting uit Elly.net: {0} verlofuren opgenomen van totaal beschikbaar {1} voor jaar {2}</b></i>", dr.GetValue(1), dr.GetValue(0), DateTime.Now.Year)
                End If
                dr.Close()

                Dim blOK As Boolean = False
                Dim s_SQL As String = "SELECT VerlofGoedkeuring FROM Gebruikers WHERE (id=@id)"
                cmd = New SqlCommand(s_SQL, cn)
                par = New SqlParameter("@id", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()
                    If Not dr.IsDBNull(0) Then
                        blOK = True
                    End If
                End If
                dr.Close()
                If Not blOK Then
                    Me.ASPxButtonVerlofuren.Enabled = False
                    Me.ASPxButtonVerlofaanvragen.Enabled = False
                    Me.ASPxButtonVerlofaanvragen.ToolTip = "Er is nog geen verantwoordelijke gekoppeld aan uw account voor de goedkeuring van verlofaanvragen.<br>Gelieve contact op te nemen met een beheerder."

                End If
            End Using
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            dicVerlof = New Dictionary(Of Date, typeVerlof)
            Dim s_SQL As String = "SELECT V.Datum, V.Goedgekeurd, V.Afgekeurd FROM Verlofdagen V INNER JOIN " _
                & "(SELECT Datum, max(id) as id FROM Verlofdagen WHERE (Gebruiker=@gebruiker)" _
                & "AND (Soort='Verlof') group by Datum) vwT ON V.id=vwT.id order by V.Datum"
            Debug.WriteLine(s_SQL)
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            Dim lijstFouten As New List(Of String)

            If dr.HasRows Then
                Do While dr.Read
                    Try
                        If CDate(dr.GetValue(0)) < DateTime.Now Then
                            If Not dr.IsDBNull(1) Then
                                If dr.GetValue(1) = True Then
                                    dicVerlof.Add(dr.GetValue(0), typeVerlof.Voorbij)
                                End If
                            End If
                            
                        Else
                            If Not dr.IsDBNull(1) Then
                                If dr.GetValue(1) = True Then
                                    dicVerlof.Add(dr.GetValue(0), typeVerlof.Goedgekeurd)
                                Else
                                    If Not dr.IsDBNull(2) Then
                                        If dr.GetValue(2) = True Then
                                            dicVerlof.Add(dr.GetValue(0), typeVerlof.Afgekeurd)
                                        Else
                                            dicVerlof.Add(dr.GetValue(0), typeVerlof.Aangevraagd)
                                        End If
                                    Else
                                        dicVerlof.Add(dr.GetValue(0), typeVerlof.Aangevraagd)
                                    End If
                                End If
                            Else
                                If Not dr.IsDBNull(2) Then
                                    If dr.GetValue(2) = True Then
                                        dicVerlof.Add(dr.GetValue(0), typeVerlof.Afgekeurd)
                                    Else
                                        dicVerlof.Add(dr.GetValue(0), typeVerlof.Aangevraagd)
                                    End If
                                Else
                                    dicVerlof.Add(dr.GetValue(0), typeVerlof.Aangevraagd)
                                End If
                            End If

                        End If
                    Catch ex As Exception
                        lijstFouten.Add(String.Format("Fout bij openen pagina verlofaanvraag door {0} {1} : {2}", Session("naam"), Session("voornaam"), ex.Message))
                        
                    End Try

                Loop
            End If
            dr.Close()
            For Each fout As String In lijstFouten
                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = fout}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            Next
        End Using
    End Sub


    Protected Sub ASPxCallback1_Callback(ByVal sender As Object, ByVal e As CallbackEventArgs)
        If ASPxCalendar1.SelectedDates.Count > 0 Then
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = ""
                Dim cmd As SqlCommand = Nothing
                Dim par As SqlParameter = Nothing
                For Each datum As Date In ASPxCalendar1.SelectedDates

                    If Not datum.DayOfWeek = DayOfWeek.Saturday Then
                        If Not datum.DayOfWeek = DayOfWeek.Sunday Then
                            Try

                                s_SQL = "INSERT INTO Verlofdagen (Gebruiker, Datum, HeleDag, Soort, AangevraagdOp, BeslistDoor, Opmerking) VALUES (@gebruiker, @datum, " _
                                    & "1, 'Verlof', GetDate(), (SELECT VerlofGoedkeuring FROM Gebruikers WHERE (id=@gebruiker)), @opmerking)"
                                cmd = New SqlCommand(s_SQL, cn)
                                par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Session("userid")}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@datum", SqlDbType.Date) With {.Value = datum}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@opmerking", SqlDbType.NVarChar, 255) With {.Value = IIf(String.IsNullOrEmpty(ASPxTextBoxRedenFA.Text), DBNull.Value, ASPxTextBoxRedenFA.Text)}
                                cmd.Parameters.Add(par)
                                cmd.ExecuteNonQuery()

                                s_SQL = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                                cmd = New SqlCommand(s_SQL, cn)
                                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Verlof aangevraagd door {0} {1} voor datum {2}", Session("naam"), Session("voornaam"), datum.ToShortDateString)}
                                cmd.Parameters.Add(par)
                                cmd.ExecuteNonQuery()

                            Catch ex As Exception

                                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij verlofaanvraag door {0} {1} voor datum {2} {3}", Session("naam"), Session("voornaam"), datum.ToShortDateString, ex.Message)}
                                cmd.Parameters.Add(par)
                                cmd.ExecuteNonQuery()
                            End Try
                        End If
                    End If

                Next
                Dim varBericht As String = ""
                Dim varOnderwerp As String = ""
                Dim varBeheerder As String = ""
                Dim varEmail As String = ""
                s_SQL = "SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)"
                cmd = New SqlCommand(s_SQL, cn)
                par = New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Melding verlofaanvraag naar verantwoordelijke"}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()
                    If Not dr.IsDBNull(0) Then
                        varBericht = dr.GetString(0)
                        varOnderwerp = dr.GetString(1)
                    End If
                End If
                dr.Close()
                s_SQL = "SELECT vwWerkn.WERKNEMER, Gebruikers_1.Email FROM Gebruikers INNER JOIN Gebruikers Gebruikers_1 ON Gebruikers.VerlofGoedkeuring=Gebruikers_1.id " _
                    & "INNER JOIN (SELECT NAAM + ' ' + VNAAM as WERKNEMER, [ON],NR FROM Elly_SQL.dbo.WERKN) vwWerkn ON Gebruikers_1.Werkg=vwWerkn.[ON] " _
                    & "AND Gebruikers_1.Werkn=vwWerkn.NR WHERE (Gebruikers.id=@gebruiker)"
                cmd = New SqlCommand(s_SQL, cn)
                par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()
                    varBeheerder = dr.GetString(0)
                    varEmail = dr.GetString(1)
                End If
                dr.Close()
                Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2010_SP2)
                exch.Url = New Uri("https://mail.apkintern.be/EWS/Exchange.asmx")
                exch.UseDefaultCredentials = False
                exch.Credentials = New System.Net.NetworkCredential("Administrator", "E01ab107%", "apk")
                TrustAllCertificatePolicy.OverrideCertificateValidation()
                exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.PrincipalName, Session("mailaccount"))
                Dim message As New EmailMessage(exch)
                message.Subject = varOnderwerp
                Dim varBody As String = varBericht.Replace("[WERKNEMER]", String.Format("{0} {1}", Session("naam"), Session("voornaam")))
                varBody = varBody.Replace("[BEHEERDER]", varBeheerder)
                varBody = varBody.Replace("[URL]", String.Format("{0}/VerlofaanvragenVerwerken.aspx?g={1}", Session("domein"), Session("userid")))
                varBody = varBody.Replace("[SITETITEL]", Session("sitetitel"))
                Dim sBody As String = "<x-html>" & vbCrLf
                sBody = sBody & varBody
                sBody = sBody & "</x-html>"
                message.Body = sBody
                message.ToRecipients.Add(varEmail)
                message.Send()
            End Using

        End If

    End Sub


    Private Sub ASPxCalendar1_DayCellPrepared(sender As Object, e As DevExpress.Web.CalendarDayCellPreparedEventArgs) Handles ASPxCalendar1.DayCellPrepared
        If dicVerlof IsNot Nothing Then
            If dicVerlof.ContainsKey(e.Date) Then
                Select Case dicVerlof.Item(e.Date)
                    Case typeVerlof.Aangevraagd
                        e.Cell.BackColor = System.Drawing.Color.Yellow
                    Case typeVerlof.Afgekeurd
                        e.Cell.BackColor = System.Drawing.Color.Red
                    Case typeVerlof.Goedgekeurd
                        e.Cell.BackColor = System.Drawing.Color.LightGreen
                    Case typeVerlof.Voorbij
                        e.Cell.BackColor = System.Drawing.Color.LightBlue
                End Select
            End If
        End If

    End Sub

    Protected Sub ASPxButtonBevestigen_Click(sender As Object, e As EventArgs) Handles ASPxButtonBevestigen.Click
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = ""
            Dim cmd As SqlCommand = Nothing
            Dim par As SqlParameter = Nothing

            Dim datum As Date = DateSerial(CDate(ASPxDateEditDag.Value).Year, CDate(ASPxDateEditDag.Value).Month, CDate(ASPxDateEditDag.Value).Day)
            If Not datum.DayOfWeek = DayOfWeek.Saturday Then
                If Not datum.DayOfWeek = DayOfWeek.Sunday Then
                    Try

                        s_SQL = "INSERT INTO Verlofdagen (Gebruiker, Datum, HeleDag, Soort, AangevraagdOp, BeslistDoor, Opmerking, Startuur, Einduur, Totaaluur, VMNM) VALUES (@gebruiker, @datum, " _
                            & "0, 'Verlof', GetDate(), (SELECT VerlofGoedkeuring FROM Gebruikers WHERE (id=@gebruiker)), @opmerking, @startuur, @einduur, @totaaluur, @vmnm)"
                        cmd = New SqlCommand(s_SQL, cn)
                        par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@datum", SqlDbType.Date) With {.Value = datum}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@opmerking", SqlDbType.NVarChar, 255) With {.Value = IIf(String.IsNullOrEmpty(ASPxTextBoxRedenFA.Text), DBNull.Value, ASPxTextBoxRedenFA.Text)}
                        cmd.Parameters.Add(par)
                        If cboVMNM.Value = 1 Then
                            par = New SqlParameter("@startuur", SqlDbType.Time) With {.Value = datum.AddHours(8).TimeOfDay}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@einduur", SqlDbType.Time) With {.Value = datum.AddHours(12).TimeOfDay}
                            cmd.Parameters.Add(par)
                        Else
                            par = New SqlParameter("@startuur", SqlDbType.Time) With {.Value = datum.AddHours(13).TimeOfDay}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@einduur", SqlDbType.Time) With {.Value = datum.AddHours(17).TimeOfDay}
                            cmd.Parameters.Add(par)
                        End If
                       
                        par = New SqlParameter("@totaaluur", SqlDbType.Int) With {.Value = 4}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@vmnm", SqlDbType.Int) With {.Value = cboVMNM.Value}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()

                        s_SQL = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                        cmd = New SqlCommand(s_SQL, cn)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Verlof aangevraagd door {0} {1} voor datum {2}", Session("naam"), Session("voornaam"), datum.ToShortDateString)}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()

                    Catch ex As Exception

                        cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij verlofaanvraag door {0} {1} voor datum {2} {3}", Session("naam"), Session("voornaam"), datum.ToShortDateString, ex.Message)}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    End Try
                End If
            End If

            Dim varBericht As String = ""
            Dim varOnderwerp As String = ""
            Dim varBeheerder As String = ""
            Dim varEmail As String = ""
            s_SQL = "SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Melding verlofaanvraag naar verantwoordelijke"}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not dr.IsDBNull(0) Then
                    varBericht = dr.GetString(0)
                    varOnderwerp = dr.GetString(1)
                End If
            End If
            dr.Close()
            s_SQL = "SELECT vwWerkn.WERKNEMER, Gebruikers_1.Email FROM Gebruikers INNER JOIN Gebruikers Gebruikers_1 ON Gebruikers.VerlofGoedkeuring=Gebruikers_1.id " _
                & "INNER JOIN (SELECT NAAM + ' ' + VNAAM as WERKNEMER, [ON],NR FROM Elly_SQL.dbo.WERKN) vwWerkn ON Gebruikers_1.Werkg=vwWerkn.[ON] " _
                & "AND Gebruikers_1.Werkn=vwWerkn.NR WHERE (Gebruikers.id=@gebruiker)"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                varBeheerder = dr.GetString(0)
                varEmail = dr.GetString(1)
            End If
            dr.Close()
            Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2010_SP2)
            exch.Url = New Uri("https://mail.apkintern.be/EWS/Exchange.asmx")
            exch.UseDefaultCredentials = False
            exch.Credentials = New System.Net.NetworkCredential("Administrator", "E01ab107%", "apk")
            TrustAllCertificatePolicy.OverrideCertificateValidation()
            exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.PrincipalName, Session("mailaccount"))
            Dim message As New EmailMessage(exch)
            message.Subject = varOnderwerp
            Dim varBody As String = varBericht.Replace("[WERKNEMER]", String.Format("{0} {1}", Session("naam"), Session("voornaam")))
            varBody = varBody.Replace("[BEHEERDER]", varBeheerder)
            varBody = varBody.Replace("[URL]", String.Format("{0}/VerlofaanvragenVerwerken.aspx?g={1}", Session("domein"), Session("userid")))
            varBody = varBody.Replace("[SITETITEL]", Session("sitetitel"))
            Dim sBody As String = "<x-html>" & vbCrLf
            sBody = sBody & varBody
            sBody = sBody & "</x-html>"
            message.Body = sBody
            message.ToRecipients.Add(varEmail)
            message.Send()
        End Using
        Response.Redirect("Verlofaanvraag.aspx", False)
        Context.ApplicationInstance.CompleteRequest()
    End Sub
End Class