﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="GebruikersToevoegen.aspx.vb" Inherits="Telecom.GebruikersToevoegen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
        </style>
    <script type="text/javascript">
        var cback = false;
        function OnClickButtonDel(s, e) {
            cback = true;
            grid.PerformCallback('Aanmaken');
           
        }
        function grid_EndCallBack() {
            if (cback==true) {
                window.location = "GebruikersVersturen.aspx";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:right; margin-bottom:10px;margin-left:10px;"><dx:ASPxMenu ID="ASPxMenu1" runat="server" ShowAsToolbar="True">
                    <ClientSideEvents ItemClick="function(s, e) {
	if (e.item.name==&quot;Gebruikersaccountmaken&quot;) {
	cback = true;
       grid.PerformCallback(&quot;Aanmaken&quot;);
} 
}" />
                    <Items>
                        <dx:MenuItem ToolTip="Gebruikersaccount aanmaken voor de geselecteerde gebruikers" Name="Gebruikersaccountmaken">
                            <Image Url="~/images/AddContacts.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>                        
                    </Items>
                   
                </dx:ASPxMenu></div><h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
   <dx:ASPxCheckBox ID="ASPxCheckBoxInfovelden" runat="server" Checked="True" CheckState="Checked" Text="Champs d'information pour le profil de l'utilisateur">
                </dx:ASPxCheckBox>
    <br />
        <dx:ASPxGridView ID="grid" KeyFieldName="Login" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGebruikersToevoegen" ClientInstanceName="grid">
        <Columns>
            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="Werkg" ReadOnly="True" VisibleIndex="1" Caption="Employeur">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Werkn" ReadOnly="True" VisibleIndex="2" Caption="Employé">
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="NAAM" VisibleIndex="3" Caption="Nom">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="VNAAM" VisibleIndex="4" Caption="Prénom">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Login" ReadOnly="True" Visible="False" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsPager PageSize="20">
            <PageSizeItemSettings Caption="Items per pagina:" Items="10, 20, 50" ShowAllItem="True" Visible="True">
            </PageSizeItemSettings>
        </SettingsPager>
        <SettingsSearchPanel Visible="true" />
        <Settings ShowHeaderFilterButton="True" />
        
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <ClientSideEvents EndCallback="grid_EndCallBack" />
        </dx:ASPxGridView> <br />
   
        <asp:SqlDataSource ID="SqlDataSourceGebruikersToevoegen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [ON] as Werkg, NR as Werkn, NAAM, VNAAM, [ON]+'-'+cast(NR as nvarchar) as Login FROM Elly_SQL.dbo.WERKN wn LEFT OUTER JOIN Gebruikers ON wn.[ON]=Gebruikers.Werkg 
AND wn.NR=Gebruikers.Werkn WHERE (isnull(wn.uitdienst,0)&lt;&gt;1) AND (wn.NR&lt;&gt;999) AND (Gebruikers.Werkn IS NULL) AND (Gebruikers.Werkg IS NULL) ORDER BY wn.NAAM, wn.VNAAM"></asp:SqlDataSource>

</asp:Content>
