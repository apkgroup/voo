﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="BestellingPlaatsenBobijn.aspx.vb" Inherits="Telecom.BestellingPlaatsenBobijn" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        <script type="text/javascript">

            function saveChangesBtn_Click(s, e) {
            
            ASPxGridView1.UpdateEdit();
            
            
        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();
           
        }

        
    </script>
        </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Passer une commande pour " Font-Size="10pt">
    </dx:ASPxLabel>
    <dx:ASPxComboBox ID="ASPxComboBox1" runat="server" DataSourceID="SqlDataSourceGebruikers" TextField="Werknemer" ValueField="id" SelectedIndex="1">
    </dx:ASPxComboBox>
    <asp:SqlDataSource ID="SqlDataSourceGebruikers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [Gebruikers].id, (
SELECT 
CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam
ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)
END
) as Werknemer
FROM [Gebruikers] where opdrachtgeverId = @opdrachtgeverId and actief = 1


">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceGroepen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [MateriaalGroep]">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    Deadline:<br />
    <dx:ASPxDateEdit ID="ASPxDateEditDeadline" runat="server">
    </dx:ASPxDateEdit>
    <br />
    Opmerking:<dx:ASPxMemo ID="ASPxMemoOpmerking" runat="server" Height="71px" Width="357px">
    </dx:ASPxMemo>
    <br />

     
                Nieuwe bestelling aanmaken forceren <dx:ASPxCheckBox ID="ASPxCheckBoxNieuw" ClientInstanceName="CheckNieuw" runat="server" CheckState="Unchecked">
                </dx:ASPxCheckBox>
           <br />

    <dx:ASPxButton ID="saveChangesBtn" runat="server" Text="Commander" AutoPostBack="false" ClientInstanceName="saveChangesBtn" CssClass="inline" Width="75px">
                        <ClientSideEvents Click="saveChangesBtn_Click" />
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="cancelChangesBtn" runat="server" Text="Annuler" AutoPostBack="false" CssClass="inline" Width="75px">
                        <ClientSideEvents Click="cancelChangesBtn_Click" />
                    </dx:ASPxButton>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" ClientInstanceName="ASPxGridView1" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMateriaux" KeyFieldName="Id">
        <SettingsPager PageSize="25">
        </SettingsPager>
        <SettingsEditing Mode="Batch">
            <BatchEditSettings StartEditAction="Click" />
        </SettingsEditing>
        <Settings ShowStatusBar="Hidden" ShowHeaderFilterButton="True" />

                    
       
        <SettingsBehavior AllowFocusedRow="True" />

                    
       
        <SettingsSearchPanel Visible="True" />

                    
       
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Id" ReadOnly="True" VisibleIndex="0" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="2">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Nombre" VisibleIndex="7">
                <PropertiesTextEdit Size="100" Width="50px">
                    <MaskSettings Mask="&lt;0..9999&gt;&lt;,|.&gt;&lt;00..99&gt;" AllowMouseWheel="False" />
                </PropertiesTextEdit>
                <EditFormSettings Visible="True" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Eenheid" VisibleIndex="6">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataImageColumn FieldName="foto" VisibleIndex="8">
                <EditFormSettings Visible="False" />
                <DataItemTemplate>  
                <dx:ASPxImageZoom runat="server" ID="zoom" LargeImageLoadMode="OnPageLoad" ShowHintText="false" ShowHint="false" 
                    ImageContentBytes='<%#If(Eval("foto") Is DBNull.Value, IO.File.ReadAllBytes(Server.MapPath("images/") & "blanco.png"), IO.File.ReadAllBytes(Server.MapPath("images/") & "camera.png")) %>' LargeImageContentBytes='<%#If(Eval("foto") Is DBNull.Value, IO.File.ReadAllBytes(Server.MapPath("images/") & "blanco.png"), Eval("foto")) %>' EnableZoomMode="False">  
                    <SettingsAutoGeneratedImages ImageCacheFolder="~/images/" ImageHeight="30" LargeImageWidth="800" LargeImageHeight="800" />  
                    <%--<SettingsZoomMode ZoomWindowWidth="350" ZoomWindowHeight="350" ZoomWindowPosition="Bottom" />--%>  
                    <Border BorderStyle="None" />  
                </dx:ASPxImageZoom>  
            </DataItemTemplate> 
            </dx:GridViewDataImageColumn>
            <dx:GridViewDataComboBoxColumn FieldName="MateriaalGroep3" VisibleIndex="5">
                <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                </PropertiesComboBox>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="MateriaalGroep" ReadOnly="True" VisibleIndex="3">
                <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                </PropertiesComboBox>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="MateriaalGroep2" VisibleIndex="4">
                <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                </PropertiesComboBox>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataComboBoxColumn>
        </Columns>
        <SettingsAdaptivity AdaptivityMode="HideDataCells">
        </SettingsAdaptivity>
        <Templates>
                <StatusBar>
                    <dx:ASPxButton ID="saveChangesBtn" runat="server" Text="Commander" AutoPostBack="false"
                        RenderMode="Link" ClientInstanceName="saveChangesBtn" ClientEnabled="false">
                        <ClientSideEvents Click="saveChangesBtn_Click" />
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="cancelChangesBtn" runat="server" Text="Annuler" AutoPostBack="false"
                        RenderMode="Link">
                        <ClientSideEvents Click="cancelChangesBtn_Click" />
                    </dx:ASPxButton>
                </StatusBar>
            </Templates>
        <Styles>
            <SelectedRow BackColor="#FFCC99">
            </SelectedRow>
            <FocusedRow Font-Bold="True" ForeColor="Black">
            </FocusedRow>
            <InlineEditRow BackColor="#FFCC99">
            </InlineEditRow>
        </Styles>
    </dx:ASPxGridView>&nbsp;<dx:ASPxButton ID="ASPxButton1" runat="server" Text="Commander" AutoPostBack="false" ClientInstanceName="saveChangesBtn" CssClass="inline" Width="75px">
                        <ClientSideEvents Click="saveChangesBtn_Click" />
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Annuler" AutoPostBack="false" CssClass="inline" Width="75px">
                        <ClientSideEvents Click="cancelChangesBtn_Click" />
                    </dx:ASPxButton>
        
    <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" InsertCommand="INSERT INTO [Bestellijn] ([materiaalId], [bestellingId], [hoeveelheid]) VALUES (@materiaalId, @bestellingId, @hoeveelheid)" SelectCommand="SELECT [Basismateriaal].[id] as 'Id',
[Basismateriaal].[Article] as 'Article', 
[Basismateriaal].[Description] as 'Description', 
[Basismateriaal].Eenheid as 'Eenheid', 
[Basismateriaal].MateriaalGroep,
[Basismateriaal].MateriaalGroep2,
[Basismateriaal].MateriaalGroep3,
0.00 as 'Nombre',
[foto]
FROM [Basismateriaal] 
where (actief is null or actief = 1) and (bobijnArtikel = 1)
and basismateriaal.id in (select mo.materiaalId from [MateriaalOpdrachtgevers] mo where opdrachtgeverId = @opdrachtgeverId ) 
order by [Basismateriaal].[Description]" ID="SqlDataSourceMateriauxADMINSYN" UpdateCommand="UPDATE [Bestellijn] SET hoeveelheid = 4 where id=1">
        <InsertParameters>
            <asp:Parameter Name="materiaalId"></asp:Parameter>
            <asp:Parameter Name="bestellingId"></asp:Parameter>
            <asp:Parameter Name="hoeveelheid"></asp:Parameter>
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" DefaultValue="" />
        </SelectParameters>
    </asp:SqlDataSource>
        
    <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" InsertCommand="INSERT INTO [Bestellijn] ([materiaalId], [bestellingId], [hoeveelheid]) VALUES (@materiaalId, @bestellingId, @hoeveelheid)" SelectCommand="SELECT [Basismateriaal].[id] as 'Id',
[Basismateriaal].[Article] as 'Article', 
[Basismateriaal].Eenheid as 'Eenheid', 
[Basismateriaal].[Description] as 'Description', 
[Basismateriaal].MateriaalGroep,
[Basismateriaal].MateriaalGroep2,
[Basismateriaal].MateriaalGroep3,
(select sm.Favoriet from Stockmagazijn sm where sm.materiaalid =  [Basismateriaal].[id]  and sm.magazijnId= (select magazijnId from Gebruikers g where g.id = @userId)) as Favoriet,
0.00 as 'Nombre',
[foto]
FROM [Basismateriaal] 
where (actief is null or actief = 1) and (bobijnArtikel = 1 )
and basismateriaal.id in (select mo.materiaalId from [MateriaalOpdrachtgevers] mo where opdrachtgeverId = @opdrachtgeverId ) 
and (onbestelbaar is null or onbestelbaar = 0)
order by Favoriet desc,[Basismateriaal].[Description] asc" ID="SqlDataSourceMateriauxSYN" UpdateCommand="UPDATE [Bestellijn] SET hoeveelheid = 4 where id=1">
        <InsertParameters>
            <asp:Parameter Name="materiaalId"></asp:Parameter>
            <asp:Parameter Name="bestellingId"></asp:Parameter>
            <asp:Parameter Name="hoeveelheid"></asp:Parameter>
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="userId" SessionField="userid" />
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
    </asp:SqlDataSource>
        
</asp:Content>
