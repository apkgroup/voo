﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="VerlofaanvragenVerwerken.aspx.vb" Inherits="Telecom.VerlofaanvragenVerwerken" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
        </style>
    <script type="text/javascript">
        // <![CDATA[
             function SetPCVisible(value) {
            var popupControl = GetPopupControl();
            if (value) {
                popupControl.Show();
            }
            else {
                popupControl.Hide();
            }
        }
        function GetPopupControl() {
            return popup;
        }
        // ]]>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table><tr><td><h2>&nbsp;</h2></td><td><h2><dx:ASPxComboBox ID="ASPxComboBoxGebruiker" runat="server" DataSourceID="SqlDataSourceAanvragen" ValueType="System.Int32" TextField="WERKNEMER" ValueField="Gebruiker" AutoPostBack="True">
            <Columns>
                <dx:ListBoxColumn FieldName="WERKNEMER" />
                <dx:ListBoxColumn FieldName="Gebruiker" Visible="False" />
            </Columns>
        </dx:ASPxComboBox></h2></td></tr></table>   
        
        <asp:SqlDataSource ID="SqlDataSourceAanvragen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Verlofdagen.Gebruiker, vwWerkn.WERKNEMER FROM Verlofdagen INNER JOIN Gebruikers 
ON Verlofdagen.Gebruiker=Gebruikers.id INNER JOIN (SELECT NAAM + ' ' + VNAAM as WERKNEMER, [ON],NR FROM Elly_SQL.dbo.WERKN) vwWerkn 
ON Gebruikers.Werkg=vwWerkn.[ON] AND Gebruikers.Werkn=vwWerkn.NR WHERE  (BeslistDoor=@gebruikerId) AND (IsNull(Goedgekeurd,0)&lt;&gt;1) AND (IsNull(Afgekeurd,0)&lt;&gt;1) AND (Isnull(Gebruikers.Actief,0)=1) 
GROUP BY Verlofdagen.Gebruiker, vwWerkn.WERKNEMER order by vwWerkn.WERKNEMER">
            <SelectParameters>
                <asp:SessionParameter Name="gebruikerId" SessionField="userid" />
            </SelectParameters>
        </asp:SqlDataSource>
    
         <span style="font-family:Calibri;font-size:small;">Adopter un jour donné (ou jours), sélectionnez le jour (s) approprié et cliquez sur le bouton &quot;Approuver&quot;. Pour rejeter un jour donné (ou jours), sélectionnez le jour (s) approprié et cliquez sur le bouton &quot;Refuser&quot;. Par la souris pour se reposer une seconde sur une date de couleur, il y a plus d&#39;informations affichées sur cette date.</span><br />
     <asp:Literal ID="Literal1" runat="server"></asp:Literal><br />
    <table>
        <tr>
            <td>
                <dx:ASPxButton ID="ASPxButtonGoedkeuren" runat="server" Text="Approuver">
                     <ClientSideEvents Click="function(s, e) {
	 if (kalender.GetSelectedDates().length&lt;1) {
                    SetPCVisible(true);
                }
}" />
         <Image Url="~/images/Apply_16x16.png">
         </Image>
     </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="ASPxButtonWeigeren" runat="server" Text="Refuser">
                     <ClientSideEvents Click="function(s, e) {
	 if (kalender.GetSelectedDates().length&lt;1) {
                    SetPCVisible(true);
                }
}" />
         <Image Url="~/images/annuleren.png">
         </Image>
     </dx:ASPxButton>
            </td>
            <td style="width:120px;"></td><td>
            <dx:ASPxButton ID="ASPxButtonRapport" runat="server" Text="Rapport sommaire demandes de congés">
                <Image Url="~/images/Print_16x16.png">
                </Image>
            </dx:ASPxButton>
            </td>
        </tr>

    </table>
   
     <dx:ASPxCalendar ID="ASPxCalendar1" runat="server" ClearButtonText="Selectie wissen" Columns="4" EnableMultiSelect="True" HighlightToday="False" Rows="3" TodayButtonText="Vandaag" VisibleDate="2015-01-01" EnableMonthNavigation="False" ClientInstanceName="kalender" ShowClearButton="False" ShowTodayButton="False">
         <DaySelectedStyle ForeColor="Black">
             <Border BorderColor="#FF3300" BorderStyle="Solid" BorderWidth="1px" />
             
         </DaySelectedStyle>
         <DayWeekendStyle ForeColor="Gray">
         </DayWeekendStyle>
         
     </dx:ASPxCalendar>
     <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="popup" runat="server" 
        CloseAction="CloseButton" HeaderText="Foutmelding" Modal="True" 
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" 
        Width="336px" MinWidth="300px">
        <HeaderImage Url="~/images/warning.png">
        </HeaderImage>
         <ContentCollection>
<dx:PopupControlContentControl runat="server">
    Il n'ya pas selecté une date correcte (Jaune)!
    <br />
    L'action est annulé
    </dx:PopupControlContentControl>
</ContentCollection>
     </dx:ASPxPopupControl>
</asp:Content>
