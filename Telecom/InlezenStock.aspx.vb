﻿Imports System.Data.SqlClient
Imports DevExpress.Web
Imports Microsoft.ajax

Public Class InlezenStock
    Inherits System.Web.UI.Page
    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property

    'Protected Overrides Sub Initializeculture()
    '    SetCulture()
    '    MyBase.InitializeCulture()

    'End Sub
    'Private Sub SetCulture()
    '    Culture = Session("globalize").ToString
    '    UICulture = Session("globalize").ToString
    '    Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(Session("globalize").ToString)
    '    Threading.Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo(Session("globalize").ToString)
    'End Sub
    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then
            ASPxLabel1.Text = "Artikel"
            ASPxLabel2.Text = "Aantal"
            ASPxLabelLotnummer.Text = "Lotnummer"
            Literal2.Text = "Artikelenbeheer"
            ASPxGridView2.Columns("Article").Caption = "Artikel"
            ASPxGridView2.Columns("Description").Caption = "Omschrijving"
            ASPxGridView2.Columns("Aantal").Caption = "Aantal"

        Else
            ASPxLabelLotnummer.Text = "Numéro de lot"
            Literal2.Text = "Materiaux"


        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))


        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer
        ASPxComboBox2.Visible = False
        ASPxLabel3.Visible = False







        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()

        End If

        Dim opId As Integer = Session("opdrachtgever")
        If voocontext.Opdrachtgever.Find(opId).naam.Contains("VFL") Then
            ASPxLabelScan.ClientVisible = True
            ASPxTextBoxScan.ClientVisible = True

        Else
            ASPxLabelScan.ClientVisible = False
            ASPxTextBoxScan.ClientVisible = False

        End If



        If Page.IsPostBack Then
            ASPxLabelFout.Text = Session("fout")




            Dim i As Int32 = 1
            For Each textboxId As String In TextBoxIdCollection
                If i <= ASPxSpinEditAantal.Value Then



                    Dim label As New Label
                    If Session("taal") = 1 Then
                        label.Text = "Serienummer " & i & ":  "
                    Else
                        label.Text = "Numéro de série " & i & ":  "
                    End If

                    PanelSerie.Controls.Add(label)
                    Dim textbox = New TextBox With {
                .ID = textboxId
            }

                    Dim test As Integer = ASPxSpinEditAantal.Value
                    Dim br As New HtmlGenericControl("br")
                    If i = TextBoxIdCollection.Count Then
                        AddHandler textbox.TextChanged, AddressOf Me.txt_changed
                        textbox.AutoPostBack = True

                    Else
                        RemoveHandler textbox.TextChanged, AddressOf Me.txt_changed
                        textbox.AutoPostBack = False
                    End If
                    PanelSerie.Controls.Add(textbox)
                    PanelSerie.Controls.Add(br)

                    i += 1
                End If
            Next
        Else
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using

            Session("fout") = ""
            Session("Gelukt") = ""
            ASPxLabelFout.Text = Session("fout")
            ASPxLabelGelukt.Text = Session("Gelukt")
        End If


        voocontext.Dispose()


    End Sub




    Protected Sub ASPxComboBoxArtikel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxArtikel.SelectedIndexChanged
        ASPxSpinEditAantal.Number = 1
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            Session("changed") = True
            cn.Open()
            Dim s_SQL As String = "select HeeftSerienummer, [SerieVerplichtBijOpboeken] from basismateriaal where id=@id"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
            cmd.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()
                If Not IsDBNull(dr2.Item(0)) Then
                    If dr2.Item(0) = True Then
                        ASPxSpinEditAantal.Number = 1

                        HiddenArtikel("heeftSerienummer") = dr2.Item(0)
                        HiddenArtikel("SerieVerplichtBijOpboeken") = dr2.Item(1)

                        ASPxTextBoxLotnummer.Visible = True
                        ASPxLabelLotnummer.Visible = True
                        ASPxComboBox2.Visible = True
                        ASPxLabel3.Visible = True
                        ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)

                    Else
                        ASPxSpinEditAantal.Number = 1
                        HiddenArtikel("heeftSerienummer") = dr2.Item(0)
                        HiddenArtikel("SerieVerplichtBijOpboeken") = dr2.Item(1)

                        ASPxTextBoxLotnummer.Visible = False
                        ASPxLabelLotnummer.Visible = False
                    End If
                Else
                    ASPxSpinEditAantal.Number = 1
                    HiddenArtikel("heeftSerienummer") = False
                    HiddenArtikel("SerieVerplichtBijOpboeken") = False
                    ASPxTextBoxLotnummer.Visible = False
                    ASPxLabelLotnummer.Visible = False
                    ASPxComboBox2.Visible = False
                    ASPxLabel3.Visible = False

                End If
            End If

            dr2.Close()

            cn.Close()
        End Using
        'Dim context As New VooEntities
        'Dim magid As Integer = Session("MagazijnId")
        'Dim artid As Integer = ASPxComboBoxArtikel.Value
        'Dim inStock As Decimal = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = artid).FirstOrDefault.Aantal
        'HiddenArtikel("artikelMax") = inStock
        ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)






    End Sub

    Protected Sub txt_changed(sender As Object, e As EventArgs)
        ASPxSpinEditAantal.Value += 1

        ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)
    End Sub
    Protected Sub ASPxSpinEditAantal_NumberChanged(sender As Object, e As EventArgs) Handles ASPxSpinEditAantal.ValueChanged
        If HiddenArtikel.Count <> 0 Then


            If Not IsNothing(HiddenArtikel("SerieVerplichtBijOpboeken")) AndAlso Not IsDBNull(HiddenArtikel("SerieVerplichtBijOpboeken")) AndAlso HiddenArtikel("SerieVerplichtBijOpboeken") Then
                Dim context As New VooEntities

                Dim newlist As New List(Of String)
                For i As Integer = 1 To ASPxSpinEditAantal.Value
                    If Not TextBoxIdCollection.Contains(Today.Date.ToShortDateString & "_Serienummer_" & ASPxComboBoxArtikel.Value & "_" & i) Then

                        Dim txt = New TextBox With {.ID = Today.Date.ToShortDateString & "_Serienummer_" & ASPxComboBoxArtikel.Value & "_" & i}

                        'txt.ClientInstanceName = bestellijn.materiaalId & "_" & i
                        'ASPxPanelSerie.Controls.Add(txt)

                        Dim br As New HtmlGenericControl("br")
                        Dim label As New Label
                        If Session("taal") = 1 Then
                            label.Text = "Serienummer " & i & ":  "
                        Else
                            label.Text = "Numéro de série " & i & ":  "
                        End If
                        If Session("changed") Then
                            PanelSerie.Controls.Clear()
                        End If
                        Session("changed") = False
                        PanelSerie.Controls.Add(label)
                        PanelSerie.Controls.Add(txt)
                        PanelSerie.Controls.Add(br)

                        If i = ASPxSpinEditAantal.Value Then
                            'Bij Max event voor nieuwe textbox te maken
                            AddHandler txt.TextChanged, AddressOf Me.txt_changed

                            txt.AutoPostBack = True
                            txt.Focus()

                        Else
                            txt.AutoPostBack = False
                            RemoveHandler txt.TextChanged, AddressOf Me.txt_changed
                        End If


                    End If

                    newlist.Add(Today.Date.ToShortDateString & "_Serienummer_" & ASPxComboBoxArtikel.Value & "_" & i)
                Next

                TextBoxIdCollection = newlist
            End If
        Else

        End If

        If HiddenArtikel("heeftSerienummer") = False Then
            PanelSerie.Controls.Clear()
        End If

    End Sub




    Protected Sub ASPxComboBoxArtikel_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxArtikel.PreRender
        Dim cb As ASPxComboBox = CType(sender, ASPxComboBox)
        cb.TextFormatString = "{0}"
    End Sub

    'Protected Sub ASPxCallback1_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback1.Callback
    '    Dim Aantalinserted As Int32 = ASPxSpinEditAantal.Value
    '    Dim voocontext As New VooEntities
    '    Dim magid As Integer = Session("MagazijnId")
    '    Dim desc As String = ""
    '    Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
    '        cn.Open()

    '        Dim s_SQL As String = "select description from basismateriaal where id=@id"
    '        Dim cmd As New SqlCommand(s_SQL, cn)
    '        Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
    '        cmd.Parameters.Add(par)
    '        Dim dr As SqlDataReader = cmd.ExecuteReader
    '        If dr.HasRows Then
    '            dr.Read()
    '            If Not IsDBNull(dr.Item(0)) Then
    '                desc = dr.Item(0)

    '            End If
    '        End If

    '        dr.Close()
    '        cn.Close()
    '    End Using

    '    If HiddenArtikel("SerieVerplichtBijOpboeken") Then
    '        Aantalinserted = 0
    '        ASPxTextBoxLotnummer.Visible = True
    '        ASPxLabelLotnummer.Visible = True
    '        Dim nrs As New List(Of String)
    '        For Each textboxId As String In TextBoxIdCollection
    '            Dim tb As TextBox = PanelSerie.FindControl(textboxId)
    '            Dim value As String = tb.Text
    '            If nrs.Contains(value) Then
    '                If Session("taal") = 1 Then
    '                    Session("fout") = "Serienummer " & value & " Staat 2 keer in de lijst om in te lezen! Serienummer kan niet worden toegevoegd. Wijzigingen zijn niet doorgevoerd."

    '                Else
    '                    Session("fout") = "Numéro de série " & value & " Peut être lu deux fois dans la liste! Le numéro de série ne peut pas être ajouté. Les changements n'ont pas été mis en œuvre."

    '                End If
    '                Return
    '            End If
    '            nrs.Add(value)
    '        Next

    '        For Each textboxId As String In TextBoxIdCollection
    '            Dim tb As TextBox = PanelSerie.FindControl(textboxId)
    '            Dim value As String = tb.Text
    '            'enkel actie uitvoeren als textbox ingevuld is
    '            If Not String.IsNullOrWhiteSpace(value) Then
    '                Dim id As String = (CType(tb, TextBox)).ID
    '                Dim parts As String() = id.Split(New Char() {"_"c})
    '                Dim matId As Integer = parts(1).ToString
    '                If voocontext.Serienummer.Where(Function(x) x.serienummer1 = value).Any Then
    '                    If Session("taal") = 1 Then
    '                        Session("fout") = "Serienummer " & value & " bestaat al! Serienummer kan niet worden toegevoegd. Wijzigingen zijn niet doorgevoerd."

    '                    Else
    '                        Session("fout") = "Le numéro de série " & value & " existe déjà! Le numéro de série ne peut pas être ajouté. Aucune modification n'a été apportée."
    '                    End If
    '                    Return
    '                End If
    '                Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '                Dim serienummer As New Serienummer

    '                serienummer.StockMagazijnId = Stockmat.id
    '                serienummer.serienummer1 = value
    '                serienummer.uitgeboekt = 0
    '                serienummer.lotNr = ASPxTextBoxLotnummer.Text
    '                serienummer.datumGemaakt = Today()
    '                serienummer.statusId = 3
    '                voocontext.Serienummer.Add(serienummer)
    '                Aantalinserted += 1

    '                Dim logSerie As New Log
    '                logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " ingelezen op hoofdmagazijn "
    '                logSerie.Tijdstip = Today
    '                logSerie.Gebruiker = Session("userid")
    '                voocontext.Log.Add(logSerie)
    '            End If

    '        Next
    '        voocontext.SaveChanges()


    '        'For Each ctr As Control In PlaceHolderSerie.Controls
    '        '    If TypeOf (ctr) Is TextBox Then
    '        '        Dim id As String = (CType(ctr, TextBox)).ID
    '        '        Dim value As String = (CType(ctr, TextBox)).Text
    '        '        Dim parts As String() = id.Split(New Char() {"_"c})
    '        '        Dim matId As Integer = Convert.ToInt32(parts(0))

    '        '        Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '        '        Dim serienummer As New Serienummer
    '        '        serienummer.StockMagazijnId = Stockmat.id
    '        '        serienummer.serienummer1 = value
    '        '        serienummer.uitgeboekt = 0
    '        '        voocontext.Serienummer.Add(serienummer)

    '        '        Dim logSerie As New Log
    '        '        logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " ingelezen op hoofdmagazijn "
    '        '        logSerie.Tijdstip = Today
    '        '        logSerie.Gebruiker = Session("userid")
    '        '        voocontext.Log.Add(logSerie)
    '        '        voocontext.SaveChanges()
    '        '    End If
    '        'Next
    '        voocontext.SaveChanges()
    '    End If


    '    'UPDATE STOCKARTIKEL
    '    Dim context As New VooEntities

    '    Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
    '    Dim stockart = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
    '    stockart.Aantal = stockart.Aantal + Aantalinserted


    '    'LOG
    '    Dim log As New Log
    '    log.Actie = "Gebruiker " & Session("userid") & " Heeft " & Aantalinserted & " Van artikel " & desc & "ingelezen op hoofdmagazijn " & Session("uitgevoerdwerk")
    '    log.Tijdstip = Today
    '    log.Gebruiker = Session("userid")
    '    context.Log.Add(log)
    '    context.SaveChanges()

    '    context.Dispose()
    '    Session("Gelukt") = Aantalinserted & " van artikel " & desc & " toegevoegd."
    '    Session("GeluktCB") = True
    '    Session("Fout") = ""
    '    ASPxLabelGelukt.Visible = True
    '    ASPxSpinEditAantal.Value = 0
    '    ASPxComboBoxArtikel.Value = 0
    '    ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)


    'End Sub

    Protected Sub ASPxButtonToevoegen_Click(sender As Object, e As EventArgs) Handles ASPxButtonToevoegen.Click
        Dim Aantalinserted As Int32 = ASPxSpinEditAantal.Value
        Dim fouten As New List(Of String)
        Dim voocontext As New VooEntities
        Dim magid As Integer = Session("MagazijnId")
        Dim desc As String = ""
        If String.IsNullOrWhiteSpace(ASPxComboBoxArtikel.Value) Then
            If Session("taal") = 1 Then
                fouten.Add("Geen artikel geselecteerd!")
                Session("Gelukt") = ""
            Else
                fouten.Add("Aucun article sélectionné!")
                Session("Gelukt") = ""
            End If
            ASPxLabelFout.Text = Session("fout")
            ASPxLabelGelukt.Text = Session("Gelukt")
            Session("fout") = ""
            Session("Gelukt") = ""
            Return
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim s_SQL As String = "select description from basismateriaal where id=@id"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not IsDBNull(dr.Item(0)) Then
                    desc = dr.Item(0)

                End If
            End If

            dr.Close()
            cn.Close()
        End Using
        Dim lijstserie As New List(Of String)
        If HiddenArtikel("SerieVerplichtBijOpboeken") Then
            Aantalinserted = 0
            ASPxTextBoxLotnummer.Visible = True
            ASPxLabelLotnummer.Visible = True
            Dim nrs As New List(Of String)
            For Each textboxId As String In TextBoxIdCollection
                Dim tb As TextBox = PanelSerie.FindControl(textboxId)
                Dim value As String = tb.Text
                If nrs.Contains(value) And Not String.IsNullOrWhiteSpace(value) Then
                    If Session("taal") = 1 Then
                        fouten.Add("Serienummer " & value & " Staat 2 keer in de lijst om in te lezen! Serienummer kan niet worden toegevoegd. Wijzigingen zijn niet doorgevoerd.")

                    Else
                        fouten.Add("Numéro de série " & value & " Peut être lu deux fois dans la liste! Le numéro de série ne peut pas être ajouté. Les changements n'ont pas été mis en œuvre.")

                    End If
                    'ASPxLabelFout.Text = Session("fout")
                    'Session("Gelukt") = ""
                    'ASPxLabelGelukt.Text = Session("Gelukt")
                    'Return
                End If
                nrs.Add(value)
            Next


            For Each textboxId As String In TextBoxIdCollection
                Dim tb As TextBox = PanelSerie.FindControl(textboxId)
                Dim value As String = tb.Text
                'enkel actie uitvoeren als textbox ingevuld is
                If Not String.IsNullOrWhiteSpace(value) Then

                    Dim id As String = (CType(tb, TextBox)).ID
                    Dim regexOk As Boolean = True
                    Dim parts As String() = id.Split(New Char() {"_"c})
                    Dim matId As Integer = parts(2).ToString
                    Dim mat = voocontext.Basismateriaal.Find(matId)
                    If Not String.IsNullOrEmpty(mat.serienummerRegex) Then
                        Dim regex As Regex = New Regex(mat.serienummerRegex)
                        Dim match As Match = regex.Match(value)
                        If Not match.Success Then
                            regexOk = False
                        End If
                    End If
                    If regexOk Then
                        If voocontext.Serienummer.Where(Function(x) x.serienummer1 = value).Any Then
                            If Session("taal") = 1 Then
                                fouten.Add("Serienummer " & value & " bestaat al! Serienummer kan niet worden toegevoegd. Wijzigingen zijn niet doorgevoerd.")

                            Else
                                fouten.Add("Le numéro de série " & value & " existe déjà! Le numéro de série ne peut pas être ajouté. Aucune modification n'a été apportée.")
                            End If
                            'ASPxLabelFout.Text = Session("fout")
                            'Session("Gelukt") = ""
                            'ASPxLabelGelukt.Text = Session("Gelukt")
                            'Return
                        End If
                        Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
                        Dim serienummer As New Serienummer
                        Dim palletno As String = "PALLET No. "
                        While value.Contains("PALLET No.")
                            value.Replace(palletno, "")
                            palletno = palletno.Remove(0, 1)
                        End While

                        lijstserie.Add(value)
                        serienummer.StockMagazijnId = Stockmat.id
                        serienummer.serienummer1 = value
                        serienummer.uitgeboekt = 0
                        serienummer.lotNr = ASPxTextBoxLotnummer.Text
                        serienummer.datumGemaakt = Today()
                        serienummer.statusId = 3
                        serienummer.locatieId = Integer.Parse(ASPxComboBox2.SelectedItem.Value)
                        voocontext.Serienummer.Add(serienummer)
                        Aantalinserted += 1
                        Dim magazijnNaam1 = ASPxComboBoxMagazijn.TextField
                        Dim magazijnLocatie1 = ASPxComboBox2.ID
                        Dim logSerie As New Log
                        logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " ingelezen in " & magazijnNaam1 & " magazijn op locatie" & magazijnLocatie1

                        logSerie.Tijdstip = Today
                        logSerie.Gebruiker = Session("userid")
                        voocontext.Log.Add(logSerie)
                    Else
                        fouten.Add("Serienummer " + value + " komt niet overeen met ingesteld patroon " + mat.serienummerRegex)
                    End If
                End If
            Next
            If fouten.Count = 0 Then
                voocontext.SaveChanges()
                ASPxLabelFout.Text = Session("fout")
                ASPxLabelGelukt.Text = Session("Gelukt")
            Else
                For Each fout In fouten
                    Session("fout") = Session("fout") + fout + "<br/>"
                Next
                ASPxLabelFout.Text = Session("fout")
                Session("fout") = ""
                Session("Gelukt") = ""
                Return
            End If
        End If
        'UPDATE STOCKARTIKEL
        Dim context As New VooEntities
        Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
        Dim stockart = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
        stockart.Aantal = stockart.Aantal + Aantalinserted


        'LOG
        Dim log As New Log
        Dim magazijnNaam = ASPxComboBoxMagazijn.TextField
        Dim magazijnLocatie = ASPxComboBox2.ID
        log.Actie = "Gebruiker " & Session("userid") & " Heeft " & Aantalinserted & " Van artikel " & desc & "ingelezen in " & magazijnNaam & " magazijn op locatie" & magazijnLocatie & Session("uitgevoerdwerk")
        log.Tijdstip = Today
        log.Gebruiker = Session("userid")
        context.Log.Add(log)

        Dim sb As New Stockbeweging
        sb.beweging = Aantalinserted
        sb.datum = DateTime.Now
        sb.gebruiker = Session("userid")
        sb.opmerking = "Inlezen Stock"
        sb.stockmagazijnId = stockart.id

        context.Stockbeweging.Add(sb)

        context.SaveChanges()

        For Each serie In lijstserie
            Dim sbsr As New StockbewegingSerienummer
            sbsr.stockbewegingid = sb.id
            sbsr.serienummerId = context.Serienummer.Where(Function(x) x.serienummer1 = serie).FirstOrDefault.id
            context.StockbewegingSerienummer.Add(sbsr)
        Next

        context.SaveChanges()

        Dim smid As Int32 = stockart.id
        If stockart.Basismateriaal.SerieVerplichtBijOpboeken And stockart.Aantal <> context.Serienummer.Where(Function(x) x.StockMagazijnId = stockart.id And x.uitgeboekt = False And (x.statusId = 1 Or x.statusId = 2 Or x.statusId = 3 Or x.statusId = 4)).Count() Then
            'Bij mat met serienummer, als aantal niet matched
            Dim addresses As New List(Of String)
            addresses.Add("conan.dufour@apkgroup.eu")
            mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                       "noreply@apkgroup.eu",
                       addresses,
                       "Fout is opgetreden bij inlezen stock. Raadpleeg stockbeweging" & sb.id,
                       New Dictionary(Of String, Byte()))
        End If



        context.SaveChanges()
        context.Dispose()
        Session("Gelukt") = Aantalinserted & " van artikel " & desc & " toegevoegd."
        Session("fout") = ""
        ASPxLabelFout.Text = Session("fout")
        ASPxLabelGelukt.Text = Session("Gelukt")
        If Session("Gelukt") <> "" Then
            ASPxSpinEditAantal.Value = 0
            ASPxComboBoxArtikel.Value = 0

        End If

        Session("GeluktCB") = True
        Session("Fout") = ""
        ASPxLabelGelukt.Visible = True
        ASPxSpinEditAantal.Value = 0
        ASPxComboBoxArtikel.Value = 0
        TextBoxIdCollection = New List(Of String)
        HiddenArtikel("heeftSerienummer") = False
        HiddenArtikel("SerieVerplichtBijOpboeken") = False
        ASPxTextBoxLotnummer.Visible = False
        ASPxLabelLotnummer.Visible = False
        ASPxSpinEditAantal_NumberChanged(ASPxSpinEditAantal, New EventArgs)



    End Sub

    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [StockMagazijnId] from [Serienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("StockMagazijnId"))
        Next row
        Session("SelectResult") = result
    End Sub


    'Protected Sub ASPxCallback2_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback2.Callback
    '    Dim voocontext As New VooEntities
    '    Dim magid As Integer = Session("MagazijnId")
    '    Dim desc As String = ""
    '    Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
    '        cn.Open()

    '        Dim s_SQL As String = "select description from basismateriaal where id=@id"
    '        Dim cmd As New SqlCommand(s_SQL, cn)
    '        Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
    '        cmd.Parameters.Add(par)
    '        Dim dr As SqlDataReader = cmd.ExecuteReader
    '        If dr.HasRows Then
    '            dr.Read()
    '            If Not IsDBNull(dr.Item(0)) Then
    '                desc = dr.Item(0)

    '            End If
    '        End If

    '        dr.Close()
    '        cn.Close()
    '    End Using

    '    If HiddenArtikel("SerieVerplichtBijOpboeken") Then
    '        ASPxTextBoxLotnummer.Visible = True
    '        ASPxLabelLotnummer.Visible = True
    '        For Each textboxId As String In TextBoxIdCollection
    '            Dim tb As TextBox = PanelSerie.FindControl(textboxId)
    '            Dim value As String = tb.Text

    '            Dim id As String = (CType(tb, TextBox)).ID
    '            Dim parts As String() = id.Split(New Char() {"_"c})
    '            Dim matId As Integer = parts(1).ToString


    '            Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '            Dim serienummer = voocontext.Serienummer.Where(Function(x) x.serienummer1 = value And x.uitgeboekt <> True).FirstOrDefault
    '            If Not IsNothing(serienummer) And serienummer.StockMagazijn.MagazijnId = magid Then
    '                serienummer.uitgeboekt = 1
    '                serienummer.statusId = 6
    '                serienummer.lotNr = ASPxTextBoxLotnummer.Text
    '                Dim logSerie As New Log
    '                logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " uitgeboekt van hoofdmagazijn "
    '                logSerie.Tijdstip = Today
    '                logSerie.Gebruiker = Session("userid")
    '                voocontext.Log.Add(logSerie)
    '                voocontext.SaveChanges()
    '            Else

    '                ASPxLabelFout.Visible = True
    '                If Session("taal") = 1 Then
    '                    If IsNothing(serienummer) Then
    '                        ASPxLabelFout.Text = "Onbestaande serienummer " & value & " uitgeboekt. Gelieve te controleren en opnieuw te proberen."
    '                    Else
    '                        ASPxLabelFout.Text = "Serienummer " & value & " behoort niet tot het hoofdmagazijn. Gelieve te controleren en opnieuw te proberen."

    '                    End If
    '                Else
    '                    If IsNothing(serienummer) Then
    '                        ASPxLabelFout.Text = "Numéro de série inexistant " & value & " annulé. Veuillez vérifier et réessayer."
    '                    Else
    '                        ASPxLabelFout.Text = "Le numéro de série " & value & " n'appartient pas à l'entrepôt principal. Veuillez vérifier et réessayer."

    '                    End If
    '                End If

    '                Return

    '            End If
    '        Next


    '        'For Each ctr As Control In PlaceHolderSerie.Controls
    '        '    If TypeOf (ctr) Is TextBox Then
    '        '        Dim id As String = (CType(ctr, TextBox)).ID
    '        '        Dim value As String = (CType(ctr, TextBox)).Text
    '        '        Dim parts As String() = id.Split(New Char() {"_"c})
    '        '        Dim matId As Integer = Convert.ToInt32(parts(0))

    '        '        Dim Stockmat = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
    '        '        Dim serienummer As New Serienummer
    '        '        serienummer.StockMagazijnId = Stockmat.id
    '        '        serienummer.serienummer1 = value
    '        '        serienummer.uitgeboekt = 0
    '        '        voocontext.Serienummer.Add(serienummer)

    '        '        Dim logSerie As New Log
    '        '        logSerie.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serienummer.serienummer1 & " Van artikel " & desc & " ingelezen op hoofdmagazijn "
    '        '        logSerie.Tijdstip = Today
    '        '        logSerie.Gebruiker = Session("userid")
    '        '        voocontext.Log.Add(logSerie)
    '        '        voocontext.SaveChanges()
    '        '    End If
    '        'Next
    '        voocontext.SaveChanges()
    '    End If



    '    'UPDATE STOCKARTIKEL
    '    Dim context As New VooEntities

    '    Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
    '    Dim stockart = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
    '    stockart.Aantal = stockart.Aantal - ASPxSpinEditAantal.Number


    '    'LOG
    '    Dim log As New Log
    '    log.Actie = "Gebruiker " & Session("userid") & " Heeft " & ASPxSpinEditAantal.Number & " Van artikel " & desc & "uitgeboekt van hoofdmagazijn " & Session("uitgevoerdwerk")
    '    log.Tijdstip = Today
    '    log.Gebruiker = Session("userid")
    '    context.Log.Add(log)
    '    context.SaveChanges()

    '    context.Dispose()
    '    ASPxSpinEditAantal.Value = 0
    '    ASPxComboBoxArtikel.Value = 0
    'End Sub

    Protected Sub ASPxGridView2_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView2.DataBinding
        DoSelect(SqlDataSourceStockMagazijn.ConnectionString)
    End Sub

    Protected Sub ASPxGridView2_DetailRowGetButtonVisibility(sender As Object, e As ASPxGridViewDetailRowButtonEventArgs) Handles ASPxGridView2.DetailRowGetButtonVisibility
        If Not (CType(Session("SelectResult"), ArrayList)).Contains(e.KeyValue) Then
            e.ButtonState = GridViewDetailRowButtonState.Hidden
        End If
    End Sub

    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("stockmagazijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub


    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender

        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub
End Class