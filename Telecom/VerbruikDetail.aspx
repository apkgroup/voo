﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="VerbruikDetail.aspx.vb" Inherits="Telecom.VerbruikDetail" ViewStateMode="Enabled" %>

<%@ Register assembly="DevExpress.Web.ASPxRichEdit.v19.2, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRichEdit" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>
        var timeout;


        function gridViewAssociations_ToggleSelection(s, e) {
            try {
                console.log(ASPxClientCheckBox.Cast(s).GetChecked());
                setTimeout(function () {
                    ASPxClientGridView.Cast('ASPxGridView1').SelectAllRowsOnPage(ASPxClientCheckBox.Cast(s).GetChecked());
                }, 0);
            } catch (ex) {
                alert('gridViewAssociations_ToggleSelection: ' + ex);
            }
        }

    </script>

    <script type="text/javascript">
        var  serienrs;

            var isAangepast = false;
         
        window.onload = function () {
            
            isAangepast = '<%=Session("isAangepast") %>';
            console.log(isAangepast);
            if (isAangepast!= false && isAangepast!="False") {
                console.log(isAangepast);
         
                 '<%Session("isAangepast") = False %>';
            }
            $('input').keypress(function (e) {
                console.log("KEYPRESS");
                if (e.which == 13) {
                    e.preventDefault();
                    console.log("ISENTER");
                    var self = $(this)
                    var form = self.parents('form:eq(0)');
                    var focusable;
                    var next;
                    var prev;

                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {

                    }
                    return false;

                }
            });
        };

        var timerHandle = -1;
        function OnBatchEditStartEditing(s, e) {
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("hoeveelheid");
            console.log(templateColumn);




        }


        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);
        }

        function saveChangesBtn_Click(s, e) {

            if (ASPxGridView1.batchEditApi.HasChanges())
                ASPxGridView1.UpdateEdit();
            else {
                buttonFlag = true;
                ASPxGridView1.PerformCallback("update");
            }


        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();

        }


    </script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal></h2>

    <br />
    <h2>Verbruik:</h2>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) and hoofd = 1">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
    <br />

    <table>
        <tr><td>Verbruikt van:</td><td>
            <dx:ASPxLabel ID="ASPxLabelVan" runat="server" Text="ASPxLabel">
            </dx:ASPxLabel>
            </td><td>Verbruikt op:</td><td>
            <dx:ASPxLabel ID="ASPxLabelOp" runat="server" Text="ASPxLabel">
            </dx:ASPxLabel>
            </td></tr>
        <tr><td>Verbruikt door:</td><td>
            <dx:ASPxLabel ID="ASPxLabelDoor" runat="server" Text="ASPxLabel">
            </dx:ASPxLabel>
            </td><td>Datum:</td><td>
            <dx:ASPxLabel ID="ASPxLabelDatum" runat="server" Text="ASPxLabel">
            </dx:ASPxLabel>
            </td></tr>
        <tr><td>Ordernummer:</td><td>
            <dx:ASPxLabel ID="ASPxLabelOrder" runat="server" Text="ASPxLabel">
            </dx:ASPxLabel>
            </td><td>Stad</td><td>
            <dx:ASPxLabel ID="ASPxLabelStad" runat="server" Text="ASPxLabel">
            </dx:ASPxLabel>
            </td></tr>
        <tr><td>Straat</td><td>
            <dx:ASPxLabel ID="ASPxLabelStraat" runat="server" Text="ASPxLabel">
            </dx:ASPxLabel>
            </td><td>&nbsp;</td><td>&nbsp;</td></tr>
    </table>
    <p>
        <asp:Label ID="Labelexport" runat="server" Text="Opmerkingen verbruik:"></asp:Label>
        
    </p>

                <dx:ASPxMemo ID="ASPxMemo1" runat="server" Height="71px" Width="567px">
    </dx:ASPxMemo>

                <br />

                <dx:ASPxGridView ID="ASPxGridView1" runat="server" DataSourceID="SqlDataSource1" ClientInstanceName="ASPxGridView1" AutoGenerateColumns="False" KeyFieldName="id" oncustomcallback="ASPxGridView1_CustomCallback" Width="100%" Caption="Verbruik" EnableCallback="True">
                    <ClientSideEvents EndCallback=" function(s, e) { 
                        console.log('callbak');    
                        isAangepast= true;
                if (s.cpExport) {
                        
                       console.log('exportYes');
                    delete s.cpExport;
                    window.open('Export.aspx', '_blank');
                }
            } 
"  BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing"/>
                    
                    <SettingsPager PageSize="100">
                    </SettingsPager>
                    
                    <SettingsEditing Mode="Inline">
                        <BatchEditSettings StartEditAction="Click" />
                    </SettingsEditing>
                    <Settings ShowStatusBar="Hidden" ShowTitlePanel="true" ShowHeaderFilterButton="True" />
                    <SettingsDataSecurity AllowInsert="False" />
                      <Templates>
        <TitlePanel>
        
        </TitlePanel>
    </Templates>
                    <Columns>
                        <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="id" VisibleIndex="1" ReadOnly="True" Visible="False">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="verbruikId" VisibleIndex="4" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="hoeveelheid" VisibleIndex="5">
                            <PropertiesTextEdit>           
</PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="bobijnId" VisibleIndex="6" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="verwerkt" VisibleIndex="7">
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="prijs" VisibleIndex="8">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
<dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="200px">
    <PanelCollection>
        <dx:PanelContent runat="server">
        </dx:PanelContent>
    </PanelCollection>
</dx:ASPxCallbackPanel>
   
    
    <br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Bestellijn] WHERE [Bestellijn].[id] = @id" SelectCommand="SELECT [Verbruiklijn].id
      ,Basismateriaal.Article
	  ,Basismateriaal.Description
      ,[verbruikId]
      ,[hoeveelheid]
      ,[bobijnId], verwerkt, [Verbruiklijn].prijs
  FROM [Voo].[dbo].[Verbruiklijn] 
  INNER JOIN [Basismateriaal] on [Verbruiklijn].[materiaalId] = [Basismateriaal].[id]
INNER JOIN [MateriaalOpdrachtgevers] b on [Basismateriaal].id = b.materiaalId
where [Verbruiklijn].verbruikId=@bestellingId and b.opdrachtgeverId=@opdrachtgever and verbruiklijn.hoeveelheid &gt; 0
"
        UpdateCommand="UPDATE [Verbruiklijn] SET  [Verwerkt] = @verwerkt WHERE ([Verbruiklijn].[id] = @id)">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="bestellingId" QueryStringField="id" />
            <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="verwerkt" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>















    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1" ReportHeader="Bestelling" PaperKind="A4" LeftMargin="1" MaxColumnWidth="200">
        <styles>
            <header font-names="Arial">
            </header>
            <cell font-names="Arial">
            </cell>
            <title font-names="Arial" forecolor="DimGray"></title>
        </styles>
        <PageHeader Center="Bestelling">
        </PageHeader>
    </dx:ASPxGridViewExporter>






    <asp:SqlDataSource ID="SqlDataSourceBestellijnSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT DISTINCT bsr.id, sr.serienummer, bm.Article, bm.Description
  FROM [Voo].[dbo].[BestellijnSerienummer] bsr
  inner join  [Voo].[dbo].[Bestellijn] bs on bsr.[bestellijnId] = bs.id
  inner join  [Voo].[dbo].Bestellingen b on bs.bestellingId = b.id
    inner join  [Voo].[dbo].Serienummer sr on bsr.serienummerId = sr.id
	    inner join  [Voo].[dbo].StockMagazijn sm on sm.id = sr.StockMagazijnId
			    inner join  [Voo].[dbo].Basismateriaal bm on bm.id = sm.MateriaalId
				where b.id=@id">
        <SelectParameters>
            <asp:QueryStringParameter Name="id" QueryStringField="id" />
        </SelectParameters>

    </asp:SqlDataSource>






    <br />


    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Bestellijn] WHERE [Bestellijn].[id] = @id" SelectCommand="SELECT [RetourBestellijn].[id],[RetourBestellijn].[bestellingId], [Article], [Description], [hoeveelheid] FROM [RetourBestellijn] INNER JOIN [Basismateriaal] on [RetourBestellijn].[materiaalId] = [Basismateriaal].[id]
 WHERE [bestellingId] = (select top 1 [Id] from [RetourBestelling] r  where r.gebruikerId = (select gebruikerId from Bestellingen  where id = @bestellingId)  and r.status = 1)"
        UpdateCommand="UPDATE [Bestellijn] SET [hoeveelheid] = @hoeveelheid WHERE ([Bestellijn].[id] = @id)">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="bestellingId" QueryStringField="id" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="hoeveelheid" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>






    <br />






    </asp:Content>
