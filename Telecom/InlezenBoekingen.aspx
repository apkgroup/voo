﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="InlezenBoekingen.aspx.vb" Inherits="Telecom.InlezenBoekingen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #TextArea1 {
            height: 51px;
            width: 102px;
        }


        </style>
    <script>
        var currentRowIndex = undefined;
        var currentRowIndex2 = undefined;

    </script>
    <script type="text/javascript">




    

        window.onload = function () {
          
           console.log("onload");
           $("input[name*='Serienummer']").each(function () {
               console.log(this.id);
               if (this.id.split("_").pop() != document.getElementById("ctl00_ContentPlaceHolder1_ASPxSpinEditAantal_I").value) {
                   this.removeAttribute("onchange");
               }
               else {
                   this.focus();
               }

               if (document.getElementById("ctl00_ContentPlaceHolder1_ASPxSpinEditAantal_I").value == 0) {
                   console.log("Het is 0!");
                   document.getElementById("ctl00_ContentPlaceHolder1_PanelSerie").style.display = "none";
               }

           });


           $('input').keypress(function (e) {
               console.log("KEYPRESS");
               if (e.which == 13) {
                   e.preventDefault();
                   console.log("ISENTER");
                   var self = $(this)
                   var form = self.parents('form:eq(0)');
                   var focusable;
                   var next;
                   var prev;

                   focusable = form.find('input,a,select,button,textarea').filter(':visible');
                   next = focusable.eq(focusable.index(this) + 1);
                   if (next.length) {
                       next.focus();
                   } else {

                   }
                   return false;

               }
           });

           ASPxGridView2.Refresh();
           var i;

           $('input').keypress(function (e) {
               console.log("KEYPRESS");
               if (e.which == 13) {
                   e.preventDefault();
                   console.log("ISENTER");
                   var self = $(this)
                   var form = self.parents('form:eq(0)');
                   var focusable;
                   var next;
                   var prev;

                   focusable = form.find('input,a,select,button,textarea').filter(':visible');
                   next = focusable.eq(focusable.index(this) + 1);
                   if (next.length) {
                       next.focus();
                   } else {

                   }
                   return false;

               }
           });
       };

       var timerHandle = -1;
       var indexrow
       function OnBatchEditStartEditing(s, e) {
           currentRowIndex = e.visibleIndex;
           currentRowIndex2 = e.visibleIndex;
           clearTimeout(timerHandle);
           var templateColumn = s.GetColumnByField("Nombre");
       }

       function OnEndCallBack(s, e) {
           if (s.cpIsUpdated != '') {


               clientText.GetMainElement().style.display = 'block';
               console.log('Waarde komt uit');
               //console.log(e); 
               //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);
               console.log('Temps de Regie updated! Waarde is...');
               //console.log(e); 
               //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);

               console.log(clientButton);
               //console.log(Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML));
               if (ASPxGridView1.GetRow(indexrow).children[3].firstElementChild == null) {
                   if (Number(ASPxGridView1.GetRow(indexrow).children[3].innerHTML.replace(",", ".")) > 0) {
                       console.log("enabled=false");
                       clientButton.SetEnabled(false);
                       clientLabel2.GetMainElement().style.display = 'block'
                   }
                   else {
                       console.log("enabled=true");
                       clientButton.SetEnabled(true);
                       clientLabel2.GetMainElement().style.display = 'none'
                   }
               }
               else {
                   if (Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML.replace(",", ".")) > 0) {
                       console.log("enabled=false");
                       clientButton.SetEnabled(false);
                       clientLabel2.GetMainElement().style.display = 'block'
                   }
                   else {
                       console.log("enabled=true");
                       clientButton.SetEnabled(true);
                       clientLabel2.GetMainElement().style.display = 'none'
                   }
               }


               //console.log(s.cpIsUpdated);


           }
           else {
               clientText.SetText('');
               console.log('Anders updated');
           }
       }

       function OnKeyDown(s, e) {
           var key = e.htmlEvent.keyCode;
           console.log("key pressed!");
           if (key == 38 || key == 40) {
               console.log("Up or down key pressed!");
               ASPxClientUtils.PreventEvent(e.htmlEvent);
               ASPxGridView1.batchEditApi.EndEdit();
               console.log(currentRowIndex);
               if (currentRowIndex !== undefined && currentRowIndex !== null) {
                   var visibleIndices = ASPxGridView1.batchEditApi.GetRowVisibleIndices();
                   var indicesArrayLength = visibleIndices.length;
                   var rowIndex = visibleIndices.indexOf(currentRowIndex);
                   if (!(rowIndex === 0 && key === 38) || !(rowIndex === indicesArrayLength - 1 && key === 40)) {
                       switch (key) {
                           case 38:
                               ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex - 1], 4); // Adjust the second argument according
                               // to the 'Nombre' column index
                               break;
                           case 40:
                               ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex + 1], 4); // Adjust the second argument according
                               // to the 'Nombre' column index
                               break;
                           default:
                               break;
                       }
                   }
               }
           }
       }

       function OnKeyDownMat(s, e) {
           var key = e.htmlEvent.keyCode;
           console.log("key pressed!");
           if (key == 38 || key == 40) {
               console.log("Up or down key pressed");
               ASPxClientUtils.PreventEvent(e.htmlEvent);
               ASPxGridView2.batchEditApi.EndEdit();
               console.log(currentRowIndex2);
               if (currentRowIndex2 !== undefined && currentRowIndex2 !== null) {
                   var visibleIndices2 = ASPxGridView2.batchEditApi.GetRowVisibleIndices();
                   var indicesArrayLength2 = visibleIndices2.length;
                   var rowIndex2 = visibleIndices2.indexOf(currentRowIndex2);
                   if (!(rowIndex2 === 0 && key === 38) || !(rowIndex2 === indicesArrayLength2 - 1 && key === 40)) {
                       console.log("ERIN");
                       switch (key) {
                           case 38:
                               ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 - 1], 4); // Adjust the second argument according
                               // to the 'Nombre' column index
                               break;
                           case 40:
                               ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 + 1], 4); // Adjust the second argument according
                               // to the 'Nombre' column index
                               break;
                           default:
                               break;
                       }
                   }
               }
           }
       }



       function OnBatchEditEndEditing(s, e) {
           timerHandle = setTimeout(function () {
               s.UpdateEdit();
           }, 50);

           indexrow = e.visibleIndex;
           console.log("Index row set to " + indexrow);
           //if (s.cpIsUpdated != '') {

           //    console.log(clientText.GetMainElement());
           //    console.log(clientText);
           //    clientText.GetMainElement().style.display = 'none';
           //    console.log('Temps de Regie updated! Waarde is...');
           //    console.log(e);
           //    //console.log(ASPxGridView1.GetRow(e.visibleIndex).children[3].innerHTML);
           //    console.log(clientButton);
           //    clientButton.setEnabled(true);
           //    console.log(s.cpIsUpdated);


           //}
           //else {
           //    clientText.SetText('');
           //    console.log('Anders updated');
           //}

       }

       function saveChangesBtn_Click(s, e) {
           ASPxGridView1.UpdateEdit();
           ASPxGridView2.UpdateEdit();
       }

       function cancelChangesBtn_Click(s, e) {
           ASPxGridView1.CancelEdit();
           ASPxGridView2.CancelEdit();
       }




       function saveChangesBtnMat_Click(s, e) {
           ASPxGridView2.UpdateEdit();
       }

       function cancelChangesBtnMat_Click(s, e) {
           ASPxGridView2.CancelEdit();



       }


       function insertArticle() {
           console.log("IETest");
           Callback1.PerformCallback();
           console.log("IEAfterCallback");
       }



       function OnCallbackComplete(s, e) {
           console.log("cbCOMPLET");
           ASPxGridView2.Refresh();
           //location.reload();

        }

        function OnClick() {  
    //cp.PerformCallback();  
    ASPxLoadingPanel2.Show();  
}  




    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Upload boekingen file
    </h2>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) and hoofd =1">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
    <p><asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </p>

    <dx:ASPxLabel ID="ASPxLabelGelukt" runat="server" Text="ASPxLabelOK" Font-Bold="True" ForeColor="#009900">
    </dx:ASPxLabel>

     <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" Text="ASPxLabel" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
                </dx:ASPxLabel>

    

    <br />
  
    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel2" runat="server" ClientInstanceName="ASPxLoadingPanel2">
    </dx:ASPxLoadingPanel>

  <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server">
    <Items>
        <dx:LayoutItem ColSpan="1" ShowCaption="False">
            <LayoutItemNestedControlCollection>
                <dx:LayoutItemNestedControlContainer runat="server">
                    <dx:ASPxUploadControl ID="ASPxUploadControlShipment" runat="server">
                        <ValidationSettings AllowedFileExtensions=".xls, .xlsx, .csv" MaxFileCount="1">
                        </ValidationSettings>
                    </dx:ASPxUploadControl>
                </dx:LayoutItemNestedControlContainer>
            </LayoutItemNestedControlCollection>
        </dx:LayoutItem>
        <dx:LayoutItem ColSpan="1" ShowCaption="False">
            <LayoutItemNestedControlCollection>
                <dx:LayoutItemNestedControlContainer runat="server">
                    <dx:ASPxButton ID="ASPxFormLayout1_E1" runat="server" Text="Upload">
                        <ClientSideEvents Click="OnClick" /> 
                    </dx:ASPxButton>
                </dx:LayoutItemNestedControlContainer>
            </LayoutItemNestedControlCollection>
        </dx:LayoutItem>
    </Items>
</dx:ASPxFormLayout>
    <asp:SqlDataSource ID="SqlDataSourceShipments" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Shipment] WHERE [id] = @id" InsertCommand="INSERT INTO [Shipment] ([datereceived], [vrijgegeven], [uploadedby], [vrijgavedatum], [shipmentNumber], [accepted], [opdrachtgeverId], [magazijnId]) VALUES (@datereceived, @vrijgegeven, @uploadedby, @vrijgavedatum, @shipmentNumber, @accepted, @opdrachtgeverId, @magazijnId)" SelectCommand="SELECT * FROM [Shipment] WHERE ([opdrachtgeverId] = @opdrachtgeverId) ORDER BY [datereceived] DESC" UpdateCommand="UPDATE [Shipment] SET [datereceived] = @datereceived, [vrijgegeven] = @vrijgegeven, [uploadedby] = @uploadedby, [vrijgavedatum] = @vrijgavedatum, [shipmentNumber] = @shipmentNumber, [accepted] = @accepted, [opdrachtgeverId] = @opdrachtgeverId, [magazijnId] = @magazijnId WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="datereceived" Type="DateTime" />
            <asp:Parameter Name="vrijgegeven" Type="Boolean" />
            <asp:Parameter Name="uploadedby" Type="Int32" />
            <asp:Parameter Name="vrijgavedatum" Type="DateTime" />
            <asp:Parameter Name="shipmentNumber" Type="String" />
            <asp:Parameter Name="accepted" Type="Boolean" />
            <asp:Parameter Name="opdrachtgeverId" Type="Int32" />
            <asp:Parameter Name="magazijnId" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="datereceived" Type="DateTime" />
            <asp:Parameter Name="vrijgegeven" Type="Boolean" />
            <asp:Parameter Name="uploadedby" Type="Int32" />
            <asp:Parameter Name="vrijgavedatum" Type="DateTime" />
            <asp:Parameter Name="shipmentNumber" Type="String" />
            <asp:Parameter Name="accepted" Type="Boolean" />
            <asp:Parameter Name="opdrachtgeverId" Type="Int32" />
            <asp:Parameter Name="magazijnId" Type="Int32" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourcePallets" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT DISTINCT palletNo
FROM [Serienummer] sr
WHERE (shipmentId = @shipmentId)">
        <SelectParameters>
            <asp:SessionParameter Name="shipmentId" SessionField="shipment" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />


</asp:Content>
