﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Werken.aspx.vb" Inherits="Telecom.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 27px;
        }
        .auto-style2 {
            height: 55px;
        }
        .auto-style3 {
            height: 28px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        </h2>
    <table style="border-collapse: collapse; border-spacing: inherit">
        <tr><td colspan="4">
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Registrer un opération pour ">
            </dx:ASPxLabel>
    <dx:ASPxComboBox ID="ASPxComboBox1" runat="server" DataSourceID="SqlDataSourceGebruikers" TextField="Werknemer" ValueField="id">
    </dx:ASPxComboBox>
            <asp:SqlDataSource ID="SqlDataSourceGebruikers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [Gebruikers].id, (
SELECT 
CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam
ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)
END
) as Werknemer
FROM [Gebruikers]
"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceTarieven" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Tarief]"></asp:SqlDataSource>
            </td></tr>
        <tr>
            <td class="auto-style1">Date d'opération: </td><td class="auto-style1">
            
            
            <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" EditFormat="DateTime">
            </dx:ASPxDateEdit>
            </td>  
        </tr>
        <tr>
            <td class="auto-style3">Numero d'opération: </td><td class="auto-style3">
            <dx:ASPxTextBox ID="ASPxTextBoxOperatieNr" runat="server" Theme="Default" Width="170px">
            </dx:ASPxTextBox>
            </td>  
        </tr>
                <tr>
            <td class="auto-style1">Tarif: </td><td class="auto-style1">

    <dx:ASPxComboBox ID="ASPxComboBox2" runat="server" DataSourceID="SqlDataSourceTarieven" TextField="beschrijving" ValueField="id">
    </dx:ASPxComboBox>

            </td>  
        </tr>
        <tr>
            <td class="auto-style1">Code Postal: </td><td class="auto-style2">
            <dx:ASPxComboBox ID="ASPxComboBoxPostcode" runat="server">
                <Items>
                    <dx:ListEditItem Text="1050 Ixelles" Value="1050" />
                    <dx:ListEditItem Text="1060 Saint-Gilles" Value="1060" />
                    <dx:ListEditItem Text="1140 Evere" Value="1140" />
                    <dx:ListEditItem Text="1150 Woluwe-Saint-Pierre" Value="1150" />
                    <dx:ListEditItem Text="1160 Auderghem" Value="1160" />
                    <dx:ListEditItem Text="1180 Uccle" Value="1180" />
                    <dx:ListEditItem Text="1300 Wavre" Value="1300" />
                    <dx:ListEditItem Text="1301 Bierges" Value="1301" />
                    <dx:ListEditItem Text="1315 Incourt" Value="1315" />
                    <dx:ListEditItem Text="1320 Beauvechain" Value="1320" />
                </Items>
            </dx:ASPxComboBox>
            </td>  
        </tr>
        <tr>
            <td>Commentaire: </td><td>
           
           
            <asp:TextBox ID="TextBoxOpmerking" TextMode="multiline" Columns="50" Rows="5" runat="server"></asp:TextBox>
           
           
            </td>  
        </tr>
        <tr>
            <td>Suivant </td><td>
            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/Last_16x16.png"/>
            
            </td>  
        </tr>
    </table>
</asp:Content>
