﻿Imports System.Data.SqlClient

Public Class NieuwsToevoegen
    Inherits System.Web.UI.Page
    Sub Vertaal(taalId As Integer)
        If taalId = 1 Then

            ASPxLabelTital.Text = "Titel:"
            ASPxLabelAfbeelding.Text = "Afbeelding (optioneel):"

            Literal2.Text = "Nieuws toevoegen"
        Else
            ASPxLabelTital.Text = "Titre:"
            ASPxLabelAfbeelding.Text = "Image(facultatif)"

        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
        If Not Page.IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("id")) Then
                If Session("taal") = 1 Then
                    Literal2.Text = "Nieuwsitem aanpassen"
                Else
                    Literal2.Text = "Modifier nouvelles"
                End If

                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()
                    Dim s_SQL As String = "SELECT Titel, Detail FROM Nieuws WHERE (id=@id)"
                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                    cmd.Parameters.Add(par)
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    If dr.HasRows Then
                        dr.Read()
                        Me.aspTextBoxTitel.Text = dr.GetString(0)
                        Me.ASPxHtmlEditor1.Html = dr.GetString(1)

                    End If
                    dr.Close()
                End Using
            Else

            End If
        End If

    End Sub

    Private Sub ASPxMenu1_ItemClick(source As Object, e As DevExpress.Web.MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "Bewaren" Then

            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = ""
                If Not String.IsNullOrEmpty(Request.QueryString("id")) Then
                    Try
                        Debug.Write(vbCrLf & Me.ASPxHtmlEditor1.Html)
                        s_SQL = "UPDATE Nieuws SET Titel=@titel, Detail=@detail, Afbeeldinglink=@afbeeldinglink, Tekst=@tekst WHERE (id=@id)"
                        Dim cmd As New SqlCommand(s_SQL, cn)
                        Dim par As New SqlParameter("@titel", SqlDbType.NVarChar, 100) With {.Value = Me.aspTextBoxTitel.Text}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@detail", SqlDbType.NVarChar, -1) With {.Value = Me.ASPxHtmlEditor1.Html}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@afbeeldinglink", SqlDbType.NVarChar, 255) With {.Value = IIf(FileUpload1.HasFile = True, String.Format("{0}/images/nieuws/{1}", Session("domein"), FileUpload1.FileName.Replace(" ", "_")), DBNull.Value)}
                        cmd.Parameters.Add(par)
                        Dim strFile As String = String.Format("{0}{1:yyyyMMdd_HHmmss}.txt", Server.MapPath("images/nieuws/"), DateTime.Now)
                        If IO.File.Exists(strFile) Then IO.File.Delete(strFile)
                        Using output As New System.IO.FileStream(strFile, IO.FileMode.Create)
                            ASPxHtmlEditor1.Export(DevExpress.Web.ASPxHtmlEditor.HtmlEditorExportFormat.Txt, output)
                        End Using
                        par = New SqlParameter("@tekst", SqlDbType.NVarChar, -1) With {.Value = IO.File.ReadAllText(strFile)}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@id", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                        If IO.File.Exists(strFile) Then IO.File.Delete(strFile)
                        If FileUpload1.HasFile = True Then
                            Dim bestandsnaam As String = IO.Path.GetFileName(FileUpload1.FileName)
                            Dim nieuwenaam As String = Server.MapPath("images/nieuws/") & bestandsnaam.Replace(" ", "_")
                            If IO.File.Exists(nieuwenaam) Then IO.File.Delete(nieuwenaam)
                            FileUpload1.SaveAs(nieuwenaam)
                        End If
                        cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Nieuwsitem met titel '{0}' bijgewerkt door {1} {2}", Me.aspTextBoxTitel.Text, Session("naam"), Session("voornaam"))}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij bijwerken nieuwsitem {0} door {1} {2}, omschrijving: {3}", Request.QueryString("id"), Session("naam"), Session("voornaam"), ex.Message)}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    End Try

                Else
                    Try
                        s_SQL = "INSERT INTO Nieuws (Titel, Datum, Actief, Detail, Afbeeldinglink, Tekst, opdrachtgeverId) VALUES (@titel, GetDate(),1, @detail, @afbeeldinglink, @tekst, @opdrachtgeverId); SELECT SCOPE_IDENTITY()"
                        Dim cmd As New SqlCommand(s_SQL, cn)
                        Dim par As New SqlParameter("@titel", SqlDbType.NVarChar, 100) With {.Value = Me.aspTextBoxTitel.Text}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@detail", SqlDbType.NVarChar, -1) With {.Value = Me.ASPxHtmlEditor1.Html}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@afbeeldinglink", SqlDbType.NVarChar, 255) With {.Value = IIf(FileUpload1.HasFile = True, String.Format("{0}/images/nieuws/{1}", Session("domein"), FileUpload1.FileName.Replace(" ", "_")), DBNull.Value)}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int, -1) With {.Value = Session("opdrachtgever")}
                        cmd.Parameters.Add(par)

                        Dim strFile As String = String.Format("{0}{1:yyyyMMdd_HHmmss}.txt", Server.MapPath("images/nieuws/"), DateTime.Now)
                        If IO.File.Exists(strFile) Then IO.File.Delete(strFile)
                        Using output As New System.IO.FileStream(strFile, IO.FileMode.Create)
                            ASPxHtmlEditor1.Export(DevExpress.Web.ASPxHtmlEditor.HtmlEditorExportFormat.Txt, output)
                        End Using

                        par = New SqlParameter("@tekst", SqlDbType.NVarChar, -1) With {.Value = IO.File.ReadAllText(strFile)}
                        cmd.Parameters.Add(par)
                        Dim nieuwsId As Integer
                        nieuwsId = cmd.ExecuteNonQuery()
                        If IO.File.Exists(strFile) Then IO.File.Delete(strFile)
                        If FileUpload1.HasFile = True Then
                            Dim bestandsnaam As String = IO.Path.GetFileName(FileUpload1.FileName)
                            Dim nieuwenaam As String = Server.MapPath("images/nieuws/") & bestandsnaam.Replace(" ", "_")
                            If IO.File.Exists(nieuwenaam) Then IO.File.Delete(nieuwenaam)
                            FileUpload1.SaveAs(nieuwenaam)
                        End If
                        cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Nieuwsitem met titel '{0}' toegevoegd door {1} {2}", Me.aspTextBoxTitel.Text, Session("naam"), Session("voornaam"))}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()

                        'stored procedure om het nieuwe nieuwsitem in "gelezen" te zetten met alle werknemers

                        cmd = New SqlCommand With {.Connection = cn, .CommandText = "prNieuwsitemsGelezen", .CommandType = CommandType.StoredProcedure}
                        par = New SqlParameter("@nieuwsid", SqlDbType.Int) With {.Value = nieuwsId}
                        cmd.Parameters.Add(par)

                        cmd.ExecuteReader()

                        '
                    Catch ex As Exception
                        Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij toevoegen nieuwsitem door {1} {2}, omschrijving: {3}", Request.QueryString("id"), Session("naam"), Session("voornaam"), ex.Message)}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    End Try

                End If

            End Using

            Response.Redirect("~/Default.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

    End Sub


End Class