﻿Imports System.Data.SqlClient
Imports DevExpress.Web
Imports Microsoft.Exchange.WebServices.Data
Imports System.IO
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class RetourBestellingArtikel
    Private artNummer As String
    Property nummer() As String
        Get
            Return artNummer
        End Get
        Set(ByVal Value As String)
            artNummer = Value
        End Set
    End Property

    Private artBeschrijving As String
    Property beschrijving() As String
        Get
            Return artBeschrijving
        End Get
        Set(ByVal Value As String)
            artBeschrijving = Value
        End Set
    End Property

    Private artHoeveelheid As Double
    Property hoeveelheid() As Double
        Get
            Return artHoeveelheid
        End Get
        Set(ByVal Value As Double)
            artHoeveelheid = Value
        End Set
    End Property

    Private artHoeveelheidoud As Double
    Property oudhoeveelheid() As Double
        Get
            Return artHoeveelheidoud
        End Get
        Set(ByVal Value As Double)
            artHoeveelheidoud = Value
        End Set
    End Property

End Class
Public Class RetourBestelling
    Inherits System.Web.UI.Page

    Protected Sub ASPxGridView1_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBinding
        DoSelect(SqlDataSource1.ConnectionString)
    End Sub

    Protected Sub ASPxGridView2_DetailRowGetButtonVisibility(sender As Object, e As ASPxGridViewDetailRowButtonEventArgs) Handles ASPxGridView1.DetailRowGetButtonVisibility
        If Not (CType(Session("SelectResult"), ArrayList)).Contains(e.KeyValue) Then
            e.ButtonState = GridViewDetailRowButtonState.Hidden
        End If
    End Sub

    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("bestellijnId") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub

    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [bestellijnId] from [RetourBestellijnSerienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("bestellijnId"))
        Next row
        Session("SelectResult") = result
    End Sub


    Protected Sub vertaal(taalid)
        If taalid = 1 Then

            ASPxGridView1.Columns("Article").Caption = "Artikel"
            ASPxGridView1.Columns("Description").Caption = "Omschrijving"
            ASPxGridView1.Columns("hoeveelheid").Caption = "Aantal"


            Labelexport.Text = "Opgelet! Bij het klikken op exporteren moet u pop-ups in uw browser toelaten"

            ASPxButton1.Text = "Goedkeuren"
            ASPxButton2.Text = "Afkeuren"

        Else
            ASPxGridView1.Columns("Werknemer").Caption = "Empl."
            ASPxGridView1.Columns("beschrijving").Caption = "Statut"
            ASPxGridView1.Columns("Datum").Caption = "Date"
            Labelexport.Text = "Attention! En cliquant sur Exporter vous devez autoriser les pop ups en haut de votre navigateur"


        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))



        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If




        If Not Page.IsPostBack Then
            Dim status As Integer
            Dim statusText As String

            If Not String.IsNullOrEmpty(Request.QueryString("id")) Then


                Dim context As New VooEntities
                Dim bestellingId As Integer = Request.QueryString("id")
                Dim bestelling = context.Bestellingen.Find(bestellingId)


                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()
                    Dim s_SQL As String = "SELECT (SELECT CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn) END) as Naam, Status FROM RetourBestelling Inner join Gebruikers on RetourBestelling.GebruikerId = Gebruikers.id where RetourBestelling.id = @id"
                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                    cmd.Parameters.Add(par)
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    If dr.HasRows Then
                        dr.Read()
                        status = dr.GetInt32(1)


                        If Session("taal") = 1 Then

                            Select Case status
                                Case 1
                                    statusText = "Aangevraagd"
                                Case 3
                                    statusText = "Afgekeurd"
                                Case 2
                                    statusText = "Goedgekeurd"
                            End Select
                            Literal2.Text = "Bestelling " & Request.QueryString("id") & " van " + dr.GetString(0) + ": " + statusText
                            ASPxGridView1.SettingsText.Title = "Retour: " & Request.QueryString("id") & " " & dr.GetString(0)
                        Else

                            Select Case status
                                Case 1
                                    statusText = "demandé"
                                Case 3
                                    statusText = "rejeté"
                                Case 2
                                    statusText = "approuvé"
                            End Select
                            Literal2.Text = "Commande " & Request.QueryString("id") & " de " + dr.GetString(0) + ": " + statusText
                            ASPxGridView1.SettingsText.Title = "Retour: " & Request.QueryString("id") & " " & dr.GetString(0)
                        End If




                    End If
                    dr.Close()
                End Using


                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                    cn.Open()
                    Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                    cmd.Parameters.Add(par)
                    Dim dr2 As SqlDataReader = cmd.ExecuteReader
                    If dr2.HasRows Then
                        dr2.Read()
                        Session("MagazijnId") = dr2.GetInt32(0)
                    End If
                    dr2.Close()
                    cn.Close()
                End Using

                If Not status = 1 Then
                    ASPxPanel1.Visible = False
                    ASPxPanelSerie.Visible = False
                    ASPxGridView1.DataColumns.Item("hoeveelheid").EditFormSettings.Visible = 1
                Else
                    ASPxPanel1.Visible = True
                    ASPxPanelSerie.Visible = True
                End If
            End If

        End If
        If Not Session("isadmin") Then
            ASPxPanel1.Visible = False
            ASPxPanelSerie.Visible = False
            ASPxGridView1.DataColumns.Item("hoeveelheid").EditFormSettings.Visible = 1
        End If


    End Sub

    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender
        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub


    Protected Sub ASPxButton2_Click(sender As Object, e As EventArgs) Handles ASPxButton2.Click
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "UPDATE [RetourBestelling] SET [Status] = 3  WHERE ([RetourBestelling].[id] = @bestellingId)"

            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteReader()
        End Using
        Dim username As String
        Dim userid As String
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim s_SQL As String = "SELECT 'Gebruiker', [gebruikers].id FROM [RetourBestelling] inner join [Gebruikers] on [RetourBestelling].[GebruikerId] = [Gebruikers].[id] where [RetourBestelling].id = @bestellingId"

            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                username = dr.GetValue(0)
                userid = dr.GetValue(1)
            End If
            dr.Close()
        End Using

        'Verwittigen(username, userid, "rejeté")

        Response.Redirect("~/RetourBestelling.aspx?id=" + Request.QueryString("id"))
        Context.ApplicationInstance.CompleteRequest()



    End Sub

    'Protected Sub ASPxGridView1_AfterPerformCallback(sender As Object, e As ASPxGridViewAfterPerformCallbackEventArgs) Handles ASPxGridView1.AfterPerformCallback
    '    If Not Session("export") = "true" Then
    '        ASPxWebControl.RedirectOnCallback(VirtualPathUtility.ToAbsolute("~/Bestelling.aspx?id=" + Request.QueryString("id")))
    '        Context.ApplicationInstance.CompleteRequest()
    '    End If

    'End Sub



    Protected Sub ASPxGridView1_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles ASPxGridView1.CustomCallback

        If e.Parameters = "update" Then
            Dim username As String
            Dim userid As String
            ASPxGridView1.UpdateEdit()
            Dim Voocontext As New VooEntities
            Dim bestellingid As Integer = Request.QueryString("id")

            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()

                Dim s_SQL As String = "SELECT (SELECT CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)END) as Naam, [gebruikers].id FROM [RetourBestelling] inner join [Gebruikers] on [RetourBestelling].[GebruikerId] = [Gebruikers].[id] where [RetourBestelling].id = @bestellingId"

                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()
                    username = dr.GetValue(0)
                    userid = dr.GetValue(1)
                End If
                dr.Close()
            End Using

            Dim bestellijnen = Voocontext.RetourBestellijn.Where(Function(x) x.bestellingId = bestellingid).ToList
            Dim intuserId As Integer = CInt(userid)
            Dim magid As Integer = Voocontext.Gebruikers.Find(intuserId).MagazijnId
            Dim opdrachtgeverId As Integer = Session("opdrachtgever")
            Dim hoofdmagId As Integer = Session("MagazijnId")


            For Each item In bestellijnen

                If Voocontext.Basismateriaal.Where(Function(x) x.id = item.materiaalId).FirstOrDefault.retourItem Or Not Voocontext.Basismateriaal.Where(Function(x) x.id = item.materiaalId).FirstOrDefault.HeeftSerienummer Then

                    If Not Voocontext.Basismateriaal.Where(Function(x) x.id = item.materiaalId).FirstOrDefault.HeeftSerienummer Then
                        Dim Stockmat = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = item.materiaalId).FirstOrDefault
                        Stockmat.Aantal = Stockmat.Aantal - item.hoeveelheid

                        'Alles is ok, log stockbeweging doelmagazijn
                        Dim sb As New Stockbeweging
                        sb.beweging = -item.hoeveelheid
                        sb.datum = DateTime.Now
                        sb.gebruiker = Session("userid")
                        sb.opmerking = "Retour (user)"
                        sb.stockmagazijnId = Stockmat.id
                        Voocontext.Stockbeweging.Add(sb)


                        Dim smid As Int32 = Stockmat.id
                        If Stockmat.Basismateriaal.SerieVerplichtBijOpboeken And Stockmat.Aantal <> Voocontext.Serienummer.Where(Function(x) x.StockMagazijnId = Stockmat.id).Count() Then
                            'Bij mat met serienummer, als aantal niet matched
                            Dim addresses As New List(Of String)
                            addresses.Add("conan.dufour@apkgroup.eu")
                            mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                               "noreply@apkgroup.eu",
                               addresses,
                               "Fout is opgetreden bij Retour. Raadpleeg stockbeweging" & sb.id,
                               New Dictionary(Of String, Byte()))
                        End If
                    End If
                    Dim Stockmathoofd = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = hoofdmagId And x.MateriaalId = item.materiaalId).FirstOrDefault
                    Stockmathoofd.Aantal = Stockmathoofd.Aantal + item.hoeveelheid



                    'Alles is ok, log stockbeweging hoofdmagazijn
                    Dim sbhoofd As New Stockbeweging
                    sbhoofd.beweging = item.hoeveelheid
                    sbhoofd.datum = Today
                    sbhoofd.gebruiker = Session("userid")
                    sbhoofd.opmerking = "Retour (hoofd)"
                    sbhoofd.stockmagazijnId = Stockmathoofd.id
                    Voocontext.Stockbeweging.Add(sbhoofd)


                    Dim smhoofdid As Int32 = Stockmathoofd.id
                    If Stockmathoofd.Basismateriaal.SerieVerplichtBijOpboeken And Stockmathoofd.Aantal <> Voocontext.Serienummer.Where(Function(x) x.StockMagazijnId = Stockmathoofd.id).Count() Then
                        'Bij mat met serienummer, als aantal niet matched
                        Dim addresses As New List(Of String)
                        addresses.Add("conan.dufour@apkgroup.eu")
                        mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                           "noreply@apkgroup.eu",
                           addresses,
                           "Fout is opgetreden bij Retour. Raadpleeg stockbeweging" & sbhoofd.id,
                           New Dictionary(Of String, Byte()))
                    End If

                    For Each serienummerretour In item.RetourBestellijnSerienummer
                        Dim serie As New Serienummer
                        serie.datumOntvangst = Today
                        serie.serienummer1 = serienummerretour.serienummer
                        serie.statusId = 2
                        serie.datumGemaakt = Today
                        serie.uitgeboekt = False

                        serie.StockMagazijnId = Voocontext.StockMagazijn.Where(Function(x) x.MateriaalId = item.materiaalId And x.MagazijnId = hoofdmagId).FirstOrDefault.id
                        Voocontext.Serienummer.Add(serie)
                    Next
                Else
                    Dim Stockmat = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = item.materiaalId).FirstOrDefault
                    Stockmat.Aantal = Stockmat.Aantal - item.hoeveelheid

                    Dim Stockmathoofd = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = hoofdmagId And x.MateriaalId = item.materiaalId).FirstOrDefault
                    Stockmathoofd.Aantal = Stockmathoofd.Aantal + item.hoeveelheid

                    'Alles is ok, log stockbeweging doelmagazijn
                    Dim sb As New Stockbeweging
                    sb.beweging = item.hoeveelheid
                    sb.datum = DateTime.Now
                    sb.gebruiker = Session("userid")
                    sb.opmerking = "Retour (user)"
                    sb.stockmagazijnId = Stockmat.id
                    Voocontext.Stockbeweging.Add(sb)


                    Dim smid As Int32 = Stockmat.id
                    If Stockmat.Basismateriaal.SerieVerplichtBijOpboeken And Stockmat.Aantal <> Voocontext.Serienummer.Where(Function(x) x.StockMagazijnId = Stockmat.id).Count() Then
                        'Bij mat met serienummer, als aantal niet matched
                        Dim addresses As New List(Of String)
                        addresses.Add("conan.dufour@apkgroup.eu")
                        mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                           "noreply@apkgroup.eu",
                           addresses,
                           "Fout is opgetreden bij Retour. Raadpleeg stockbeweging" & sb.id,
                           New Dictionary(Of String, Byte()))
                    End If

                    'Alles is ok, log stockbeweging hoofdmagazijn
                    Dim sbhoofd As New Stockbeweging
                    sbhoofd.beweging = -item.hoeveelheid
                    sbhoofd.datum = Today
                    sbhoofd.gebruiker = Session("userid")
                    sbhoofd.opmerking = "Retour (hoofd)"
                    sbhoofd.stockmagazijnId = Stockmathoofd.id
                    Voocontext.Stockbeweging.Add(sbhoofd)


                    Dim smhoofdid As Int32 = Stockmathoofd.id
                    If Stockmathoofd.Basismateriaal.SerieVerplichtBijOpboeken And Stockmathoofd.Aantal <> Voocontext.Serienummer.Where(Function(x) x.StockMagazijnId = Stockmathoofd.id).Count() Then
                        'Bij mat met serienummer, als aantal niet matched
                        Dim addresses As New List(Of String)
                        addresses.Add("conan.dufour@apkgroup.eu")
                        mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                           "noreply@apkgroup.eu",
                           addresses,
                           "Fout is opgetreden bij Retour. Raadpleeg stockbeweging" & sbhoofd.id,
                           New Dictionary(Of String, Byte()))
                    End If

                    For Each serienummerretour In item.RetourBestellijnSerienummer

                        If Not Voocontext.Serienummer.Where(Function(x) x.serienummer1 = serienummerretour.serienummer).Any Then
                            'serienummer bestaat niet in lijst van serienummers

                        End If
                        Voocontext.Serienummer.Where(Function(x) x.serienummer1 = serienummerretour.serienummer).FirstOrDefault.StockMagazijnId = Stockmathoofd.id
                        Voocontext.Serienummer.Where(Function(x) x.serienummer1 = serienummerretour.serienummer).FirstOrDefault.statusId = serienummerretour.status
                    Next
                End If

            Next

            Voocontext.SaveChanges()


            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = "UPDATE [RetourBestelling] SET [Status] = 2  WHERE ([RetourBestelling].[id] = @bestellingId)"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteReader()

                cn.Close()

            End Using




            ASPxWebControl.RedirectOnCallback(VirtualPathUtility.ToAbsolute("~/RetourBestelling.aspx?id=" + Request.QueryString("id")))
            Context.ApplicationInstance.CompleteRequest()

        End If
        If e.Parameters = "CEXP" Then
            Dim ps As New PrintingSystemBase()

            Dim link1 As New PrintableComponentLinkBase(ps)
            link1.Component = ASPxGridViewExporter1

            Dim link2 As New PrintableComponentLinkBase(ps)
            link2.Component = ASPxGridViewExporter1

            Dim compositeLink As New CompositeLinkBase(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()

            Using stream As New MemoryStream()
                compositeLink.ExportToPdf(stream)
                Session("gridData") = stream.ToArray()
            End Using

            Session("export") = "true"

            ASPxGridView1.JSProperties("cpExport") = True
        End If

    End Sub

    Protected Sub ConfirmButton_Click(sender As Object, e As EventArgs)
        For Each ctr As System.Web.UI.Control In PlaceHolderSerie.Controls
            If TypeOf (ctr) Is ASPxTextBox Then
                Dim value As String = (CType(ctr, TextBox)).Text
            End If
        Next
    End Sub


    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim editingRowVisibleIndex As Integer = ASPxGridView1.FindVisibleIndexByKeyValue(e.Keys.Values(0))
        Session("bestelling") = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Poste")




    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click

    End Sub
End Class