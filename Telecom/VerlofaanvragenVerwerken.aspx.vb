﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data

Public Class VerlofaanvragenVerwerken
    Inherits System.Web.UI.Page

    Private dicVerlof As Dictionary(Of Date, VerlofInfo)

    Enum typeVerlof As Integer
        Voorbij = 0
        Goedgekeurd = 1
        Afgekeurd = 2
        Aangevraagd = 3
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
           Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using

        If Not Page.IsPostBack Then
            ASPxCalendar1.VisibleDate = DateSerial(DateTime.Now.Year, 1, 1)
        End If
        If Session("verlofgebruiker") > 0 Then
                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()

                    Dim s_SQL As String = "SELECT Datum, Verloftypes.Omschrijving, Opmerking FROM Verlofdagen LEFT OUTER JOIN Verloftypes ON Verlofdagen.VerlofCode=Verloftypes.id WHERE (Gebruiker=@gebruiker) " _
                         & "AND (BeslistDoor=@userid) AND (IsNull(Goedgekeurd,0)<>1) AND (IsNull(Afgekeurd,0)<>1)"
                    Dim cmd As New SqlCommand With {.Connection = cn, .CommandText = "prVerlofinfo", .CommandType = CommandType.StoredProcedure}
                    Dim par As New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Session("verlofgebruiker")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@verantwoordelijke", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)


                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    dicVerlof = New Dictionary(Of Date, VerlofInfo)
                    If dr.HasRows Then
                        Dim varAnderen As String = ""
                        Dim varDatum As Date = DateSerial(2000, 1, 1)
                        Dim varO As String = ""
                        Dim iSoort As VerlofInfo = Nothing

                        Do While dr.Read

                            If dr.GetValue(0) <> varDatum Then
                                If varDatum.Year <> 2000 Then
                                    iSoort = New VerlofInfo(typeVerlof.Aangevraagd, varO, varAnderen)
                                    dicVerlof.Add(varDatum, iSoort)
                                    varAnderen = ""
                                    varO = ""
                                End If



                            End If
                            If Not dr.IsDBNull(3) Then
                                If varAnderen = "" Then
                                    varAnderen = String.Format("{0}Andere werknemers die deze dag ook verlof hebben:{1}{2}", vbCrLf, vbCr, dr.GetString(3))
                                Else
                                    varAnderen = String.Format("{0}{1}{2}", varAnderen, vbNewLine, dr.GetString(3))
                                End If

                            End If

                            If Not dr.IsDBNull(1) Then
                                varO = dr.GetString(1)
                            End If
                            If Not dr.IsDBNull(2) Then
                                If varO = "" Then
                                    varO = "opm: " & dr.GetString(2)
                                Else
                                    varO = String.Format("{0} - opm: {1}", varO, dr.GetString(2))
                                End If
                            End If
                            varDatum = dr.GetValue(0)
                        Loop
                        iSoort = New VerlofInfo(typeVerlof.Aangevraagd, varO, varAnderen)
                        dicVerlof.Add(varDatum, iSoort)

                    End If
                    dr.Close()

                    ASPxComboBoxGebruiker.Value = Session("verlofgebruiker")
                End Using
            End If



    End Sub

    Private Sub ASPxCalendar1_DayCellPrepared(sender As Object, e As DevExpress.Web.CalendarDayCellPreparedEventArgs) Handles ASPxCalendar1.DayCellPrepared

            If dicVerlof IsNot Nothing Then
            If dicVerlof.ContainsKey(e.Date) Then
                Dim iSoort As VerlofInfo = dicVerlof.Item(e.Date)

                Select Case iSoort.SoortVerlof
                    Case typeVerlof.Aangevraagd
                        e.Cell.BackColor = System.Drawing.Color.Yellow
                    Case typeVerlof.Afgekeurd
                        e.Cell.BackColor = System.Drawing.Color.Red
                    Case typeVerlof.Goedgekeurd
                        e.Cell.BackColor = System.Drawing.Color.LightGreen
                    Case typeVerlof.Voorbij
                        e.Cell.BackColor = System.Drawing.Color.LightBlue
                End Select
                e.Cell.Wrap = True
                Dim varTip As String = ""

                If Not iSoort.Omschrijving = "" Then
                    varTip = iSoort.Omschrijving
                End If
                If Not iSoort.Ookverlof = "" Then
                    varTip = varTip & iSoort.Ookverlof
                End If
                If Not varTip = "" Then
                    e.Cell.ToolTip = varTip

                End If

            End If
            End If



    End Sub

    Private Sub ASPxComboBoxGebruiker_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxGebruiker.ValueChanged
        If Not ASPxComboBoxGebruiker.SelectedIndex = -1 Then
            If ASPxComboBoxGebruiker.Value <> Session("verlofgebruiker") Then
                Session("verlofgebruiker") = ASPxComboBoxGebruiker.Value
                Response.Redirect("Verlofaanvragenverwerken.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If

        End If
    End Sub

    Protected Sub ASPxButtonGoedkeuren_Click(sender As Object, e As EventArgs) Handles ASPxButtonGoedkeuren.Click
        If ASPxCalendar1.SelectedDates.Count > 0 Then
            Dim ONnr As String
            Dim WN As Integer
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                For Each datum As Date In ASPxCalendar1.SelectedDates
                    Try
                        Dim s_SQL As String = "UPDATE Verlofdagen SET Goedgekeurd=1, Afgekeurd=0, BeslistOp=GetDate() WHERE (Gebruiker=@gebruiker) AND (Datum=@datum) " _
                        & "AND (BeslistDoor=@userid)"
                        Dim cmd As New SqlCommand(s_SQL, cn)
                        Dim par As New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = ASPxComboBoxGebruiker.Value}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@datum", SqlDbType.Date) With {.Value = datum}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()

                        ''Start insert daguren
                        ''on en wn ophalen
                        Dim s_SQL2 As String = "SELECT Werkg, Werkn FROM Gebruikers WHERE (id=@id)"
                        Dim cmd2 As New SqlCommand(s_SQL2, cn)

                        Dim par2 As SqlParameter
                        par2 = New SqlParameter("@id", SqlDbType.Int, -1) With {.Value = ASPxComboBoxGebruiker.Value}
                        cmd2.Parameters.Add(par2)
                        Dim dr2 As SqlDataReader = cmd2.ExecuteReader
                        If dr2.HasRows Then
                            dr2.Read()

                            ONnr = dr2.GetString(0)
                            WN = dr2.GetInt32(1)
                        End If
                        dr2.Close()


                        'controleren of al bestaat
                        's_SQL = "SELECT id FROM DagUren WHERE ([ON]=@ON) and (NR = @NR)"
                        'cmd = New SqlCommand(s_SQL, cn)
                        'par = New SqlParameter("@ON", SqlDbType.NVarChar, 25) With {.Value = ONnr}
                        'cmd.Parameters.Add(par)
                        'par = New SqlParameter("@NR", SqlDbType.Int, -1) With {.Value = WN}
                        'cmd.Parameters.Add(par)
                        'Dim dr As SqlDataReader = cmd.ExecuteReader
                        'Dim blbestaat As Boolean = dr.HasRows
                        'dr.Close()
                        'If blbestaat Then
                        '    'Medling al bestaat? Mag eigenlijk niet tegengehouden worden
                        'End If

                        s_SQL = "INSERT INTO [Qformz].[dbo].DagUren ([ON], [NR], [uren],datum, bron, kilometers) VALUES (@on,@nr,@uren,@datum,'Voo', @kilometers)"
                        cmd = New SqlCommand(s_SQL, cn)
                        par = New SqlParameter("@on", SqlDbType.NVarChar, 100) With {.Value = ONnr}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@nr", SqlDbType.Int, -1) With {.Value = WN}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@uren", SqlDbType.Decimal) With {.Value = 0.00}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@datum", SqlDbType.DateTime) With {.Value = datum}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@kilometers", SqlDbType.Int, -1) With {.Value = 0}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                        ''Eind insert daguren
                        Dim varType As String = ""
                        Dim iVMNM As Integer = 0
                        cmd = New SqlCommand("SELECT T.Omschrijving, VMNM FROM Verlofdagen D INNER JOIN Verloftypes T ON D.VerlofCode=T.id WHERE (D.Gebruiker=@gebruiker) AND (D.Datum=@datum) AND (D.BeslistDoor=@userid)", cn)
                        par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = ASPxComboBoxGebruiker.Value}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@datum", SqlDbType.Date) With {.Value = datum}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        Dim dr3 As SqlDataReader = cmd.ExecuteReader
                        If dr3.HasRows Then
                            dr3.Read()
                            If Not dr3.IsDBNull(0) Then
                                varType = dr3.GetString(0)
                            End If
                            If Not dr3.IsDBNull(1) Then
                                iVMNM = dr3.GetInt32(1)
                            End If
                        End If
                        dr3.Close()
                        s_SQL = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                        cmd = New SqlCommand(s_SQL, cn)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Verlof goedgekeurd door {0} voor datum {1} voor gebruiker {2}", Session("userid"), datum.ToShortDateString, ASPxComboBoxGebruiker.Text)}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                        If iVMNM = 0 Then
                            cmd = New SqlCommand("UPDATE Planning set Node=@node WHERE (Gebruiker=@gebruiker) AND (Dagdeel=@dagdeel) AND (Datum=@datum)", cn)
                            par = New SqlParameter("@node", SqlDbType.NVarChar, 50) With {.Value = "Afwezig"}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = ASPxComboBoxGebruiker.Value}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@dagdeel", SqlDbType.Int) With {.Value = 1}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@datum", SqlDbType.Date) With {.Value = datum}
                            cmd.Parameters.Add(par)
                            cmd.ExecuteNonQuery()
                            s_SQL = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                            cmd = New SqlCommand(s_SQL, cn)
                            par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Appointments verwijderd vanwege goedkeuring verlof door {0} {1} voor datum {2} voor gebruiker {3}", Session("naam"), Session("voornaam"), datum.ToShortDateString, ASPxComboBoxGebruiker.Text)}
                            cmd.Parameters.Add(par)
                            cmd.ExecuteNonQuery()
                            cmd = New SqlCommand("UPDATE Planning set Node=@node WHERE (Gebruiker=@gebruiker) AND (Dagdeel=@dagdeel) AND (Datum=@datum)", cn)
                            par = New SqlParameter("@node", SqlDbType.NVarChar, 50) With {.Value = "Afwezig"}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = ASPxComboBoxGebruiker.Value}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@dagdeel", SqlDbType.Int) With {.Value = 2}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@datum", SqlDbType.Date) With {.Value = datum}
                            cmd.Parameters.Add(par)
                            cmd.ExecuteNonQuery()
                            cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                            par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Planning toegevoegd voor goedgekeurde afwezigheid {0} technieker {1} start {2} einde {3} door {4} {5}", varType, ASPxComboBoxGebruiker.Text, DateAdd(DateInterval.Hour, 8, datum), DateAdd(DateInterval.Hour, 17, datum), Session("naam"), Session("voornaam"))}
                            cmd.Parameters.Add(par)
                            cmd.ExecuteNonQuery()
                        Else
                            If iVMNM = 1 Then
                                cmd = New SqlCommand("UPDATE Planning set Node=@node WHERE (Gebruiker=@gebruiker) AND (Dagdeel=@dagdeel) AND (Datum=@datum)", cn)
                                par = New SqlParameter("@node", SqlDbType.NVarChar, 50) With {.Value = "Afwezig"}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = ASPxComboBoxGebruiker.Value}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@dagdeel", SqlDbType.Int) With {.Value = 1}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@datum", SqlDbType.Date) With {.Value = datum}
                                cmd.Parameters.Add(par)
                                cmd.ExecuteNonQuery()
                                s_SQL = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                                cmd = New SqlCommand(s_SQL, cn)
                                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Appointments verwijderd vanwege goedkeuring verlof door {0} {1} voor datum {2} voor gebruiker {3}", Session("naam"), Session("voornaam"), datum.ToShortDateString, ASPxComboBoxGebruiker.Text)}
                                cmd.Parameters.Add(par)
                                cmd.ExecuteNonQuery()
                            Else
                                cmd = New SqlCommand("UPDATE Planning set Node=@node WHERE (Gebruiker=@gebruiker) AND (Dagdeel=@dagdeel) AND (Datum=@datum)", cn)
                                par = New SqlParameter("@node", SqlDbType.NVarChar, 50) With {.Value = "Afwezig"}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = ASPxComboBoxGebruiker.Value}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@dagdeel", SqlDbType.Int) With {.Value = 2}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@datum", SqlDbType.Date) With {.Value = datum}
                                cmd.Parameters.Add(par)
                                cmd.ExecuteNonQuery()
                                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                                cmd.Parameters.Add(par)
                                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Planning toegevoegd voor goedgekeurde afwezigheid {0} technieker {1} start {2} einde {3} door {4} {5}", varType, ASPxComboBoxGebruiker.Text, DateAdd(DateInterval.Hour, 8, datum), DateAdd(DateInterval.Hour, 17, datum), Session("naam"), Session("voornaam"))}
                                cmd.Parameters.Add(par)
                                cmd.ExecuteNonQuery()
                            End If
                        End If
                    Catch ex As Exception
                        Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij verlofgoedkeuring door {0} voor datum {1} voor gebruiker {2} : {3}", Session("userid"), datum.ToShortDateString, ASPxComboBoxGebruiker.Text, ex.Message)}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    End Try
                Next
            End Using
            Verwittigen(ASPxComboBoxGebruiker.Text, ASPxComboBoxGebruiker.Value)
            Response.Redirect("Verlofaanvragenverwerken.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub

    Protected Sub ASPxButtonWeigeren_Click(sender As Object, e As EventArgs) Handles ASPxButtonWeigeren.Click
        If ASPxCalendar1.SelectedDates.Count > 0 Then
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                For Each datum As Date In ASPxCalendar1.SelectedDates
                    Try
                        Dim s_SQL As String = "UPDATE Verlofdagen SET Afgekeurd=1, Goedgekeurd=0, BeslistOp=GetDate() WHERE (Gebruiker=@gebruiker) AND (Datum=@datum) " _
                        & "AND (BeslistDoor=@userid)"
                        Dim cmd As New SqlCommand(s_SQL, cn)
                        Dim par As New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = ASPxComboBoxGebruiker.Value}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@datum", SqlDbType.Date) With {.Value = datum}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()

                        s_SQL = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                        cmd = New SqlCommand(s_SQL, cn)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Verlof afgekeurd door {0} voor datum {1} voor gebruiker {2}", Session("userid"), datum.ToShortDateString, ASPxComboBoxGebruiker.Text)}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()

                    Catch ex As Exception
                        Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij verlofafkeuring door {0} voor datum {1} voor gebruiker {2} : {3}", Session("userid"), datum.ToShortDateString, ASPxComboBoxGebruiker.Text, ex.Message)}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    End Try
                Next
            End Using
            Verwittigen(ASPxComboBoxGebruiker.Text, ASPxComboBoxGebruiker.Value)
            Response.Redirect("Verlofaanvragenverwerken.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub

    Private Sub Verwittigen(ByVal varGebruiker As String, ByVal Gebruikerid As Integer)

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim varBericht As String = ""
            Dim varOnderwerp As String = ""
            Dim varBeheerder As String = String.Format("{0} {1}", Session("naam"), Session("voornaam"))
            Dim varEmail As String = ""
            Dim s_SQL As String = "SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Melding verlofaanvraag verwerkt naar gebruiker"}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not dr.IsDBNull(0) Then
                    varBericht = dr.GetString(0)
                    varOnderwerp = dr.GetString(1)
                End If
            End If
            dr.Close()
            s_SQL = "SELECT Email FROM Gebruikers WHERE (Gebruikers.id=@gebruiker)"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Gebruikerid}
            cmd.Parameters.Add(par)
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                varEmail = dr.GetString(0)
            End If
            dr.Close()
            Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2010_SP2)
            exch.Url = New Uri("https://mail.apkintern.be/EWS/Exchange.asmx")
            exch.UseDefaultCredentials = False
            exch.Credentials = New System.Net.NetworkCredential("Administrator", "E01ab107%", "apk")
            TrustAllCertificatePolicy.OverrideCertificateValidation()
            exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.PrincipalName, Session("mailaccount"))
            Dim message As New EmailMessage(exch)
            message.Subject = varOnderwerp
            Dim varBody As String = varBericht.Replace("[WERKNEMER]", varGebruiker)
            varBody = varBody.Replace("[BEHEERDER]", varBeheerder)
            varBody = varBody.Replace("[URL]", String.Format("{0}/Verlofaanvraag.aspx?g=1", Session("domein")))
            varBody = varBody.Replace("[SITETITEL]", Session("sitetitel"))
            Dim sBody As String = "<x-html>" & vbCrLf
            sBody = sBody & varBody
            sBody = sBody & "</x-html>"
            message.Body = sBody
            message.ToRecipients.Add(varEmail)
            message.Send()
        End Using
    End Sub

    Class VerlofInfo

        Private _omschrijving As String
        Private _soortverlof As typeVerlof
        Private _ookverlof As String

        Public Sub New(ByVal varT As typeVerlof, ByVal varOmschrijving As String, ByVal varOokverlof As String)
            SoortVerlof = varT
            Omschrijving = varOmschrijving
            Ookverlof = varOokverlof
        End Sub

        Public Property SoortVerlof() As typeVerlof
            Get
                Return _soortverlof
            End Get
            Set(ByVal value As typeVerlof)
                _soortverlof = value
            End Set
        End Property
        Public Property Omschrijving() As String
            Get
                Return _omschrijving
            End Get
            Set(ByVal value As String)
                _omschrijving = value
            End Set
        End Property
        Public Property Ookverlof() As String
            Get
                Return _ookverlof
            End Get
            Set(ByVal value As String)
                _ookverlof = value
            End Set
        End Property
    End Class

    Protected Sub ASPxButtonRapport_Click(sender As Object, e As EventArgs) Handles ASPxButtonRapport.Click
        Response.Redirect("~/rapporten/Afdruk_Verlofaanvragen.aspx", False)
        Context.ApplicationInstance.CompleteRequest()
    End Sub
End Class