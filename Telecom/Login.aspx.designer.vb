﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Login

    '''<summary>
    '''ASPxFormLayout1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxFormLayout1 As Global.DevExpress.Web.ASPxFormLayout

    '''<summary>
    '''txtGebruiker control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtGebruiker As Global.DevExpress.Web.ASPxTextBox

    '''<summary>
    '''txtPaswoord control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPaswoord As Global.DevExpress.Web.ASPxTextBox

    '''<summary>
    '''btnLogin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnLogin As Global.DevExpress.Web.ASPxButton

    '''<summary>
    '''ASPxFormLayout1_E1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxFormLayout1_E1 As Global.DevExpress.Web.ASPxLabel

    '''<summary>
    '''txtNieuwwachtwoord1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNieuwwachtwoord1 As Global.DevExpress.Web.ASPxTextBox

    '''<summary>
    '''txtNieuwwachtwoord2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNieuwwachtwoord2 As Global.DevExpress.Web.ASPxTextBox

    '''<summary>
    '''btnBevestigen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBevestigen As Global.DevExpress.Web.ASPxButton
End Class
