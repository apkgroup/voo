﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="BestellingBobijnPage.aspx.vb" Inherits="Telecom.BestellingBobijnPage" EnableViewState="false"%>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v19.2, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>
        var timeout;


        function gridViewAssociations_ToggleSelection(s, e) {
            try {
                console.log(ASPxClientCheckBox.Cast(s).GetChecked());
                setTimeout(function () {
                    ASPxClientGridView.Cast('ASPxGridView1').SelectAllRowsOnPage(ASPxClientCheckBox.Cast(s).GetChecked());
                }, 0);
            } catch (ex) {
                alert('gridViewAssociations_ToggleSelection: ' + ex);
            }
        }

    </script>

    <script type="text/javascript">

        var timerHandle = -1;
        function OnBatchEditStartEditing(s, e) {
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("hoeveelheid");
            console.log(templateColumn);

        }

        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);
        }

        function saveChangesBtn_Click(s, e) {

            if (ASPxGridView1.batchEditApi.HasChanges())
                ASPxGridView1.UpdateEdit();
            else {
                buttonFlag = true;
                ASPxGridView1.PerformCallback("update");
            }
        }

        function saveChangesBtn_Click(s, e) {

            if (ASPxGridView1.batchEditApi.HasChanges())
                ASPxGridView1.UpdateEdit("reserve");
            else {
                buttonFlag = true;
                ASPxGridView1.PerformCallback("update");
            }


        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();

        }

        function AddTxt(bobType, target) {
            console.log(target);
            callbackPanelbob.PerformCallback(bobType);
        }



    </script>
    <style type="text/css">
        .auto-style2 {
            height: 37px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal></h2>

    <h2>Picken uit magazijn:</h2>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) and hoofd = 1">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
    Opmerking besteller:<br />
    <dx:ASPxMemo ID="ASPxMemoOpmerking" runat="server" Height="71px" ReadOnly="True" Width="278px">
    </dx:ASPxMemo>
    <br />
    Deadline:<br />
    <dx:ASPxDateEdit ID="ASPxDateEditDeadline" runat="server" ReadOnly="True">
    </dx:ASPxDateEdit>

    <br />
    <br />

    <dx:ASPxButton ID="ASPxButton3" runat="server" AutoPostBack="False"
        Text="Exporter">
        <ClientSideEvents Click="function(s, e) {
            ASPxGridView1.PerformCallback('CEXP');
	
}" />
    </dx:ASPxButton>
    <p>
        <asp:Label ID="Labelexport" runat="server" Text="Label"></asp:Label>

    </p>
    <dx:ASPxLabel ID="ASPxLabelGoed" runat="server" Text="ASPxLabel" Font-Bold="True" ForeColor="#006600">
    </dx:ASPxLabel>

    <br />



    <dx:ASPxGridView ID="ASPxGridView1" runat="server" DataSourceID="SqlDataSource1" ClientInstanceName="ASPxGridView1" AutoGenerateColumns="False" KeyFieldName="id" OnCustomCallback="ASPxGridView1_CustomCallback" Width="100%" Caption="Bestelling" EnableCallback="True">
        <ClientSideEvents EndCallback=" function(s, e) { 
                        console.log('callbak');    
                        isAangepast= true;
                if (s.cpExport) {
                        
                       console.log('exportYes');
                    delete s.cpExport;
                    window.open('Export.aspx', '_blank');
                }
            } 
"
            BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing" />

        <SettingsPager PageSize="100">
        </SettingsPager>

        <SettingsEditing Mode="Batch">
            <BatchEditSettings StartEditAction="Click" />
        </SettingsEditing>
        <Settings ShowStatusBar="Hidden" ShowTitlePanel="true" ShowHeaderFilterButton="True" />
        <SettingsDataSecurity AllowInsert="False" />
        <Templates>
            <TitlePanel>
            </TitlePanel>
        </Templates>
        <Columns>
            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0" Width="10px">
                <HeaderTemplate>
                    <dx:ASPxCheckBox ID="checkBoxSelectUnselectAll" runat="server" ToolTip="Marcar/Desmarcar Todos">
                        <ClientSideEvents CheckedChanged="gridViewAssociations_ToggleSelection" />
                    </dx:ASPxCheckBox>
                </HeaderTemplate>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" VisibleIndex="1" ReadOnly="True" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="bestellingId" VisibleIndex="2" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="4" ExportWidth="110" Width="200px">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="5" ExportWidth="385" Width="200px">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="hoeveelheid" VisibleIndex="7" Caption="Nombre" ExportWidth="100" Width="50px">
                <PropertiesTextEdit>
                </PropertiesTextEdit>
                <EditFormSettings Visible="True" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Materiaalgroep" VisibleIndex="3" Width="100px">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Eenheid" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>

    <br />
    <dx:ASPxCallbackPanel ID="ASPxCallbackPanelbob" ClientInstanceName="callbackPanelbob" runat="server" Width="200px">
        <PanelCollection>
            <dx:PanelContent runat="server">
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>



    <br />

    <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="400px">
        <PanelCollection>
            <dx:PanelContent runat="server" SupportsDisabledAttribute="True">

                <dx:ASPxLabel ID="LabelFoutmelding" CssClass="enableMultiLine" EncodeHtml="false" runat="server" ClientInstanceName="LabelFoutmelding" Font-Bold="True" ForeColor="#CC0000" Text="Foutmelding">
                </dx:ASPxLabel>
                <br />
                <asp:PlaceHolder ID="PlaceHolderHaspel" runat="server"></asp:PlaceHolder>
                <br />
                <br />

                <br />
                <dx:ASPxLabel ID="ASPxLabelDoel" runat="server" Text="Bestelling voor magazijn:">
                </dx:ASPxLabel>
                <dx:ASPxComboBox ID="ASPxComboBoxMagazijnDoel" runat="server" AutoPostBack="True" DataSourceID="SqlDataSourceMagazijnenActief" TextField="Naam" ValueField="id">
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSourceMagazijnenActief" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) and (AF = 0 or AF is null) and (hoofd =0 or hoofd is null)">
                    <SelectParameters>
                        <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Approuver" ClientInstanceName="saveChangesBtn" Width="75px" CssClass="inline" Height="27px">
                    <ClientSideEvents Click="saveChangesBtn_Click" />
                </dx:ASPxButton>

                <dx:ASPxButton ID="ASPxButton7" runat="server" ClientInstanceName="saveChangesBtn" CssClass="inline" Height="27px" Text="Picken en reserveren" Width="75px">
                    <ClientSideEvents Click="reserveChangesBtn_Click" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Réjeter" AutoPostBack="false" ClientInstanceName="cancelorderBtn" Width="75px" CssClass="inline">
                </dx:ASPxButton>
                <br />
                <br />
                <br />
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [BestellijnBobijn] WHERE [BestellijnBobijn].[id] = @id" SelectCommand="SELECT [BestellijnBobijn].[id],[BestellijnBobijn].[bobijnbestellingId], [Article], [Description], [hoeveelheid], Eenheid,
(Select omschrijving from voo.dbo.MateriaalGroep mg where mg.id =[Basismateriaal].[MateriaalGroep]) as 'Materiaalgroep' FROM [BestellijnBobijn] 
INNER JOIN [Basismateriaal] on [BestellijnBobijn].[bobijnmateriaalId] = [Basismateriaal].[id]
INNER JOIN [MateriaalOpdrachtgevers] b on [Basismateriaal].id = b.materiaalId
 WHERE ([bobijnbestellingId] = @bestellingId) and b.opdrachtgeverId=@opdrachtgever
order by b.volgorde desc, [Description] asc
"
        UpdateCommand="UPDATE [BestellijnBobijn] SET [hoeveelheid] = @hoeveelheid WHERE ([BestellijnBobijn].[id] = @id)">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="bestellingId" QueryStringField="id" />
            <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="hoeveelheid" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <table>
        <tr>
            <td class="auto-style2">
                <dx:ASPxTextBox ID="ASPxTextBoxReden" runat="server" Width="370px">
                </dx:ASPxTextBox>
            </td>
            <td class="auto-style2">
                <dx:ASPxButton ID="ASPxButton6" runat="server" Text="Reden aanpassing/afkeuring verzenden naar plaatser">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>

    <dx:ASPxPanel ID="ASPxPanel2" runat="server" Width="200px">

        <PanelCollection>
            <dx:PanelContent runat="server">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="ASPxButtonVorige" runat="server" Text="Vorige">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="ASPxButtonVolgende" runat="server" Text="Volgende">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>

    </dx:ASPxPanel>






    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1" ReportHeader="Bestelling" PaperKind="A4" LeftMargin="1" MaxColumnWidth="200">
        <Styles>
            <Header Font-Names="Arial">
            </Header>
            <Cell Font-Names="Arial">
            </Cell>
            <Title Font-Names="Arial" ForeColor="DimGray"></Title>
        </Styles>
        <PageHeader Center="Bestelling">
        </PageHeader>
    </dx:ASPxGridViewExporter>






    <br />


    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server" GridViewID="ASPxGridViewBobijn" ReportHeader="Bobijnen" PaperKind="A4" DetailHorizontalOffset="0" LeftMargin="1">
        <Styles>
            <Header Font-Names="Arial">
            </Header>
            <Cell Font-Names="arial">
            </Cell>
            <Title Font-Names="Arial" ForeColor="DimGray"></Title>
        </Styles>
        <PageHeader Center="Bobijn">
        </PageHeader>
    </dx:ASPxGridViewExporter>






                <dx:ASPxGridView ID="ASPxGridViewBobijn" KeyFieldName="id" runat="server" DataSourceID="SqlDataSourceBobijnen" ClientInstanceName="ASPxGridViewBobijn" AutoGenerateColumns="False" Width="100%" Caption="Bobijnen">
                    <ClientSideEvents EndCallback=" function(s, e) { 
                        console.log('callbak');
                if (s.cpExport) {
                       console.log('exportYes');
                    delete s.cpExport;
                    window.open('Export.aspx', '_blank');
                }
            } 
"  BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing"/>
                    
                    <SettingsPager PageSize="100">
                    </SettingsPager>
                    
                    <SettingsEditing Mode="Batch">
                        <BatchEditSettings StartEditAction="Click" />
                    </SettingsEditing>
                    <Settings ShowStatusBar="Hidden" ShowTitlePanel="true" />
                    <SettingsDataSecurity AllowInsert="False" />
                      <Templates>
        <TitlePanel>
        
        </TitlePanel>
    </Templates>
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="bobijnnummer" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="aantal" Visible="False" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="stockmagazijnId" Visible="False" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="AF" Visible="False" VisibleIndex="4">
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="paklijstnummer" VisibleIndex="5">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="plaatsingsstaatnr" VisibleIndex="6">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="plaatsingsstaatgemaakt" Visible="False" VisibleIndex="7">
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="voorzieneLocatie" Visible="False" VisibleIndex="8">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="JMSNummer" VisibleIndex="9">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn FieldName="DatumIngelezen" VisibleIndex="10">
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn FieldName="DatumUitgeboekt" Visible="False" VisibleIndex="11">
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn FieldName="soortBobijn" Visible="False" VisibleIndex="12">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>

    




    <br />








    <asp:SqlDataSource ID="SqlDataSourceBobijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="select * from bobijn where id in (SELECT [bobijnId] FROM [BobijnPerBestellijn] WHERE [bestellijnBobijnId] in (select id from [BestellijnBobijn] where bobijnbestellingId = @bestellingid))">
        <SelectParameters>
            <asp:QueryStringParameter Name="bestellingid" QueryStringField="id" />
        </SelectParameters>
    </asp:SqlDataSource>






</asp:Content>
