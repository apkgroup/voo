﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Bestelling.aspx.vb" Inherits="Telecom.Bestelling" ViewStateMode="Enabled" %>

<%@ Register assembly="DevExpress.Web.ASPxRichEdit.v19.2, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRichEdit" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>
        var timeout;


        function gridViewAssociations_ToggleSelection(s, e) {
            try {
                console.log(ASPxClientCheckBox.Cast(s).GetChecked());
                setTimeout(function () {
                    ASPxClientGridView.Cast('ASPxGridView1').SelectAllRowsOnPage(ASPxClientCheckBox.Cast(s).GetChecked());
                }, 0);
            } catch (ex) {
                alert('gridViewAssociations_ToggleSelection: ' + ex);
            }
        }

    </script>

    <script type="text/javascript">
        var  serienrs;

            var isAangepast = false;
         
        window.onload = function () {
            
            isAangepast = '<%=Session("isAangepast") %>';
            console.log(isAangepast);
            if (isAangepast!= false && isAangepast!="False") {
                console.log(isAangepast);
         
                 '<%Session("isAangepast") = False %>';
            }
            $('input').keypress(function (e) {
                console.log("KEYPRESS");
                if (e.which == 13) {
                    e.preventDefault();
                    console.log("ISENTER");
                    var self = $(this)
                    var form = self.parents('form:eq(0)');
                    var focusable;
                    var next;
                    var prev;

                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {

                    }
                    return false;

                }
            });
        };

        var timerHandle = -1;
        function OnBatchEditStartEditing(s, e) {
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("hoeveelheid");
            console.log(templateColumn);




        }


        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);
        }

        function saveChangesBtn_Click(s, e) {

            if (ASPxGridView1.batchEditApi.HasChanges())
                ASPxGridView1.UpdateEdit();
            else {
                buttonFlag = true;
                ASPxGridView1.PerformCallback("update");
            }


        }

                function saveChangesBtn_Click(s, e) {

            if (ASPxGridView1.batchEditApi.HasChanges())
                ASPxGridView1.UpdateEdit("reserve");
            else {
                buttonFlag = true;
                ASPxGridView1.PerformCallback("update");
            }


        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();

        }


    </script>
    <style type="text/css">
        .auto-style2 {
            height: 37px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal></h2>

    <h2>Picken uit magazijn:</h2>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) and hoofd = 1">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
    Opmerking besteller:<br />
    <dx:ASPxMemo ID="ASPxMemoOpmerking" runat="server" Height="71px" ReadOnly="True" Width="278px">
    </dx:ASPxMemo>
    <br />
    Deadline:<br />
    <dx:ASPxDateEdit ID="ASPxDateEditDeadline" runat="server" ReadOnly="True">
    </dx:ASPxDateEdit>

    <br />
    <br />

    <dx:ASPxButton ID="ASPxButton3" runat="server" AutoPostBack="False"
        Text="Exporter">
        <ClientSideEvents Click="function(s, e) {
            ASPxGridView1.PerformCallback('CEXP');
	
}" />
    </dx:ASPxButton>
    <p>
        <asp:Label ID="Labelexport" runat="server" Text="Label"></asp:Label>
        
    </p>
    <dx:ASPxLabel ID="ASPxLabelGoed" runat="server" Text="ASPxLabel" Font-Bold="True" ForeColor="#006600">
        </dx:ASPxLabel>

                <br />



                <dx:ASPxGridView ID="ASPxGridView1" runat="server" DataSourceID="SqlDataSource1" ClientInstanceName="ASPxGridView1" AutoGenerateColumns="False" KeyFieldName="id" oncustomcallback="ASPxGridView1_CustomCallback" Width="100%" Caption="Bestelling" EnableCallback="True">
                    <ClientSideEvents EndCallback=" function(s, e) { 
                        console.log('callbak');    
                        isAangepast= true;
                if (s.cpExport) {
                        
                       console.log('exportYes');
                    delete s.cpExport;
                    window.open('Export.aspx', '_blank');
                }
            } 
"  BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing"/>
                    
                    <SettingsPager PageSize="100">
                    </SettingsPager>
                    
                    <SettingsEditing Mode="Batch">
                        <BatchEditSettings StartEditAction="Click" />
                    </SettingsEditing>
                    <Settings ShowStatusBar="Hidden" ShowTitlePanel="true" ShowHeaderFilterButton="True" />
                    <SettingsDataSecurity AllowInsert="False" />
                      <Templates>
        <TitlePanel>
        
        </TitlePanel>
    </Templates>
                    <Columns>
                        <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0" Width="10px">
                                <HeaderTemplate>
                    <dx:ASPxCheckBox ID="checkBoxSelectUnselectAll" runat="server" ToolTip="Marcar/Desmarcar Todos">
                        <ClientSideEvents   CheckedChanged="gridViewAssociations_ToggleSelection" />
                    </dx:ASPxCheckBox>
                </HeaderTemplate>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="id" VisibleIndex="1" ReadOnly="True" Visible="False">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="bestellingId" VisibleIndex="2" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="4" ExportWidth="110" >
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="5" ExportWidth="385" >
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="hoeveelheid" VisibleIndex="7" Caption="Nombre" ExportWidth="100" Width="50px">
                            <PropertiesTextEdit>           
</PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Materiaalgroep" VisibleIndex="3" >
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Eenheid" VisibleIndex="6" ExportWidth="50">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Locatie" FieldName="locatie" VisibleIndex="8">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>

    <br />
                    <dx:ASPxGridView ID="ASPxGridView3" runat="server" DataSourceID="SqlDataSourceGrabbel" ClientInstanceName="ASPxGridView3" AutoGenerateColumns="False" KeyFieldName="id" oncustomcallback="ASPxGridView1_CustomCallback" Width="100%" Caption="Grabbelmateriaal" EnableCallback="False" EnableCallBacks="False">
                    <ClientSideEvents EndCallback=" function(s, e) { 
                        console.log('callbak1');

                if (s.cpExport) {
                       console.log('exportYes');
                    delete s.cpExport;
                    window.open('Export.aspx', '_blank');
                }
            } 
"  BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing"/>
                    
                    <SettingsPager PageSize="100">
                    </SettingsPager>
                    
                    <SettingsEditing Mode="Batch">
                        <BatchEditSettings StartEditAction="Click" />
                    </SettingsEditing>
                    <Settings ShowStatusBar="Hidden" ShowTitlePanel="true" />
                    <SettingsDataSecurity AllowInsert="False" />
                      <Templates>
        <TitlePanel>
        
        </TitlePanel>
    </Templates>
                    <Columns>
                        <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0" Width="30px">
                                <HeaderTemplate>
                    <dx:ASPxCheckBox ID="checkBoxSelectUnselectAll0" runat="server" ToolTip="Marcar/Desmarcar Todos">
                        <ClientSideEvents   CheckedChanged="gridViewAssociations_ToggleSelection" />
                    </dx:ASPxCheckBox>
                </HeaderTemplate>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="id" VisibleIndex="1" ReadOnly="True" Visible="False">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="bestellingId" VisibleIndex="2" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="3" ExportWidth="110" Caption="Artikel" Width="200px">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Omschrijving" VisibleIndex="4" ExportWidth="385" Width="200px">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="hoeveelheid" VisibleIndex="5" Caption="Aantal" ExportWidth="100" Width="50px">
                            <PropertiesTextEdit>           
</PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
<dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="200px">
    <PanelCollection>
        <dx:PanelContent runat="server">
             <dx:ASPxButton ID="ASPxButton4" runat="server" Text="Doos inlezen" AutoPostBack="False">
        <ClientSideEvents Click="function(s, e) {
        popupcontrolbox.Show();
    }" />
    </dx:ASPxButton>
             &nbsp;<dx:ASPxButton ID="ASPxButtonPallet" runat="server" AutoPostBack="False" Text="Pallet inlezen">
                 <ClientSideEvents Click="function(s, e) {
        popupcontrolpallet.Show();
    }" />
             </dx:ASPxButton>
        </dx:PanelContent>
    </PanelCollection>
</dx:ASPxCallbackPanel>
   
    
    <dx:ASPxPopupControl ID="popupcontrolbox" runat="server" ClientInstanceName="popupcontrolbox" HeaderText="Geef een Boxnr. in" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        
        <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <dx:ASPxTextBox ID="ASPxTextBoxBox" runat="server" Width="170px">
    </dx:ASPxTextBox>
    <dx:ASPxButton ID="ASPxButton5" runat="server" Text="Inlezen">
    </dx:ASPxButton>
    <br />
            </dx:PopupControlContentControl>
</ContentCollection>
        
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="ASPxPopupControlPallet" runat="server" ClientInstanceName="popupcontrolpallet" HeaderText="Geef een Palletnr. in" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        
        <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <dx:ASPxTextBox ID="ASPxTextBoxPallet" runat="server" Width="170px">
    </dx:ASPxTextBox>
    <dx:ASPxButton ID="ASPxButton8" runat="server" Text="Inlezen">
    </dx:ASPxButton>
    <br />
            </dx:PopupControlContentControl>
</ContentCollection>
        
    </dx:ASPxPopupControl>
    <br />
                <asp:PlaceHolder runat="server" ID="PlaceHolderHaspel"></asp:PlaceHolder>

    <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="400px">
        <PanelCollection>
            <dx:PanelContent runat="server" SupportsDisabledAttribute="True">

                <dx:ASPxPanel ID="ASPxPanelSerie" runat="server" Width="200px">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
                <dx:ASPxLabel ID="LabelFoutmelding" cssClass="enableMultiLine" EncodeHtml="false" runat="server" ClientInstanceName="LabelFoutmelding" Font-Bold="True" ForeColor="#CC0000" Text="Foutmelding">
                </dx:ASPxLabel>
                <br />
                <asp:PlaceHolder ID="PlaceHolderSerie" runat="server"></asp:PlaceHolder>
                <br />
                <dx:ASPxLabel ID="ASPxLabelDoel" runat="server" Text="Bestelling voor magazijn:">
                </dx:ASPxLabel>
                <br />
                <dx:ASPxComboBox ID="ASPxComboBoxMagazijnDoel" runat="server" AutoPostBack="True" DataSourceID="SqlDataSourceMagazijnenActief" TextField="Naam" ValueField="id">
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSourceMagazijnenActief" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) and (AF = 0 or AF is null) and (hoofd =0 or hoofd is null)">
                    <SelectParameters>
                        <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br />

                    <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Approuver" ClientInstanceName="saveChangesBtn" Width="75px" CssClass="inline" Height="27px">
                    <ClientSideEvents Click="saveChangesBtn_Click" />
                </dx:ASPxButton>
               
                <dx:ASPxButton ID="ASPxButton7" runat="server" AutoPostBack="True"  CssClass="inline" Height="27px" Text="Picken en reserveren" Width="75px">
                </dx:ASPxButton>
                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Réjeter" AutoPostBack="false" ClientInstanceName="cancelorderBtn" Width="75px" CssClass="inline">
                </dx:ASPxButton>
                  <dx:ASPxButton ID="ASPxButtonReservatieGoedkeuren" runat="server" Text="Reservatie overdragen" AutoPostBack="true"  Width="75px" CssClass="inline">
                </dx:ASPxButton>

                <br />
                <br />
                <br />
  
                
               
               
                


            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
  
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Bestellijn] WHERE [Bestellijn].[id] = @id" SelectCommand="SELECT [Bestellijn].[id],[Bestellijn].[bestellingId], [Article], [Description], [hoeveelheid], Eenheid,
(Select omschrijving from voo.dbo.MateriaalGroep mg where mg.id =[Basismateriaal].[MateriaalGroep]) as 'Materiaalgroep',
b.locatie FROM [Bestellijn] 
INNER JOIN [Basismateriaal] on [Bestellijn].[materiaalId] = [Basismateriaal].[id]
INNER JOIN [MateriaalOpdrachtgevers] b on [Basismateriaal].id = b.materiaalId
 WHERE ([bestellingId] = @bestellingId) and b.opdrachtgeverId=@opdrachtgever
order by b.volgorde desc, [Description] asc
"
        UpdateCommand="UPDATE [Bestellijn] SET [hoeveelheid] = @hoeveelheid WHERE ([Bestellijn].[id] = @id)">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="bestellingId" QueryStringField="id" />
            <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="hoeveelheid" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>





    <table><tr><td class="auto-style2"> <dx:ASPxTextBox ID="ASPxTextBoxReden" runat="server" Width="370px">
    </dx:ASPxTextBox>
    </td><td class="auto-style2"><dx:ASPxButton ID="ASPxButton6" runat="server" Text="Reden aanpassing/afkeuring verzenden naar plaatser">
    </dx:ASPxButton></td></tr></table>
   






    <br />
    <asp:SqlDataSource ID="SqlDataSourceGrabbel" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Bestellijn] WHERE [Bestellijn].[id] = @id" SelectCommand="SELECT [BestellijnGrabbel].[id],[BestellijnGrabbel].[grabbelbestellingId], [Article],Omschrijving, [BestellijnGrabbel].hoeveelheid FROM [BestellijnGrabbel] INNER JOIN [Grabbelmateriaal] on [BestellijnGrabbel].[grabbelmateriaalId] = [Grabbelmateriaal].[id]
 WHERE ([grabbelbestellingId] = @bestellingId)
Order by volgorde desc, omschrijving asc"
        UpdateCommand="UPDATE [BestellijnGrabbel] SET [hoeveelheid] = @hoeveelheid WHERE ([BestellijnGrabbel].[id] = @id)">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="bestellingId" QueryStringField="id" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="hoeveelheid" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>






    <dx:ASPxPanel ID="ASPxPanel2" runat="server" Width="200px">

        <PanelCollection>
<dx:PanelContent runat="server">
    <table>
        <tr>
            <td>
                <dx:ASPxButton ID="ASPxButtonVorige" runat="server" Text="Vorige">
                </dx:ASPxButton>
            </td>
            <td>
                <dx:ASPxButton ID="ASPxButtonVolgende" runat="server" Text="Volgende">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
            </dx:PanelContent>
</PanelCollection>

    </dx:ASPxPanel>






    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1" ReportHeader="Bestelling" PaperKind="A4" LeftMargin="1" MaxColumnWidth="200">
        <styles>
            <header font-names="Arial">
            </header>
            <cell font-names="Arial">
            </cell>
            <title font-names="Arial" forecolor="DimGray"></title>
        </styles>
        <PageHeader Center="Bestelling">
        </PageHeader>
    </dx:ASPxGridViewExporter>






                <dx:ASPxGridView ID="ASPxGridView2" KeyFieldName="id" runat="server" DataSourceID="SqlDataSourceBestellijnSerienummers" ClientInstanceName="ASPxGridView2" AutoGenerateColumns="False" Width="100%" Caption="Serienummers">
                    <ClientSideEvents EndCallback=" function(s, e) { 
                        console.log('callbak');
                if (s.cpExport) {
                       console.log('exportYes');
                    delete s.cpExport;
                    window.open('Export.aspx', '_blank');
                }
            } 
"  BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing"/>
                    
                    <SettingsPager PageSize="100">
                    </SettingsPager>
                    
                    <SettingsEditing Mode="Batch">
                        <BatchEditSettings StartEditAction="Click" />
                    </SettingsEditing>
                    <Settings ShowStatusBar="Hidden" ShowTitlePanel="true" />
                    <SettingsDataSecurity AllowInsert="False" />
                      <Templates>
        <TitlePanel>
        
        </TitlePanel>
    </Templates>
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>

    
    <asp:SqlDataSource ID="SqlDataSourceBestellijnSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT DISTINCT bsr.id, sr.serienummer, bm.Article, bm.Description
  FROM [Voo].[dbo].[BestellijnSerienummer] bsr
  inner join  [Voo].[dbo].[Bestellijn] bs on bsr.[bestellijnId] = bs.id
  inner join  [Voo].[dbo].Bestellingen b on bs.bestellingId = b.id
    inner join  [Voo].[dbo].Serienummer sr on bsr.serienummerId = sr.id
	    inner join  [Voo].[dbo].StockMagazijn sm on sm.id = sr.StockMagazijnId
			    inner join  [Voo].[dbo].Basismateriaal bm on bm.id = sm.MateriaalId
				where b.id=@id">
        <SelectParameters>
            <asp:QueryStringParameter Name="id" QueryStringField="id" />
        </SelectParameters>

    </asp:SqlDataSource>






    <br />


    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Bestellijn] WHERE [Bestellijn].[id] = @id" SelectCommand="SELECT [RetourBestellijn].[id],[RetourBestellijn].[bestellingId], [Article], [Description], [hoeveelheid] FROM [RetourBestellijn] INNER JOIN [Basismateriaal] on [RetourBestellijn].[materiaalId] = [Basismateriaal].[id]
 WHERE [bestellingId] = (select top 1 [Id] from [RetourBestelling] r  where r.gebruikerId = (select gebruikerId from Bestellingen  where id = @bestellingId)  and r.status = 1)"
        UpdateCommand="UPDATE [Bestellijn] SET [hoeveelheid] = @hoeveelheid WHERE ([Bestellijn].[id] = @id)">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="bestellingId" QueryStringField="id" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="hoeveelheid" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>






    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server" GridViewID="ASPxGridView2" ReportHeader="Serienummers" PaperKind="A4" DetailHorizontalOffset="0" LeftMargin="1">
        <styles>
            <header font-names="Arial">
            </header>
            <cell font-names="arial">
            </cell>
            <title font-names="Arial" forecolor="DimGray"></title>
        </styles>
        <PageHeader Center="Retour">
        </PageHeader>
    </dx:ASPxGridViewExporter>






    <br />






    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter3" runat="server" GridViewID="ASPxGridView3" ReportHeader="Grabbelmateriaal" PaperKind="A4" DetailHorizontalOffset="0" LeftMargin="1">
        <styles>
            <header font-names="Arial">
            </header>
            <cell font-names="arial">
            </cell>
            <title font-names="Arial" forecolor="DimGray"></title>
        </styles>
        <PageHeader Center="Retour">
        </PageHeader>
    </dx:ASPxGridViewExporter>






    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [RetourBestellijnSerienummer] WHERE ([bestellijnId] = @bestellijnId)">
        <SelectParameters>
            <asp:SessionParameter Name="bestellijnId" SessionField="bestellijnId" />
        </SelectParameters>
    </asp:SqlDataSource>
    





</asp:Content>
