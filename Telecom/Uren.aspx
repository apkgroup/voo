﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Uren.aspx.vb" Inherits="Telecom.Uren" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 27px;
        }
    </style>
    <script type="text/JavaScript">
    function GetCheckBoxValue(s, e) {
        var value = s.GetChecked();
        if (value == true) {
            ASPxTimeEditBegin.SetEnabled(false);
            ASPxTimeEditEind.SetEnabled(false);
            ASPxSpinEditKm.SetEnabled(false);
        }
        else {
            ASPxTimeEditBegin.SetEnabled(true);
            ASPxTimeEditEind.SetEnabled(true);
            ASPxSpinEditKm.SetEnabled(true);
        }
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        </h2>
    <table style="border-collapse: collapse; border-spacing: inherit">
        <tr><td colspan="4">
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Registrer un opération pour ">
            </dx:ASPxLabel>
    <dx:ASPxComboBox ID="ASPxComboBox1" runat="server" DataSourceID="SqlDataSourceGebruikers" TextField="Werknemer" ValueField="id">
    </dx:ASPxComboBox>
            b</td></tr>
        <tr>
            <td class="auto-style1">Date : </td><td class="auto-style1">
            
            
            <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" ClientInstanceName="ASPxDateEdit1">
            </dx:ASPxDateEdit>
            </td>  
        </tr>
                <tr>
            <td class="auto-style1">Pas travaillé: </td><td class="auto-style1">
            
            
            
                    <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" CheckState="Unchecked">
                        <ClientSideEvents CheckedChanged="function(s, e) {
	GetCheckBoxValue(s, e)
}" />
                    </dx:ASPxCheckBox>
            
            
            
            </td>  
        </tr>
        <tr>
            <td class="auto-style1">Heure de début: </td><td class="auto-style1">
            <dx:ASPxTimeEdit ID="ASPxTimeEditBegin" runat="server" EnableClientSideAPI="True" ClientInstanceName="ASPxTimeEditBegin">
            </dx:ASPxTimeEdit>
            </td>  
        </tr>
        <tr>
            <td>Heure de fin: </td><td>
            <dx:ASPxTimeEdit ID="ASPxTimeEditEind" runat="server" EnableClientSideAPI="True" ClientInstanceName="ASPxTimeEditEind">
            </dx:ASPxTimeEdit>
            </td>  
        </tr>
        <tr>
            <td>Kilomètres: </td><td>
           
           
            <dx:ASPxSpinEdit ID="ASPxSpinEditKm" runat="server" Number="0" EnableClientSideAPI="True" ClientInstanceName="ASPxSpinEditKm" />
           
           
            </td>  
        </tr>
        <tr>
            <td>Enregistrer</td><td>
            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/Last_16x16.png"/>
            
            </td>  
        </tr>
    </table>
</asp:Content>
