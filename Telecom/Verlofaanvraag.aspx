﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Verlofaanvraag.aspx.vb" Inherits="Telecom.Verlofaanvraag" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
        </style>
    <script type="text/javascript">
        // <![CDATA[
             function SetPCVisible(value) {
            var popupControl = GetPopupControl();
            if (value) {
                popupControl.Show();
            }
            else {
                popupControl.Hide();
            }
             }
             function SetPC2Visible(value) {
                 var popupControl = GetPopupControl2();
                 if (value) {
                     popupControl.Show();
                 }
                 else {
                     popupControl.Hide();
                 }
             }
        function GetPopupControl() {
            return popup;
        }
        function GetPopupControl2() {
            return popup2;
        }
        function OnCallbackComplete(s, e) {
            kalender.ClearSelection();
        }
        // ]]>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h2>Calendrier des congés </h2>     
     <span style="font-family:Calibri;font-size:small;">Sélectionnez le type de congé, une date (ou plusieurs) et cliquez sur le &quot; Laissez Applications &quot; d'envoyer une nouvelle demande (tous les jours). Les demandes de congé de moins d'un jour, cliquez sur le &quot; Congé &lt; 1 jour. &quot;</span><br />
    <asp:Literal ID="Literal2" runat="server"></asp:Literal>
     <br />
    <table>
        <tr><td>

            </td>
            <td>

                &nbsp;</td>
            <td>
                <div id="labelfa" style="display:none;">Raison FA:</div>

            </td>
            <td>
                <div id="textfa" style="display:none;"><dx:ASPxTextBox ID="ASPxTextBoxRedenFA" ClientInstanceName="txtRedenfa" runat="server" Width="250px" MaxLength="255">
                </dx:ASPxTextBox></div>

                

            </td>
            <td>
                 <dx:ASPxButton ID="ASPxButtonVerlofaanvragen" runat="server" Text="Appliquer congé" AutoPostBack="False">
                     <ClientSideEvents Click="function(s, e) {
	 if (kalender.GetSelectedDates().length&lt;1) {
                    lblPopup.SetText('Il n y a pas de date sélectionnée!');
                    SetPCVisible(true);
                } else {
                           Callback1.PerformCallback();
                         }
                        
}" />
         <Image Url="~/images/EmailTemplate_32x32.png" Height="20px" Width="20px">
         </Image>
     </dx:ASPxButton>
            </td>
             <td>

                 &nbsp;</td>
            <td>

                &nbsp;</td>
            <td>
                <dx:ASPxButton ID="ASPxButtonVerlofuren" runat="server" Text="Congé &lt; 1 jour" AutoPostBack="False">
                    <ClientSideEvents Click="function(s, e) {
	SetPC2Visible(true);

}" />
                    <Image Url="~/images/TimeLineView_32x32.png" Height="20px" Width="20px">
                    </Image>
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="background-color:yellow; padding:5px;">Congé en demande</td><td style="background-color:lightgreen; padding:5px;">Congé accepté</td><td style="background-color:red; padding:5px;">Congé réfuse</td><td style="background-color:lightblue; padding:5px;">Congés pris</td>
        </tr>

    </table>
     <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback1"
        OnCallback="ASPxCallback1_Callback">
         <ClientSideEvents CallbackComplete="OnCallbackComplete" />
     </dx:ASPxCallback>
     
     <br />
     <dx:ASPxCalendar ID="ASPxCalendar1" runat="server" ClearButtonText="Effacer" Columns="4" EnableMultiSelect="True" HighlightToday="False" Rows="3" TodayButtonText="Aujourd'hui" VisibleDate="2016-01-01" EnableMonthNavigation="False" ClientInstanceName="kalender">
         <DaySelectedStyle>
             <Border BorderColor="#FF3300" BorderStyle="Solid" BorderWidth="1px" />
         </DaySelectedStyle>
         <DayWeekendStyle ForeColor="Gray">
         </DayWeekendStyle>
     </dx:ASPxCalendar>
     <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="popup" runat="server" 
        CloseAction="CloseButton" HeaderText="Foutmelding" Modal="True" 
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" 
        Width="314px" MinWidth="300px">
        <HeaderImage Url="~/images/warning.png">
        </HeaderImage>
         <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <dx:ASPxLabel ID="lblPopup" ClientInstanceName="lblPopup" runat="server"></dx:ASPxLabel>
      
    <br />
    L'action est annulée.
    </dx:PopupControlContentControl>

</ContentCollection>
     </dx:ASPxPopupControl>
     <br />
    <dx:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="popup2" runat="server" 
        CloseAction="CloseButton" HeaderText="Sélectionnez le jour et l'heure pour laquelle vous voulez laisser" Modal="True" 
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" 
        Width="400px" MinWidth="400px" Height="300px" ViewStateMode="Enabled">
        <HeaderImage Url="~/images/TimeLineView_32x32.png" Height="20px" Width="20px">
        </HeaderImage>
         <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <table>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Jour:">
    </dx:ASPxLabel>
            </td>
             <td>
                 <dx:ASPxDateEdit ID="ASPxDateEditDag" runat="server" DisplayFormatString="d" EditFormat="Custom" EditFormatString="d">
                     <ValidationSettings>
                         <ErrorImage Url="~/images/warning.png">
                         </ErrorImage>
                         <RequiredField ErrorText="Champ obligatoire" IsRequired="True" />
                     </ValidationSettings>
    </dx:ASPxDateEdit>
            </td>
        </tr>
        <tr>
            <td>

                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Matin ou après-midi:">
                </dx:ASPxLabel>

            </td>
            <td>

                <dx:ASPxComboBox ID="cboVMNM" runat="server">
                    <Items>
                        <dx:ListEditItem Text="Matin" Value="1" />
                        <dx:ListEditItem Text="Après-midi" Value="2" />
                    </Items>
                    <ValidationSettings>
                        <RequiredField ErrorText="Champ obligatoire" IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxComboBox>

            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>

                <dx:ASPxButton ID="ASPxButtonBevestigen" runat="server" Text="Confirmer" Width="100%">
                    <ClientSideEvents Click="function(s, e) {
	SetPC2Visible(false);

}" />
                    <Image Url="~/images/Apply_16x16.png">
                    </Image>
                </dx:ASPxButton>

            </td>
        </tr>
    </table>
    
    
    </dx:PopupControlContentControl>
</ContentCollection>
     </dx:ASPxPopupControl>
     </asp:Content>
