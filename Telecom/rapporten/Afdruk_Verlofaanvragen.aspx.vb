﻿Imports System.Data.SqlClient

Public Class Afdruk_Verlofaanvragen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
        Dim rpt As New rap_OverzichtVerlofaanvragen
        rpt.parameter1.Value = DateTime.Now.Year
        rpt.parameter1.Visible = False
        rpt.parameter2.Value = Session("userid")
        rpt.parameter2.Visible = False
        rpt.RequestParameters = False
        rpt.lblTitel.Text = Session("sitetitel")
        If Not String.IsNullOrEmpty(Session("telenetlogo")) Then
            rpt.XrPictureBoxTelenetlogo.ImageUrl = Session("telenetlogo")
        End If
        ASPxDocumentViewer1.Report = rpt
        ASPxDocumentViewer1.SettingsSplitter.ParametersPanelCollapsed = True
    End Sub

End Class