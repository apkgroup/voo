﻿Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Public Class rap_OverzichtVerlofaanvragen
    Private Sub xrSubreport1_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs) Handles XrSubreport1.BeforePrint
        CType((CType(sender, XRSubreport)).ReportSource, sub_OverzichtVerlofaanvragenTotalen).GebruikerID.Value = Convert.ToInt32(GetCurrentColumnValue("GebruikerID"))
        CType((CType(sender, XRSubreport)).ReportSource, sub_OverzichtVerlofaanvragenTotalen).jaar.Value = Me.parameter1.Value
        CType((CType(sender, XRSubreport)).ReportSource, sub_OverzichtVerlofaanvragenTotalen).userid.Value = Me.parameter2.Value
    End Sub
End Class