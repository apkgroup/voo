﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Afdruk_Verlofaanvragen.aspx.vb" Inherits="Telecom.Afdruk_Verlofaanvragen" %>

<%@ Register assembly="DevExpress.XtraReports.v19.2.Web.WebForms, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rapport Verlofaanvragen</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" runat="server">
              <SettingsSplitter ParametersPanelCollapsed="True" SidePanePosition="Left" />


<StylesReportViewer>
<Paddings Padding="10px"></Paddings>
</StylesReportViewer>
              <ToolbarItems>
                  <dx:ReportToolbarButton ItemKind="Search" />
                  <dx:ReportToolbarSeparator />
                  <dx:ReportToolbarButton ItemKind="PrintReport" />
                  <dx:ReportToolbarButton ItemKind="PrintPage" />
                  <dx:ReportToolbarSeparator />
                  <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                  <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                  <dx:ReportToolbarLabel ItemKind="PageLabel" />
                  <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                  </dx:ReportToolbarComboBox>
                  <dx:ReportToolbarLabel ItemKind="OfLabel" />
                  <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                  <dx:ReportToolbarButton ItemKind="NextPage" />
                  <dx:ReportToolbarButton ItemKind="LastPage" />
                  <dx:ReportToolbarSeparator />
                  <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                  <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                  <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                      <Elements>
                          <dx:ListElement Value="pdf" />
                          <dx:ListElement Value="xls" />
                          <dx:ListElement Value="xlsx" />
                          <dx:ListElement Value="rtf" />
                          <dx:ListElement Value="mht" />
                          <dx:ListElement Value="html" />
                          <dx:ListElement Value="txt" />
                          <dx:ListElement Value="csv" />
                          <dx:ListElement Value="png" />
                      </Elements>
                  </dx:ReportToolbarComboBox>
                  <dx:ReportToolbarButton ImageUrl="~/images/Backward_16x16.png" Name="Terugkeren" Text="Terug naar de website" ToolTip="Klik hier om het rapport te sluiten en terug te keren naar de website" />
              </ToolbarItems>
              <ClientSideEvents BeginCallback="function(s, e) {
	}" ToolbarItemClick="function(s, e) {
	 if(e.item.name == &quot;Terugkeren&quot;)
    {
        window.location.href = &quot;http://&quot; + window.location.host + &quot;/Verlofaanvragenverwerken.aspx&quot; 
    }

}" />
        </dx:ASPxDocumentViewer>
    
    </div>
    </form>
</body>
</html>
