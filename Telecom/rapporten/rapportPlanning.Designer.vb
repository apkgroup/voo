﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rapportPlanning
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim StoredProcQuery1 As DevExpress.DataAccess.Sql.StoredProcQuery = New DevExpress.DataAccess.Sql.StoredProcQuery()
        Dim QueryParameter1 As DevExpress.DataAccess.Sql.QueryParameter = New DevExpress.DataAccess.Sql.QueryParameter()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rapportPlanning))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.FormattingRuleCoordinatie = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleFieldC = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleMagazijn = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleRegel = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleTechnieker = New DevExpress.XtraReports.UI.FormattingRule()
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.FormattingRuleCoordinatie01 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleTechnieker01 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleFieldC01 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleRegel01 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleMagazijn01 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleZ01 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleAfwezig01 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.FormattingRuleCoordinatie02 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleFieldC02 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleMagazijn02 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleRegel02 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleTechnieker02 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleZ02 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleAfwezig02 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.FormattingRuleCoordinatie03 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleFieldC03 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleMagazijn03 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleRegel03 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleTechnieker03 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleZ03 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleAfwezig03 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.FormattingRuleCoordinatie04 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleFieldC04 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleMagazijn04 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleRegel04 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleTechnieker04 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleZ04 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleAfwezig04 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.FormattingRuleCoordinatie05 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleFieldC05 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleMagazijn05 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleRegel05 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleTechnieker05 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleZ05 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleAfwezig05 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.FormattingRuleCoordinatie06 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleFieldC06 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleMagazijn06 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleRegel06 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleTechnieker06 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleZ06 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleAfwezig06 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.FormattingRuleCoordinatie07 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleFieldC07 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleMagazijn07 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleRegel07 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleTechnieker07 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleZ07 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleAfwezig07 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.FormattingRuleCoordinatie08 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleFieldC08 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleMagazijn08 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleRegel08 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleTechnieker08 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleZ08 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleAfwezig08 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.FormattingRuleCoordinatie09 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleFieldC09 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleMagazijn09 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleRegel09 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleTechnieker09 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleZ09 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleAfwezig09 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.FormattingRuleCoordinatie10 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleFieldC10 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleMagazijn10 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleRegel10 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleTechnieker10 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleZ10 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.FormattingRuleAfwezig10 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.SqlDataSource1 = New DevExpress.DataAccess.Sql.SqlDataSource()
        Me.PageFooterBand1 = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Dag01 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Dag02 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Dag03 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Dag04 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Dag05 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Dag06 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Dag07 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Dag08 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Dag09 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Dag10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.lblTitel = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPictureBoxTelenetLogo = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.Title = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.FieldCaption = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.PageInfo = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.DataField = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.XrControlStyleHoofding = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.Parameter1 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
        Me.Detail.Dpi = 254.0!
        Me.Detail.HeightF = 63.5!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.Detail.SortFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("Groep", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending), New DevExpress.XtraReports.UI.GroupField("Werknemer", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrTable1
        '
        Me.XrTable1.BorderColor = System.Drawing.Color.DimGray
        Me.XrTable1.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable1.Dpi = 254.0!
        Me.XrTable1.Font = New System.Drawing.Font("Calibri", 8.0!)
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(2727.36499!, 63.5!)
        Me.XrTable1.StylePriority.UseBorderColor = False
        Me.XrTable1.StylePriority.UseBorders = False
        Me.XrTable1.StylePriority.UseFont = False
        Me.XrTable1.StylePriority.UseTextAlignment = False
        Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell3, Me.XrTableCell4, Me.XrTableCell5, Me.XrTableCell6, Me.XrTableCell7, Me.XrTableCell8, Me.XrTableCell9, Me.XrTableCell11, Me.XrTableCell10})
        Me.XrTableRow1.Dpi = 254.0!
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 1.0R
        '
        'XrTableCell1
        '
        Me.XrTableCell1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "prRapportPlanning.Werknemer")})
        Me.XrTableCell1.Dpi = 254.0!
        Me.XrTableCell1.FormattingRules.Add(Me.FormattingRuleCoordinatie)
        Me.XrTableCell1.FormattingRules.Add(Me.FormattingRuleFieldC)
        Me.XrTableCell1.FormattingRules.Add(Me.FormattingRuleMagazijn)
        Me.XrTableCell1.FormattingRules.Add(Me.FormattingRuleRegel)
        Me.XrTableCell1.FormattingRules.Add(Me.FormattingRuleTechnieker)
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.StylePriority.UseTextAlignment = False
        Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell1.Weight = 1.4161754799313169R
        '
        'FormattingRuleCoordinatie
        '
        Me.FormattingRuleCoordinatie.Condition = "[Groep] == 'Coordinatie'"
        Me.FormattingRuleCoordinatie.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleCoordinatie.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleCoordinatie.Name = "FormattingRuleCoordinatie"
        '
        'FormattingRuleFieldC
        '
        Me.FormattingRuleFieldC.Condition = "[Groep] == 'Field Coordinator'"
        Me.FormattingRuleFieldC.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleFieldC.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FormattingRuleFieldC.Name = "FormattingRuleFieldC"
        '
        'FormattingRuleMagazijn
        '
        Me.FormattingRuleMagazijn.Condition = "[Groep] == 'Magazijn'"
        Me.FormattingRuleMagazijn.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleMagazijn.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormattingRuleMagazijn.Name = "FormattingRuleMagazijn"
        '
        'FormattingRuleRegel
        '
        Me.FormattingRuleRegel.Condition = "[Groep] == 'Regel/Meet technieker'"
        Me.FormattingRuleRegel.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleRegel.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.FormattingRuleRegel.Name = "FormattingRuleRegel"
        '
        'FormattingRuleTechnieker
        '
        Me.FormattingRuleTechnieker.Condition = "[Groep] == 'Techniekers'"
        Me.FormattingRuleTechnieker.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleTechnieker.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleTechnieker.Name = "FormattingRuleTechnieker"
        '
        'XrTableCell2
        '
        Me.XrTableCell2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "prRapportPlanning.Dag01")})
        Me.XrTableCell2.Dpi = 254.0!
        Me.XrTableCell2.FormattingRules.Add(Me.FormattingRuleCoordinatie01)
        Me.XrTableCell2.FormattingRules.Add(Me.FormattingRuleTechnieker01)
        Me.XrTableCell2.FormattingRules.Add(Me.FormattingRuleFieldC01)
        Me.XrTableCell2.FormattingRules.Add(Me.FormattingRuleRegel01)
        Me.XrTableCell2.FormattingRules.Add(Me.FormattingRuleMagazijn01)
        Me.XrTableCell2.FormattingRules.Add(Me.FormattingRuleZ01)
        Me.XrTableCell2.FormattingRules.Add(Me.FormattingRuleAfwezig01)
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.Weight = 0.87556578190059997R
        '
        'FormattingRuleCoordinatie01
        '
        Me.FormattingRuleCoordinatie01.Condition = "[Groep] == 'Coordinatie'  And IsNull([Dag01]) == False"
        Me.FormattingRuleCoordinatie01.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleCoordinatie01.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleCoordinatie01.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleCoordinatie01.Name = "FormattingRuleCoordinatie01"
        '
        'FormattingRuleTechnieker01
        '
        Me.FormattingRuleTechnieker01.Condition = "[Groep] == 'Techniekers'  And IsNull([Dag01]) == False"
        Me.FormattingRuleTechnieker01.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleTechnieker01.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleTechnieker01.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleTechnieker01.Name = "FormattingRuleTechnieker01"
        '
        'FormattingRuleFieldC01
        '
        Me.FormattingRuleFieldC01.Condition = "[Groep] == 'Field Coordinator'  And IsNull([Dag01]) == False"
        Me.FormattingRuleFieldC01.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleFieldC01.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FormattingRuleFieldC01.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleFieldC01.Name = "FormattingRuleFieldC01"
        '
        'FormattingRuleRegel01
        '
        Me.FormattingRuleRegel01.Condition = "[Groep] == 'Regel/Meet technieker'  And IsNull([Dag01]) == False"
        Me.FormattingRuleRegel01.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleRegel01.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.FormattingRuleRegel01.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleRegel01.Name = "FormattingRuleRegel01"
        '
        'FormattingRuleMagazijn01
        '
        Me.FormattingRuleMagazijn01.Condition = "[Groep] == 'Magazijn'  And IsNull([Dag01]) == False"
        Me.FormattingRuleMagazijn01.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleMagazijn01.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormattingRuleMagazijn01.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleMagazijn01.Name = "FormattingRuleMagazijn01"
        '
        'FormattingRuleZ01
        '
        Me.FormattingRuleZ01.Condition = "Substring([Dag01],0,1 ) == 'Z'"
        Me.FormattingRuleZ01.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleZ01.Formatting.BackColor = System.Drawing.Color.Gainsboro
        Me.FormattingRuleZ01.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleZ01.Name = "FormattingRuleZ01"
        '
        'FormattingRuleAfwezig01
        '
        Me.FormattingRuleAfwezig01.Condition = "[Dag01] == 'Afwezig'"
        Me.FormattingRuleAfwezig01.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleAfwezig01.Formatting.BackColor = System.Drawing.Color.Red
        Me.FormattingRuleAfwezig01.Formatting.ForeColor = System.Drawing.Color.White
        Me.FormattingRuleAfwezig01.Name = "FormattingRuleAfwezig01"
        '
        'XrTableCell3
        '
        Me.XrTableCell3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "prRapportPlanning.Dag02")})
        Me.XrTableCell3.Dpi = 254.0!
        Me.XrTableCell3.FormattingRules.Add(Me.FormattingRuleCoordinatie02)
        Me.XrTableCell3.FormattingRules.Add(Me.FormattingRuleFieldC02)
        Me.XrTableCell3.FormattingRules.Add(Me.FormattingRuleMagazijn02)
        Me.XrTableCell3.FormattingRules.Add(Me.FormattingRuleRegel02)
        Me.XrTableCell3.FormattingRules.Add(Me.FormattingRuleTechnieker02)
        Me.XrTableCell3.FormattingRules.Add(Me.FormattingRuleZ02)
        Me.XrTableCell3.FormattingRules.Add(Me.FormattingRuleAfwezig02)
        Me.XrTableCell3.Name = "XrTableCell3"
        Me.XrTableCell3.Weight = 0.87556581048366366R
        '
        'FormattingRuleCoordinatie02
        '
        Me.FormattingRuleCoordinatie02.Condition = "[Groep] == 'Coordinatie'  And IsNull([Dag02]) == False"
        Me.FormattingRuleCoordinatie02.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleCoordinatie02.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleCoordinatie02.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleCoordinatie02.Name = "FormattingRuleCoordinatie02"
        '
        'FormattingRuleFieldC02
        '
        Me.FormattingRuleFieldC02.Condition = "[Groep] == 'Field Coordinator'  And IsNull([Dag02]) == False"
        Me.FormattingRuleFieldC02.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleFieldC02.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FormattingRuleFieldC02.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleFieldC02.Name = "FormattingRuleFieldC02"
        '
        'FormattingRuleMagazijn02
        '
        Me.FormattingRuleMagazijn02.Condition = "[Groep] == 'Magazijn'  And IsNull([Dag02]) == False"
        Me.FormattingRuleMagazijn02.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleMagazijn02.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormattingRuleMagazijn02.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleMagazijn02.Name = "FormattingRuleMagazijn02"
        '
        'FormattingRuleRegel02
        '
        Me.FormattingRuleRegel02.Condition = "[Groep] == 'Regel/Meet technieker'  And IsNull([Dag02]) == False"
        Me.FormattingRuleRegel02.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleRegel02.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.FormattingRuleRegel02.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleRegel02.Name = "FormattingRuleRegel02"
        '
        'FormattingRuleTechnieker02
        '
        Me.FormattingRuleTechnieker02.Condition = "[Groep] == 'Techniekers'  And IsNull([Dag02]) == False"
        Me.FormattingRuleTechnieker02.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleTechnieker02.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleTechnieker02.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleTechnieker02.Name = "FormattingRuleTechnieker02"
        '
        'FormattingRuleZ02
        '
        Me.FormattingRuleZ02.Condition = "Substring([Dag02],0,1 ) == 'Z'"
        Me.FormattingRuleZ02.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleZ02.Formatting.BackColor = System.Drawing.Color.Gainsboro
        Me.FormattingRuleZ02.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleZ02.Name = "FormattingRuleZ02"
        '
        'FormattingRuleAfwezig02
        '
        Me.FormattingRuleAfwezig02.Condition = "[Dag02] == 'Afwezig'"
        Me.FormattingRuleAfwezig02.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleAfwezig02.Formatting.BackColor = System.Drawing.Color.Red
        Me.FormattingRuleAfwezig02.Formatting.ForeColor = System.Drawing.Color.White
        Me.FormattingRuleAfwezig02.Name = "FormattingRuleAfwezig02"
        '
        'XrTableCell4
        '
        Me.XrTableCell4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "prRapportPlanning.Dag03")})
        Me.XrTableCell4.Dpi = 254.0!
        Me.XrTableCell4.FormattingRules.Add(Me.FormattingRuleCoordinatie03)
        Me.XrTableCell4.FormattingRules.Add(Me.FormattingRuleFieldC03)
        Me.XrTableCell4.FormattingRules.Add(Me.FormattingRuleMagazijn03)
        Me.XrTableCell4.FormattingRules.Add(Me.FormattingRuleRegel03)
        Me.XrTableCell4.FormattingRules.Add(Me.FormattingRuleTechnieker03)
        Me.XrTableCell4.FormattingRules.Add(Me.FormattingRuleZ03)
        Me.XrTableCell4.FormattingRules.Add(Me.FormattingRuleAfwezig03)
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.Weight = 0.8755658154867525R
        '
        'FormattingRuleCoordinatie03
        '
        Me.FormattingRuleCoordinatie03.Condition = "[Groep] == 'Coordinatie'  And IsNull([Dag03]) == False"
        Me.FormattingRuleCoordinatie03.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleCoordinatie03.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleCoordinatie03.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleCoordinatie03.Name = "FormattingRuleCoordinatie03"
        '
        'FormattingRuleFieldC03
        '
        Me.FormattingRuleFieldC03.Condition = "[Groep] == 'Field Coordinator'  And IsNull([Dag03]) == False"
        Me.FormattingRuleFieldC03.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleFieldC03.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FormattingRuleFieldC03.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleFieldC03.Name = "FormattingRuleFieldC03"
        '
        'FormattingRuleMagazijn03
        '
        Me.FormattingRuleMagazijn03.Condition = "[Groep] == 'Magazijn'  And IsNull([Dag03]) == False"
        Me.FormattingRuleMagazijn03.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleMagazijn03.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormattingRuleMagazijn03.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleMagazijn03.Name = "FormattingRuleMagazijn03"
        '
        'FormattingRuleRegel03
        '
        Me.FormattingRuleRegel03.Condition = "[Groep] == 'Regel/Meet technieker'  And IsNull([Dag03]) == False"
        Me.FormattingRuleRegel03.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleRegel03.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.FormattingRuleRegel03.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleRegel03.Name = "FormattingRuleRegel03"
        '
        'FormattingRuleTechnieker03
        '
        Me.FormattingRuleTechnieker03.Condition = "[Groep] == 'Techniekers'  And IsNull([Dag03]) == False"
        Me.FormattingRuleTechnieker03.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleTechnieker03.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleTechnieker03.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleTechnieker03.Name = "FormattingRuleTechnieker03"
        '
        'FormattingRuleZ03
        '
        Me.FormattingRuleZ03.Condition = "Substring([Dag03],0,1 ) == 'Z'"
        Me.FormattingRuleZ03.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleZ03.Formatting.BackColor = System.Drawing.Color.Gainsboro
        Me.FormattingRuleZ03.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleZ03.Name = "FormattingRuleZ03"
        '
        'FormattingRuleAfwezig03
        '
        Me.FormattingRuleAfwezig03.Condition = "[Dag03] == 'Afwezig'"
        Me.FormattingRuleAfwezig03.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleAfwezig03.Formatting.BackColor = System.Drawing.Color.Red
        Me.FormattingRuleAfwezig03.Formatting.ForeColor = System.Drawing.Color.White
        Me.FormattingRuleAfwezig03.Name = "FormattingRuleAfwezig03"
        '
        'XrTableCell5
        '
        Me.XrTableCell5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "prRapportPlanning.Dag04")})
        Me.XrTableCell5.Dpi = 254.0!
        Me.XrTableCell5.FormattingRules.Add(Me.FormattingRuleCoordinatie04)
        Me.XrTableCell5.FormattingRules.Add(Me.FormattingRuleFieldC04)
        Me.XrTableCell5.FormattingRules.Add(Me.FormattingRuleMagazijn04)
        Me.XrTableCell5.FormattingRules.Add(Me.FormattingRuleRegel04)
        Me.XrTableCell5.FormattingRules.Add(Me.FormattingRuleTechnieker04)
        Me.XrTableCell5.FormattingRules.Add(Me.FormattingRuleZ04)
        Me.XrTableCell5.FormattingRules.Add(Me.FormattingRuleAfwezig04)
        Me.XrTableCell5.Name = "XrTableCell5"
        Me.XrTableCell5.Weight = 0.87556581548675239R
        '
        'FormattingRuleCoordinatie04
        '
        Me.FormattingRuleCoordinatie04.Condition = "[Groep] == 'Coordinatie'  And IsNull([Dag04]) == False"
        Me.FormattingRuleCoordinatie04.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleCoordinatie04.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleCoordinatie04.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleCoordinatie04.Name = "FormattingRuleCoordinatie04"
        '
        'FormattingRuleFieldC04
        '
        Me.FormattingRuleFieldC04.Condition = "[Groep] == 'Field Coordinator'  And IsNull([Dag04]) == False"
        Me.FormattingRuleFieldC04.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleFieldC04.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FormattingRuleFieldC04.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleFieldC04.Name = "FormattingRuleFieldC04"
        '
        'FormattingRuleMagazijn04
        '
        Me.FormattingRuleMagazijn04.Condition = "[Groep] == 'Magazijn'  And IsNull([Dag04]) == False"
        Me.FormattingRuleMagazijn04.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleMagazijn04.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormattingRuleMagazijn04.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleMagazijn04.Name = "FormattingRuleMagazijn04"
        '
        'FormattingRuleRegel04
        '
        Me.FormattingRuleRegel04.Condition = "[Groep] == 'Regel/Meet technieker'  And IsNull([Dag04]) == False"
        Me.FormattingRuleRegel04.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleRegel04.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.FormattingRuleRegel04.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleRegel04.Name = "FormattingRuleRegel04"
        '
        'FormattingRuleTechnieker04
        '
        Me.FormattingRuleTechnieker04.Condition = "[Groep] == 'Techniekers'  And IsNull([Dag04]) == False"
        Me.FormattingRuleTechnieker04.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleTechnieker04.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleTechnieker04.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleTechnieker04.Name = "FormattingRuleTechnieker04"
        '
        'FormattingRuleZ04
        '
        Me.FormattingRuleZ04.Condition = "Substring([Dag04],0,1 ) == 'Z'"
        Me.FormattingRuleZ04.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleZ04.Formatting.BackColor = System.Drawing.Color.Gainsboro
        Me.FormattingRuleZ04.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleZ04.Name = "FormattingRuleZ04"
        '
        'FormattingRuleAfwezig04
        '
        Me.FormattingRuleAfwezig04.Condition = "[Dag04] == 'Afwezig'"
        Me.FormattingRuleAfwezig04.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleAfwezig04.Formatting.BackColor = System.Drawing.Color.Red
        Me.FormattingRuleAfwezig04.Formatting.ForeColor = System.Drawing.Color.White
        Me.FormattingRuleAfwezig04.Name = "FormattingRuleAfwezig04"
        '
        'XrTableCell6
        '
        Me.XrTableCell6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "prRapportPlanning.Dag05")})
        Me.XrTableCell6.Dpi = 254.0!
        Me.XrTableCell6.FormattingRules.Add(Me.FormattingRuleCoordinatie05)
        Me.XrTableCell6.FormattingRules.Add(Me.FormattingRuleFieldC05)
        Me.XrTableCell6.FormattingRules.Add(Me.FormattingRuleMagazijn05)
        Me.XrTableCell6.FormattingRules.Add(Me.FormattingRuleRegel05)
        Me.XrTableCell6.FormattingRules.Add(Me.FormattingRuleTechnieker05)
        Me.XrTableCell6.FormattingRules.Add(Me.FormattingRuleZ05)
        Me.XrTableCell6.FormattingRules.Add(Me.FormattingRuleAfwezig05)
        Me.XrTableCell6.Name = "XrTableCell6"
        Me.XrTableCell6.Weight = 0.8755658154867525R
        '
        'FormattingRuleCoordinatie05
        '
        Me.FormattingRuleCoordinatie05.Condition = "[Groep] == 'Coordinatie'  And IsNull([Dag05]) == False"
        Me.FormattingRuleCoordinatie05.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleCoordinatie05.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleCoordinatie05.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleCoordinatie05.Name = "FormattingRuleCoordinatie05"
        '
        'FormattingRuleFieldC05
        '
        Me.FormattingRuleFieldC05.Condition = "[Groep] == 'Field Coordinator'  And IsNull([Dag05]) == False"
        Me.FormattingRuleFieldC05.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleFieldC05.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FormattingRuleFieldC05.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleFieldC05.Name = "FormattingRuleFieldC05"
        '
        'FormattingRuleMagazijn05
        '
        Me.FormattingRuleMagazijn05.Condition = "[Groep] == 'Magazijn'  And IsNull([Dag05]) == False"
        Me.FormattingRuleMagazijn05.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleMagazijn05.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormattingRuleMagazijn05.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleMagazijn05.Name = "FormattingRuleMagazijn05"
        '
        'FormattingRuleRegel05
        '
        Me.FormattingRuleRegel05.Condition = "[Groep] == 'Regel/Meet technieker'  And IsNull([Dag05]) == False"
        Me.FormattingRuleRegel05.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleRegel05.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.FormattingRuleRegel05.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleRegel05.Name = "FormattingRuleRegel05"
        '
        'FormattingRuleTechnieker05
        '
        Me.FormattingRuleTechnieker05.Condition = "[Groep] == 'Techniekers'  And IsNull([Dag05]) == False"
        Me.FormattingRuleTechnieker05.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleTechnieker05.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleTechnieker05.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleTechnieker05.Name = "FormattingRuleTechnieker05"
        '
        'FormattingRuleZ05
        '
        Me.FormattingRuleZ05.Condition = "Substring([Dag05],0,1 ) == 'Z'"
        Me.FormattingRuleZ05.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleZ05.Formatting.BackColor = System.Drawing.Color.Gainsboro
        Me.FormattingRuleZ05.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleZ05.Name = "FormattingRuleZ05"
        '
        'FormattingRuleAfwezig05
        '
        Me.FormattingRuleAfwezig05.Condition = "[Dag05] == 'Afwezig'"
        Me.FormattingRuleAfwezig05.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleAfwezig05.Formatting.BackColor = System.Drawing.Color.Red
        Me.FormattingRuleAfwezig05.Formatting.ForeColor = System.Drawing.Color.White
        Me.FormattingRuleAfwezig05.Name = "FormattingRuleAfwezig05"
        '
        'XrTableCell7
        '
        Me.XrTableCell7.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "prRapportPlanning.Dag06")})
        Me.XrTableCell7.Dpi = 254.0!
        Me.XrTableCell7.FormattingRules.Add(Me.FormattingRuleCoordinatie06)
        Me.XrTableCell7.FormattingRules.Add(Me.FormattingRuleFieldC06)
        Me.XrTableCell7.FormattingRules.Add(Me.FormattingRuleMagazijn06)
        Me.XrTableCell7.FormattingRules.Add(Me.FormattingRuleRegel06)
        Me.XrTableCell7.FormattingRules.Add(Me.FormattingRuleTechnieker06)
        Me.XrTableCell7.FormattingRules.Add(Me.FormattingRuleZ06)
        Me.XrTableCell7.FormattingRules.Add(Me.FormattingRuleAfwezig06)
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.Weight = 0.87556581548675272R
        '
        'FormattingRuleCoordinatie06
        '
        Me.FormattingRuleCoordinatie06.Condition = "[Groep] == 'Coordinatie'  And IsNull([Dag06]) == False"
        Me.FormattingRuleCoordinatie06.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleCoordinatie06.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleCoordinatie06.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleCoordinatie06.Name = "FormattingRuleCoordinatie06"
        '
        'FormattingRuleFieldC06
        '
        Me.FormattingRuleFieldC06.Condition = "[Groep] == 'Field Coordinator'  And IsNull([Dag06]) == False"
        Me.FormattingRuleFieldC06.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleFieldC06.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FormattingRuleFieldC06.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleFieldC06.Name = "FormattingRuleFieldC06"
        '
        'FormattingRuleMagazijn06
        '
        Me.FormattingRuleMagazijn06.Condition = "[Groep] == 'Magazijn'  And IsNull([Dag06]) == False"
        Me.FormattingRuleMagazijn06.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleMagazijn06.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormattingRuleMagazijn06.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleMagazijn06.Name = "FormattingRuleMagazijn06"
        '
        'FormattingRuleRegel06
        '
        Me.FormattingRuleRegel06.Condition = "[Groep] == 'Regel/Meet technieker'  And IsNull([Dag06]) == False"
        Me.FormattingRuleRegel06.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleRegel06.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.FormattingRuleRegel06.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleRegel06.Name = "FormattingRuleRegel06"
        '
        'FormattingRuleTechnieker06
        '
        Me.FormattingRuleTechnieker06.Condition = "[Groep] == 'Techniekers'  And IsNull([Dag06]) == False"
        Me.FormattingRuleTechnieker06.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleTechnieker06.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleTechnieker06.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleTechnieker06.Name = "FormattingRuleTechnieker06"
        '
        'FormattingRuleZ06
        '
        Me.FormattingRuleZ06.Condition = "Substring([Dag06],0,1 ) == 'Z'"
        Me.FormattingRuleZ06.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleZ06.Formatting.BackColor = System.Drawing.Color.Gainsboro
        Me.FormattingRuleZ06.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleZ06.Name = "FormattingRuleZ06"
        '
        'FormattingRuleAfwezig06
        '
        Me.FormattingRuleAfwezig06.Condition = "[Dag06] == 'Afwezig'"
        Me.FormattingRuleAfwezig06.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleAfwezig06.Formatting.BackColor = System.Drawing.Color.Red
        Me.FormattingRuleAfwezig06.Formatting.ForeColor = System.Drawing.Color.White
        Me.FormattingRuleAfwezig06.Name = "FormattingRuleAfwezig06"
        '
        'XrTableCell8
        '
        Me.XrTableCell8.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "prRapportPlanning.Dag07")})
        Me.XrTableCell8.Dpi = 254.0!
        Me.XrTableCell8.FormattingRules.Add(Me.FormattingRuleCoordinatie07)
        Me.XrTableCell8.FormattingRules.Add(Me.FormattingRuleFieldC07)
        Me.XrTableCell8.FormattingRules.Add(Me.FormattingRuleMagazijn07)
        Me.XrTableCell8.FormattingRules.Add(Me.FormattingRuleRegel07)
        Me.XrTableCell8.FormattingRules.Add(Me.FormattingRuleTechnieker07)
        Me.XrTableCell8.FormattingRules.Add(Me.FormattingRuleZ07)
        Me.XrTableCell8.FormattingRules.Add(Me.FormattingRuleAfwezig07)
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.Weight = 0.8755658154867525R
        '
        'FormattingRuleCoordinatie07
        '
        Me.FormattingRuleCoordinatie07.Condition = "[Groep] == 'Coordinatie'  And IsNull([Dag07]) == False"
        Me.FormattingRuleCoordinatie07.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleCoordinatie07.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleCoordinatie07.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleCoordinatie07.Name = "FormattingRuleCoordinatie07"
        '
        'FormattingRuleFieldC07
        '
        Me.FormattingRuleFieldC07.Condition = "[Groep] == 'Field Coordinator'  And IsNull([Dag07]) == False"
        Me.FormattingRuleFieldC07.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleFieldC07.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FormattingRuleFieldC07.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleFieldC07.Name = "FormattingRuleFieldC07"
        '
        'FormattingRuleMagazijn07
        '
        Me.FormattingRuleMagazijn07.Condition = "[Groep] == 'Magazijn'  And IsNull([Dag07]) == False"
        Me.FormattingRuleMagazijn07.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleMagazijn07.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormattingRuleMagazijn07.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleMagazijn07.Name = "FormattingRuleMagazijn07"
        '
        'FormattingRuleRegel07
        '
        Me.FormattingRuleRegel07.Condition = "[Groep] == 'Regel/Meet technieker'  And IsNull([Dag07]) == False"
        Me.FormattingRuleRegel07.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleRegel07.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.FormattingRuleRegel07.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleRegel07.Name = "FormattingRuleRegel07"
        '
        'FormattingRuleTechnieker07
        '
        Me.FormattingRuleTechnieker07.Condition = "[Groep] == 'Techniekers'  And IsNull([Dag07]) == False"
        Me.FormattingRuleTechnieker07.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleTechnieker07.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleTechnieker07.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleTechnieker07.Name = "FormattingRuleTechnieker07"
        '
        'FormattingRuleZ07
        '
        Me.FormattingRuleZ07.Condition = "Substring([Dag07],0,1 ) == 'Z'"
        Me.FormattingRuleZ07.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleZ07.Formatting.BackColor = System.Drawing.Color.Gainsboro
        Me.FormattingRuleZ07.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleZ07.Name = "FormattingRuleZ07"
        '
        'FormattingRuleAfwezig07
        '
        Me.FormattingRuleAfwezig07.Condition = "[Dag07] == 'Afwezig'"
        Me.FormattingRuleAfwezig07.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleAfwezig07.Formatting.BackColor = System.Drawing.Color.Red
        Me.FormattingRuleAfwezig07.Formatting.ForeColor = System.Drawing.Color.White
        Me.FormattingRuleAfwezig07.Name = "FormattingRuleAfwezig07"
        '
        'XrTableCell9
        '
        Me.XrTableCell9.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "prRapportPlanning.Dag08")})
        Me.XrTableCell9.Dpi = 254.0!
        Me.XrTableCell9.FormattingRules.Add(Me.FormattingRuleCoordinatie08)
        Me.XrTableCell9.FormattingRules.Add(Me.FormattingRuleFieldC08)
        Me.XrTableCell9.FormattingRules.Add(Me.FormattingRuleMagazijn08)
        Me.XrTableCell9.FormattingRules.Add(Me.FormattingRuleRegel08)
        Me.XrTableCell9.FormattingRules.Add(Me.FormattingRuleTechnieker08)
        Me.XrTableCell9.FormattingRules.Add(Me.FormattingRuleZ08)
        Me.XrTableCell9.FormattingRules.Add(Me.FormattingRuleAfwezig08)
        Me.XrTableCell9.Name = "XrTableCell9"
        Me.XrTableCell9.Weight = 0.87556581548675227R
        '
        'FormattingRuleCoordinatie08
        '
        Me.FormattingRuleCoordinatie08.Condition = "[Groep] == 'Coordinatie'  And IsNull([Dag08]) == False"
        Me.FormattingRuleCoordinatie08.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleCoordinatie08.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleCoordinatie08.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleCoordinatie08.Name = "FormattingRuleCoordinatie08"
        '
        'FormattingRuleFieldC08
        '
        Me.FormattingRuleFieldC08.Condition = "[Groep] == 'Field Coordinator'  And IsNull([Dag08]) == False"
        Me.FormattingRuleFieldC08.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleFieldC08.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FormattingRuleFieldC08.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleFieldC08.Name = "FormattingRuleFieldC08"
        '
        'FormattingRuleMagazijn08
        '
        Me.FormattingRuleMagazijn08.Condition = "[Groep] == 'Magazijn'  And IsNull([Dag08]) == False"
        Me.FormattingRuleMagazijn08.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleMagazijn08.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormattingRuleMagazijn08.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleMagazijn08.Name = "FormattingRuleMagazijn08"
        '
        'FormattingRuleRegel08
        '
        Me.FormattingRuleRegel08.Condition = "[Groep] == 'Regel/Meet technieker'  And IsNull([Dag08]) == False"
        Me.FormattingRuleRegel08.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleRegel08.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.FormattingRuleRegel08.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleRegel08.Name = "FormattingRuleRegel08"
        '
        'FormattingRuleTechnieker08
        '
        Me.FormattingRuleTechnieker08.Condition = "[Groep] == 'Techniekers'  And IsNull([Dag08]) == False"
        Me.FormattingRuleTechnieker08.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleTechnieker08.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleTechnieker08.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleTechnieker08.Name = "FormattingRuleTechnieker08"
        '
        'FormattingRuleZ08
        '
        Me.FormattingRuleZ08.Condition = "Substring([Dag08],0,1 ) == 'Z'"
        Me.FormattingRuleZ08.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleZ08.Formatting.BackColor = System.Drawing.Color.Gainsboro
        Me.FormattingRuleZ08.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleZ08.Name = "FormattingRuleZ08"
        '
        'FormattingRuleAfwezig08
        '
        Me.FormattingRuleAfwezig08.Condition = "[Dag08] == 'Afwezig'"
        Me.FormattingRuleAfwezig08.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleAfwezig08.Formatting.BackColor = System.Drawing.Color.Red
        Me.FormattingRuleAfwezig08.Formatting.ForeColor = System.Drawing.Color.White
        Me.FormattingRuleAfwezig08.Name = "FormattingRuleAfwezig08"
        '
        'XrTableCell11
        '
        Me.XrTableCell11.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "prRapportPlanning.Dag09")})
        Me.XrTableCell11.Dpi = 254.0!
        Me.XrTableCell11.FormattingRules.Add(Me.FormattingRuleCoordinatie09)
        Me.XrTableCell11.FormattingRules.Add(Me.FormattingRuleFieldC09)
        Me.XrTableCell11.FormattingRules.Add(Me.FormattingRuleMagazijn09)
        Me.XrTableCell11.FormattingRules.Add(Me.FormattingRuleRegel09)
        Me.XrTableCell11.FormattingRules.Add(Me.FormattingRuleTechnieker09)
        Me.XrTableCell11.FormattingRules.Add(Me.FormattingRuleZ09)
        Me.XrTableCell11.FormattingRules.Add(Me.FormattingRuleAfwezig09)
        Me.XrTableCell11.Name = "XrTableCell11"
        Me.XrTableCell11.Weight = 0.8755658154867525R
        '
        'FormattingRuleCoordinatie09
        '
        Me.FormattingRuleCoordinatie09.Condition = "[Groep] == 'Coordinatie'  And IsNull([Dag09]) == False"
        Me.FormattingRuleCoordinatie09.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleCoordinatie09.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleCoordinatie09.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleCoordinatie09.Name = "FormattingRuleCoordinatie09"
        '
        'FormattingRuleFieldC09
        '
        Me.FormattingRuleFieldC09.Condition = "[Groep] == 'Field Coordinator'  And IsNull([Dag09]) == False"
        Me.FormattingRuleFieldC09.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleFieldC09.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FormattingRuleFieldC09.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleFieldC09.Name = "FormattingRuleFieldC09"
        '
        'FormattingRuleMagazijn09
        '
        Me.FormattingRuleMagazijn09.Condition = "[Groep] == 'Magazijn'  And IsNull([Dag09]) == False"
        Me.FormattingRuleMagazijn09.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleMagazijn09.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormattingRuleMagazijn09.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleMagazijn09.Name = "FormattingRuleMagazijn09"
        '
        'FormattingRuleRegel09
        '
        Me.FormattingRuleRegel09.Condition = "[Groep] == 'Regel/Meet technieker'  And IsNull([Dag09]) == False"
        Me.FormattingRuleRegel09.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleRegel09.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.FormattingRuleRegel09.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleRegel09.Name = "FormattingRuleRegel09"
        '
        'FormattingRuleTechnieker09
        '
        Me.FormattingRuleTechnieker09.Condition = "[Groep] == 'Techniekers'  And IsNull([Dag09]) == False"
        Me.FormattingRuleTechnieker09.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleTechnieker09.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleTechnieker09.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleTechnieker09.Name = "FormattingRuleTechnieker09"
        '
        'FormattingRuleZ09
        '
        Me.FormattingRuleZ09.Condition = "Substring([Dag09],0,1 ) == 'Z'"
        Me.FormattingRuleZ09.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleZ09.Formatting.BackColor = System.Drawing.Color.Gainsboro
        Me.FormattingRuleZ09.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleZ09.Name = "FormattingRuleZ09"
        '
        'FormattingRuleAfwezig09
        '
        Me.FormattingRuleAfwezig09.Condition = "[Dag09] == 'Afwezig'"
        Me.FormattingRuleAfwezig09.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleAfwezig09.Formatting.BackColor = System.Drawing.Color.Red
        Me.FormattingRuleAfwezig09.Formatting.ForeColor = System.Drawing.Color.White
        Me.FormattingRuleAfwezig09.Name = "FormattingRuleAfwezig09"
        '
        'XrTableCell10
        '
        Me.XrTableCell10.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell10.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "prRapportPlanning.Dag10")})
        Me.XrTableCell10.Dpi = 254.0!
        Me.XrTableCell10.FormattingRules.Add(Me.FormattingRuleCoordinatie10)
        Me.XrTableCell10.FormattingRules.Add(Me.FormattingRuleFieldC10)
        Me.XrTableCell10.FormattingRules.Add(Me.FormattingRuleMagazijn10)
        Me.XrTableCell10.FormattingRules.Add(Me.FormattingRuleRegel10)
        Me.XrTableCell10.FormattingRules.Add(Me.FormattingRuleTechnieker10)
        Me.XrTableCell10.FormattingRules.Add(Me.FormattingRuleZ10)
        Me.XrTableCell10.FormattingRules.Add(Me.FormattingRuleAfwezig10)
        Me.XrTableCell10.Name = "XrTableCell10"
        Me.XrTableCell10.StylePriority.UseBorders = False
        Me.XrTableCell10.Weight = 0.8755656874002512R
        '
        'FormattingRuleCoordinatie10
        '
        Me.FormattingRuleCoordinatie10.Condition = "[Groep] == 'Coordinatie'  And IsNull([Dag10]) == False"
        Me.FormattingRuleCoordinatie10.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleCoordinatie10.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleCoordinatie10.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleCoordinatie10.Name = "FormattingRuleCoordinatie10"
        '
        'FormattingRuleFieldC10
        '
        Me.FormattingRuleFieldC10.Condition = "[Groep] == 'Field Coordinator'  And IsNull([Dag10]) == False"
        Me.FormattingRuleFieldC10.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleFieldC10.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FormattingRuleFieldC10.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleFieldC10.Name = "FormattingRuleFieldC10"
        '
        'FormattingRuleMagazijn10
        '
        Me.FormattingRuleMagazijn10.Condition = "[Groep] == 'Magazijn'  And IsNull([Dag10]) == False"
        Me.FormattingRuleMagazijn10.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleMagazijn10.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormattingRuleMagazijn10.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleMagazijn10.Name = "FormattingRuleMagazijn10"
        '
        'FormattingRuleRegel10
        '
        Me.FormattingRuleRegel10.Condition = "[Groep] == 'Regel/Meet technieker'  And IsNull([Dag10]) == False"
        Me.FormattingRuleRegel10.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleRegel10.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.FormattingRuleRegel10.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleRegel10.Name = "FormattingRuleRegel10"
        '
        'FormattingRuleTechnieker10
        '
        Me.FormattingRuleTechnieker10.Condition = "[Groep] == 'Techniekers'  And IsNull([Dag10]) == False"
        Me.FormattingRuleTechnieker10.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleTechnieker10.Formatting.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.FormattingRuleTechnieker10.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleTechnieker10.Name = "FormattingRuleTechnieker10"
        '
        'FormattingRuleZ10
        '
        Me.FormattingRuleZ10.Condition = "Substring([Dag10],0,1 ) == 'Z'"
        Me.FormattingRuleZ10.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleZ10.Formatting.BackColor = System.Drawing.Color.Gainsboro
        Me.FormattingRuleZ10.Formatting.ForeColor = System.Drawing.Color.Black
        Me.FormattingRuleZ10.Name = "FormattingRuleZ10"
        '
        'FormattingRuleAfwezig10
        '
        Me.FormattingRuleAfwezig10.Condition = "[Dag10] == 'Afwezig'"
        Me.FormattingRuleAfwezig10.DataMember = "prRapportPlanning"
        '
        '
        '
        Me.FormattingRuleAfwezig10.Formatting.BackColor = System.Drawing.Color.Red
        Me.FormattingRuleAfwezig10.Formatting.ForeColor = System.Drawing.Color.White
        Me.FormattingRuleAfwezig10.Name = "FormattingRuleAfwezig10"
        '
        'TopMargin
        '
        Me.TopMargin.Dpi = 254.0!
        Me.TopMargin.HeightF = 120.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 254.0!
        Me.BottomMargin.HeightF = 120.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'SqlDataSource1
        '
        Me.SqlDataSource1.ConnectionName = "TelecomConnectionString"
        Me.SqlDataSource1.Name = "SqlDataSource1"
        StoredProcQuery1.Name = "prRapportPlanning"
        QueryParameter1.Name = "@gebruiker"
        QueryParameter1.Type = GetType(DevExpress.DataAccess.Expression)
        QueryParameter1.Value = New DevExpress.DataAccess.Expression("[Parameters.Parameter1]", GetType(Integer))
        StoredProcQuery1.Parameters.Add(QueryParameter1)
        StoredProcQuery1.StoredProcName = "prRapportPlanning"
        Me.SqlDataSource1.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {StoredProcQuery1})
        Me.SqlDataSource1.ResultSchemaSerializable = resources.GetString("SqlDataSource1.ResultSchemaSerializable")
        '
        'PageFooterBand1
        '
        Me.PageFooterBand1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo2, Me.XrPageInfo1, Me.XrLabel1, Me.XrLine2})
        Me.PageFooterBand1.Dpi = 254.0!
        Me.PageFooterBand1.HeightF = 74.0!
        Me.PageFooterBand1.Name = "PageFooterBand1"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2, Me.XrLine1, Me.XrPictureBox1, Me.lblTitel, Me.XrPictureBoxTelenetLogo})
        Me.PageHeader.Dpi = 254.0!
        Me.PageHeader.HeightF = 250.169998!
        Me.PageHeader.Name = "PageHeader"
        '
        'XrTable2
        '
        Me.XrTable2.Dpi = 254.0!
        Me.XrTable2.Font = New System.Drawing.Font("Calibri", 8.0!)
        Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 186.669998!)
        Me.XrTable2.Name = "XrTable2"
        Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
        Me.XrTable2.SizeF = New System.Drawing.SizeF(2727.36499!, 63.5!)
        Me.XrTable2.StyleName = "XrControlStyleHoofding"
        Me.XrTable2.StylePriority.UseFont = False
        Me.XrTable2.StylePriority.UseTextAlignment = False
        Me.XrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell12, Me.Dag01, Me.Dag02, Me.Dag03, Me.Dag04, Me.Dag05, Me.Dag06, Me.Dag07, Me.Dag08, Me.Dag09, Me.Dag10})
        Me.XrTableRow2.Dpi = 254.0!
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 1.0R
        '
        'XrTableCell12
        '
        Me.XrTableCell12.Dpi = 254.0!
        Me.XrTableCell12.Name = "XrTableCell12"
        Me.XrTableCell12.StylePriority.UseTextAlignment = False
        Me.XrTableCell12.Text = "Werknemer"
        Me.XrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell12.Weight = 1.4161754799313169R
        '
        'Dag01
        '
        Me.Dag01.Dpi = 254.0!
        Me.Dag01.Name = "Dag01"
        Me.Dag01.Text = "Dag01"
        Me.Dag01.Weight = 0.87556578190059997R
        '
        'Dag02
        '
        Me.Dag02.Dpi = 254.0!
        Me.Dag02.Name = "Dag02"
        Me.Dag02.Text = "Dag02"
        Me.Dag02.Weight = 0.87556581048366366R
        '
        'Dag03
        '
        Me.Dag03.Dpi = 254.0!
        Me.Dag03.Name = "Dag03"
        Me.Dag03.Weight = 0.8755658154867525R
        '
        'Dag04
        '
        Me.Dag04.Dpi = 254.0!
        Me.Dag04.Name = "Dag04"
        Me.Dag04.Weight = 0.87556581548675239R
        '
        'Dag05
        '
        Me.Dag05.Dpi = 254.0!
        Me.Dag05.Name = "Dag05"
        Me.Dag05.Weight = 0.8755658154867525R
        '
        'Dag06
        '
        Me.Dag06.Dpi = 254.0!
        Me.Dag06.Name = "Dag06"
        Me.Dag06.Weight = 0.87556581548675272R
        '
        'Dag07
        '
        Me.Dag07.Dpi = 254.0!
        Me.Dag07.Name = "Dag07"
        Me.Dag07.Weight = 0.8755658154867525R
        '
        'Dag08
        '
        Me.Dag08.Dpi = 254.0!
        Me.Dag08.Name = "Dag08"
        Me.Dag08.Weight = 0.87556581548675227R
        '
        'Dag09
        '
        Me.Dag09.Dpi = 254.0!
        Me.Dag09.Name = "Dag09"
        Me.Dag09.Weight = 0.8755658154867525R
        '
        'Dag10
        '
        Me.Dag10.Dpi = 254.0!
        Me.Dag10.Name = "Dag10"
        Me.Dag10.Weight = 0.8755656874002512R
        '
        'XrLine1
        '
        Me.XrLine1.Dpi = 254.0!
        Me.XrLine1.ForeColor = System.Drawing.Color.DarkGray
        Me.XrLine1.LineWidth = 3
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 160.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(2727.36499!, 26.6699791!)
        Me.XrLine1.StylePriority.UseForeColor = False
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Dpi = 254.0!
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(300.0!, 160.0!)
        Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'lblTitel
        '
        Me.lblTitel.Dpi = 254.0!
        Me.lblTitel.Font = New System.Drawing.Font("Calibri", 16.0!)
        Me.lblTitel.LocationFloat = New DevExpress.Utils.PointFloat(335.385895!, 71.0999908!)
        Me.lblTitel.Name = "lblTitel"
        Me.lblTitel.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.lblTitel.SizeF = New System.Drawing.SizeF(1694.604!, 88.8999786!)
        Me.lblTitel.StyleName = "Title"
        Me.lblTitel.StylePriority.UseFont = False
        Me.lblTitel.StylePriority.UseTextAlignment = False
        Me.lblTitel.Text = "PLANNING VAN "
        Me.lblTitel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'XrPictureBoxTelenetLogo
        '
        Me.XrPictureBoxTelenetLogo.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrPictureBoxTelenetLogo.Dpi = 254.0!
        Me.XrPictureBoxTelenetLogo.LocationFloat = New DevExpress.Utils.PointFloat(2534.23999!, 0.0!)
        Me.XrPictureBoxTelenetLogo.Name = "XrPictureBoxTelenetLogo"
        Me.XrPictureBoxTelenetLogo.SizeF = New System.Drawing.SizeF(193.125198!, 160.0!)
        Me.XrPictureBoxTelenetLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        Me.XrPictureBoxTelenetLogo.StylePriority.UseBorders = False
        '
        'Title
        '
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.BorderColor = System.Drawing.Color.Black
        Me.Title.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.Title.BorderWidth = 1.0!
        Me.Title.Font = New System.Drawing.Font("Times New Roman", 21.0!)
        Me.Title.ForeColor = System.Drawing.Color.Black
        Me.Title.Name = "Title"
        '
        'FieldCaption
        '
        Me.FieldCaption.BackColor = System.Drawing.Color.Transparent
        Me.FieldCaption.BorderColor = System.Drawing.Color.Black
        Me.FieldCaption.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.FieldCaption.BorderWidth = 1.0!
        Me.FieldCaption.Font = New System.Drawing.Font("Times New Roman", 10.0!)
        Me.FieldCaption.ForeColor = System.Drawing.Color.Black
        Me.FieldCaption.Name = "FieldCaption"
        '
        'PageInfo
        '
        Me.PageInfo.BackColor = System.Drawing.Color.Transparent
        Me.PageInfo.BorderColor = System.Drawing.Color.Black
        Me.PageInfo.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.PageInfo.BorderWidth = 1.0!
        Me.PageInfo.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.PageInfo.ForeColor = System.Drawing.Color.Black
        Me.PageInfo.Name = "PageInfo"
        '
        'DataField
        '
        Me.DataField.BackColor = System.Drawing.Color.Transparent
        Me.DataField.BorderColor = System.Drawing.Color.Black
        Me.DataField.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.DataField.BorderWidth = 1.0!
        Me.DataField.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.DataField.ForeColor = System.Drawing.Color.Black
        Me.DataField.Name = "DataField"
        Me.DataField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        '
        'XrControlStyleHoofding
        '
        Me.XrControlStyleHoofding.BackColor = System.Drawing.Color.FromArgb(CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer), CType(CType(102, Byte), Integer))
        Me.XrControlStyleHoofding.BorderColor = System.Drawing.Color.White
        Me.XrControlStyleHoofding.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top), DevExpress.XtraPrinting.BorderSide)
        Me.XrControlStyleHoofding.BorderWidth = 1.0!
        Me.XrControlStyleHoofding.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrControlStyleHoofding.ForeColor = System.Drawing.Color.White
        Me.XrControlStyleHoofding.Name = "XrControlStyleHoofding"
        Me.XrControlStyleHoofding.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        '
        'XrControlStyle1
        '
        Me.XrControlStyle1.BorderColor = System.Drawing.Color.Gray
        Me.XrControlStyle1.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrControlStyle1.BorderWidth = 1.0!
        Me.XrControlStyle1.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrControlStyle1.Name = "XrControlStyle1"
        Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        '
        'Parameter1
        '
        Me.Parameter1.Description = "gebruiker"
        Me.Parameter1.Name = "Parameter1"
        Me.Parameter1.Type = GetType(Integer)
        Me.Parameter1.ValueInfo = "0"
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.Dpi = 254.0!
        Me.XrPageInfo2.Font = New System.Drawing.Font("Calibri", 8.0!)
        Me.XrPageInfo2.Format = "Pagina {0} van {1}"
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(2412.22998!, 24.9999294!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(318.769989!, 45.1051292!)
        Me.XrPageInfo2.StyleName = "PageInfo"
        Me.XrPageInfo2.StylePriority.UseFont = False
        Me.XrPageInfo2.StylePriority.UseTextAlignment = False
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Dpi = 254.0!
        Me.XrPageInfo1.Font = New System.Drawing.Font("Calibri", 8.0!)
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(1.0!, 24.9999294!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(411.374207!, 45.1051292!)
        Me.XrPageInfo1.StyleName = "PageInfo"
        Me.XrPageInfo1.StylePriority.UseFont = False
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Dpi = 254.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Calibri", 8.0!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(930.770386!, 24.9999294!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(915.458313!, 45.1051292!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = " "
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 254.0!
        Me.XrLine2.LineWidth = 3
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(1.0!, 3.89494705!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(2730.0!, 8.37337303!)
        '
        'rapportPlanning
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageFooterBand1, Me.PageHeader})
        Me.ComponentStorage.Add(Me.SqlDataSource1)
        Me.DataMember = "prRapportPlanning"
        Me.DataSource = Me.SqlDataSource1
        Me.Dpi = 254.0!
        Me.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.FormattingRuleAfwezig01, Me.FormattingRuleAfwezig02, Me.FormattingRuleAfwezig03, Me.FormattingRuleAfwezig04, Me.FormattingRuleAfwezig05, Me.FormattingRuleAfwezig06, Me.FormattingRuleAfwezig07, Me.FormattingRuleAfwezig08, Me.FormattingRuleAfwezig09, Me.FormattingRuleAfwezig10, Me.FormattingRuleZ01, Me.FormattingRuleZ02, Me.FormattingRuleCoordinatie01, Me.FormattingRuleZ03, Me.FormattingRuleZ04, Me.FormattingRuleZ05, Me.FormattingRuleZ06, Me.FormattingRuleZ07, Me.FormattingRuleZ08, Me.FormattingRuleZ09, Me.FormattingRuleZ10, Me.FormattingRuleCoordinatie02, Me.FormattingRuleCoordinatie03, Me.FormattingRuleCoordinatie04, Me.FormattingRuleCoordinatie05, Me.FormattingRuleCoordinatie06, Me.FormattingRuleCoordinatie07, Me.FormattingRuleCoordinatie08, Me.FormattingRuleCoordinatie09, Me.FormattingRuleCoordinatie10, Me.FormattingRuleTechnieker01, Me.FormattingRuleTechnieker02, Me.FormattingRuleTechnieker03, Me.FormattingRuleTechnieker04, Me.FormattingRuleTechnieker05, Me.FormattingRuleTechnieker06, Me.FormattingRuleTechnieker07, Me.FormattingRuleTechnieker08, Me.FormattingRuleTechnieker09, Me.FormattingRuleTechnieker10, Me.FormattingRuleMagazijn01, Me.FormattingRuleMagazijn02, Me.FormattingRuleMagazijn03, Me.FormattingRuleMagazijn04, Me.FormattingRuleMagazijn05, Me.FormattingRuleMagazijn06, Me.FormattingRuleMagazijn07, Me.FormattingRuleMagazijn08, Me.FormattingRuleMagazijn09, Me.FormattingRuleMagazijn10, Me.FormattingRuleRegel01, Me.FormattingRuleRegel02, Me.FormattingRuleRegel03, Me.FormattingRuleRegel04, Me.FormattingRuleRegel05, Me.FormattingRuleRegel06, Me.FormattingRuleRegel07, Me.FormattingRuleRegel08, Me.FormattingRuleRegel09, Me.FormattingRuleRegel10, Me.FormattingRuleFieldC01, Me.FormattingRuleFieldC02, Me.FormattingRuleFieldC03, Me.FormattingRuleFieldC04, Me.FormattingRuleFieldC05, Me.FormattingRuleFieldC06, Me.FormattingRuleFieldC07, Me.FormattingRuleFieldC08, Me.FormattingRuleFieldC09, Me.FormattingRuleFieldC10, Me.FormattingRuleCoordinatie, Me.FormattingRuleFieldC, Me.FormattingRuleMagazijn, Me.FormattingRuleRegel, Me.FormattingRuleTechnieker})
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(119, 119, 120, 120)
        Me.PageHeight = 2100
        Me.PageWidth = 2970
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.Parameter1})
        Me.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.Title, Me.FieldCaption, Me.PageInfo, Me.DataField, Me.XrControlStyleHoofding, Me.XrControlStyle1})
        Me.Version = "14.2"
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents SqlDataSource1 As DevExpress.DataAccess.Sql.SqlDataSource
    Friend WithEvents PageFooterBand1 As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents lblTitel As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBoxTelenetLogo As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell11 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents FormattingRuleAfwezig01 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleAfwezig02 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleAfwezig03 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleAfwezig04 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleAfwezig05 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleAfwezig06 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleAfwezig07 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleAfwezig08 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleAfwezig09 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleAfwezig10 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Dag01 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Dag02 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Dag03 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Dag04 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Dag05 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Dag06 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Dag07 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Dag08 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Dag09 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Dag10 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents Title As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents FieldCaption As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents PageInfo As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents DataField As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrControlStyleHoofding As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents FormattingRuleZ01 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleZ02 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleZ03 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleZ04 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleZ05 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleZ06 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleZ07 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleZ08 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleZ09 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleZ10 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleCoordinatie01 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleCoordinatie02 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleCoordinatie03 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleCoordinatie04 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleCoordinatie05 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleCoordinatie06 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleCoordinatie07 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleCoordinatie08 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleCoordinatie09 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleCoordinatie10 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleTechnieker01 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleTechnieker02 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleTechnieker03 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleTechnieker04 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleTechnieker05 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleTechnieker06 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleTechnieker07 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleTechnieker08 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleTechnieker09 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleTechnieker10 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleFieldC01 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleFieldC02 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleFieldC03 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleFieldC04 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleFieldC05 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleFieldC06 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleFieldC07 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleFieldC08 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleFieldC09 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleFieldC10 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleRegel01 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleRegel02 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleRegel03 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleRegel04 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleRegel05 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleRegel06 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleRegel07 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleRegel08 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleRegel09 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleRegel10 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleMagazijn01 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleMagazijn02 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleMagazijn03 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleMagazijn04 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleMagazijn05 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleMagazijn06 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleMagazijn07 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleMagazijn08 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleMagazijn09 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleMagazijn10 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleCoordinatie As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleFieldC As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleMagazijn As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleRegel As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents FormattingRuleTechnieker As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents Parameter1 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
End Class
