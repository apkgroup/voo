﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Transfer.aspx.vb" Inherits="Telecom.Transfer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .row {
    float:left;
    width: 96%;
    margin-right: 10px;
    height: 115px;
    overflow: auto;
    position: static;
}

td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
.fixed {    
    position: fixed;   
    top:0px;
    background-color: #ffffff ! important; 
}
    </style>
     
    <script type="text/javascript">
      
        function SetPCVisible(value) {
            var popupControl = GetPopupControl();
            if (value) {
                popupControl.Show();
            }
            else {
                popupControl.Hide();
            }
        }

        function GetPopupControl() {
            return popup;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:right; margin-bottom:10px;margin-left:10px;"></div><h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
    <p>
        <asp:Literal ID="Literal3" runat="server"></asp:Literal>
    </p>
    <p>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="select id, naam from opdrachtgever where id not like @opdrachtgever and id in (select opdrachtgeverId from AdminOpdrachtgevers where gebruikerId = @gebruikerId )">
            <SelectParameters>
                <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" />
                <asp:SessionParameter Name="gebruikerId" SessionField="userid" />
            </SelectParameters>
        </asp:SqlDataSource>
        <dx:ASPxComboBox ID="ASPxComboBox1" runat="server" DataSourceID="SqlDataSource1" TextField="naam" ValueField="id">
        </dx:ASPxComboBox>
    </p>
    <table>
        <tr>
            <td>
                  <dx:ASPxButton ID="ASPxButton1" runat="server" Text="ASPxButton">
    </dx:ASPxButton>
            </td>
            <td>
                  <dx:ASPxButton ID="ASPxButton2" runat="server" Text="ASPxButton">
    </dx:ASPxButton>
            </td>
        </tr>
    </table>
  
  
    
    <br />
    </asp:Content>
