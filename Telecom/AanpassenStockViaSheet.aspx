﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="AanpassenStockViaSheet.aspx.vb" Inherits="Telecom.AanpassenStockViaSheet" %>

<%@ Register assembly="DevExpress.Web.ASPxSpreadsheet.v19.2, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpreadsheet" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #TextArea1 {
            height: 51px;
            width: 102px;
        }


        .auto-style1 {
            width: 48px;
        }

        .auto-style2 {
            height: 30px;
        }

        </style>
    <script>
        var currentRowIndex = undefined;
        var currentRowIndex2 = undefined;

    </script>
    <script type="text/javascript">




    

        window.onload = function () {
          
           console.log("onload");
           $("input[name*='Serienummer']").each(function () {
               console.log(this.id);
               if (this.id.split("_").pop() != document.getElementById("ctl00_ContentPlaceHolder1_ASPxSpinEditAantal_I").value) {
                   this.removeAttribute("onchange");
               }
               else {
                   this.focus();
               }

               if (document.getElementById("ctl00_ContentPlaceHolder1_ASPxSpinEditAantal_I").value == 0) {
                   console.log("Het is 0!");
                   document.getElementById("ctl00_ContentPlaceHolder1_PanelSerie").style.display = "none";
               }

           });


           $('input').keypress(function (e) {
               console.log("KEYPRESS");
               if (e.which == 13) {
                   e.preventDefault();
                   console.log("ISENTER");
                   var self = $(this)
                   var form = self.parents('form:eq(0)');
                   var focusable;
                   var next;
                   var prev;

                   focusable = form.find('input,a,select,button,textarea').filter(':visible');
                   next = focusable.eq(focusable.index(this) + 1);
                   if (next.length) {
                       next.focus();
                   } else {

                   }
                   return false;

               }
           });

           ASPxGridView2.Refresh();
           var i;

           $('input').keypress(function (e) {
               console.log("KEYPRESS");
               if (e.which == 13) {
                   e.preventDefault();
                   console.log("ISENTER");
                   var self = $(this)
                   var form = self.parents('form:eq(0)');
                   var focusable;
                   var next;
                   var prev;

                   focusable = form.find('input,a,select,button,textarea').filter(':visible');
                   next = focusable.eq(focusable.index(this) + 1);
                   if (next.length) {
                       next.focus();
                   } else {

                   }
                   return false;

               }
           });
       };

       var timerHandle = -1;
       var indexrow
       function OnBatchEditStartEditing(s, e) {
           currentRowIndex = e.visibleIndex;
           currentRowIndex2 = e.visibleIndex;
           clearTimeout(timerHandle);
           var templateColumn = s.GetColumnByField("Nombre");
       }

       function OnEndCallBack(s, e) {
           if (s.cpIsUpdated != '') {


               clientText.GetMainElement().style.display = 'block';
               console.log('Waarde komt uit');
               //console.log(e); 
               //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);
               console.log('Temps de Regie updated! Waarde is...');
               //console.log(e); 
               //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);

               console.log(clientButton);
               //console.log(Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML));
               if (ASPxGridView1.GetRow(indexrow).children[3].firstElementChild == null) {
                   if (Number(ASPxGridView1.GetRow(indexrow).children[3].innerHTML.replace(",", ".")) > 0) {
                       console.log("enabled=false");
                       clientButton.SetEnabled(false);
                       clientLabel2.GetMainElement().style.display = 'block'
                   }
                   else {
                       console.log("enabled=true");
                       clientButton.SetEnabled(true);
                       clientLabel2.GetMainElement().style.display = 'none'
                   }
               }
               else {
                   if (Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML.replace(",", ".")) > 0) {
                       console.log("enabled=false");
                       clientButton.SetEnabled(false);
                       clientLabel2.GetMainElement().style.display = 'block'
                   }
                   else {
                       console.log("enabled=true");
                       clientButton.SetEnabled(true);
                       clientLabel2.GetMainElement().style.display = 'none'
                   }
               }


               //console.log(s.cpIsUpdated);


           }
           else {
               clientText.SetText('');
               console.log('Anders updated');
           }
       }

       function OnKeyDown(s, e) {
           var key = e.htmlEvent.keyCode;
           console.log("key pressed!");
           if (key == 38 || key == 40) {
               console.log("Up or down key pressed!");
               ASPxClientUtils.PreventEvent(e.htmlEvent);
               ASPxGridView1.batchEditApi.EndEdit();
               console.log(currentRowIndex);
               if (currentRowIndex !== undefined && currentRowIndex !== null) {
                   var visibleIndices = ASPxGridView1.batchEditApi.GetRowVisibleIndices();
                   var indicesArrayLength = visibleIndices.length;
                   var rowIndex = visibleIndices.indexOf(currentRowIndex);
                   if (!(rowIndex === 0 && key === 38) || !(rowIndex === indicesArrayLength - 1 && key === 40)) {
                       switch (key) {
                           case 38:
                               ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex - 1], 4); // Adjust the second argument according
                               // to the 'Nombre' column index
                               break;
                           case 40:
                               ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex + 1], 4); // Adjust the second argument according
                               // to the 'Nombre' column index
                               break;
                           default:
                               break;
                       }
                   }
               }
           }
       }

       function OnKeyDownMat(s, e) {
           var key = e.htmlEvent.keyCode;
           console.log("key pressed!");
           if (key == 38 || key == 40) {
               console.log("Up or down key pressed");
               ASPxClientUtils.PreventEvent(e.htmlEvent);
               ASPxGridView2.batchEditApi.EndEdit();
               console.log(currentRowIndex2);
               if (currentRowIndex2 !== undefined && currentRowIndex2 !== null) {
                   var visibleIndices2 = ASPxGridView2.batchEditApi.GetRowVisibleIndices();
                   var indicesArrayLength2 = visibleIndices2.length;
                   var rowIndex2 = visibleIndices2.indexOf(currentRowIndex2);
                   if (!(rowIndex2 === 0 && key === 38) || !(rowIndex2 === indicesArrayLength2 - 1 && key === 40)) {
                       console.log("ERIN");
                       switch (key) {
                           case 38:
                               ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 - 1], 4); // Adjust the second argument according
                               // to the 'Nombre' column index
                               break;
                           case 40:
                               ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 + 1], 4); // Adjust the second argument according
                               // to the 'Nombre' column index
                               break;
                           default:
                               break;
                       }
                   }
               }
           }
       }



       function OnBatchEditEndEditing(s, e) {
           timerHandle = setTimeout(function () {
               s.UpdateEdit();
           }, 50);

           indexrow = e.visibleIndex;
           console.log("Index row set to " + indexrow);
           //if (s.cpIsUpdated != '') {

           //    console.log(clientText.GetMainElement());
           //    console.log(clientText);
           //    clientText.GetMainElement().style.display = 'none';
           //    console.log('Temps de Regie updated! Waarde is...');
           //    console.log(e);
           //    //console.log(ASPxGridView1.GetRow(e.visibleIndex).children[3].innerHTML);
           //    console.log(clientButton);
           //    clientButton.setEnabled(true);
           //    console.log(s.cpIsUpdated);


           //}
           //else {
           //    clientText.SetText('');
           //    console.log('Anders updated');
           //}

       }

       function saveChangesBtn_Click(s, e) {
           ASPxGridView1.UpdateEdit();
           ASPxGridView2.UpdateEdit();
       }

       function cancelChangesBtn_Click(s, e) {
           ASPxGridView1.CancelEdit();
           ASPxGridView2.CancelEdit();
       }




       function saveChangesBtnMat_Click(s, e) {
           ASPxGridView2.UpdateEdit();
       }

       function cancelChangesBtnMat_Click(s, e) {
           ASPxGridView2.CancelEdit();



       }


       function insertArticle() {
           console.log("IETest");
           Callback1.PerformCallback();
           console.log("IEAfterCallback");
       }



       function OnCallbackComplete(s, e) {
           console.log("cbCOMPLET");
           ASPxGridView2.Refresh();
           //location.reload();

       }




    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>&nbsp;Stock aanpassen via sheet</h2>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId and (g.Actief = 1 or hoofd=1 or type is not null) )">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <p>
        &nbsp; 
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </p>
    <p>
        <dx:ASPxButton ID="ASPxButton1" ClientInstanceName="ASPxButton1" runat="server" Text="Inlezen" AutoPostBack="False">
            <ClientSideEvents Click="function(s, e) {
                
                ASPxSpreadsheet1.ApplyCellEdit()
	ASPxCallback1.PerformCallback(); 
}" />
        </dx:ASPxButton>
        <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="ASPxCallback1"></dx:ASPxCallback>
    </p>
    <p>
        <dx:ASPxSpreadsheet ID="ASPxSpreadsheet1" ClientInstanceName="ASPxSpreadsheet1" runat="server" RibbonMode="None" ShowFormulaBar="False" ShowSheetTabs="False" WorkDirectory="~/App_Data/WorkDirectory">
            <SettingsDocumentSelector>
                <ToolbarSettings Visible="False">
                </ToolbarSettings>
            </SettingsDocumentSelector>
        </dx:ASPxSpreadsheet>
    </p>

    <dx:ASPxLabel ID="ASPxLabelFout" runat="server" Font-Bold="True" ForeColor="#CC0000" EncodeHtml="False">
    </dx:ASPxLabel>

    <p></p>













    <asp:SqlDataSource ID="SqlDataSourceArtikelen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT distinct article, description, CONCAT(article, ' | ', description) as naam, b.id FROM [Basismateriaal] b
INNER JOIN MateriaalOpdrachtgevers o ON b.id =o.materiaalId
WHERE o.opdrachtgeverId = @OpdrachtgeverId and (o.actief = 1 or o.actief is null) and (b.bobijnartikel is null or b.bobijnartikel = 0)">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" DefaultValue="1" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceMateriaux" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [VOO_Materiaal] WHERE [id] = @id" InsertCommand="INSERT INTO [VOO_Materiaal] ([Werk], [Article], [Description], [Nombre]) VALUES (@Werk, @Article, @Description, @Nombre)" SelectCommand="SELECT distinct s.[id],[naam],b.Article,b.Description, s.[MateriaalId], [MagazijnId], [Aantal] FROM [StockMagazijn] s
INNER JOIN Magazijn m on s.MagazijnId = m.id 
INNER JOIN Basismateriaal b on s.MateriaalId = b.id 
INNER JOIN  [Voo].[dbo].[MateriaalOpdrachtgevers] mo on mo.opdrachtgeverId = m.opdrachtgeverId and mo.materiaalId = b.id
WHERE ([MagazijnId] = @MagazijnId) and (mo.actief = 1 or mo.actief is null) and (b.bobijnartikel is null or b.bobijnartikel = 0)"
        UpdateCommand="UPDATE [VOO_Materiaal] SET  [Nombre] = @Nombre WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Werk" Type="Int32" />
            <asp:Parameter Name="Article" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Nombre" Type="Decimal" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Nombre" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>


    <br />
    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Serienummer] WHERE ([StockMagazijnId] = @StockMagazijnId) and (uitgeboekt &lt;&gt; 1)">
        <SelectParameters>
            <asp:SessionParameter Name="StockMagazijnId" SessionField="stockmagazijn" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceStockMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [StockMagazijn] WHERE [id] = @id" InsertCommand="INSERT INTO [StockMagazijn] ([MateriaalId], [MagazijnId], [Aantal]) VALUES (@MateriaalId, @MagazijnId, @Aantal)" SelectCommand="SELECT s.[id],[naam],b.Article,b.Description, [MateriaalId], [MagazijnId], [Aantal] FROM [StockMagazijn] s
INNER JOIN Magazijn m on s.MagazijnId = m.id 
INNER JOIN Basismateriaal b on s.MateriaalId = b.id WHERE ([MagazijnId] = @MagazijnId)"
        UpdateCommand="UPDATE [StockMagazijn] SET [MateriaalId] = @MateriaalId, [MagazijnId] = @MagazijnId, [Aantal] = @Aantal WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="MagazijnId" SessionField="MagazijnId" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    &nbsp;

</asp:Content>
