﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data
Imports DevExpress.Web



Public Class reactivate
    Inherits System.Web.UI.Page
    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)
        Dim voocontext As New VooEntities
        Dim id As String = Request.QueryString("id")
        If taalId = 1 Then
            Literal2.Text = "Serienummer heractiveren: " & id
            Literal3.Text = "Bent u zeker dat u deze serienummer terug actief wilt maken?"
            ASPxButton1.Text = "Nee"
            ASPxButton2.Text = "Ja"
        Else
            Literal2.Text = "Reset mot de passe pour " & id
            Literal3.Text = "Êtes-vous sûr de vouloir réinitialiser le mot de passe?"
            ASPxButton1.Text = "Non"
            ASPxButton2.Text = "Oui"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub

    Protected Sub ASPxButton2_Click(sender As Object, e As EventArgs) Handles ASPxButton2.Click
        Dim voocontext As New VooEntities
        Dim id As String = Request.QueryString("id")

        Dim sr = voocontext.Serienummer.Where(Function(x) x.serienummer1 = id).FirstOrDefault
        sr.uitgeboekt = False
        sr.statusId = 3
        sr.StockMagazijn.Aantal = sr.StockMagazijn.Aantal + 1


        voocontext.SaveChanges()
        voocontext.Dispose()

        Response.Redirect("~/Beheer/OverzichtSerienummers.aspx", False)
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        Response.Redirect("~/Beheer/OverzichtSerienummers.aspx", False)
    End Sub
End Class