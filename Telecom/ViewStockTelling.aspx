﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="ViewStockTelling.aspx.vb" Inherits="Telecom.ViewStockTelling" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        <script type="text/javascript">
            var timerHandle = -1;
        var indexrow
            function saveChangesBtn_Click(s, e) {
            
            ASPxGridView1.UpdateEdit();
            
            
        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();
           
        }

                        function OnBatchEditStartEditing(s, e) {
            currentRowIndex = e.visibleIndex;
            currentRowIndex2 = e.visibleIndex;
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("Hoeveelheid");
        }
        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);
            
            indexrow = e.visibleIndex;
            console.log("Index row set to " + indexrow);
        }
        
    </script>
        </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>

     <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
                </dx:ASPxLabel>

    

    <dx:ASPxLabel ID="ASPxLabelSuccess" CssClass="enableMultiLine" runat="server" EncodeHtml="false" Font-Bold="True" ForeColor="#00CC00">
                </dx:ASPxLabel>

    

    <dx:ASPxButton ID="ASPxButtonGoedkeuren" runat="server" Text="Zet stock gelijk aan telling">
    </dx:ASPxButton>

    

    <br />

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" ClientInstanceName="ASPxGridView1" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMateriauxADMIN" KeyFieldName="id">
        <SettingsDetail ShowDetailRow="True" />
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                    <SettingsDetail AllowOnlyOneMasterRowExpanded="True" />
                    <SettingsEditing Mode="Inline">
                    </SettingsEditing>
                    <Columns>
                        <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2" Caption="Serie" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
<StatusBar>
                    <dx:ASPxButton ID="saveChangesBtn" runat="server" Text="Commander" AutoPostBack="false"
                        RenderMode="Link" ClientInstanceName="saveChangesBtn" ClientEnabled="false">
                        <ClientSideEvents Click="saveChangesBtn_Click" />
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="cancelChangesBtn" runat="server" Text="Annuler" AutoPostBack="false"
                        RenderMode="Link">
                        <ClientSideEvents Click="cancelChangesBtn_Click" />
                    </dx:ASPxButton>
                
</StatusBar>
        </Templates>
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsEditing Mode="Batch">
            <BatchEditSettings StartEditAction="Click" />
        </SettingsEditing>
        <Settings ShowStatusBar="Hidden" />

                    
       
        <SettingsBehavior AllowSort="False" AllowFocusedRow="True" />

                    
       
        <SettingsSearchPanel Visible="True" />
        <ClientSideEvents BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing"/>
                    
       
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="0" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="2">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Hoeveelheid" VisibleIndex="3">
                <PropertiesTextEdit Size="100" Width="50px">
                    <MaskSettings Mask="&lt;0..9999&gt;&lt;,|.&gt;&lt;00..99&gt;" AllowMouseWheel="False" />
                </PropertiesTextEdit>
                <EditFormSettings Visible="True" />
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsAdaptivity AdaptivityMode="HideDataCells">
        </SettingsAdaptivity>
        <Templates>
                <StatusBar>
                    <dx:ASPxButton ID="saveChangesBtn" runat="server" Text="Commander" AutoPostBack="false"
                        RenderMode="Link" ClientInstanceName="saveChangesBtn" ClientEnabled="false">
                        <ClientSideEvents Click="saveChangesBtn_Click" />
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="cancelChangesBtn" runat="server" Text="Annuler" AutoPostBack="false"
                        RenderMode="Link">
                        <ClientSideEvents Click="cancelChangesBtn_Click" />
                    </dx:ASPxButton>
                </StatusBar>
            </Templates>
        <Styles>
            <SelectedRow BackColor="#FFCC99">
            </SelectedRow>
            <FocusedRow Font-Bold="True" ForeColor="Black">
            </FocusedRow>
            <InlineEditRow BackColor="#FFCC99">
            </InlineEditRow>
        </Styles>
    </dx:ASPxGridView>&nbsp;<asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT sl.[id]
      ,[materiaalId]
	  ,m.Article
	  ,m.Description
      ,[stocktellingId]
      ,[hoeveelheid] as Hoeveelheid
  FROM [Voo].[dbo].[Stocktellinglijn] sl
  inner join Basismateriaal m on sl.materiaalId = m.id
  where stocktellingId = @stocktellingId" ID="SqlDataSourceMateriauxADMIN" UpdateCommand="UPDATE [Stocktellinglijn] SET  Hoeveelheid = @Hoeveelheid WHERE [id] = @id">
        <SelectParameters>
            <asp:SessionParameter Name="stocktellingId" SessionField="StockTellingId" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Hoeveelheid" />
            <asp:Parameter Name="id" />
        </UpdateParameters>
    </asp:SqlDataSource>
        
    <br />
    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT id, serienummer  FROM [StocktellingSerienummer] WHERE [stocktellinglijnid]= @stocktellinglijnid" InsertCommand="Insert into [StocktellingSerienummer] ([stocktellinglijnid],[serienummer]) values(@stocktellinglijnid,@serienummer)" UpdateCommand="Update [StocktellingSerienummer] set serienummer=@serienummer where id=@id
">
        <InsertParameters>
            <asp:SessionParameter Name="stocktellinglijnid" SessionField="serienummerlijn" />
            <asp:Parameter Name="serienummer" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="stocktellinglijnid" SessionField="serienummerlijn" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="serienummer" />
            <asp:Parameter Name="id" />
        </UpdateParameters>
    </asp:SqlDataSource>
            
    </asp:Content>
