﻿Imports System.Data.SqlClient
Imports DevExpress.Spreadsheet
Imports DevExpress.Web

Imports EntityFramework.Extensions
Imports Microsoft.ajax

Public Class InlezenStockViaSheet
    Inherits System.Web.UI.Page
    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property

    'Protected Overrides Sub Initializeculture()
    '    SetCulture()
    '    MyBase.InitializeCulture()

    'End Sub
    'Private Sub SetCulture()
    '    Culture = Session("globalize").ToString
    '    UICulture = Session("globalize").ToString
    '    Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(Session("globalize").ToString)
    '    Threading.Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo(Session("globalize").ToString)
    'End Sub
    Protected Sub vertaal(taalid As Integer)


    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))


        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer
        Dim worksheet As Worksheet


        worksheet = ASPxSpreadsheet1.Document.Worksheets(0)
        worksheet.Cells("A1").Value = "Doosnummer"
        worksheet.Cells("B1").Value = "Artikel"
        worksheet.Cells("C1").Value = "Omschrijving"
        worksheet.Cells("D1").Value = "Hoeveelheid"
        worksheet.Cells("E1").Value = "Eenheid"
        worksheet.Cells("F1").Value = "Haspel"
        worksheet.Cells("G1").Value = "Serienummer"
        'worksheet("$A:$XFD").Protection.Locked = False
        'worksheet("A1:G1").Protection.Locked = True
        'worksheet.Protect("", WorksheetProtectionPermissions.Default)

        worksheet.Columns().AutoFit(0, 5)





        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()

        End If


        Dim opId As Integer = Session("opdrachtgever")
        If voocontext.Opdrachtgever.Find(opId).naam <> "Proximus" Then
            ASPxLabelJMS.ClientVisible = False
            ASPxTextBoxJMS.ClientVisible = False

        Else
            ASPxLabelOrdernummer.Text = "Matkab-nummer"
            ASPxLabelJMS.ClientVisible = True

            ASPxTextBoxJMS.ClientVisible = True
        End If

        voocontext.Dispose()







    End Sub




    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("stockmagazijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub


    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender

        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click

        Response.Redirect("~/Beheer/PaklijstDetails.aspx?id=" & Session("plid2"), False)


    End Sub

    Protected Sub ASPxCallback1_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback1.Callback
        Dim plid2 As Integer
        Using context As New VooEntities()

            Using transaction = context.Database.BeginTransaction()


                Dim paklijstlijnId = 0
                Try
                    If String.IsNullOrWhiteSpace(ASPxTextBox1.Value) Then
                        Throw New Exception("Ordernummer moet ingevuld zijn")
                    End If
                    Dim paklijstnummer As String = ASPxTextBox1.Value.ToString
                    If context.Paklijst.Where(Function(x) x.ordernummer = paklijstnummer).Any Then
                        Throw New Exception("Ordernummer is al in gebruik")
                    End If
                    If String.IsNullOrWhiteSpace(ASPxComboBoxMagazijn0.Value) Then
                        Throw New Exception("Voorziene locatie moet ingevuld zijn")
                    End If
                    If String.IsNullOrWhiteSpace(ASPxComboBoxMagazijn.Value) Then
                        Throw New Exception("Magazijn moet ingevuld zijn")
                    End If

                    Dim worksheet As Worksheet
                    Dim magazijnid As Integer = ASPxComboBoxMagazijn.Value
                    Dim voorzienelocatie As Integer = ASPxComboBoxMagazijn0.Value

                    worksheet = ASPxSpreadsheet1.Document.Worksheets(0)
                    Dim usedRange As CellRange = worksheet.GetUsedRange()

                    Dim paklijst As New Paklijst

                    paklijst.datumIngelezen = DateTime.Today

                    'TODO basisinfo
                    paklijst.voorzieneLocatie = voorzienelocatie
                    paklijst.ordernummer = ASPxTextBox1.Value.ToString
                    paklijst.magazijnId = magazijnid
                    paklijst.opdrachtgeverId = Session("opdrachtgever")
                    paklijst.opmerking = ASPxTextBoxOpmerking.Value



                    Dim opId As Integer = Session("opdrachtgever")
                    If context.Opdrachtgever.Find(opId).naam <> "Proximus" Then

                    Else
                        paklijst.JMSnummer = ASPxTextBoxJMS.Value

                    End If

                    context.Paklijst.Add(paklijst)

                    context.SaveChanges()
                    plid2 = paklijst.id

                    For i As Integer = 1 To usedRange.RowCount - 1
                        Dim hoeveelheidCell As Cell = usedRange(i, 3)
                        Dim OmschrijvingCell As Cell = usedRange(i, 2)
                        Dim seriecell As Cell = usedRange(i, 6)
                        Dim haspelCell As Cell = usedRange(i, 5)
                        Dim materiaalCell As Cell = usedRange(i, 1)
                        Dim test = hoeveelheidCell.Value.Type
                        Dim aantal As Decimal = hoeveelheidCell.Value.NumericValue
                        Dim Artikelnummer As String = materiaalCell.Value.ToString

                        If Not hoeveelheidCell.Value.IsEmpty Then
                            If materiaalCell.Value.IsEmpty Then
                                'foutmelding geven
                                Return
                            End If

                            Dim paklijstlijn As New PaklijstLijn
                            paklijstlijn.aantal = aantal
                            paklijstlijn.article = materiaalCell.Value.ToString.Replace(vbCr, "").Replace(vbCrLf, "").Replace(vbLf, "")
                            paklijstlijn.paklijstId = paklijst.id
                            paklijstlijn.omschrijving = OmschrijvingCell.Value.ToString.Replace(vbCr, "").Replace(vbCrLf, "").Replace(vbLf, "")
                            paklijstlijn.goedgekeurd = False
                            context.PaklijstLijn.Add(paklijstlijn)
                            context.SaveChanges()
                            paklijstlijnId = paklijstlijn.id

                            If Not haspelCell.Value.IsEmpty Then
                                paklijstlijn.bobijnnummer = haspelCell.Value.ToString
                            End If



                            Dim serie As String
                            If seriecell.Value.IsEmpty Then
                            Else
                                Dim pllsr As New PaklijstLijnSerienummer
                                pllsr.serienummer = seriecell.Value.TextValue
                                pllsr.paklijstlijnId = paklijstlijn.id
                                context.PaklijstLijnSerienummer.Add(pllsr)
                            End If




                        ElseIf hoeveelheidCell.Value.IsEmpty And Not seriecell.Value.IsEmpty Then
                            Dim pllsr As New PaklijstLijnSerienummer
                            pllsr.serienummer = seriecell.Value.ToString
                            pllsr.paklijstlijnId = paklijstlijnId
                            context.PaklijstLijnSerienummer.Add(pllsr)

                        End If
                    Next

                    For Each paklijstlijn In paklijst.PaklijstLijn.Where(Function(x) x.bobijnnummer Is Nothing)

                        Dim materiaal As Basismateriaal
                        materiaal = context.Basismateriaal.Where(Function(x) x.Article = paklijstlijn.article).FirstOrDefault
                        If materiaal Is Nothing Then
                            Continue For
                        End If
                        Dim sm = context.StockMagazijn.Where(Function(x) x.MagazijnId = magazijnid And x.MateriaalId = materiaal.id).FirstOrDefault
                        If sm Is Nothing Then
                            'foutmelding geven en in foutrapport zetten
                            Continue For

                        End If
                        sm.Aantal = sm.Aantal + paklijstlijn.aantal
                        paklijstlijn.goedgekeurd = True
                        Dim sb As New Stockbeweging
                        sb.stockmagazijnId = sm.id
                        sb.opmerking = "Stock inlezen via sheet"
                        sb.gebruiker = Session("userid")
                        sb.beweging = paklijstlijn.aantal
                        sb.datum = DateTime.Now
                        context.Stockbeweging.Add(sb)
                        context.SaveChanges()
                        For Each plserie In paklijstlijn.PaklijstLijnSerienummer
                            If context.Serienummer.Where(Function(x) x.serienummer1 = plserie.serienummer).Any Then
                                Throw New Exception("Serienummer " & plserie.serienummer & " bestaat al in systeem")
                            End If
                            Dim serie As New Serienummer
                            serie.StockMagazijnId = sm.id
                            serie.serienummer1 = plserie.serienummer
                            serie.statusId = 3
                            serie.uitgeboekt = False
                            serie.datumOntvangstMagazijn = DateTime.Today
                            serie.datumGemaakt = DateTime.Today
                            context.Serienummer.Add(serie)
                            context.SaveChanges()
                            Dim sbsr As New StockbewegingSerienummer
                            sbsr.serienummerId = serie.id
                            sbsr.stockbewegingid = sb.id
                        Next

                        context.SaveChanges()

                    Next

                    'begin
                    For Each paklijstlijn In paklijst.PaklijstLijn.Where(Function(x) x.bobijnnummer IsNot Nothing)

                        Dim materiaal As Basismateriaal
                        materiaal = context.Basismateriaal.Where(Function(x) x.Article = paklijstlijn.article).FirstOrDefault
                        If materiaal Is Nothing Then
                            Continue For
                        End If
                        Dim sm = context.StockMagazijn.Where(Function(x) x.MagazijnId = magazijnid And x.MateriaalId = materiaal.id).FirstOrDefault
                        If sm Is Nothing Then
                            ASPxLabelFout.Text = "Materiaal met artikelcode" & materiaal.Article & " niet gevonden in magazijn " & ASPxComboBoxMagazijn.Text
                            Continue For

                        End If
                        sm.Aantal = sm.Aantal + paklijstlijn.aantal
                        paklijstlijn.goedgekeurd = True

                        Dim nr As String = paklijstlijn.bobijnnummer
                        If context.Bobijn.Where(Function(x) x.bobijnnummer = nr And x.AF = False).Any Then
                            ASPxLabelFout.Text = "Er bestaat al een actieve bobijn met deze nummer"
                            Return
                        End If
                        Dim bobijn As New Bobijn
                        If String.IsNullOrEmpty(ASPxComboBoxMagazijn.Value) Then
                            Return
                        End If

                        bobijn.aantal = paklijstlijn.aantal
                        bobijn.bobijnnummer = nr
                        bobijn.AF = False

                        bobijn.plaatsingsstaatgemaakt = False
                        bobijn.paklijstnummer = paklijst.ordernummer



                        Dim magId As Integer = ASPxComboBoxMagazijn.Value
                        Dim matId As Integer = context.Basismateriaal.Where(Function(x) x.Article = paklijstlijn.article).FirstOrDefault.id
                        Dim stockmag = context.StockMagazijn.Where(Function(x) x.MagazijnId = magId And x.MateriaalId = matId).FirstOrDefault
                        bobijn.stockmagazijnId = stockmag.id
                        stockmag.Aantal = stockmag.Aantal + paklijstlijn.aantal

                        context.Bobijn.Add(bobijn)

                        Dim sb As New Stockbeweging
                        sb.gebruiker = Session("userId")
                        sb.datum = DateTime.Now
                        sb.beweging = paklijstlijn.aantal
                        sb.opmerking = "Inlezen nieuwe bobijn met nummer " & bobijn.bobijnnummer
                        sb.stockmagazijnId = stockmag.id

                        context.Stockbeweging.Add(sb)

                        context.SaveChanges()


                    Next

                    'end
                    If paklijst.magazijnId = paklijst.voorzieneLocatie Then
                        paklijst.afgehandeld = True
                        paklijst.datumVerplaatst = Today.Date

                    End If
                Catch ex As Exception
                    transaction.Rollback()
                    ASPxLabelFout.Text = "Er is een fout opgetreden bij het inlezen van de sheet: " & ex.Message
                    Return

                End Try

                'transaction.Commit()
                context.SaveChanges()

                transaction.Commit()
                Session("plid2") = plid2

            End Using
        End Using
    End Sub
End Class