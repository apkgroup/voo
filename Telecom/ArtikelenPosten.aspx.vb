﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class ArtikelenPosten
    Inherits System.Web.UI.Page





    'Protected Overrides Sub Initializeculture()
    '    SetCulture()
    '    MyBase.InitializeCulture()

    'End Sub
    'Private Sub SetCulture()
    '    Culture = Session("globalize").ToString
    '    UICulture = Session("globalize").ToString
    '    Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(Session("globalize").ToString)
    '    Threading.Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo(Session("globalize").ToString)
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ASPxGridView1.JSProperties("cpIsUpdated") = ""

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()

        End If


        buttonKlaar.Enabled = False
        clientLabel2.Text = "Remplissez le champ d'explication pour continuer (plus de 10 caractères)"

        Dim redenregie As String = ""

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "SELECT tijd, RedenRegie FROM [Voo].[dbo].[Werk] where id = @id"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()

                If dr.GetDateTime(0).Date.AddHours(24) < DateTime.Now Then
                    'visible=1 is onzichtbaar. Ik weet ook niet waarom.
                    ASPxGridView1.DataColumns.Item("Nombre").EditFormSettings.Visible = 1
                    ASPxGridView2.DataColumns.Item("Nombre").EditFormSettings.Visible = 1
                    Literal2.Text = "La période d'édition a passé. Contactez votre administrateur pour les modifications"


                End If
                If Not IsDBNull(dr.Item(1)) Then
                    redenregie = dr.GetString(1)
                    ASPxTextBoxRegie.Text = redenregie
                End If

            End If
            dr.Close()


            s_SQL = "select Nombre from [Voo].[dbo].[VOO_Posten] where werk=@werk and poste='A20000'"
            Dim cmd2 As New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@werk", SqlDbType.Int) With {.Value = Session("uitgevoerdwerk")}
            cmd2.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd2.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()
                If Not IsDBNull(dr2.Item(0)) Then
                    If dr2.GetDecimal(0) > 0 And ASPxTextBoxRegie.Text.Length < 10 Then
                        buttonKlaar.Enabled = False
                        clientLabel2.Text = "Remplissez le champ d'explication pour continuer (plus de 10 caractères)"
                        'buttonKlaar.Cursor = "Default"
                        ASPxTextBoxRegie.Visible = True
                        ASPxLabelRegie.Visible = True
                    Else
                        buttonKlaar.Enabled = True
                        clientLabel2.Text = ""
                    End If
                End If
            Else
                buttonKlaar.Enabled = True
                clientLabel2.Text = ""
            End If
            dr2.Close()

            '
            'ASPxTextBoxRegie.Visible = True
            'ASPxLabelRegie.Visible = True
            'If Not IsDBNull(dr.Item(1)) Then

            '    ASPxTextBoxRegie.Text = dr.GetString(1)

            '    buttonKlaar.Enabled = True
            'Else
            '    buttonKlaar.Enabled = False
            '    buttonKlaar.Cursor = "Default"
            'End If
            '
        End Using

        If Session("isadmin") Then
            ASPxGridView1.DataColumns.Item("Nombre").EditFormSettings.Visible = 2
            Literal2.Text = ""


        End If




    End Sub


    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()




            Dim editingRowVisibleIndex As Integer = ASPxGridView1.FindVisibleIndexByKeyValue(e.Keys.Values(0))



            If IsDBNull(ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Werk")) Then
                Try
                    If (e.NewValues(0) > 0) Then
                        Dim s_SQL As String = "Insert into VOO_Posten (Werk, Poste, Description, Nombre) VALUES (@werk, @Poste, @Description, @Nombre)"

                        Dim cmd As New SqlCommand(s_SQL, cn)
                        Dim par As New SqlParameter("@werk", SqlDbType.Int) With {.Value = Session("uitgevoerdwerk")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@Poste", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Poste").ToString()}
                        cmd.Parameters.Add(par)


                        par = New SqlParameter("@Description", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Description").ToString()}
                        cmd.Parameters.Add(par)


                        par = New SqlParameter("@Nombre", SqlDbType.Decimal) With {.Value = e.NewValues(0)}


                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    End If

                Catch ex As Exception
                    Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij update post aan postengroep door {0} {1} {2}", Session("naam"), Session("voornaam"), ex.Message)}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                End Try
            Else
                Dim s_SQL As String = "Update Voo_Posten set nombre=@nombre where id=@id"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "id").ToString()}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@Nombre", SqlDbType.Decimal) With {.Value = e.NewValues(0)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End If



        End Using




    End Sub

    Protected Sub ASPxGridView2_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView2.RowUpdating
        'Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
        '    cn.Open()
        '    Dim editingRowVisibleIndex As Integer = ASPxGridView2.FindVisibleIndexByKeyValue(e.Keys.Values(0))

        '    If IsDBNull(ASPxGridView2.GetRowValues(editingRowVisibleIndex, "Werk")) Then
        '        Try
        '            If (e.NewValues(0) > 0) Then
        '                Dim s_SQL As String = "Insert into VOO_Materiaal (Werk, Article, Description, Nombre) VALUES (@werk, @Article, @Description, @Nombre)"

        '                Dim cmd As New SqlCommand(s_SQL, cn)
        '                Dim par As New SqlParameter("@werk", SqlDbType.Int) With {.Value = Session("uitgevoerdwerk")}
        '                cmd.Parameters.Add(par)
        '                par = New SqlParameter("@Article", SqlDbType.NVarChar) With {.Value = ASPxGridView2.GetRowValues(editingRowVisibleIndex, "Article").ToString()}
        '                cmd.Parameters.Add(par)


        '                par = New SqlParameter("@Description", SqlDbType.NVarChar) With {.Value = ASPxGridView2.GetRowValues(editingRowVisibleIndex, "Description").ToString()}
        '                cmd.Parameters.Add(par)


        '                par = New SqlParameter("@Nombre", SqlDbType.Decimal) With {.Value = e.NewValues(0)}


        '                cmd.Parameters.Add(par)
        '                cmd.ExecuteNonQuery()
        '            End If
        '        Catch ex As Exception
        '            Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
        '            Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
        '            cmd.Parameters.Add(par)
        '            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij update post aan postengroep door {0} {1} {2}", Session("naam"), Session("voornaam"), ex.Message)}
        '            cmd.Parameters.Add(par)
        '            cmd.ExecuteNonQuery()
        '        End Try
        '    Else
        '        Dim s_SQL As String = "Update Voo_Materiaal set nombre=@nombre where id=@id"
        '        Dim cmd As New SqlCommand(s_SQL, cn)
        '        Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxGridView2.GetRowValues(editingRowVisibleIndex, "id").ToString()}
        '        cmd.Parameters.Add(par)
        '        par = New SqlParameter("@Nombre", SqlDbType.Decimal) With {.Value = e.NewValues(0)}
        '        cmd.Parameters.Add(par)
        '        cmd.ExecuteNonQuery()
        '    End If
        'End Using

    End Sub

    Protected Sub ButtonKlaar_Click(sender As Object, e As EventArgs) Handles buttonKlaar.Click
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "Update Werk set RedenRegie=@reden where id=@id"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = Session("uitgevoerdwerk")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@reden", SqlDbType.NVarChar) With {.Value = ASPxTextBoxRegie.Text}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()
            cn.Close()
        End Using

        Response.Redirect("~/Werken.aspx", False)
        Context.ApplicationInstance.CompleteRequest()
    End Sub

    Protected Sub ASPxGridView1_RowUpdated(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatedEventArgs) Handles ASPxGridView1.RowUpdated
        If e.Exception Is Nothing Then
            Dim editingRowVisibleIndex As Integer = ASPxGridView1.FindVisibleIndexByKeyValue(e.Keys.Values(0))
            If ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Poste") = "A20000" Then
                DirectCast(sender, ASPxGridView).JSProperties("cpIsUpdated") = e.Keys(0)
            End If
        End If



    End Sub

    Protected Sub ASPxTextBoxRegie_TextChanged(sender As Object, e As EventArgs) Handles ASPxTextBoxRegie.TextChanged
        If ASPxTextBoxRegie.Text.Length > 9 Then
            buttonKlaar.Enabled = True
            clientLabel2.Text = ""

        End If


    End Sub

    Protected Sub ASPxPageControl1_ActiveTabChanged(source As Object, e As TabControlEventArgs) Handles ASPxPageControl1.ActiveTabChanged

    End Sub

    Protected Sub ASPxComboBoxArtikel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxArtikel.SelectedIndexChanged
        ASPxSpinEditAantal.Number = 0
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

            cn.Open()
            Dim s_SQL As String = "select HeeftSerienummer from basismateriaal where id=@id"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
            cmd.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()
                If Not IsDBNull(dr2.Item(0)) Then
                    If dr2.Item(0) = True Then

                        HiddenArtikel("heeftSerienummer") = dr2.Item(0)

                    Else
                        HiddenArtikel("heeftSerienummer") = dr2.Item(0)
                    End If
                Else
                    HiddenArtikel("heeftSerienummer") = False
                    buttonKlaar.Enabled = True
                    clientLabel2.Text = ""
                End If
            End If

            dr2.Close()

            cn.Close()
        End Using
        'Dim context As New VooEntities
        'Dim magid As Integer = context.Gebruikers.Find(Session("userid")).MagazijnId
        'Dim artid As Integer = ASPxComboBoxArtikel.Value
        'Dim inStock As Decimal = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = artid).FirstOrDefault

        'HiddenArtikel("artikelMax") = inStock

    End Sub

    Protected Sub ASPxSpinEditAantal_NumberChanged(sender As Object, e As EventArgs) Handles ASPxSpinEditAantal.NumberChanged
        ShowSerienummers()
    End Sub

    Protected Sub ShowSerienummers()
        If ASPxSpinEditAantal.Number = 1 Then

        End If
    End Sub


    Protected Sub ASPxComboBoxArtikel_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxArtikel.PreRender
        Dim cb As ASPxComboBox = CType(sender, ASPxComboBox)
        cb.TextFormatString = "{0}"
    End Sub

    Protected Sub ASPxCallback1_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback1.Callback
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim desc As String = ""
            Dim s_SQL As String = "select description from basismateriaal where id=@id"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = ASPxComboBoxArtikel.Value}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not IsDBNull(dr.Item(0)) Then
                    desc = dr.Item(0)

                End If
            End If

            dr.Close()


            s_SQL = "select article, id from VOO_materiaal where werk=@Werk and article = @article"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@Werk", SqlDbType.Int) With {.Value = Session("uitgevoerdwerk")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@Article", SqlDbType.NVarChar) With {.Value = ASPxComboBoxArtikel.Text}
            cmd.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd.ExecuteReader
            Dim matid As Integer
            If dr2.HasRows Then
                dr2.Read()
                If Not IsDBNull(dr2.Item(0)) Then
                    matid = dr2.Item(1)
                    'Artikel is al gebruikt in werk
                    s_SQL = "update VOO_Materiaal set nombre = nombre + @Nombre where id = @id"
                    Dim cmd2 As New SqlCommand(s_SQL, cn)
                    par = New SqlParameter("@Nombre", SqlDbType.Decimal) With {.Value = ASPxSpinEditAantal.Value}
                    cmd2.Parameters.Add(par)
                    par = New SqlParameter("@id", SqlDbType.Int) With {.Value = dr2.Item(1)}
                    cmd2.Parameters.Add(par)
                    dr2.Close()
                    cmd2.ExecuteNonQuery()

                End If
            Else
                'Artikel is nog niet gebruikt
                s_SQL = "insert into VOO_Materiaal (Werk, Article, Description, Nombre) values (@Werk, @Article, @Description, @Nombre)"
                Dim cmd2 = New SqlCommand(s_SQL, cn)
                par = New SqlParameter("@Werk", SqlDbType.Int) With {.Value = Session("uitgevoerdwerk")}
                cmd2.Parameters.Add(par)
                par = New SqlParameter("@Article", SqlDbType.NVarChar) With {.Value = ASPxComboBoxArtikel.Text}
                cmd2.Parameters.Add(par)
                par = New SqlParameter("@Nombre", SqlDbType.Decimal) With {.Value = ASPxSpinEditAantal.Value}
                cmd2.Parameters.Add(par)
                par = New SqlParameter("@Description", SqlDbType.NVarChar) With {.Value = desc}
                cmd2.Parameters.Add(par)
                dr2.Close()
                cmd2.ExecuteNonQuery()
            End If

            dr2.Close()

            'UPDATE STOCKARTIKEL
            Dim context As New VooEntities
            Dim magid As Integer = context.Gebruikers.Find(Session("userid")).MagazijnId
            Dim basisId = Convert.ToInt32(ASPxComboBoxArtikel.Value)
            Dim stockart = context.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = basisId).FirstOrDefault
            stockart.Aantal = stockart.Aantal - ASPxSpinEditAantal.Number

            Dim sb As New Stockbeweging
            sb.beweging = -ASPxSpinEditAantal.Number
            sb.datum = DateTime.Now
            sb.gebruiker = Session("userid")
            sb.opmerking = "Inlezen Stock"
            sb.stockmagazijnId = stockart.id
            context.Stockbeweging.Add(sb)

            Dim smid As Int32 = stockart.id
            If stockart.Basismateriaal.SerieVerplichtBijOpboeken And stockart.Aantal <> context.Serienummer.Where(Function(x) x.StockMagazijnId = stockart.id).Count() Then
                'Bij mat met serienummer, als aantal niet matched
                Dim addresses As New List(Of String)
                addresses.Add("conan.dufour@apkgroup.eu")
                mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                       "noreply@apkgroup.eu",
                       addresses,
                       "Fout is opgetreden bij inlezen stock. Raadpleeg stockbeweging" & sb.id,
                       New Dictionary(Of String, Byte()))
            End If

            'UPDATE SERIENUMMER
            If HiddenArtikel("heeftSerienummer") Then
                For i = 1 To ASPxSpinEditAantal.Value
                    Dim serienummer As String
                    Dim serienummerObject As Serienummer
                    Select Case i
                        Case 1
                            serienummer = ASPxTextBoxSerie1.Value
                        Case 2
                            serienummer = ASPxTextBoxSerie2.Value
                        Case 3
                            serienummer = ASPxTextBoxSerie3.Value
                        Case 4
                            serienummer = ASPxTextBoxSerie4.Value
                        Case 5
                            serienummer = ASPxTextBoxSerie5.Value
                        Case 6
                            serienummer = ASPxTextBoxSerie6.Value
                        Case 7
                            serienummer = ASPxTextBoxSerie7.Value
                    End Select
                    serienummerObject = context.Serienummer.Where(Function(x) x.serienummer1 = serienummer).FirstOrDefault

                    If Not IsNothing(serienummerObject) Then

                        serienummerObject.uitgeboekt = True

                    End If

                    Dim logS As New Log
                    logS.Actie = "Gebruiker " & Session("userid") & " Heeft " & serienummer & "Uitgeboekt op werk " & Session("uitgevoerdwerk")
                    logS.Tijdstip = Today
                    logS.Gebruiker = Session("userid")
                    context.Log.Add(logS)

                Next
                context.SaveChanges()
            End If



            'LOG
            Dim log As New Log
            log.Actie = "Gebruiker " & Session("userid") & " Heeft " & ASPxSpinEditAantal.Number & " Van artikel " & desc & "gebruikt op werk " & Session("uitgevoerdwerk")
            log.Tijdstip = Today
            log.Gebruiker = Session("userid")
            context.Log.Add(log)
            context.SaveChanges()

            context.Dispose()

            cn.Close()

        End Using
    End Sub

    Protected Sub ASPxButtonToevoegen_Click(sender As Object, e As EventArgs) Handles ASPxButtonToevoegen.Click

    End Sub
End Class