﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="WachtwoordWijzigen.aspx.vb" Inherits="Telecom.WachtwoordWijzigen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>    
    <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server">
        <Items>
            <dx:LayoutGroup Caption="Changement mot de passe">
                <Items>
                    <dx:LayoutItem Caption="Nouveau mot de passe">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtWachtwoord1" runat="server" MaxLength="20" Password="True" Width="170px">
                                    <ValidationSettings>
                                        <ErrorImage Url="~/images/warning.png">
                                        </ErrorImage>
                                        <RequiredField ErrorText="Obligatoire" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="Repeter nouveau mot de passe">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtWachtwoord2" runat="server" MaxLength="20" Password="True" Width="170px">
                                    <ValidationSettings>
                                        <ErrorImage Url="~/images/warning.png">
                                        </ErrorImage>
                                        <RequiredField ErrorText="Obligatoire" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption=" ">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxButton ID="btnBevestigen" runat="server" Text="Confirmer">
                                    <Image Url="~/images/Apply_16x16.png">
                                    </Image>
                                </dx:ASPxButton>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
        </Items>
</dx:ASPxFormLayout>
    <br />

</asp:Content>
