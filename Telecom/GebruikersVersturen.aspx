﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="GebruikersVersturen.aspx.vb" Inherits="Telecom.GebruikersVersturen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function OnClickButtonDel(s, e) {
            grid.PerformCallback('Versturen');
        }
        function SelChanged(s, e) {
            if (e.isSelected) {
                grid.GetRowValues(e.visibleIndex, 'Email;id', OnGridSelectionComplete);

            }
        }
        function OnGridSelectionComplete(values) {
            var str = values[0];
            var key = values[1];
            if (str=="Geen email") {
                grid.SetFocusedRowIndex(0);
                grid.UnselectRowsByKey(key);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:right; margin-bottom:10px;margin-left:10px;"><dx:ASPxMenu ID="ASPxMenu1" runat="server" ShowAsToolbar="True">
                    <ClientSideEvents ItemClick="function(s, e) {
	if (e.item.name==&quot;VerstuurEmail&quot;) {
 grid.PerformCallback('Versturen');
} 
}" />
                    <Items>
                        <dx:MenuItem ToolTip="Envoyer des informations de connexion aux employés sélectionnés" Name="VerstuurEmail">
                            <Image Url="~/images/Mail_32x32.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                    </Items>
                </dx:ASPxMenu></div><h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
    
    <asp:Literal ID="Literal3" runat="server"></asp:Literal>
    
    <br />
    <dx:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceNieuweGebruikers" KeyFieldName="id" ClientInstanceName="grid">
        <Columns>
            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowDeleteButton="True" ShowEditButton="True" ShowSelectCheckbox="True" VisibleIndex="9">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="0" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Login" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="GSM" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Email" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="WachtwoordVerstuurd" VisibleIndex="6" Visible="False">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataDateColumn FieldName="WachtwoordVerstuurdOp" VisibleIndex="7" Visible="False">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataComboBoxColumn FieldName="Niveau" VisibleIndex="4">
                <PropertiesComboBox DataSourceID="SqlDataSource1" DropDownRows="25" TextField="Naam" ValueField="id" ValueType="System.Int32">
                    <Columns>
                        <dx:ListBoxColumn Caption="Naam" FieldName="Naam" />
                        <dx:ListBoxColumn FieldName="id" Visible="False" />
                        <dx:ListBoxColumn Caption="Niveau" FieldName="Niveau" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn FieldName="Beheerder" ReadOnly="True" Visible="False" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Nom" FieldName="Werknemer" ReadOnly="True" VisibleIndex="2">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsPager PageSize="20">
            <PageSizeItemSettings Caption="Items per pagina:" Items="10, 20, 50" ShowAllItem="True" Visible="True">
            </PageSizeItemSettings>
        </SettingsPager>
        <SettingsDataSecurity AllowInsert="False" AllowDelete="False" />
        <ClientSideEvents SelectionChanged="function(s, e) { SelChanged(s, e); }" />
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceNieuweGebruikers" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Gebruikers] WHERE [id] = @original_id AND (([Login] = @original_Login) OR ([Login] IS NULL AND @original_Login IS NULL)) AND (([GSM] = @original_GSM) OR ([GSM] IS NULL AND @original_GSM IS NULL)) AND (([Niveau] = @original_Niveau) OR ([Niveau] IS NULL AND @original_Niveau IS NULL)) AND (([Email] = @original_Email) OR ([Email] IS NULL AND @original_Email IS NULL)) AND (([WachtwoordVerstuurd] = @original_WachtwoordVerstuurd) OR ([WachtwoordVerstuurd] IS NULL AND @original_WachtwoordVerstuurd IS NULL)) AND (([WachtwoordVerstuurdOp] = @original_WachtwoordVerstuurdOp) OR ([WachtwoordVerstuurdOp] IS NULL AND @original_WachtwoordVerstuurdOp IS NULL))" InsertCommand="INSERT INTO [Gebruikers] ([Login], [GSM], [Niveau], [Email], [WachtwoordVerstuurd], [WachtwoordVerstuurdOp]) VALUES (@Login, @GSM, @Niveau, @Email, @WachtwoordVerstuurd, @WachtwoordVerstuurdOp)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT Gebruikers.id, Gebruikers.Login, Gebruikers.GSM, Gebruikers.Niveau, Gebruikers.Email, Gebruikers.WachtwoordVerstuurd, Gebruikers.WachtwoordVerstuurdOp, vwWerkn.Beheerder, vwWerkn2.Werknemer FROM Gebruikers LEFT OUTER JOIN Gebruikers AS Gebruikers_1 ON Gebruikers.AangemaaktDoor = Gebruikers_1.id INNER JOIN (SELECT NAAM + ' ' + VNAAM AS Beheerder, [ON], NR FROM Elly_SQL.dbo.WERKN) AS vwWerkn ON Gebruikers_1.Werkg = vwWerkn.[ON] AND Gebruikers_1.Werkn = vwWerkn.NR 
 INNER JOIN (SELECT NAAM + ' ' + VNAAM AS Werknemer, [ON], NR FROM Elly_SQL.dbo.WERKN) AS vwWerkn2 ON Gebruikers.Werkg = vwWerkn2.[ON] AND Gebruikers.Werkn = vwWerkn2.NR
WHERE (isnull(Gebruikers.WachtwoordVerstuurd,0)&lt;&gt;1)" UpdateCommand="UPDATE [Gebruikers] SET [Login] = @Login, [GSM] = @GSM, [Niveau] = @Niveau, [Email] = @Email, [WachtwoordVerstuurd] = @WachtwoordVerstuurd, [WachtwoordVerstuurdOp] = @WachtwoordVerstuurdOp WHERE [id] = @original_id AND (([Login] = @original_Login) OR ([Login] IS NULL AND @original_Login IS NULL)) AND (([GSM] = @original_GSM) OR ([GSM] IS NULL AND @original_GSM IS NULL)) AND (([Niveau] = @original_Niveau) OR ([Niveau] IS NULL AND @original_Niveau IS NULL)) AND (([Email] = @original_Email) OR ([Email] IS NULL AND @original_Email IS NULL)) AND (([WachtwoordVerstuurd] = @original_WachtwoordVerstuurd) OR ([WachtwoordVerstuurd] IS NULL AND @original_WachtwoordVerstuurd IS NULL)) AND (([WachtwoordVerstuurdOp] = @original_WachtwoordVerstuurdOp) OR ([WachtwoordVerstuurdOp] IS NULL AND @original_WachtwoordVerstuurdOp IS NULL))">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_Login" Type="String" />
            <asp:Parameter Name="original_GSM" Type="String" />
            <asp:Parameter Name="original_Niveau" Type="Int32" />
            <asp:Parameter Name="original_Email" Type="String" />
            <asp:Parameter Name="original_WachtwoordVerstuurd" Type="Boolean" />
            <asp:Parameter Name="original_WachtwoordVerstuurdOp" Type="DateTime" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Login" Type="String" />
            <asp:Parameter Name="GSM" Type="String" />
            <asp:Parameter Name="Niveau" Type="Int32" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="WachtwoordVerstuurd" Type="Boolean" />
            <asp:Parameter Name="WachtwoordVerstuurdOp" Type="DateTime" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Login" Type="String" />
            <asp:Parameter Name="GSM" Type="String" />
            <asp:Parameter Name="Niveau" Type="Int32" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="WachtwoordVerstuurd" Type="Boolean" />
            <asp:Parameter Name="WachtwoordVerstuurdOp" Type="DateTime" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_Login" Type="String" />
            <asp:Parameter Name="original_GSM" Type="String" />
            <asp:Parameter Name="original_Niveau" Type="Int32" />
            <asp:Parameter Name="original_Email" Type="String" />
            <asp:Parameter Name="original_WachtwoordVerstuurd" Type="Boolean" />
            <asp:Parameter Name="original_WachtwoordVerstuurdOp" Type="DateTime" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Naam], [Niveau] FROM [Gebruikersniveaus]"></asp:SqlDataSource>
</asp:Content>
