﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Login.aspx.vb" Inherits="Telecom.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   

   <div style="padding-left: 12px;"><b>Login</b></div>
    <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server">
        <Items>
            <dx:LayoutGroup Caption="Connectez-vous / Inloggen">
                <Items>
                    <dx:LayoutItem Caption="Nom d'utilisateur/Gebruikersnaam:">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtGebruiker" runat="server" Width="170px">
                                    <MaskSettings PromptChar=" " />
                                    <ValidationSettings SetFocusOnError="True">
                                         <RequiredField ErrorText="Gebruikersnaam kan niet leeg zijn" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="Mot de passe/Wachtwoord:">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtPaswoord" runat="server" Password="True" Width="170px" MaxLength="50">
                                    <ValidationSettings ErrorText="Paswoord kan niet leeg zijn" SetFocusOnError="True">
                                        <ErrorImage Url="~/images/exclamation_red.png">
                                        </ErrorImage>
                                        <RequiredField ErrorText="Mot de passe ne peut pas être vide" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption=" ">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxButton ID="btnLogin" runat="server" Text="Signer/Login">
                                    <Image Height="20px" Url="~/images/OperatingSystem_32x32.png" Width="20px">
                                    </Image>
                                </dx:ASPxButton>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
            <dx:LayoutGroup Caption="Choisissez un nouveau mot de passe s'il vous plaît." Visible="False">
                <Items>
                    <dx:LayoutItem Caption=" ">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxLabel ID="ASPxFormLayout1_E1" runat="server" Text="Pour des raisons de sécurité, nous demandons qu'une fois que vous choisissez un mot de passe personnel - ce mot de passe, vous devez maintenant vous connecter, le mot de passe que vous avez reçu de nous, annuler ci-dessous.">
                                </dx:ASPxLabel>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:EmptyLayoutItem>
                    </dx:EmptyLayoutItem>
                    <dx:LayoutItem Caption="Nouveau mot de passe:">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtNieuwwachtwoord1" runat="server" MaxLength="50" Password="True" Width="170px">
                                    <ValidationSettings SetFocusOnError="True">
                                        <ErrorImage Url="~/images/exclamation_red.png">
                                        </ErrorImage>
                                        <RequiredField ErrorText="Nouveau mot de passe est obligatoire" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="Confirmation nouveau mot de passe:">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtNieuwwachtwoord2" runat="server" MaxLength="50" Password="True" Width="170px">
                                    <ValidationSettings SetFocusOnError="True">
                                        <ErrorImage Url="~/images/exclamation_red.png">
                                        </ErrorImage>
                                        <RequiredField ErrorText="Confirmation nouveau mot de passe est obligatoire" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption=" ">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxButton ID="btnBevestigen" runat="server" Text="Confirmer">
                                </dx:ASPxButton>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
        </Items>
    </dx:ASPxFormLayout>
   
</asp:Content>
