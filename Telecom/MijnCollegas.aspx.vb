﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data
Imports DevExpress.Web

Public Class MijnCollegas
    Inherits System.Web.UI.Page
    Protected Sub vertaal(taalid)
        If taalid = 1 Then
            Literal2.Text = "Mijn collega's"
            ASPxGridView1.Columns("Naam").Caption = "Naam"
        Else

            Literal2.Text = "Mes collègues"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub


End Class