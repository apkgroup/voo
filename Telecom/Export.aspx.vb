﻿Public Class Export
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim data As Byte() = TryCast(Session("gridData"), Byte())

        If data IsNot Nothing Then
            Response.Clear()
            Response.ContentType = "application/pdf"
            Response.AddHeader("Content-Disposition", "inline; filename=export.xlsx")
            Response.BinaryWrite(data)
            Response.Flush()
            Response.Close()
            Response.[End]()
            Session("export") = "false"
        End If
    End Sub

End Class