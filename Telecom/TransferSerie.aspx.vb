﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data
Imports DevExpress.Web



Public Class TransferSerie
    Inherits System.Web.UI.Page
    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)
        Dim voocontext As New VooEntities
        Dim id As String = Request.QueryString("id")

        If taalId = 1 Then
            Literal2.Text = "Wisselen van serienummer opdrachtgever " & id
            Literal3.Text = "Opdrachtgever wisselen naar:"
            ASPxButton1.Text = "Annuleren"
            ASPxButton2.Text = "Bevestigen"
        Else
            Literal2.Text = "Transfer admin serie " & id
            Literal3.Text = "Nouveau administration:"
            ASPxButton1.Text = "Annuler"
            ASPxButton2.Text = "Confirmer"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub

    Protected Sub ASPxButton2_Click(sender As Object, e As EventArgs) Handles ASPxButton2.Click
        Dim voocontext As New VooEntities

        Dim value = Request.QueryString("id")
        Dim serienummer = voocontext.Serienummer.Where(Function(x) x.serienummer1 = value).FirstOrDefault
        If Not IsNothing(serienummer) AndAlso serienummer.uitgeboekt = False Then
            If Not serienummer.statusId = 3 And Not serienummer.statusId = 4 Then
                Response.Redirect("~/Beheer/OverzichtSerienummers.aspx?transferfout=" & Request.QueryString("id"), False)

                Return
            End If
        End If

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim LinkLeggenSQL As String = "select mo.opdrachtgeverId from [Voo].[dbo].[MateriaalOpdrachtgevers] mo
inner join [Voo].[dbo].StockMagazijn sm on sm.MateriaalId = mo.materiaalId
inner join [Voo].[dbo].Serienummer s on s.StockMagazijnId = sm.id where serienummer = @serienummer and mo.opdrachtgeverId = @opdrachtgeverId"

            Dim cmd As New SqlCommand(LinkLeggenSQL, cn)
            Dim par As New SqlParameter("@serienummer", SqlDbType.NVarChar, 200) With {.Value = Request.QueryString("id")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int, -1) With {.Value = ASPxComboBox1.Value}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If Not dr.HasRows Then
                dr.Close()
                ASPxPopupControlError.HeaderText = "Voorraadfout"


                ASPxPopupControlError.PopupHorizontalAlign = PopupHorizontalAlign.WindowCenter

                ASPxPopupControlError.PopupVerticalAlign = PopupVerticalAlign.WindowCenter
                ASPxPopupControlError.ShowOnPageLoad = True
                Return
            Else
                dr.Close()

            End If

            Dim mag_SQL As String = " DECLARE @stockhoofd int; 
 DECLARE @stockcurrent int; 
 DECLARE @MateriaalId int; 

 SELECT @stockcurrent = stockmagazijnId from [Voo].[dbo].[Serienummer] where serienummer = @serienummer; 
 
 SELECT @MateriaalId = MateriaalId from voo.dbo.StockMagazijn where id = @stockcurrent; 
 
 SELECT @stockhoofd = id 
 from voo.dbo.StockMagazijn 
 where MateriaalId = @MateriaalId
 and MagazijnId = (select top 1 id from voo.dbo.Magazijn where hoofd = 1 and OpdrachtgeverId = @opdrachtgeverId); 
 
 update [Voo].[dbo].[Serienummer] set StockMagazijnId = @stockhoofd where serienummer = @serienummer; 
 
 update voo.dbo.StockMagazijn set Aantal = Aantal +1 where id =@stockhoofd; 
 
 update voo.dbo.StockMagazijn set Aantal = Aantal -1 where id =@stockcurrent;

"
            cmd = New SqlCommand(mag_SQL, cn)
            par = New SqlParameter("@serienummer", SqlDbType.NVarChar, 200) With {.Value = Request.QueryString("id")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int, -1) With {.Value = ASPxComboBox1.Value}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()

        End Using
        Response.Redirect("~/Beheer/OverzichtSerienummers.aspx?transfer=" & Request.QueryString("id"), False)
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        Response.Redirect("~/Beheer/OverzichtSerienummers.aspx?transfer=", False)
    End Sub
End Class