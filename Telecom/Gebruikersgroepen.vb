'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class Gebruikersgroepen
    Public Property id As Integer
    Public Property Naam As String
    Public Property Kleur As String
    Public Property opdrachtgeverId As Nullable(Of Integer)

    Public Overridable Property Opdrachtgever As Opdrachtgever
    Public Overridable Property GebruikersgroepenLeden As ICollection(Of GebruikersgroepenLeden) = New HashSet(Of GebruikersgroepenLeden)

End Class
