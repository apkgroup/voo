﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="GrabbelBestellingPlaatsen.aspx.vb" Inherits="Telecom.GrabbelBestellingPlaatsen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        <script type="text/javascript">
            var changedValues = [];
            var tableWinkel;
            var divWinkel;
            function saveChangesBtn_Click(s, e) {
            
            ASPxGridView1.UpdateEdit();
            
            
        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();
           
        }
            window.addEventListener("load", init);

            function init() {
                tableWinkel = document.createElement("table");
                tableWinkel.className = 'tableWinkel'
                divWinkel = document.getElementById("winkelMandje");
                divWinkel.appendChild(tableWinkel);
            }
        
        </script>
        </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Passer une commande pour ">
    </dx:ASPxLabel>
    <dx:ASPxComboBox ID="ASPxComboBox1" runat="server" DataSourceID="SqlDataSourceGebruikers" TextField="Werknemer" ValueField="id" SelectedIndex="1">
    </dx:ASPxComboBox>

    <br />

     
                Nieuwe bestelling aanmaken forceren <dx:ASPxCheckBox ID="ASPxCheckBoxNieuw" ClientInstanceName="CheckNieuw" runat="server" CheckState="Unchecked">
                </dx:ASPxCheckBox>
           

    <br />
     <div id="winkelMandje" >      
    </div>
    <asp:SqlDataSource ID="SqlDataSourceGebruikers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [Gebruikers].id, (
SELECT 
CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam
ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)
END
) as Werknemer
FROM [Gebruikers] where opdrachtgeverId = @opdrachtgeverId and actief = 1


">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
    </dx:ASPxGridViewExporter>
    <br />

    <dx:ASPxButton ID="saveChangesBtn" runat="server" Text="Commander" AutoPostBack="false" ClientInstanceName="saveChangesBtn" CssClass="inline" Width="75px">
                        <ClientSideEvents Click="saveChangesBtn_Click" />
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="cancelChangesBtn" runat="server" Text="Annuler" AutoPostBack="false" CssClass="inline" Width="75px">
                        <ClientSideEvents Click="cancelChangesBtn_Click" />
                    </dx:ASPxButton>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" ClientInstanceName="ASPxGridView1" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMateriauxADMIN" KeyFieldName="Id">
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsEditing Mode="Batch">
            <BatchEditSettings StartEditAction="Click" />
        </SettingsEditing>
        <Settings ShowStatusBar="Hidden" />

                    
       
        <SettingsBehavior AllowFocusedRow="True" />

                    
       
        <SettingsSearchPanel Visible="True" />

                    
       
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Id" ReadOnly="True" VisibleIndex="0" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Eenheid" VisibleIndex="1" ReadOnly="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Artikel" VisibleIndex="2" ReadOnly="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="3" ReadOnly="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Nombre" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
        </Columns>
        <ClientSideEvents BatchEditEndEditing="function(s, e) {            
  window.setTimeout(function () {
    var index = e.visibleIndex; 
             console.log('index ' + index);
      var aantal= ASPxGridView1.batchEditApi.GetCellValue(index, &quot;Nombre&quot;,false);
              console.log('aantal ' + aantal);
   var omschrijving= ASPxGridView1.batchEditApi.GetCellValue(index, &quot;Description&quot;,false);
             console.log('omschrijving ' + omschrijving);
   var artikel= ASPxGridView1.batchEditApi.GetCellValue(index, &quot;Artikel&quot;,false);
             console.log('artikel ' + artikel);
    if (aantal !== 0) {
	var totaalData = `${artikel}***${omschrijving}***${aantal} `;
            console.log('totaaldata ' + totaalData);        
           changedValues = changedValues.filter(function(item) {
    return item.split('***')[0] !== artikel;
});
      	changedValues.push(totaalData );
            console.log('changedValues' + changedValues);
             tableWinkel.innerHTML = '';
          
                 changedValues.forEach(function(item) {

            console.log('item' + item);
        var data = item.split('***');
            console.log('data' + data);
             console.log('data artikel' + data[0]);
             console.log('data omschrijving' + data[1]);
             console.log('data aantal' + data[2]);
        var row = tableWinkel.insertRow();
            console.log('row' + row);
        var artikelCell = row.insertCell();
        var omschrijvingCell = row.insertCell();
        var aantalCell = row.insertCell();       
        artikelCell.innerHTML = data[0];
           
        omschrijvingCell.innerHTML = data[1];
        aantalCell.innerHTML = data[2]; })}}, 10);}

" />
        <SettingsAdaptivity AdaptivityMode="HideDataCells">
        </SettingsAdaptivity>
        <Templates>
                <StatusBar>
                    <dx:ASPxButton ID="saveChangesBtn" runat="server" Text="Commander" AutoPostBack="false"
                        RenderMode="Link" ClientInstanceName="saveChangesBtn" ClientEnabled="false">
                        <ClientSideEvents Click="saveChangesBtn_Click" />
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="cancelChangesBtn" runat="server" Text="Annuler" AutoPostBack="false"
                        RenderMode="Link">
                        <ClientSideEvents Click="cancelChangesBtn_Click" />
                    </dx:ASPxButton>
                </StatusBar>
            </Templates>
        <Styles>
            <SelectedRow BackColor="#FFCC99">
            </SelectedRow>
            <FocusedRow Font-Bold="True" ForeColor="Black">
            </FocusedRow>
            <InlineEditRow BackColor="#FFCC99">
            </InlineEditRow>
        </Styles>
    </dx:ASPxGridView>&nbsp;<dx:ASPxButton ID="ASPxButton1" runat="server" Text="Commander" AutoPostBack="false" ClientInstanceName="saveChangesBtn" CssClass="inline" Width="75px">
                        <ClientSideEvents Click="saveChangesBtn_Click" />
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Annuler" AutoPostBack="false" CssClass="inline" Width="75px">
                        <ClientSideEvents Click="cancelChangesBtn_Click" />
                    </dx:ASPxButton>
    <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" InsertCommand="INSERT INTO [Bestellijn] ([materiaalId], [bestellingId], [hoeveelheid]) VALUES (@materiaalId, @bestellingId, @hoeveelheid)" SelectCommand="SELECT [Grabbelmateriaal].[id] as 'Id',
[Grabbelmateriaal].Eenheid as 'Eenheid', 
[Grabbelmateriaal].article as 'Artikel', 
[Grabbelmateriaal].Omschrijving as 'Description', 
0.00 as 'Nombre'
FROM [Grabbelmateriaal]
where ([AF ]is null or AF = 0)
and opdrachtgeverId = @opdrachtgeverId
order by [Grabbelmateriaal].omschrijving" ID="SqlDataSourceMateriauxADMIN" UpdateCommand="UPDATE [Bestellijn] SET hoeveelheid = 4 where id=1">
        <InsertParameters>
            <asp:Parameter Name="materiaalId"></asp:Parameter>
            <asp:Parameter Name="bestellingId"></asp:Parameter>
            <asp:Parameter Name="hoeveelheid"></asp:Parameter>
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" DefaultValue="" />
        </SelectParameters>
    </asp:SqlDataSource>
        
    </asp:Content>
