﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data
Imports DevExpress.Web

Public Class BuitenFIFO
    Inherits System.Web.UI.Page
    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)
        Dim voocontext As New VooEntities
        Dim id As Integer = Request.QueryString("id")
        Dim naam As String = voocontext.Shipment.Find(id).shipmentNumber


        If taalId = 1 Then
            Literal2.Text = "Shipment buiten FIFO zetten:" & naam
            Literal3.Text = "Bent u zeker dat u de shipment met shipmentnummer " & naam & " met nog " & voocontext.Serienummer.Where(Function(x) x.shipmentId = id And x.FIFO = True And x.statusId = 3).Count & " serienummers op hoofdmagazijn buiten FIFO wilt zetten?"
            ASPxButton1.Text = "Nee"
            ASPxButton2.Text = "Ja"
        Else

        End If
        voocontext.Dispose()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If


    End Sub



    Protected Sub ASPxButton2_Click(sender As Object, e As EventArgs) Handles ASPxButton2.Click
        ' Dim voocontext As New VooEntities
        Dim id As Integer = Request.QueryString("id")




        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

            cn.Open()
            Dim s_SQL As String = "UPDATE [Voo].[dbo].[Serienummer] set FIFO = 0 where [shipmentId] = @shipId and [FIFO] = 1"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@shipId", SqlDbType.Int) With {.Value = id}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()
            cn.Close()
        End Using


        Response.Redirect("~/InlezenShipmentFile.aspx?Fifo=" & id, False)
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        Response.Redirect("~/InlezenShipmentFile.aspx", False)
    End Sub
End Class