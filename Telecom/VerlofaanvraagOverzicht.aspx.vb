﻿Imports System.Data.SqlClient
Imports DevExpress.Export

Public Class VerlofaanvraagOverzicht
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Session("level") > 4 Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
        If Not Page.IsPostBack Then
            If ASPxSpinEditJaar.Value = 0 Then
                ASPxSpinEditJaar.Value = DateTime.Now.Year
            End If
            If Session("verlofgebruiker") > 0 Then

                ASPxComboBoxGebruiker.Value = Session("verlofgebruiker")
            End If
        End If
    End Sub

    Private Sub ASPxComboBoxGebruiker_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxGebruiker.ValueChanged
        If Not ASPxComboBoxGebruiker.SelectedIndex = -1 Then
            If ASPxComboBoxGebruiker.Value <> Session("verlofgebruiker") Then
                Session("verlofgebruiker") = ASPxComboBoxGebruiker.Value
                Response.Redirect("VerlofaanvraagOverzicht.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If

        End If
    End Sub

    Private Sub ASPxMenu1_ItemClick(source As Object, e As DevExpress.Web.MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "ExportExcel" Then
            ASPxGridViewExporter1.WriteXlsxToResponse(New DevExpress.XtraPrinting.XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})
        End If
    End Sub
End Class