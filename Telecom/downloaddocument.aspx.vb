﻿Imports System.IO
Imports System.Data.SqlClient

Public Class downloaddocument
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
        If Not String.IsNullOrEmpty(Request.QueryString("d")) Then
            If Request.QueryString("t") = "wagen" Then
                Dim varext As String = ""
                Dim blok As Boolean = False
                Dim btImage As Byte() = Nothing

                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()
                    Dim cmd As New SqlCommand(String.Format("SELECT [Document], ext FROM OnderhoudSQL.dbo.Keuringsdocumenten WHERE (id={0})", Request.QueryString("d")), cn)
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    If dr.HasRows Then
                        dr.Read()
                        If Not dr.IsDBNull(1) Then
                            varext = dr.GetString(1)
                            If Not dr.IsDBNull(0) Then
                                btImage = CType(dr.GetValue(0), Byte())
                                Using ms As New IO.MemoryStream(btImage, 0, btImage.Length)
                                    Response.Clear()
                                    Response.ContentType = "application/" & varext.Replace(".", "")
                                    Response.AddHeader("Accept-Header", ms.Length.ToString())
                                    Response.AddHeader("Content-Disposition", String.Format("{0}; filename={1:yyyyMMdd_HHmmss}{2}", ("Attachment"), DateTime.Now, varext))
                                    Response.AddHeader("Content-Length", ms.Length.ToString())
                                    Response.BinaryWrite(ms.ToArray())
                                End Using
                            End If
                        End If
                        dr.Close()
                    End If
                End Using
            ElseIf Request.QueryString("t") = "leverbon" Then
                Dim varext As String = ""
                Dim blok As Boolean = False
                Dim btImage As Byte() = Nothing
                Dim id As Integer = Integer.Parse(Request.QueryString("d"))
                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()
                    Dim cmd As New SqlCommand(String.Format("SELECT Bon, ext FROM Leveringsbonnen WHERE (id={0})", id), cn)
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    If dr.HasRows Then
                        dr.Read()
                        If Not dr.IsDBNull(0) Then
                            btImage = CType(dr.GetValue(0), Byte())
                            varext = dr.GetString(1)
                        End If
                    End If
                    dr.Close()
                End Using
                Using ms As New IO.MemoryStream(btImage, 0, btImage.Length)
                    Response.Clear()
                    Response.ContentType = "application/" & varext.Replace(".", "")
                    Response.AddHeader("Accept-Header", ms.Length.ToString())
                    Response.AddHeader("Content-Disposition", String.Format("{0}; filename={1:yyyyMMdd_HHmmss}{2}", ("Attachment"), DateTime.Now, varext))
                    Response.AddHeader("Content-Length", ms.Length.ToString())
                    Response.BinaryWrite(ms.ToArray())
                End Using
            ElseIf Request.QueryString("t") = "telenetbestelbonuploadfile" Then
                Dim bestand As String = IO.Path.Combine(Server.MapPath("~/upload/"), Request.QueryString("d"))
                Dim btImage As Byte() = File.ReadAllBytes(bestand)
                Using ms As New IO.MemoryStream(btImage, 0, btImage.Length)
                    Response.Clear()
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    Response.AddHeader("Accept-Header", ms.Length.ToString())
                    Response.AddHeader("Content-Disposition", String.Format("filename={0}", Request.QueryString("d")))
                    Response.AddHeader("Content-Length", ms.Length.ToString())
                    Response.BinaryWrite(ms.ToArray())
                End Using
            ElseIf Request.QueryString("t") = "opleiding" Then
                Dim varext As String = ".tif"
                Dim blok As Boolean = False
                Dim btImage As Byte() = Nothing
                Dim id As Integer = Integer.Parse(Request.QueryString("d"))
                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()
                    Dim cmd As New SqlCommand(String.Format("Select Document from Elly_SQL.dbo.Opleidingen where (id={0})", id), cn)
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    If dr.HasRows Then
                        dr.Read()
                        If Not dr.IsDBNull(0) Then
                            btImage = CType(dr.GetValue(0), Byte())
                        End If
                    End If
                    dr.Close()
                End Using
                Using ms As New IO.MemoryStream(btImage, 0, btImage.Length)
                    Response.Clear()
                    Response.ContentType = "application/" & varext.Replace(".", "")
                    Response.AddHeader("Accept-Header", ms.Length.ToString())
                    Response.AddHeader("Content-Disposition", String.Format("{0}; filename={1:yyyyMMdd_HHmmss}{2}", ("Attachment"), DateTime.Now, varext))
                    Response.AddHeader("Content-Length", ms.Length.ToString())
                    Response.BinaryWrite(ms.ToArray())
                End Using
            ElseIf Request.QueryString("t") = "gebruikersfoto" Then
                If Not Session("isadmin") Then
                    Response.Redirect("~/Gebruikersprofiel.aspx", False)
                    Context.ApplicationInstance.CompleteRequest()
                End If
                Dim varext As String = ""
                Dim blok As Boolean = False
                Dim btImage As Byte() = Nothing
                Dim id As Integer = Integer.Parse(Request.QueryString("d"))
                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()
                    Dim cmd As New SqlCommand(String.Format("SELECT Pasfoto FROM Gebruikers WHERE (id={0})", id), cn)
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    If dr.HasRows Then
                        dr.Read()
                        If Not dr.IsDBNull(0) Then
                            btImage = CType(dr.GetValue(0), Byte())
                            varext = ".jpg"
                        End If
                    End If
                    Response.Clear()
                    Response.ContentType = "image/jpg"
                    '   Response.AddHeader("Accept-Header", MS.Length.ToString())
                    Response.AddHeader("Content-Disposition", String.Format("{0}; filename={1:yyyyMMdd_HHmmss}{2}", "Attachment", DateTime.Now, varext))
                    '     Response.AddHeader("Content-Length", MS.Length.ToString())
                    Response.BinaryWrite(btImage)
                    dr.Close()
                End Using
                '   Using ms As New IO.MemoryStream(btImage, 0, btImage.Length)
               
                '    End Using
            End If
            
        End If
        Response.End()
    End Sub

End Class