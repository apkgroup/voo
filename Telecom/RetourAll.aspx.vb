﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data
Imports DevExpress.Web



Public Class RetourAll
    Inherits System.Web.UI.Page
    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)
        Dim voocontext As New VooEntities
        Dim id As Integer = Request.QueryString("id")
        Dim naam As String = voocontext.StockMagazijn.Find(id).Magazijn.Gebruikers.FirstOrDefault.Login
        Dim prod As String = voocontext.StockMagazijn.Find(id).MateriaalId

        If taalId = 1 Then
            Literal2.Text = "Retour alle artikelen " & prod & " Voor " & naam
            Literal3.Text = "Bent u zeker dat u alle artikelen van " & naam & " met id " & prod & " wilt retourneren naar het hoofdmagazijn"
            ASPxButton1.Text = "Nee"
            ASPxButton2.Text = "Ja"
        Else
            Literal2.Text = "Retour all pour aritcle " & prod & " de " & naam
            Literal3.Text = "Êtes-vous sûr de vouloir réinitialiser le mot de passe?"
            ASPxButton1.Text = "Non"
            ASPxButton2.Text = "Oui"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

            cn.Open()
            Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
            cmd.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()
                Session("MagazijnId") = dr2.GetInt32(0)
            End If
            dr2.Close()
            cn.Close()
        End Using

    End Sub

    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender
        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxButton2_Click(sender As Object, e As EventArgs) Handles ASPxButton2.Click
        Dim voocontext As New VooEntities
        Dim id As Integer = Request.QueryString("id")
        Dim prod As Integer = voocontext.StockMagazijn.Find(id).MateriaalId
        Dim opdrachtId As Integer = Session("opdrachtgever")
        Dim hoofdMagId As Integer = Session("MagazijnId")
        Dim StockMagazijnHoofd As StockMagazijn = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = hoofdMagId And x.MateriaalId = prod).FirstOrDefault
        Dim stockart = voocontext.StockMagazijn.Find(id)

        Dim sb As New Stockbeweging
        sb.beweging = -voocontext.StockMagazijn.Find(id).Aantal
        sb.datum = DateTime.Now
        sb.gebruiker = Session("userid")
        sb.opmerking = "Retour all (user)"
        sb.stockmagazijnId = stockart.id
        voocontext.Stockbeweging.Add(sb)

        Dim sbhoofd As New Stockbeweging
        sbhoofd.beweging = voocontext.StockMagazijn.Find(id).Aantal
        sbhoofd.datum = DateTime.Now
        sbhoofd.gebruiker = Session("userid")
        sbhoofd.opmerking = "Retour all (hoofd)"
        sbhoofd.stockmagazijnId = StockMagazijnHoofd.id
        voocontext.Stockbeweging.Add(sbhoofd)

        StockMagazijnHoofd.Aantal += voocontext.StockMagazijn.Find(id).Aantal
        voocontext.StockMagazijn.Find(id).Aantal = 0





        Dim serienummerstoupdate = voocontext.Serienummer.Where(Function(x) x.StockMagazijnId = id).ToList

        For Each srnr In serienummerstoupdate
            srnr.StockMagazijnId = StockMagazijnHoofd.id
            srnr.statusId = 3
        Next



        Dim smid As Int32 = stockart.id
        If stockart.Basismateriaal.SerieVerplichtBijOpboeken And stockart.Aantal <> voocontext.Serienummer.Where(Function(x) x.StockMagazijnId = smid).Count() Then
            'Bij mat met serienummer, als aantal niet matched
            Dim addresses As New List(Of String)
            addresses.Add("conan.dufour@apkgroup.eu")
            mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                       "noreply@apkgroup.eu",
                       addresses,
                       "Fout is opgetreden bij retour all stock. Raadpleeg stockbeweging" & sb.id,
                       New Dictionary(Of String, Byte()))
        End If



        Dim smidhoofd As Int32 = StockMagazijnHoofd.id
        If stockart.Basismateriaal.SerieVerplichtBijOpboeken And StockMagazijnHoofd.Aantal <> voocontext.Serienummer.Where(Function(x) x.StockMagazijnId = smidhoofd).Count() Then
            'Bij mat met serienummer, als aantal niet matched
            Dim addresses As New List(Of String)
            addresses.Add("conan.dufour@apkgroup.eu")
            mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                       "noreply@apkgroup.eu",
                       addresses,
                       "Fout is opgetreden bij retour all stock. Raadpleeg stockbeweging" & sbhoofd.id,
                       New Dictionary(Of String, Byte()))
        End If
        voocontext.SaveChanges()


        voocontext.Dispose()

        Response.Redirect("~/Beheer/Magazijn.aspx?Magazijn=" & id, False)
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        Response.Redirect("~/Beheer/Magazijn.aspx", False)
    End Sub
End Class