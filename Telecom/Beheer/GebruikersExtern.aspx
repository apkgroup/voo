﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="GebruikersExtern.aspx.vb" Inherits="Telecom.GebruikersExtern" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 32px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2><asp:Literal ID="LiteralTitel" runat="server"></asp:Literal>
        
       
    </h2>
     <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="ASPxLabel" Font-Bold="True" ForeColor="#CC0000">
        </dx:ASPxLabel>
        <table style="width:100%;">
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Naam *">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="ASPxTextBoxNaam" runat="server" Width="170px">
                        <ValidationSettings>
                            <RegularExpression ValidationExpression="^.{1,50}$" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>

                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="GSM">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="ASPxTextBoxGSM" runat="server" Width="170px">
                        <ValidationSettings>
                            <RegularExpression ValidationExpression="\+[0-9]{11}" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Email *">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="ASPxTextBoxEmail" runat="server" Width="170px">
                        <ValidationSettings>
                            <RegularExpression ErrorText="Email incorrect" ValidationExpression="^\w+([-+.'%]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">

                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Taal *">
                    </dx:ASPxLabel>

                </td>
                <td class="auto-style1">

                    <dx:ASPxComboBox ID="ASPxComboBoxTaal" runat="server">
                        <Items>
                            <dx:ListEditItem Text="Nederlands" Value="1" />
                            <dx:ListEditItem Text="Français" Value="2" />
                        </Items>
                        <ValidationSettings>
                            <RegularExpression ValidationExpression="^.{1,50}$" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>

                </td>
            </tr>
            <tr><td>
                    <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Groep">
                    </dx:ASPxLabel>
                </td><td>


                <dx:ASPxComboBox ID="cmbGroepen" ClientInstanceName="cmdGroepen" runat="server" DataSourceID="SqlDataSourceGroepen" EnableSynchronization="False" TextField="Naam" ValueField="id" ValueType="System.Int32">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
	OnGroepChanged(s);
}" />
                    <ValidationSettings>
                        <RegularExpression ValidationExpression="^.{0,50}$" />
                    </ValidationSettings>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSourceGroepen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Naam] FROM [Gebruikersgroepen] where opdrachtgeverId = @opdrachtgever ORDER BY [Naam]">
                    <SelectParameters>
                        <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" />
                    </SelectParameters>
                    </asp:SqlDataSource>


                </td></tr>

            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Niveau *">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="ASPxComboBoxNiveau" runat="server">
                        <Items>
                            <dx:ListEditItem Text="Technieker" Value="2" />
                            <dx:ListEditItem Text="Klant" Value="3" />
                             <dx:ListEditItem Text="Magazijnier" Value="4" />
                             <dx:ListEditItem Text="Klantverantwoordelijke" Value="5" />
                            <dx:ListEditItem Text="Beheerder" Value="1" />
                        </Items>
                        <ValidationSettings>
                            <RegularExpression ValidationExpression="^.{1,50}$" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr><td>

                &nbsp;</td>
                <td>

                    <dx:ASPxButton ID="ASPxButton1" runat="server">
                        <Image Url="~/images/Add_32x32.png">
                        </Image>
                    </dx:ASPxButton>

                </td>
            </tr>
        </table>

    
</asp:Content>
