﻿Imports DevExpress.Web

Public Class OpdrachtgeverSelect
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsCallback Then


            If Session("isingelogd") Then
                Session("url") = Request.Url.AbsoluteUri
                Response.Redirect("~/Default.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
            If Not Session("isadmin") Then
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
        End If
        Dim voocontext As New VooEntities
        If Session("taal") = 2 Then
            Literal1.Text = "Choisissez une Administration"
        Else
            Literal1.Text = "Kies een administratie"
        End If

        Dim group = New LayoutGroup()
        group.ColumnCount = 2
        group.ShowCaption = DevExpress.Utils.DefaultBoolean.False
        ASPxFormLayoutOpdrachtgevers.Items.Add(group)

        For Each opdrachtgever In voocontext.Gebruikers.Find(Session("userid")).AdminOpdrachtgevers
            Dim item = New LayoutItem()
            group.Items.Add(item)
            item.ShowCaption = DevExpress.Utils.DefaultBoolean.False
            Dim edit As New ASPxButton()

            edit.ID = opdrachtgever.opdrachtgeverId & "Edit"
            edit.ImageUrl = "~/images/" + opdrachtgever.Opdrachtgever.naam.Replace(" ", "").ToLower & ".png"
            edit.RenderMode = ButtonRenderMode.Button
            item.Controls.Add(edit)
            edit.Width = 310
            edit.Height = 140
            edit.AutoPostBack = False
            edit.ClientSideEvents.Click = "function() {ASPxCallback.PerformCallback(" & opdrachtgever.opdrachtgeverId & ");}"

            'If opdrachtgever.Opdrachtgever.naam = "Fluvius Geel" Then
            '    ASPxButton3.Visible = True
            'End If
            'If opdrachtgever.Opdrachtgever.naam = "Fluvius Brugge" Then
            '    ASPxButton4.Visible = True
            'End If
            'If opdrachtgever.Opdrachtgever.naam = "Brutele" Then
            '    ASPxButton1.Visible = True

            'End If
            'If opdrachtgever.Opdrachtgever.naam = "Nethys" Then
            '    ASPxButton2.Visible = True
            'End If
            'If opdrachtgever.Opdrachtgever.naam = "Telenet" Then
            '    ASPxButton5.Visible = True
            'End If

            'If opdrachtgever.Opdrachtgever.naam = "Fluvius - ex Eandis" Then
            '    ASPxButton6.Visible = True
            'End If
            'If opdrachtgever.Opdrachtgever.naam = "Fluvius - ex Infrax" Then
            '    ASPxButton7.Visible = True
            'End If
            'If opdrachtgever.Opdrachtgever.naam = "Farys" Then
            '    ASPxButton8.Visible = True
            'End If

            'If opdrachtgever.Opdrachtgever.naam = "Allego" Then
            '    ASPxButton9.Visible = True
            'End If

            'If opdrachtgever.Opdrachtgever.naam = "Luminus" Then
            '    ASPxButton10.Visible = True
            'End If

            'If opdrachtgever.Opdrachtgever.naam = "Blue Corner" Then
            '    ASPxButton11.Visible = True
            'End If

            'If opdrachtgever.Opdrachtgever.naam = "NST" Then
            '    ASPxButton12.Visible = True
            'End If

            'If opdrachtgever.Opdrachtgever.naam = "Pluginvest" Then
            '    ASPxButton13.Visible = True
            'End If

            'If opdrachtgever.Opdrachtgever.naam = "Verbraeken" Then
            '    ASPxButton14.Visible = True
            'End If

        Next

        If Not voocontext.Gebruikers.Find(Session("userid")).AdminOpdrachtgevers.Any Then
            Dim Opdrachtgever = voocontext.Opdrachtgever.Find(voocontext.Gebruikers.Find(Session("userid")).OpdrachtgeverId)
            Dim item = New LayoutItem()
            group.Items.Add(item)
            item.ShowCaption = DevExpress.Utils.DefaultBoolean.False
            Dim edit As New ASPxButton()

            edit.ID = Opdrachtgever.id & "Edit"
            edit.ImageUrl = "~/images/" + Opdrachtgever.naam.Replace(" ", "").ToLower & ".png"
            edit.RenderMode = ButtonRenderMode.Button
            item.Controls.Add(edit)
            edit.Width = 310
            edit.Height = 140
            edit.AutoPostBack = False
            edit.ClientSideEvents.Click = "function() {ASPxCallback.PerformCallback(" & Opdrachtgever.id & ");}"
        End If
    End Sub



    Protected Sub ASPxCallback_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback.Callback
        Dim opdrachtgeverId As Integer = e.Parameter
        Dim Voocontext As New VooEntities
        Session("opdrachtgever") = Voocontext.Opdrachtgever.Where(Function(x) x.id = opdrachtgeverId).FirstOrDefault.id
        Dim opdr As Integer = Session("opdrachtgever")
        Session("TAG") = Voocontext.Opdrachtgever.Find(opdr).tag
        Session("isingelogd") = True
        ASPxWebControl.RedirectOnCallback("~/Default.aspx")

    End Sub
End Class