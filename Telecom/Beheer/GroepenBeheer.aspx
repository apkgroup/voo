﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="GroepenBeheer.aspx.vb" Inherits="Telecom.GroepenBeheer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 32px;
        }
        .auto-style2 {
            font-family: "Segoe UI", Helvetica, "Droid Sans", Tahoma, Geneva, sans-serif;
            font-size: 12px;
            color: #333333;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2><asp:Literal ID="LiteralTitel" runat="server"></asp:Literal>
        
       
    </h2>
     <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="ASPxLabel" Font-Bold="True" ForeColor="#CC0000">
        </dx:ASPxLabel>
        <table style="width:100%;">
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Naam *">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="ASPxTextBoxNaam" runat="server" Width="170px">
                        <ValidationSettings>
                            <RegularExpression ValidationExpression="^.{1,50}$" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>

                </td>
            </tr>




            <tr><td>

                &nbsp;</td>
                <td>

                    <dx:ASPxButton ID="ASPxButton1" runat="server">
                        <Image Url="~/images/Add_32x32.png">
                        </Image>
                    </dx:ASPxButton>

                </td>
            </tr>
        </table>

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGroepen" KeyFieldName="id">
        <SettingsPager PageSize="20">
        </SettingsPager>
        <SettingsSearchPanel Visible="True" />
        <EditFormLayoutProperties ColCount="2" ColumnCount="2">
            <Items>
                <dx:GridViewColumnLayoutItem ColSpan="1" ColumnName="omschrijving">
                </dx:GridViewColumnLayoutItem>
                <dx:EditModeCommandLayoutItem ColSpan="2" ColumnSpan="2" HorizontalAlign="Right">
                </dx:EditModeCommandLayoutItem>
            </Items>
        </EditFormLayoutProperties>
        <Columns>
            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="omschrijving" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="opdrachtgever" Visible="False" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="aantalInGroep" ReadOnly="True" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>

    
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="select
[id]
      ,[Naam]
      ,[Hoofd]
      ,[OpdrachtgeverId]
      ,[type],AF
      ,[profitcenter]   FROM [Voo].[dbo].[Magazijn]
WHERE (([type] = 1) OR ([type] = 2)) AND opdrachtgeverId = @opdrachtgever" UpdateCommand="update [Voo].[dbo].[Magazijn] set AF=@AF, naam=@naam, [profitcenter]=@profitcenter where  id=@id">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="AF" />
            <asp:Parameter Name="naam" />
            <asp:Parameter Name="profitcenter" />
            <asp:Parameter Name="id" />
        </UpdateParameters>
    </asp:SqlDataSource>

    
    <asp:SqlDataSource ID="SqlDataSourceGroepen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [MateriaalGroep] WHERE [id] = @id" InsertCommand="INSERT INTO [MateriaalGroep] ([omschrijving], [opdrachtgever]) VALUES (@omschrijving, @opdrachtgever)" SelectCommand="SELECT m.id, omschrijving, opdrachtgever, (select count(article) from Basismateriaal where Materiaalgroep=m.id) as aantalInGroep FROM [MateriaalGroep] m" UpdateCommand="UPDATE [MateriaalGroep] SET [omschrijving] = @omschrijving, [opdrachtgever] = @opdrachtgever WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="omschrijving" Type="String" />
            <asp:Parameter Name="opdrachtgever" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="omschrijving" Type="String" />
            <asp:Parameter Name="opdrachtgever" Type="Int32" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

    
</asp:Content>
