﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="VerbruikProject.aspx.vb" Inherits="Telecom.VerbruikProject" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        Verbruik per project
        </h2>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId and (g.Actief = 1 or hoofd=1 or type is not null) )">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceDetails" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT
   b.Id,
      vl.[hoeveelheid],vl.prijs,
      (vl.prijs*vl.hoeveelheid) as 'TotalePrijs'
,v.stad,v.straat,v.datum,v.ordernummer,v.opmerking, mvan.naam as 'van', mop.naam as 'op'
  FROM [Voo].[dbo].[Verbruiklijn] vl
  left join [Voo].[dbo].[Verbruik] v on vl.verbruikId = v.id
  left join  [Voo].[dbo].magazijn mvan on v.verbruiktvan = mvan.id
    left join  [Voo].[dbo].magazijn mop on v.verbruiktop = mop.id
	left join [Voo].[dbo].basismateriaal b on b.id = vl.materiaalId 
	where hoeveelheid &lt;&gt; 0 and (mop.id = @magazijnId) and v.status =2 and vl.materiaalId =@matId
">
        <SelectParameters>
            <asp:ControlParameter ControlID="ASPxComboBoxMagazijn" Name="magazijnId" PropertyName="Value" />
            <asp:SessionParameter Name="matId" SessionField="MatIdVerbruik" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceStockMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT
   b.Id,
	  b.Article
      ,b.Description
      ,sum([hoeveelheid]) as 'TotaleHoeveelheid'
      ,sum(vl.prijs*vl.hoeveelheid) as 'TotalePrijs'
  FROM [Voo].[dbo].[Verbruiklijn] vl
  left join [Voo].[dbo].[Verbruik] v on vl.verbruikId = v.id
  left join  [Voo].[dbo].magazijn mvan on v.verbruiktvan = mvan.id
    left join  [Voo].[dbo].magazijn mop on v.verbruiktop = mop.id
	left join [Voo].[dbo].basismateriaal b on b.id = vl.materiaalId 
	where hoeveelheid &lt;&gt; 0 and (mop.id = @magazijnId) and v.status =2
	group by b.id,Article,b.Description
"> 
        <SelectParameters>
            <asp:ControlParameter ControlID="ASPxComboBoxMagazijn" Name="magazijnId" PropertyName="Value" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
         <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu>
<dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
     </dx:ASPxGridViewExporter>

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceStockMagazijn" KeyFieldName="Id">
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceDetails" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="Id" ReadOnly="True" Visible="False" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="hoeveelheid" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="prijs" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TotalePrijs" ReadOnly="True" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="stad" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="straat" VisibleIndex="5">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn FieldName="datum" VisibleIndex="6">
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn FieldName="ordernummer" VisibleIndex="7">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="opmerking" VisibleIndex="8">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="van" VisibleIndex="9">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="op" VisibleIndex="10">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <SettingsPager PageSize="25">
        </SettingsPager>
        <Settings ShowFilterBar="Auto" ShowHeaderFilterButton="True" ShowFilterRow="True" ShowFooter="True" />
        <SettingsDetail ShowDetailRow="true" ExportMode="All" />
        <SettingsDataSecurity AllowDelete="False" AllowInsert="False" AllowEdit="False" />
        <SettingsPopup>
            <HeaderFilter MinHeight="350px" MinWidth="600px">
            </HeaderFilter>
        </SettingsPopup>
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="0">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="TotaleHoeveelheid" VisibleIndex="2" ReadOnly="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="TotalePrijs" VisibleIndex="3" ReadOnly="True">
            </dx:GridViewDataTextColumn>
        </Columns>
        <TotalSummary>
            <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="TotalePrijs" ShowInColumn="Totale Prijs" SummaryType="Sum" />
        </TotalSummary>
    </dx:ASPxGridView>
    <br />
    <br />
</asp:Content>