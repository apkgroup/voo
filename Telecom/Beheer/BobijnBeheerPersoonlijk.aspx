﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="BobijnBeheerPersoonlijk.aspx.vb" Inherits="Telecom.BobijnBeheerPersoonlijk" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function OnInit(s, e) {  
    setTimeout(ChangeHint, 50);  
    s.ActiveItemChanged.AddHandler(function (s, args) { setTimeout(ChangeHint, 50); })  
}  

function ChangeHint() {  
    var src = imageZoom.GetMainElement().getElementsByClassName("dxiz-clipPanel")[0].getElementsByTagName("img")[0].src;  
    var hintElem = imageZoom.GetMainElement().getElementsByClassName("dxiz-hint")[0];  
    if (src.indexOf("empty") >= 0) {  
        if (!hintElem.classList.contains("MyClass"))  
            hintElem.classList.add("MyClass");  
    }  
    else {  
        if (hintElem.classList.contains("MyClass"))  
            hintElem.classList.remove("MyClass");  
    }  
        }

          function showWisselen(bobijnId, bobijnNummer,kabelType) {
       
              ASPxHiddenFieldLijnId.Set('bobijnId', bobijnId);
              console.log(bobijnNummer);
            ASPxTextBoxBobijn.SetValue(bobijnNummer);
            ASPxComboBoxKabelTypeOud.SetValue(kabelType);
            popupcontrolbox.Show();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>


    <br />
    <dx:ASPxLabel ID="ASPxLabelFout" runat="server" Font-Bold="True" ForeColor="Red">
    </dx:ASPxLabel>

 
    <br />
     <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu>

     <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
     </dx:ASPxGridViewExporter>

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" KeyFieldName="id" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMagazijnen0" EnableRowsCache="False">
        <SettingsPager PageSize="25">
        </SettingsPager>
        <Settings ShowHeaderFilterButton="True" />
        <SettingsBehavior ConfirmDelete="True" AutoFilterRowInputDelay="5000" FilterRowMode="OnClick" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewCommandColumn VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="bobijnnummer" VisibleIndex="1" ReadOnly="True" Caption="Nummer">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="aantal" VisibleIndex="2" Caption="Resterend">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="AF" VisibleIndex="3">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="Naam" VisibleIndex="4" ReadOnly="True" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="8" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="9" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataHyperLinkColumn Caption="Plaatsingsstaat" FieldName="id" ShowInCustomizationForm="True" VisibleIndex="10" ReadOnly="True" Visible="False">
                <PropertiesHyperLinkEdit NavigateUrlFormatString="../Beheer/BobijnMeetstaat.aspx?id={0}" Text="Plaatsingsstaat">
                </PropertiesHyperLinkEdit>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataHyperLinkColumn>
           
            <dx:GridViewDataComboBoxColumn Caption="Magazijn" FieldName="magazijnid" VisibleIndex="5">
                <PropertiesComboBox DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
           
            <dx:GridViewDataTextColumn Caption="Paklijst nr" FieldName="paklijstnummer" VisibleIndex="11">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Plaatsingsstaat nr" FieldName="plaatsingsstaatnr" VisibleIndex="12">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn Caption="Plaatsingsstaat" FieldName="plaatsingsstaatgemaakt" VisibleIndex="14" Visible="False">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataColumn Caption="Download" VisibleIndex="7">  
                    <DataItemTemplate>  
                        <dx:ASPxButton ID="ASPxButton1" runat="server" OnInit="ASPxButton1_Init"  
                            AutoPostBack="False" RenderMode="Link" Text="Download">  
                        </dx:ASPxButton>  
                    </DataItemTemplate>  
                </dx:GridViewDataColumn>
            <dx:GridViewDataComboBoxColumn Caption="Voorziene locatie" FieldName="voorzieneLocatie" VisibleIndex="6">
                <PropertiesComboBox DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn Caption="JMS nr" FieldName="JMSNummer" VisibleIndex="13">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Bobijnsoort" FieldName="Bobijnsoort" VisibleIndex="16">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Kabeltype wijzigen" FieldName="id" VisibleIndex="15" Visible="False">
      <DataItemTemplate >  
             <a class="tooltip" id="clickElement" href="javascript:void(0);" onclick="showWisselen(<%# Eval("id")%>,'<%# Eval("bobijnnummer")%>','<%# Eval("Article")%>')">Kabeltype Wisselen</a>
          
     </DataItemTemplate>  

<CellStyle HorizontalAlign="Center"></CellStyle>

            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Dagen aanwezig" FieldName="dagenInMagazijn" VisibleIndex="18">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="DatumIngelezen" VisibleIndex="16" Caption="Ingelezen">
                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy">
                </PropertiesDateEdit>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataDateColumn FieldName="DatumUitgeboekt" VisibleIndex="17" Caption="Uitgeboekt">
                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy">
                </PropertiesDateEdit>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataDateColumn>
        </Columns>
    </dx:ASPxGridView>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen0" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT b.id, [bobijnnummer]
      ,b.[aantal]
      ,b.[AF]
	  ,m.Naam
,m.id as magazijnid
	  ,ba.Article
	  ,ba.[Description]
      ,[paklijstnummer]
      ,[plaatsingsstaatnr]
      ,[PDF]

      ,[plaatsingsstaatgemaakt]
,DatumIngelezen
,DatumUitgeboekt,
CASE
WHEN DatumUitgeboekt is null THEN DATEDIFF(day,DatumIngelezen, GETDATE())
ELSE DATEDIFF(day,DatumIngelezen, DatumUitgeboekt)
END as dagenInMagazijn
,voorzieneLocatie
,JMSNummer
        ,bs.beschrijving as 'Bobijnsoort'
  FROM [Voo].[dbo].[Bobijn] b
  inner join [Voo].[dbo].StockMagazijn sm on b.stockmagazijnId = sm.id
   inner join [Voo].[dbo].Magazijn m on sm.MagazijnId = m.id
    inner join [Voo].[dbo].Basismateriaal ba on ba.id = sm.MateriaalId
         left join [Voo].[dbo].BobijnSoort bs on bs.id = b.soortBobijn
where m.id = (select magazijnId from Gebruikers g where g.id=@userid)" UpdateCommand="UPDATE voo.dbo.StockMagazijn set Aantal = aantal - (select aantal from voo.dbo.Bobijn where id = @id) where id = (select stockmagazijnId from voo.dbo.Bobijn where id = @id);
 INSERT INTO voo.dbo.stockbeweging (stockmagazijnId, datum, beweging, gebruiker, opmerking) values((select stockmagazijnId from voo.dbo.Bobijn where id = @id),GETDATE(),-(select aantal from voo.dbo.Bobijn where id = @id),@gebruiker,'bobijn verplaatsen (van)');


UPDATE [Voo].[dbo].[Bobijn] set stockmagazijnid =
 (select id from [Voo].[dbo].stockmagazijn sm where sm.MagazijnId= @magazijnid and sm.MateriaalId=(select materiaalid from [Voo].[dbo].stockmagazijn where id =(select stockmagazijnId from voo.dbo.Bobijn where id = @id))),[paklijstnummer]=@paklijstnummer
      ,[plaatsingsstaatnr]=@plaatsingsstaatnr
      ,[PDF]=CONVERT(VARBINARY(25), @PDF, 1)
      ,[plaatsingsstaatgemaakt]=@plaatsingsstaatgemaakt
 ,[JMSNummer]=@JMSNummer
,[aantal]=@aantal
,[voorzieneLocatie]=@voorzieneLocatie
,[AF]=@af
 where id = @id;
 

 UPDATE voo.dbo.StockMagazijn set Aantal = aantal + (select aantal from voo.dbo.Bobijn where id = @id) where id = (select stockmagazijnId from voo.dbo.Bobijn where id = @id);
  INSERT INTO voo.dbo.stockbeweging (stockmagazijnId, datum, beweging, gebruiker, opmerking) values((select stockmagazijnId from voo.dbo.Bobijn where id = @id),GETDATE(),(select aantal from voo.dbo.Bobijn where id = @id),@gebruiker,'bobijn verplaatsen (naar)')">
        <SelectParameters>
            <asp:SessionParameter Name="userid" SessionField="userid" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="id" />
            <asp:SessionParameter Name="gebruiker" SessionField="userid"  />
            <asp:Parameter Name="magazijnid" />
            <asp:Parameter Name="paklijstnummer" />
            <asp:Parameter Name="plaatsingsstaatnr" />
            <asp:Parameter Name="PDF" />
            <asp:Parameter Name="plaatsingsstaatgemaakt" />
            <asp:Parameter Name="JMSNummer" />
            <asp:Parameter Name="aantal" />
            <asp:Parameter Name="voorzieneLocatie" />
            <asp:Parameter Name="af" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
                        <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId )" ID="SqlDataSourceMagazijnen"><SelectParameters>
<asp:SessionParameter SessionField="opdrachtgever" Name="OpdrachtgeverId" Type="Int32"></asp:SessionParameter>
</SelectParameters>
</asp:SqlDataSource>

                   
                    <br />
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId and (g.Actief = 1 or hoofd=1 or type is not null) )">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
                   
                    </asp:Content>