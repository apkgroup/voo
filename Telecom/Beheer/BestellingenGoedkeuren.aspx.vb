﻿Imports DevExpress.Export

Public Class BestellingenGoedkeuren
    Inherits System.Web.UI.Page

    Protected Sub vertaal(taalid)
        If taalid = 1 Then
            Literal2.Text = "Beheer bestellingen"
            ASPxGridView1.Columns("Werknemer").Caption = "Werknemer"
            ASPxGridView1.Columns("beschrijving").Caption = "Status"
            ASPxGridView1.Columns("Datum").Caption = "Datum"
            ASPxGridView1.Columns("groep").Caption = "Groep"
        Else
            ASPxGridView1.Columns("Werknemer").Caption = "Empl."
            ASPxGridView1.Columns("beschrijving").Caption = "Statut"
            ASPxGridView1.Columns("Datum").Caption = "Date"
            ASPxGridView1.Columns("groep").Caption = "Groupe"
            Literal2.Text = "Gérer commandes"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Page.IsPostBack Then
            ASPxGridView1.FilterExpression = "[beschrijving] = 'Aangevraagd'"
        End If

    End Sub

    Protected Sub ASPxMenu1_ItemClick(source As Object, e As DevExpress.Web.MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "ExportExcel" Then
            ASPxGridViewExporter1.FileName = "Export Bestellingen " & Today.Date()
            ASPxGridViewExporter1.WriteXlsxToResponse(New DevExpress.XtraPrinting.XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})

        End If

    End Sub
End Class