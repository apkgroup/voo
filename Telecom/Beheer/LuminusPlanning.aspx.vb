﻿Imports System.IO
Imports DevExpress.Export
Imports DevExpress.Web

Public Class LuminusPlanning
    Inherits System.Web.UI.Page
    Protected Sub vertaal(taalid As Integer)


    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") And Not Session("level") = 10 Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        If Not Request.QueryString("transfer") Is Nothing Then
            ASPxLabel1.Text = "Serienummer " & Request.QueryString("transfer") & " is overgezet naar een nieuwe opdrachtgever."
        Else
            ASPxLabel1.Text = ""
        End If

        If Not Request.QueryString("transferfout") Is Nothing Then
            ASPxLabel1.Text = "Serienummer " & Request.QueryString("transfer") & " kan niet overgezet worden. Enkel serienummers met status 'Received Warehouse' of 'Received Technician' mogen verplaatst worden."
        Else
            ASPxLabel1.Text = ""
        End If




    End Sub



    Protected Sub ASPxMenu1_ItemClick(source As Object, e As DevExpress.Web.MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "ExportExcel" Then
            ASPxGridViewExporter1.FileName = "Export Serie " & Today.Date()
            ASPxGridViewExporter1.WriteXlsxToResponse(New DevExpress.XtraPrinting.XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})

        End If
    End Sub

End Class