﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Email notificaties.aspx.vb" Inherits="Telecom.Email_notificaties" %>
<%@ Register assembly="DevExpress.Web.ASPxHtmlEditor.v19.2, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHtmlEditor" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v19.2, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
td {
    padding:5px;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:right; margin-bottom:10px;margin-left:10px;"><dx:ASPxMenu ID="ASPxMenu1" runat="server" ShowAsToolbar="True" AutoPostBack="True">
                    <Items>
                        <dx:MenuItem Name="Bewaren" Text="" ToolTip="Klik hier om de gegevens te bewaren.">
                            <Image Url="~/images/save32x32.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         
                </dx:ASPxMenu></div> <h2>Beheer email notificaties</h2>
    <br />
    <p><span style="font-family:Calibri;font-size:small;">Je kan hier de inhoud van systeem email notificaties aan gebruikers aanpassen. Let wel op dat je geen variabele velden verwijdert. Variabele velden staan tussen brackets en zijn in hoofdletter, bijvoorbeeld [DOMEIN]
       </span></p>
    <table>
        <tr>
            <td>
                Selecteer een notificatie:
            </td>
             <td>

                 <dx:ASPxComboBox ID="cboNotificatie" runat="server" ClientInstanceName="cboNotificatie" DataSourceID="SqlDataSourceNotificaties" TextField="Omschrijving" ValueField="id" ValueType="System.Int32" AutoPostBack="True">
                 </dx:ASPxComboBox>
                 
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSourceNotificaties" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Omschrijving] FROM [Emailberichten] ORDER BY [Omschrijving]"></asp:SqlDataSource>
    <asp:Literal ID="LiteralFeedback" runat="server"></asp:Literal>
    <br />
    <table>
        <tr>
            <td>
                <b>Onderwerp e-mail:</b>
            </td>
            <td>

                <dx:ASPxTextBox ID="txtOnderwerp" runat="server" ClientInstanceName="txtOnderwerp" MaxLength="200" Width="400px">
                </dx:ASPxTextBox>

            </td>
        </tr>
    </table>
    <br />
    <p><b>Inhoud e-mail:</b></p>
     <dx:ASPxHtmlEditor ID="ASPxHtmlEditorBericht" runat="server">
          <SettingsDialogs>
<InsertImageDialog>
<SettingsImageUpload UploadImageFolder="~/upload/" UploadFolder="~/upload/"></SettingsImageUpload>
</InsertImageDialog>
</SettingsDialogs>
    </dx:ASPxHtmlEditor>
     <br />

</asp:Content>
