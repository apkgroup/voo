﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="ArtikelSelectiesWaardeAanpassen.aspx.vb" Inherits="Telecom.ArtikelSelectiesWaardeAanpassen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
         .fixed {    
    position: fixed;   
    top:0px;
    background-color: #ffffff ! important; 
}
         td {
    padding:5px;
}
    </style>
    <script type="text/javascript" src="../js/jquery-1.4.2.min.js">
</script>
		<script type="text/javascript" src="../js/fib-jquery.js"></script>
           
          <script type="text/javascript" charset="utf-8">
              $(document).ready(function () {

                  var offset = $('#gridmenu').offset();

                  $(window).scroll(function () {
                      var scrollTop = $(window).scrollTop();
                      if (offset.top < scrollTop) {
                          $('#gridmenu').addClass('fixed');
                      } else {
                          $('#gridmenu').removeClass('fixed');
                      };
                  });

              });

		</script>
    <script type="text/javascript">
     
        function Autosaven() {
            grid.UpdateEdit();
            SetButtonsVisibility(grid);
        }
        function SetPCVisible(value) {
            var popupControl = GetPopupControl();
            if (value) {
                popupControl.Show();
            }
            else {
                popupControl.Hide();
            }
        }

       
        function GetPopupControl() {
            return popup;
        }
        function SetButtonsVisibility(s) {
            if (!s.batchEditApi.HasChanges()) {
                toolbarrechts.GetItem(0).SetVisible(false);
                toolbarrechts.GetItem(1).SetVisible(false);
            }
            else {
                toolbarrechts.GetItem(0).SetVisible(true);
                toolbarrechts.GetItem(1).SetVisible(true);

            }
        }
        function OnMoreInfoClick(element, key) {
            callbackPanel.SetContentHtml("");
            popupFoto.ShowAtElement(element);

        }
        function popup_Shown(s, e) {
            callbackPanel.PerformCallback();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:right; margin-bottom:10px;margin-left:10px;"><div id="gridmenu"><dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Klik hier om de wijzingen te bewaren (wijzing=groen gemarkeerd)" Name="Bewaren" Text="">
                            <Image Url="~/images/save32x32.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                         <dx:MenuItem ToolTip="Klik hier om de wijzingen te annuleren (wijzing=groen gemarkeerd)" Name="Annuleren" Text="">
                            <Image Url="~/images/cancellen.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                        <dx:MenuItem ToolTip="Klik hier als u voor meerdere geselecteerde arikels dezelfde waarden wil instellen" Name="SelectieAanpassen" Text="">
                            <Image Url="~/images/Scripts_32x32.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                        
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;Bewaren&quot;) {
		grid.UpdateEdit();
             SetButtonsVisibility(grid);
} else {
	if (e.item.name==&quot;Annuleren&quot;) {
		grid.CancelEdit(); 
SetButtonsVisibility(grid);
} else {
             grid.UpdateEdit();
             if (!grid.batchEditApi.HasChanges()) {
	SetPCVisible(true);
}
             }
} 

 
}" />
                </dx:ASPxMenu></div></div> <h2>Waardes aanpassen voor een selectie van artikels</h2>
    <br />
    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceArtikels" KeyFieldName="id">
        <SettingsText EmptyDataRow="Geen data gevonden... " />
        <ClientSideEvents Init="function(s, e){ SetButtonsVisibility(s); }" EndCallback="function(s, e){ 
 grid.UnselectAllRowsOnPage();
SetButtonsVisibility(s);
}"
            BatchEditEndEditing="function(s, e){ window.setTimeout(function(){ SetButtonsVisibility(s); }, 10); }" />
        <Columns>
            <dx:GridViewCommandColumn VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ERP" FieldName="Artikel" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Omschrijving" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
           <dx:GridViewDataComboBoxColumn Caption="Type" FieldName="TypeArtikel" VisibleIndex="4">
                <PropertiesComboBox DataSourceID="SqlDataSourceArtikeltypes" TextField="Omschrijving" ValueField="id" ValueType="System.Int32">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="Leverancier" VisibleIndex="5">
                <PropertiesComboBox DataSourceID="SqlDataSourceLeveranciers" TextField="Naam" ValueField="id" ValueType="System.Int32">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataCheckColumn FieldName="Actief" VisibleIndex="6">
                 <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataSpinEditColumn Caption="Min" FieldName="Minimum" VisibleIndex="7">
                <PropertiesSpinEdit DisplayFormatString="g">
                </PropertiesSpinEdit>
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataSpinEditColumn>
            <dx:GridViewDataSpinEditColumn Caption="Max" FieldName="Maximum" VisibleIndex="8">
                <PropertiesSpinEdit DisplayFormatString="g">
                </PropertiesSpinEdit>
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataSpinEditColumn>
              <dx:GridViewDataColumn Caption="Foto" FieldName="HeeftFoto" VisibleIndex="8" Width="15%">
                <EditFormSettings Visible="False" />
                <DataItemTemplate>
                    <%# GetRender(Eval("[HeeftFoto]"))%>
                    
                </DataItemTemplate>
                   <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataColumn>
            <dx:GridViewDataSpinEditColumn Caption="VerpaktPer" FieldName="VerpaktPer" VisibleIndex="13">
                <PropertiesSpinEdit DisplayFormatString="g" MaxValue="10000">
                </PropertiesSpinEdit>
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataSpinEditColumn>
             <dx:GridViewDataTextColumn FieldName="VerpakkingsEH" VisibleIndex="14">
            </dx:GridViewDataTextColumn>
        </Columns>
       <Styles>           
             <BatchEditModifiedCell ForeColor="Black">
            </BatchEditModifiedCell>
             <FocusedRow ForeColor="Black">
            </FocusedRow>
           <SelectedRow ForeColor="Black" BackColor="#ff8000"></SelectedRow>
        </Styles>
        <SettingsPager Mode="ShowAllRecords" Visible="False">
        </SettingsPager>
        <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
         <SettingsEditing Mode="Batch">
        </SettingsEditing>
         <SettingsBehavior SortMode="DisplayText" AllowFocusedRow="True" AllowSelectByRowClick="True" />
         <Settings ShowHeaderFilterButton="False" ShowStatusBar="Hidden" />
        <SettingsSearchPanel Visible="true" />
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceArtikels" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [stock_Artikels] WHERE [id] = @id" InsertCommand="INSERT INTO [stock_Artikels] ([Artikel], [Omschrijving], [Leverancier], [Actief], [Minimum], [Maximum], [HeeftFoto], [TypeArtikel], [VerpakkingsEH], [VerpaktPer]) VALUES (@Artikel, @Omschrijving, @Leverancier, @Actief, @Minimum, @Maximum, @HeeftFoto, @TypeArtikel, @VerpakkingsEH, @VerpaktPer)" SelectCommand="SELECT [id], [Artikel], [Omschrijving], [Leverancier], [Actief], [Minimum], [Maximum], cast(isnull([HeeftFoto],0) as int) as HeeftFoto, [TypeArtikel], [VerpakkingsEH], [VerpaktPer] FROM [stock_Artikels] ORDER BY [Artikel]" UpdateCommand="UPDATE [stock_Artikels] SET [Artikel] = @Artikel, [Omschrijving] = @Omschrijving, [Leverancier] = @Leverancier, [Actief] = @Actief, [Minimum] = @Minimum, [Maximum] = @Maximum, [HeeftFoto] = @HeeftFoto, [TypeArtikel] = @TypeArtikel, [VerpakkingsEH] = @VerpakkingsEH, [VerpaktPer] = @VerpaktPer WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Artikel" Type="String" />
            <asp:Parameter Name="Omschrijving" Type="String" />
            <asp:Parameter Name="Leverancier" Type="Int32" />
            <asp:Parameter Name="Actief" Type="Boolean" />
            <asp:Parameter Name="Minimum" Type="Decimal" />
            <asp:Parameter Name="Maximum" Type="Decimal" />
            <asp:Parameter Name="HeeftFoto" Type="Boolean" />
            <asp:Parameter Name="TypeArtikel" Type="Int32" />
            <asp:Parameter Name="VerpakkingsEH" Type="String" />
            <asp:Parameter Name="VerpaktPer" Type="Decimal" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Artikel" Type="String" />
            <asp:Parameter Name="Omschrijving" Type="String" />
            <asp:Parameter Name="Leverancier" Type="Int32" />
            <asp:Parameter Name="Actief" Type="Boolean" />
            <asp:Parameter Name="Minimum" Type="Decimal" />
            <asp:Parameter Name="Maximum" Type="Decimal" />
            <asp:Parameter Name="HeeftFoto" Type="Boolean" />
            <asp:Parameter Name="TypeArtikel" Type="Int32" />
            <asp:Parameter Name="VerpakkingsEH" Type="String" />
            <asp:Parameter Name="VerpaktPer" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />

    <asp:SqlDataSource ID="SqlDataSourceArtikeltypes" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Omschrijving] FROM [stock_Artikels_Types] WHERE (isnull(Actief,0)=1) ORDER BY [Omschrijving]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceLeveranciers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Naam] FROM [stock_Leveranciers] WHERE (Actief=1) ORDER BY Naam"></asp:SqlDataSource>
    <dx:ASPxPopupControl ID="popup" ClientInstanceName="popup" runat="server" CloseAction="CloseButton" HeaderText="Waarden doorvoeren in meervoudige selectie" Height="321px" Modal="True" Width="398px" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <br />
             <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server">
        <Items>
            <dx:LayoutGroup Caption="Vul enkel de velden in die u wil doorvoeren">
                <Items>
                    <dx:LayoutItem Caption="Artikeltype:">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxComboBox ID="cboArtikelType" runat="server" ClientInstanceName="cboArtikelType" DataSourceID="SqlDataSourceArtikeltypes" TextField="Omschrijving" ValueField="id" ValueType="System.Int32" Width="250px">
                                </dx:ASPxComboBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="Leverancier">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxComboBox ID="cboLeverancier" ClientInstanceName="cboLeverancier" runat="server" DataSourceID="SqlDataSourceLeveranciers" TextField="Naam" ValueField="id" ValueType="System.Int32" Width="250px">
                                </dx:ASPxComboBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="Actief">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxCheckBox ID="chkActief" runat="server" Checked="True" CheckState="Checked" ClientInstanceName="chkActief">
                                </dx:ASPxCheckBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="Minimum">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxSpinEdit ID="spinMinimum" ClientInstanceName="spinMinimum" runat="server" Number="0" NumberType="Integer" HorizontalAlign="Right">
                                </dx:ASPxSpinEdit>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="Maximum">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxSpinEdit ID="spinMaximum" ClientInstanceName="spinMaximum" runat="server" Number="0" NumberType="Integer" HorizontalAlign="Right">
                                </dx:ASPxSpinEdit>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="Verpakt per:">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxSpinEdit ID="spinVerpakt" runat="server" ClientInstanceName="spinVerpakt" HorizontalAlign="Right" Number="0" NumberType="Integer">
                                </dx:ASPxSpinEdit>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="VerpakkingsEH:">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtVerpakkingsEH" runat="server" ClientInstanceName="txtVerpakkingsEH" MaxLength="20" Width="170px">
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption=" ">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxButton ID="btnBevestigenselectie" runat="server" AutoPostBack="False" Text="Bevestigen">
                                    <ClientSideEvents Click="function(s, e) {
if (cboLeverancier.GetSelectedIndex()&gt;(-1)) {
	for (i = grid.GetTopVisibleIndex() ; i &lt; grid.GetVisibleRowsOnPage() ; i++) {
      if (grid.IsRowSelectedOnPage(i)) {
            grid.batchEditApi.SetCellValue(i, &quot;Actief&quot;, chkActief.GetChecked());
 grid.batchEditApi.SetCellValue(i, &quot;Minimum&quot;, spinMinimum.GetValue());
grid.batchEditApi.SetCellValue(i, &quot;Maximum&quot;, spinMaximum.GetValue());
grid.batchEditApi.SetCellValue(i, &quot;Leverancier&quot;, cboLeverancier.GetValue());
if (cboArtikelType.GetSelectedIndex()&gt;(-1)) {
	grid.batchEditApi.SetCellValue(i, &quot;TypeArtikel&quot;, cboArtikelType.GetValue());

}
grid.batchEditApi.SetCellValue(i, &quot;VerpaktPer&quot;, spinVerpakt.GetValue());

grid.batchEditApi.SetCellValue(i, &quot;VerpakkingsEH&quot;,txtVerpakkingsEH.GetText());


}
        
    }
} else {
for (i = grid.GetTopVisibleIndex() ; i &lt; grid.GetVisibleRowsOnPage() ; i++) {
                                     
 if (grid.IsRowSelectedOnPage(i)) {
                                        
	   grid.batchEditApi.SetCellValue(i, &quot;Actief&quot;, chkActief.GetChecked());
 grid.batchEditApi.SetCellValue(i, &quot;Minimum&quot;, spinMinimum.GetValue());
grid.batchEditApi.SetCellValue(i, &quot;Maximum&quot;, spinMaximum.GetValue());
if (cboArtikelType.GetSelectedIndex()&gt;(-1)) {
	grid.batchEditApi.SetCellValue(i, &quot;TypeArtikel&quot;, cboArtikelType.GetValue());

}
grid.batchEditApi.SetCellValue(i, &quot;VerpaktPer&quot;, spinVerpakt.GetValue());

grid.batchEditApi.SetCellValue(i, &quot;VerpakkingsEH&quot;,txtVerpakkingsEH.GetText());



}
}
}
SetPCVisible(false);
SetButtonsVisibility(grid);

}" />
                                    <Image Url="~/images/Apply_16x16.png">
                                    </Image>
                                </dx:ASPxButton>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
        </Items>
    </dx:ASPxFormLayout>
            </dx:PopupControlContentControl>
</ContentCollection>
    </dx:ASPxPopupControl>
     <dx:ASPxPopupControl ID="popupFoto" ClientInstanceName="popupFoto" runat="server" AllowDragging="true"
        PopupHorizontalAlign="OutsideLeft" HeaderText="Foto">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxCallbackPanel ID="callbackPanel" ClientInstanceName="callbackPanel" runat="server"
                    Width="400px" Height="300px" OnCallback="callbackPanel_Callback" RenderMode="Table">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <table>
                                <tr>
                                    <td><span style="font-family:Calibri;font-size:medium;font-weight:bold;"><asp:Literal ID="litText" runat="server" Text=""></asp:Literal></span></td>
                                </tr>
                                <tr>
                                    <td><dx:ASPxBinaryImage ID="edBinaryImage" runat="server" AlternateText="Bezig met laden..." ImageAlign="Left" CssClass="Image">
                                        </dx:ASPxBinaryImage></td>
                                </tr>
                            </table>
                            
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents Shown="popup_Shown" />
    </dx:ASPxPopupControl>
    <dx:ASPxTimer ID="autosaver" runat="server" ClientInstanceName="autosaver">
         <ClientSideEvents Tick="function(s, e) {
	Autosaven();
}" />
     </dx:ASPxTimer>
</asp:Content>
