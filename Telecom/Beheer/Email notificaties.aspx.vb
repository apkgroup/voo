﻿Imports System.Data.SqlClient

Public Class Email_notificaties
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Private Sub cboNotificatie_ValueChanged(sender As Object, e As EventArgs) Handles cboNotificatie.ValueChanged
        Dim ibericht As Integer = cboNotificatie.Value
        If ibericht > 0 Then

            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim cmd As New SqlCommand(String.Format("SELECT Bericht, Onderwerp FROM Emailberichten  WHERE (id={0})", ibericht), cn)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()
                    If Not dr.IsDBNull(0) Then
                        ASPxHtmlEditorBericht.Html = dr.GetString(0)
                    End If
                    If Not dr.IsDBNull(1) Then
                        txtOnderwerp.Text = dr.GetString(1)
                    End If
                End If
                dr.Close()
            End Using

        End If
    End Sub

    Protected Sub ASPxMenu1_ItemClick(source As Object, e As DevExpress.Web.MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Try
                Dim cmd As New SqlCommand("Update Emailberichten SET Bericht=@bericht, Onderwerp=@onderwerp WHERE (id=@id)", cn)
                Dim par As New SqlParameter("@bericht", SqlDbType.NVarChar, 4000) With {.Value = ASPxHtmlEditorBericht.Html}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@onderwerp", SqlDbType.NVarChar, 200) With {.Value = txtOnderwerp.Text}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@id", SqlDbType.Int) With {.Value = cboNotificatie.Value}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Emailnotificatie {0} bijgewerkt door {1} {2}", cboNotificatie.Text, Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
                LiteralFeedback.Text = "<span style=""color:red;"">De gegevens werden succesvol bewaard. U mag nu dit formulier sluiten of een andere notificatie selecteren.</span>"
            Catch ex As Exception
                LiteralFeedback.Text = String.Format("<span style=""color:red;"">Er is een fout opgetreden en de gegevens zijn niet bewaard. <br>Foutmelding server:<br>{0}</span>", ex.Message)
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij bijwerken emailnotificatie {0} door {1} {2}", cboNotificatie.Text, Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End Try
           
        End Using
    End Sub
End Class