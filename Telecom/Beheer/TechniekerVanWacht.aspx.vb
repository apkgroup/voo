﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data

Public Class TechniekerVanWacht
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Private Sub grid_InitNewRow(sender As Object, e As DevExpress.Web.Data.ASPxDataInitNewRowEventArgs) Handles grid.InitNewRow
        e.NewValues("Actief") = True
        e.NewValues("IngesteldDoor") = Session("userid")
        e.NewValues("IngesteldOp") = DateTime.Now
    End Sub

    Private Sub grid_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles grid.RowInserted
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
            Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Technieker van wacht {0} toegevoegd door {1} {2} {3}", e.NewValues("Technieker"), Session("userid"), Session("naam"), Session("voornaam"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand("UPDATE Gebruikers SET VanWacht=0", cn)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand(String.Format("UPDATE Gebruikers SET VanWacht=1 WHERE (id={0})", e.NewValues("Technieker")), cn)
            cmd.ExecuteNonQuery()
            Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2010_SP2)
            exch.Url = New Uri("https://mail.apkintern.be/EWS/Exchange.asmx")
            exch.UseDefaultCredentials = False
            exch.Credentials = New System.Net.NetworkCredential("Administrator", "E01ab107%", "apk")
            TrustAllCertificatePolicy.OverrideCertificateValidation()
            exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.PrincipalName, Session("mailaccount"))
            Dim varBericht As String = ""
            Dim varOnderwerp As String = ""
            Try
                cmd = New SqlCommand("SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)", cn)
                par = New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Melding technieker van wacht"}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()
                    If Not dr.IsDBNull(0) Then
                        varBericht = dr.GetString(0)
                        varOnderwerp = dr.GetString(1)
                    End If
                End If
                dr.Close()
                Dim varGSM As String = ""
                Dim varEmail As String = ""
                cmd = New SqlCommand(String.Format("SELECT GSM, Email FROM Gebruikers WHERE (id={0})", e.NewValues("Technieker")), cn)
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()
                    If Not dr.IsDBNull(0) Then
                        varGSM = dr.GetString(0)
                    End If
                    If Not dr.IsDBNull(1) Then
                        varEmail = dr.GetString(1)
                    End If
                End If
                dr.Close()
                Dim message As New EmailMessage(exch)
                message.Subject = varOnderwerp
                Dim varBody As String = varBericht.Replace("[BEHEERDER]", String.Format("{0} {1}", Session("naam"), Session("voornaam")))
                varBody = varBody.Replace("[START]", e.NewValues("Start"))
                varBody = varBody.Replace("[STOP]", e.NewValues("Stop"))
                varBody = varBody.Replace("[SITETITEL]", Session("sitetitel"))
                Dim sBody As String = "<x-html>" & vbCrLf
                sBody = sBody & varBody
                sBody = sBody & "</x-html>"
                message.Body = sBody
                message.ToRecipients.Add(varEmail)
                message.Send()
                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("E-mail melding 'technieker van wacht' verzonden door {0} {1} aan {2}", Session("naam"), Session("voornaam"), e.NewValues("Technieker"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("INSERT INTO SMSWachtrij (Afzender, Afzenderid, Verzonden, VerzondenAan, VerzondenAanID, Bericht) VALUES (@Afzender, @Afzenderid, GetDate(), @VerzondenAan, @VerzondenAanID, @Bericht)", cn)
                par = New SqlParameter("@Afzender", SqlDbType.NVarChar, 20) With {.Value = String.Format("{0} {1}", Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@Afzenderid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                Dim gsmnummer As String = varGSM
                par = New SqlParameter("@VerzondenAan", SqlDbType.NVarChar, 20) With {.Value = IIf(gsmnummer.Substring(0, 1) = "+", gsmnummer, "+" & gsmnummer)}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@VerzondenAanID", SqlDbType.Int) With {.Value = e.NewValues("Technieker")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@Bericht", SqlDbType.NVarChar, 255) With {.Value = String.Format("U bent zonet aangesteld door {0} {1} als technieker van wacht, gelieve u 24/24 bereikbaar te houden.", Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("SMS bericht aan {0} toegevoegd aan wachtrij  door {1} {2}", gsmnummer, Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij verzenden melding 'technieker van wacht' door {0} {1} aan {2} : {3}", Session("naam"), Session("voornaam"), e.NewValues("Technieker"), ex.Message)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End Try
        End Using
    End Sub

    Private Sub grid_RowUpdated(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatedEventArgs) Handles grid.RowUpdated
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
            Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Technieker van wacht {0} bijgewerkt door {1} {2} {3}", e.NewValues("Technieker"), Session("userid"), Session("naam"), Session("voornaam"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

End Class