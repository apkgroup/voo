﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="OverzichtSerienummers.aspx.vb" Inherits="Telecom.OverzichtSerienummers" %>
<%@ Register assembly="DevExpress.Web.ASPxScheduler.v19.2, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxScheduler" tagprefix="dxwschs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h2>
         <asp:Literal ID="Literal2" runat="server"></asp:Literal>
     </h2>
     <dx:ASPxButton ID="ASPxButton3" runat="server" AutoPostBack="False" Text="Exporter" Visible="False">
         <ClientSideEvents Click="function(s, e) {
            ASPxGridView1.PerformCallback('CEXP');
	
}" />
     </dx:ASPxButton>
     <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" ForeColor="#009933">
     </dx:ASPxLabel>
     <dx:ASPxButton ID="ASPxButton4" runat="server" Text="Geselecteerde items overboeken naar">
     </dx:ASPxButton>
        <dx:ASPxComboBox ID="ASPxComboBoxArtikel" runat="server" ClientInstanceName="comboboxArtikel" DataSourceID="SqlDataSourceArtikelen" DisplayFormatString="{0}; {1}" DropDownWidth="450px" Height="20px" NullValueItemDisplayText="{0}; {1}" TextFormatString="{0}; {1} " ValueField="id" Width="323px">
        <Columns>
            <dx:ListBoxColumn FieldName="article" Name="0" Width="50px">
            </dx:ListBoxColumn>
            <dx:ListBoxColumn FieldName="description" Name="1">
            </dx:ListBoxColumn>
        </Columns>

    </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="SqlDataSourceArtikelen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT distinct article, description, CONCAT(article, ' | ', description) as naam, b.id FROM [Basismateriaal] b
INNER JOIN MateriaalOpdrachtgevers o ON b.id =o.materiaalId
WHERE o.opdrachtgeverId = @OpdrachtgeverId and [HeeftSerienummer] = 1">
                            <SelectParameters>
                                <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" DefaultValue="1" />
                            </SelectParameters>
                        </asp:SqlDataSource>


    <br />
     <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu>
     <dx:ASPxCheckBox ID="ASPxCheckBoxAllesZien" autopostback="true" runat="server" Text="Alles zien">
     </dx:ASPxCheckBox>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" ClientInstanceName="ASPxGridView1" KeyFieldName="id">
                            <ClientSideEvents EndCallback=" function(s, e) { 
                if (s.cpExport) {
                     
                    delete s.cpExport;
                    window.open('Export.aspx', '_blank');
                }
            } 
"/>
        <SettingsPager NumericButtonCount="20" PageSize="20">
        </SettingsPager>
                            <Settings ShowFilterBar="Auto" ShowHeaderFilterButton="True" ShowFilterRow="True" />
        <Columns>
<dx:GridViewDataTextColumn FieldName="id" VisibleIndex="0" ReadOnly="True">
    <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Naam" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="uitgeboekt" VisibleIndex="5">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataDateColumn FieldName="datumOntvangst" VisibleIndex="6">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="beschrijving" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="lotNr" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="datumGemaakt" VisibleIndex="9">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataDateColumn FieldName="datumUsed" VisibleIndex="10">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="locatie" VisibleIndex="11" ReadOnly="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Shipment" VisibleIndex="12" ReadOnly="True">
            </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="palletNo" VisibleIndex="13">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn VisibleIndex="14" FieldName="opdrachtgeverId">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT distinct se.id,m.Naam
,b.[Description]
,b.[Article]
      ,[serienummer]
      ,[uitgeboekt]
      ,[datumOntvangst]
      ,sta.beschrijving
      ,[lotNr]
      ,[datumGemaakt]
,[datumUsed]
,coalesce((select naam from locatie where id = locatieid ),( select sh.opmerking from [Voo].[dbo].shipment sh where sh.id = se.shipmentId)) as locatie
,(select shipmentnumber from [Voo].[dbo].shipment sh where sh.id = se.shipmentId) as Shipment
,[palletNo]
,m.opdrachtgeverId
  FROM [Voo].[dbo].[Serienummer] se 
  inner join voo.dbo.StockMagazijn st on se.StockMagazijnId = st.id 
  inner join voo.dbo.Magazijn m on st.MagazijnId = m.id 
    inner join voo.dbo.Basismateriaal b on st.MateriaalId = b.id 
INNER JOIN MateriaalOpdrachtgevers o ON b.id =o.materiaalId
inner join voo.dbo.status sta on sta.id = se.statusId
  where  m.OpdrachtgeverId =  @opdrachtgeverId">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
    </asp:SqlDataSource>
     <br />
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT distinct se.id,m.Naam
,b.[Description]
,b.[Article]
      ,[serienummer]
      ,[uitgeboekt]
      ,[datumOntvangst]
      ,sta.beschrijving
      ,[lotNr]
      ,[datumGemaakt]
,[datumUsed]
,(select opmerking from [Voo].[dbo].shipment sh where sh.id = se.shipmentId) as locatie
,(select shipmentnumber from [Voo].[dbo].shipment sh where sh.id = se.shipmentId) as Shipment
,[palletNo]
,m.opdrachtgeverId
  FROM [Voo].[dbo].[Serienummer] se 
  inner join voo.dbo.StockMagazijn st on se.StockMagazijnId = st.id 
  inner join voo.dbo.Magazijn m on st.MagazijnId = m.id 
    inner join voo.dbo.Basismateriaal b on st.MateriaalId = b.id 
INNER JOIN MateriaalOpdrachtgevers o ON b.id =o.materiaalId
inner join voo.dbo.status sta on sta.id = se.statusId
  where  m.OpdrachtgeverId in (select opdrachtgeverId from [Voo].[dbo].AdminOpdrachtgevers where gebruikerId = @gebruikerid)">
        <SelectParameters>
            <asp:SessionParameter Name="gebruikerid" SessionField="userid" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT distinct  se.id, m.Naam
,b.[Description]
,b.[Article]
      ,[serienummer]
      ,[uitgeboekt]
      ,[datumOntvangst]
      ,sta.beschrijving
      ,[lotNr]
      ,[datumGemaakt]
,[datumUsed]
        ,[locatieId]
,[palletNo]
,m.opdrachtgeverId
  FROM [Voo].[dbo].[Serienummer] se 
  inner join voo.dbo.StockMagazijn st on se.StockMagazijnId = st.id 
  inner join voo.dbo.Magazijn m on st.MagazijnId = m.id 
    inner join voo.dbo.Basismateriaal b on st.MateriaalId = b.id 
INNER JOIN MateriaalOpdrachtgevers o ON b.id =o.materiaalId
inner join voo.dbo.status sta on sta.id = se.statusId
  where  o.OpdrachtgeverId = @opdrachtgeverId">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
    </asp:SqlDataSource>
     <br />
    <asp:SqlDataSource ID="SqlDataSourceLocaties" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [magazijnid], [capaciteit], [naam] FROM [Locatie]">
    </asp:SqlDataSource>
     <br />
    <asp:SqlDataSource ID="SqlDataSourceOpdrachtgevers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [naam] FROM [Opdrachtgever]">
    </asp:SqlDataSource>
     <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
     </dx:ASPxGridViewExporter>
</asp:Content>
