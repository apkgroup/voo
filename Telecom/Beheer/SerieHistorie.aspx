﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="SerieHistorie.aspx.vb" Inherits="Telecom.SerieHistorie" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
     <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" Text="" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
                </dx:ASPxLabel>

            &nbsp;<table style="width:50%;">
                <tr>
                    <td style="text-align: right">Serienummer:</td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextBoxSerie" runat="server" Width="170px">
                        </dx:ASPxTextBox>
            <dx:ASPxButton ID="ASPxButtonToevoegen0" runat="server" Width="50px">
                <Image IconID="dashboards_zoom_svg_16x16">
                </Image>
            </dx:ASPxButton>

                    </td>
                    
                </tr>
            </table>
            <h2>&nbsp;</h2>

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerie">
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="0">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Naam" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn FieldName="stockmagazijnId" VisibleIndex="2" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="datum" VisibleIndex="3">
            </dx:GridViewDataDateColumn>
           
            <dx:GridViewDataTextColumn FieldName="beweging" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn FieldName="gebruiker" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn FieldName="opmerking" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
           
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceSerie" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT 
sr.serienummer,m.Naam
      ,sb.[stockmagazijnId]
      ,[datum]
      ,[beweging]
      ,[gebruiker]
      ,[opmerking]
  FROM [Voo].[dbo].[Stockbeweging] sb 
  inner join voo.dbo.StockMagazijn sm on sb.stockmagazijnId = sm.id
  inner join voo.dbo.Magazijn m on sm.MagazijnId = m.id
  inner join voo.dbo.StockbewegingSerienummer sbsr on sbsr.stockbewegingid = sb.id
  inner join voo.dbo.Serienummer sr on sbsr.serienummerId = sr.id
  where sr.serienummer=@serienummer">
        <SelectParameters>
            <asp:QueryStringParameter Name="serienummer" QueryStringField="serie" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>