﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="OverzichtMateriaal.aspx.vb" Inherits="Telecom.OverzichtMateriaal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        </h2>
    <br />
    <br />
    <asp:SqlDataSource ID="SqlDataSourceStockMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [StockMagazijn] WHERE [id] = @id" InsertCommand="INSERT INTO [StockMagazijn] ([MateriaalId], [MagazijnId], [Aantal]) VALUES (@MateriaalId, @MagazijnId, @Aantal)" SelectCommand="SELECT s.[id],[naam],b.Article,b.Description, [MateriaalId], [MagazijnId], [Aantal]
 FROM [StockMagazijn] s
INNER JOIN Magazijn m on s.MagazijnId = m.id 
INNER JOIN Basismateriaal b on s.MateriaalId = b.id WHERE b.id in (select mo.materiaalId from [MateriaalOpdrachtgevers] mo where opdrachtgeverId = @opdrachtgeverId and (actief = 1 or actief is null))
order by b.id
" UpdateCommand="UPDATE [StockMagazijn] SET [MateriaalId] = @MateriaalId, [MagazijnId] = @MagazijnId, [Aantal] = @Aantal WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceStockMagazijn" KeyFieldName="id">
        <SettingsDetail ShowDetailRow="True" />
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2" Caption="Serie" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="beschrijving" VisibleIndex="5" Caption="Status" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Lotnr" FieldName="lotNr" VisibleIndex="6" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Creation" FieldName="datumGemaakt" ShowInCustomizationForm="True" VisibleIndex="7">
                        </dx:GridViewDataDateColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <SettingsPager PageSize="25">
        </SettingsPager>
        <SettingsEditing Mode="Batch">
        </SettingsEditing>
        <Settings ShowFilterRow="True" ShowHeaderFilterButton="True" />
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="0" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="naam" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MateriaalId" VisibleIndex="4" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MagazijnId" VisibleIndex="5" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Aantal" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT serienummer, lotNr, datumGemaakt, st.beschrijving  FROM [Serienummer] sr left join Status st on sr.statusId = st.id WHERE ([StockMagazijnId] = @StockMagazijnId) and (uitgeboekt &lt;&gt; 1 or uitgeboekt is null)
">
        <SelectParameters>
            <asp:SessionParameter Name="StockMagazijnId" SessionField="stockmagazijn" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>