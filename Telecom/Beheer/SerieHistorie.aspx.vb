﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class SerieHistorie
    Inherits System.Web.UI.Page

    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then

            Literal2.Text = "Historie serienummer"

        Else


        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        Session("opdrachtgeverSession") = Session("opdrachtgever")

        If String.IsNullOrEmpty(Request.QueryString("serie")) Then
            ASPxGridView1.ClientVisible = False
        End If
        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer
        If Not Page.IsPostBack And String.IsNullOrEmpty(Session("MagazijnId")) Then



            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using

            If Not Session("isingelogd") Then
                Session("url") = Request.Url.AbsoluteUri
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
            If Not Session("isadmin") And Not Session("level") = 10 Then
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
        End If

        vertaal(Session("taal"))


    End Sub





    Protected Sub ASPxGridView1_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles ASPxGridView1.RowInserted



    End Sub

    Protected Sub ASPxButtonToevoegen0_Click(sender As Object, e As EventArgs) Handles ASPxButtonToevoegen0.Click
        Response.Redirect("~/Beheer/SerieHistorie.aspx?serie=" & ASPxTextBoxSerie.Value, False)
    End Sub
End Class