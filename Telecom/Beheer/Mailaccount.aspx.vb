﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.Web
Imports DevExpress.Utils

Public Class Mailaccount
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Page.IsPostBack Then
            Me.cboMailaccount.Value = Session("mailaccount")
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Protected Sub btnBevestig_Click(sender As Object, e As EventArgs) Handles btnBevestig.Click
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Try
                Dim cmd As New SqlCommand("UPDATE Mailaccount SET Mailaccount=@mailaccount", cn)
                Dim par As New SqlParameter("@mailaccount", SqlDbType.NVarChar, 50) With {.Value = cboMailaccount.Value}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Mailaccount aangepast door {0} {1} naar {2}", Session("naam"), Session("voornaam"), cboMailaccount.Value)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij aanpassing mailaccount door {0} {1}, omschrijving: {2}", Session("naam"), Session("voornaam"), ex.Message)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End Try
           
        End Using
        Session("mailaccount") = cboMailaccount.Value
        Dim rootGroup As LayoutGroup = TryCast(ASPxFormLayout1.Items(0), LayoutGroup)
        rootGroup.Visible = False
        rootGroup = TryCast(ASPxFormLayout1.Items(1), LayoutGroup)
        rootGroup.Visible = True
        lblBevestiging.Text = "De account voor het versturen van e-mails is aangepast."
    End Sub
End Class