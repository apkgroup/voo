﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="MateriaalOpdrachtgevers.aspx.vb" Inherits="Telecom.MateriaalOpdrachtgeversPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}

    </style>
    <script type="text/javascript">
        var groep = 0;
        function OnGroepChanged(cmbGroepen) {
            groep = cmbGroepen.GetValue();
            lijstLeden.PerformCallback(cmbGroepen.GetValue());
        }
      
        function Lidweg(id) {
           
            lijstLeden.DeleteRowByKey(id);
        }

        function Lidnaargroep(id) {
           
            lijstGebruikers.PerformCallback(id);
         
        }
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>  
     
    <br /> 
    <table>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabelGroepSelect" runat="server" Text="ASPxLabel">
                </dx:ASPxLabel>
            </td>
            <td>


                <dx:ASPxComboBox ID="cmbGroepen" ClientInstanceName="cmdGroepen" runat="server" DataSourceID="SqlDataSourceGroepen" EnableSynchronization="False" TextField="Description" ValueField="id" ValueType="System.Int32">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
	OnGroepChanged(s);
}" />
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSourceGroepen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT distinct b.[id], [Article], [Description]
FROM [Basismateriaal] b  inner join [MateriaalOpdrachtgevers] m on b.id = m.materiaalId
where m.opdrachtgeverId in (select opdrachtgeverId from AdminOpdrachtgevers a where a.gebruikerId=@gebruikerid)">
                    <SelectParameters>
                        <asp:SessionParameter Name="gebruikerid" SessionField="userid" />
                    </SelectParameters>
                </asp:SqlDataSource>


            </td>
            <td>
                &nbsp;</td>
            <td>

                &nbsp;</td>
            <td>

                &nbsp;</td>
        </tr>
        <tr>
            <td style="vertical-align:top;" colspan="2">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabelGroepleden" runat="server" Text="ASPxLabel">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">
                             <dx:ASPxGridView ID="lijstLeden" runat="server" AutoGenerateColumns="False" ClientInstanceName="lijstLeden" DataSourceID="SqlDataSourceLeden" KeyFieldName="id">
                                 <Columns>
                                     <dx:GridViewDataTextColumn FieldName="naam" ReadOnly="True" VisibleIndex="0">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataHyperLinkColumn Caption=" " FieldName="id" ReadOnly="True" VisibleIndex="1">
                                         <PropertiesHyperLinkEdit ImageUrl="~/images/Close_16x16.png" NavigateUrlFormatString="javascript:Lidweg({0});" TextFormatString="">
                                         </PropertiesHyperLinkEdit>
                                      
                                     </dx:GridViewDataHyperLinkColumn>
                                 </Columns>
                                 <SettingsPager Mode="ShowAllRecords" Visible="False">
                                 </SettingsPager>
                                 <Settings ShowColumnHeaders="False" />
                                 <SettingsText EmptyDataRow="Aucune donnée chargée... groupe sélect svp" />
                                 <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                             </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSourceLeden" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT 
naam,
 mo.id FROM MateriaalOpdrachtgevers mo INNER JOIN Opdrachtgever o
ON mo.opdrachtgeverid=o.id  WHERE (mo.materiaalId=@materiaal)" DeleteCommand="DELETE FROM MateriaalOpdrachtgevers WHERE (id=@id)">
                    <DeleteParameters>
                        <asp:Parameter Name="id" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="cmbGroepen" Name="materiaal" PropertyName="Value" />
                    </SelectParameters>
                </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>

               


            </td>
            <td></td>
            <td style="vertical-align:top;" colspan="2">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabelGebruikers" runat="server" Text="ASPxLabel">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                       <td>
                             <dx:ASPxGridView ID="lijstGebruikers" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGebruikers" KeyFieldName="id" ClientInstanceName="lijstGebruikers">
                                 <Columns>
                                     <dx:GridViewDataTextColumn FieldName="naam" ReadOnly="True" VisibleIndex="1" ShowInCustomizationForm="True">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataHyperLinkColumn FieldName="opdrachtgeverId" ReadOnly="True" VisibleIndex="0">
                                         <PropertiesHyperLinkEdit ImageUrl="~/images/Backward_16x16.png" NavigateUrlFormatString="javascript:Lidnaargroep({0});" TextFormatString="">
                                         </PropertiesHyperLinkEdit>
                                         <EditFormSettings Visible="False" />
                                     </dx:GridViewDataHyperLinkColumn>
                                 </Columns>
                                 <SettingsPager Mode="ShowAllRecords" Visible="False">
                                 </SettingsPager>
                                 <Settings ShowColumnHeaders="False" />
                                 <SettingsText EmptyDataRow="Geen data geladen...selecteer eerst een groep aub" />
                                 <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                 <ClientSideEvents
    EndCallback="function (s, e) {
        lijstLeden.PerformCallback(groep);
    }"
/>
                             </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSourceGebruikers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="Select o.naam, opdrachtgeverId, gebruikerid from Adminopdrachtgevers a inner join opdrachtgever o on o.id = a.opdrachtgeverid where gebruikerId=@gebruikerid">
                    <SelectParameters>
                        <asp:SessionParameter Name="gebruikerid" SessionField="userid" />
                    </SelectParameters>

                </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</asp:Content>
