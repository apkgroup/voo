﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="MinMaxBeheer.aspx.vb" Inherits="Telecom.MinMaxBeheer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 79px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
    <p>&nbsp;</p>
     <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" Text="" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
                </dx:ASPxLabel>

                <dx:ASPxButton ID="ASPxButtonSet0" runat="server" AutoPostBack="false" Width="50px" Text="Zet alle min-maxen op 0" CssClass="minimargin">
                    
                </dx:ASPxButton>

                <dx:ASPxButton ID="ASPxButtonSetDefault" runat="server" AutoPostBack="false" Width="50px" Text="Reset naar standaard" CssClass="minimargin">
                  
                </dx:ASPxButton>

                <dx:ASPxButton ID="ASPxButtonToevoegen" runat="server" AutoPostBack="false" Width="50px" Text="Min Max toevoegen OF updaten" CssClass="minimargin">
                     <ClientSideEvents Click="function(s, e) { pnlForm.SetVisible(true); }" />  
                </dx:ASPxButton>

    <dx:ASPxPanel ID="pnlForm" runat="server"  ClientVisible="false" ClientInstanceName="pnlForm" CssClass="minimargin" Paddings-PaddingLeft="5px" Paddings-PaddingBottom="5px" Width="400px">  
        <Paddings PaddingBottom="5px" />
    <PanelCollection>  
        <dx:PanelContent>  
            &nbsp;<table style="width:50%;">
                <tr>
                    <td style="text-align: right" class="auto-style1">Materiaal:</td>
                    <td>
                     
                        <dx:ASPxComboBox ID="ASPxComboBoxArtikel" runat="server" DataSourceID="SqlDataSource1" TextField="Description" ValueField="id">
                        </dx:ASPxComboBox>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT distinct article,CONCAT(article, ' | ', description) as description, b.id FROM [Basismateriaal] b
INNER JOIN MateriaalOpdrachtgevers o ON b.id =o.materiaalId
WHERE o.opdrachtgeverId = @OpdrachtgeverId and (o.actief = 1 or o.actief is null)">
                            <SelectParameters>
                                <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                     
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right" class="auto-style1">Min:</td>
                    <td>
                        <dx:ASPxSpinEdit ID="ASPxSpinEditMin" runat="server" Width="170px" Number="0" >
                        </dx:ASPxSpinEdit>
                    </td>
                    
                </tr>
                <tr>
                    <td style="text-align: right" class="auto-style1">Max:</td>
                    <td>
                        <dx:ASPxSpinEdit ID="ASPxSpinEditMax" runat="server" Width="170px" Number="0">
                        </dx:ASPxSpinEdit>
                    </td>
                </tr>
                  
          
            </table>
            <h2>&nbsp;</h2>
            <dx:ASPxButton ID="ASPxButtonToevoegen0" runat="server" Width="50px">
                <Image IconID="actions_add_32x32office2013">
                </Image>
            </dx:ASPxButton>
        </dx:PanelContent>  
    </PanelCollection>  
        <Border BorderColor="Silver" BorderWidth="1px" />
</dx:ASPxPanel>  
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceLocatie">
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Article" ReadOnly="True" VisibleIndex="0" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="1" ReadOnly="True" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn FieldName="min" ReadOnly="True" VisibleIndex="2" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="max" ReadOnly="True" VisibleIndex="3" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn Caption="Standaard" FieldName="standaard" Visible="False" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
           
        </Columns>
         <FormatConditions>
            <dx:GridViewFormatConditionHighlight FieldName="standaard" Expression="[standaard] < 1"  Format="YellowFillWithDarkYellowText" ApplyToRow="True" />
          
        </FormatConditions>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceLocatie" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT DISTINCT
mo.opdrachtgeverId,
      [Article]
      ,[Description]
      ,mo.[min]
      ,mo.[max]
,1 as standaard
  FROM [Voo].[dbo].[Basismateriaal] b
  inner join [Voo].[dbo].StockMagazijn s on b.id = s.MateriaalId
  inner join [Voo].[dbo].magazijn m on m.id = s.MagazijnId
 inner join voo.dbo.MateriaalOpdrachtgevers mo on mo.materiaalId = b.id
  where (mo.actief = 1 or mo.actief is null) and m.id = @magazijnid
  and b.id not in (select materiaalId from voo.dbo.minmax where magazijnId = @magazijnid ) and mo.opdrachtgeverId = m.OpdrachtgeverId
      UNION
  SELECT DISTINCT mo.opdrachtgeverId,
      [Article]
      ,[Description]
      ,mm.[min]
      ,mm.[max]
      ,    CASE
  when (select [min] from [Voo].[dbo].MateriaalOpdrachtgevers b1 where  b1.materiaalId = b.id and b1.opdrachtgeverId = m.OpdrachtgeverId)  = 
 (select [min] from [Voo].[dbo].minmax mm1 where  mm1.materiaalId = b.id and mm1.magazijnId  = @magazijnid )
  AND (select [max] from [Voo].[dbo].MateriaalOpdrachtgevers b1 where  b1.materiaalId = b.id and b1.opdrachtgeverId = m.OpdrachtgeverId)  = 
  (select [max] from [Voo].[dbo].minmax mm1 where  mm1.materiaalId = b.id  and mm1.magazijnId  = @magazijnid  ) THEN 1
  else 0
  end as altered
  FROM [Voo].[dbo].[Basismateriaal] b
  inner join [Voo].[dbo].StockMagazijn s on b.id = s.MateriaalId
  inner join [Voo].[dbo].magazijn m on m.id = s.MagazijnId
  inner join voo.dbo.minmax mm on mm.materiaalId = b.id and mm.magazijnId = m.id
 inner join voo.dbo.MateriaalOpdrachtgevers mo on mo.materiaalId = b.id
  where  mm.[min] is not null and m.id = @magazijnid and  (mo.actief = 1 or mo.actief is null)   and mo.opdrachtgeverId = m.OpdrachtgeverId" DeleteCommand="DELETE FROM [Locatie] WHERE [id] = @id" InsertCommand="INSERT INTO [Locatie] ([magazijnid], [capaciteit], [naam]) VALUES (@magazijnid, @capaciteit, @naam)" UpdateCommand="UPDATE [Locatie] SET [magazijnid] = @magazijnid, [capaciteit] = @capaciteit, [naam] = @naam WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="magazijnid" Type="Int32" />
            <asp:Parameter Name="capaciteit" Type="Int32" />
            <asp:Parameter Name="naam" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ASPxComboBoxMagazijn" Name="magazijnid" PropertyName="Value" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="magazijnid" Type="Int32" />
            <asp:Parameter Name="capaciteit" Type="Int32" />
            <asp:Parameter Name="naam" Type="String" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) ">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>