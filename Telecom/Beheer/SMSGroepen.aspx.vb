﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class SMSGroepen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub
    Protected Sub ASPxCallback1_Callback(ByVal sender As Object, ByVal e As CallbackEventArgs)
        If Not (String.IsNullOrEmpty(e.Parameter)) Then
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = "SELECT id FROM SMSgroepen WHERE (Naam=@naam)"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@naam", SqlDbType.NVarChar, 25) With {.Value = e.Parameter}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                Dim blbestaat As Boolean = dr.HasRows
                dr.Close()
                If blbestaat Then
                    ASPxTextBoxNieuweGroep.ErrorText = "Deze groep bestaat al."
                    ASPxTextBoxNieuweGroep.IsValid = False
                    Return
                End If
                Try
                    Dim iGroep As Integer = 0
                    s_SQL = "INSERT INTO SMSgroepen (Naam, Eigenaar, Actief) VALUES (@naam, @eigenaar, 1); SELECT SCOPE_IDENTITY()"
                    cmd = New SqlCommand(s_SQL, cn)
                    par = New SqlParameter("@naam", SqlDbType.NVarChar, 25) With {.Value = e.Parameter}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@eigenaar", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    iGroep = cmd.ExecuteScalar
                    If iGroep > 0 Then
                        e.Result = String.Format("1;{0};{1}", iGroep, e.Parameter)
                    Else
                        e.Result = "0;0;0"
                    End If
                    cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("SMSgroep {0} toegevoegd door {1} {2}", e.Parameter, Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                    cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij toevoeging SMSgroep door {0} {1}, omschrijving: {2}", Session("naam"), Session("voornaam"), ex.Message)}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                End Try

            End Using
        End If
    End Sub
   

    Private Sub VulLedenlijst(ByVal iGroep As Integer)

        SqlDataSourceLeden.SelectParameters(0).DefaultValue = iGroep
        lijstLeden.DataBind()

    End Sub

    Private Sub lijstLeden_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs) Handles lijstLeden.CustomCallback
      VulLedenlijst(Convert.ToInt32(e.Parameters))

    End Sub

    Private Sub lijstGebruikers_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs) Handles lijstGebruikers.CustomCallback
        If String.IsNullOrEmpty(e.Parameters) Then Return
        If e.Parameters.ToString.Length > 4 Then
            Dim eId As Int32 = Convert.ToInt32(e.Parameters.ToString.Replace("9999", ""))
            Dim varNaam As String = ""
            Dim varGsm As String = ""
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = "SELECT Naam, GSMnummer FROM SMSgroepenLeden WHERE (id=@id)"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = eId}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()
                    varNaam = dr.GetString(0)
                    varGsm = dr.GetString(1)
                End If
                dr.Close()
                If varNaam <> "" Then
                    s_SQL = "INSERT INTO SMSgroepenLeden (Groep, Naam, GSMnummer) VALUES (@groep, @naam, @gsm)"
                    cmd = New SqlCommand(s_SQL, cn)
                    par = New SqlParameter("@groep", SqlDbType.Int) With {.Value = cmbGroepen.Value}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@naam", SqlDbType.NVarChar, 50) With {.Value = varNaam}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@gsm", SqlDbType.NVarChar, 20) With {.Value = varGsm}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                End If
            End Using
        Else
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = "SELECT id FROM SMSgroepenLeden WHERE (Groep=@groep) AND (Gebruiker=@gebruiker)"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@groep", SqlDbType.Int) With {.Value = cmbGroepen.Value}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Convert.ToInt32(e.Parameters)}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                Dim blbestaat As Boolean = dr.HasRows
                dr.Close()
                If Not blbestaat Then
                    s_SQL = "INSERT INTO SMSgroepenLeden (Groep, Gebruiker, Naam, GSMnummer) VALUES (@groep,@gebruiker, (SELECT Werknemer FROM (SELECT [ON], NR, NAAM + ' ' + VNAAM As " _
                        & "Werknemer FROM Elly_SQL.dbo.WERKN) vwWn INNER JOIN Gebruikers G ON vwWn.[ON]=G.Werkg AND vwWn.NR=G.Werkn WHERE (G.id=@gebruiker))" _
                        & ", (SELECT GSM FROM Gebruikers G WHERE (G.id=@gebruiker)))"
                    cmd = New SqlCommand(s_SQL, cn)
                    par = New SqlParameter("@groep", SqlDbType.Int) With {.Value = cmbGroepen.Value}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Convert.ToInt32(e.Parameters)}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                End If
            End Using
        End If

       
    


    End Sub

    Private Sub lijstLeden_RowDeleted(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletedEventArgs) Handles lijstLeden.RowDeleted
        VulLedenlijst(cmbGroepen.Value)
    End Sub

    Protected Sub ASPxCallback2_Callback(source As Object, e As CallbackEventArgs)
        If String.IsNullOrEmpty(e.Parameter) Then Return
        Dim varpar As String() = e.Parameter.Split(New Char() {";"})
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "SELECT id FROM SMSgroepenLeden WHERE (Groep=@groep) AND (Naam=@naam)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@groep", SqlDbType.Int) With {.Value = cmbGroepen.Value}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@naam", SqlDbType.NVarChar, 50) With {.Value = varpar(0)}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            Dim blbestaat As Boolean = dr.HasRows
            dr.Close()
            If Not blbestaat Then
                s_SQL = "INSERT INTO SMSgroepenLeden (Groep,  Naam, GSMnummer) VALUES (@groep, @naam, @gsm)"
                cmd = New SqlCommand(s_SQL, cn)
                par = New SqlParameter("@groep", SqlDbType.Int) With {.Value = cmbGroepen.Value}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@naam", SqlDbType.NVarChar, 50) With {.Value = varpar(0)}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@gsm", SqlDbType.NVarChar, 20) With {.Value = varpar(1)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End If
            e.Result = "OK"
        End Using

    End Sub

End Class