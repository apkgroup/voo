﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Bewegingen.aspx.vb" Inherits="Telecom.Bewegingen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h2>Beheer Bewegingen</h2>    
    <br />
    <p><span style="font-family:Calibri;font-size:small;">Indien je een artikeltype aanduidt voor een beweging, zal deze beweging enkel zichtbaar zijn indien het een artikel betreft van dat specifieke type.</span></p>
    <br />
     <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceBewegingen" KeyFieldName="id">
          <SettingsText EmptyDataRow="Geen data gevonden... " />
         <Columns>
             <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
             </dx:GridViewCommandColumn>
             <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="False">
                 <EditFormSettings Visible="False" />
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn FieldName="Omschrijving" VisibleIndex="2" Width="350px">
                 <PropertiesTextEdit MaxLength="50" Width="350px">
                 </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
               <dx:GridViewDataComboBoxColumn Caption="Type" FieldName="ArtikelType" VisibleIndex="3">
                <PropertiesComboBox DataSourceID="SqlDataSourceArtikeltypes" TextField="Omschrijving" ValueField="id" ValueType="System.Int32">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
             <dx:GridViewDataCheckColumn FieldName="Actief" VisibleIndex="4">
                 <HeaderStyle HorizontalAlign="Center" />
             </dx:GridViewDataCheckColumn>
         </Columns>
         <SettingsDataSecurity AllowDelete="False" />
         <SettingsPager PageSize="20">
            <PageSizeItemSettings Caption="Items per pagina:" Items="10, 20, 50" ShowAllItem="True" Visible="True">
            </PageSizeItemSettings>
        </SettingsPager>
          <SettingsCommandButton>
             <NewButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Nieuw" Url="~/images/Add_32x32.png" Width="16px">
                 </Image>
             </NewButton>
             <UpdateButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewaren" Url="~/images/Save_16x16.png" Width="16px">
                 </Image>
             </UpdateButton>
             <CancelButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Annuleren" Url="~/images/annuleren.png" Width="16px">
                 </Image>
             </CancelButton>
             <EditButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewerken" Url="~/images/Edit.png" Width="16px">
                 </Image>
             </EditButton>
             <DeleteButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Verwijderen" Url="~/images/Folders-OS-Recycle-Bin-Full-Metro-icon.png" Width="16px">
                 </Image>
             </DeleteButton>
         </SettingsCommandButton>
     </dx:ASPxGridView>
     <asp:SqlDataSource ID="SqlDataSourceBewegingen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [stock_Bewegingen] WHERE [id] = @id" InsertCommand="INSERT INTO [stock_Bewegingen] ([Omschrijving], [Actief], [ArtikelType]) VALUES (@Omschrijving, @Actief, @ArtikelType)" SelectCommand="SELECT [id], [Omschrijving], [Actief], [ArtikelType] FROM [stock_Bewegingen] ORDER BY [Omschrijving]" UpdateCommand="UPDATE [stock_Bewegingen] SET [Omschrijving] = @Omschrijving, [Actief] = @Actief, [ArtikelType] = @ArtikelType WHERE [id] = @id">
         <DeleteParameters>
             <asp:Parameter Name="id" Type="Int32" />
         </DeleteParameters>
         <InsertParameters>
             <asp:Parameter Name="Omschrijving" Type="String" />
             <asp:Parameter Name="Actief" Type="Boolean" />
             <asp:Parameter Name="ArtikelType" Type="Int32" />
         </InsertParameters>
         <UpdateParameters>
             <asp:Parameter Name="Omschrijving" Type="String" />
             <asp:Parameter Name="Actief" Type="Boolean" />
             <asp:Parameter Name="ArtikelType" Type="Int32" />
             <asp:Parameter Name="id" Type="Int32" />
         </UpdateParameters>
     </asp:SqlDataSource>
    <br />   
      <asp:SqlDataSource ID="SqlDataSourceArtikeltypes" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Omschrijving] FROM [stock_Artikels_Types] WHERE (isnull(Actief,0)=1) ORDER BY [Omschrijving]"></asp:SqlDataSource>

</asp:Content>
