﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="BobijnMeetstaat.aspx.vb" Inherits="Telecom.BobijnMeetstaat" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function OnInit(s, e) {  
    setTimeout(ChangeHint, 50);  
    s.ActiveItemChanged.AddHandler(function (s, args) { setTimeout(ChangeHint, 50); })  
}  

function ChangeHint() {  
    var src = imageZoom.GetMainElement().getElementsByClassName("dxiz-clipPanel")[0].getElementsByTagName("img")[0].src;  
    var hintElem = imageZoom.GetMainElement().getElementsByClassName("dxiz-hint")[0];  
    if (src.indexOf("empty") >= 0) {  
        if (!hintElem.classList.contains("MyClass"))  
            hintElem.classList.add("MyClass");  
    }  
    else {  
        if (hintElem.classList.contains("MyClass"))  
            hintElem.classList.remove("MyClass");  
    }  
}  
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>

          

      
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMagazijnen0" EnableRowsCache="False">
        <SettingsPager PageSize="25">
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="van" VisibleIndex="0" ReadOnly="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="hoeveelheid" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="tot" VisibleIndex="2" ReadOnly="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="datum" VisibleIndex="3">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="verbruikt op" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn Caption="Opmerking" FieldName="ordernummer" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn Caption="Verbruikt door" FieldName="ExNaam" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Adres" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
           
        </Columns>
    </dx:ASPxGridView>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen0" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT 
      b.aantal + coalesce((select sum(hoeveelheid) from Verbruiklijn v1  inner join  [Voo].[dbo].verbruik vd1 on v1.verbruikId = vd1.id where vd1.datum &gt; vd.datum and v1.bobijnId = @bobijnid),0)+[hoeveelheid] as 'van'
      ,[hoeveelheid]
	  ,b.aantal + coalesce((select sum(hoeveelheid) from Verbruiklijn v1  inner join  [Voo].[dbo].verbruik vd1 on v1.verbruikId = vd1.id where vd1.datum &gt; vd.datum and v1.bobijnId = @bobijnid),0) as 'tot'
	,vd.datum
	,m.Naam as 'verbruikt op'
,vd.ordernummer
,g.ExNaam
,concat(vd.straat,' ',vd.stad) as Adres
  FROM [Voo].[dbo].[Verbruiklijn] vl 
  inner join  [Voo].[dbo].bobijn b on vl.bobijnId = b.id
  inner join  [Voo].[dbo].verbruik vd on vl.verbruikId = vd.id
  inner join  [Voo].[dbo].magazijn m on vd.verbruiktop = m.id
  left join  [Voo].[dbo].gebruikers g on vd.gebruiker = g.id
  where bobijnId =@bobijnid">
        <SelectParameters>
            <asp:QueryStringParameter Name="bobijnid" QueryStringField="id" />
        </SelectParameters>
    </asp:SqlDataSource>
    </asp:Content>