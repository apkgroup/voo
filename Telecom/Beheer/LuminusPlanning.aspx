﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="LuminusPlanning.aspx.vb" Inherits="Telecom.LuminusPlanning" %>
<%@ Register assembly="DevExpress.Web.ASPxScheduler.v19.2, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxScheduler" tagprefix="dxwschs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h2>
         <asp:Literal ID="Literal2" runat="server"></asp:Literal>
     </h2>
     <dx:ASPxButton ID="ASPxButton3" runat="server" AutoPostBack="False" Text="Exporter" Visible="False">
         <ClientSideEvents Click="function(s, e) {
            ASPxGridView1.PerformCallback('CEXP');
	
}" />
     </dx:ASPxButton>
     <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" ForeColor="#009933">
     </dx:ASPxLabel>
    <br />
     <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" ClientInstanceName="ASPxGridView1" KeyFieldName="id">
                            <ClientSideEvents EndCallback=" function(s, e) { 
                if (s.cpExport) {
                     
                    delete s.cpExport;
                    window.open('Export.aspx', '_blank');
                }
            } 
"/>
        <SettingsPager NumericButtonCount="20" PageSize="20">
        </SettingsPager>
                            <Settings ShowFilterBar="Visible" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" VisibleIndex="0" ReadOnly="True" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2" Caption="Serienummer">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="installatiedatum" VisibleIndex="3" Caption="Installatiedatum" Settings-AllowHeaderFilter="true">
<Settings AllowHeaderFilter="True"></Settings>

            </dx:GridViewDataDateColumn>

            <dx:GridViewDataComboBoxColumn Caption="Technieker" FieldName="gebruikerId" ShowInCustomizationForm="True" VisibleIndex="1">
                <PropertiesComboBox DataSourceID="SqlDataSourceTechniekers" TextField="naam" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
        <SettingsPopup>
            <HeaderFilter MinHeight="400px" MinWidth="600px">
                <SettingsAdaptivity Mode="OnWindowInnerWidth" SwitchAtWindowInnerWidth="768" />
            </HeaderFilter>
        </SettingsPopup>
    </dx:ASPxGridView>
     <asp:SqlDataSource ID="SqlDataSourceTechniekers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT id, ISNULL(ExNaam, (select top 1 concat(NAAM, ' ', VNAAM) from Elly_SQL.dbo.werkn where [ON] = g.Werkg and [NR] = g.werkn)) as naam FROM voo.dbo.[Gebruikers] g"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [LuminusPlanning]">
    </asp:SqlDataSource>
     <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
     </dx:ASPxGridViewExporter>
</asp:Content>
