﻿Imports DevExpress.Export

Public Class VerbruikPerIngave
    Inherits System.Web.UI.Page

    Protected Sub vertaal(taalid)
        If taalid = 1 Then
            Literal2.Text = "Verbruik per ingave"

        Else
            ASPxGridView1.Columns("Werknemer").Caption = "Empl."
            ASPxGridView1.Columns("beschrijving").Caption = "Statut"
            ASPxGridView1.Columns("Datum").Caption = "Date"
            ASPxGridView1.Columns("groep").Caption = "Groupe"
            Literal2.Text = "Gérer commandes"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Page.IsPostBack Then
        End If

    End Sub


End Class