﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="GrabbelBeheer.aspx.vb" Inherits="Telecom.GrabbelBeheer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
     <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" Text="" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
                </dx:ASPxLabel>

                <dx:ASPxButton ID="ASPxButtonToevoegen" runat="server" AutoPostBack="false" Width="50px" Text="Nieuw materiaal toevoegen" CssClass="minimargin" ClientVisible="False">
                     <ClientSideEvents Click="function(s, e) { pnlForm.SetVisible(true); }" />  
                </dx:ASPxButton>

    <dx:ASPxPanel ID="pnlForm" runat="server"  ClientVisible="False" ClientInstanceName="pnlForm" CssClass="minimargin" Paddings-PaddingLeft="5px" Paddings-PaddingBottom="5px" Width="400px">  
        <Paddings PaddingBottom="5px" />
    <PanelCollection>  
        <dx:PanelContent>  
            &nbsp;<table style="width:50%;">
                <tr>
                    <td style="text-align: right">Omschrijving:</td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextBoxNaam" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td style="text-align: right">Eenheid:</td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextBoxEH" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td style="text-align: right">Max:</td>
                    <td>
                        <dx:ASPxSpinEdit ID="ASPxSpinEditHoeveelheid" runat="server" Number="0">
                        </dx:ASPxSpinEdit>
                    </td>
                </tr>
                  
          
            </table>
            <h2>&nbsp;</h2>
            <dx:ASPxButton ID="ASPxButtonToevoegen0" runat="server" Width="50px">
                <Image IconID="actions_add_32x32office2013">
                </Image>
            </dx:ASPxButton>
        </dx:PanelContent>  
    </PanelCollection>  
        <Border BorderColor="Silver" BorderWidth="1px" />
</dx:ASPxPanel>  
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceLocatie" KeyFieldName="id">
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0" ShowNewButtonInHeader="True">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Omschrijving" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn FieldName="Eenheid" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="AF" VisibleIndex="6">
            </dx:GridViewDataCheckColumn>
           
            <dx:GridViewDataTextColumn Caption="Artikel" FieldName="article" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn Caption="Volgorde" FieldName="volgorde" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn Caption="Max. te bestellen" FieldName="hoeveelheid" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
           
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceLocatie" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Grabbelmateriaal] WHERE ([opdrachtgeverId] = @opdrachtgeverId)" DeleteCommand="DELETE FROM [Grabbelmateriaal] WHERE [id] = @id" InsertCommand="INSERT INTO [Grabbelmateriaal] ([Omschrijving], [opdrachtgeverId], [Eenheid], [AF], [article],[hoeveelheid]) VALUES (@Omschrijving, @opdrachtgeverId, @Eenheid, 0, @article,@hoeveelheid)" UpdateCommand="UPDATE [Grabbelmateriaal] SET [Omschrijving] = @Omschrijving, [opdrachtgeverId] = @opdrachtgeverId, [Eenheid] = @Eenheid, [AF] = @AF, [article] = @article, [volgorde] = @volgorde , [hoeveelheid] = @hoeveelheid WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Omschrijving" Type="String" />
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
            <asp:Parameter Name="Eenheid" Type="String" />
            <asp:Parameter Name="article" Type="String" />
            <asp:Parameter Name="hoeveelheid" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Omschrijving" Type="String" />
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
            <asp:Parameter Name="Eenheid" Type="String" />
            <asp:Parameter Name="AF" Type="Boolean" />
            <asp:Parameter Name="article" Type="String" />
            <asp:Parameter Name="volgorde" />
            <asp:Parameter Name="hoeveelheid" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>