﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="StockBewegingBeheer.aspx.vb" Inherits="Telecom.StockBewegingBeheer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        Stockbewegingen
        </h2>
    <br />
    <br />
    <asp:SqlDataSource ID="SqlDataSourceStockMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [StockMagazijn] WHERE [id] = @id" InsertCommand="INSERT INTO [StockMagazijn] ([MateriaalId], [MagazijnId], [Aantal]) VALUES (@MateriaalId, @MagazijnId, @Aantal)" SelectCommand="SELECT sb.[id]
      ,m.Naam as 'Magazijn'
      ,b.Article
      ,b.[Description]
      ,[datum]
      ,[beweging],
case 
when gebruiker ='API' then 'api'
else coalesce((select exnaam from gebruikers where id = gebruiker),'')
end as 'uitgevoerddoor'
      ,[opmerking]
  FROM [Voo].[dbo].[Stockbeweging] sb
  inner join StockMagazijn sm on sb.stockmagazijnId = sm.id
  inner join magazijn m on sm.MagazijnId = m.id
  inner join Basismateriaal b on sm.MateriaalId = b.id
    left join Gebruikers g on sb.gebruiker = g.id
where m.OpdrachtgeverId = @opdrachtgeverId
order by sb.id desc" UpdateCommand="UPDATE [StockMagazijn] SET [MateriaalId] = @MateriaalId, [MagazijnId] = @MagazijnId, [Aantal] = @Aantal WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
     <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceStockMagazijn" KeyFieldName="id">
        <SettingsDetail ShowDetailRow="True" />
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2" Caption="Serie" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="beschrijving" VisibleIndex="5" Caption="Status" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Lotnr" FieldName="lotNr" VisibleIndex="6" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Creation" FieldName="datumGemaakt" ShowInCustomizationForm="True" VisibleIndex="7">
                        </dx:GridViewDataDateColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <SettingsPager PageSize="25">
        </SettingsPager>
        <SettingsEditing Mode="Batch">
        </SettingsEditing>
        <Settings ShowFilterBar="Auto" ShowHeaderFilterButton="True" ShowFilterRow="True" />
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <SettingsPopup>
            <HeaderFilter MinHeight="350px" MinWidth="600px">
            </HeaderFilter>
        </SettingsPopup>
        <Columns>
            <dx:GridViewCommandColumn ShowClearFilterButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Magazijn" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="beweging" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="uitgevoerddoor" VisibleIndex="7" Caption="Uitgevoerd door" ReadOnly="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="opmerking" VisibleIndex="8">
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="datum" VisibleIndex="5">
                <PropertiesDateEdit DisplayFormatString="">
                </PropertiesDateEdit>
            </dx:GridViewDataDateColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT serienummer, lotNr, datumGemaakt, st.beschrijving  
FROM StockbewegingSerienummer sbsr 
inner join Serienummer sr on sbsr.serienummerId = sr.id
left join Status st on sr.statusId = st.id WHERE (stockbewegingid = @stockbewegingid)">
        <SelectParameters>
            <asp:SessionParameter Name="stockbewegingid" SessionField="stockbeweging" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
     <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
     </dx:ASPxGridViewExporter>
    <br />
</asp:Content>