﻿Imports System.Data.SqlClient
Imports DevExpress.Web
Imports DevExpress.Web.Rendering
Imports System.Reflection

Public Class MinMax_Magazijnen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Session("level") > 5 Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Protected Sub ASPxCallback1_Callback(ByVal sender As Object, ByVal e As CallbackEventArgs)

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Try
                Dim cmd As New SqlCommand With {.Connection = cn, .CommandText = "prStockartikelsnaarMinMaxperMagazijn", .CommandType = CommandType.StoredProcedure}
                Dim par As New SqlParameter("@Magazijn", SqlDbType.Int) With {.Value = cboMagazijn.Value}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Actieve artikels toegevoegd aan min-max per Magazijn {0} door {1} {2}", cboMagazijn.Text, Session("userid"), Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()

            Catch ex As Exception
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij toevoegen actieve artikels aan min-max per Magazijn {0} door {1} {2} : {3}", cboMagazijn.Text, Session("naam"), Session("voornaam"), ex.Message)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End Try

        End Using

        grid.DataBind()
    End Sub


    Private Sub grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles grid.BatchUpdate
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            For Each args In e.UpdateValues
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Min-max artikels per Magazijn {0} artikel {1} gewijzigd door {2} {3}", cboMagazijn.Text, grid.GetRowValuesByKeyValue(args.Keys("id"), "Artikel"), Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            Next
        End Using
    End Sub

    Private Sub grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs) Handles grid.CustomCallback
        If String.IsNullOrEmpty(e.Parameters) Then Return

        Dim varPar As String() = e.Parameters.Split(New Char() {";"})
        Dim selectItems As List(Of Object) = grid.GetSelectedFieldValues("id")
        If (varPar(0) = "MinMaxAanpassen") Then
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                For Each selectItemId As Object In selectItems
                    Try
                        Dim cmd As New SqlCommand(String.Format("UPDATE stock_Artikels_minmax_Magazijn SET Minimum={0}, Maximum={1} WHERE (id={2})", varPar(1), varPar(2), selectItemId), cn)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Min-max artikels per Magazijn {0} artikel {1} gewijzigd door {2} {3}", cboMagazijn.Text, grid.GetRowValuesByKeyValue(selectItemId, "Artikel"), Session("naam"), Session("voornaam"))}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij wijzigen Min-max artikels per Magazijn {0} door {1} {2}", cboMagazijn.Text, Session("naam"), Session("voornaam"))}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    End Try

                Next
            End Using
            grid.DataBind()
            grid.Selection.UnselectAll()
        ElseIf (varPar(0) = "VerwijderSelectie") Then
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                For Each selectItemId As Object In selectItems
                    Try
                        Dim cmd As New SqlCommand(String.Format("DELETE FROM stock_Artikels_minmax_Magazijn WHERE ([id] = {0})", selectItemId), cn)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Min-max artikels per magazijn {0} : rij verwijderd door door {1} {2}", cboMagazijn.Text, Session("naam"), Session("voornaam"))}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij verwijderen rij Min-max artikels per magazijn {0} door {1} {2}", cboMagazijn.Text, Session("naam"), Session("voornaam"))}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    End Try
                Next
            End Using
            grid.DataBind()
            grid.Selection.UnselectAll()
        End If

    End Sub

    Public Function GetRender(data As Object) As String

        Dim str As Int32 = Convert.ToInt32(data)

        If str = 1 Then

            Return "<a href=""javascript:void(0);"" onclick=""OnMoreInfoClick(this)"">Foto ...</a>"
        Else
            Return String.Empty
        End If

    End Function

    Private Sub grid_RowInserting(sender As Object, e As Data.ASPxDataInsertingEventArgs) Handles grid.RowInserting
        e.NewValues("Magazijn") = cboMagazijn.Value
    End Sub
    Protected Sub callbackPanel_Callback(ByVal source As Object, ByVal e As DevExpress.Web.CallbackEventArgsBase)
        edBinaryImage.Value = FindImage(grid.GetRowValues(grid.FocusedRowIndex, "Artikel"))

        litText.Text = String.Format("{0} - {1}", grid.GetRowValuesByKeyValue(grid.GetRowValues(grid.FocusedRowIndex, "id"), "ERP"), grid.GetRowValuesByKeyValue(grid.GetRowValues(grid.FocusedRowIndex, "id"), "Omschrijving"))
    End Sub

    Private Function FindImage(ByVal id As String) As Byte()
        Dim btImage As Byte() = Nothing
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("SELECT Foto FROM stock_Artikels WHERE (id={0})", id), cn)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not dr.IsDBNull(0) Then
                    btImage = CType(dr.GetValue(0), Byte())
                End If
            End If
            dr.Close()
        End Using
        Return btImage
    End Function
End Class