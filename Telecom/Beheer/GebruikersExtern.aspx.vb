﻿Imports System.Data.SqlClient
Imports System.Windows.Forms.VisualStyles.VisualStyleElement.Tab

Public Class GebruikersExtern
    Inherits System.Web.UI.Page

    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)
        If taalId = 1 Then


            ASPxTextBoxGSM.ValidationSettings.RegularExpression.ErrorText = "Foute GSM Nummer. Formaat +00000000000 nodig"
        Else
            ASPxLabel1.Text = "Nom *"
            ASPxLabel4.Text = "Langue *"
            ASPxLabel5.Text = "Groupe"
            ASPxTextBoxGSM.ValidationSettings.RegularExpression.ErrorText = "Format +00000000000"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))

        If Page.IsPostBack Then
            If Session("FoutGebruiker") <> "" Then
                ASPxLabel6.Text = Session("FoutGebruiker")
                Session("FoutGebruiker") = ""
            End If
        Else
            ASPxLabel6.Text = ""
        End If




        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        If Session("taal") = 1 Then
            LiteralTitel.Text = "Nieuwe externe gebruiker toevoegen"
        Else
            LiteralTitel.Text = "Ajouter utilisateur external"
        End If
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        ASPxTextBoxNaam.Validate()
        ASPxTextBoxGSM.Validate()
        ASPxTextBoxEmail.Validate()
        Dim voocontext As New VooEntities
        Dim email As String = ASPxTextBoxEmail.Value
        'If voocontext.Gebruikers.Where(Function(x) x.Email = email).Any Then
        '    If Session("taal") = 1 Then
        '        Session("FoutGebruiker") = "Emailadres bestaat al"
        '    Else
        '        Session("FoutGebruiker") = "L'adresse email existe déjà"
        '    End If
        '    ASPxLabel6.Text = Session("FoutGebruiker")
        '    Return
        'End If

        If ASPxTextBoxNaam.Value = "" Then

            If Session("taal") = 1 Then
                Session("FoutGebruiker") = "Geen naam ingevuld"
            Else
                Session("FoutGebruiker") = "Aucun nom entré"
            End If
            ASPxLabel6.Text = Session("FoutGebruiker")
            Return
        End If

        If ASPxTextBoxEmail.Value = "" Then
            If Session("taal") = 1 Then
                Session("FoutGebruiker") = "Geen email ingevuld"
            Else
                Session("FoutGebruiker") = "Aucun email entré"
            End If
            ASPxLabel6.Text = Session("FoutGebruiker")
            Return
        End If

        If ASPxComboBoxTaal.Value = "" Then
            If Session("taal") = 1 Then
                Session("FoutGebruiker") = "Geen taal ingevuld"
            Else
                Session("FoutGebruiker") = "Aucun langue entré"
            End If
            ASPxLabel6.Text = Session("FoutGebruiker")
            Return
        End If

        If ASPxComboBoxNiveau.Value = "" Then
            If Session("taal") = 1 Then
                Session("FoutGebruiker") = "Geen niveau ingevuld"
            Else
                Session("FoutGebruiker") = "Aucun niveau entré"
            End If
            ASPxLabel6.Text = Session("FoutGebruiker")
            Return
        End If






        Dim m As New QueryStringModule
            Dim KeyGen As New RandomKeyGenerator With {.KeyLetters = "abcdefghijklmnopqrstuvwxyz", .KeyNumbers = "0123456789", .KeyChars = 12}
            Dim RandomKey As String = KeyGen.Generate()
            Dim wachtwoord As String = m.Encrypt(RandomKey, venc)


        Dim gebruiker As New Gebruikers
            gebruiker.AangemaaktDoor = Session("userid")
            gebruiker.AangemaaktOp = Today
            gebruiker.Actief = 1
            gebruiker.OpdrachtgeverId = Session("opdrachtgever")
            gebruiker.TaalId = Convert.ToInt32(ASPxComboBoxTaal.Value)
            gebruiker.GSM = ASPxTextBoxGSM.Value
            gebruiker.Email = ASPxTextBoxEmail.Value
        gebruiker.Paswoord = wachtwoord

        gebruiker.ExNaam = ASPxTextBoxNaam.Value
        gebruiker.WachtwoordVerstuurd = True
        gebruiker.WachtwoordVerstuurdOp = Today
        gebruiker.Niveau = 2
        gebruiker.Niveau = Convert.ToInt32(ASPxComboBoxNiveau.Value)
        Dim laatsteExNummer As Integer = 1

        Dim rept_max = (From c In voocontext.Gebruikers Where c.Werkg = "EX" Select c).Max(Function(c) c.Werkn)
        If rept_max Is Nothing Then
            rept_max = 0
        End If
        laatsteExNummer = rept_max + 1
            gebruiker.Werkg = "EX"
            gebruiker.Werkn = laatsteExNummer

            gebruiker.Login = "EX-" & laatsteExNummer
            voocontext.Gebruikers.Add(gebruiker)
            voocontext.SaveChanges()


        If Not String.IsNullOrEmpty(cmbGroepen.Value) Then
            Dim lid As New GebruikersgroepenLeden

            lid.Gebruiker = gebruiker.id
            lid.Groep = cmbGroepen.Value

            voocontext.GebruikersgroepenLeden.Add(lid)
        End If



        If gebruiker.Niveau <> 3 Then
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim mag_SQL As String = "INSERT INTO Magazijn (Naam, Hoofd, OpdrachtgeverId) VALUES (@Naam, 0, @opdrachtgeverId);SELECT SCOPE_IDENTITY();"
                Dim cmd = New SqlCommand(mag_SQL, cn)
                Dim par = New SqlParameter("@naam", SqlDbType.NVarChar, 20) With {.Value = gebruiker.ExNaam}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int, -1) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim magId As Int32 = cmd.ExecuteScalar()

                Dim usermag_SQL As String = "UPDATE Gebruikers set magazijnId = @magazijnId where id = @gebruikerId"
                cmd = New SqlCommand(usermag_SQL, cn)
                par = New SqlParameter("@gebruikerId", SqlDbType.Int, -1) With {.Value = gebruiker.id}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@magazijnId", SqlDbType.Int, -1) With {.Value = magId}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()

                Dim smag_SQL As String = "INSERT INTO Stockmagazijn (MateriaalId, MagazijnId, Aantal) SELECT materiaalid,@MagazijnId, 0 from MateriaalOpdrachtgevers where opdrachtgeverId = @opdrachtgeverId"
                cmd = New SqlCommand(smag_SQL, cn)
                par = New SqlParameter("@MagazijnId", SqlDbType.Int, -1) With {.Value = magId}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int, -1) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()

                Dim s_SQL As String = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                cmd = New SqlCommand(s_SQL, cn)
                par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Gebruiker toegevoegd door {0} {1} {2} : {3}", Session("userid"), Session("naam"), Session("voornaam"), gebruiker.ExNaam)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End Using
        End If
        Dim toadd As New List(Of String)
            toadd.Add(gebruiker.Email)
        Dim sBody As String = "<p>Beste, [GEBRUIKERNAAM]</p><p>Uw account voor APK WMS is aangemaakt. U kan inloggen op https://wms.apkgroup.eu/ met gebruikersnaam [LOGIN] (of uw emailadres) en wachtwoord [WACHTWOORD]</p><p>&nbsp;</p><p>Met vriendelijke groeten,</p><p>APK Group</p>"
        sBody = sBody.Replace("[GEBRUIKERNAAM]", gebruiker.ExNaam)
            sBody = sBody.Replace("[LOGIN]", gebruiker.Login)
            sBody = sBody.Replace("[WACHTWOORD]", RandomKey)
            mailGoogle("Account WMS APK", "noreply@apk.be", toadd, sBody, New Dictionary(Of String, Byte()))
            voocontext.SaveChanges()
            voocontext.Dispose()

            Response.Redirect("~/BeheerGebruikers.aspx", False)



    End Sub
End Class