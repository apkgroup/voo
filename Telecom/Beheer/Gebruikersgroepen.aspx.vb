﻿Imports System.Data.SqlClient

Public Class Gebruikersgroepen
    Inherits System.Web.UI.Page
    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then
            Literal2.Text = "Groepen"
            ASPxButtonNieuwegroep.Text = "Aanmaken"
            ASPxLabelGroepSelect.Text = "Selecteer een groep"
            ASPxLabellCreergroep.Text = "of maak een nieuwe groep"
            ASPxLabelGroepleden.Text = "Leden groep:"
            ASPxLabelGebruikers.Text = " Gebruikerslijst:"
            lijstLeden.SettingsText.EmptyDataRow = "Geen groep geselecteerd."
            lijstGebruikers.SettingsText.EmptyDataRow = ""
        Else
            Literal2.Text = "Groupes"
            ASPxLabelGroepSelect.Text = "Groupe sélect"
            ASPxLabellCreergroep.Text = "ou créer un noveau groupe:"
            ASPxLabelGroepleden.Text = "Les membres de ce groupe"
            ASPxLabelGebruikers.Text = "Les utilisateurs"
            lijstLeden.SettingsText.EmptyDataRow = "Aucune donnée chargée. Select groupe"
            lijstGebruikers.SettingsText.EmptyDataRow = "Gebruikerslijst:"
        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using

    End Sub

    Protected Sub ASPxButtonNieuwegroep_Click(sender As Object, e As EventArgs) Handles ASPxButtonNieuwegroep.Click
        If Not (String.IsNullOrEmpty(Me.ASPxTextBoxNieuweGroep.Text)) Then
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = "SELECT id FROM Gebruikersgroepen WHERE (Naam=@naam and opdrachtgeverId = @opdrachtgeverId)"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@naam", SqlDbType.NVarChar, 25) With {.Value = ASPxTextBoxNieuweGroep.Text}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                Dim blbestaat As Boolean = dr.HasRows
                dr.Close()
                If blbestaat Then
                    ASPxTextBoxNieuweGroep.ErrorText = "Deze groep bestaat al."
                    ASPxTextBoxNieuweGroep.IsValid = False
                    Return
                End If
                Try
                    s_SQL = "INSERT INTO Gebruikersgroepen (Naam, opdrachtgeverId) VALUES (@naam, @opdrachtgever)"
                    cmd = New SqlCommand(s_SQL, cn)
                    par = New SqlParameter("@naam", SqlDbType.NVarChar, 25) With {.Value = ASPxTextBoxNieuweGroep.Text}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@opdrachtgever", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()

                    cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Gebruikersgroep {0} toegevoegd door {1} {2}", ASPxTextBoxNieuweGroep.Text, Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                    ASPxTextBoxNieuweGroep.Text = ""
                Catch ex As Exception
                    cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij toevoeging gebruikersgroep door {0} {1}, omschrijving: {2}", Session("naam"), Session("voornaam"), ex.Message)}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                End Try

            End Using
        End If
        cmbGroepen.DataBind()
    End Sub


    Private Sub VulLedenlijst(ByVal iGroep As Integer)
        
        SqlDataSourceLeden.SelectParameters(0).DefaultValue = iGroep
        lijstLeden.DataBind()

    End Sub

    Private Sub lijstLeden_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs) Handles lijstLeden.CustomCallback
        VulLedenlijst(Convert.ToInt32(e.Parameters))
    End Sub

    Private Sub lijstGebruikers_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs) Handles lijstGebruikers.CustomCallback
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Try
                Dim s_SQL As String = "SELECT id FROM GebruikersgroepenLeden WHERE (Groep=@groep) AND (Gebruiker=@gebruiker)"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@groep", SqlDbType.Int) With {.Value = cmbGroepen.Value}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Convert.ToInt32(e.Parameters)}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                Dim blbestaat As Boolean = dr.HasRows
                dr.Close()
                If Not blbestaat Then
                    s_SQL = "DELETE FROM GebruikersgroepenLeden WHERE Gebruiker = @gebruiker; INSERT INTO GebruikersgroepenLeden (Groep, Gebruiker) VALUES (@groep,@gebruiker)"
                    cmd = New SqlCommand(s_SQL, cn)
                    par = New SqlParameter("@groep", SqlDbType.Int) With {.Value = cmbGroepen.Value}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Convert.ToInt32(e.Parameters)}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Gebruiker {0} toegevoegd aan groep {1} door {2} {3}", lijstGebruikers.GetRowValuesByKeyValue(Convert.ToInt32(e.Parameters), "Werknemer"), cmbGroepen.Text, Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()

                End If
            Catch ex As Exception
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij toevoeging gebruiker aan gebruikersgroep door {0} {1} {2}", Session("naam"), Session("voornaam"), ex.Message)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End Try
           
        End Using

    End Sub

    Private Sub lijstLeden_RowDeleted(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletedEventArgs) Handles lijstLeden.RowDeleted
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Try
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Gebruiker {0} verwijderd uit gebruikersgroep {1} door {2} {3}", e.Values("Werknemer"), cmbGroepen.Text, Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            Catch ex As Exception

            End Try
        End Using
   
        VulLedenlijst(cmbGroepen.Value)
    End Sub


End Class