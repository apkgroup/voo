﻿Imports System.IO
Imports DevExpress.Export
Imports DevExpress.Web

Public Class OverzichtSerienummers
    Inherits System.Web.UI.Page
    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then
            Literal2.Text = "Overzicht serienummers"
            ASPxButton3.Text = "Exporteren"
            ASPxGridView1.Columns("Naam").Caption = "Magazijn"
            ASPxGridView1.Columns("Article").Caption = "Artikelnr."
            ASPxGridView1.Columns("Description").Caption = "Omschrijving"
            ASPxGridView1.Columns("serienummer").Caption = "Serienummer"
            ASPxGridView1.Columns("uitgeboekt").Caption = "Uitgeboekt"
            ASPxGridView1.Columns("datumOntvangst").Caption = "Datum Ontvangen Technieker"
            ASPxGridView1.Columns("beschrijving").Caption = "Status"
            ASPxGridView1.Columns("lotNr").Caption = "Lotnummer"
            ASPxGridView1.Columns("datumGemaakt").Caption = "Datum ingelezen Magazijn"
        Else
            Literal2.Text = "Vue d'ensemble des numéros de série"
            ASPxGridView1.Columns("Naam").Caption = "Nom"
            ASPxGridView1.Columns("Article").Caption = "Article"
            ASPxGridView1.Columns("Description").Caption = "Description"
            ASPxGridView1.Columns("serienummer").Caption = "Numéro de série"
            ASPxGridView1.Columns("uitgeboekt").Caption = "Réserve"
            ASPxGridView1.Columns("datumOntvangst").Caption = "Date de réception Tech"
            ASPxGridView1.Columns("beschrijving").Caption = "Statut"
            ASPxGridView1.Columns("lotNr").Caption = "Numéro de lot"
            ASPxGridView1.Columns("datumGemaakt").Caption = "Date de création Magasin"

        End If

    End Sub
    Protected Sub page_init() Handles Me.Init

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") And Not Session("level") = 10 Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        If Not Request.QueryString("transfer") Is Nothing Then
            ASPxLabel1.Text = "Serienummer " & Request.QueryString("transfer") & " is overgezet naar een nieuwe opdrachtgever."
        Else
            ASPxLabel1.Text = ""
        End If

        If Not Request.QueryString("transferfout") Is Nothing Then
            ASPxLabel1.Text = "Serienummer " & Request.QueryString("transfer") & " kan niet overgezet worden. Enkel serienummers met status 'Received Warehouse' of 'Received Technician' mogen verplaatst worden."
        Else
            ASPxLabel1.Text = ""
        End If


        If Session("level") = 10 Then
            ASPxGridView1.Columns("Transfer").Visible = False
        End If


    End Sub

    Protected Sub ASPxGridView1_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs) Handles ASPxGridView1.CustomCallback
        Dim ms As New MemoryStream
        ASPxGridViewExporter1.WritePdf(ms)
        Session("gridData") = ms.ToArray()
        Session("export") = "true"

        ASPxGridView1.JSProperties("cpExport") = True
    End Sub

    Protected Sub gridView_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles ASPxGridView1.CustomColumnDisplayText
        If (e.Column.FieldName = "uitgeboekt") And e.Column.VisibleIndex = 15 Then
            If e.Value = True Then
                e.DisplayText = "<a href='../reactivate.aspx?id=" + ASPxGridView1.GetRowValues(e.VisibleIndex, "serienummer") + "'>Heractiveren</a>"
            Else
                e.DisplayText = ""
            End If

        End If


    End Sub


    Protected Sub ASPxMenu1_ItemClick(source As Object, e As DevExpress.Web.MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "ExportExcel" Then
            ASPxGridViewExporter1.FileName = "Export Serie " & Today.Date()
            ASPxGridViewExporter1.WriteXlsxToResponse(New DevExpress.XtraPrinting.XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})

        End If
    End Sub

    Protected Sub ASPxGridView1_HtmlRowHtmlRowCreated(sender As Object, e As DevExpress.Web.ASPxGridViewTableRowEventArgs) Handles ASPxGridView1.HtmlRowCreated
        'If e.RowType = GridViewRowType.Data Then
        '    Dim grid As ASPxGridView = TryCast(sender, ASPxGridView)
        '    Dim value As String = e.GetValue("url_column").ToString()
        '    Dim lblmycolumn As ASPxLabel = TryCast(grid.FindRowCellTemplateControl(e.VisibleIndex, TryCast(grid.Columns("MyColumn"), GridViewDataTextColumn), "lblmycolumn "), ASPxLabel)
        '    Dim txtHyperlink As ASPxHyperLink = TryCast(grid.FindRowCellTemplateControl(e.VisibleIndex, TryCast(grid.Columns("MyColumn"), GridViewDataTextColumn), "hlColumn"), ASPxHyperLink)

        '    If Not String.IsNullOrWhiteSpace(value) Then
        '        txtHyperlink.Visible = True
        '        lblmycolumn.Visible = False
        '    Else
        '        txtHyperlink.Visible = False
        '        lblQuestion.Visible = True
        '    End If
        'End If
    End Sub

    Protected Sub ASPxButton4_Click(sender As Object, e As EventArgs) Handles ASPxButton4.Click
        Dim context As New VooEntities

        Dim artikelId As Integer = ASPxComboBoxArtikel.Value
        Dim opdrachtgever As Integer = Session("opdrachtgever")
        'Dim naarmagazijn = context.Magazijn.Where(Function(x) x.Hoofd = True And x.OpdrachtgeverId = opdrachtgever).FirstOrDefault
        'Dim naarStockMagazijn = context.StockMagazijn.Where(Function(x) x.Basismateriaal = artikelId)
        For Each item In ASPxGridView1.GetSelectedFieldValues("id")
            Dim sr = context.Serienummer.Find(item)
            If sr.uitgeboekt = False Then
                Dim sourcemateriaalId = sr.StockMagazijn.MateriaalId
                Dim sourceStockMagazijn = sr.StockMagazijn
                Dim sourceMagazijn As Integer = sr.StockMagazijn.MagazijnId

                Dim targetMagazijn = context.StockMagazijn.Where(Function(x) x.MagazijnId = sourceMagazijn And x.MateriaalId = artikelId).FirstOrDefault
                sr.StockMagazijnId = targetMagazijn.id
                sourceStockMagazijn.Aantal = sourceStockMagazijn.Aantal - 1
                targetMagazijn.Aantal = targetMagazijn.Aantal + 1

                Dim sb As New Stockbeweging
                sb.beweging = 1
                sb.datum = DateTime.Now
                sb.gebruiker = Session("userid")
                sb.stockmagazijnId = targetMagazijn.id
                sb.opmerking = "transfer serienummer naar andere soort"

                Dim sb2 As New Stockbeweging
                sb2.beweging = -1
                sb2.datum = DateTime.Now
                sb2.gebruiker = Session("userid")
                sb2.stockmagazijnId = sourceStockMagazijn.id
                sb2.opmerking = "transfer serienummer naar andere soort"

                context.Stockbeweging.Add(sb)
                context.Stockbeweging.Add(sb2)

                context.SaveChanges()
            End If




        Next
    End Sub

    Protected Sub ASPxCheckBoxAllesZien_CheckedChanged(sender As Object, e As EventArgs) Handles ASPxCheckBoxAllesZien.CheckedChanged
        If ASPxCheckBoxAllesZien.Checked Then
            ASPxGridView1.DataSourceID = "SqlDataSource3"
        Else
            ASPxGridView1.DataSourceID = "SqlDataSource1"
        End If
        ASPxGridView1.DataBind()
    End Sub
End Class