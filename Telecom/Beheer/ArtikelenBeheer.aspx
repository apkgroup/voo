﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="ArtikelenBeheer.aspx.vb" Inherits="Telecom.ArtikelenBeheer" ValidateRequest="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function OnInit(s, e) {  
    setTimeout(ChangeHint, 50);  
    s.ActiveItemChanged.AddHandler(function (s, args) { setTimeout(ChangeHint, 50); })  
}  

function ChangeHint() {  
    var src = imageZoom.GetMainElement().getElementsByClassName("dxiz-clipPanel")[0].getElementsByTagName("img")[0].src;  
    var hintElem = imageZoom.GetMainElement().getElementsByClassName("dxiz-hint")[0];  
    if (src.indexOf("empty") >= 0) {  
        if (!hintElem.classList.contains("MyClass"))  
            hintElem.classList.add("MyClass");  
    }  
    else {  
        if (hintElem.classList.contains("MyClass"))  
            hintElem.classList.remove("MyClass");  
    }  
}  
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>

                <dx:ASPxButton ID="ASPxButtonToevoegen" runat="server" AutoPostBack="false" Width="50px" Text="Nieuw artikel toevoegen" CssClass="minimargin">
                     <ClientSideEvents Click="function(s, e) { pnlForm.SetVisible(true); }" />  
                </dx:ASPxButton>

    <dx:ASPxPanel ID="pnlForm" runat="server"  ClientVisible="false" ClientInstanceName="pnlForm" CssClass="minimargin" Paddings-PaddingLeft="5px" Paddings-PaddingBottom="5px">  
        <Paddings PaddingBottom="5px" />
    <PanelCollection>  
        <dx:PanelContent>  
            &nbsp;<table style="width:100%;">
                <tr>
                    <td style="text-align: right">Artikelnummer (extern):</td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextBoxArtikel" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                        <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Controleer Artikelnummer">
                        </dx:ASPxButton>
                    </td>
                    <td style="text-align: right">Beschrijving:</td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextBoxBeschrijving" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">Artikelnummer (intern):</td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextBoxIntern" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                    <td style="text-align: right">Actief:</td>
                    <td>
                        <dx:ASPxCheckBox ID="ASPxCheckBoxActief" runat="server" CheckState="Unchecked">
                        </dx:ASPxCheckBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">Min (tech):</td>
                    <td>
                        <dx:ASPxSpinEdit ID="ASPxSpinEditMin" runat="server" Number="0">
                        </dx:ASPxSpinEdit>
                    </td>
                    <td style="text-align: right">Max (tech):</td>
                    <td>
                        <dx:ASPxSpinEdit ID="ASPxSpinEditMax" runat="server" Number="0">
                        </dx:ASPxSpinEdit>
                    </td>
                </tr>
                                <tr>
                    <td style="text-align: right">Min (hoofd):</td>
                    <td>
                        <dx:ASPxSpinEdit ID="ASPxSpinEditMinHoofd" runat="server" Number="0">
                        </dx:ASPxSpinEdit>
                    </td>
                    <td style="text-align: right">Max (hoofd):</td>
                    <td>
                        <dx:ASPxSpinEdit ID="ASPxSpinEditMaxHoofd" runat="server" Number="0">
                        </dx:ASPxSpinEdit>
                    </td>
                </tr>
                   <tr>
                    <td style="text-align: right">Serienr:</td>
                    <td>
                        <dx:ASPxCheckBox ID="ASPxCheckBoxSerienr" runat="server" CheckState="Unchecked">
                        </dx:ASPxCheckBox>
                       </td>
                    <td style="text-align: right">Serienr verplicht bij opboeken:</td>
                    <td>
                        <dx:ASPxCheckBox ID="ASPxCheckBoxSerienrVerplicht" runat="server" CheckState="Unchecked">
                        </dx:ASPxCheckBox>
                       </td>
                </tr>
                   <tr>
                         <td style="text-align: right">RegEx Serienummer (optioneel):</td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextBoxRegEx" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                       </td>
                       <td style="text-align: right">Materiaalgroep(en):</td>
                    <td>
                        <dx:aspxComboBox ID="ASPxComboBoxMateriaalgroep" runat="server" Width="170px" DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                        </dx:aspxComboBox>
                        <dx:ASPxComboBox ID="ASPxComboBoxMateriaalgroep2" runat="server" DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id" Width="170px">
                        </dx:ASPxComboBox>
                        <dx:ASPxComboBox ID="ASPxComboBoxMateriaalgroep3" runat="server" DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id" Width="170px">
                        </dx:ASPxComboBox>
                       </td>
                       </tr>
                <tr><td style="text-align: right">Materiaaltype</td>
                    <td><dx:ASPxComboBox ID="ASPxComboBoxMateriaalType" runat="server" DataSourceID="SqlDataSourceTypes" TextField="omschrijving" ValueField="id" Width="170px">
                        </dx:ASPxComboBox></td>
                    <td style="text-align: right">Consignatie</td><td> <dx:ASPxCheckBox ID="ASPxCheckBoxConsignatie" runat="server" CheckState="Unchecked">
                        </dx:ASPxCheckBox></td></tr>
                <tr>
                    <td style="text-align: right">Haspel-artikel:</td>
                    <td>
                        <dx:ASPxCheckBox ID="ASPxCheckBoxHaspel" runat="server" CheckState="Unchecked">
                        </dx:ASPxCheckBox>
                       </td>
                        
                    <td style="text-align: right">Foto:</td>
                     <td>
                         <dx:ASPxUploadControl ID="ASPxUploadControl1"  OnFileUploadComplete="ASPxUploadControl1_FileUploadComplete" runat="server"  Width="280px" FileUploadMode="OnPageLoad">
                             <ValidationSettings AllowedFileExtensions=".png, .jpg, .gif">
                             </ValidationSettings>
                         </dx:ASPxUploadControl>
                       </td>
                       </tr>
                <tr><td style="text-align: right">Eenheid:</td><td>
                    <dx:ASPxTextBox ID="ASPxTextBoxArtikelEenheid" runat="server" Width="170px">
                    </dx:ASPxTextBox>
                    </td><td style="text-align: right">Prijs</td><td>
                    <dx:ASPxSpinEdit ID="ASPxSpinEditPrijs" runat="server" Number="0">
                    </dx:ASPxSpinEdit>
                    </td></tr>
                <tr><td style="text-align: right">Locatie:</td><td>
                    <dx:ASPxTextBox ID="ASPxTextBoxLocatie" runat="server" Width="170px">
                    </dx:ASPxTextBox>
                    </td><td style="text-align: right"></td><td>
               
                    </td></tr>
                  
          
            </table>
            <h2><asp:Literal ID="LiteralOpdrachtgevers" runat="server"></asp:Literal></h2>
            <p>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceAdminOpdrachtgevers" KeyFieldName="opdrachtgeverId">
                    <Columns>
                        <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowInCustomizationForm="True" ShowSelectCheckbox="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="opdrachtgeverId" ShowInCustomizationForm="True" Visible="False" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Opdrachtgever" FieldName="naam" ShowInCustomizationForm="True" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="gebruikerId" ShowInCustomizationForm="True" Visible="False" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSourceAdminOpdrachtgevers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [opdrachtgeverId], naam, [gebruikerId] FROM [AdminOpdrachtgevers] a Inner Join opdrachtgever g on a.opdrachtgeverId = g.id
where gebruikerId = @gebruikerId">
                    <SelectParameters>
                        <asp:SessionParameter Name="gebruikerId" SessionField="userid" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </p>
            <dx:ASPxButton ID="ASPxButtonToevoegen0" runat="server" Width="50px">
                <Image IconID="actions_add_32x32office2013">
                </Image>
            </dx:ASPxButton>
        </dx:PanelContent>  
    </PanelCollection>  
        <Border BorderColor="Silver" BorderWidth="1px" />
</dx:ASPxPanel>  
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceArtikelen" KeyFieldName="Id" EnableRowsCache="False">
        <SettingsPager PageSize="25">
        </SettingsPager>
        <Settings ShowHeaderFilterButton="True" />
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="Id" ReadOnly="True" VisibleIndex="1" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="3" Caption="Extern">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="5" Caption="Omschrijving">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="HeeftSerienummer" VisibleIndex="6" Caption="Serienr.">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="actief" VisibleIndex="12" Caption="Actief">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="SerieVerplichtBijOpboeken" VisibleIndex="14" Visible="False">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="onbestelbaar" VisibleIndex="15" Visible="False">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="retourItem" VisibleIndex="16" Visible="False">
            </dx:GridViewDataCheckColumn>
             <dx:GridViewDataTextColumn Caption="RegEx Sr." FieldName="serienummerRegex" VisibleIndex="18" Visible="False">
                 <EditFormSettings Visible="True" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataHyperLinkColumn Caption=" " FieldName="Id" VisibleIndex="26">
                <PropertiesHyperLinkEdit NavigateUrlFormatString="../Beheer/MateriaalOpdrachtgevers.aspx?id={0}" Text="Opdrachtgevers">
                </PropertiesHyperLinkEdit>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataHyperLinkColumn>
           
            <dx:GridViewDataTextColumn Caption="min (h)" FieldName="minhoofd" VisibleIndex="9">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="max (h)" FieldName="maxhoofd" VisibleIndex="10">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataCheckColumn FieldName="bobijnArtikel" VisibleIndex="17" Caption="Bobijn">
            </dx:GridViewDataCheckColumn>
           
            <dx:GridViewDataBinaryImageColumn Caption="Foto" FieldName="foto" VisibleIndex="19">
                <DataItemTemplate>  
                <dx:ASPxImageZoom runat="server" ID="zoom" LargeImageLoadMode="OnPageLoad" ShowHintText="false" ShowHint="false" 
                    ImageContentBytes='<%#If(Eval("foto") Is DBNull.Value, IO.File.ReadAllBytes(Server.MapPath("../images/") & "blanco.png"), Eval("foto")) %>' LargeImageContentBytes='<%#If(Eval("foto") Is DBNull.Value, IO.File.ReadAllBytes(Server.MapPath("../images/") & "blanco.png"), Eval("foto")) %>' EnableZoomMode="False">  
                    <SettingsAutoGeneratedImages ImageCacheFolder="~/images/" ImageHeight="30" LargeImageWidth="800" LargeImageHeight="800" />  
                    <%--<SettingsZoomMode ZoomWindowWidth="350" ZoomWindowHeight="350" ZoomWindowPosition="Bottom" />--%>  
                    <Border BorderStyle="None" />  
                </dx:ASPxImageZoom>  
            </DataItemTemplate>  
                <PropertiesBinaryImage ImageHeight="30px">
                  <EditingSettings Enabled="true">
                            <UploadSettings>
                                <UploadValidationSettings MaxFileSize="4194304">
                                </UploadValidationSettings>
                            </UploadSettings>
                        </EditingSettings>
                </PropertiesBinaryImage>
            </dx:GridViewDataBinaryImageColumn>
           
            <dx:GridViewDataComboBoxColumn FieldName="MateriaalGroep" VisibleIndex="20" Caption="Groep 1">
                <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
           
            <dx:GridViewDataTextColumn FieldName="Eenheid" VisibleIndex="24" Caption="EH">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataComboBoxColumn Caption="Groep 2" FieldName="MateriaalGroep2" VisibleIndex="21">
                <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
           
            <dx:GridViewDataSpinEditColumn Caption="Volg." FieldName="volgorde" VisibleIndex="2">
                <PropertiesSpinEdit DisplayFormatString="g">
                </PropertiesSpinEdit>
            </dx:GridViewDataSpinEditColumn>
            <dx:GridViewDataSpinEditColumn FieldName="Prijs" VisibleIndex="25">
                <PropertiesSpinEdit DisplayFormatString="g">
                </PropertiesSpinEdit>
            </dx:GridViewDataSpinEditColumn>
            <dx:GridViewDataSpinEditColumn Caption="min (t)" FieldName="min" VisibleIndex="7">
                <PropertiesSpinEdit DisplayFormatString="g">
                </PropertiesSpinEdit>
            </dx:GridViewDataSpinEditColumn>
            <dx:GridViewDataSpinEditColumn Caption="max (t)" FieldName="max" VisibleIndex="8">
                <PropertiesSpinEdit DisplayFormatString="g">
                </PropertiesSpinEdit>
            </dx:GridViewDataSpinEditColumn>
           
            <dx:GridViewDataComboBoxColumn Caption="Groep 3" FieldName="MateriaalGroep3" VisibleIndex="22">
                <PropertiesComboBox DataSourceID="SqlDataSourceGroepen" TextField="omschrijving" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataCheckColumn FieldName="Consignatie" VisibleIndex="13" Caption="Consign.">
                <EditFormSettings Visible="True" />
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn Caption="Intern" FieldName="ArticleInternal" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn FieldName="MateriaalType" VisibleIndex="23" Caption="Type">
                <PropertiesComboBox DataSourceID="SqlDataSourceTypes" TextField="omschrijving" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
           
            <dx:GridViewDataTextColumn FieldName="locatie" VisibleIndex="11">
            </dx:GridViewDataTextColumn>
           
        </Columns>
    </dx:ASPxGridView>
    <br />
   
    
    <dx:ASPxPopupControl ID="popupcontrolbox" runat="server" ClientInstanceName="popupcontrolbox" HeaderText="Artikelnummer:" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="442px">
        
        <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <dx:ASPxLabel ID="ASPxLabel1" runat="server" EncodeHtml="False" Text="ASPxLabel">
    </dx:ASPxLabel>
    <dx:ASPxButton ID="ASPxButton5" runat="server" Text="Ok">
    </dx:ASPxButton>
    <br />
            </dx:PopupControlContentControl>
</ContentCollection>
        
    </dx:ASPxPopupControl>
    <asp:SqlDataSource ID="SqlDataSourceArtikelen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT b.[Id],[Article], [Description],[HeeftSerienummer], Eenheid, Prijs,
(SELECT [min] from [MateriaalOpdrachtgevers] m where b.id = m.[materiaalId] and m.opdrachtgeverId =@opdrachtgeverSession ) as 'min',
(SELECT [max] from [MateriaalOpdrachtgevers] m where b.id = m.[materiaalId] and m.opdrachtgeverId =@opdrachtgeverSession ) as 'max',
(SELECT [minhoofd] from [MateriaalOpdrachtgevers] m where b.id = m.[materiaalId] and m.opdrachtgeverId =@opdrachtgeverSession ) as 'minhoofd',
(SELECT [maxhoofd] from [MateriaalOpdrachtgevers] m where b.id = m.[materiaalId] and m.opdrachtgeverId =@opdrachtgeverSession ) as 'maxhoofd',
(SELECT [locatie] from [MateriaalOpdrachtgevers] m where b.id = m.[materiaalId] and m.opdrachtgeverId =@opdrachtgeverSession ) as 'locatie',
(SELECT [volgorde] from [MateriaalOpdrachtgevers] m where b.id = m.[materiaalId] and m.opdrachtgeverId =@opdrachtgeverSession ) as [volgorde],
(SELECT actief from [MateriaalOpdrachtgevers] m where b.id = m.[materiaalId] and m.opdrachtgeverId =@opdrachtgeverSession ) as 'actief'
      ,[SerieVerplichtBijOpboeken], [onbestelbaar], [retourItem] ,[serienummerRegex],[bobijnArtikel],[foto],[MateriaalGroep],[MateriaalGroep2],[MateriaalGroep3],[ArticleInternal]
      ,[MateriaalType]
      ,[Consignatie]
	  FROM [Basismateriaal] b

	  where b.id in (select mo.materiaalId from [MateriaalOpdrachtgevers] mo where opdrachtgeverId = @opdrachtgeverSession ) 
	" UpdateCommand="UPDATE [Basismateriaal] SET [Article] = @Article,[eenheid]= @eenheid,[prijs]= @prijs, [Description] = @Description, [HeeftSerienummer] = CASE WHEN @retouritem = 1 THEN 1 ELSE ISNULL(@HeeftSerienummer,0) END, [SerieVerplichtBijOpboeken] = @SerieVerplichtBijOpboeken, [onbestelbaar]= @Onbestelbaar, [retourItem] = @retourItem ,[serienummerRegex] = @serienummerRegex,[bobijnArtikel]=@bobijnArtikel,  [foto] = CONVERT(varbinary(MAX), @foto), [MateriaalGroep]=@MateriaalGroep, [MateriaalGroep2]=@MateriaalGroep2,[MateriaalGroep3]=@MateriaalGroep3,[ArticleInternal] = @ArticleInternal
      ,[MateriaalType] = @MateriaalType
      ,[Consignatie]=@Consignatie WHERE [id] = @id;
UPDATE [MateriaalOpdrachtgevers] SET [min] = @min, [max] = @max, [minhoofd] = @minhoofd, [maxhoofd] = @maxhoofd, [Actief]= @actief, [volgorde]= @volgorde,  [locatie]= @locatie where [materiaalId] = @id and [opdrachtgeverId] = @opdrachtgeverSession">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverSession" SessionField="opdrachtgeverSession" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Article" Type="String" />
            <asp:Parameter Name="eenheid" />
            <asp:Parameter Name="prijs" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="retourItem"  Type="Boolean" />
            <asp:Parameter Name="HeeftSerienummer" Type="Boolean" />
            <asp:Parameter Name="SerieVerplichtBijOpboeken" Type="Boolean" />
            <asp:Parameter Name="Onbestelbaar"  Type="Boolean"/>
            <asp:Parameter Name="serienummerRegex" Type="String" />
            <asp:Parameter Name="bobijnArtikel" />
            <asp:Parameter Name="foto" />
            <asp:Parameter Name="MateriaalGroep" />
            <asp:Parameter Name="MateriaalGroep2" />
            <asp:Parameter Name="MateriaalGroep3" />
            <asp:Parameter Name="ArticleInternal" />
            <asp:Parameter Name="MateriaalType" />
            <asp:Parameter Name="Consignatie" />
            <asp:Parameter Name="id" />
            <asp:Parameter Name="min" Type="Decimal" />
            <asp:Parameter Name="max" Type="Decimal" />
            <asp:Parameter Name="minhoofd" Type="Decimal"  />
            <asp:Parameter Name="maxhoofd" Type="Decimal"  />
            <asp:Parameter Name="actief" Type="Boolean"/>
            <asp:Parameter Name="volgorde" Type="Int32" />
            <asp:Parameter Name="locatie" />
            <asp:SessionParameter Name="opdrachtgeverSession" SessionField="opdrachtgeverSession" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceGroepen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [MateriaalGroep] ORDER BY [omschrijving]">
    </asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceTypes" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [MateriaalType] ORDER BY [omschrijving]">
    </asp:SqlDataSource>
</asp:Content>