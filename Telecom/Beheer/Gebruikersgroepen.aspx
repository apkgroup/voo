﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Gebruikersgroepen.aspx.vb" Inherits="Telecom.Gebruikersgroepen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}

    </style>
    <script type="text/javascript">
        var groep = 0;
        function OnGroepChanged(cmbGroepen) {
            groep = cmbGroepen.GetValue();
            lijstLeden.PerformCallback(cmbGroepen.GetValue());
        }
      
        function Lidweg(id) {
           
            lijstLeden.DeleteRowByKey(id);
        }

        function Lidnaargroep(id) {
           
            lijstGebruikers.PerformCallback(id);
         
        }
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>  
     
    <br /> 
    <table>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabelGroepSelect" runat="server" Text="ASPxLabel">
                </dx:ASPxLabel>
            </td>
            <td>


                <dx:ASPxComboBox ID="cmbGroepen" ClientInstanceName="cmdGroepen" runat="server" DataSourceID="SqlDataSourceGroepen" EnableSynchronization="False" TextField="Naam" ValueField="id" ValueType="System.Int32">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
	OnGroepChanged(s);
}" />
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSourceGroepen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Naam] FROM [Gebruikersgroepen] where opdrachtgeverId = @opdrachtgever ORDER BY [Naam]">
                    <SelectParameters>
                        <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" />
                    </SelectParameters>
                </asp:SqlDataSource>


            </td>
            <td>
                <dx:ASPxLabel ID="ASPxLabellCreergroep" runat="server" Text="ASPxLabel">
                </dx:ASPxLabel>
            </td>
            <td>

                <dx:ASPxTextBox ID="ASPxTextBoxNieuweGroep" runat="server" MaxLength="25" Width="250px">
                </dx:ASPxTextBox>

            </td>
            <td>

                <dx:ASPxButton ID="ASPxButtonNieuwegroep" runat="server" Text="Créer">
                    <Image Url="~/images/Add_32x32.png" Height="20px" Width="20px">
                    </Image>
                </dx:ASPxButton>

            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;" colspan="2">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabelGroepleden" runat="server" Text="ASPxLabel">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">
                             <dx:ASPxGridView ID="lijstLeden" runat="server" AutoGenerateColumns="False" ClientInstanceName="lijstLeden" DataSourceID="SqlDataSourceLeden" KeyFieldName="id">
                                 <Columns>
                                     <dx:GridViewDataTextColumn FieldName="Werknemer" ReadOnly="True" VisibleIndex="0">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataHyperLinkColumn Caption=" " FieldName="id" ReadOnly="True" VisibleIndex="1">
                                         <PropertiesHyperLinkEdit ImageUrl="~/images/Close_16x16.png" NavigateUrlFormatString="javascript:Lidweg({0});" TextFormatString="">
                                         </PropertiesHyperLinkEdit>
                                      
                                     </dx:GridViewDataHyperLinkColumn>
                                 </Columns>
                                 <SettingsPager Mode="ShowAllRecords" Visible="False">
                                 </SettingsPager>
                                 <Settings ShowColumnHeaders="False" />
                                 <SettingsText EmptyDataRow="Aucune donnée chargée... groupe sélect svp" />
                                 <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                             </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSourceLeden" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT 
(SELECT
CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam
ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)
END
) as Werknemer,
 GebruikersgroepenLeden.id FROM GebruikersgroepenLeden INNER JOIN Gebruikers 
ON GebruikersgroepenLeden.Gebruiker=Gebruikers.id  WHERE (GebruikersgroepenLeden.Groep=@groep) and gebruikers.OpdrachtgeverId = @opdrachtgeverId ORDER BY Werknemer" DeleteCommand="DELETE FROM GebruikersgroepenLeden WHERE (id=@id)">
                    <DeleteParameters>
                        <asp:Parameter Name="id" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:Parameter Name="groep" />
                        <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
                    </SelectParameters>
                </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>

               


            </td>
            <td></td>
            <td style="vertical-align:top;" colspan="2">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabelGebruikers" runat="server" Text="ASPxLabel">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                       <td>
                             <dx:ASPxGridView ID="lijstGebruikers" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGebruikers" KeyFieldName="id" ClientInstanceName="lijstGebruikers">
                                 <Columns>
                                     <dx:GridViewDataTextColumn FieldName="Werknemer" ReadOnly="True" VisibleIndex="1">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataHyperLinkColumn FieldName="id" ReadOnly="True" VisibleIndex="0">
                                         <PropertiesHyperLinkEdit ImageUrl="~/images/Backward_16x16.png" NavigateUrlFormatString="javascript:Lidnaargroep({0});" TextFormatString="">
                                         </PropertiesHyperLinkEdit>
                                         <EditFormSettings Visible="False" />
                                     </dx:GridViewDataHyperLinkColumn>
                                 </Columns>
                                 <SettingsPager Mode="ShowAllRecords" Visible="False">
                                 </SettingsPager>
                                 <Settings ShowColumnHeaders="False" />
                                 <SettingsText EmptyDataRow="Geen data geladen...selecteer eerst een groep aub" />
                                 <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                 <ClientSideEvents
    EndCallback="function (s, e) {
        lijstLeden.PerformCallback(groep);
    }"
/>
                             </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSourceGebruikers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT (SELECT CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam
ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)
END
) as Werknemer, Gebruikers.id FROM Gebruikers WHERE gebruikers.opdrachtgeverId = @opdrachtgeverId ORDER BY Werknemer ">
                    <SelectParameters>
                        <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
                    </SelectParameters>

                </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</asp:Content>
