﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="PaklijstDetails.aspx.vb" Inherits="Telecom.PaklijstDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function OnInit(s, e) {  
    setTimeout(ChangeHint, 50);  
    s.ActiveItemChanged.AddHandler(function (s, args) { setTimeout(ChangeHint, 50); })  
}  

function ChangeHint() {  
    var src = imageZoom.GetMainElement().getElementsByClassName("dxiz-clipPanel")[0].getElementsByTagName("img")[0].src;  
    var hintElem = imageZoom.GetMainElement().getElementsByClassName("dxiz-hint")[0];  
    if (src.indexOf("empty") >= 0) {  
        if (!hintElem.classList.contains("MyClass"))  
            hintElem.classList.add("MyClass");  
    }  
    else {  
        if (hintElem.classList.contains("MyClass"))  
            hintElem.classList.remove("MyClass");  
    }  
        }  

        function showToevoegen(lijnId, omschrijving, artikel, aantal) {
            console.log(lijnId);
            ASPxHiddenFieldLijnId.Set('LijnId', lijnId); 
            ASPxTextBoxArtikelNummer.SetValue(artikel);
            ASPxSpinEditAantal.SetValue(aantal);
            ASPxTextBoxOmschrijving.SetValue(omschrijving);
            popupcontrolbox.Show();
        }



        function isNumber(n) {
    'use strict';
    n = n.toString().replace(/\./g, '').replace(',', '.');
    return n;
}
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        Paklijst Details: <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    </h2>

    <br />



            <asp:Literal ID="Literal2" runat="server"></asp:Literal>
     

    <br />
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn0" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id">
    </dx:ASPxComboBox>
    <br />



            <asp:Literal ID="Literal3" runat="server"></asp:Literal>
     

    <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Width="170px">
    </dx:ASPxTextBox>
    <br />
            <dx:ASPxButton ID="ASPxButton6" runat="server" Text="Verplaatsen">
    </dx:ASPxButton>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId and (g.Actief = 1 or hoofd=1 or type is not null) )">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
     

    <dx:ASPxLabel ID="ASPxLabelFout" runat="server" Font-Bold="True" ForeColor="Red">
    </dx:ASPxLabel>

    <dx:ASPxPanel ID="pnlForm" runat="server"  ClientVisible="false" ClientInstanceName="pnlForm" CssClass="minimargin" Paddings-PaddingLeft="5px" Paddings-PaddingBottom="5px">  
        <Paddings PaddingBottom="5px" />
    <PanelCollection>  
        <dx:PanelContent>  
            &nbsp;<br />
        </dx:PanelContent>  
    </PanelCollection>  
        <Border BorderColor="Silver" BorderWidth="1px" />
</dx:ASPxPanel>  
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" KeyFieldName="id" AutoGenerateColumns="False" DataSourceID="SqlDataSourcePaklijstLijnen" EnableRowsCache="False">
        <SettingsDetail ShowDetailRow="True" />
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="0" Visible="False">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="paklijstlijnId" VisibleIndex="1" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <SettingsPager PageSize="25">
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="article" ShowInCustomizationForm="True" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="omschrijving" ShowInCustomizationForm="True" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="goedgekeurd" ShowInCustomizationForm="True" VisibleIndex="4">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="aantal" ShowInCustomizationForm="True" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="Paklijst Ordernummer" FieldName="paklijstId" VisibleIndex="1">
                <PropertiesComboBox DataSourceID="SqlDataSourcePaklijst" TextField="ordernummer" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
    </dx:ASPxGridView>
     <h2>
         Lijnen met artikels nog niet aanwezig in systeem
    </h2>
    <dx:ASPxGridView ID="ASPxGridView3" runat="server" KeyFieldName="id" AutoGenerateColumns="False" DataSourceID="SqlDataSourcePaklijstLijnen0" EnableRowsCache="False">
        <SettingsDetail ShowDetailRow="True" />
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView4" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers0" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView4_BeforePerformDataSelect">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="paklijstlijnId" Visible="False" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <SettingsPager PageSize="25">
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="article" ShowInCustomizationForm="True" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="omschrijving" ShowInCustomizationForm="True" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="goedgekeurd" ShowInCustomizationForm="True" VisibleIndex="5" ReadOnly="True">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="aantal" ShowInCustomizationForm="True" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="Paklijst Ordernummer" FieldName="paklijstId" VisibleIndex="2" ReadOnly="True">
                <PropertiesComboBox DataSourceID="SqlDataSourcePaklijst" TextField="ordernummer" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn  FieldName="id" Caption="Artikel aanmaken" Width="180" VisibleIndex="14" CellStyle-HorizontalAlign="Center">  
      <DataItemTemplate >  
             <a class="tooltip" id="clickElement" href="javascript:void(0);" onclick="showToevoegen(<%# Eval("id")%>,'<%# Eval("omschrijving").ToString.Replace("""", "")%>','<%# Eval("article")%>',isNumber(<%# Eval("aantal")%>))">Toevoegen</a>
          
     </DataItemTemplate>  

<CellStyle HorizontalAlign="Center"></CellStyle>
</dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <br />
    <br />
   
    
    <dx:ASPxPopupControl ID="popupcontrolbox" runat="server" ClientInstanceName="popupcontrolbox" HeaderText="Artikel aanmaken: Details" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        
        <ContentCollection>
<dx:PopupControlContentControl runat="server">
 
    <table style="width:100%;">
        <tr>
            <td>Artikelnummer</td>
            <td>
                <dx:ASPxTextBox ID="ASPxTextBoxArtikelNummer" runat="server" Width="170px" ClientInstanceName="ASPxTextBoxArtikelNummer">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td>Omschrijving</td>
            <td>
                <dx:ASPxTextBox ID="ASPxTextBoxOmschrijving" runat="server" Width="170px" ClientInstanceName="ASPxTextBoxOmschrijving">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td>Aantal</td>
            <td>
                <dx:ASPxSpinEdit ID="ASPxSpinEditAantal" runat="server" Number="0" ClientInstanceName="ASPxSpinEditAantal">
                </dx:ASPxSpinEdit>
            </td>
        </tr>
    </table>
 
    <dx:ASPxHiddenField ID="ASPxHiddenFieldLijnId" runat="server" ClientInstanceName="ASPxHiddenFieldLijnId">
    </dx:ASPxHiddenField>
    <dx:ASPxButton ID="ASPxButton5" runat="server" Text="Inlezen">
    </dx:ASPxButton>
    <br />
            </dx:PopupControlContentControl>
</ContentCollection>
        
    </dx:ASPxPopupControl>
    <br />
    <asp:SqlDataSource ID="SqlDataSourcePaklijstLijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [PaklijstLijn] WHERE (([paklijstId] = @paklijstId) AND ([goedgekeurd] = @goedgekeurd))">
        <SelectParameters>
            <asp:QueryStringParameter Name="paklijstId" QueryStringField="id" Type="Int32" />
            <asp:Parameter DefaultValue="true" Name="goedgekeurd" Type="Boolean" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourcePaklijst" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Paklijst]">
    </asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id]
      ,[paklijstlijnId]
      ,[serienummer]
  FROM [Voo].[dbo].[PaklijstLijnSerienummer] WHERE ([paklijstlijnId] = @paklijstlijnId)">
        <SelectParameters>
            <asp:SessionParameter Name="paklijstlijnId" SessionField="paklijstlijnId" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceSerienummers0" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id]
      ,[paklijstlijnId]
      ,[serienummer]
  FROM [Voo].[dbo].[PaklijstLijnSerienummer] WHERE ([paklijstlijnId] = @paklijstlijnId)">
        <SelectParameters>
            <asp:SessionParameter Name="paklijstlijnId" SessionField="paklijstlijnId2" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourcePaklijstLijnen0" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [PaklijstLijn] WHERE [paklijstId] = @paklijstId AND ([goedgekeurd] = 0 or goedgekeurd is null)" DeleteCommand="DELETE FROM [PaklijstLijn] WHERE [id] = @id" InsertCommand="INSERT INTO [PaklijstLijn] ([paklijstId], [article], [omschrijving], [goedgekeurd], [aantal], [bobijnnummer]) VALUES (@paklijstId, @article, @omschrijving, @goedgekeurd, @aantal, @bobijnnummer)" UpdateCommand="UPDATE [PaklijstLijn] SET [paklijstId] = @paklijstId, [article] = @article, [omschrijving] = @omschrijving, [goedgekeurd] = @goedgekeurd, [aantal] = @aantal, [bobijnnummer] = @bobijnnummer WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="paklijstId" Type="Int32" />
            <asp:Parameter Name="article" Type="String" />
            <asp:Parameter Name="omschrijving" Type="String" />
            <asp:Parameter Name="goedgekeurd" Type="Boolean" />
            <asp:Parameter Name="aantal" Type="Decimal" />
            <asp:Parameter Name="bobijnnummer" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="paklijstId" QueryStringField="id" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="paklijstId" Type="Int32" />
            <asp:Parameter Name="article" Type="String" />
            <asp:Parameter Name="omschrijving" Type="String" />
            <asp:Parameter Name="goedgekeurd" Type="Boolean" />
            <asp:Parameter Name="aantal" Type="Decimal" />
            <asp:Parameter Name="bobijnnummer" Type="String" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
                        
                   
                    </asp:Content>