﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="GebruikersgroepenKleurenPlanning.aspx.vb" Inherits="Telecom.GebruikersgroepenKleurenPlanning" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Beheer gebruikersgroepen kleuren (voor planning)</h2>       
    <br /> 
     <p><span style="font-family:Calibri;font-size:small;">Gebruikersgroepen met een kleur worden gebruikt in de planning, gebruikersgroepen zonder kleur worden niet meegenomen in de planning.</span></p>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGebruikersgroepen" KeyFieldName="id">
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Naam" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataColorEditColumn FieldName="Kleur" VisibleIndex="3">
            </dx:GridViewDataColorEditColumn>
        </Columns>
         <SettingsPager Mode="ShowAllRecords" Visible="False">
                                 </SettingsPager>
                   
                                 <SettingsText EmptyDataRow="Geen data geladen... " />
        <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
          <SettingsCommandButton>
             <NewButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Nieuw" Url="~/images/Add_32x32.png" Width="16px">
                 </Image>
             </NewButton>
             <UpdateButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewaren" Url="~/images/Save_16x16.png" Width="16px">
                 </Image>
             </UpdateButton>
             <CancelButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Annuleren" Url="~/images/annuleren.png" Width="16px">
                 </Image>
             </CancelButton>
             <EditButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewerken" Url="~/images/Edit.png" Width="16px">
                 </Image>
             </EditButton>
             <DeleteButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Verwijderen" Url="~/images/Folders-OS-Recycle-Bin-Full-Metro-icon.png" Width="16px">
                 </Image>
             </DeleteButton>
         </SettingsCommandButton>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceGebruikersgroepen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Gebruikersgroepen] WHERE [id] = @id" InsertCommand="INSERT INTO [Gebruikersgroepen] ([Naam], [Kleur]) VALUES (@Naam, @Kleur)" SelectCommand="SELECT [id], [Naam], [Kleur] FROM [Gebruikersgroepen] ORDER BY [Naam]" UpdateCommand="UPDATE [Gebruikersgroepen] SET [Naam] = @Naam, [Kleur] = @Kleur WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Naam" Type="String" />
            <asp:Parameter Name="Kleur" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Naam" Type="String" />
            <asp:Parameter Name="Kleur" Type="String" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br /> 

</asp:Content>
