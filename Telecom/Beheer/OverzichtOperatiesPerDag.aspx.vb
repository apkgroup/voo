﻿Public Class OverzichtOperatiesPerDag
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub

    Protected Sub ASPxDateEdit1_DateChanged(sender As Object, e As EventArgs) Handles ASPxDateEdit1.DateChanged
        Session("Tijd") = ASPxDateEdit1.Value
        ASPxGridView1.DataBind()
        
    End Sub

    Protected Sub ASPxDateEdit1_Load(sender As Object, e As EventArgs) Handles ASPxDateEdit1.Load
        If Not Page.IsPostBack Then
            ASPxDateEdit1.Date = DateTime.Today
        End If
    End Sub
End Class