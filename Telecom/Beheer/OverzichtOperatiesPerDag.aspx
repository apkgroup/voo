﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="OverzichtOperatiesPerDag.aspx.vb" Inherits="Telecom.OverzichtOperatiesPerDag" %>
<%@ Register assembly="DevExpress.Web.ASPxScheduler.v19.2, Version=19.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxScheduler" tagprefix="dxwschs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h2>Opérations par jour</h2>
    <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" AutoPostBack="True">
    </dx:ASPxDateEdit>
    <br />
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
        <SettingsPager NumericButtonCount="20" PageSize="20">
        </SettingsPager>
        <Columns>
            <dx:GridViewDataTextColumn Caption="Nom" FieldName="Werknemer" ReadOnly="True" VisibleIndex="0">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Nombre" FieldName="Nombre" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
     bb
</asp:Content>
