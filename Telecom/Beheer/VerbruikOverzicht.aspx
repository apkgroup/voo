﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="VerbruikOverzicht.aspx.vb" Inherits="Telecom.VerbruikOverzicht" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        Verbruik per lijn
        </h2>
    <br />
    <br />
    <asp:SqlDataSource ID="SqlDataSourceStockMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [StockMagazijn] WHERE [id] = @id" InsertCommand="INSERT INTO [StockMagazijn] ([MateriaalId], [MagazijnId], [Aantal]) VALUES (@MateriaalId, @MagazijnId, @Aantal)" SelectCommand="/****** Script for SelectTopNRows command from SSMS  ******/
SELECT
vl.id,
[datum]
 ,mvan.Naam as 'Verbruikt van'
	  ,mop.Naam as 'Verbruikt op'
	  ,b.Article
      ,b.Description
	  ,(select bobijnnummer from voo.dbo.Bobijn bb where bb.id = [bobijnId]) as bobijnnr
      ,[hoeveelheid]
      ,[ordernummer]
      ,(select exnaam from gebruikers gg where gg.id =[gebruiker]) as gebruiker
      ,[stad]
      ,[straat]
	  ,CASE 
WHEN [status] = 1 THEN 'Begonnen met ingeven'
WHEN [status] = 2 THEN 'Voltooid'
END as status
,vl.verwerkt
      ,vl.prijs, (vl.prijs*vl.hoeveelheid) as totaal
  FROM [Voo].[dbo].[Verbruiklijn] vl
  left join [Voo].[dbo].[Verbruik] v on vl.verbruikId = v.id
  left join  [Voo].[dbo].magazijn mvan on v.verbruiktvan = mvan.id
    left join  [Voo].[dbo].magazijn mop on v.verbruiktop = mop.id
	left join [Voo].[dbo].basismateriaal b on b.id = vl.materiaalId 
	where hoeveelheid &lt;&gt; 0 and (mop.OpdrachtgeverId = @opdrachtgeverId or mvan.OpdrachtgeverId = @opdrachtgeverId)
	order by vl.id desc
" UpdateCommand="UPDATE [Voo].[dbo].[Verbruiklijn] SET  [Verwerkt]= @Verwerkt WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Verwerkt" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
     <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceStockMagazijn" KeyFieldName="id">
        <SettingsDetail ShowDetailRow="True" />
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2" Caption="Serie" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="beschrijving" VisibleIndex="5" Caption="Status" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Lotnr" FieldName="lotNr" VisibleIndex="6" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Creation" FieldName="datumGemaakt" ShowInCustomizationForm="True" VisibleIndex="7">
                        </dx:GridViewDataDateColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <SettingsPager PageSize="25">
        </SettingsPager>
        <SettingsEditing Mode="Batch">
        </SettingsEditing>
        <Settings ShowFilterBar="Auto" ShowHeaderFilterButton="True" ShowFilterRow="True" />
        <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
        <SettingsPopup>
            <HeaderFilter MinHeight="350px" MinWidth="600px">
            </HeaderFilter>
        </SettingsPopup>
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" VisibleIndex="0" ReadOnly="True" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="datum" VisibleIndex="1" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="Verbruikt van" VisibleIndex="2" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Verbruikt op" VisibleIndex="3" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="4" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="5" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="bobijnnr" ReadOnly="True" VisibleIndex="6">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="hoeveelheid" VisibleIndex="7" ReadOnly="True">
    <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ordernummer" VisibleIndex="8" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="gebruiker" ReadOnly="True" VisibleIndex="9">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="stad" VisibleIndex="10" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="straat" VisibleIndex="11" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="status" VisibleIndex="12" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="verwerkt" VisibleIndex="13">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="prijs" VisibleIndex="14">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Totaal" FieldName="totaal" VisibleIndex="15">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT serienummer, lotNr, datumGemaakt, st.beschrijving  
FROM StockbewegingSerienummer sbsr 
inner join Serienummer sr on sbsr.serienummerId = sr.id
left join Status st on sr.statusId = st.id WHERE (stockbewegingid = @stockbewegingid)">
        <SelectParameters>
            <asp:SessionParameter Name="stockbewegingid" SessionField="stockbeweging" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
     <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
     </dx:ASPxGridViewExporter>
    <br />
</asp:Content>