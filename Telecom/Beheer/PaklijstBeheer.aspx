﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="PaklijstBeheer.aspx.vb" Inherits="Telecom.PaklijstBeheer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function OnInit(s, e) {  
    setTimeout(ChangeHint, 50);  
    s.ActiveItemChanged.AddHandler(function (s, args) { setTimeout(ChangeHint, 50); })  
}  

function ChangeHint() {  
    var src = imageZoom.GetMainElement().getElementsByClassName("dxiz-clipPanel")[0].getElementsByTagName("img")[0].src;  
    var hintElem = imageZoom.GetMainElement().getElementsByClassName("dxiz-hint")[0];  
    if (src.indexOf("empty") >= 0) {  
        if (!hintElem.classList.contains("MyClass"))  
            hintElem.classList.add("MyClass");  
    }  
    else {  
        if (hintElem.classList.contains("MyClass"))  
            hintElem.classList.remove("MyClass");  
    }  
}  
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        Paklijsten
    </h2>

    <br />
    <dx:ASPxLabel ID="ASPxLabelFout" runat="server" Font-Bold="True" ForeColor="Red">
    </dx:ASPxLabel>

    <dx:ASPxPanel ID="pnlForm" runat="server"  ClientVisible="false" ClientInstanceName="pnlForm" CssClass="minimargin" Paddings-PaddingLeft="5px" Paddings-PaddingBottom="5px">  
        <Paddings PaddingBottom="5px" />
    <PanelCollection>  
        <dx:PanelContent>  
            &nbsp;<br />
        </dx:PanelContent>  
    </PanelCollection>  
        <Border BorderColor="Silver" BorderWidth="1px" />
</dx:ASPxPanel>  
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" KeyFieldName="id" AutoGenerateColumns="False" DataSourceID="SqlDataSourcePaklijsten" EnableRowsCache="False">
        <SettingsPager PageSize="25">
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsSearchPanel Visible="True" />
        <EditFormLayoutProperties ColCount="2" ColumnCount="2">
            <Items>
                <dx:GridViewColumnLayoutItem ColSpan="1" ColumnName="voorzieneLocatie">
                </dx:GridViewColumnLayoutItem>
                <dx:GridViewColumnLayoutItem ColSpan="1" ColumnName="opmerking">
                </dx:GridViewColumnLayoutItem>
                <dx:EditModeCommandLayoutItem ColSpan="2" ColumnSpan="2" HorizontalAlign="Right">
                </dx:EditModeCommandLayoutItem>
            </Items>
        </EditFormLayoutProperties>
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" VisibleIndex="1" ReadOnly="True" Visible="False" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ordernummer" VisibleIndex="4" ShowInCustomizationForm="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="datumIngelezen" VisibleIndex="5" ShowInCustomizationForm="False">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataDateColumn FieldName="datumVerplaatst" VisibleIndex="6" ShowInCustomizationForm="False">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="opmerking" VisibleIndex="7" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataCheckColumn FieldName="afgehandeld" VisibleIndex="9" ShowInCustomizationForm="True">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="opdrachtgeverId" VisibleIndex="10" Visible="False" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn FieldName="voorzieneLocatie" VisibleIndex="3" ShowInCustomizationForm="False">
                <PropertiesComboBox DataSourceID="SqlDataSourceMagazijn" TextField="Naam" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Huidige locatie" FieldName="magazijnId" VisibleIndex="2" ShowInCustomizationForm="False">
                <PropertiesComboBox DataSourceID="SqlDataSourceMagazijn" TextField="Naam" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Plaats (rek)" FieldName="locatieStandaardId" VisibleIndex="8" Visible="False">
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataHyperLinkColumn Caption="Details" FieldName="id" VisibleIndex="11">
                <PropertiesHyperLinkEdit NavigateUrlFormatString="../Beheer/PaklijstDetails.aspx?id={0}" Text="Details...">
                </PropertiesHyperLinkEdit>
            </dx:GridViewDataHyperLinkColumn>
            <dx:GridViewDataTextColumn Caption="JMS Nummer" FieldName="JMSNummer" VisibleIndex="12">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <br />
    <asp:SqlDataSource ID="SqlDataSourcePaklijsten" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Paklijst] WHERE ([opdrachtgeverId] = @opdrachtgeverId)
order by id desc" UpdateCommand="UPDATE [Paklijst] SET [voorzieneLocatie] = @voorzieneLocatie, [ordernummer] = @ordernummer, [datumIngelezen] = @datumIngelezen, [datumVerplaatst] = @datumVerplaatst, [opmerking] = @opmerking, [locatieStandaardId] = @locatieStandaardId, [afgehandeld] = @afgehandeld, [opdrachtgeverId] = @opdrachtgeverId, [magazijnId] = @magazijnId WHERE [id] = @id" DeleteCommand="DELETE FROM [Paklijst] WHERE [id] = @id" InsertCommand="INSERT INTO [Paklijst] ([voorzieneLocatie], [ordernummer], [datumIngelezen], [datumVerplaatst], [opmerking], [locatieStandaardId], [afgehandeld], [opdrachtgeverId], [magazijnId]) VALUES (@voorzieneLocatie, @ordernummer, @datumIngelezen, @datumVerplaatst, @opmerking, @locatieStandaardId, @afgehandeld, @opdrachtgeverId, @magazijnId)">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="voorzieneLocatie" Type="Int32" />
            <asp:Parameter Name="ordernummer" Type="Int32" />
            <asp:Parameter Name="datumIngelezen" Type="DateTime" />
            <asp:Parameter Name="datumVerplaatst" Type="DateTime" />
            <asp:Parameter Name="opmerking" Type="String" />
            <asp:Parameter Name="locatieStandaardId" Type="Int32" />
            <asp:Parameter Name="afgehandeld" Type="Boolean" />
            <asp:Parameter Name="opdrachtgeverId" Type="Int32" />
            <asp:Parameter Name="magazijnId" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="voorzieneLocatie" Type="Int32" />
            <asp:Parameter Name="ordernummer" Type="Int32" />
            <asp:Parameter Name="datumIngelezen" Type="DateTime" />
            <asp:Parameter Name="datumVerplaatst" Type="DateTime" />
            <asp:Parameter Name="opmerking" Type="String" />
            <asp:Parameter Name="locatieStandaardId" Type="Int32" />
            <asp:Parameter Name="afgehandeld" Type="Boolean" />
            <asp:Parameter Name="opdrachtgeverId" Type="Int32" />
            <asp:Parameter Name="magazijnId" Type="Int32" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn]">
    </asp:SqlDataSource>
    <br />
                        
                   
                    </asp:Content>