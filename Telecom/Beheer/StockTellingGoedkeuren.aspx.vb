﻿Imports DevExpress.Export

Public Class StockTellingGoedkeuren
    Inherits System.Web.UI.Page

    Protected Sub vertaal(taalid)

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Page.IsPostBack Then
            'ASPxGridView1.FilterExpression = "[beschrijving] = 'Aangevraagd'"
        End If

    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        Dim selectedids = ASPxGridView1.GetSelectedFieldValues("id")
        Dim idstring As String = ""
        If selectedids.Count > 0 Then
            For Each selected In selectedids
                idstring = idstring & selected.ToString & ", "
            Next
            idstring = idstring.Substring(0, idstring.Length - 2)

            SqlDataSourceMateriauxADMIN.SelectCommand = "SELECT sl.[id]
	  ,m.naam
	  ,b.Article
	  ,b.Description
	  ,b.HeeftSerienummer
	  ,s.datumdoorgestuurd
      ,[hoeveelheid] as Hoeveelheid
	  ,hoeveelheidSnapshot
  FROM [Voo].[dbo].[Stocktellinglijn] sl
  inner join StockTelling s on sl.stocktellingId = s.id
  inner join Basismateriaal b on sl.materiaalId = b.id
  inner join magazijn m on s.magazijnId = m.id
    where s.status = 2 and hoeveelheid <> hoeveelheidSnapshot  and m.opdrachtgeverId = @opdrachtgever
	and stocktellingId in (" & idstring & ")"

            ASPxGridView2.DataBind()
            ASPxGridViewExporter2.FileName = "Export Foutrapport " & Today.Date()
            ASPxGridViewExporter2.WriteXlsxToResponse(New DevExpress.XtraPrinting.XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})

        End If

    End Sub
End Class