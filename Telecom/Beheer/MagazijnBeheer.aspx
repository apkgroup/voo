﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="MagazijnBeheer.aspx.vb" Inherits="Telecom.MagazijnBeheer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        </h2>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId and (g.Actief = 1 or hoofd=1 or type is not null) )">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
    <br />
    <br />
     <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu>
    <asp:SqlDataSource ID="SqlDataSourceStockMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [StockMagazijn] WHERE [id] = @id" InsertCommand="INSERT INTO [StockMagazijn] ([MateriaalId], [MagazijnId], [Aantal]) VALUES (@MateriaalId, @MagazijnId, @Aantal)" SelectCommand="SELECT distinct  s.[id],[naam],b.Article,b.Description, s.[MateriaalId], [MagazijnId], [Aantal] as 'Totaal aantal'
,mo2.minhoofd, m.hoofd,
(select count(sr.id) from Serienummer sr inner join StockMagazijn sm on sr.StockMagazijnId = sm.id where sm.MateriaalId = s.MateriaalId and (sr.statusId = 1 or sr.statusId=2) and [MagazijnId] = @MagazijnId ) 
+ COALESCE((select sum(hoeveelheid) from bestellijn bl inner join bestellingen best on bl.bestellingId = best.id  inner join gebruikers g on best.GebruikerId = g.id where best.status = 4 and g.OpdrachtgeverId = @opdrachtgeverId and bl.materiaalId = s.MateriaalId ),0) as AantalKapot,
(Select omschrijving from voo.dbo.MateriaalGroep mg where mg.id =b.[MateriaalGroep]) as 'Materiaalgroep'
,[Aantal]-(select count(sr.id) from Serienummer sr inner join StockMagazijn sm on sr.StockMagazijnId = sm.id where sm.MateriaalId = s.MateriaalId and  (sr.statusId = 1 or sr.statusId=2) and [MagazijnId] = @MagazijnId ) - COALESCE((select sum(hoeveelheid) from bestellijn bl inner join bestellingen best on bl.bestellingId = best.id  inner join gebruikers g on best.GebruikerId = g.id where best.status = 4 and g.OpdrachtgeverId = @opdrachtgeverId and bl.materiaalId = s.MateriaalId ),0)  as Beschikbaar
 FROM [StockMagazijn] s
INNER JOIN Magazijn m on s.MagazijnId = m.id 
INNER JOIN Basismateriaal b on s.MateriaalId = b.id
INNER JOIN [MateriaalOpdrachtgevers] mo2 on mo2.MateriaalId = b.id and mo2.opdrachtgeverId = @opdrachtgeverId 
WHERE ([MagazijnId] = @MagazijnId)
AND b.id in (select mo.materiaalId from [MateriaalOpdrachtgevers] mo where opdrachtgeverId = @opdrachtgeverId and (actief = 1 or actief is null))
" UpdateCommand="UPDATE [StockMagazijn] SET [MateriaalId] = @MateriaalId, [MagazijnId] = @MagazijnId, [Aantal] = @Aantal WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ASPxComboBoxMagazijn" Name="MagazijnId" PropertyName="Value" Type="Int32" />
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <dx:ASPxCheckBox ID="ASPxCheckBoxNulwaarden" runat="server" Height="16px" Text="Nulwaarden zien" Width="16px" AutoPostBack="true">
    </dx:ASPxCheckBox>
    <asp:SqlDataSource ID="SqlDataSourceStockMagazijnNiet0" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [StockMagazijn] WHERE [id] = @id" InsertCommand="INSERT INTO [StockMagazijn] ([MateriaalId], [MagazijnId], [Aantal]) VALUES (@MateriaalId, @MagazijnId, @Aantal)" SelectCommand="SELECT distinct  s.[id],[naam],b.Article,b.Description, s.[MateriaalId], [MagazijnId], [Aantal] as 'Totaal aantal'
,mo2.minhoofd, m.hoofd,
(select count(sr.id) from Serienummer sr inner join StockMagazijn sm on sr.StockMagazijnId = sm.id where sm.MateriaalId = s.MateriaalId and (sr.statusId = 1 or sr.statusId=2) and [MagazijnId] = @MagazijnId ) 
+ COALESCE((select sum(hoeveelheid) from bestellijn bl inner join bestellingen best on bl.bestellingId = best.id  inner join gebruikers g on best.GebruikerId = g.id where best.status = 4 and g.OpdrachtgeverId = @opdrachtgeverId and bl.materiaalId = s.MateriaalId ),0) as AantalKapot,
(Select omschrijving from voo.dbo.MateriaalGroep mg where mg.id =b.[MateriaalGroep]) as 'Materiaalgroep'
,[Aantal]-(select count(sr.id) from Serienummer sr inner join StockMagazijn sm on sr.StockMagazijnId = sm.id where sm.MateriaalId = s.MateriaalId and  (sr.statusId = 1 or sr.statusId=2) and [MagazijnId] = @MagazijnId ) - COALESCE((select sum(hoeveelheid) from bestellijn bl inner join bestellingen best on bl.bestellingId = best.id  inner join gebruikers g on best.GebruikerId = g.id where best.status = 4 and g.OpdrachtgeverId = @opdrachtgeverId and bl.materiaalId = s.MateriaalId ),0)  as Beschikbaar
 FROM [StockMagazijn] s
INNER JOIN Magazijn m on s.MagazijnId = m.id 
INNER JOIN Basismateriaal b on s.MateriaalId = b.id
INNER JOIN [MateriaalOpdrachtgevers] mo2 on mo2.MateriaalId = b.id and mo2.opdrachtgeverId = @opdrachtgeverId 
WHERE ([MagazijnId] = @MagazijnId)
AND b.id in (select mo.materiaalId from [MateriaalOpdrachtgevers] mo where opdrachtgeverId = @opdrachtgeverId and (actief = 1 or actief is null)) and aantal &lt;&gt; 0
" UpdateCommand="UPDATE [StockMagazijn] SET [MateriaalId] = @MateriaalId, [MagazijnId] = @MagazijnId, [Aantal] = @Aantal WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ASPxComboBoxMagazijn" Name="MagazijnId" PropertyName="Value" Type="Int32" />
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceStockMagazijnNiet0" KeyFieldName="id">
        <SettingsDetail ShowDetailRow="True" />
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2" Caption="Serie" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="beschrijving" VisibleIndex="5" Caption="Status" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Lotnr" FieldName="lotNr" VisibleIndex="6" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Creation" FieldName="datumGemaakt" ShowInCustomizationForm="True" VisibleIndex="7">
                        </dx:GridViewDataDateColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <SettingsPager PageSize="25">
        </SettingsPager>
        <SettingsEditing Mode="Batch">
        </SettingsEditing>
        <Settings ShowHeaderFilterButton="True" />
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="0" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="naam" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MateriaalId" VisibleIndex="4" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MagazijnId" VisibleIndex="5" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Totaal aantal" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="AantalKapot" ReadOnly="True" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Beschikbaar" ReadOnly="True" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataHyperLinkColumn Caption="Retour" FieldName="id" VisibleIndex="9">
                <PropertiesHyperLinkEdit NavigateUrlFormatString="../RetourAll.aspx?id={0}" Text="Retour All">
                </PropertiesHyperLinkEdit>
            </dx:GridViewDataHyperLinkColumn>
            <dx:GridViewDataTextColumn FieldName="Materiaalgroep" VisibleIndex="10">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="minhoofd" ShowInCustomizationForm="True" Visible="False" VisibleIndex="11">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="hoofd" Visible="False" VisibleIndex="12">
            </dx:GridViewDataTextColumn>
        </Columns>
        <FormatConditions>
            <dx:GridViewFormatConditionHighlight ApplyToRow="True" Expression="[hoofd] = 1 And [Beschikbaar] &lt; [minhoofd]" FieldName="Beschikbaar" Format="YellowFillWithDarkYellowText">
            </dx:GridViewFormatConditionHighlight>
        </FormatConditions>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT serienummer, lotNr, datumGemaakt, st.beschrijving  FROM [Serienummer] sr left join Status st on sr.statusId = st.id WHERE ([StockMagazijnId] = @StockMagazijnId) and (uitgeboekt &lt;&gt; 1 or uitgeboekt is null)
">
        <SelectParameters>
            <asp:SessionParameter Name="StockMagazijnId" SessionField="stockmagazijn" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
     <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
     </dx:ASPxGridViewExporter>
    <br />
</asp:Content>