﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="MagazijnToevoegen.aspx.vb" Inherits="Telecom.MagazijnToevoegen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 32px;
        }
        .auto-style2 {
            font-family: "Segoe UI", Helvetica, "Droid Sans", Tahoma, Geneva, sans-serif;
            font-size: 12px;
            color: #333333;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2><asp:Literal ID="LiteralTitel" runat="server"></asp:Literal>
        
       
    </h2>
     <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="ASPxLabel" Font-Bold="True" ForeColor="#CC0000">
        </dx:ASPxLabel>
        <table style="width:100%;">
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Naam *">
                    </dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="ASPxTextBoxNaam" ClientInstanceName="ASPxTextBoxNaam" runat="server" Width="170px">
                        <ValidationSettings>
                            <RegularExpression ValidationExpression="^.{1,50}$" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>

                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    Profitcenter</td>
                <td>

                    <dx:ASPxComboBox ID="ASPxComboBoxProfitcenter" ClientInstanceName="ASPxComboBoxProfitcenter" runat="server" DataSourceID="SqlDataSourceProfitcenter" TextField="faktn" ValueField="faktn">
                        <ClientSideEvents ValueChanged="function(s, e) {
                            	if(ASPxTextBoxNaam.GetValue()===null){
                            ASPxTextBoxNaam.SetValue(ASPxComboBoxProfitcenter.GetValue());
                            }
                            }" />
                        <Items>
                            <dx:ListEditItem Text="Nederlands" Value="1" />
                            <dx:ListEditItem Text="Français" Value="2" />
                        </Items>
                        <ValidationSettings>
                            <RegularExpression ValidationExpression="^.{1,50}$" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>

                </td>
            </tr>
            
            <tr>
                <td class="auto-style1">

                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Type">
                    </dx:ASPxLabel>

                </td>
                <td class="auto-style1">

                    <dx:ASPxComboBox ID="ASPxComboBoxType" runat="server" DataSourceID="SqlDataSourceType" TextField="omschrijving" ValueField="id" SelectedIndex="0">
                        <ValidationSettings>
                            <RegularExpression ValidationExpression="^.{1,50}$" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>

                    <asp:SqlDataSource ID="SqlDataSourceType" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [MagazijnType]"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceProfitcenter" runat="server" ConnectionString="<%$ ConnectionStrings:ProjectSQLConnectionString %>" SelectCommand="SELECT [faktn] FROM [projecten] WHERE ([afpc] &lt;&gt; @afpc)">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="true" Name="afpc" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                </td>
            </tr>


            <tr><td>

                &nbsp;</td>
                <td>

                    <dx:ASPxButton ID="ASPxButton1" runat="server">
                        <Image Url="~/images/Add_32x32.png">
                        </Image>
                    </dx:ASPxButton>

                </td>
            </tr>
        </table>

    <br />
     <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu>
     <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
     </dx:ASPxGridViewExporter>

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMagazijnen" KeyFieldName="id">
        <SettingsSearchPanel Visible="True" />
        <EditFormLayoutProperties ColCount="2" ColumnCount="2">
            <Items>
                <dx:GridViewColumnLayoutItem ColSpan="1" ColumnName="Naam">
                </dx:GridViewColumnLayoutItem>
                <dx:GridViewColumnLayoutItem ColSpan="1" ColumnName="AF">
                </dx:GridViewColumnLayoutItem>
                <dx:GridViewColumnLayoutItem ColSpan="1" ColumnName="profitcenter">
                </dx:GridViewColumnLayoutItem>
                <dx:EditModeCommandLayoutItem ColSpan="2" ColumnSpan="2" HorizontalAlign="Right">
                </dx:EditModeCommandLayoutItem>
            </Items>
        </EditFormLayoutProperties>
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" VisibleIndex="1" ReadOnly="True" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Naam" VisibleIndex="2">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="Hoofd" Visible="False" VisibleIndex="3">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="OpdrachtgeverId" Visible="False" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="type" Visible="False" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="AF" VisibleIndex="6">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataComboBoxColumn FieldName="profitcenter" VisibleIndex="7">
                <PropertiesComboBox DataSourceID="SqlDataSourceProfitcenter" TextField="faktn" ValueField="faktn">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
    </dx:ASPxGridView>

    
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="select
[id]
      ,[Naam]
      ,[Hoofd]
      ,[OpdrachtgeverId]
      ,[type],AF
      ,[profitcenter]   FROM [Voo].[dbo].[Magazijn]
WHERE (([type] = 1) OR ([type] = 2)) AND opdrachtgeverId = @opdrachtgever" UpdateCommand="update [Voo].[dbo].[Magazijn] set AF=@AF, naam=@naam, [profitcenter]=@profitcenter where  id=@id">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="AF" />
            <asp:Parameter Name="naam" />
            <asp:Parameter Name="profitcenter" />
            <asp:Parameter Name="id" />
        </UpdateParameters>
    </asp:SqlDataSource>

    
</asp:Content>
