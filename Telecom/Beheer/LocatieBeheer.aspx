﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="LocatieBeheer.aspx.vb" Inherits="Telecom.LocatieBeheer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
    <p>&nbsp;</p>
     <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" Text="" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
                </dx:ASPxLabel>

                <dx:ASPxButton ID="ASPxButtonToevoegen" runat="server" AutoPostBack="false" Width="50px" Text="Nieuwe locatie toevoegen" CssClass="minimargin">
                     <ClientSideEvents Click="function(s, e) { pnlForm.SetVisible(true); }" />  
                </dx:ASPxButton>

    <dx:ASPxPanel ID="pnlForm" runat="server"  ClientVisible="false" ClientInstanceName="pnlForm" CssClass="minimargin" Paddings-PaddingLeft="5px" Paddings-PaddingBottom="5px" Width="400px">  
        <Paddings PaddingBottom="5px" />
    <PanelCollection>  
        <dx:PanelContent>  
            &nbsp;<table style="width:50%;">
                <tr>
                    <td style="text-align: right">Locatienaam:</td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextBoxNaam" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>                    
                </tr>   
            </table>
            <h2>&nbsp;</h2>
            <dx:ASPxButton ID="ASPxButtonToevoegen0" runat="server" Width="50px">
                <Image IconID="actions_add_32x32office2013">
                </Image>
            </dx:ASPxButton>
        </dx:PanelContent>  
    </PanelCollection>  
        <Border BorderColor="Silver" BorderWidth="1px" />
</dx:ASPxPanel>  
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"  DataSourceID="SqlDataSourceLocatie" KeyFieldName="id">
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
         <SettingsDetail ShowDetailRow="True" />
         <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="DataSourceSerie" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect" Width="344px">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="id" VisibleIndex="0" ReadOnly="True">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="materiaalbeschrijving" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn FieldName="datumGemaakt" VisibleIndex="3">
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn FieldName="statusbeschrijving" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="DataSourceSerie" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT s.id
      ,b.Description as materiaalbeschrijving
	  ,s.serienummer 
	  ,s.datumGemaakt
	  ,stat.beschrijving as statusbeschrijving
  FROM [Voo].[dbo].[Serienummer] s inner join voo.dbo.StockMagazijn sm on s.StockMagazijnId = sm.id 
  inner join voo.dbo.basismateriaal b on sm.MateriaalId = b.id 
  inner join voo.dbo.status stat on stat.id = s.statusId where s.locatieId = @locatieid;">
                    <SelectParameters>
                        <asp:SessionParameter Name="locatieId" SessionField="LocatieId" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </DetailRow>
        </Templates>
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" ShowInCustomizationForm="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="magazijnid" VisibleIndex="2" ShowInCustomizationForm="True" Visible="False">
            </dx:GridViewDataTextColumn>           
            <dx:GridViewDataTextColumn FieldName="naam" VisibleIndex="3" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>           
            <dx:GridViewDataTextColumn FieldName="Huidig aantal" ReadOnly="True" VisibleIndex="4" ShowInCustomizationForm="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceLocatie" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [magazijnid],  [naam], 
(select count(*) from (select * from Serienummer where Locatieid = l.id and (uitgeboekt is null or uitgeboekt = 0)) as p) as 'Huidig aantal'
FROM [Locatie] l WHERE ([magazijnid] = @magazijnid);






" DeleteCommand="DELETE FROM [Locatie] WHERE [id] = @id" InsertCommand="INSERT INTO [Locatie] ([magazijnid], [capaciteit], [naam]) VALUES (@magazijnid, @capaciteit, @naam)" UpdateCommand="UPDATE [Locatie] SET [magazijnid] = @magazijnid, [naam] = @naam WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="magazijnid" Type="Int32" />
            <asp:Parameter Name="capaciteit" Type="Int32" />
            <asp:Parameter Name="naam" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ASPxComboBoxMagazijn" Name="magazijnid" PropertyName="Value" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="magazijnid" Type="Int32" />
            <asp:Parameter Name="naam" Type="String" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) and hoofd =1">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>