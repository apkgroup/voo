﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="PostenBeheer.aspx.vb" Inherits="Telecom.PostenBeheer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Postes</h2>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourcePosten" KeyFieldName="id">
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsBehavior ConfirmDelete="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Poste" VisibleIndex="1">
                <PropertiesTextEdit>
                    <ValidationSettings CausesValidation="True">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="2">
                <PropertiesTextEdit>
                    <ValidationSettings CausesValidation="True">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="5">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataComboBoxColumn FieldName="groepid" VisibleIndex="4" Caption="Groupe">
                <PropertiesComboBox DataSourceID="SqlDataSource1" TextField="groep" ValueField="groepid" ValueType="System.Int32">
                    <ValidationSettings CausesValidation="True">
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id] as 'groepid', [groep] FROM [Postengroepen]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourcePosten" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Basisposten] WHERE [id] = @id" InsertCommand="INSERT INTO [Basisposten] ([Poste], [Description], [Groupe],[opdrachtgeverId]) VALUES (@Poste, @Description, @groepid, @OpdrachtgeverId)" SelectCommand="SELECT [Basisposten].[id], [Poste], [Description], Postengroepen.groep, Postengroepen.id as 'groepid' FROM [Basisposten]
inner join postengroepen on  [Basisposten].[Groupe] = postengroepen.id
where opdrachtgeverId = @OpdrachtgeverId"
        UpdateCommand="UPDATE [Basisposten] SET [Poste] = @Poste, [Description] = @Description, [Groupe] = @groepid WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Poste" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="groepid" Type="Int32" />
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Poste" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="groepid" Type="Int32" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
