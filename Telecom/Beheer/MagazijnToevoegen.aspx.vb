﻿Imports System.Data.SqlClient
Imports System.Windows.Forms.VisualStyles.VisualStyleElement.Tab
Imports DevExpress.Export

Public Class MagazijnToevoegen
    Inherits System.Web.UI.Page

    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))

        If Page.IsPostBack Then
            If Session("FoutGebruiker") <> "" Then
                ASPxLabel6.Text = Session("FoutGebruiker")
                Session("FoutGebruiker") = ""
            End If
        Else
            ASPxLabel6.Text = ""
        End If




        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
        End If

        If Session("taal") = 1 Then
            LiteralTitel.Text = "Nieuw magazijn toevoegen"
        Else
            LiteralTitel.Text = "Ajouter magasin external"
        End If
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        ASPxTextBoxNaam.Validate()

        Dim voocontext As New VooEntities

        'If voocontext.Gebruikers.Where(Function(x) x.Email = email).Any Then
        '    If Session("taal") = 1 Then
        '        Session("FoutGebruiker") = "Emailadres bestaat al"
        '    Else
        '        Session("FoutGebruiker") = "L'adresse email existe déjà"
        '    End If
        '    ASPxLabel6.Text = Session("FoutGebruiker")
        '    Return
        'End If

        If ASPxTextBoxNaam.Value = "" Then

            If Session("taal") = 1 Then
                Session("FoutGebruiker") = "Geen naam ingevuld"
            Else
                Session("FoutGebruiker") = "Aucun nom entré"
            End If
            ASPxLabel6.Text = Session("FoutGebruiker")
            Return
        End If

        If ASPxComboBoxType.Value = "" Then
            If Session("taal") = 1 Then
                Session("FoutGebruiker") = "Geen type ingevuld"
            Else
                Session("FoutGebruiker") = "Aucun type entré"
            End If
            ASPxLabel6.Text = Session("FoutGebruiker")
            Return
        End If


        Dim profitcenter = ASPxComboBoxProfitcenter.Value
        If String.IsNullOrWhiteSpace(profitcenter) Then
            profitcenter = ""
        End If

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim mag_SQL As String = "INSERT INTO Magazijn (Naam, Hoofd, OpdrachtgeverId, type, profitcenter) VALUES (@Naam, 0, @opdrachtgeverId,@type,@profitcenter);SELECT SCOPE_IDENTITY();"
            Dim cmd = New SqlCommand(mag_SQL, cn)
            Dim par = New SqlParameter("@naam", SqlDbType.NVarChar, 50) With {.Value = ASPxTextBoxNaam.Value}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int, -1) With {.Value = Session("opdrachtgever")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@type", SqlDbType.Int, -1) With {.Value = ASPxComboBoxType.Value}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@profitcenter", SqlDbType.NVarChar) With {.Value = profitcenter}
            cmd.Parameters.Add(par)
            Dim magId As Int32 = cmd.ExecuteScalar()



            Dim smag_SQL As String = "INSERT INTO Stockmagazijn (MateriaalId, MagazijnId, Aantal) SELECT materiaalid,@MagazijnId, 0 from MateriaalOpdrachtgevers where opdrachtgeverId = @opdrachtgeverId"
            cmd = New SqlCommand(smag_SQL, cn)
            par = New SqlParameter("@MagazijnId", SqlDbType.Int, -1) With {.Value = magId}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int, -1) With {.Value = Session("opdrachtgever")}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()

            Dim s_SQL As String = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Magazijn toegevoegd door {0} {1} {2} : {3}", Session("userid"), Session("naam"), Session("voornaam"), ASPxTextBoxNaam.Value)}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()
        End Using


        voocontext.SaveChanges()
        voocontext.Dispose()

        Response.Redirect("~/Beheer/MagazijnToevoegen.aspx", False)



    End Sub

    Protected Sub ASPxMenu1_ItemClick(source As Object, e As DevExpress.Web.MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "ExportExcel" Then
            ASPxGridViewExporter1.FileName = "Export Magazijnen " & Today.Date()
            ASPxGridViewExporter1.WriteXlsxToResponse(New DevExpress.XtraPrinting.XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})

        End If
    End Sub
End Class