﻿Imports System.Data.SqlClient
Imports DevExpress.Export
Imports DevExpress.Web

Public Class BobijnBeheerPersoonlijk
    Inherits System.Web.UI.Page




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Session("opdrachtgeverSession") = Session("opdrachtgever")
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If


        Dim voocontext As New VooEntities
        Dim opId As Integer = Session("opdrachtgever")


    End Sub

    Protected Sub ASPxGridView1_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles ASPxGridView1.RowInserted



    End Sub


    Protected Sub ASPxButton1_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim button As ASPxButton = CType(sender, ASPxButton)
        Dim container As GridViewDataItemTemplateContainer = CType(button.NamingContainer, GridViewDataItemTemplateContainer)


        Dim entities As New VooEntities
        Dim id As Integer = container.KeyValue
        If entities.Bobijn.Find(id).PDF Is Nothing Then
            button.Text = ""
            button.ClientVisible = False
        End If
        If FileExists(container.KeyValue) Then
            button.ClientSideEvents.Click = String.Format("function(s, e) {{ window.location = 'FileDownloadHandler.ashx?id={0}'; }}", container.KeyValue)

        Else
            button.ClientEnabled = False
            'button.ClientSideEvents.Click = string.Format("function(s, e) {{ alert('There is no file for key {0}'); }}", container.KeyValue);
        End If
    End Sub

    Private Function FileExists(ByVal key As Object) As Boolean
        Return True
        ' Return Not String.IsNullOrEmpty(Product.GetData().First(Function(p) p.ProductID.Equals(key)).ImagePath)
    End Function

    Protected Sub ASPxMenu1_ItemClick(source As Object, e As MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "ExportExcel" Then
            ASPxGridViewExporter1.FileName = "Export Magazijnen " & Today.Date()
            ASPxGridViewExporter1.WriteXlsxToResponse(New DevExpress.XtraPrinting.XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})

        End If
    End Sub

    Protected Sub ASPxGridView1_RowUpdated(sender As Object, e As Data.ASPxDataUpdatedEventArgs) Handles ASPxGridView1.RowUpdated
        Dim entities As New VooEntities
        Dim bobid = e.NewValues.Item("id")
        Dim bobijn = entities.Bobijn.Find(bobid)
        If bobijn.AF And bobijn.DatumUitgeboekt Is Nothing Then bobijn.DatumUitgeboekt = Date.Today
        entities.SaveChanges()
    End Sub

    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim changed As Boolean = False
        For Each value In e.NewValues
            If Not IsDBNull(value.value) AndAlso Not String.IsNullOrWhiteSpace(value.value) Then
                If e.OldValues(value.Key) <> e.NewValues(value.Key) Then
                    changed = True
                End If
            End If
        Next
        e.Cancel = Not changed
    End Sub
End Class