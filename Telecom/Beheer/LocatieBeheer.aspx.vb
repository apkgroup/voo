﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class LocatieBeheer
    Inherits System.Web.UI.Page

    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then

            Literal2.Text = "Locatiebeheer"

        Else


        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        Session("opdrachtgeverSession") = Session("opdrachtgever")

        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer
        If Not Page.IsPostBack And String.IsNullOrEmpty(Session("MagazijnId")) Then



            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using

            If Not Session("isingelogd") Then
                Session("url") = Request.Url.AbsoluteUri
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
            If Not Session("isadmin") And Not Session("level") = 10 Then
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
        End If

        vertaal(Session("taal"))


    End Sub

    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender

        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub

    Protected Sub ASPxGridView1_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles ASPxGridView1.RowInserted



    End Sub
    Protected Sub ASPxGridView1_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBinding
        DoSelect(SqlDataSourceLocatie.ConnectionString)
    End Sub

    Protected Sub ASPxGridView1_DetailRowGetButtonVisibility(sender As Object, e As ASPxGridViewDetailRowButtonEventArgs) Handles ASPxGridView1.DetailRowGetButtonVisibility
        If Not (CType(Session("SelectResult"), ArrayList)).Contains(e.KeyValue) Then
            e.ButtonState = GridViewDetailRowButtonState.Hidden
        End If
    End Sub
    Protected Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [LocatieId] from [Serienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("LocatieId"))
        Next row
        Session("SelectResult") = result
    End Sub
    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("locatieId") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub
    Protected Sub ASPxButtonToevoegen0_Click(sender As Object, e As EventArgs) Handles ASPxButtonToevoegen0.Click
        ASPxLabelFout.Text = ""
        Dim fields As String() = {"opdrachtgeverId"}
        Dim voocontext As New VooEntities
        Dim magId As Integer = Session("MagazijnId")
        If voocontext.Locatie.Where(Function(x) x.magazijnid = magId And x.naam = ASPxTextBoxNaam.Text).Any Then
            ASPxLabelFout.Text = "Locatienaam moet uniek zijn binnen 1 magazijn"
            Return
        End If

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim i_sql As String = "INSERT INTO Locatie ([magazijnid], [capaciteit], [naam]) VALUES (@magazijnid, @capaciteit, @naam)"
            Dim cmd As New SqlCommand(i_sql, cn)
            Dim par As New SqlParameter("@magazijnid", SqlDbType.NVarChar) With {.Value = Session("MagazijnId")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@capaciteit", SqlDbType.NVarChar) With {.Value = ""}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@naam", SqlDbType.NVarChar) With {.Value = ASPxTextBoxNaam.Text}
            cmd.Parameters.Add(par)
            Dim locId = cmd.ExecuteNonQuery
            Dim s_SQL = "INSERT INTO LOG (Gebruiker, tijdstip, actie, type) values (@userId, GetDate(), @actie, 'stock')"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = "Locatie " & ASPxTextBoxNaam.Text & " toegevoegd. "}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()
            cn.Close()
        End Using
        Response.Redirect("~/Beheer/Locatiebeheer.aspx", False)
    End Sub
End Class