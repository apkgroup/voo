﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class BobijnBeheer
    
    '''<summary>
    '''Literal2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal2 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''ASPxButtonToevoegen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxButtonToevoegen As Global.DevExpress.Web.ASPxButton
    
    '''<summary>
    '''ASPxLabelFout control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabelFout As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''pnlForm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlForm As Global.DevExpress.Web.ASPxPanel
    
    '''<summary>
    '''ASPxComboBoxMagazijn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxMagazijn As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''ASPxComboBoxMagazijn0 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxMagazijn0 As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''ASPxLabelJMS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabelJMS As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxTextboxJMS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextboxJMS As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxLabelSoort control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabelSoort As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxComboBoxBobijnSoort control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxBobijnSoort As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''SqlDataSource2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSource2 As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''ASPxComboBoxArtikel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxArtikel1 As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''SqlDataSourceArtikelen0 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceArtikelen0 As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''ASPxTextBoxBobijnnummer1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxBobijnnummer1 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxSpinEditHoeveelheid1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxSpinEditHoeveelheid1 As Global.DevExpress.Web.ASPxSpinEdit
    
    '''<summary>
    '''ASPxCheckBoxGemaakt1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxCheckBoxGemaakt1 As Global.DevExpress.Web.ASPxCheckBox
    
    '''<summary>
    '''ASPxLabel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel2 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxTextBoxPaklijst1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxPaklijst1 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxTextBoxPlaatsingsstaat1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxPlaatsingsstaat1 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxComboBoxArtikel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxArtikel2 As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''ASPxTextBoxBobijnNummer2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxBobijnNummer2 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxSpinEditHoeveelheid2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxSpinEditHoeveelheid2 As Global.DevExpress.Web.ASPxSpinEdit
    
    '''<summary>
    '''ASPxCheckBoxGemaakt2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxCheckBoxGemaakt2 As Global.DevExpress.Web.ASPxCheckBox
    
    '''<summary>
    '''ASPxLabel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel1 As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxTextBoxPaklijst2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxPaklijst2 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxTextBoxPlaatsingsstaat2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxPlaatsingsstaat2 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxComboBoxArtikel3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxArtikel3 As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''ASPxTextBoxBobijnNummer3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxBobijnNummer3 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxSpinEditHoeveelheid3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxSpinEditHoeveelheid3 As Global.DevExpress.Web.ASPxSpinEdit
    
    '''<summary>
    '''ASPxCheckBoxGemaakt3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxCheckBoxGemaakt3 As Global.DevExpress.Web.ASPxCheckBox
    
    '''<summary>
    '''ASPxLabelPaklijst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabelPaklijst As Global.DevExpress.Web.ASPxLabel
    
    '''<summary>
    '''ASPxTextBoxPaklijst3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxPaklijst3 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxTextBoxPlaatsingsstaat3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxPlaatsingsstaat3 As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxButtonToevoegen0 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxButtonToevoegen0 As Global.DevExpress.Web.ASPxButton
    
    '''<summary>
    '''ASPxMenu1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxMenu1 As Global.DevExpress.Web.ASPxMenu
    
    '''<summary>
    '''popupcontrolbox control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents popupcontrolbox As Global.DevExpress.Web.ASPxPopupControl
    
    '''<summary>
    '''ASPxTextBoxBobijn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxTextBoxBobijn As Global.DevExpress.Web.ASPxTextBox
    
    '''<summary>
    '''ASPxComboBoxKabelTypeOud control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxKabelTypeOud As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''ASPxComboBoxKabelTypeNieuw control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxKabelTypeNieuw As Global.DevExpress.Web.ASPxComboBox
    
    '''<summary>
    '''SqlDataSource1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSource1 As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''ASPxHiddenFieldLijnId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxHiddenFieldLijnId As Global.DevExpress.Web.ASPxHiddenField
    
    '''<summary>
    '''ASPxButton5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxButton5 As Global.DevExpress.Web.ASPxButton
    
    '''<summary>
    '''ASPxGridViewExporter1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxGridViewExporter1 As Global.DevExpress.Web.ASPxGridViewExporter
    
    '''<summary>
    '''ASPxGridView1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxGridView1 As Global.DevExpress.Web.ASPxGridView
    
    '''<summary>
    '''SqlDataSourceMagazijnen0 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceMagazijnen0 As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''SqlDataSourceMagazijnen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceMagazijnen As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''SqlDataSourceMagazijnen1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceMagazijnen1 As Global.System.Web.UI.WebControls.SqlDataSource
End Class
