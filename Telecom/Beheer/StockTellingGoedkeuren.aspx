﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="StockTellingGoedkeuren.aspx.vb" Inherits="Telecom.StockTellingGoedkeuren" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu>
        <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Export geselecteerde">
        </dx:ASPxButton>
    </h2> 
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceBestellingen" KeyFieldName="id">
        <SettingsPager PageSize="20">
        </SettingsPager>
        <Settings ShowHeaderFilterButton="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="naam" ReadOnly="True" VisibleIndex="1" ShowInCustomizationForm="True" Caption="Magazijn">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="beschrijving" VisibleIndex="2" Caption="Status" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="Datum" VisibleIndex="3" ShowInCustomizationForm="True" Caption="Datum gestart">
                <Settings AllowHeaderFilter="False" ShowInFilterControl="False" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataHyperLinkColumn FieldName="id" VisibleIndex="5">
            <PropertiesHyperLinkEdit NavigateUrlFormatString="../ViewStockTelling.aspx?id={0}" Text="details" TextFormatString="" >
            </PropertiesHyperLinkEdit>
                <Settings AllowHeaderFilter="False" ShowInFilterControl="False" />
            <EditFormSettings Visible="False" />
        </dx:GridViewDataHyperLinkColumn>
            <dx:GridViewDataTextColumn FieldName="groep" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn Caption="Datum doorgestuurd" FieldName="datumdoorgestuurd" VisibleIndex="4">
                <PropertiesDateEdit DisplayFormatString="">
                </PropertiesDateEdit>
            </dx:GridViewDataDateColumn>
        </Columns>
    </dx:ASPxGridView>
    <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" ClientVisible="False" DataSourceID="SqlDataSourceMateriauxADMIN" KeyFieldName="id">
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="naam" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="HeeftSerienummer" VisibleIndex="4">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataDateColumn FieldName="datumdoorgestuurd" VisibleIndex="5">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="Hoeveelheid" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="hoeveelheidSnapshot" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <br />

    <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT sl.[id]
	  ,m.naam
	  ,b.Article
	  ,b.Description
	  ,b.HeeftSerienummer
	  ,s.datumdoorgestuurd
      ,[hoeveelheid] as Hoeveelheid
	  ,hoeveelheidSnapshot
  FROM [Voo].[dbo].[Stocktellinglijn] sl
  inner join StockTelling s on sl.stocktellingId = s.id
  inner join Basismateriaal b on sl.materiaalId = b.id
  inner join magazijn m on s.magazijnId = m.id
    where s.status = 2 and hoeveelheid &lt;&gt; hoeveelheidSnapshot  and m.opdrachtgeverId = @opdrachtgever
	and stocktellingId in (select max(id) from Stocktelling where status=2 group by magazijnId)
" ID="SqlDataSourceMateriauxADMIN" UpdateCommand="UPDATE [Stocktellinglijn] SET  Hoeveelheid = @Hoeveelheid WHERE [id] = @id">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Hoeveelheid" />
            <asp:Parameter Name="id" />
        </UpdateParameters>
    </asp:SqlDataSource>
        
    <asp:SqlDataSource ID="SqlDataSourceBestellingen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="select
m.naam, 
CASE 
WHEN s.[status] = 1 THEN 'In uitvoering'
WHEN s.[status] = 2 THEN 'Uitgevoerd'
WHEN s.[status] = 3 THEN 'Goedgekeurd'
END as beschrijving,[datumdoorgestuurd],
[Datum], s.[id],  
(select top 1 g.Naam from GebruikersgroepenLeden l inner join Gebruikersgroepen g on g.id = l.Groep where l.Gebruiker = g.id) as groep 
FROM Stocktelling s
INNER JOIN 
Magazijn m on s.magazijnid = m.id
INNER JOIN
Gebruikers g on m.id = g.MagazijnId
WHERE m.opdrachtgeverId = @opdrachtgever and (g.Actief= 1 or g.Actief is null)
ORDER BY [Datum] DESC">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="opdrachtgever" SessionField="opdrachtgever" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
    </dx:ASPxGridViewExporter>
      <br />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server" GridViewID="ASPxGridView2">
    </dx:ASPxGridViewExporter>
      </asp:Content>
