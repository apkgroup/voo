﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="BobijnBeheer.aspx.vb" Inherits="Telecom.BobijnBeheer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

        function OnInit(s, e) {  
    setTimeout(ChangeHint, 50);  
    s.ActiveItemChanged.AddHandler(function (s, args) { setTimeout(ChangeHint, 50); })  
}  

function ChangeHint() {  
    var src = imageZoom.GetMainElement().getElementsByClassName("dxiz-clipPanel")[0].getElementsByTagName("img")[0].src;  
    var hintElem = imageZoom.GetMainElement().getElementsByClassName("dxiz-hint")[0];  
    if (src.indexOf("empty") >= 0) {  
        if (!hintElem.classList.contains("MyClass"))  
            hintElem.classList.add("MyClass");  
    }  
    else {  
        if (hintElem.classList.contains("MyClass"))  
            hintElem.classList.remove("MyClass");  
    }  
        }

          function showWisselen(bobijnId, bobijnNummer,kabelType) {
       
              ASPxHiddenFieldLijnId.Set('bobijnId', bobijnId);
              console.log(bobijnNummer);
            ASPxTextBoxBobijn.SetValue(bobijnNummer);
            ASPxComboBoxKabelTypeOud.SetValue(kabelType);
            popupcontrolbox.Show();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>

                <dx:ASPxButton ID="ASPxButtonToevoegen" runat="server" AutoPostBack="false" Width="50px" Text="Nieuwe bobijn toevoegen" CssClass="minimargin">
                     <ClientSideEvents Click="function(s, e) { pnlForm.SetVisible(true); }" />  
                </dx:ASPxButton>

    <br />
    <dx:ASPxLabel ID="ASPxLabelFout" runat="server" Font-Bold="True" ForeColor="Red">
    </dx:ASPxLabel>

    <dx:ASPxPanel ID="pnlForm" runat="server"  ClientVisible="false" ClientInstanceName="pnlForm" CssClass="minimargin" Paddings-PaddingLeft="5px" Paddings-PaddingBottom="5px">  
        <Paddings PaddingBottom="5px" />
    <PanelCollection>  
        <dx:PanelContent>  
            &nbsp;<table style="width:100%;">
                <tr>
                              
                    <td style="text-align: right">Magazijn:</td>
                    <td>
                        <dx:ASPxComboBox runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" ID="ASPxComboBoxMagazijn"></dx:ASPxComboBox>

               
                        <br />

               
                    </td>
                    <td style="text-align: right">Voorziene locatie</td>
                    <td>
                        <dx:ASPxComboBox ID="ASPxComboBoxMagazijn0" runat="server" DataSourceID="SqlDataSourceMagazijnen1" TextField="Naam" ValueField="id">
                        </dx:ASPxComboBox>

                    </td>
                    <td style="text-align: right">
                        <dx:ASPxLabel ID="ASPxLabelJMS" runat="server" Text="JMS Nummer:">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextboxJMS" runat="server" Width="170px">
                        </dx:ASPxTextBox>

                    </td>
                    <td style="text-align: right">
                        <dx:ASPxLabel ID="ASPxLabelSoort" runat="server" Text="Bobijnsoort">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="ASPxComboBoxBobijnSoort" runat="server" DataSourceID="SqlDataSource2" TextField="beschrijving" ValueField="id">
                        </dx:ASPxComboBox>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id]
      ,[beschrijving]
  FROM [Voo].[dbo].[BobijnSoort]"></asp:SqlDataSource>

                         
                    </td>
                   
                </tr>

                <tr>
                              
                    <td style="text-align: right">Artikel:</td>
                    <td>
                        <dx:ASPxComboBox ID="ASPxComboBoxArtikel1" runat="server" ValueType="System.String" DataSourceID="SqlDataSourceArtikelen0" TextField="naam" ValueField="id"></dx:ASPxComboBox>
               
                        <asp:SqlDataSource ID="SqlDataSourceArtikelen0" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT distinct article, description, CONCAT(article, ' | ', description) as naam, b.id FROM [Basismateriaal] b
INNER JOIN MateriaalOpdrachtgevers o ON b.id =o.materiaalId
WHERE o.opdrachtgeverId = @OpdrachtgeverId and (o.actief = 1 or o.actief is null) and b.bobijnArtikel =1">
                            <SelectParameters>
                                <asp:SessionParameter DefaultValue="1" Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
               
                    </td>
                    <td style="text-align: right">Bobijnnummer:</td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextBoxBobijnnummer1" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                    <td style="text-align: right">Hoeveelheid (m):</td>
                    <td>
                        <dx:ASPxSpinEdit ID="ASPxSpinEditHoeveelheid1" runat="server" Number="0">
                        </dx:ASPxSpinEdit>
                    </td>
                    <td style="text-align: right">Plaatsingsstaat gemaakt:</td>
                    <td>
                        <dx:ASPxCheckBox ID="ASPxCheckBoxGemaakt1" runat="server"></dx:ASPxCheckBox>
                         
                    </td>
                     <td style="text-align: right">
                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Paklijstnummer">
                        </dx:ASPxLabel>:</td>
                    <td>
                          <dx:ASPxTextBox ID="ASPxTextBoxPaklijst1" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                   

                    <td style="text-align: right">Plaatsingsstaatnummer:</td>
                    <td>
                         <dx:ASPxTextBox ID="ASPxTextBoxPlaatsingsstaat1" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                   
                    </td>
                   
                </tr>

                <!-- 2 -->
                <tr>
                    <td style="text-align: right">Artikel:</td>
                    <td>
                        <dx:ASPxComboBox ID="ASPxComboBoxArtikel2" runat="server" ValueType="System.String" DataSourceID="SqlDataSourceArtikelen0" TextField="naam" ValueField="id"></dx:ASPxComboBox>
               
                    </td>
                    <td style="text-align: right">Bobijnnummer:</td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextBoxBobijnNummer2" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                    <td style="text-align: right">Hoeveelheid (m):</td>
                    <td>
                        <dx:ASPxSpinEdit ID="ASPxSpinEditHoeveelheid2" runat="server" Number="0">
                        </dx:ASPxSpinEdit>
                    </td>
                    <td style="text-align: right">Plaatsingsstaat gemaakt:</td>
                    <td>
                        <dx:ASPxCheckBox ID="ASPxCheckBoxGemaakt2" runat="server"></dx:ASPxCheckBox>
                         
                    </td>

                     <td style="text-align: right">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Paklijstnummer">
                        </dx:ASPxLabel>:</td>
                    <td>
                          <dx:ASPxTextBox ID="ASPxTextBoxPaklijst2" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                    <td style="text-align: right">Plaatsingsstaatnummer:</td>
                    <td>
                         <dx:ASPxTextBox ID="ASPxTextBoxPlaatsingsstaat2" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                   
                    </td>

                   
                </tr>
                <!-- 3 -->
                <tr>
                    <td style="text-align: right">Artikel:</td>
                    <td>
                        <dx:ASPxComboBox ID="ASPxComboBoxArtikel3" runat="server" ValueType="System.String" DataSourceID="SqlDataSourceArtikelen0" TextField="naam" ValueField="id"></dx:ASPxComboBox>
               
                    </td>
                    <td style="text-align: right">Bobijnnummer:</td>
                    <td>
                        <dx:ASPxTextBox ID="ASPxTextBoxBobijnNummer3" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                    <td style="text-align: right">Hoeveelheid (m):</td>
                    <td>
                        <dx:ASPxSpinEdit ID="ASPxSpinEditHoeveelheid3" runat="server" Number="0">
                        </dx:ASPxSpinEdit>
                    </td>
                      <td style="text-align: right">Plaatsingsstaat gemaakt:</td>
                    <td>
                        <dx:ASPxCheckBox ID="ASPxCheckBoxGemaakt3" runat="server"></dx:ASPxCheckBox>
                         
                    </td>
                    
                     <td style="text-align: right">
                        <dx:ASPxLabel ID="ASPxLabelPaklijst" runat="server" Text="Paklijstnummer">
                        </dx:ASPxLabel>:</td>
                    <td>
                          <dx:ASPxTextBox ID="ASPxTextBoxPaklijst3" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                    <td style="text-align: right">Plaatsingsstaatnummer:</td>
                    <td>
                         <dx:ASPxTextBox ID="ASPxTextBoxPlaatsingsstaat3" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                   
                    </td>
                   
                </tr>


               
                 
                  
                  
                


                  
                  
                
          
            </table>
            <h2>&nbsp;</h2>
            <dx:ASPxButton ID="ASPxButtonToevoegen0" runat="server" Width="50px">
                <Image IconID="actions_add_32x32office2013">
                </Image>
            </dx:ASPxButton>
            <br />
        </dx:PanelContent>  
    </PanelCollection>  
        <Border BorderColor="Silver" BorderWidth="1px" />
</dx:ASPxPanel>  
    <br />
     <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu>
        <dx:ASPxPopupControl ID="popupcontrolbox" runat="server" ClientInstanceName="popupcontrolbox" HeaderText="Kabeltype wijzigen" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        
        <ContentCollection>
<dx:PopupControlContentControl runat="server">
 
    <table style="width:100%;">
        <tr>
            <td>Bobijn</td>
            <td>
                <dx:ASPxTextBox ID="ASPxTextBoxBobijn" ReadOnly="true" runat="server" Width="170px" ClientInstanceName="ASPxTextBoxBobijn">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td>Oud Kabeltype</td>
            <td>
                <dx:ASPxComboBox ID="ASPxComboBoxKabelTypeOud" ReadOnly="true" Width="170px" ClientInstanceName="ASPxComboBoxKabelTypeOud" runat="server">
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td>Nieuw Kabeltype</td>
            <td>
                <dx:ASPxComboBox ID="ASPxComboBoxKabelTypeNieuw" Width="170px" ClientInstanceName="ASPxComboBoxKabelTypeNieuw" runat="server" DataSourceID="SqlDataSource1" TextField="volledig" ValueField="id">
                </dx:ASPxComboBox>
            </td>
        </tr>
    </table>
 
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="select b.id,article,description, concat(article,' ', description) as volledig from Basismateriaal b
inner join MateriaalOpdrachtgevers mo on mo.materiaalid =b.id 
where mo.opdrachtgeverid = @opdrachtgeverid
and bobijnartikel = 1
order by article">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverid" SessionField="opdrachtgever" />
        </SelectParameters>
    </asp:SqlDataSource>
 
    <dx:ASPxHiddenField ID="ASPxHiddenFieldLijnId" runat="server" ClientInstanceName="ASPxHiddenFieldLijnId">
    </dx:ASPxHiddenField>
    <dx:ASPxButton ID="ASPxButton5" runat="server" Text="Inlezen">
    </dx:ASPxButton>
    <br />
            </dx:PopupControlContentControl>
</ContentCollection>
        
    </dx:ASPxPopupControl>
     <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
     </dx:ASPxGridViewExporter>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" KeyFieldName="id" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMagazijnen0" EnableRowsCache="False">
        <SettingsPager PageSize="25">
        </SettingsPager>
        <Settings ShowHeaderFilterButton="True" />
        <SettingsBehavior ConfirmDelete="True" AutoFilterRowInputDelay="5000" FilterRowMode="OnClick" />
        <SettingsSearchPanel Visible="True" Delay="2500" />
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="bobijnnummer" VisibleIndex="1" ReadOnly="True" Caption="Nummer">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="aantal" VisibleIndex="2" Caption="Resterend">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="AF" VisibleIndex="3">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="Naam" VisibleIndex="4" ReadOnly="True" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="8" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="9" ReadOnly="True">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataHyperLinkColumn Caption="Plaatsingsstaat" FieldName="id" ShowInCustomizationForm="True" VisibleIndex="10" ReadOnly="True">
                <PropertiesHyperLinkEdit NavigateUrlFormatString="../Beheer/BobijnMeetstaat.aspx?id={0}" Text="Plaatsingsstaat">
                </PropertiesHyperLinkEdit>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataHyperLinkColumn>
           
            <dx:GridViewDataComboBoxColumn Caption="Magazijn" FieldName="magazijnid" VisibleIndex="5">
                <PropertiesComboBox DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
           
            <dx:GridViewDataTextColumn Caption="Paklijst nr" FieldName="paklijstnummer" VisibleIndex="11">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Plaatsingsstaat nr" FieldName="plaatsingsstaatnr" VisibleIndex="12">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn Caption="Plaatsingsstaat" FieldName="plaatsingsstaatgemaakt" VisibleIndex="14">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataColumn Caption="Download" VisibleIndex="7">  
                    <DataItemTemplate>  
                        <dx:ASPxButton ID="ASPxButton1" runat="server" OnInit="ASPxButton1_Init"  
                            AutoPostBack="False" RenderMode="Link" Text="Download">  
                        </dx:ASPxButton>  
                    </DataItemTemplate>  
                </dx:GridViewDataColumn>
            <dx:GridViewDataComboBoxColumn Caption="Voorziene locatie" FieldName="voorzieneLocatie" VisibleIndex="6">
                <PropertiesComboBox DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn Caption="JMS nr" FieldName="JMSNummer" VisibleIndex="13">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Bobijnsoort" FieldName="Bobijnsoort" VisibleIndex="16">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Kabeltype wijzigen" FieldName="id" VisibleIndex="15">
      <DataItemTemplate >  
             <a class="tooltip" id="clickElement" href="javascript:void(0);" onclick="showWisselen(<%# Eval("id")%>,'<%# Eval("bobijnnummer")%>','<%# Eval("Article")%>')">Kabeltype Wisselen</a>
          
     </DataItemTemplate>  

<CellStyle HorizontalAlign="Center"></CellStyle>

            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Dagen aanwezig" FieldName="dagenInMagazijn" VisibleIndex="19">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="DatumIngelezen" VisibleIndex="17" Caption="Ingelezen">
                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy">
                </PropertiesDateEdit>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataDateColumn FieldName="DatumUitgeboekt" VisibleIndex="18" Caption="Uitgeboekt">
                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy">
                </PropertiesDateEdit>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataDateColumn>
        </Columns>
    </dx:ASPxGridView>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen0" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT b.id, [bobijnnummer]
      ,b.[aantal]
      ,b.[AF]
	  ,m.Naam
,m.id as magazijnid
	  ,ba.Article
	  ,ba.[Description]
      ,[paklijstnummer]
      ,[plaatsingsstaatnr]
      ,[PDF]

      ,[plaatsingsstaatgemaakt]
,DatumIngelezen
,DatumUitgeboekt,
CASE
WHEN DatumUitgeboekt is null THEN DATEDIFF(day,DatumIngelezen, GETDATE())
ELSE DATEDIFF(day,DatumIngelezen, DatumUitgeboekt)
END as dagenInMagazijn
,voorzieneLocatie
,JMSNummer
        ,bs.beschrijving as 'Bobijnsoort'
  FROM [Voo].[dbo].[Bobijn] b
  inner join [Voo].[dbo].StockMagazijn sm on b.stockmagazijnId = sm.id
   inner join [Voo].[dbo].Magazijn m on sm.MagazijnId = m.id
    inner join [Voo].[dbo].Basismateriaal ba on ba.id = sm.MateriaalId
        left join [Voo].[dbo].BobijnSoort bs on bs.id = b.soortBobijn
where m.opdrachtgeverId = @opdrachtgeverId" UpdateCommand="UPDATE voo.dbo.StockMagazijn set Aantal = aantal - (select aantal from voo.dbo.Bobijn where id = @id) where id = (select stockmagazijnId from voo.dbo.Bobijn where id = @id);
 INSERT INTO voo.dbo.stockbeweging (stockmagazijnId, datum, beweging, gebruiker, opmerking) values((select stockmagazijnId from voo.dbo.Bobijn where id = @id),GETDATE(),-(select aantal from voo.dbo.Bobijn where id = @id),@gebruiker,concat('bobijn verplaatsen (van) ',(select bobijnnummer from voo.dbo.Bobijn where id = @id)));


UPDATE [Voo].[dbo].[Bobijn] set stockmagazijnid =
 (select id from [Voo].[dbo].stockmagazijn sm where sm.MagazijnId= @magazijnid and sm.MateriaalId=(select materiaalid from [Voo].[dbo].stockmagazijn where id =(select stockmagazijnId from voo.dbo.Bobijn where id = @id))),[paklijstnummer]=@paklijstnummer
      ,[plaatsingsstaatnr]=@plaatsingsstaatnr
      ,[PDF]=CONVERT(VARBINARY(25), @PDF, 1)
      ,[plaatsingsstaatgemaakt]=@plaatsingsstaatgemaakt
 ,[JMSNummer]=@JMSNummer
,[aantal]=@aantal
,[voorzieneLocatie]=@voorzieneLocatie
,[AF]=@af
 where id = @id;
 

 UPDATE voo.dbo.StockMagazijn set Aantal = aantal + (select aantal from voo.dbo.Bobijn where id = @id) where id = (select stockmagazijnId from voo.dbo.Bobijn where id = @id);
  INSERT INTO voo.dbo.stockbeweging (stockmagazijnId, datum, beweging, gebruiker, opmerking) values((select stockmagazijnId from voo.dbo.Bobijn where id = @id),GETDATE(),(select aantal from voo.dbo.Bobijn where id = @id),@gebruiker,concat('bobijn verplaatsen (naar) ',(select bobijnnummer from voo.dbo.Bobijn where id = @id)))">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="id" />
            <asp:SessionParameter Name="gebruiker" SessionField="userid"  />
            <asp:Parameter Name="magazijnid" />
            <asp:Parameter Name="paklijstnummer" />
            <asp:Parameter Name="plaatsingsstaatnr" />
            <asp:Parameter Name="PDF" />
            <asp:Parameter Name="plaatsingsstaatgemaakt" />
            <asp:Parameter Name="JMSNummer" />
            <asp:Parameter Name="aantal" />
            <asp:Parameter Name="voorzieneLocatie" />
            <asp:Parameter Name="af" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
                        <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId )" ID="SqlDataSourceMagazijnen"><SelectParameters>
<asp:SessionParameter SessionField="opdrachtgever" Name="OpdrachtgeverId" Type="Int32"></asp:SessionParameter>
</SelectParameters>
</asp:SqlDataSource>

                   
                    <br />
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId and (g.Actief = 1 or hoofd=1 or type is not null) )">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
                   
                    </asp:Content>