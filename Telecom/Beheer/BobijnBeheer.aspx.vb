﻿Imports System.Data.SqlClient
Imports DevExpress.Export
Imports DevExpress.Web

Public Class BobijnBeheer
    Inherits System.Web.UI.Page




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Session("opdrachtgeverSession") = Session("opdrachtgever")
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        Dim voocontext As New VooEntities
        Dim opId As Integer = Session("opdrachtgever")
        If voocontext.Opdrachtgever.Find(opId).naam <> "Proximus" And voocontext.Opdrachtgever.Find(opId).naam <> "Synductis Limburg" And voocontext.Opdrachtgever.Find(opId).naam <> "Synductis Kempen" Then
            ASPxLabelJMS.ClientVisible = False
            ASPxTextboxJMS.ClientVisible = False
        Else
            ASPxLabelPaklijst.Text = "MatKap"
            ASPxGridView1.Columns("paklijstnummer").Caption = "MatKap"
            ASPxGridView1.Columns("JMSNummer").Visible = True
        End If

        If voocontext.Opdrachtgever.Find(opId).naam <> "Synductis Limburg" And voocontext.Opdrachtgever.Find(opId).naam <> "Synductis Kempen" Then
            ASPxComboBoxBobijnSoort.ClientVisible = False
            ASPxLabelSoort.ClientVisible = False
        Else
            ASPxGridView1.Columns("Bobijnsoort").Visible = True
        End If


    End Sub

    Protected Sub ASPxGridView1_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles ASPxGridView1.RowInserted



    End Sub



    Protected Sub ASPxButtonToevoegen0_Click(sender As Object, e As EventArgs) Handles ASPxButtonToevoegen0.Click
        If Not String.IsNullOrWhiteSpace(ASPxTextBoxBobijnnummer1.Value) And Not String.IsNullOrWhiteSpace(ASPxSpinEditHoeveelheid1.Value) Then
            If Not maakBobijn(ASPxTextBoxBobijnnummer1.Value, ASPxComboBoxArtikel1.Value, ASPxSpinEditHoeveelheid1.Value, ASPxCheckBoxGemaakt1.Checked, ASPxTextBoxPaklijst1.Value, ASPxTextBoxPlaatsingsstaat1.Value) Then
                Return
            End If
        End If
        If Not String.IsNullOrWhiteSpace(ASPxTextBoxBobijnNummer2.Value) And Not String.IsNullOrWhiteSpace(ASPxSpinEditHoeveelheid2.Value) Then
            If Not maakBobijn(ASPxTextBoxBobijnNummer2.Value, ASPxComboBoxArtikel2.Value, ASPxSpinEditHoeveelheid2.Value, ASPxCheckBoxGemaakt2.Checked, ASPxTextBoxPaklijst2.Value, ASPxTextBoxPlaatsingsstaat2.Value) Then
                Return
            End If
        End If
        If Not String.IsNullOrWhiteSpace(ASPxTextBoxBobijnNummer3.Value) And Not String.IsNullOrWhiteSpace(ASPxSpinEditHoeveelheid3.Value) Then
            If Not maakBobijn(ASPxTextBoxBobijnNummer3.Value, ASPxComboBoxArtikel3.Value, ASPxSpinEditHoeveelheid3.Value, ASPxCheckBoxGemaakt3.Checked, ASPxTextBoxPaklijst3.Value, ASPxTextBoxPlaatsingsstaat3.Value) Then
                Return
            End If
        End If

    End Sub
    Public Function maakBobijn(Bobijnnummer As String, Artikel As Int32, hoeveelheid As Decimal, plaatsinggemaakt As Boolean, paklijst As String, plaatsingsstaat As String) As Boolean
        Dim voocontext As New VooEntities

        Dim nr As String = Bobijnnummer
        If voocontext.Bobijn.Where(Function(x) x.bobijnnummer = nr And x.AF = False).Any Then
            ASPxLabelFout.Text = "Er bestaat al een actieve bobijn met deze nummer"
            Return False
        End If
        Dim bobijn As New Bobijn
        If String.IsNullOrEmpty(ASPxComboBoxMagazijn.Value) Or String.IsNullOrEmpty(Artikel) Then
            Return False
        End If

        bobijn.aantal = hoeveelheid
        bobijn.bobijnnummer = Bobijnnummer
        bobijn.AF = False
        Dim voorzienelocId As Integer = ASPxComboBoxMagazijn0.Value
        bobijn.voorzieneLocatie = voorzienelocId

        bobijn.plaatsingsstaatgemaakt = plaatsinggemaakt
        bobijn.paklijstnummer = paklijst
        bobijn.plaatsingsstaatnr = plaatsingsstaat
        bobijn.DatumIngelezen = Date.Today
        If ASPxComboBoxBobijnSoort.Value IsNot Nothing AndAlso ASPxComboBoxBobijnSoort.Value <> "" Then
            bobijn.soortBobijn = Convert.ToInt32(ASPxComboBoxBobijnSoort.Value)
        End If


        If ASPxTextboxJMS.Value IsNot Nothing Then
            bobijn.JMSNummer = ASPxTextboxJMS.Value
        End If




        Dim magId As Integer = ASPxComboBoxMagazijn.Value
        Dim matId As Integer = Artikel
        Dim stockmag = voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magId And x.MateriaalId = matId).FirstOrDefault
        bobijn.stockmagazijnId = stockmag.id
        stockmag.Aantal = stockmag.Aantal + hoeveelheid

        voocontext.Bobijn.Add(bobijn)

        Dim sb As New Stockbeweging
        sb.gebruiker = Session("userId")
        sb.datum = DateTime.Now
        sb.beweging = hoeveelheid
        sb.opmerking = "Inlezen nieuwe bobijn met nummer " & bobijn.bobijnnummer
        sb.stockmagazijnId = stockmag.id

        voocontext.Stockbeweging.Add(sb)

        voocontext.SaveChanges()

        ASPxGridView1.DataBind()
        Return True
    End Function

    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender

        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub


    Protected Sub ASPxButton1_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim button As ASPxButton = CType(sender, ASPxButton)
        Dim container As GridViewDataItemTemplateContainer = CType(button.NamingContainer, GridViewDataItemTemplateContainer)


        Dim entities As New VooEntities
        Dim id As Integer = container.KeyValue
        If entities.Bobijn.Find(id).PDF Is Nothing Then
            button.Text = ""
            button.ClientVisible = False
        End If
        If FileExists(container.KeyValue) Then
            button.ClientSideEvents.Click = String.Format("function(s, e) {{ window.location = 'FileDownloadHandler.ashx?id={0}'; }}", container.KeyValue)

        Else
            button.ClientEnabled = False
            'button.ClientSideEvents.Click = string.Format("function(s, e) {{ alert('There is no file for key {0}'); }}", container.KeyValue);
        End If
    End Sub

    Private Function FileExists(ByVal key As Object) As Boolean
        Return True
        ' Return Not String.IsNullOrEmpty(Product.GetData().First(Function(p) p.ProductID.Equals(key)).ImagePath)
    End Function

    Protected Sub ASPxMenu1_ItemClick(source As Object, e As MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "ExportExcel" Then
            ASPxGridViewExporter1.FileName = "Export Magazijnen " & Today.Date()
            ASPxGridViewExporter1.WriteXlsxToResponse(New DevExpress.XtraPrinting.XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})

        End If
    End Sub

    Protected Sub ASPxButton5_Click(sender As Object, e As EventArgs) Handles ASPxButton5.Click
        Dim entities As New VooEntities
        Dim bobid As Integer = ASPxHiddenFieldLijnId.FirstOrDefault.Value
        Dim bobijn = entities.Bobijn.Find(bobid)
        Dim nieuwmat As Integer = ASPxComboBoxKabelTypeNieuw.Value
        Dim stockmagNieuw = entities.StockMagazijn.Where(Function(x) x.MateriaalId = nieuwmat And x.MagazijnId = bobijn.StockMagazijn.MagazijnId).FirstOrDefault
        bobijn.StockMagazijn.Aantal = bobijn.StockMagazijn.Aantal - bobijn.aantal

        Dim sb As New Stockbeweging
        sb.stockmagazijnId = bobijn.StockMagazijn.id
        sb.opmerking = "Kabeltype wijziging bobijn (van)"
        sb.datum = Today
        sb.beweging = -bobijn.aantal
        sb.gebruiker = HuidigPersoonId
        entities.Stockbeweging.Add(sb)

        bobijn.StockMagazijn = stockmagNieuw

        stockmagNieuw.Aantal = stockmagNieuw.Aantal + bobijn.aantal

        Dim sb2 As New Stockbeweging
        sb2.stockmagazijnId = bobijn.StockMagazijn.id
        sb2.opmerking = "Kabeltype wijziging bobijn (naar)"
        sb2.datum = Today
        sb2.beweging = -bobijn.aantal
        sb2.gebruiker = HuidigPersoonId
        entities.Stockbeweging.Add(sb2)
        entities.SaveChanges()

        Response.Redirect("~/Beheer/BobijnBeheer.aspx", False)
    End Sub

    Protected Sub ASPxGridView1_RowUpdated(sender As Object, e As Data.ASPxDataUpdatedEventArgs) Handles ASPxGridView1.RowUpdated
        Dim entities As New VooEntities
        Dim bobid = e.NewValues.Item("id")
        Dim bobijn = entities.Bobijn.Find(bobid)
        If bobijn.AF And bobijn.DatumUitgeboekt Is Nothing Then bobijn.DatumUitgeboekt = Date.Today
        entities.SaveChanges()
    End Sub

    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim changed As Boolean = False
        For Each value In e.NewValues
            If Not IsDBNull(value.value) AndAlso Not String.IsNullOrWhiteSpace(value.value) Then
                If Not IsDBNull(e.OldValues(value.Key)) Then
                    If e.OldValues(value.Key) <> e.NewValues(value.Key) Then
                        changed = True
                    End If
                End If

                If IsDBNull(e.OldValues(value.Key)) And Not IsDBNull(e.NewValues(value.Key)) Then
                    changed = True
                End If
            End If
        Next
        e.Cancel = Not changed
    End Sub
End Class