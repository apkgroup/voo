﻿Imports DevExpress.Web
Imports System.Data.SqlClient

Public Class SiteInstellingen
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
        If Not Page.IsPostBack Then
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim cmd As New SqlCommand("SELECT top 1 datepart(year,[Datum]) as Jaar  FROM [dbo].[Planning] ORDER BY datepart(year,[Datum]) DESC", cn)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()
                    lblPlanningJaar.Text = "Laatste planningjaar: " & dr.GetInt32(0)
                    btnPlanningJaar.Text = "Voeg planning toe voor jaar " & dr.GetInt32(0) + 1
                End If
                dr.Close()

            End Using
        End If
    End Sub
    Protected Sub ASPxCallback1_Callback(ByVal sender As Object, ByVal e As CallbackEventArgs)
        If String.IsNullOrEmpty(e.Parameter) Then Return
        Dim arrWaarden As String() = e.Parameter.Split(New Char() {";"})
        Select Case arrWaarden(0)
            Case "0"
                Dim iJaar As Integer = Convert.ToInt32(arrWaarden(1)) + 1
                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()
                    Dim cmd As New SqlCommand("SELECT id FROM Gebruikers", cn)
                    Dim gebruikers As New List(Of Int32)
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    If dr.HasRows Then
                        Do While dr.Read
                            gebruikers.Add(dr.GetInt32(0))
                        Loop
                    End If
                    dr.Close()
                    For Each gebruiker As Int32 In gebruikers
                        Try
                            cmd = New SqlCommand With {.Connection = cn, .CommandText = "prPlanningJaar", .CommandType = CommandType.StoredProcedure}
                            Dim par As New SqlParameter("@Start", SqlDbType.Date) With {.Value = DateSerial(iJaar, 1, 1)}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@Einde", SqlDbType.Date) With {.Value = DateSerial(iJaar, 12, 31)}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@Gebruiker", SqlDbType.Int) With {.Value = gebruiker}
                            cmd.Parameters.Add(par)
                            cmd.ExecuteNonQuery()
                            cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                            par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Jaarplanning toegevoegd voor jaar {0} gebruiker {1} door {2} {3}", iJaar, gebruiker, Session("naam"), Session("voornaam"))}
                            cmd.Parameters.Add(par)
                            cmd.ExecuteNonQuery()
                        Catch ex As Exception
                            cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                            Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij toevoegen jaarplanning voor jaar {0} gebruiker {1} door {2} {3} : {4}", iJaar, gebruiker, Session("naam"), Session("voornaam"), ex.Message)}
                            cmd.Parameters.Add(par)
                            cmd.ExecuteNonQuery()
                        End Try
                        
                    Next
                End Using
                e.Result = "0;" & iJaar
            Case "1"
                Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                    cn.Open()
                    Dim cmd As SqlCommand = Nothing
                    Dim dr As SqlDataReader = Nothing
                    Dim par As SqlParameter = Nothing

                    Try
                        Using swr As New System.IO.StreamWriter(Server.MapPath("~/Navigatie/") & "Beheerder.xml", False)
                            swr.WriteLine("<?xml version=""1.0"" encoding=""utf-8""?>")
                            swr.WriteLine("<Menu>")
                            cmd = New SqlCommand With {.Connection = cn, .CommandText = "prMenuBeheerders", .CommandType = CommandType.StoredProcedure}
                            dr = cmd.ExecuteReader
                            Dim blStart As Boolean = True
                            If dr.HasRows Then
                                Do While dr.Read
                                    If dr.IsDBNull(1) Then
                                        If blStart Then
                                            swr.WriteLine(String.Format("<Groep Text=""{0}"" HeaderImageUrl=""{1}"">", dr.GetString(3), dr.GetString(2)))
                                            blStart = False
                                        Else
                                            swr.WriteLine("</Groep>")
                                            swr.WriteLine(String.Format("<Groep Text=""{0}"" HeaderImageUrl=""{1}"">", dr.GetString(3), dr.GetString(2)))
                                        End If

                                    Else
                                        swr.WriteLine(String.Format("<Pagina Text=""{0}"" NavigateUrl=""{1}"" />", dr.GetString(3), dr.GetString(4)))
                                    End If
                                Loop
                            End If
                            dr.Close()
                            swr.WriteLine("</Groep>")
                            swr.WriteLine("</Menu>")
                        End Using
                        Using swr As New System.IO.StreamWriter(Server.MapPath("~/Navigatie/") & "Magazijnier.xml", False)
                            swr.WriteLine("<?xml version=""1.0"" encoding=""utf-8""?>")
                            swr.WriteLine("<Menu>")
                            cmd = New SqlCommand With {.Connection = cn, .CommandText = "prMenuperLevel", .CommandType = CommandType.StoredProcedure}
                            par = New SqlParameter("level", SqlDbType.Int) With {.Value = 5}
                            cmd.Parameters.Add(par)
                            dr = cmd.ExecuteReader
                            Dim blStart As Boolean = True
                            If dr.HasRows Then
                                Do While dr.Read
                                    If Not dr.IsDBNull(1) Then
                                        If blStart Then
                                            swr.WriteLine(String.Format("<Groep Text=""{0}"" HeaderImageUrl=""{1}"">", dr.GetString(2), dr.GetString(1)))
                                            blStart = False
                                        Else
                                            swr.WriteLine("</Groep>")
                                            swr.WriteLine(String.Format("<Groep Text=""{0}"" HeaderImageUrl=""{1}"">", dr.GetString(2), dr.GetString(1)))
                                        End If

                                    Else
                                        swr.WriteLine(String.Format("<Pagina Text=""{0}"" NavigateUrl=""{1}"" />", dr.GetString(2), dr.GetString(3)))
                                    End If
                                Loop
                            End If
                            dr.Close()
                            swr.WriteLine("</Groep>")
                            swr.WriteLine("</Menu>")
                        End Using
                        Using swr As New System.IO.StreamWriter(Server.MapPath("~/Navigatie/") & "Technieker.xml", False)
                            swr.WriteLine("<?xml version=""1.0"" encoding=""utf-8""?>")
                            swr.WriteLine("<Menu>")
                            cmd = New SqlCommand With {.Connection = cn, .CommandText = "prMenuperLevel", .CommandType = CommandType.StoredProcedure}
                            par = New SqlParameter("level", SqlDbType.Int) With {.Value = 5}
                            cmd.Parameters.Add(par)
                            dr = cmd.ExecuteReader
                            Dim blStart As Boolean = True
                            If dr.HasRows Then
                                Do While dr.Read
                                    If Not dr.IsDBNull(1) Then
                                        If blStart Then
                                            swr.WriteLine(String.Format("<Groep Text=""{0}"" HeaderImageUrl=""{1}"">", dr.GetString(2), dr.GetString(1)))
                                            blStart = False
                                        Else
                                            swr.WriteLine("</Groep>")
                                            swr.WriteLine(String.Format("<Groep Text=""{0}"" HeaderImageUrl=""{1}"">", dr.GetString(2), dr.GetString(1)))
                                        End If

                                    Else
                                        swr.WriteLine(String.Format("<Pagina Text=""{0}"" NavigateUrl=""{1}"" />", dr.GetString(2), dr.GetString(3)))
                                    End If
                                Loop
                            End If
                            dr.Close()
                            swr.WriteLine("</Groep>")
                            swr.WriteLine("</Menu>")
                        End Using
                        Using swr As New System.IO.StreamWriter(Server.MapPath("~/Navigatie/") & "Urenadministratie.xml", False)
                            swr.WriteLine("<?xml version=""1.0"" encoding=""utf-8""?>")
                            swr.WriteLine("<Menu>")
                            cmd = New SqlCommand With {.Connection = cn, .CommandText = "prMenuperLevel", .CommandType = CommandType.StoredProcedure}
                            par = New SqlParameter("level", SqlDbType.Int) With {.Value = 5}
                            cmd.Parameters.Add(par)
                            dr = cmd.ExecuteReader
                            Dim blStart As Boolean = True
                            If dr.HasRows Then
                                Do While dr.Read
                                    If Not dr.IsDBNull(1) Then
                                        If blStart Then
                                            swr.WriteLine(String.Format("<Groep Text=""{0}"" HeaderImageUrl=""{1}"">", dr.GetString(2), dr.GetString(1)))
                                            blStart = False
                                        Else
                                            swr.WriteLine("</Groep>")
                                            swr.WriteLine(String.Format("<Groep Text=""{0}"" HeaderImageUrl=""{1}"">", dr.GetString(2), dr.GetString(1)))
                                        End If

                                    Else
                                        swr.WriteLine(String.Format("<Pagina Text=""{0}"" NavigateUrl=""{1}"" />", dr.GetString(2), dr.GetString(3)))
                                    End If
                                Loop
                            End If
                            dr.Close()
                            swr.WriteLine("</Groep>")
                            swr.WriteLine("</Menu>")
                        End Using
                        cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Menu's geregeneerd door {0} {1}", Session("naam"), Session("voornaam"))}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij regenereren menu's door {0} {1} : {2}", Session("naam"), Session("voornaam"), ex.Message)}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    End Try
                   
                End Using
                e.Result = "1;1"
        End Select
    End Sub


    Private Sub grid_RowUpdated(sender As Object, e As Data.ASPxDataUpdatedEventArgs) Handles grid.RowUpdated
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            If e.NewValues("Omschrijving") = "Type Werf (Node, Werf, ...)" Then
                Session("werftype") = e.NewValues("Waarde")
            End If
            Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
            Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Siteinstelling {0} gewijzigd door {1} {2}", e.NewValues("Omschrijving"), Session("naam"), Session("voornaam"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()
        End Using
    End Sub
End Class