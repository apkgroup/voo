﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="MinMax_Magazijnen.aspx.vb" Inherits="Telecom.MinMax_Magazijnen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
        .fixed {
            position: fixed;
            top: 0px;
            background-color: #ffffff !important;
        } 
        </style>
    <script type="text/javascript" src="../js/jquery-1.4.2.min.js">
</script>
		<script type="text/javascript" src="../js/fib-jquery.js"></script>
           
          <script type="text/javascript" charset="utf-8">
              $(document).ready(function () {

                  var offset = $('#gridmenu').offset();

                  $(window).scroll(function () {
                      var scrollTop = $(window).scrollTop();
                      if (offset.top < scrollTop) {
                          $('#gridmenu').addClass('fixed');
                      } else {
                          $('#gridmenu').removeClass('fixed');
                      };
                  });

              });

		</script>
    <script type="text/javascript">
        var cback = false;
        function OnClickButtonDel(s, e) {
            SetPCVisible(true);
           
        }
        
        function SetPCVisible(value) {
            var popupControl = GetPopupControl();
            if (value) {
                popupControl.Show();
            }
            else {
                popupControl.Hide();
            }
        }
       
        function GetPopupControl() {
            return popup;
        }
        function OnCallbackComplete(s, e) {
            window.location.href = "https://" + window.location.host + "/Beheer/MinMax_Magazijnen.aspx";
        }
        function SetMeldingVisible(value) {
            var popupControl = GetPopupControlMelding();
            if (value) {
                popupControl.Show();
            }
            else {
                popupControl.Hide();
            }
        }
        function GetPopupControlMelding() {
            return popupMelding;
        }
        function OnMoreInfoClick(element, key) {
            callbackPanel.SetContentHtml("");
            popupFoto.ShowAtElement(element);

        }
        function popup_Shown(s, e) {
            callbackPanel.PerformCallback();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:right; margin-bottom:10px;margin-left:10px;"><div id="gridmenu"><dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <ClientSideEvents ItemClick="function(s, e) {
    if (cboMagazijn.GetSelectedIndex()==-1) {
        SetMeldingVisible(true);
    } else {
	if (e.item.name==&quot;MinMaxSelectie&quot;) {
	SetPCVisible(true);
} else {
	if (e.item.name==&quot;ArtikelsToevoegen&quot;) {
		Callback1.PerformCallback();
} else {
        if (e.item.name==&quot;VerwijderSelectie&quot;) {
                        grid.PerformCallback(&quot;VerwijderSelectie&quot;);
        } else {
                        if (e.item.name==&quot;NieuweRij&quot;) { 

                            grid.AddNewRow();
                        }
                }
        
       }
}
}
}" />
                    <Items>
                        <dx:MenuItem ToolTip="Klik hier om alle actieve artikels uit de artikellijst te importeren" Name="ArtikelsToevoegen">
                            <Image Url="~/images/ImportChartOfAccounts.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                        <dx:MenuItem Name="MinMaxSelectie" ToolTip="Stel min-max in voor de geselecteerde artikels">
                            <Image Url="~/images/EditLines.png" Width="20px" Height="20px"></Image>
                        </dx:MenuItem>
                        <dx:MenuItem Name="VerwijderSelectie" ToolTip="Verwijder de geselecteerde artikels uit de lijst">
                            <Image Url="~/images/cancellen.png" Width="20px" Height="20px"></Image>
                        </dx:MenuItem>
                        <dx:MenuItem Name="NieuweRij" ToolTip="Voeg een nieuwe rij toe aan de lijst">
                            <Image Url="~/images/Add_32x32.png" Width="20px" Height="20px"></Image>
                        </dx:MenuItem>
                    </Items>
                   
                </dx:ASPxMenu></div></div><h2>Minimum-maximum artikels instellen per magazijn</h2>
    
    <br />
    <table>
        <tr>
            <td>Selecteer een magazijn:</td>
            <td>
                <dx:ASPxComboBox ID="cboMagazijn" ClientInstanceName="cboMagazijn" runat="server" AutoPostBack="True" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" ValueType="System.Int32">
                    <ClientSideEvents ValueChanged="function(s, e) {
                if (!grid.batchEditApi.HasChanges()) {
	lpanel.Show();
	e.processOnServer = true;
                }
}" />
                </dx:ASPxComboBox>
                
            </td>
           
        </tr>
    </table> 
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT id, Naam FROM stock_Magazijnen WHERE (ISNULL(Actief, 0) = 1) ORDER BY Naam"></asp:SqlDataSource>
     <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback1"
        OnCallback="ASPxCallback1_Callback">
         <ClientSideEvents CallbackComplete="OnCallbackComplete" />
     </dx:ASPxCallback>
    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server" ClientInstanceName="lpanel" Modal="True" Text="Bezig met laden gegevens ...">

                        </dx:ASPxLoadingPanel>
    
    <br />
    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMinMaxMagazijn" KeyFieldName="id">
        <SettingsText CommandBatchEditCancel="Annuleren" CommandBatchEditUpdate="Wijzigen opslaan" ConfirmOnLosingBatchChanges="Er zijn wijzigingen aangebracht maar deze werden nog niet bewaard (onderaan wijzigingen opslaan klikken). Ben u zeker dat u de wijzigingen niet wil bewaren?" />

        <Columns>
            <dx:GridViewCommandColumn VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Magazijn" VisibleIndex="2" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataSpinEditColumn FieldName="Minimum" VisibleIndex="6">
                <PropertiesSpinEdit DisplayFormatString="g" NumberType="Integer">
                </PropertiesSpinEdit>
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataSpinEditColumn>
            <dx:GridViewDataSpinEditColumn FieldName="Maximum" VisibleIndex="7">
                <PropertiesSpinEdit DisplayFormatString="g" NumberType="Integer">
                </PropertiesSpinEdit>
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataSpinEditColumn>
            <dx:GridViewDataComboBoxColumn Caption="Artikel ERP" FieldName="Artikel" VisibleIndex="4">
                <PropertiesComboBox DataSourceID="SqlDataSourceArtikels" TextField="Artikel" TextFormatString="{0}" ValueField="id" ValueType="System.Int32" DropDownWidth="500px">
                    <Columns>
                        <dx:ListBoxColumn FieldName="Artikel" Width="175px" />
                        <dx:ListBoxColumn FieldName="Omschrijving" Width="300px" />
                        <dx:ListBoxColumn FieldName="id" Visible="False" />
                    </Columns>
                </PropertiesComboBox>
                <EditFormSettings ColumnSpan="2" />
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn Caption="Omschrijving" FieldName="Omschrijving" VisibleIndex="5">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataColumn Caption="Foto" FieldName="HeeftFoto" VisibleIndex="8" Width="15%">
                <EditFormSettings Visible="False" />
                <DataItemTemplate>
                    <%# GetRender(Eval("HeeftFoto"))%>
                    
                </DataItemTemplate>
                 <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataColumn>
            <dx:GridViewDataTextColumn FieldName="ERP" VisibleIndex="3" Visible="False">
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn FieldName="Verpaktper" ReadOnly="True" VisibleIndex="7">
                <HeaderStyle HorizontalAlign="Right" />
                         <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn FieldName="VerpakkingsEH" VisibleIndex="9" Visible="True">
                 <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior SortMode="DisplayText" AllowFocusedRow="True" AllowSelectByRowClick="True" />
        <SettingsPager Mode="ShowAllRecords" Visible="False">
        </SettingsPager>
         <SettingsPopup>
            <EditForm HorizontalAlign="WindowCenter" Modal="True" VerticalAlign="WindowCenter" />
        </SettingsPopup>
        <SettingsEditing Mode="PopupEditForm">
        </SettingsEditing>
        <SettingsText EmptyDataRow="Geen data gevonden... selecteer een magazijn aub" />

        <Styles>           
             <BatchEditModifiedCell ForeColor="Black">
            </BatchEditModifiedCell>
             <FocusedRow ForeColor="Black">
            </FocusedRow>
           <SelectedRow ForeColor="Black" BackColor="#ff8000"></SelectedRow>
        </Styles>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceMinMaxMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [stock_Artikels_minmax_Magazijn] WHERE [id] = @id" InsertCommand="INSERT INTO [stock_Artikels_minmax_Magazijn] ([Magazijn], [Artikel], [Minimum], [Maximum]) VALUES (@Magazijn, @Artikel, @Minimum, @Maximum)" SelectCommand="SELECT stock_Artikels.Omschrijving, stock_Artikels.Artikel AS ERP, ISNULL(stock_Artikels.HeeftFoto, 0) AS HeeftFoto, stock_Artikels_minmax_Magazijn.id, stock_Artikels_minmax_Magazijn.Magazijn, stock_Artikels_minmax_Magazijn.Artikel, stock_Artikels_minmax_Magazijn.Minimum, stock_Artikels_minmax_Magazijn.Maximum, ISNULL(stock_Artikels.VerpaktPer, 1) AS Verpaktper, stock_Artikels.VerpakkingsEH FROM stock_Artikels_minmax_Magazijn INNER JOIN stock_Artikels ON stock_Artikels_minmax_Magazijn.Artikel = stock_Artikels.id WHERE (stock_Artikels_minmax_Magazijn.Magazijn = @magazijn) AND (ISNULL(stock_Artikels.Actief, 0) = 1) ORDER BY ERP" UpdateCommand="UPDATE [stock_Artikels_minmax_Magazijn] SET [Magazijn] = @Magazijn, [Artikel] = @Artikel, [Minimum] = @Minimum, [Maximum] = @Maximum WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Magazijn" Type="Int32" />
            <asp:Parameter Name="Artikel" Type="Int32" />
            <asp:Parameter Name="Minimum" Type="Decimal" />
            <asp:Parameter Name="Maximum" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="cboMagazijn" Name="magazijn" PropertyName="Value" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Magazijn" Type="Int32" />
            <asp:Parameter Name="Artikel" Type="Int32" />
            <asp:Parameter Name="Minimum" Type="Decimal" />
            <asp:Parameter Name="Maximum" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
     <asp:SqlDataSource ID="SqlDataSourceArtikels" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Artikel], [Omschrijving] FROM [stock_Artikels] WHERE (IsNull(Actief,0)=1) ORDER BY [Artikel]"></asp:SqlDataSource>

     <dx:ASPxPopupControl ID="popup" ClientInstanceName="popup" runat="server" CloseAction="CloseButton" HeaderText="Waarden doorvoeren in meervoudige selectie" Height="321px" Modal="True" Width="398px" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
         <HeaderImage Height="20px" Url="~/images/EditLines.png" Width="20px">
         </HeaderImage>
        <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <br />
             <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server">
        <Items>
            <dx:LayoutGroup Caption="Vul min en/of max in en klik op bevestigen">
                <Items>
                   <dx:LayoutItem Caption="Minimum">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxSpinEdit ID="spinMinimum" ClientInstanceName="spinMinimum" runat="server" Number="0" NumberType="Integer">
                                </dx:ASPxSpinEdit>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="Maximum">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxSpinEdit ID="spinMaximum" ClientInstanceName="spinMaximum" runat="server" Number="0" NumberType="Integer">
                                </dx:ASPxSpinEdit>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption=" ">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxButton ID="btnBevestigenselectie" runat="server" Text="Bevestigen" AutoPostBack="False">
                                    <ClientSideEvents Click="function(s, e) {
	SetPCVisible(false);
             grid.PerformCallback(&quot;MinMaxAanpassen;&quot; + spinMinimum.GetValue() + &quot;;&quot; + spinMaximum.GetValue());
}" />
                                    <Image Url="~/images/Apply_16x16.png">
                                    </Image>
                                </dx:ASPxButton>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
        </Items>
    </dx:ASPxFormLayout>
            </dx:PopupControlContentControl>
</ContentCollection>
    </dx:ASPxPopupControl>
   
     <dx:ASPxPopupControl ID="popupMelding" ClientInstanceName="popupMelding" runat="server" 
        CloseAction="CloseButton" HeaderText="Fout" 
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Modal="True"
        Width="314px" MinWidth="300px" CloseAnimationType="Fade">
        <HeaderImage Url="~/images/Security_Shields_Alert_32xLG_color.png">
        </HeaderImage>
         <ContentCollection>
<dx:PopupControlContentControl runat="server">
    Gelieve eerst een magazijn te selecteren.
    <br />
    
    </dx:PopupControlContentControl>
</ContentCollection>
     </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="popupFoto" ClientInstanceName="popupFoto" runat="server" AllowDragging="true"
        PopupHorizontalAlign="OutsideLeft" HeaderText="Foto">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxCallbackPanel ID="callbackPanel" ClientInstanceName="callbackPanel" runat="server"
                    Width="400px" Height="300px" OnCallback="callbackPanel_Callback" RenderMode="Table">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <table>
                                <tr>
                                    <td><span style="font-family:Calibri;font-size:medium;font-weight:bold;"><asp:Literal ID="litText" runat="server" Text=""></asp:Literal></span></td>
                                </tr>
                                <tr>
                                    <td><dx:ASPxBinaryImage ID="edBinaryImage" runat="server" AlternateText="Bezig met laden..." ImageAlign="Left" CssClass="Image">
                                        </dx:ASPxBinaryImage></td>
                                </tr>
                            </table>
                            
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents Shown="popup_Shown" />
    </dx:ASPxPopupControl>
</asp:Content>
