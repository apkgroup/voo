﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Statuslijst.aspx.vb" Inherits="Telecom.Statuslijst" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
         .fixed {    
    position: fixed;   
    top:0px;
    background-color: #ffffff ! important; 
}
         td {
    padding:5px;
}
    </style>
    <script type="text/javascript" src="../js/jquery-1.4.2.min.js">
</script>
		<script type="text/javascript" src="../js/fib-jquery.js"></script>
           
          <script type="text/javascript" charset="utf-8">
              $(document).ready(function () {

                  var offset = $('#gridmenu').offset();

                  $(window).scroll(function () {
                      var scrollTop = $(window).scrollTop();
                      if (offset.top < scrollTop) {
                          $('#gridmenu').addClass('fixed');
                      } else {
                          $('#gridmenu').removeClass('fixed');
                      };
                  });

              });

		</script>
    <script type="text/javascript">

      
        function SetButtonsVisibility(s) {
            if (!s.batchEditApi.HasChanges()) {
                toolbarrechts.GetItem(0).SetVisible(false);
                toolbarrechts.GetItem(1).SetVisible(false);
            }
            else {
                toolbarrechts.GetItem(0).SetVisible(true);
                toolbarrechts.GetItem(1).SetVisible(true);

            }
        }
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div style="float:right; margin-bottom:10px;margin-left:10px;margin-right:350px;"><div id="gridmenu"><dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Klik hier om de wijzingen te bewaren (wijzing=groen gemarkeerd)" Name="Bewaren" Text="">
                            <Image Url="~/images/save32x32.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                         <dx:MenuItem ToolTip="Klik hier om de wijzingen te annuleren (wijzing=groen gemarkeerd)" Name="Annuleren" Text="">
                            <Image Url="~/images/cancellen.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                        <dx:MenuItem ToolTip="Klik hier om voor de geselecteerde node meetstaatgegevens in te laden" Name="Meetstaatinladen" Text="">
                            <Image Url="~/images/ImportExcelmeetstaat.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                        <dx:MenuItem ToolTip="Klik hier om voor de geselecteerde node de meetstaatdetails weer te geven" Name="Meetstaatweergeven" Text="">
                            <Image Url="~/images/Meetstaat.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;Bewaren&quot;) {
		        grid.UpdateEdit();
               window.setTimeout(function(){ window.location.href = &quot;Statuslijst.aspx&quot;; }, 10);
             
} else {
	if (e.item.name==&quot;Annuleren&quot;) {
		grid.CancelEdit(); 
        SetButtonsVisibility(grid);
} else {
                if (e.item.name==&quot;Meetstaatinladen&quot;) {
                    grid.UpdateEdit();
             lpanel.Show();
                     e.processOnServer = true;
             } else {
                if (e.item.name==&quot;Meetstaatweergeven&quot;) {
                    grid.UpdateEdit();
                    lpanel.Show();
                     e.processOnServer = true;
             }
             }
             }
} 

 
}" />
                </dx:ASPxMenu></div></div> <h2>Statuslijst</h2>
     <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" KeyFieldName="id" Font-Names="Calibri" Font-Size="9pt">
          <SettingsText EmptyDataRow="Geen data gevonden... " />
        <ClientSideEvents Init="function(s, e){ SetButtonsVisibility(s); }" EndCallback="function(s, e){ 
SetButtonsVisibility(s);
}" 
            BatchEditEndEditing="function(s, e){ window.setTimeout(function(){ SetButtonsVisibility(s); }, 10); }" />
         <Columns>
             <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                 <EditFormSettings Visible="False" />
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn FieldName="Team" VisibleIndex="1">
                 <PropertiesTextEdit MaxLength="20">
                 </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Head-End" FieldName="Head_End" VisibleIndex="2">
                 <PropertiesTextEdit MaxLength="50">
                 </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn FieldName="Node" VisibleIndex="3">
                 <PropertiesTextEdit MaxLength="50">
                 </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Stad/Gemeente" FieldName="Plaats" VisibleIndex="4">
                 <PropertiesTextEdit MaxLength="50">
                 </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="DD Uitvoering" FieldName="DD_Uitvoering" VisibleIndex="5">
                 <PropertiesTextEdit MaxLength="50">
                 </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
             <dx:GridViewDataDateColumn Caption="DD Sparedag" FieldName="Sparedag" VisibleIndex="6">
             </dx:GridViewDataDateColumn>
                       
             <dx:GridViewDataComboBoxColumn Caption="PLM Aanvraag" FieldName="PLM_Aanvraag" VisibleIndex="7">
                 <PropertiesComboBox>
                     <Items>
                         <dx:ListEditItem Text="PENDING" Value="PENDING" />
                         <dx:ListEditItem Text="STARTED" Value="STARTED" />
                         <dx:ListEditItem Text="DONE" Value="DONE" />
                         <dx:ListEditItem Text="NOT READY" Value="NOT READY" />
                         <dx:ListEditItem Text="READY TO START" Value="READY TO START" />
                         <dx:ListEditItem Text="AFGEKEURD" Value="AFGEKEURD" />
                         <dx:ListEditItem Text="CANCELLED" Value="CANCELLED" />
                         <dx:ListEditItem Text="READY" Value="READY" />
                     </Items>
                 </PropertiesComboBox>
             </dx:GridViewDataComboBoxColumn>
             <dx:GridViewDataComboBoxColumn Caption="Status Admin" FieldName="Status_Admin" VisibleIndex="8">
                 <PropertiesComboBox>
                     <Items>
                         <dx:ListEditItem Text="DONE" Value="DONE" />
                         <dx:ListEditItem Text="NOT READY" Value="NOT READY" />
                         <dx:ListEditItem Text="READY TO START" Value="READY TO START" />
                         <dx:ListEditItem Text="AFGEKEURD" Value="AFGEKEURD" />
                         <dx:ListEditItem Text="CANCELLED" Value="CANCELLED" />
                         <dx:ListEditItem Text="READY" Value="READY" />
                     </Items>
                 </PropertiesComboBox>
             </dx:GridViewDataComboBoxColumn>
             <dx:GridViewDataComboBoxColumn Caption="Status Uitvoering" FieldName="Status_Uitvoering" VisibleIndex="9">
                 <PropertiesComboBox>
                     <Items>
                         <dx:ListEditItem Text="DONE" Value="DONE" />
                         <dx:ListEditItem Text="NOT READY" Value="NOT READY" />
                         <dx:ListEditItem Text="READY TO START" Value="READY TO START" />
                         <dx:ListEditItem Text="AFGEKEURD" Value="AFGEKEURD" />
                         <dx:ListEditItem Text="CANCELLED" Value="CANCELLED" />
                         <dx:ListEditItem Text="READY" Value="READY" />
                     </Items>
                 </PropertiesComboBox>
             </dx:GridViewDataComboBoxColumn>
              <dx:GridViewDataComboBoxColumn FieldName="Walkout" VisibleIndex="10">
                 <PropertiesComboBox>
                     <Items>
                         <dx:ListEditItem Text="PENDING" Value="PENDING" />
                         <dx:ListEditItem Text="STARTED" Value="STARTED" />
                         <dx:ListEditItem Text="DONE" Value="DONE" />
                         <dx:ListEditItem Text="NOT READY" Value="NOT READY" />
                         <dx:ListEditItem Text="READY TO START" Value="READY TO START" />
                         <dx:ListEditItem Text="AFGEKEURD" Value="AFGEKEURD" />
                         <dx:ListEditItem Text="CANCELLED" Value="CANCELLED" />
                         <dx:ListEditItem Text="READY" Value="READY" />
                     </Items>
                 </PropertiesComboBox>
             </dx:GridViewDataComboBoxColumn>
             <dx:GridViewDataComboBoxColumn FieldName="Plannen" VisibleIndex="11">
                 <PropertiesComboBox>
                     <Items>
                         <dx:ListEditItem Text="PENDING" Value="PENDING" />
                         <dx:ListEditItem Text="STARTED" Value="STARTED" />
                         <dx:ListEditItem Text="DONE" Value="DONE" />
                         <dx:ListEditItem Text="NOT READY" Value="NOT READY" />
                         <dx:ListEditItem Text="READY TO START" Value="READY TO START" />
                         <dx:ListEditItem Text="AFGEKEURD" Value="AFGEKEURD" />
                         <dx:ListEditItem Text="CANCELLED" Value="CANCELLED" />
                         <dx:ListEditItem Text="READY" Value="READY" />
                     </Items>
                 </PropertiesComboBox>
             </dx:GridViewDataComboBoxColumn>
             <dx:GridViewDataComboBoxColumn FieldName="Kringlijst" VisibleIndex="12">
                 <PropertiesComboBox>
                     <Items>
                         <dx:ListEditItem Text="PENDING" Value="PENDING" />
                         <dx:ListEditItem Text="STARTED" Value="STARTED" />
                         <dx:ListEditItem Text="DONE" Value="DONE" />
                         <dx:ListEditItem Text="NOT READY" Value="NOT READY" />
                         <dx:ListEditItem Text="READY TO START" Value="READY TO START" />
                         <dx:ListEditItem Text="AFGEKEURD" Value="AFGEKEURD" />
                         <dx:ListEditItem Text="CANCELLED" Value="CANCELLED" />
                         <dx:ListEditItem Text="READY" Value="READY" />
                     </Items>
                 </PropertiesComboBox>

             </dx:GridViewDataComboBoxColumn>
             <dx:GridViewDataComboBoxColumn FieldName="Asbuild" VisibleIndex="13">
                 <PropertiesComboBox>
                     <Items>
                         <dx:ListEditItem Text="DONE" Value="DONE" />
                         <dx:ListEditItem Text="AFGEKEURD" Value="AFGEKEURD" />
                         <dx:ListEditItem Text="MEER INFO NODIG" Value="MEER INFO NODIG" />
                         <dx:ListEditItem Text="READY TO START" Value="READY TO START" />
                     </Items>
                 </PropertiesComboBox>
             </dx:GridViewDataComboBoxColumn>
              <dx:GridViewDataComboBoxColumn FieldName="Debrief" VisibleIndex="14">
                 <PropertiesComboBox>
                     <Items>
                          <dx:ListEditItem Text="DONE" Value="DONE" />
                          <dx:ListEditItem Text="AFGEKEURD" Value="AFGEKEURD" />
                         <dx:ListEditItem Text="NOT READY" Value="NOT READY" />
                         <dx:ListEditItem Text="READY TO START" Value="READY TO START" />
                     </Items>
                 </PropertiesComboBox>
             </dx:GridViewDataComboBoxColumn>
              <dx:GridViewDataComboBoxColumn FieldName="Meetstaat" VisibleIndex="15">
                 <PropertiesComboBox>
                     <Items>
                        <dx:ListEditItem Text="DONE" Value="DONE" />
                          <dx:ListEditItem Text="AFGEKEURD" Value="AFGEKEURD" />
                         <dx:ListEditItem Text="NOT READY" Value="NOT READY" />
                         <dx:ListEditItem Text="READY TO START" Value="READY TO START" />
                     </Items>
                 </PropertiesComboBox>

             </dx:GridViewDataComboBoxColumn>
              <dx:GridViewDataComboBoxColumn FieldName="Selfbill" VisibleIndex="16">
                 <PropertiesComboBox>
                     <Items>
                        <dx:ListEditItem Text="DONE" Value="DONE" />
                          <dx:ListEditItem Text="REJECT" Value="REJECT" />
                         <dx:ListEditItem Text="READY TO START" Value="READY TO START" />
                     </Items>
                 </PropertiesComboBox>
             </dx:GridViewDataComboBoxColumn>
      <dx:GridViewDataTextColumn FieldName="Opmerking" VisibleIndex="17">
                 <PropertiesTextEdit MaxLength="255">
                 </PropertiesTextEdit>
             </dx:GridViewDataTextColumn>
         </Columns>
          <Styles>           
             <BatchEditModifiedCell ForeColor="Black" BackColor="#FF9900">
            </BatchEditModifiedCell>
             <FocusedRow ForeColor="Black">
            </FocusedRow>
           <SelectedRow ForeColor="Black" BackColor="#ff8000"></SelectedRow>
        </Styles>
          <SettingsEditing Mode="Batch">
        </SettingsEditing>
         <SettingsBehavior SortMode="DisplayText" AllowFocusedRow="True" AllowSelectByRowClick="True" />
         <Settings ShowHeaderFilterButton="True" ShowStatusBar="Hidden" />
        <SettingsSearchPanel Visible="true" />
         <SettingsPager PageSize="15">
            <PageSizeItemSettings Caption="Items per pagina:" Items="15, 25, 50" ShowAllItem="True" Visible="True">
            </PageSizeItemSettings>
        </SettingsPager>
         <SettingsText CommandBatchEditCancel="Annuleren" CommandBatchEditUpdate="Wijzigen opslaan" ConfirmOnLosingBatchChanges="Er zijn wijzigingen aangebracht maar deze werden nog niet bewaard (onderaan wijzigingen opslaan klikken). Ben u zeker dat u de wijzigingen niet wil bewaren?" />

     </dx:ASPxGridView>
     <asp:SqlDataSource ID="SqlDataSourceStatuslijst" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Statuslijst] WHERE [id] = @id" InsertCommand="INSERT INTO [Statuslijst] ([Team], [Head_End], [Node], [Plaats], [DD_Uitvoering], [Sparedag], [PLM_Aanvraag], [Status_Admin], [Status_Uitvoering], [Walkout], [Plannen], [Kringlijst], [Asbuild], [Debrief], [Meetstaat], [Selfbill], [Opmerking]) VALUES (@Team, @Head_End, @Node, @Plaats, @DD_Uitvoering, @Sparedag, @PLM_Aanvraag, @Status_Admin, @Status_Uitvoering, @Walkout, @Plannen, @Kringlijst, @Asbuild, @Debrief, @Meetstaat, @Selfbill, @Opmerking)" SelectCommand="SELECT [id], [Team], [Head_End], [Node], [Plaats], [DD_Uitvoering], [Sparedag], [PLM_Aanvraag], [Status_Admin], [Status_Uitvoering], [Walkout], [Plannen], [Kringlijst], [Asbuild], [Debrief], [Meetstaat], [Selfbill], [Opmerking] FROM [Statuslijst] ORDER BY [Node]" UpdateCommand="UPDATE [Statuslijst] SET [Team] = @Team, [Head_End] = @Head_End, [Node] = @Node, [Plaats] = @Plaats, [DD_Uitvoering] = @DD_Uitvoering, [Sparedag] = @Sparedag, [PLM_Aanvraag] = @PLM_Aanvraag, [Status_Admin] = @Status_Admin, [Status_Uitvoering] = @Status_Uitvoering, [Walkout] = @Walkout, [Plannen] = @Plannen, [Kringlijst] = @Kringlijst, [Asbuild] = @Asbuild, [Debrief] = @Debrief, [Meetstaat] = @Meetstaat, [Selfbill] = @Selfbill, [Opmerking]=@Opmerking WHERE [id] = @id">
         <DeleteParameters>
             <asp:Parameter Name="id" Type="Int32" />
         </DeleteParameters>
         <InsertParameters>
             <asp:Parameter Name="Team" Type="String" />
             <asp:Parameter Name="Head_End" Type="String" />
             <asp:Parameter Name="Node" Type="String" />
             <asp:Parameter Name="Plaats" Type="String" />
             <asp:Parameter Name="DD_Uitvoering" Type="String" />
             <asp:Parameter DbType="Date" Name="Sparedag" />
             <asp:Parameter Name="PLM_Aanvraag" Type="String" />
             <asp:Parameter Name="Status_Admin" Type="String" />
             <asp:Parameter Name="Status_Uitvoering" Type="String" />
             <asp:Parameter Name="Walkout" Type="String" />
             <asp:Parameter Name="Plannen" Type="String" />
             <asp:Parameter Name="Kringlijst" Type="String" />
             <asp:Parameter Name="Asbuild" Type="String" />
             <asp:Parameter Name="Debrief" Type="String" />
             <asp:Parameter Name="Meetstaat" Type="String" />
             <asp:Parameter Name="Selfbill" Type="String" />
             <asp:Parameter Name="Opmerking" Type="String" />
         </InsertParameters>
         <UpdateParameters>
             <asp:Parameter Name="Team" Type="String" />
             <asp:Parameter Name="Head_End" Type="String" />
             <asp:Parameter Name="Node" Type="String" />
             <asp:Parameter Name="Plaats" Type="String" />
             <asp:Parameter Name="DD_Uitvoering" Type="String" />
             <asp:Parameter DbType="Date" Name="Sparedag" />
             <asp:Parameter Name="PLM_Aanvraag" Type="String" />
             <asp:Parameter Name="Status_Admin" Type="String" />
             <asp:Parameter Name="Status_Uitvoering" Type="String" />
             <asp:Parameter Name="Walkout" Type="String" />
             <asp:Parameter Name="Plannen" Type="String" />
             <asp:Parameter Name="Kringlijst" Type="String" />
             <asp:Parameter Name="Asbuild" Type="String" />
             <asp:Parameter Name="Debrief" Type="String" />
             <asp:Parameter Name="Meetstaat" Type="String" />
             <asp:Parameter Name="Selfbill" Type="String" />
              <asp:Parameter Name="Opmerking" Type="String" />
             <asp:Parameter Name="id" Type="Int32" />
         </UpdateParameters>
     </asp:SqlDataSource>
    <br />
    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server" ClientInstanceName="lpanel" Modal="True" Text="Bezig met verwerken gegevens ...">

                        </dx:ASPxLoadingPanel>
</asp:Content>
