Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Linq
Imports System.Web
Imports System

Public Class FileDownloadHandler
    Implements IHttpHandler
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim id As String = context.Request("id")
        Dim reQruitcontext As New VooEntities
        Dim key As Int32 = id

        Dim content() As Byte = reQruitcontext.Bobijn.Find(key).PDF


        'Dim stringcontent = Convert.ToBase64String(reQruitcontext.Bobijn.Find(key).PDF, 0, reQruitcontext.Bobijn.Find(key).PDF.Length)
        'Dim fileExtension As String = GetFileExtension(reQruitcontext.Bobijn.Find(key).PDF.ToString)


        ExportToResponse(context, content, "bestand_" & id, ".pdf", False)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property



    Public Sub ExportToResponse(ByVal context As HttpContext, ByVal content() As Byte, ByVal fileName As String, ByVal fileType As String, ByVal inline As Boolean)
        context.Response.Clear()
        context.Response.ContentType = "application/" & fileType
        If inline Then
            context.Response.AddHeader("Content-Disposition", String.Format("{0}; filename={1}.{2}", "Inline", fileName, fileType))
        Else
            context.Response.AddHeader("Content-Disposition", String.Format("{0}; filename={1}.{2}", "Attachment", fileName, fileType))
        End If
        context.Response.AddHeader("Content-Length", content.Length.ToString())
        'Response.ContentEncoding = System.Text.Encoding.Default;
        context.Response.BinaryWrite(content)
        context.Response.Flush()
        context.Response.Close()
        context.Response.End()
    End Sub

    Public Shared Function GetFileExtension(ByVal base64String As String) As String
        Dim data = base64String.Substring(0, 5)
        If data.StartsWith("Qk") Then
            Return "bmp"
        End If

        Select Case data.ToUpper()
            Case "IVBOR"
                Return "png"
            Case "/9J/4"
                Return "jpg"
            Case "AAAAF"
                Return "mp4"
            Case "JVBER"
                Return "pdf"
            Case "AAABA"
                Return "ico"
            Case "UMFYI"
                Return "rar"
            Case "E1XYD"
                Return "rtf"
            Case "U1PKC"
                Return "txt"
            Case "MQOWM", "77U/M"
                Return "srt"
            Case "Qk2q2", "Qk12Kw"
                Return "bmp"
            Case Else
                Return String.Empty
        End Select
    End Function
End Class