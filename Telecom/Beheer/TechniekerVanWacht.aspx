﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="TechniekerVanWacht.aspx.vb" Inherits="Telecom.TechniekerVanWacht" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
   .fixed {    
    position: fixed;   
    top:0px;
    background-color: #ffffff ! important; 
}
     </style>
    <script type="text/javascript" src="../js/jquery-1.4.2.min.js">
</script>
		<script type="text/javascript" src="../js/fib-jquery.js"></script>
           
          <script type="text/javascript" charset="utf-8">
              $(document).ready(function () {

                  var offset = $('#gridmenu').offset();

                  $(window).scroll(function () {
                      var scrollTop = $(window).scrollTop();
                      if (offset.top < scrollTop) {
                          $('#gridmenu').addClass('fixed');
                      } else {
                          $('#gridmenu').removeClass('fixed');
                      };
                  });

              });

		</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:right; margin-bottom:10px;margin-left:10px;"><div id="gridmenu"><dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <ClientSideEvents ItemClick="function(s, e) {

	if (e.item.name==&quot;Nieuw&quot;) {
	grid.AddNewRow();
} 
}" />
                    <Items>

                        <dx:MenuItem Name="Nieuw" ToolTip="Klik hier om een regel toe te voegen">
                            <Image Url="~/images/Add_32x32.png" Width="20px" Height="20px"></Image>
                        </dx:MenuItem>
                    </Items>
                   
                </dx:ASPxMenu></div></div><h2>Beheer technieker van wacht</h2>   
    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceTechniekervanwacht" KeyFieldName="id">
        <SettingsText EmptyDataRow="Geen data gevonden... " />
        <Columns>
             <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="Start" VisibleIndex="3">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataDateColumn FieldName="Stop" VisibleIndex="4">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="IngesteldDoor" VisibleIndex="5" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="IngesteldOp" VisibleIndex="6" Visible="false">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataCheckColumn FieldName="Actief" VisibleIndex="7">
                 <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataCheckColumn>
             <dx:GridViewDataComboBoxColumn FieldName="Technieker" VisibleIndex="2">
                 <PropertiesComboBox DataSourceID="SqlDataSourceGebruikers" TextField="Werknemer" ValueField="id" ValueType="System.Int32">
                     <Columns>
                         <dx:ListBoxColumn FieldName="Werknemer" />
                         <dx:ListBoxColumn FieldName="id" Visible="False" />
                     </Columns>
                 </PropertiesComboBox>
             </dx:GridViewDataComboBoxColumn>
        </Columns>
        <SettingsDataSecurity AllowDelete="False" />
        <SettingsPager PageSize="20">
            <PageSizeItemSettings Caption="Items per pagina:" Items="10, 20, 50" ShowAllItem="True" Visible="True">
            </PageSizeItemSettings>
        </SettingsPager>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceTechniekervanwacht" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [TechVanWacht] WHERE [id] = @id" InsertCommand="INSERT INTO [TechVanWacht] ([Technieker], [Start], [Stop], [IngesteldDoor], [IngesteldOp], [Actief]) VALUES (@Technieker, @Start, @Stop, @IngesteldDoor, @IngesteldOp, @Actief)" SelectCommand="SELECT [id], [Technieker], [Start], [Stop], [IngesteldDoor], [IngesteldOp], [Actief] FROM [TechVanWacht] ORDER BY [IngesteldOp] DESC" UpdateCommand="UPDATE [TechVanWacht] SET [Technieker] = @Technieker, [Start] = @Start, [Stop] = @Stop, [IngesteldDoor] = @IngesteldDoor, [IngesteldOp] = @IngesteldOp, [Actief] = @Actief WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Technieker" Type="Int32" />
            <asp:Parameter DbType="Date" Name="Start" />
            <asp:Parameter DbType="Date" Name="Stop" />
            <asp:Parameter Name="IngesteldDoor" Type="Int32" />
            <asp:Parameter Name="IngesteldOp" Type="DateTime" />
            <asp:Parameter Name="Actief" Type="Boolean" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Technieker" Type="Int32" />
            <asp:Parameter DbType="Date" Name="Start" />
            <asp:Parameter DbType="Date" Name="Stop" />
            <asp:Parameter Name="IngesteldDoor" Type="Int32" />
            <asp:Parameter Name="IngesteldOp" Type="DateTime" />
            <asp:Parameter Name="Actief" Type="Boolean" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceGebruikers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Werknemer, id
FROM
(SELECT vwWerkn.Naam + ' ' + vwWerkn.VNaam as Werknemer, Gebruikers.id, Gebruikers.GSM FROM Gebruikers INNER JOIN (SELECT [ON],NR, NAAM, VNAAM FROM Elly_SQL.dbo.WERKN 
WHERE (isnull(uitdienst,0)&lt;&gt;1)) vwWerkn ON Gebruikers.Werkg=vwWerkn.[ON] AND Gebruikers.Werkn=vwWerkn.NR) vwGebruikers
ORDER BY Werknemer">
                    <SelectParameters>
                        <asp:SessionParameter Name="gebruiker" SessionField="userid" />
                    </SelectParameters>

                </asp:SqlDataSource>
</asp:Content>
