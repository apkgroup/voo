﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Mailaccount.aspx.vb" Inherits="Telecom.Mailaccount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}   
td {
    padding:5px;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h2>Beheer mailaccount </h2>
    <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server">
        <Items>
            <dx:LayoutGroup Caption="Mailaccount wijzigen">
                <Items>
                    <dx:LayoutItem ShowCaption="False">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxLabel ID="ASPxFormLayout1_E1" runat="server" 
                                    Text="Dit is de account die wordt gebruikt om e-mails te versturen">
                                </dx:ASPxLabel>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="Mailaccount:">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxComboBox ID="cboMailaccount" runat="server" 
                                    DataSourceID="SqlDataSourceMailaccount" DropDownRows="25" DropDownWidth="200px" 
                                    TextField="Naam" ValueField="cn" Width="300px">
                                    <Columns>
                                        <dx:ListBoxColumn FieldName="Naam" Width="200px" />
                                        <dx:ListBoxColumn FieldName="cn" Visible="False" />
                                    </Columns>
                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="Text" 
                                        ErrorTextPosition="Bottom" SetFocusOnError="True">
                                        <RequiredField ErrorText="Verplicht veld" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource ID="SqlDataSourceMailaccount" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT     TOP 100 PERCENT derivedtbl_1.cn, derivedtbl_1.sn + ' ' + derivedtbl_1.givenName AS Naam

FROM         OPENQUERY(ADSI, 
                      'SELECT cn, mail, Company, title, givenName, sn, telephoneNumber
FROM ''LDAP://apk.intern''
WHERE objectCategory = ''Person'' 
AND objectClass = ''user''
order by mail
') 
                      AS derivedtbl_1 
WHERE (derivedtbl_1.givenName is not null) and (derivedtbl_1.sn is not null) 
ORDER BY derivedtbl_1.sn + ' ' + derivedtbl_1.givenName"></asp:SqlDataSource>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption=" ">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxButton ID="btnBevestig" runat="server" Text="Bevestigen">
                                    <Image Url="~/images/Apply_16x16.png">
                                    </Image>
                                </dx:ASPxButton>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
            <dx:LayoutGroup Caption="Bevestiging" Visible="False">
                <Items>
                    <dx:LayoutItem ShowCaption="False">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer1" runat="server">
                                <dx:ASPxLabel ID="lblBevestiging" runat="server">
                                </dx:ASPxLabel>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
        </Items>
    </dx:ASPxFormLayout>
</asp:Content>
