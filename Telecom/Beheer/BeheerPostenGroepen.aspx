﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="BeheerPostenGroepen.aspx.vb" Inherits="Telecom.BeheerPostenGroepen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}

    </style>
    <script type="text/javascript">
        var groep = 0;
        function OnGroepChanged(cmbGroepen) {
            groep = cmbGroepen.GetValue();
            lijstLeden.PerformCallback(cmbGroepen.GetValue());
        }
      
        function Lidweg(id) {
           
            lijstLeden.DeleteRowByKey(id);
        }

        function Lidnaargroep(id) {
           
            lijstGebruikers.PerformCallback(id);
         
        }
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Géstion groupes des postes</h2>  
     
    <br /> 
    <table>
        <tr>
            <td>Sélectionnez un groupe</td>
            <td>


                <dx:ASPxComboBox ID="cmbGroepen" ClientInstanceName="cmdGroepen" runat="server" DataSourceID="SqlDataSourceGroepen" EnableSynchronization="False" TextField="groep" ValueField="id" ValueType="System.Int32">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
	OnGroepChanged(s);
}" />
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSourceGroepen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], groep FROM Postengroepen ORDER BY groep"></asp:SqlDataSource>


            </td>
            <td>ou créer un nouveau groupe:</td>
            <td>

                <dx:ASPxTextBox ID="ASPxTextBoxNieuweGroep" runat="server" MaxLength="25" Width="250px">
                </dx:ASPxTextBox>

            </td>
            <td>

                <dx:ASPxButton ID="ASPxButtonNieuwegroep" runat="server" Text="Crée">
                    <Image Url="~/images/Add_32x32.png" Height="20px" Width="20px">
                    </Image>
                </dx:ASPxButton>

            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;" colspan="2">
                <table>
                    <tr>
                        <td>
                            Membres de groupe</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">
                             <dx:ASPxGridView ID="lijstLeden" runat="server" AutoGenerateColumns="False" ClientInstanceName="lijstLeden" DataSourceID="SqlDataSourceLeden" KeyFieldName="id">
                                 <Columns>
                                     <dx:GridViewDataTextColumn FieldName="Poste" ReadOnly="True" VisibleIndex="0">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="1">
                                     </dx:GridViewDataTextColumn>
                                 </Columns>
                                 <SettingsPager Mode="ShowAllRecords" Visible="False">
                                 </SettingsPager>
                                 <Settings ShowColumnHeaders="False" />
                                 <SettingsText EmptyDataRow="Geen data geladen... selecteer eerst een groep aub" />
                                 <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                             </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSourceLeden" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT id, Poste, [Description] FROM Basisposten 
 WHERE (Groupe=@groep) and opdrachtgeverId = @OpdrachtgeverId ORDER BY Poste" DeleteCommand="DELETE FROM GebruikersgroepenLeden WHERE (id=@id)">
                    <DeleteParameters>
                        <asp:Parameter Name="id" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:Parameter Name="groep" />
                        <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" />
                    </SelectParameters>
                </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>

               


            </td>
            <td></td>
            <td style="vertical-align:top;" colspan="2">
                <table>
                    <tr>
                        <td>
                            Liste de postes</td>
                    </tr>
                    <tr>
                       <td>
                             <dx:ASPxGridView ID="lijstGebruikers" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGebruikers" KeyFieldName="id" ClientInstanceName="lijstGebruikers">
                                 <Columns>
                                     <dx:GridViewDataTextColumn FieldName="Poste" ReadOnly="True" VisibleIndex="1">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataHyperLinkColumn FieldName="id" ReadOnly="True" VisibleIndex="0">
                                         <PropertiesHyperLinkEdit ImageUrl="~/images/Backward_16x16.png" NavigateUrlFormatString="javascript:Lidnaargroep({0});" TextFormatString="">
                                         </PropertiesHyperLinkEdit>
                                         <EditFormSettings Visible="False" />
                                     </dx:GridViewDataHyperLinkColumn>
                                     <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="2">
                                     </dx:GridViewDataTextColumn>
                                 </Columns>
                                 <SettingsPager Mode="ShowAllRecords" Visible="False">
                                 </SettingsPager>
                                 <Settings ShowColumnHeaders="False" />
                                 <SettingsText EmptyDataRow="Geen data geladen...selecteer eerst een groep aub" />
                                 <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                 <ClientSideEvents
    EndCallback="function (s, e) {
        lijstLeden.PerformCallback(groep);
    }"
/>
                             </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSourceGebruikers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT id, Poste, [Description] From basisposten where opdrachtgeverId = @OpdrachtgeverId ORDER BY Poste">
                    <SelectParameters>
                        <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" />
                    </SelectParameters>

                </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</asp:Content>