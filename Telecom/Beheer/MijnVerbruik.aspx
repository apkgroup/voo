﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="MijnVerbruik.aspx.vb" Inherits="Telecom.MijnVerbruik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        <dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Cliquez ici pour afficher les produits exportés par excel" Name="ExportExcel" Text="">
                            <Image Url="~/images/Excel-icon.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                       
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;ExportExcel&quot;) {
		
             e.processOnServer = true;
} 
 
}" />
                </dx:ASPxMenu></h2> 
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceBestellingen" KeyFieldName="id">
        <SettingsPager PageSize="20">
        </SettingsPager>
        <Settings ShowHeaderFilterButton="True" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Werknemer" ReadOnly="True" VisibleIndex="0" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="beschrijving" VisibleIndex="1" Caption="Status" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataHyperLinkColumn FieldName="id" VisibleIndex="8">
            <PropertiesHyperLinkEdit NavigateUrlFormatString="../VerbruikDetail.aspx?id={0}" Text="details" TextFormatString="" >
            </PropertiesHyperLinkEdit>
                <Settings AllowHeaderFilter="False" ShowInFilterControl="False" />
            <EditFormSettings Visible="False" />
        </dx:GridViewDataHyperLinkColumn>
            <dx:GridViewDataTextColumn FieldName="ordernummer" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Naam1" VisibleIndex="4" Caption="Verbruikt op">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Verbruikt van" FieldName="Naam" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Datum" FieldName="datum" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="stad" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="straat" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <br />
    <h2>&nbsp;</h2>
    <p>&nbsp;</p>
    <p>
        &nbsp;</p>
    <asp:SqlDataSource ID="SqlDataSourceBestellingen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="  SELECT (
SELECT 
CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam
ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)
END
) as Werknemer, 

CASE 
WHEN [Verbruik].[status] = 1 THEN 'Begonnen met ingeven'
WHEN [Verbruik].[status] = 2 THEN 'Voltooid'
END as beschrijving
 ,van.Naam
      ,op.Naam
	  ,[stad]
      ,[straat]
      ,[datum]
, [Verbruik].[id],
[ordernummer]
FROM [Voo].[dbo].[Verbruik]
INNER JOIN
[Voo].[dbo].[Gebruikers] on [Verbruik].[gebruiker] = [Gebruikers].id
INNER JOIN
[Voo].[dbo].magazijn van on [Verbruik].verbruiktvan = van.id
INNER JOIN
[Voo].[dbo].magazijn op on [Verbruik].verbruiktop = op.id
WHERE Gebruikers.opdrachtgeverId = @opdrachtgever and (Gebruikers.Actief= 1 or Gebruikers.Actief is null) and (Gebruikers.id = @userid or [Verbruik].verbruiktvan =(select magazijnId from voo.dbo.gebruikers where id =@userid))
ORDER BY [Datum] DESC">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="opdrachtgever" SessionField="opdrachtgever" />
            <asp:SessionParameter Name="userid" SessionField="userid" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
    </dx:ASPxGridViewExporter>
      </asp:Content>
