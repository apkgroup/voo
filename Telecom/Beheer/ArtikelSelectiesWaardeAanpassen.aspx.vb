﻿Imports System.Data.SqlClient

Public Class ArtikelSelectiesWaardeAanpassen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Session("level") > 5 Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Protected Sub callbackPanel_Callback(ByVal source As Object, ByVal e As DevExpress.Web.CallbackEventArgsBase)
        edBinaryImage.Value = FindImage(grid.GetRowValues(grid.FocusedRowIndex, "id"))

        litText.Text = String.Format("{0} - {1}", grid.GetRowValuesByKeyValue(grid.GetRowValues(grid.FocusedRowIndex, "id"), "Artikel"), grid.GetRowValuesByKeyValue(grid.GetRowValues(grid.FocusedRowIndex, "id"), "Omschrijving"))
    End Sub

    Private Function FindImage(ByVal id As String) As Byte()
        Dim btImage As Byte() = Nothing
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("SELECT Foto FROM stock_Artikels WHERE (id={0})", id), cn)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not dr.IsDBNull(0) Then
                    btImage = CType(dr.GetValue(0), Byte())
                End If
            End If
            dr.Close()
        End Using
        Return btImage
    End Function
    Public Function GetRender(data As Object) As String
        Try
            Dim str As Int32 = Convert.ToInt32(data)

            If str = 1 Then

                Return "<a href=""javascript:void(0);"" onclick=""OnMoreInfoClick(this)"">Foto ...</a>"
            Else
                Return String.Empty
            End If

        Catch ex As Exception
            Return String.Empty
        End Try
     
    End Function

    Private Sub grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles grid.BatchUpdate

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            For Each args In e.UpdateValues
                Try
                    Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Artikel {0} gewijzigd door {1} {2} {3}", args.NewValues("Artikel"), Session("userid"), Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                Catch ex As Exception

                End Try
            Next


        End Using
    End Sub

End Class