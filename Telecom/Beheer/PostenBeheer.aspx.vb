﻿Public Class PostenBeheer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub

    Protected Sub ASPxGridView1_ParseValue(sender As Object, e As DevExpress.Web.Data.ASPxParseValueEventArgs) Handles ASPxGridView1.ParseValue
        If e.FieldName = "groep" Then
            Try
                e.Value = Convert.ToInt32(e.Value)

            Catch ex As Exception
                Throw New Exception("groep must be a number")
            End Try
        End If
    End Sub
End Class