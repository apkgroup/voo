﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="SMSGroepen.aspx.vb" Inherits="Telecom.SMSGroepen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}

           </style>
    <script type="text/javascript">
        var groep = 0;
        function OnGroepChanged(cmbGroepen) {
            groep = cmbGroepen.GetValue();
            
            lijstLeden.PerformCallback(cmbGroepen.GetValue());
        }
      
        function Lidweg(id) {
           
            lijstLeden.DeleteRowByKey(id);
        }

        function Lidnaargroep(id) {
       
            lijstGebruikers.PerformCallback(id);
         
        }
        function OnGetRowValues(values) {
            var controlestock = parseInt(values[2]) - parseInt(values[1]);
            if (controlestock < 0) {
                SetPCVisible2(true);
            } else {
                HuidigeId = parseInt(values[0]);
                grid.PerformCallback("K=" + HuidigeId.toString());
            }
        }
        function OnCallbackCompleteNieuwegroep(result) {
            if (result[0] == 1) {
                txtNiewegroep.SetText("");
                window.location.href = "https://" + window.location.host + "/Beheer/SMSGroepen.aspx";
                
            }
        }
        function OnCallbackCompleteNieuwContact(result) {
            if (result == "OK") {
                txtExternNaam.SetText("");
                txtExternGSM.SetText("");
               
                lijstLeden.PerformCallback(cmbGroepen.GetValue());
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Beheer SMSgroepen</h2>       
    <br /> 
    <table>
        
        <tr>
             <td>
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Selecteer een groep:">
                </dx:ASPxLabel>
            </td>
             <td>


                <dx:ASPxComboBox ID="cmbGroepen" ClientInstanceName="cmbGroepen" runat="server" DataSourceID="SqlDataSourceGroepen" EnableSynchronization="False" TextField="Naam" ValueField="id" ValueType="System.Int32">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
	OnGroepChanged(s);
}" />
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSourceGroepen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT id, Naam FROM SMSGroepen WHERE (Eigenaar = @eigenaar) AND (ISNULL(Actief, 0) = 1) ORDER BY Naam">
                    <SelectParameters>
                        <asp:SessionParameter Name="eigenaar" SessionField="userid" />
                    </SelectParameters>
                </asp:SqlDataSource>


            </td>
             <td>
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="of maak een nieuwe groep:">
                </dx:ASPxLabel>
            </td>
            <td>
                 
                <dx:ASPxTextBox ID="ASPxTextBoxNieuweGroep" runat="server" MaxLength="25" ClientInstanceName="txtNiewegroep">
                     <ValidationSettings ValidateOnLeave="False" EnableCustomValidation="True">
                                         <ErrorImage Url="~/images/warning.png">
                                         </ErrorImage>
                                     </ValidationSettings>
                </dx:ASPxTextBox>
               
             
            </td>
      
        </tr>
        <tr>
            <td></td><td></td><td></td><td><dx:ASPxButton ID="ASPxButtonNieuwegroep" runat="server" Text="Maak" AutoPostBack="False">
                    <ClientSideEvents Click="function(s, e) {
	varTekst = txtNiewegroep.GetText();
                    
	if (varTekst == null || varTekst == &quot;&quot;) {
		txtNiewegroep.SetIsValid(false);
		txtNiewegroep.SetErrorText(&quot;Vul de groepnaam in aub&quot;);
} else {
txtNiewegroep.SetIsValid(true);
Callback1.PerformCallback(varTekst);
}
}" />
                    <Image Url="~/images/CustormerGroup.png" Height="20px" Width="20px">
                    </Image>
                </dx:ASPxButton></td>
        </tr>
        <tr>
            <td style="vertical-align:top;" colspan="2">
                <table>
                    <tr>
                        
                        <td>
                            Leden van deze groep
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">
                             <dx:ASPxGridView ID="lijstLeden" runat="server" AutoGenerateColumns="False" ClientInstanceName="lijstLeden" DataSourceID="SqlDataSourceLeden" KeyFieldName="id">
                                 <Columns>
                                     <dx:GridViewDataTextColumn FieldName="Contact" ReadOnly="True" VisibleIndex="0">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataHyperLinkColumn Caption=" " FieldName="id" ReadOnly="True" VisibleIndex="1">
                                         <PropertiesHyperLinkEdit ImageUrl="~/images/Close_16x16.png" NavigateUrlFormatString="javascript:Lidweg({0});" TextFormatString="">
                                         </PropertiesHyperLinkEdit>
                                      
                                     </dx:GridViewDataHyperLinkColumn>
                                 </Columns>
                                 <SettingsPager Mode="ShowAllRecords" Visible="False">
                                 </SettingsPager>
                                 <Settings ShowColumnHeaders="False" />
                                 <SettingsText EmptyDataRow="Geen data geladen... selecteer eerst een groep aub" />
                                 <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                             </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSourceLeden" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Contact, id FROM
(SELECT vwWerkn.Naam + ' ' + vwWerkn.Vnaam as Contact, SMSgroepenLeden.id FROM SMSgroepenLeden INNER JOIN Gebruikers 
ON SMSgroepenLeden.Gebruiker=Gebruikers.id INNER JOIN 
(SELECT [ON],NR,Naam,VNaam FROM Elly_SQL.dbo.WERKN WHERE (isnull(uitdienst,0)&lt;&gt;1)) vwWerkn ON Gebruikers.Werkg=vwWerkn.[ON] AND 
Gebruikers.Werkn=vwWerkn.NR WHERE (SMSgroepenLeden.Groep=@groep) AND (isnull(Gebruikers.Actief,0)=1) 
UNION 
SELECT Naam as Contact, id FROM SMSGroepenLeden WHERE (Gebruiker is null) AND (SMSgroepenLeden.Groep=@groep)) vwContacten
ORDER BY Contact" DeleteCommand="DELETE FROM SMSgroepenLeden WHERE (id=@id)">
                    <DeleteParameters>
                        <asp:Parameter Name="id" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:SessionParameter Name="groep" SessionField="groep" />
                    </SelectParameters>
                </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>

               


            </td>
            <td></td>
            <td style="vertical-align:top;" colspan="2">
                <table>
                    <tr>
                        <td>
                            Lijst gebruikers met GSM </td>
                    </tr>
                    <tr>
                       <td>
                             <dx:ASPxGridView ID="lijstGebruikers" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGebruikers" KeyFieldName="id" ClientInstanceName="lijstGebruikers">
                                 <Columns>
                                     
                                     <dx:GridViewDataHyperLinkColumn FieldName="id" ReadOnly="True" VisibleIndex="0">
                                         <PropertiesHyperLinkEdit ImageUrl="~/images/Backward_16x16.png" NavigateUrlFormatString="javascript:Lidnaargroep({0});" TextFormatString="">
                                         </PropertiesHyperLinkEdit>
                                         <EditFormSettings Visible="False" />
                                     </dx:GridViewDataHyperLinkColumn>
                                     <dx:GridViewDataTextColumn FieldName="Werknemer" ReadOnly="True" VisibleIndex="1">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="GSM" VisibleIndex="2" ReadOnly="True">
                                     </dx:GridViewDataTextColumn>
                                 </Columns>
                                 <SettingsPager Mode="ShowAllRecords" Visible="False">
                                 </SettingsPager>
                                 <Settings ShowColumnHeaders="False" />
                                 <SettingsText EmptyDataRow="Geen data geladen...selecteer eerst een groep aub" />
                                 <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                 <ClientSideEvents
    EndCallback="function (s, e) {
        lijstLeden.PerformCallback(groep);
    }"
/>
                             </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDataSourceGebruikers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Werknemer, id, GSM
FROM
(SELECT vwWerkn.Naam + ' ' + vwWerkn.VNaam as Werknemer, Gebruikers.id, Gebruikers.GSM FROM Gebruikers INNER JOIN (SELECT [ON],NR, NAAM, VNAAM FROM Elly_SQL.dbo.WERKN 
WHERE (isnull(uitdienst,0)&lt;&gt;1)) vwWerkn ON Gebruikers.Werkg=vwWerkn.[ON] AND Gebruikers.Werkn=vwWerkn.NR WHERE (len(GSM)&gt;3) AND (isnull(Gebruikers.Actief,0)=1) 
UNION 
SELECT SMSGroepenLeden.[Naam] as Werknemer, max(cast('9999' + cast(SMSGroepenLeden.[id] as nvarchar) as int)) as id
      ,SMSGroepenLeden.[GSMnummer] as GSM
  FROM [dbo].[SMSGroepenLeden] 
  INNER JOIN SMSGroepen ON [SMSGroepenLeden].Groep=SMSGroepen.id
  where (SMSGroepen.Eigenaar=@gebruiker) AND (SMSGroepenleden.Gebruiker is null) 
  group by SMSGroepenLeden.[Naam], SMSGroepenLeden.GSMnummer) vwGSMGebruikers
ORDER BY Werknemer">
                    <SelectParameters>
                        <asp:SessionParameter Name="gebruiker" SessionField="userid" />
                    </SelectParameters>

                </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server">
        <Items>
            <dx:LayoutGroup Caption="Voeg een extern contact toe aan bovenstaande groep">
                <Items>
                    <dx:LayoutItem Caption="Naam:">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtExternNaam" ClientInstanceName="txtExternNaam" runat="server" MaxLength="50" Width="170px">
                                  <ValidationSettings ValidateOnLeave="False" EnableCustomValidation="True">
                                         <ErrorImage Url="~/images/warning.png">
                                         </ErrorImage>
                                     </ValidationSettings>
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption="GSMNr:">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxTextBox ID="txtExternGSM" ClientInstanceName="txtExternGSM" runat="server" MaxLength="12" Width="170px">
                                     <ValidationSettings ValidateOnLeave="False" EnableCustomValidation="True">
                                         <ErrorImage Url="~/images/warning.png">
                                         </ErrorImage>
                                     </ValidationSettings>
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption=" ">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server">
                                <dx:ASPxButton ID="btnExternToevoegen" runat="server" Text="Toevoegen aan groep" AutoPostBack="False">
                                    <ClientSideEvents Click="function(s, e) {
	varTekst =txtExternNaam.GetText();
                    
	if (varTekst == null || varTekst == &quot;&quot;) {
		txtExternNaam.SetIsValid(false);
		txtExternNaam.SetErrorText(&quot;Vul de naam in aub&quot;);
} else {
	varTekst =txtExternGSM.GetText();
	if (varTekst == null || varTekst == &quot;&quot;) {
		txtExternGSM.SetIsValid(false);
		txtExternGSM.SetErrorText(&quot;Vul de gsmnr in aub&quot;);
	} else {
		var patt=new RegExp(&quot;[+][3][1-2][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]&quot;);
		var res = patt.test(varTekst);
		if (res==false) {
			txtExternGSM.SetIsValid(false);
			txtExternGSM.SetErrorText(&quot;Het gsm nummer is niet goed geformatteerd, gebruik dit als format +32000000000&quot;);
		} else {
			txtExternNaam.SetIsValid(true);
txtExternGSM.SetIsValid(true);
Callback2.PerformCallback(txtExternNaam.GetText() + &quot;;&quot; + varTekst);

}
}

}
}" />
                                    <Image Url="~/images/AddContacts.png" Height="20px" Width="20px">
                                    </Image>
                                </dx:ASPxButton>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
        </Items>
                        </dx:ASPxFormLayout>

    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback1"
        OnCallback="ASPxCallback1_Callback">
         <ClientSideEvents CallbackComplete="function(s, e) { OnCallbackCompleteNieuwegroep(e.result); }" />
     </dx:ASPxCallback>
     <dx:ASPxCallback ID="ASPxCallback2" runat="server" ClientInstanceName="Callback2"
        OnCallback="ASPxCallback2_Callback">
         <ClientSideEvents CallbackComplete="function(s, e) { OnCallbackCompleteNieuwContact(e.result); }" />
     </dx:ASPxCallback>
</asp:Content>
