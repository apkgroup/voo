﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="SiteInstellingen.aspx.vb" Inherits="Telecom.SiteInstellingen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        

.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
   
         .auto-style1 {
             width: 256px;
         }
   
    </style>
    <script type="text/javascript">
        
        function OnCallbackComplete(result) {
            var resultaat = result.split(";");
            if (resultaat[0] == "0") {
                lblPlanningJaar.SetText("Laatste planningjaar: " + resultaat[1]);
                btnPlanningJaar.SetText("Voeg planning toe voor jaar " + resultaat[1]);
                lpanel.Hide()
            } else {
                if (resultaat[0] == "1") {
                    lpanel.Hide();
                    window.location.href = "https://" + window.location.host + "/Beheer/SiteInstellingen.aspx";
                }
            }
        }
      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Site instellingen</h2> 
    <br />
    <table>
        <tr>
            <td class="auto-style1">

                <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Planning instellingen" View="GroupBox" Width="200px">
                    <PanelCollection>
<dx:PanelContent runat="server">
    <dx:ASPxLabel ID="lblPlanningJaar" ClientInstanceName="lblPlanningJaar" runat="server" Text="Laatste planningjaar: ">
    </dx:ASPxLabel>
    <br />
    <br />
    <dx:ASPxButton ID="btnPlanningJaar" ClientInstanceName="btnPlanningJaar" runat="server" Text="Voeg planning toe voor jaar 2016" AutoPostBack="False">
        <ClientSideEvents Click="function(s, e) {
	lpanel.Show();
    var Jaar = lblPlanningJaar.GetText().replace(&quot;Laatste planningjaar: &quot;,&quot;&quot;);
    Callback1.PerformCallback(&quot;0&quot; + &quot;;&quot; + Jaar);
}" />
        <Image Height="20px" Url="~/images/AddAction.png" Width="20px">
        </Image>
    </dx:ASPxButton>
                        </dx:PanelContent>
</PanelCollection>
                </dx:ASPxRoundPanel>

            </td>
            <td>

            </td>
        </tr>
    </table>
    <br />

    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceInstellingen" KeyFieldName="id">
         <Settings ShowTitlePanel="True" />
         <SettingsText EmptyDataRow="Geen data gevonden... " Title="Basis parameters" />
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="false">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Naam" VisibleIndex="2" Visible="false">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Omschrijving" VisibleIndex="3">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Waarde" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsPager Visible="False" Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsCommandButton>
             <NewButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Nieuw" Url="~/images/Add_32x32.png" Width="16px">
                 </Image>
             </NewButton>
             <UpdateButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewaren" Url="~/images/Save_16x16.png" Width="16px">
                 </Image>
             </UpdateButton>
             <CancelButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Annuleren" Url="~/images/annuleren.png" Width="16px">
                 </Image>
             </CancelButton>
             <EditButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewerken" Url="~/images/Edit.png" Width="16px">
                 </Image>
             </EditButton>
             <DeleteButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Verwijderen" Url="~/images/Folders-OS-Recycle-Bin-Full-Metro-icon.png" Width="16px">
                 </Image>
             </DeleteButton>
         </SettingsCommandButton>
        <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
         <Styles>
             <TitlePanel HorizontalAlign="Left">
             </TitlePanel>
         </Styles>
    </dx:ASPxGridView>
    <br />
    <span style="color:red;font-family:Calibri;font-size:smaller;">Opgelet: als u de het werftype wijzigt, moeten de menu's opnieuw gegenereerd worden.<br />
    <dx:ASPxButton ID="btnMenu" runat="server" AutoPostBack="False" ClientInstanceName="btnMenu" Text="Regenereer de menu's">
        <ClientSideEvents Click="function(s, e) {
	lpanel.Show();
            Callback1.PerformCallback(&quot;1;1&quot;);
}" />
        <Image Height="20px" Url="~/images/build_Selection_32xLG.png" Width="20px">
        </Image>
    </dx:ASPxButton>
    </span>

    &nbsp;<asp:SqlDataSource ID="SqlDataSourceInstellingen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Standaardwaarden] WHERE [id] = @id" InsertCommand="INSERT INTO [Standaardwaarden] ([Naam], [Omschrijving], [Waarde]) VALUES (@Naam, @Omschrijving, @Waarde)" SelectCommand="SELECT [id], [Naam], [Omschrijving], [Waarde] FROM [Standaardwaarden] WHERE (isnull(Laden,0)=1) ORDER BY [Omschrijving]" UpdateCommand="UPDATE [Standaardwaarden] SET [Naam] = @Naam, [Omschrijving] = @Omschrijving, [Waarde] = @Waarde WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Naam" Type="String" />
            <asp:Parameter Name="Omschrijving" Type="String" />
            <asp:Parameter Name="Waarde" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Naam" Type="String" />
            <asp:Parameter Name="Omschrijving" Type="String" />
            <asp:Parameter Name="Waarde" Type="String" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server" ClientInstanceName="lpanel" Modal="True" Text="Bezig met verwerken ...">

                        </dx:ASPxLoadingPanel>
    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback1"
        OnCallback="ASPxCallback1_Callback">
         <ClientSideEvents CallbackComplete="function(s, e) { OnCallbackComplete(e.result); }" />
     </dx:ASPxCallback>
</asp:Content>
