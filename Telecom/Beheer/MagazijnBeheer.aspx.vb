﻿Imports System.Data.SqlClient
Imports DevExpress.Export
Imports DevExpress.Web
Imports DevExpress.XtraPrinting

Public Class MagazijnBeheer
    Inherits System.Web.UI.Page
    Sub Vertaal(taalId As Integer)
        If taalId = 1 Then
            ASPxGridView1.Columns("naam").Caption = "Naam"
            ASPxGridView1.Columns("Article").Caption = "Artikel"
            ASPxGridView1.Columns("Description").Caption = "Beschrijving"
            ASPxGridView1.Columns("Totaal aantal").Caption = "Totaal aantal"
            ASPxGridView1.Columns("AantalKapot").Caption = "Aantal onbeschikbaar"
            ASPxGridView1.Columns("Beschikbaar").Caption = "Beschikbaar"
            Literal2.Text = "Magazijn"
        Else
            ASPxGridView1.Columns("naam").Caption = "Nom"
            ASPxGridView1.Columns("article").Caption = "Article"
            ASPxGridView1.Columns("Description").Caption = "Description"
            ASPxGridView1.Columns("Totaal aantal").Caption = "Nombre"
            ASPxGridView1.Columns("AantalKapot").Caption = "Nombre Cassé"
            ASPxGridView1.Columns("Beschikbaar").Caption = "Disponible"
            Literal2.Text = "Magasin"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer

        If Not Page.IsPostBack Then



            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using

            If Not Session("isingelogd") Then
                Session("url") = Request.Url.AbsoluteUri
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
            If Not Session("isadmin") And Not Session("level") = 10 Then
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
        End If

        Vertaal(Session("taal"))
    End Sub

    Protected Sub ASPxGridView1_RowUpdated(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatedEventArgs) Handles ASPxGridView1.RowUpdated
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "INSERT INTO LOG (Gebruiker, tijdstip, actie, type) values (@userId, GetDate(), @actie, 'stock')"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = "Materiaal " & e.NewValues(0) & " van Magazijn " & e.NewValues(1) & " aangepast van " & e.OldValues(2) & " naar " & e.NewValues(2) & ". Opdrachtgever:" & Session("opdrachtgever")}
            cmd.Parameters.Add(par)

            cmd.ExecuteNonQuery()
            cn.Close()
        End Using
    End Sub

    Protected Sub ASPxGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles ASPxGridView1.SelectionChanged
        'Session("stockmagazijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub

    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("stockmagazijn") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub

    Protected Sub ASPxGridView1_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBinding
        DoSelect(SqlDataSourceStockMagazijn.ConnectionString)
    End Sub

    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [StockMagazijnId] from [Serienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("StockMagazijnId"))
        Next row
        Session("SelectResult") = result
    End Sub

    Protected Sub ASPxGridView1_DetailRowGetButtonVisibility(sender As Object, e As ASPxGridViewDetailRowButtonEventArgs) Handles ASPxGridView1.DetailRowGetButtonVisibility
        If Not (CType(Session("SelectResult"), ArrayList)).Contains(e.KeyValue) Then
            e.ButtonState = GridViewDetailRowButtonState.Hidden
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender
        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub

    Protected Sub ASPxGridView1_DataBound(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBound
        ASPxGridView1.Columns("Retour").Visible = True
        Dim voocontext As New VooEntities
        Dim hoofdmagazijnen = voocontext.Magazijn.Where(Function(x) x.Hoofd = True)
        For Each hm In hoofdmagazijnen
            If Session("MagazijnId") = hm.id Then
                ASPxGridView1.Columns("Retour").Visible = False
            End If
        Next
        If Session("level") = 10 Then
            ASPxGridView1.Columns("Retour").Visible = False
        End If

    End Sub

    Protected Sub ASPxMenu1_ItemClick(source As Object, e As MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "ExportExcel" Then
            ASPxGridViewExporter1.FileName = "Export Magazijn " & Today.Date()
            ASPxGridViewExporter1.WriteXlsxToResponse(New DevExpress.XtraPrinting.XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})

        End If
    End Sub

    Protected Sub ASPxCheckBoxNulwaarden_CheckedChanged(sender As Object, e As EventArgs) Handles ASPxCheckBoxNulwaarden.CheckedChanged
        If ASPxCheckBoxNulwaarden.Checked Then
            ASPxGridView1.DataSourceID = "SqlDataSourceStockMagazijn"
        Else
            ASPxGridView1.DataSourceID = "SqlDataSourceStockMagazijnNiet0"
        End If
        ASPxGridView1.DataBind()
    End Sub
End Class