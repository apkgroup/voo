﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class MinMaxBeheer
    Inherits System.Web.UI.Page

    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then

            Literal2.Text = "Min-Max per magazijn instellen"

        Else


        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        Session("opdrachtgeverSession") = Session("opdrachtgever")

        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer
        If Not Page.IsPostBack And String.IsNullOrEmpty(Session("MagazijnId")) Then



            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using

            If Not Session("isingelogd") Then
                Session("url") = Request.Url.AbsoluteUri
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
            If Not Session("isadmin") And Not Session("level") = 10 Then
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
        End If

        vertaal(Session("taal"))


    End Sub

    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender

        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub

    Protected Sub ASPxGridView1_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles ASPxGridView1.RowInserted



    End Sub


    Protected Sub ASPxButtonToevoegen0_Click(sender As Object, e As EventArgs) Handles ASPxButtonToevoegen0.Click
        ASPxLabelFout.Text = ""
        Dim context As New VooEntities
        Dim magid As Integer = Session("magazijnid")
        Dim matid As Integer = ASPxComboBoxArtikel.Value
        Dim minmax As minmax

        If context.minmax.Where(Function(x) x.magazijnId = magid And x.materiaalId = matid).Any Then
            minmax = context.minmax.Where(Function(x) x.magazijnId = magid And x.materiaalId = matid).FirstOrDefault
            minmax.max = ASPxSpinEditMax.Value
            minmax.min = ASPxSpinEditMin.Value
        Else
            minmax = New minmax
            minmax.max = ASPxSpinEditMax.Value
            minmax.min = ASPxSpinEditMin.Value
            minmax.materiaalId = matid
            minmax.magazijnId = magid
            context.minmax.Add(minmax)
        End If
        context.SaveChanges()




        Response.Redirect("~/Beheer/MinMaxBeheer.aspx", False)
    End Sub

    Protected Sub ASPxButtonSet0_Click(sender As Object, e As EventArgs) Handles ASPxButtonSet0.Click
        ASPxLabelFout.Text = ""
        Dim context As New VooEntities
        Dim magid As Integer = Session("magazijnid")
        Dim minmax As minmax

        For Each sm In context.StockMagazijn.Where(Function(x) x.MagazijnId = magid)
            If context.minmax.Where(Function(x) x.magazijnId = magid And x.materiaalId = sm.MateriaalId).Any Then
                minmax = context.minmax.Where(Function(x) x.magazijnId = magid And x.materiaalId = sm.MateriaalId).FirstOrDefault
                minmax.max = 0
                minmax.min = 0
            Else
                minmax = New minmax
                minmax.max = 0
                minmax.min = 0
                minmax.materiaalId = sm.MateriaalId
                minmax.magazijnId = magid
                context.minmax.Add(minmax)
            End If
        Next

        context.SaveChanges()




        Response.Redirect("~/Beheer/MinMaxBeheer.aspx", False)
    End Sub

    Protected Sub ASPxButtonSetDefault_Click(sender As Object, e As EventArgs) Handles ASPxButtonSetDefault.Click
        Dim context As New VooEntities
        Dim magid As Integer = Session("magazijnid")

        context.minmax.RemoveRange(context.minmax.Where(Function(x) x.magazijnId = magid))
        context.SaveChanges()

    End Sub
End Class