﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="GebruikersprofielInfovelden.aspx.vb" Inherits="Telecom.GebruikersprofielInfovelden" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Beheer infovelden gebruikersprofielen</h2>       
    <br /> 
    <span style="font-family:Calibri;font-size:small;">Beheer hier de infovelden die in het gebruikersprofiel door de gebruiker dienen ingevuld te worden (bv maten).</span>
    <br />
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceParameters" KeyFieldName="id">
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Omschrijving" VisibleIndex="2" Width="350px">
                <PropertiesTextEdit MaxLength="50" Width="350px">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="Actief" VisibleIndex="3">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataCheckColumn>
        </Columns>
        <SettingsPager Mode="ShowAllRecords" Visible="False">
        </SettingsPager>
        <SettingsDataSecurity AllowDelete="False" />
        <SettingsText EmptyDataRow="Geen data gevonden... klik op Nieuw om toe te voegen " />
          <SettingsCommandButton>
             <NewButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Nieuw" Url="~/images/Add_32x32.png" Width="16px">
                 </Image>
             </NewButton>
             <UpdateButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewaren" Url="~/images/Save_16x16.png" Width="16px">
                 </Image>
             </UpdateButton>
             <CancelButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Annuleren" Url="~/images/annuleren.png" Width="16px">
                 </Image>
             </CancelButton>
             <EditButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewerken" Url="~/images/Edit.png" Width="16px">
                 </Image>
             </EditButton>
             <DeleteButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Verwijderen" Url="~/images/Folders-OS-Recycle-Bin-Full-Metro-icon.png" Width="16px">
                 </Image>
             </DeleteButton>
         </SettingsCommandButton>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceParameters" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Gebruikersparameters] WHERE [id] = @id" InsertCommand="INSERT INTO [Gebruikersparameters] ([Omschrijving], [Actief]) VALUES (@Omschrijving, @Actief)" SelectCommand="SELECT [id], [Omschrijving], [Actief] FROM [Gebruikersparameters] ORDER BY [Omschrijving]" UpdateCommand="UPDATE [Gebruikersparameters] SET [Omschrijving] = @Omschrijving, [Actief] = @Actief WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Omschrijving" Type="String" />
            <asp:Parameter Name="Actief" Type="Boolean" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Omschrijving" Type="String" />
            <asp:Parameter Name="Actief" Type="Boolean" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br /> 


</asp:Content>
