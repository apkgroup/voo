﻿Imports System.Data.SqlClient
Imports System.Windows.Forms.VisualStyles.VisualStyleElement.Tab

Public Class GroepenBeheer
    Inherits System.Web.UI.Page

    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))

        If Page.IsPostBack Then
            If Session("FoutGebruiker") <> "" Then
                ASPxLabel6.Text = Session("FoutGebruiker")
                Session("FoutGebruiker") = ""
            End If
        Else
            ASPxLabel6.Text = ""
        End If




        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        If Session("taal") = 1 Then
            LiteralTitel.Text = "Nieuwe materiaalgroep toevoegen"
        Else
            LiteralTitel.Text = "Ajouter magasin external"
        End If

        ASPxGridView1.DataBind()
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        ASPxTextBoxNaam.Validate()


        'If voocontext.Gebruikers.Where(Function(x) x.Email = email).Any Then
        '    If Session("taal") = 1 Then
        '        Session("FoutGebruiker") = "Emailadres bestaat al"
        '    Else
        '        Session("FoutGebruiker") = "L'adresse email existe déjà"
        '    End If
        '    ASPxLabel6.Text = Session("FoutGebruiker")
        '    Return
        'End If

        If ASPxTextBoxNaam.Value = "" Then

            If Session("taal") = 1 Then
                Session("FoutGebruiker") = "Geen naam ingevuld"
            Else
                Session("FoutGebruiker") = "Aucun nom entré"
            End If
            ASPxLabel6.Text = Session("FoutGebruiker")
            Return
        End If



        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim mag_SQL As String = "INSERT INTO MateriaalGroep (omschrijving, Opdrachtgever) VALUES (@Naam, @opdrachtgeverId);SELECT SCOPE_IDENTITY();"
            Dim cmd = New SqlCommand(mag_SQL, cn)
            Dim par = New SqlParameter("@naam", SqlDbType.NVarChar, 50) With {.Value = ASPxTextBoxNaam.Value}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int, -1) With {.Value = Session("opdrachtgever")}
            cmd.Parameters.Add(par)
            Dim grpId As Int32 = cmd.ExecuteScalar()


        End Using



        ASPxGridView1.DataBind()



    End Sub
End Class