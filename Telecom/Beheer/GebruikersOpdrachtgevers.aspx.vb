﻿Imports System.Data.SqlClient

Public Class GebruikersOpdrachtgevers
    Inherits System.Web.UI.Page
    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then
            Literal2.Text = "Groepen"
            ASPxLabelGroepSelect.Text = "Selecteer een opdrachtgever"
            ASPxLabelGroepleden.Text = "Admins met toegang tot opdrachtgever:"
            ASPxLabelGebruikers.Text = "Gebruikerslijst Admins:"
            lijstLeden.SettingsText.EmptyDataRow = "Geen opdrachtgever geselecteerd."
            lijstGebruikers.SettingsText.EmptyDataRow = ""
        Else
            Literal2.Text = "Groupes"
            ASPxLabelGroepSelect.Text = "Groupe sélect"
            ASPxLabelGroepleden.Text = "Les membres de ce groupe"
            ASPxLabelGebruikers.Text = "Les utilisateurs"
            lijstLeden.SettingsText.EmptyDataRow = "Aucune donnée chargée. Select groupe"
            lijstGebruikers.SettingsText.EmptyDataRow = "Gebruikerslijst:"
        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using

    End Sub




    Private Sub VulLedenlijst(ByVal iGroep As Integer)

        SqlDataSourceLeden.SelectParameters(0).DefaultValue = iGroep
        lijstLeden.DataBind()

    End Sub

    Private Sub lijstLeden_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs) Handles lijstLeden.CustomCallback
        VulLedenlijst(Convert.ToInt32(e.Parameters))
    End Sub

    Private Sub lijstGebruikers_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs) Handles lijstGebruikers.CustomCallback
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Try
                Dim s_SQL As String = "SELECT id FROM AdminOpdrachtgevers WHERE (opdrachtgeverId=@groep) AND (gebruikerId=@gebruiker)"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@groep", SqlDbType.Int) With {.Value = cmbGroepen.Value}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Convert.ToInt32(e.Parameters)}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                Dim blbestaat As Boolean = dr.HasRows
                dr.Close()
                If Not blbestaat Then
                    s_SQL = "INSERT INTO AdminOpdrachtgevers (gebruikerid, opdrachtgeverId) VALUES (@gebruiker, @opdrachtgever)"
                    cmd = New SqlCommand(s_SQL, cn)
                    par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Convert.ToInt32(e.Parameters)}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@opdrachtgever", SqlDbType.Int) With {.Value = cmbGroepen.Value}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()


                End If
            Catch ex As Exception
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij toevoeging gebruiker aan gebruikersgroep door {0} {1} {2}", Session("naam"), Session("voornaam"), ex.Message)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End Try

        End Using

    End Sub

    Private Sub lijstLeden_RowDeleted(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletedEventArgs) Handles lijstLeden.RowDeleted
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Try
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Gebruiker {0} verwijderd uit gebruikersgroep {1} door {2} {3}", e.Values("Werknemer"), cmbGroepen.Text, Session("naam"), Session("voornaam"))}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            Catch ex As Exception

            End Try
        End Using

        VulLedenlijst(cmbGroepen.Value)
    End Sub


End Class