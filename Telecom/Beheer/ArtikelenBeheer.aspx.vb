﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class ArtikelenBeheer
    Inherits System.Web.UI.Page

    Protected Sub vertaal(taalid As Integer)
        'If taalid = 1 Then
        '    ASPxGridView1.Columns("Article").Caption = "Artikel"
        '    ASPxGridView1.Columns("Description").Caption = "Beschrijving"
        '    ASPxGridView1.Columns("min").Caption = "Min"
        '    ASPxGridView1.Columns("max").Caption = "Max"
        '    ASPxGridView1.Columns("HeeftSerienummer").Caption = "Heeft Serienr"
        '    ASPxGridView1.Columns("SerieVerplichtBijOpboeken").Caption = "Serienr verplicht bij opboeken"
        '    ASPxGridView1.Columns("actief").Caption = "Actief"
        '    ASPxGridView1.Columns("onbestelbaar").Caption = "Niet te bestellen door technieker"
        '    Literal2.Text = "Artikelenbeheer"
        '    LiteralOpdrachtgevers.Text = "Opdrachtgevers"
        'Else
        '    Literal2.Text = "Materiaux"
        '    ASPxGridView1.Columns("Article").Caption = "Article"
        '    ASPxGridView1.Columns("Description").Caption = "Description"
        '    ASPxGridView1.Columns("min").Caption = "Min"
        '    ASPxGridView1.Columns("max").Caption = "Max"
        '    ASPxGridView1.Columns("HeeftSerienummer").Caption = "A un Numéro de série"
        '    ASPxGridView1.Columns("SerieVerplichtBijOpboeken").Caption = "Numéro de série requis lors de l'admission"
        '    ASPxGridView1.Columns("actief").Caption = "Actief"
        '    LiteralOpdrachtgevers.Text = "Administration"
        'End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        Session("opdrachtgeverSession") = Session("opdrachtgever")
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If


    End Sub

    Protected Sub ASPxGridView1_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles ASPxGridView1.RowInserted



    End Sub

    Protected Sub ASPxGridView2_DataBound(sender As Object, e As EventArgs) Handles ASPxGridView2.DataBound
        Dim grid As ASPxGridView = TryCast(sender, ASPxGridView)
        Dim opdrachtgeverIds As New List(Of Integer)
        Dim fields As String() = {"opdrachtgeverId"}

        For i As Integer = 0 To grid.VisibleRowCount - 1
            Dim opdrachtgever = grid.GetRowValues(i, fields)

            If opdrachtgever = Session("opdrachtgever") Then
                'grid.Selection.SelectRow(i)
                grid.Selection.SetSelection(i, True)
            End If
        Next i
    End Sub

    Protected Sub ASPxButtonToevoegen0_Click(sender As Object, e As EventArgs) Handles ASPxButtonToevoegen0.Click
        Dim grid As ASPxGridView = ASPxGridView2
        Dim fields As String() = {"opdrachtgeverId"}




        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim i_sql As String = "INSERT INTO [Basismateriaal] ([Article], [Description], [actief], [HeeftSerienummer], [SerieVerplichtBijOpboeken],[onbestelbaar],[retourItem],[serienummerRegex],[bobijnArtikel],[foto],[MateriaalGroep],[MateriaalGroep2],[eenheid],[prijs],[MateriaalGroep3],[ArticleInternal],[MateriaalType],[Consignatie]) VALUES (@Article, @Description, 1, CASE WHEN @retouritem = 1 THEN 1 ELSE ISNULL(@HeeftSerienummer,0) END, ISNULL(@SerieVerplichtBijOpboeken,0),ISNULL(@Onbestelbaar,0), ISNULL(@retouritem,0),@serienummerRegex,@bobijnArtikel,@foto,@MateriaalGroep,@MateriaalGroep2,@Eenheid,@Prijs,@MateriaalGroep3,@ArticleInternal,@MateriaalType,@Consignatie);
declare @id1 as int = scope_identity();
INSERT INTO [Stockmagazijn] (MateriaalId, MagazijnId, Aantal)
SELECT @id1 , id, 0 from [Magazijn] where opdrachtgeverId in ({0});
SELECT @id1;
"



            Dim opdrachtgevers = grid.GetSelectedFieldValues(fields)

            Dim paramNames As String() = opdrachtgevers.[Select](Function(s, i) "@tag" & i.ToString()).ToArray()
            Dim stringlijst = ""
            Dim inClause As String = String.Join(", ", paramNames)

            i_sql = String.Format(i_sql, inClause)
            Dim cmd As New SqlCommand(i_sql, cn)
            For i As Integer = 0 To paramNames.Length - 1
                cmd.Parameters.AddWithValue(paramNames(i), opdrachtgevers(i))
            Next

            Dim par As New SqlParameter("@Article", SqlDbType.NVarChar) With {.Value = ASPxTextBoxArtikel.Text}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@ArticleInternal", SqlDbType.NVarChar) With {.Value = ASPxTextBoxIntern.Text}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@Description", SqlDbType.NVarChar) With {.Value = ASPxTextBoxBeschrijving.Text}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@retouritem", SqlDbType.Bit) With {.Value = False}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@HeeftSerienummer", SqlDbType.Bit) With {.Value = ASPxCheckBoxSerienr.Checked}
            cmd.Parameters.Add(par)


            If ASPxCheckBoxSerienr.Checked Then
                par = New SqlParameter("@SerieVerplichtBijOpboeken", SqlDbType.Bit) With {.Value = True}
            Else
                par = New SqlParameter("@SerieVerplichtBijOpboeken", SqlDbType.Bit) With {.Value = False}
            End If
            cmd.Parameters.Add(par)



            par = New SqlParameter("@Onbestelbaar", SqlDbType.Bit) With {.Value = False}
            cmd.Parameters.Add(par)

            par = New SqlParameter("@opdrachtgevers", SqlDbType.NVarChar) With {.Value = stringlijst}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@serienummerRegex", SqlDbType.NVarChar) With {.Value = If(ASPxTextBoxRegEx.Value Is Nothing, DBNull.Value, ASPxTextBoxRegEx.Value)}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@bobijnArtikel", SqlDbType.Bit) With {.Value = ASPxCheckBoxHaspel.Checked}
            cmd.Parameters.Add(par)

            par = New SqlParameter("@consignatie", SqlDbType.Bit) With {.Value = ASPxCheckBoxConsignatie.Checked}
            cmd.Parameters.Add(par)


            If ASPxComboBoxMateriaalType.Value IsNot Nothing AndAlso ASPxComboBoxMateriaalType.Value <> "" Then
                par = New SqlParameter("@MateriaalType", SqlDbType.Int) With {.Value = ASPxComboBoxMateriaalType.Value}
                cmd.Parameters.Add(par)
            Else
                par = New SqlParameter("@MateriaalType", SqlDbType.Int) With {.Value = DBNull.Value}
                cmd.Parameters.Add(par)
            End If


            If ASPxComboBoxMateriaalgroep.Value IsNot Nothing AndAlso ASPxComboBoxMateriaalgroep.Value <> "" Then
                par = New SqlParameter("@MateriaalGroep", SqlDbType.Int) With {.Value = ASPxComboBoxMateriaalgroep.Value}
                cmd.Parameters.Add(par)
            Else
                par = New SqlParameter("@MateriaalGroep", SqlDbType.Int) With {.Value = DBNull.Value}
                cmd.Parameters.Add(par)
            End If



            If ASPxComboBoxMateriaalgroep2.Value IsNot Nothing AndAlso ASPxComboBoxMateriaalgroep2.Value <> "" Then
                par = New SqlParameter("@MateriaalGroep2", SqlDbType.Int) With {.Value = ASPxComboBoxMateriaalgroep2.Value}
                cmd.Parameters.Add(par)
            Else
                par = New SqlParameter("@MateriaalGroep2", SqlDbType.Int) With {.Value = DBNull.Value}
                cmd.Parameters.Add(par)
            End If
            If ASPxComboBoxMateriaalgroep3.Value IsNot Nothing AndAlso ASPxComboBoxMateriaalgroep3.Value <> "" Then
                par = New SqlParameter("@MateriaalGroep3", SqlDbType.Int) With {.Value = ASPxComboBoxMateriaalgroep3.Value}
                cmd.Parameters.Add(par)
            Else
                par = New SqlParameter("@MateriaalGroep3", SqlDbType.Int) With {.Value = DBNull.Value}
                cmd.Parameters.Add(par)
            End If
            par = New SqlParameter("@Eenheid", SqlDbType.NVarChar) With {.Value = ASPxTextBoxArtikelEenheid.Text}
            cmd.Parameters.Add(par)
            If Not String.IsNullOrWhiteSpace(ASPxSpinEditPrijs.Value) Then
                If ASPxCheckBoxConsignatie.Checked Then
                    par = New SqlParameter("@Prijs", SqlDbType.Decimal) With {.Value = New Decimal(0.00)}
                    cmd.Parameters.Add(par)
                Else
                    par = New SqlParameter("@Prijs", SqlDbType.Decimal) With {.Value = ASPxSpinEditPrijs.Value}
                    cmd.Parameters.Add(par)
                End If

            Else
                par = New SqlParameter("@Prijs", SqlDbType.Decimal) With {.Value = DBNull.Value}
                cmd.Parameters.Add(par)
            End If


            Dim foto64
            If ASPxUploadControl1.UploadedFiles.Count > 0 Then
                Dim edit As ASPxUploadControl = ASPxUploadControl1
                If edit.UploadedFiles.FirstOrDefault.FileBytes.Length > 0 Then
                    foto64 = edit.UploadedFiles.FirstOrDefault.FileBytes
                Else

                    foto64 = DBNull.Value
                End If

            Else
                    foto64 = DBNull.Value
                End If

            par = New SqlParameter("@foto", SqlDbType.VarBinary) With {.Value = foto64}
            cmd.Parameters.Add(par)
            Dim matid = cmd.ExecuteScalar()

            For Each opdrachtgever In opdrachtgevers
                Dim s_SQL_l As String = "INSERT INTO materiaalOpdrachtgevers (opdrachtgeverId, materiaalId, [min],[max],[minhoofd],[maxhoofd], [actief], [locatie]) values (@opdrachtgeverId, @materiaalId,ISNULL(@min,0), ISNULL(@max,0),ISNULL(@minh,0), ISNULL(@maxh,0),1,@locatie)"
                cmd = New SqlCommand(s_SQL_l, cn)
                par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = opdrachtgever}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@materiaalId", SqlDbType.Int) With {.Value = matid}
                cmd.Parameters.Add(par)

                Try
                    If ASPxSpinEditMin.Value IsNot Nothing AndAlso ASPxSpinEditMin.Value <> "" Then
                        par = New SqlParameter("@min", SqlDbType.Int) With {.Value = ASPxSpinEditMin.Value}
                        cmd.Parameters.Add(par)
                    Else
                        par = New SqlParameter("@min", SqlDbType.Int) With {.Value = DBNull.Value}
                        cmd.Parameters.Add(par)
                    End If
                Catch ex As Exception
                    par = New SqlParameter("@min", SqlDbType.Int) With {.Value = DBNull.Value}
                    cmd.Parameters.Add(par)
                End Try


                Try
                    If ASPxSpinEditMax.Value IsNot Nothing AndAlso ASPxSpinEditMax.Value <> "" Then
                        par = New SqlParameter("@max", SqlDbType.Int) With {.Value = ASPxSpinEditMax.Value}
                        cmd.Parameters.Add(par)
                    Else
                        par = New SqlParameter("@max", SqlDbType.Int) With {.Value = DBNull.Value}
                        cmd.Parameters.Add(par)
                    End If
                Catch ex As Exception
                    par = New SqlParameter("@max", SqlDbType.Int) With {.Value = DBNull.Value}
                    cmd.Parameters.Add(par)
                End Try


                Try
                    If ASPxSpinEditMinHoofd.Value IsNot Nothing AndAlso ASPxSpinEditMinHoofd.Value <> "" Then
                        par = New SqlParameter("@minh", SqlDbType.Int) With {.Value = ASPxSpinEditMinHoofd.Value}
                        cmd.Parameters.Add(par)
                    Else
                        par = New SqlParameter("@minh", SqlDbType.Int) With {.Value = DBNull.Value}
                        cmd.Parameters.Add(par)
                    End If
                Catch ex As Exception
                    par = New SqlParameter("@minh", SqlDbType.Int) With {.Value = DBNull.Value}
                    cmd.Parameters.Add(par)
                End Try


                Try
                    If ASPxSpinEditMaxHoofd.Value IsNot Nothing AndAlso ASPxSpinEditMaxHoofd.Value <> "" Then
                        par = New SqlParameter("@maxh", SqlDbType.Int) With {.Value = ASPxSpinEditMaxHoofd.Value}
                        cmd.Parameters.Add(par)
                    Else
                        par = New SqlParameter("@maxh", SqlDbType.Int) With {.Value = DBNull.Value}
                        cmd.Parameters.Add(par)
                    End If
                Catch ex As Exception
                    par = New SqlParameter("@maxh", SqlDbType.Int) With {.Value = DBNull.Value}
                    cmd.Parameters.Add(par)
                End Try

                Try
                    If ASPxTextBoxLocatie.Value IsNot Nothing AndAlso ASPxTextBoxLocatie.Value <> "" Then
                        par = New SqlParameter("@locatie", SqlDbType.NVarChar) With {.Value = ASPxTextBoxLocatie.Value}
                        cmd.Parameters.Add(par)
                    Else
                        par = New SqlParameter("@locatie", SqlDbType.NVarChar) With {.Value = DBNull.Value}
                        cmd.Parameters.Add(par)
                    End If
                Catch ex As Exception
                    par = New SqlParameter("@locatie", SqlDbType.NVarChar) With {.Value = DBNull.Value}
                    cmd.Parameters.Add(par)
                End Try




                cmd.ExecuteNonQuery()
            Next



            Dim s_SQL = "INSERT INTO LOG (Gebruiker, tijdstip, actie, type) values (@userId, GetDate(), @actie, 'stock')"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = "Materiaal " & ASPxTextBoxArtikel.Text & " toegevoegd. Stock aangemaakt bij elk magazijn van " & Session("opdrachtgever")}
            cmd.Parameters.Add(par)

            cmd.ExecuteNonQuery()
            cn.Close()
        End Using
        Response.Redirect("~/Beheer/ArtikelenBeheer.aspx", False)
        Context.ApplicationInstance.CompleteRequest()
    End Sub

    Protected Sub ASPxUploadControl1_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs) Handles ASPxUploadControl1.FileUploadComplete

        If e.IsValid Then
            Dim FileInfo = New System.IO.FileInfo(e.UploadedFile.FileName)
            'Dim resfilename = MapPath(System.Web.Configuration.WebConfigurationManager.AppSettings("DocumentPath").ToString()) & "\" + FileInfo.Name
            'e.UploadedFile.SaveAs(resfilename)
        End If

    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        Dim voocontext As New VooEntities
        Dim artikelnr As String = ASPxTextBoxArtikel.Text
        If voocontext.Basismateriaal.Where(Function(x) x.Article = artikelnr).Any Then
            popupcontrolbox.ShowOnPageLoad = True
            Dim mat = voocontext.Basismateriaal.Where(Function(x) x.Article = artikelnr).FirstOrDefault
            ASPxLabel1.Text = "Artikel " & mat.Article & " is gevonden met de volgende omschrijving: " & mat.Description & " in de volgende magazijn(en):<br/><br/>"
            For Each opdr In mat.MateriaalOpdrachtgevers
                ASPxLabel1.Text = ASPxLabel1.Text & opdr.Opdrachtgever.naam & "<br/><br/>"

            Next
            ASPxLabel1.Text = ASPxLabel1.Text & "Wilt u dit artikel koppelen?"
        Else
            popupcontrolbox.ShowOnPageLoad = False

        End If
        pnlForm.ClientVisible = True
        pnlForm.Visible = True
    End Sub

    Protected Sub ASPxButton5_Click(sender As Object, e As EventArgs) Handles ASPxButton5.Click
        Dim voocontext As New VooEntities
        Dim artikelnr As String = ASPxTextBoxArtikel.Text
        If voocontext.Basismateriaal.Where(Function(x) x.Article = artikelnr).Any Then
            Dim mat = voocontext.Basismateriaal.Where(Function(x) x.Article = artikelnr).FirstOrDefault

            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Try
                    'groep is materiaal
                    Dim s_SQL As String = "SELECT id FROM MateriaalOpdrachtgevers WHERE (materiaalId=@matid) AND (opdrachtgeverId=@opdrachtgever)"
                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter("@matid", SqlDbType.Int) With {.Value = mat.id}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@opdrachtgever", SqlDbType.Int) With {.Value = Convert.ToInt32(Session("opdrachtgever"))}
                    cmd.Parameters.Add(par)
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    Dim blbestaat As Boolean = dr.HasRows
                    dr.Close()
                    If Not blbestaat Then
                        s_SQL = "INSERT INTO MateriaalOpdrachtgevers (materiaalId, opdrachtgeverId,[min]
      ,[max]
      ,[minhoofd]
      ,[maxhoofd],[actief]) VALUES (@matid,@opdrachtgever,0,0,0,0,1);
INSERT INTO [Stockmagazijn] (MateriaalId, MagazijnId, Aantal)
SELECT @matid , id, 0 from [Magazijn] where opdrachtgeverId = @opdrachtgever;"

                        cmd = New SqlCommand(s_SQL, cn)
                        par = New SqlParameter("@matid", SqlDbType.Int) With {.Value = mat.id}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@opdrachtgever", SqlDbType.Int) With {.Value = Convert.ToInt32(Session("opdrachtgever"))}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()


                    End If
                Catch ex As Exception
                    Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij toevoeging materiaal aan opdrachtgever door {0} {1} {2}", Session("naam"), Session("voornaam"), ex.Message)}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                End Try

            End Using
            pnlForm.ClientVisible = False
            pnlForm.Visible = False
            popupcontrolbox.ShowOnPageLoad = False
        Else


        End If

    End Sub
End Class