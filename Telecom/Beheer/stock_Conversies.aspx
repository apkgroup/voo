﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="stock_Conversies.aspx.vb" Inherits="Telecom.stock_Conversies" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
 .fixed {    
    position: fixed;   
    top:0px;
    background-color: #ffffff ! important; 
}
    </style>
    <script type="text/javascript" src="../js/jquery-1.4.2.min.js">
</script>
		<script type="text/javascript" src="../js/fib-jquery.js"></script>
           
          <script type="text/javascript" charset="utf-8">
              $(document).ready(function () {

                  var offset = $('#gridmenu').offset();

                  $(window).scroll(function () {
                      var scrollTop = $(window).scrollTop();
                      if (offset.top < scrollTop) {
                          $('#gridmenu').addClass('fixed');
                      } else {
                          $('#gridmenu').removeClass('fixed');
                      };
                  });

              });

		</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:right; margin-bottom:10px;margin-left:10px;"><div id="gridmenu"><dx:ASPxMenu ID="ASPxMenu1" ClientInstanceName="toolbarrechts" runat="server" ShowAsToolbar="True">
                    <Items>
                         <dx:MenuItem ToolTip="Klik hier om de geselecteerde rij te wijzigen" Name="Wijzigen" Text="">
                            <Image Url="~/images/Edit.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                         <dx:MenuItem ToolTip="Klik hier om de geselecteerde rij te verwijderen" Name="Verwijderen" Text="">
                            <Image Url="~/images/cancellen.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                         <dx:MenuItem ToolTip="Klik hier om een nieuwe rij toe te voegen" Name="Nieuw" Text="">
                            <Image Url="~/images/Add_32x32.png" Width="20px" Height="20px">
                            </Image>
                        </dx:MenuItem>
                    </Items>
         <ClientSideEvents ItemClick="function(s, e) {
             if (e.item.name==&quot;Wijzigen&quot;) {
		            grid.StartEditRow(grid.GetFocusedRowIndex());
} else {
	if (e.item.name==&quot;Verwijderen&quot;) {
		grid.DeleteRow(grid.GetFocusedRowIndex());
} else {
            if (e.item.name==&quot;Nieuw&quot;) {
                    grid.AddNewRow();
             }
} 
}
 
}" />
                </dx:ASPxMenu></div></div><h2>Beheer stock artikels - conversies</h2>
    <br />

    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceConversie" KeyFieldName="id">
           <SettingsText EmptyDataRow="Geen data gevonden... selecteer een pickinglijst aub" />
      
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="0" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn FieldName="Artikel" VisibleIndex="1">
                <PropertiesComboBox DataSourceID="SqlDataSourceArtikels" TextField="Artikel" TextFormatString="{0} {1}" ValueField="id" ValueType="System.Int32">
                    <Columns>
                        <dx:ListBoxColumn FieldName="Artikel" />
                        <dx:ListBoxColumn FieldName="Omschrijving" />
                        <dx:ListBoxColumn FieldName="id" Visible="False" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="ConversieArtikel" VisibleIndex="2">
                <PropertiesComboBox DataSourceID="SqlDataSourceArtikelsConversie" TextField="Artikel" TextFormatString="{0} {1}" ValueField="id" ValueType="System.Int32">
                    <Columns>
                        <dx:ListBoxColumn FieldName="Artikel" />
                        <dx:ListBoxColumn FieldName="Omschrijving" />
                        <dx:ListBoxColumn FieldName="id" Visible="False" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataSpinEditColumn Caption="Volgorde" FieldName="Conversie" VisibleIndex="3">
                <PropertiesSpinEdit DisplayFormatString="g" NumberType="Integer">
                </PropertiesSpinEdit>
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataSpinEditColumn>
        </Columns>
          <SettingsBehavior SortMode="DisplayText" AllowFocusedRow="True" />
       <SettingsPager Mode="ShowAllRecords" Visible="False">

        </SettingsPager>
        <Settings ShowHeaderFilterButton="True" ShowStatusBar="Hidden" />
           <SettingsCommandButton>
               <UpdateButton ButtonType="Image">
                   <Image Height="20px" ToolTip="Bewaar de wijziging" Url="~/images/Save_32x32.png" Width="20px">
                   </Image>
               </UpdateButton>
               <CancelButton ButtonType="Image">
                   <Image Height="20px" ToolTip="Annuleer de wijziging" Url="~/images/annuleren.png" Width="20px">
                   </Image>
               </CancelButton>
           </SettingsCommandButton>
        <Styles>           
             <FocusedRow ForeColor="Black">
            </FocusedRow>
        </Styles>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceConversie" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [stock_Artikels_Conversie] WHERE [id] = @id" InsertCommand="INSERT INTO [stock_Artikels_Conversie] ([Artikel], [ConversieArtikel], [Conversie]) VALUES (@Artikel, @ConversieArtikel, @Conversie)" SelectCommand="SELECT stock_Artikels_Conversie.[id], stock_Artikels_Conversie.[Artikel], stock_Artikels_Conversie.[ConversieArtikel], stock_Artikels_Conversie.[Conversie] FROM [stock_Artikels_Conversie] inner join stock_Artikels on stock_Artikels_Conversie.Artikel=stock_Artikels.id WHERE (isnull(stock_Artikels.Actief,0)=1)" UpdateCommand="UPDATE [stock_Artikels_Conversie] SET [Artikel] = @Artikel, [ConversieArtikel] = @ConversieArtikel, [Conversie] = @Conversie WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Artikel" Type="Int32" />
            <asp:Parameter Name="ConversieArtikel" Type="Int32" />
            <asp:Parameter Name="Conversie" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Artikel" Type="Int32" />
            <asp:Parameter Name="ConversieArtikel" Type="Int32" />
            <asp:Parameter Name="Conversie" Type="Int32" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSourceArtikels" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Artikel], [Omschrijving] FROM [stock_Artikels] WHERE (IsNull(Actief,0)=1) ORDER BY [Artikel]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceArtikelsConversie" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Artikel], [Omschrijving] FROM [stock_Artikels] ORDER BY [Artikel]"></asp:SqlDataSource>
</asp:Content>
