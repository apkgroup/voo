﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Nieuwsitems.aspx.vb" Inherits="Telecom.Nieuwsitems" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}   
td {
    padding:5px;
}
         .auto-style1 {
             margin-right: 4px;
         }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>     
    <br />

<dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceNieuwsitems" KeyFieldName="id" CssClass="auto-style1">
    <Columns>
        <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
        </dx:GridViewCommandColumn>
        <dx:GridViewDataTextColumn FieldName="Titel" VisibleIndex="2" Caption="Titre">
            <PropertiesTextEdit MaxLength="100">
            </PropertiesTextEdit>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataDateColumn FieldName="Datum" VisibleIndex="3" Caption="Date">
            <PropertiesDateEdit EditFormat="Custom" EditFormatString="d">
            </PropertiesDateEdit>
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataCheckColumn FieldName="Actief" VisibleIndex="4" Caption="Actif">
            <HeaderStyle HorizontalAlign="Center" />
        </dx:GridViewDataCheckColumn>
        <dx:GridViewDataHyperLinkColumn FieldName="id" VisibleIndex="1">
            <PropertiesHyperLinkEdit NavigateUrlFormatString="../Nieuwstoevoegen.aspx?id={0}" Text="Aanpassen" TextFormatString="">
            </PropertiesHyperLinkEdit>
            <EditFormSettings Visible="False" />
        </dx:GridViewDataHyperLinkColumn>
    </Columns>
    <SettingsPager PageSize="20">
            <PageSizeItemSettings Caption="Items per pagina:" Items="10, 20, 50" ShowAllItem="True" Visible="True">
            </PageSizeItemSettings>
        </SettingsPager>
    <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
          <SettingsCommandButton>
             <NewButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Nieuw" Url="~/images/Add_32x32.png" Width="16px">
                 </Image>
             </NewButton>
             <UpdateButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewaren" Url="~/images/Save_16x16.png" Width="16px">
                 </Image>
             </UpdateButton>
             <CancelButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Annuleren" Url="~/images/annuleren.png" Width="16px">
                 </Image>
             </CancelButton>
             <EditButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewerken" Url="~/images/Edit.png" Width="16px">
                 </Image>
             </EditButton>
             <DeleteButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Verwijderen" Url="~/images/Folders-OS-Recycle-Bin-Full-Metro-icon.png" Width="16px">
                 </Image>
             </DeleteButton>
         </SettingsCommandButton>
</dx:ASPxGridView>
<asp:SqlDataSource ID="SqlDataSourceNieuwsitems" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Nieuws] WHERE [id] = @id" InsertCommand="INSERT INTO [Nieuws] ([Titel], [Datum], [Actief]) VALUES (@Titel, @Datum, @Actief)" SelectCommand="SELECT [id], [Titel], [Datum], [Actief] FROM [Nieuws] WHERE opdrachtgeverId = @opdrachtgeverId ORDER BY [Datum] DESC" UpdateCommand="UPDATE [Nieuws] SET [Titel] = @Titel, [Datum] = @Datum, [Actief] = @Actief WHERE [id] = @id">
    <DeleteParameters>
        <asp:Parameter Name="id" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="Titel" Type="String" />
        <asp:Parameter Name="Datum" Type="DateTime" />
        <asp:Parameter Name="Actief" Type="Boolean" />
    </InsertParameters>
    <SelectParameters>
        <asp:SessionParameter DefaultValue="1" Name="opdrachtgeverId" SessionField="opdrachtgever" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="Titel" Type="String" />
        <asp:Parameter Name="Datum" Type="DateTime" />
        <asp:Parameter Name="Actief" Type="Boolean" />
        <asp:Parameter Name="id" Type="Int32" />
    </UpdateParameters>
</asp:SqlDataSource>

</asp:Content>
