﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class PaklijstBeheer
    Inherits System.Web.UI.Page




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Session("opdrachtgeverSession") = Session("opdrachtgever")
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Dim voocontext As New VooEntities
        Dim opId As Integer = Session("opdrachtgever")
        If voocontext.Opdrachtgever.Find(opId).naam <> "Proximus" Then

            ASPxGridView1.Columns("JMSNummer").Visible = False
        Else

            ASPxGridView1.Columns("ordernummer").Caption = "Matkab-nummer"
            ASPxGridView1.Columns("JMSNummer").Visible = True
        End If

    End Sub

    Protected Sub ASPxGridView1_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles ASPxGridView1.RowInserted



    End Sub





    Protected Sub ASPxButton1_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim button As ASPxButton = CType(sender, ASPxButton)
        Dim container As GridViewDataItemTemplateContainer = CType(button.NamingContainer, GridViewDataItemTemplateContainer)


        Dim entities As New VooEntities
        Dim id As Integer = container.KeyValue
        If entities.Bobijn.Find(id).PDF Is Nothing Then
            button.Text = ""
            button.ClientVisible = False
        End If
        If FileExists(container.KeyValue) Then
            button.ClientSideEvents.Click = String.Format("function(s, e) {{ window.location = 'FileDownloadHandler.ashx?id={0}'; }}", container.KeyValue)

        Else
            button.ClientEnabled = False
            'button.ClientSideEvents.Click = string.Format("function(s, e) {{ alert('There is no file for key {0}'); }}", container.KeyValue);
        End If
    End Sub

    Private Function FileExists(ByVal key As Object) As Boolean
        Return True
        ' Return Not String.IsNullOrEmpty(Product.GetData().First(Function(p) p.ProductID.Equals(key)).ImagePath)
    End Function

End Class