﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="LogPlanning.aspx.vb" Inherits="Telecom.LogPlanning" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .row {
    float:left;
    width: 96%;
    margin-right: 10px;
    height: 115px;
    overflow: auto;
    position: static;
}

td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
.fixed {    
    position: fixed;   
    top:0px;
    background-color: #ffffff ! important; 
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Raadplegen log planning (enkel recentste 4 maanden worden getoond)</h2>        
    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceLogPlanning" KeyFieldName="id">
        <SettingsText EmptyDataRow="Geen data gevonden... " />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="Gewijzigd" VisibleIndex="1">
                <PropertiesDateEdit DisplayFormatString="g">
                </PropertiesDateEdit>
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="Door" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="Datum" VisibleIndex="3">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="Dagdeel" VisibleIndex="4">
                <HeaderStyle HorizontalAlign="Right" />
                <CellStyle HorizontalAlign="Right">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Werknemer" ReadOnly="True" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Node" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
        </Columns>
         <Settings ShowHeaderFilterButton="True" ShowStatusBar="Hidden" />
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
         <SettingsPager PageSize="20">
            <PageSizeItemSettings Caption="Items per pagina:" Items="10, 20, 50" ShowAllItem="True" Visible="True">
            </PageSizeItemSettings>
        </SettingsPager>
        <SettingsSearchPanel Visible="True" />
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceLogPlanning" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT LP.[id]
      ,LP.[Gewijzigd]
      ,LP.[Door]
      ,LP.[Datum]
      ,LP.[Dagdeel]
      ,W.WERKNEMER as Werknemer
      ,LP.[Node]
  FROM [dbo].[LogPlanning] LP INNER JOIN Gebruikers G ON LP.Gebruiker=G.id 
  INNER JOIN (SELECT [ON], NR, Naam + ' ' + VNAAM as WERKNEMER FROM Elly_SQL.dbo.Werkn) W
ON G.Werkn=W.NR AND G.Werkg=W.[ON] WHERE (LP.Gewijzigd>Dateadd(month,-4,GetDate())) ORDER BY Gewijzigd DESC"></asp:SqlDataSource>
    <br />

</asp:Content>
