﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class PaklijstDetails
    Inherits System.Web.UI.Page




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Session("opdrachtgeverSession") = Session("opdrachtgever")
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        Dim entities As New VooEntities
        Dim plid As Integer = Request.QueryString("id")

        Literal1.Text = entities.Paklijst.Find(plid).ordernummer
        If entities.Paklijst.Find(plid).PaklijstLijn.Where(Function(x) x.goedgekeurd <> True).Any Then
            Literal2.Text = ""
            Literal3.Text = ""
            ASPxComboBoxMagazijn0.ClientVisible = False
            ASPxTextBox1.ClientVisible = False
            ASPxButton6.ClientVisible = False
        Else
            Literal2.Text = "Kies een magazijn om naar te verplaatsen"
            Literal3.Text = "Opmerking"
            ASPxComboBoxMagazijn0.ClientVisible = True
            ASPxTextBox1.ClientVisible = True
            ASPxButton6.ClientVisible = True
        End If


    End Sub

    Protected Sub ASPxGridView1_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles ASPxGridView1.RowInserted



    End Sub

    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("paklijstlijnId") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub

    'Protected Sub ASPxGridView1_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBinding
    '    DoSelect(SqlDataSourcePaklijstLijnen.ConnectionString)
    'End Sub

    'Private Sub DoSelect2(ByVal connectionString As String)
    '    Dim selectResult As New DataView()
    '    Dim selectCommand As String = "select distinct [paklijstlijnId] from [PaklijstLijnSerienummer]"
    '    Using ds As New SqlDataSource(connectionString, selectCommand)
    '        selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
    '    End Using
    '    Dim result As New ArrayList()
    '    For Each row As DataRow In selectResult.Table.Rows
    '        result.Add(row("paklijstlijnId"))
    '    Next row
    '    Session("SelectResult2") = result
    'End Sub

    Protected Sub ASPxGridView4_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("paklijstlijnId2") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub

    'Protected Sub ASPxGridView3_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView3.DataBinding
    '    DoSelect2(SqlDataSourcePaklijstLijnen.ConnectionString)
    'End Sub

    'Private Sub DoSelect(ByVal connectionString As String)
    '    Dim selectResult As New DataView()
    '    Dim selectCommand As String = "select distinct [paklijstlijnId] from [PaklijstLijnSerienummer]"
    '    Using ds As New SqlDataSource(connectionString, selectCommand)
    '        selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
    '    End Using
    '    Dim result As New ArrayList()
    '    For Each row As DataRow In selectResult.Table.Rows
    '        result.Add(row("paklijstlijnId"))
    '    Next row
    '    Session("SelectResult") = result
    'End Sub




    Protected Sub ASPxButton1_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim button As ASPxButton = CType(sender, ASPxButton)
        Dim container As GridViewDataItemTemplateContainer = CType(button.NamingContainer, GridViewDataItemTemplateContainer)


        Dim entities As New VooEntities
        Dim id As Integer = container.KeyValue
        If entities.Bobijn.Find(id).PDF Is Nothing Then
            button.Text = ""
            button.ClientVisible = False
        End If
        If FileExists(container.KeyValue) Then
            button.ClientSideEvents.Click = String.Format("function(s, e) {{ window.location = 'FileDownloadHandler.ashx?id={0}'; }}", container.KeyValue)

        Else
            button.ClientEnabled = False
            'button.ClientSideEvents.Click = string.Format("function(s, e) {{ alert('There is no file for key {0}'); }}", container.KeyValue);
        End If
    End Sub

    Private Function FileExists(ByVal key As Object) As Boolean
        Return True
        ' Return Not String.IsNullOrEmpty(Product.GetData().First(Function(p) p.ProductID.Equals(key)).ImagePath)
    End Function

    Protected Sub ASPxButton5_Click(sender As Object, e As EventArgs) Handles ASPxButton5.Click
        Dim entities As New VooEntities
        Dim basis As Basismateriaal
        Dim opdrachtgeverId As Int32 = Convert.ToInt32(Session("opdrachtgever"))
        Dim artikelnr As String = ASPxTextBoxArtikelNummer.Value
        If entities.Basismateriaal.Where(Function(x) x.Article = artikelnr).Any Then
            basis = entities.Basismateriaal.Where(Function(x) x.Article = artikelnr).FirstOrDefault
        Else
            basis = New Basismateriaal
            basis.actief = True
            basis.Article = ASPxTextBoxArtikelNummer.Value
            basis.Description = ASPxTextBoxOmschrijving.Value
            basis.HeeftSerienummer = False
            basis.SerieVerplichtBijOpboeken = False
            basis.bobijnArtikel = False
            basis.max = 0
            basis.min = 0
            basis.onbestelbaar = False
            basis.retourItem = False

            entities.Basismateriaal.Add(basis)
        End If
        entities.SaveChanges()

        If entities.MateriaalOpdrachtgevers.Where(Function(x) x.materiaalId = basis.id And x.opdrachtgeverId = opdrachtgeverId).Any Then
            'is al gekoppeld, no worries
        Else
            'koppelen en aanmaken
            Dim matop As New MateriaalOpdrachtgevers
            matop.materiaalId = basis.id
            matop.min = 0
            matop.max = 0
            matop.minhoofd = 0
            matop.maxhoofd = 0
            matop.opdrachtgeverId = opdrachtgeverId
            entities.MateriaalOpdrachtgevers.Add(matop)
            entities.SaveChanges()

            'begin

        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim i_sql As String = "INSERT INTO [Stockmagazijn] (MateriaalId, MagazijnId, Aantal) SELECT @id1 , id, 0 from [Magazijn] m where opdrachtgeverId in (@opdrachtgeverid) AND NOT EXISTS (SELECT 1 FROM Stockmagazijn WHERE MateriaalId = @id1 and MagazijnId = m.id)"


            Dim cmd As New SqlCommand(i_sql, cn)

            Dim par As New SqlParameter("@id1", SqlDbType.Int) With {.Value = basis.id}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@opdrachtgeverid", SqlDbType.Int) With {.Value = opdrachtgeverId}
            cmd.Parameters.Add(par)

            cmd.ExecuteNonQuery()
            'end
        End Using

        'For Each magazijn In entities.Magazijn.Where(Function(x) x.OpdrachtgeverId = opdrachtgeverId).ToList
        '    If magazijn.StockMagazijn.Where(Function(x) x.MateriaalId = basis.id).Any Then
        '        'bestaat al, alles is oke
        '    Else
        '        Dim sm As New StockMagazijn
        '        sm.MateriaalId = basis.id
        '        sm.MagazijnId = magazijn.id
        '        sm.Aantal = 0
        '        entities.StockMagazijn.Add(sm)
        '    End If
        'Next
        'entities.SaveChanges()

        Dim id As Integer = Convert.ToInt32(Request.QueryString("id"))
        Dim paklijst = entities.Paklijst.Find(id)
        Dim aantepassen = entities.StockMagazijn.Where(Function(x) x.MagazijnId = paklijst.magazijnId And x.MateriaalId = basis.id).FirstOrDefault
        aantepassen.Aantal = aantepassen.Aantal + ASPxSpinEditAantal.Value
        entities.SaveChanges()
        Dim pllid As Integer = ASPxHiddenFieldLijnId.FirstOrDefault.Value
        Dim paklijstlijn = entities.PaklijstLijn.Find(pllid)
        paklijstlijn.goedgekeurd = True
        Dim sb As New Stockbeweging
        sb.stockmagazijnId = aantepassen.id
        sb.opmerking = "Stock inlezen via sheet: Artikel aanmaken"
        sb.gebruiker = Session("userid")
        sb.beweging = ASPxSpinEditAantal.Value
        sb.datum = DateTime.Now
        entities.Stockbeweging.Add(sb)
        entities.SaveChanges()
        For Each plserie In paklijstlijn.PaklijstLijnSerienummer
            If entities.Serienummer.Where(Function(x) x.serienummer1 = plserie.serienummer).Any Then
                Throw New Exception("Serienummer " & plserie.serienummer & " bestaat al in systeem")
            End If
            Dim serie As New Serienummer
            serie.StockMagazijnId = aantepassen.id
            serie.serienummer1 = plserie.serienummer
            serie.statusId = 3
            serie.uitgeboekt = False
            serie.datumOntvangstMagazijn = DateTime.Today
            serie.datumGemaakt = DateTime.Today
            entities.Serienummer.Add(serie)
            entities.SaveChanges()
            Dim sbsr As New StockbewegingSerienummer
            sbsr.serienummerId = serie.id
            sbsr.stockbewegingid = sb.id
        Next
        entities.SaveChanges()


        Response.Redirect("~//Beheer/PaklijstDetails.aspx?id=" & Request.QueryString("id"), False)
        context.ApplicationInstance.CompleteRequest()


    End Sub

    Protected Sub ASPxButton6_Click(sender As Object, e As EventArgs) Handles ASPxButton6.Click
        Dim context As New VooEntities
        Dim plid As Integer = Request.QueryString("id")
        Dim paklijst = context.Paklijst.Find(plid)
        Dim magnaar As Integer = ASPxComboBoxMagazijn0.Value
        For Each paklijstlijn In paklijst.PaklijstLijn
            Dim smvan = context.StockMagazijn.Where(Function(x) x.Basismateriaal.Article = paklijstlijn.article And x.MagazijnId = paklijst.magazijnId).FirstOrDefault
            Dim smnaar = context.StockMagazijn.Where(Function(x) x.Basismateriaal.Article = paklijstlijn.article And x.MagazijnId = magnaar).FirstOrDefault

            smvan.Aantal = smvan.Aantal - paklijstlijn.aantal
            smnaar.Aantal = smnaar.Aantal + paklijstlijn.aantal
            context.SaveChanges()

            Dim sb As New Stockbeweging
            sb.stockmagazijnId = smvan.id
            sb.opmerking = "Overboeken paklijst (van)"
            sb.gebruiker = Session("userid")
            sb.beweging = paklijstlijn.aantal
            sb.datum = DateTime.Now
            context.Stockbeweging.Add(sb)
            context.SaveChanges()


            For Each plserie In paklijstlijn.PaklijstLijnSerienummer
                Dim sbsr As New StockbewegingSerienummer
                sbsr.serienummerId = context.Serienummer.Where(Function(x) x.serienummer1 = plserie.serienummer).FirstOrDefault.id
                sbsr.stockbewegingid = sb.id
            Next


            Dim sb2 As New Stockbeweging
            sb2.stockmagazijnId = smvan.id
            sb2.opmerking = "Overboeken paklijst (naar)"
            sb2.gebruiker = Session("userid")
            sb2.beweging = paklijstlijn.aantal
            sb2.datum = DateTime.Now
            context.Stockbeweging.Add(sb2)
            context.SaveChanges()
            For Each plserie In paklijstlijn.PaklijstLijnSerienummer
                Dim sbsr As New StockbewegingSerienummer
                sbsr.serienummerId = context.Serienummer.Where(Function(x) x.serienummer1 = plserie.serienummer).FirstOrDefault.id
                sbsr.stockbewegingid = sb.id
            Next
            context.SaveChanges()

        Next

        paklijst.magazijnId = magnaar


        paklijst.opmerking = ASPxTextBox1.Value
        context.SaveChanges()
        If paklijst.magazijnId = paklijst.voorzieneLocatie Then
            paklijst.afgehandeld = True
            paklijst.datumVerplaatst = Today.Date

        End If
        context.SaveChanges()

        Response.Redirect("~/Beheer/PaklijstBeheer.aspx", False)
    End Sub

    Protected Sub ASPxComboBoxMagazijn0_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn0.PreRender
        Dim entities As New VooEntities
        Dim plid As Integer = Request.QueryString("id")
        Dim voorzienelocatie = entities.Paklijst.Find(plid).voorzieneLocatie

        ASPxComboBoxMagazijn0.ValueType = GetType(Integer)
        ASPxComboBoxMagazijn0.SelectedItem = ASPxComboBoxMagazijn0.Items.FindByValue(voorzienelocatie)
    End Sub
End Class