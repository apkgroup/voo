﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class GrabbelBeheer
    Inherits System.Web.UI.Page

    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then

            Literal2.Text = "Grabbelmateriaalbeheer"

        Else


        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        Session("opdrachtgeverSession") = Session("opdrachtgever")

        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer
        If Not Page.IsPostBack And String.IsNullOrEmpty(Session("MagazijnId")) Then



            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using

            If Not Session("isingelogd") Then
                Session("url") = Request.Url.AbsoluteUri
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
            If Not Session("isadmin") And Not Session("level") = 10 Then
                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
        End If

        vertaal(Session("taal"))


    End Sub





    Protected Sub ASPxGridView1_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles ASPxGridView1.RowInserted



    End Sub


    Protected Sub ASPxButtonToevoegen0_Click(sender As Object, e As EventArgs) Handles ASPxButtonToevoegen0.Click
        ASPxLabelFout.Text = ""
        Dim fields As String() = {"opdrachtgeverId"}
        If ASPxSpinEditHoeveelheid.Value < 1 Or String.IsNullOrEmpty(ASPxTextBoxNaam.Value) Then
            ASPxLabelFout.Text = "Hoeveelheid moet hoger zijn dan 0 en naam moet ingevuld zijn"
            Return
        End If
        Dim voocontext As New VooEntities
        Dim magId As Integer = Session("MagazijnId")
        If voocontext.Locatie.Where(Function(x) x.magazijnid = magId And x.naam = ASPxTextBoxNaam.Text).Any Then
            ASPxLabelFout.Text = "Naam moet uniek zijn binnen 1 opdrachtgever"
            Return
        End If

        Dim grabbel As New Grabbelmateriaal
        grabbel.AF = False
        grabbel.Eenheid = ASPxTextBoxEH.Value
        grabbel.max = ASPxSpinEditHoeveelheid.Value
        grabbel.opdrachtgeverId = Session("opdrachtgever")
        grabbel.Omschrijving = ASPxTextBoxNaam.Value
        voocontext.Grabbelmateriaal.Add(grabbel)
        voocontext.SaveChanges()


        Response.Redirect("~/Beheer/GrabbelBeheer.aspx", False)

    End Sub
End Class