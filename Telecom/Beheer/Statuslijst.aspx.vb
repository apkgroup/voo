﻿Imports DevExpress.Web
Imports System.Data.SqlClient

Public Class Statuslijst
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
       
        If Not Page.IsPostBack Then
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim cmd As New SqlCommand With {.Connection = cn, .CommandText = "prToevoegenStatuslijst", .CommandType = CommandType.StoredProcedure}
                cmd.ExecuteNonQuery()
            End Using
            grid.DataSourceID = "SqlDataSourceStatuslijst"
            grid.DataBind()

        End If
        AddHandler grid.HtmlDataCellPrepared, New ASPxGridViewTableDataCellEventHandler(AddressOf ASPxGridView1_HtmlDataCellPrepared)
    End Sub

    Protected Sub ASPxGridView1_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As ASPxGridViewTableDataCellEventArgs)

        If Not e.DataColumn.FieldName = "Sparedag" Then
            If Not IsDBNull(e.CellValue) Then

                Select Case e.CellValue
                    Case "PENDING"
                        e.Cell.BackColor = Drawing.Color.FromArgb(255, 235, 156)
                        e.Cell.ForeColor = Drawing.Color.FromArgb(207, 148, 0)
                        e.Cell.Font.Size = New FontUnit(9)
                        e.Cell.Font.Name = "Calibri"
                        e.Cell.Font.Bold = True
                        e.Cell.HorizontalAlign = HorizontalAlign.Center
                    Case "STARTED"
                        e.Cell.BackColor = Drawing.Color.FromArgb(255, 235, 156)
                        e.Cell.ForeColor = Drawing.Color.FromArgb(207, 148, 0)
                        e.Cell.Font.Size = New FontUnit(9)
                        e.Cell.Font.Name = "Calibri"
                        e.Cell.Font.Bold = True
                        e.Cell.HorizontalAlign = HorizontalAlign.Center
                    Case "DONE"
                        e.Cell.BackColor = Drawing.Color.FromArgb(198, 239, 206)
                        e.Cell.ForeColor = Drawing.Color.FromArgb(0, 102, 0)
                        e.Cell.Font.Size = New FontUnit(9)
                        e.Cell.Font.Name = "Calibri"
                        e.Cell.Font.Bold = True
                        e.Cell.HorizontalAlign = HorizontalAlign.Center
                    Case "NOT READY"
                        e.Cell.BackColor = Drawing.Color.FromArgb(255, 204, 204)
                        e.Cell.ForeColor = Drawing.Color.FromArgb(153, 0, 0)
                        e.Cell.Font.Size = New FontUnit(9)
                        e.Cell.Font.Name = "Calibri"
                        e.Cell.Font.Bold = True
                        e.Cell.HorizontalAlign = HorizontalAlign.Center
                    Case "READY TO START"
                        e.Cell.BackColor = Drawing.Color.FromArgb(255, 235, 156)
                        e.Cell.ForeColor = Drawing.Color.FromArgb(207, 148, 0)
                        e.Cell.Font.Size = New FontUnit(9)
                        e.Cell.Font.Name = "Calibri"
                        e.Cell.Font.Bold = True
                        e.Cell.HorizontalAlign = HorizontalAlign.Center
                    Case "AFGEKEURD"
                        e.Cell.BackColor = Drawing.Color.FromArgb(255, 204, 204)
                        e.Cell.ForeColor = Drawing.Color.FromArgb(153, 0, 0)
                        e.Cell.Font.Size = New FontUnit(9)
                        e.Cell.Font.Name = "Calibri"
                        e.Cell.Font.Bold = True
                        e.Cell.HorizontalAlign = HorizontalAlign.Center
                    Case "CANCELLED"
                        e.Cell.BackColor = Drawing.Color.FromArgb(255, 204, 204)
                        e.Cell.ForeColor = Drawing.Color.FromArgb(153, 0, 0)
                        e.Cell.Font.Size = New FontUnit(9)
                        e.Cell.Font.Name = "Calibri"
                        e.Cell.Font.Bold = True
                        e.Cell.HorizontalAlign = HorizontalAlign.Center
                    Case "READY"
                        e.Cell.BackColor = Drawing.Color.FromArgb(255, 235, 156)
                        e.Cell.ForeColor = Drawing.Color.FromArgb(207, 148, 0)
                        e.Cell.Font.Size = New FontUnit(9)
                        e.Cell.Font.Name = "Calibri"
                        e.Cell.Font.Bold = True
                        e.Cell.HorizontalAlign = HorizontalAlign.Center
                    Case "MEER INFO NODIG"
                        e.Cell.BackColor = Drawing.Color.FromArgb(255, 235, 156)
                        e.Cell.ForeColor = Drawing.Color.FromArgb(207, 148, 0)
                        e.Cell.Font.Size = New FontUnit(9)
                        e.Cell.Font.Name = "Calibri"
                        e.Cell.Font.Bold = True
                        e.Cell.HorizontalAlign = HorizontalAlign.Center
                    Case "REJECT"
                        e.Cell.BackColor = Drawing.Color.FromArgb(255, 204, 204)
                        e.Cell.ForeColor = Drawing.Color.FromArgb(153, 0, 0)
                        e.Cell.Font.Size = New FontUnit(9)
                        e.Cell.Font.Name = "Calibri"
                        e.Cell.Font.Bold = True
                        e.Cell.HorizontalAlign = HorizontalAlign.Center
                End Select

            End If
        End If


    End Sub

    Private Sub ASPxMenu1_ItemClick(source As Object, e As MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "Meetstaatinladen" Then
            Session("importnode") = grid.GetRowValues(grid.FocusedRowIndex, "Node")
            Session("importtype") = "Meetstaten"
            Response.Redirect("~/ImportExcelCopyPaste.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        ElseIf e.Item.Name = "Meetstaatweergeven" Then
            Session("importnode") = grid.GetRowValues(grid.FocusedRowIndex, "Node")
            Response.Redirect("~/Beheer/NodeMeetstaat.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub

    Private Sub grid_BatchUpdate(sender As Object, e As Data.ASPxDataBatchUpdateEventArgs) Handles grid.BatchUpdate
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            For Each args In e.UpdateValues
                Try
                    Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Node {0} statuslijst gewijzigd door {1} {2} {3}", args.NewValues("Node"), Session("userid"), Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                Catch ex As Exception

                End Try
            Next


        End Using
    End Sub
End Class