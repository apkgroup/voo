﻿Imports System.Data.SqlClient

Public Class PloegenWerknemers
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Protected Sub cboPloeg_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPloeg.SelectedIndexChanged
        If cboPloeg.SelectedIndex > -1 Then
            grid.Visible = True
        End If

    End Sub

    Private Sub ASPxGridView1_RowInserted(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertedEventArgs) Handles grid.RowInserted
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
            Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Ploeg {0} werknemer {1} toegevoegd door {2} {3}", e.NewValues("Ploeg"), e.NewValues("Werknemer"), Session("naam"), Session("voornaam"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Private Sub grid_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles grid.RowInserting
        e.NewValues("Actief") = True
        e.NewValues("Ploeg") = cboPloeg.Value
    End Sub

    Private Sub ASPxGridView1_RowUpdated(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatedEventArgs) Handles grid.RowUpdated
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
            Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Ploeg {0} werknemer {1} bijgewerkt door {2} {3}", e.NewValues("Ploeg"), e.NewValues("Werknemer"), Session("naam"), Session("voornaam"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteNonQuery()
        End Using
    End Sub
End Class