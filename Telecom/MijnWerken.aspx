﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="MijnWerken.aspx.vb" Inherits="Telecom.MijnWerken" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Opérations</h2>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
        <SettingsPager NumericButtonCount="25" PageSize="25">
        </SettingsPager>
        <Settings ShowFilterRow="True" ShowHeaderFilterButton="True" />
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataDateColumn FieldName="Tijd" VisibleIndex="2" Caption="Temps">
                <PropertiesDateEdit DisplayFormatString="dd/MM/yyy HH:mm">
                </PropertiesDateEdit>
                <Settings AllowHeaderFilter="False" ShowInFilterControl="False" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="WerkNr" VisibleIndex="1" Caption="Numéro d'opération">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataHyperLinkColumn FieldName="id" VisibleIndex="3" Caption="Details">
            <PropertiesHyperLinkEdit NavigateUrlFormatString="../Werken.aspx?id={0}" Text="Details" TextFormatString="">
            </PropertiesHyperLinkEdit>
                <Settings AllowHeaderFilter="False" ShowInFilterControl="False" />
            <EditFormSettings Visible="False" />
        </dx:GridViewDataHyperLinkColumn>
            <dx:GridViewDataTextColumn FieldName="Werknemer" Visible="False" VisibleIndex="0" Caption="Emp">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Code Postal" FieldName="Postcode" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT  [Werk].[id],

(
SELECT 
CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam
ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)
END
) as Werknemer,

 [Werk].[Tijd],  [Werk].[KlantNr], [Werk].[WerkNr], [Werk].[Postcode] FROM [Werk] 
inner join gebruikers on [Werk].GebruikerId = gebruikers.id
WHERE Gebruikers.opdrachtgeverId = @opdrachtgeverId
ORDER BY [Werk].[Tijd] DESC">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT  [Werk].[id], W.Werknemer, [Werk].[Tijd],  [Werk].[KlantNr], [Werk].[WerkNr], [Werk].[Postcode] FROM [Werk] 
inner join gebruikers on [Werk].GebruikerId = gebruikers.id
INNER JOIN (SELECT [ON], NR, NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN) AS W ON Gebruikers.Werkg = W.[ON] AND Gebruikers.Werkn = W.NR
WHERE ([GebruikerId] = @GebruikerId) AND ([Tijd] &gt;= DATEADD(day,-7, GETDATE()))
ORDER BY [Werk].[Tijd] DESC">
        <SelectParameters>
            <asp:SessionParameter Name="GebruikerId" SessionField="userid" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
