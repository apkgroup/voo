'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class minmax
    Public Property id As Integer
    Public Property magazijnId As Nullable(Of Integer)
    Public Property materiaalId As Nullable(Of Integer)
    Public Property min As Nullable(Of Decimal)
    Public Property max As Nullable(Of Decimal)

    Public Overridable Property Basismateriaal As Basismateriaal
    Public Overridable Property Magazijn As Magazijn

End Class
