﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Ploegen.aspx.vb" Inherits="Telecom.Ploegen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <h2>Beheer ploegen</h2>   
    <br />
      <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourcePloegen" KeyFieldName="PloegNr">
            <SettingsText EmptyDataRow="Geen data gevonden... " />
          <Columns>
              <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
              </dx:GridViewCommandColumn>
              <dx:GridViewDataTextColumn FieldName="PloegNr" VisibleIndex="1">
                  <HeaderStyle HorizontalAlign="Right" />
                  <CellStyle HorizontalAlign="Right">
                  </CellStyle>
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn FieldName="Naam" VisibleIndex="2">
              </dx:GridViewDataTextColumn>
              <dx:GridViewDataCheckColumn FieldName="Actief" VisibleIndex="3">
                  <HeaderStyle HorizontalAlign="Center" />
              </dx:GridViewDataCheckColumn>
          </Columns>
           <SettingsPager PageSize="20">
            <PageSizeItemSettings Caption="Items per pagina:" Items="10, 20, 50" ShowAllItem="True" Visible="True">
            </PageSizeItemSettings>
        </SettingsPager>
          <SettingsDataSecurity AllowDelete="False" />
            <SettingsCommandButton>
             <NewButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Nieuw" Url="~/images/Add_32x32.png" Width="16px">
                 </Image>
             </NewButton>
             <UpdateButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewaren" Url="~/images/Save_16x16.png" Width="16px">
                 </Image>
             </UpdateButton>
             <CancelButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Annuleren" Url="~/images/annuleren.png" Width="16px">
                 </Image>
             </CancelButton>
             <EditButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewerken" Url="~/images/Edit.png" Width="16px">
                 </Image>
             </EditButton>
             <DeleteButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Verwijderen" Url="~/images/Folders-OS-Recycle-Bin-Full-Metro-icon.png" Width="16px">
                 </Image>
             </DeleteButton>
         </SettingsCommandButton>
      </dx:ASPxGridView>
      <asp:SqlDataSource ID="SqlDataSourcePloegen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Ploegen] WHERE [PloegNr] = @PloegNr" InsertCommand="INSERT INTO [Ploegen] ([PloegNr], [Naam], [Actief]) VALUES (@PloegNr, @Naam, @Actief)" SelectCommand="SELECT [PloegNr], [Naam], [Actief] FROM [Ploegen]" UpdateCommand="UPDATE [Ploegen] SET [Naam] = @Naam, [Actief] = @Actief WHERE [PloegNr] = @PloegNr">
          <DeleteParameters>
              <asp:Parameter Name="PloegNr" Type="Int32" />
          </DeleteParameters>
          <InsertParameters>
              <asp:Parameter Name="PloegNr" Type="Int32" />
              <asp:Parameter Name="Naam" Type="String" />
              <asp:Parameter Name="Actief" Type="Boolean" />
          </InsertParameters>
          <UpdateParameters>
              <asp:Parameter Name="Naam" Type="String" />
              <asp:Parameter Name="Actief" Type="Boolean" />
              <asp:Parameter Name="PloegNr" Type="Int32" />
          </UpdateParameters>
      </asp:SqlDataSource>
    <br />

</asp:Content>
