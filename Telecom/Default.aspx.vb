﻿
Imports System.Threading
Imports DevExpress.Web
Imports System.Data.SqlClient

Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("taal") = 1 Then
            Literal2.Text = "Nieuws"
        Else
            Literal2.Text = "Nouvelles"
        End If
        If Not Page.IsCallback Then
            If Not Session("isingelogd") Then

                Response.Redirect("~/Login.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            End If
        End If
        Dim aantalOngelezen As Integer
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand("select count(id) as aantal from gelezen where gebruikerId = @gebruikerid and gelezen = 0", cn)
            Dim par As New SqlParameter("@gebruikerId", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                aantalOngelezen = dr.GetInt32(0)
            End If
            dr.Close()
        End Using
        popup3.ShowOnPageLoad = False
        If aantalOngelezen > 0 Then
            'popup3.ShowOnPageLoad = True
        End If


    End Sub

    Protected Sub ASPxCallback1_Callback(ByVal sender As Object, ByVal e As CallbackEventArgs)
        Dim newsID As Integer = Integer.Parse(e.Parameter)
        Dim text As String = ""
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand("SELECT Detail FROM Nieuws WHERE (id=@id)", cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = newsID}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                text = dr.GetString(0)
            End If
            dr.Close()
        End Using
        e.Result = text
    End Sub


    Protected Sub ASPxButtonGelezen_Click(sender As Object, e As EventArgs) Handles ASPxButtonGelezen.Click
        popup3.ShowOnPageLoad = False
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("UPDATE Gelezen SET gelezen=1 WHERE (gebruikerId={0})", Session("userid")), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub
End Class