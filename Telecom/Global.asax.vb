Imports System.Web.SessionState
Imports DevExpress.Web
Imports System.Data.SqlClient

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        AddHandler DevExpress.Web.ASPxWebControl.CallbackError, AddressOf Application_Error
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started


        Session("isingelogd") = False
        Session("userid") = 0
        Session("werkg") = ""
        Session("opdrachtgever") = 0
        Session("werkn") = 0
        Session("naam") = ""
        Session("voornaam") = ""
        Session("level") = 9
        Session("email") = ""
        Session("gsm") = ""
        Session("isadmin") = False
        Session("beginvandeweek") = DateSerial(2015, 1, 1)
        Session("mailaccount") = ""
        Session("selwerkg") = ""
        Session("selwerkn") = 0
        Session("selgebruiker") = 0
        Session("selnaam") = ""
        Session("selvoornaam") = ""
        Session("TAG") = ""
        Session("selgsm") = ""
        Session("selemail") = ""
        Session("selvan") = DateSerial(2015, 1, 1)
        Session("seltot") = DateSerial(2015, 1, 1)
        Session("selperiode") = ""
        Session("techverschil") = 0
        Session("magverschil") = 0
        Session("eerstelogin") = False
        Session("tmplevering") = 0
        Session("magazijn") = 0
        Session("magazijnnaam") = ""
        Session("magazijnier") = 0
        Session("pickinglijst") = 0
        Session("url") = ""
        Session("taal") = 1
        Session("leverancier") = 0
        Session("bestelbonid") = 0
        Session("stocktellingbegin") = DateSerial(2015, 1, 1)
        Session("stocktellingeinde") = DateSerial(2015, 1, 1)
        Session("afdrukrapport") = ""
        Session("StocktellingID") = 0
        Session("stocktellingsucces") = False
        Session("nietladen") = False
        Session("foutmelden") = False
        Session("startplanning") = DateSerial(2015, 1, 1)
        Session("leveringsbonnummer") = ""
        Session("nodeplanning") = 0
        Session("nodenaamplanning") = ""
        Session("planningrichting") = 0
        Session("nodefilter") = ""
        Session("verlofgebruiker") = 0
        Session("controlelijst") = 0
        Session("uitleenid") = 0
        Session("ploegfilter") = 0
        Session("stockfilter") = ""
        Session("detailstockfilter") = ""
        Session("pickinggereedschap") = False
        Session("wachtbegin") = DateSerial(2015, 1, 1)
        Session("wachteinde") = DateSerial(2015, 1, 1)
        Session("grafiekartikel") = 0
        Session("grafiekdagen") = 15
        Session("grafiekhoeveelheden") = 1
        Session("grafiektechnieker") = 0
        Session("stocktransactie") = True
        Session("importtype") = ""
        Session("importnode") = ""
        Session("laatsteplanningjaar") = 0
        Session("sitetitel") = "WMS"
        Session("globalize") = "fr-BE"
        Session("Tijd") = DateTime.Now.ToString("yyyy-MM-dd")
        Session.Timeout = 180

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "SELECT Naam, Waarde FROM StandaardWaarden WHERE (Laden=1)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                Do While dr.Read
                    If Not dr.IsDBNull(1) Then
                        Session(dr.GetString(0)) = dr.GetString(1)
                    End If

                Loop
            End If
            dr.Close()
            s_SQL = "SELECT Mailaccount FROM Mailaccount"
            cmd = New SqlCommand(s_SQL, cn)
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not dr.IsDBNull(0) Then
                    Session("mailaccount") = dr.GetString(0)
                End If
            End If
        End Using
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)

    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        'Dim objErr As Exception = Server.GetLastError().GetBaseException()
        'Dim VooEntities As New VooEntities
        'If Not objErr.Message.Contains("A potentially dangerous Request.") Then
        '    Server.ClearError()

        '    Response.Clear()
        '    Session("LastErrorMsg") = objErr.Message
        '    Session("LastErrorStack") = objErr.StackTrace
        '    Dim voolog As New Log
        '    voolog.type = "Error"
        '    voolog.Tijdstip = DateTime.Now
        '    voolog.Actie = objErr.Message & ": " & objErr.StackTrace
        '    voolog.Gebruiker = Session("userid").ToString
        '    VooEntities.Log.Add(voolog)
        '    HttpContext.Current.Response.Redirect("~/WMSerror.aspx", False)
        'End If


    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
 
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class