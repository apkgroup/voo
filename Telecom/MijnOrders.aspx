﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="MijnOrders.aspx.vb" Inherits="Telecom.MijnOrders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-family: "Segoe UI";
            font-size: 12px;
            background-color: #F0F0F0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
    <br />
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceBestellingen" KeyFieldName="id">
        <Settings ShowHeaderFilterButton="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Werknemer" ReadOnly="True" VisibleIndex="0" ShowInCustomizationForm="True" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="beschrijving" VisibleIndex="1" Caption="Statut" ShowInCustomizationForm="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="Datum" VisibleIndex="2" ShowInCustomizationForm="True">
                <Settings AllowHeaderFilter="False" ShowInFilterControl="False" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataHyperLinkColumn FieldName="id" VisibleIndex="3">
            <PropertiesHyperLinkEdit NavigateUrlFormatString="../Bestelling.aspx?id={0}" Text="détails" TextFormatString="" >
            </PropertiesHyperLinkEdit>
                <Settings AllowHeaderFilter="False" ShowInFilterControl="False" />
            <EditFormSettings Visible="False" />
        </dx:GridViewDataHyperLinkColumn>
        </Columns>
    </dx:ASPxGridView>
      <asp:SqlDataSource ID="SqlDataSourceBestellingen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT (
SELECT 
CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam
ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)
END
) as Werknemer, 
CASE 
WHEN [Bestellingstatus].beschrijving = 'demandé' AND @taal =1 THEN 'Aangevraagd'
WHEN [Bestellingstatus].beschrijving = 'demandé' AND @taal =2 THEN 'Demandé'
WHEN [Bestellingstatus].beschrijving = 'approuvé' AND @taal =1 THEN 'Goedgekeurd'
WHEN [Bestellingstatus].beschrijving = 'approuvé' AND @taal =2 THEN 'Approuvé'
WHEN [Bestellingstatus].beschrijving = 'rejeté' AND @taal =1 THEN 'Afgekeurd'
WHEN [Bestellingstatus].beschrijving = 'rejeté' AND @taal =2 THEN 'Rejeté'
END as beschrijving,

 [Datum], [Bestellingen].[id] FROM [Bestellingen] INNER JOIN 
[Bestellingstatus] on [Bestellingstatus].id = [Bestellingen].status INNER JOIN
[Gebruikers] on [Bestellingen].GebruikerId = [Gebruikers].id
Where [Bestellingen].GebruikerId  = @GebruikerId
order by datum desc">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="1" Name="taal" SessionField="taal" />
            <asp:SessionParameter Name="GebruikerId" SessionField="userId" />
        </SelectParameters>
    </asp:SqlDataSource>
    </asp:Content>
