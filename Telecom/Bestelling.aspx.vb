﻿Imports System.Data.SqlClient
Imports DevExpress.Web
Imports Microsoft.Exchange.WebServices.Data
Imports System.IO
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.Drawing
Imports DevExpress.Export
Imports System.Net

Public Class Bestelling


    Inherits System.Web.UI.Page
    Private headerImage As Image
    Dim vorige As Integer
    Dim volgende As Integer

    Protected json As New System.Web.Script.Serialization.JavaScriptSerializer
    Protected jsvars As New Hashtable

    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property




    Protected Sub vertaal(taalid)
        If taalid = 1 Then

            ASPxGridView1.Columns("Article").Caption = "Artikel"
            ASPxGridView1.Columns("Description").Caption = "Omschrijving"
            ASPxGridView1.Columns("hoeveelheid").Caption = "Aantal"


            Labelexport.Text = "Opgelet! Bij het klikken op exporteren moet u pop-ups in uw browser toelaten"
            ASPxButton3.Text = "Exporteren"
            ASPxButton1.Text = "Picken en overdragen"
            ASPxButton2.Text = "Afkeuren"

        Else
            ASPxGridView1.Columns("Werknemer").Caption = "Empl."
            ASPxGridView1.Columns("beschrijving").Caption = "Statut"
            ASPxGridView1.Columns("Datum").Caption = "Date"
            Labelexport.Text = "Attention! En cliquant sur Exporter vous devez autoriser les pop ups en haut de votre navigateur"


        End If
    End Sub



    Protected Sub SqlDataSourceGrabbel_Selected(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SqlDataSourceGrabbel.Selected
        If e.AffectedRows.ToString() = 0 Then
            ASPxGridView3.ClientVisible = False
        End If
    End Sub

    Protected Sub SqlDataSourceBestellijnSerienummers_Selected(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SqlDataSourceBestellijnSerienummers.Selected
        If e.AffectedRows.ToString() = 0 Then
            ASPxGridView2.ClientVisible = False
        End If
    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim vooentities As New VooEntities
        Dim userid As Integer = Convert.ToInt32(Session("userid"))
        Dim bestid As Integer = Convert.ToInt32(Request.QueryString("id"))
        Dim allowed As Boolean = False
        Dim opdrbestelling As Integer = Convert.ToInt32(vooentities.Bestellingen.Find(bestid).Gebruikers.OpdrachtgeverId)
        If Not Session("isadmin") Then
            For Each opdr In vooentities.AdminOpdrachtgevers.Where(Function(x) x.gebruikerId = userid)
                If opdr.opdrachtgeverId = opdrbestelling Then
                    allowed = True
                End If
            Next
            If Not allowed Then
                ' Response.Redirect("~/Login.aspx", False)
                ' Context.ApplicationInstance.CompleteRequest()
            End If
            Session("opdrachtgever") = vooentities.Bestellingen.Find(bestid).Gebruikers.OpdrachtgeverId
        End If


    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        Dim serienrs As New Dictionary(Of String, String)



        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If






        Initialiseer()



        If Session("isadmin") And Not Session("KLV") Then
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = "SELECT min(b.id) as 'vorige', max (b.id) as 'volgende' FROM [Bestellingen] b inner join Gebruikers g on b.GebruikerId = g.id WHERE g.opdrachtgeverId = @opdrachtgever and b.id <> @huidig and b.[Status] = 1"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@huidig", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@opdrachtgever", SqlDbType.Int) With {.Value = CInt(Session("opdrachtgever"))}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader

                If dr.HasRows Then
                    dr.Read()
                    vorige = IIf(IsDBNull(dr.GetValue(0)), 0, dr.GetValue(0))
                    volgende = IIf(IsDBNull(dr.GetValue(1)), 0, dr.GetValue(1))
                End If
                cn.Close()

            End Using

            If volgende = 0 Then
                ASPxButtonVorige.Visible = False
                ASPxButtonVolgende.Visible = False
            Else
                If volgende < Request.QueryString("id") Then
                    ASPxButtonVolgende.Visible = False
                    vorige = volgende
                ElseIf vorige > Request.QueryString("id") Then
                    ASPxButtonVorige.Visible = False
                    volgende = vorige
                End If
            End If




        ElseIf Session("KLV") And Session("TAG") <> "SYN" Then
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = "SELECT min(b.id) as 'vorige', max (b.id) as 'volgende' FROM [Bestellingen] b inner join Gebruikers g on b.GebruikerId = g.id WHERE g.opdrachtgeverId = @opdrachtgever and b.id <> @huidig and b.[Status] = 1"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@huidig", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@opdrachtgever", SqlDbType.Int) With {.Value = CInt(Session("opdrachtgever"))}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader

                If dr.HasRows Then
                    dr.Read()
                    vorige = IIf(IsDBNull(dr.GetValue(0)), 0, dr.GetValue(0))
                    volgende = IIf(IsDBNull(dr.GetValue(1)), 0, dr.GetValue(1))
                End If
                cn.Close()

            End Using

            If volgende = 0 Then
                ASPxButtonVorige.Visible = False
                ASPxButtonVolgende.Visible = False
            Else
                If volgende < Request.QueryString("id") Then
                    ASPxButtonVolgende.Visible = False
                    vorige = volgende
                ElseIf vorige > Request.QueryString("id") Then
                    ASPxButtonVorige.Visible = False
                    volgende = vorige
                End If
            End If
        Else


            ASPxPanel2.Visible = False
            ASPxPanel1.Visible = False
            ASPxPanelSerie.Visible = False
            ASPxGridView1.DataColumns.Item("hoeveelheid").EditFormSettings.Visible = 1
            ASPxGridView3.DataColumns.Item("hoeveelheid").EditFormSettings.Visible = 1
            ASPxTextBoxReden.Visible = False
            ASPxButton6.Visible = False
            ASPxButton4.Visible = False


        End If

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

            cn.Open()
            Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
            cmd.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()
                Session("MagazijnId") = dr2.GetInt32(0)
            End If
            dr2.Close()
            cn.Close()
        End Using

        Dim entities As New VooEntities
        Dim bestid As Integer = CInt(Request.QueryString("id"))
        ASPxTextBoxReden.Value = entities.Bestellingen.Find(bestid).RedenAfkeuring
        Dim magazijnid As Integer = Session("MagazijnId")
        If entities.Serienummer.Where(Function(x) x.StockMagazijn.MagazijnId = magazijnid And x.boxNo IsNot Nothing).Any Then
            ASPxButton4.ClientVisible = True
        Else
            ASPxButton4.ClientVisible = False
        End If

        If entities.Serienummer.Where(Function(x) x.StockMagazijn.MagazijnId = magazijnid And x.palletNo IsNot Nothing).Any Then
            ASPxButtonPallet.ClientVisible = True
        Else
            ASPxButtonPallet.ClientVisible = False
        End If




    End Sub
    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender
        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijnDoel_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijnDoel.PreRender
        Dim entities As New VooEntities
        Dim bestid As Integer = CInt(Request.QueryString("id"))

        If ASPxComboBoxMagazijnDoel.SelectedIndex = -1 Then
            Dim magid As String = entities.Bestellingen.Find(bestid).Gebruikers.MagazijnId
            ASPxComboBoxMagazijnDoel.SelectedItem = ASPxComboBoxMagazijnDoel.Items.FindByValue(magid)
        End If


    End Sub




    Protected Sub Initialiseer()
        vulserie()
        vulhaspel()
        Dim status As Integer
        Dim statusText As String
        LabelFoutmelding.Text = Session("Bestellingfout")
        ASPxLabelGoed.Text = Session("BestellingGelukt")
        Session("Bestellingfout") = ""
        Session("BestellingGelukt") = ""

        If Not String.IsNullOrEmpty(Request.QueryString("id")) Then






            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = "SELECT (SELECT CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn) END) as Naam, Status, deadline, opmerking FROM Bestellingen Inner join Gebruikers on bestellingen.GebruikerId = Gebruikers.id where Bestellingen.id = @id"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader
                If dr.HasRows Then
                    dr.Read()


                    status = dr.GetInt32(1)

                    If Session("taal") = 1 Then
                        Select Case status
                            Case 1
                                statusText = "Aangevraagd"
                            Case 3
                                statusText = "Afgekeurd"
                            Case 2
                                statusText = "Goedgekeurd"
                            Case 4
                                statusText = "Gereserveerd"
                        End Select
                        Literal2.Text = "Bestelling " & Request.QueryString("id") & " van " + dr.GetString(0) + ": " + statusText
                        ASPxGridView1.SettingsText.Title = "Bestelling: " & Request.QueryString("id") & " " & dr.GetString(0)
                        ASPxMemoOpmerking.Value = IIf(IsDBNull(dr.GetValue(3)), "", dr.GetValue(3))
                        ASPxDateEditDeadline.Value = IIf(IsDBNull(dr.GetValue(2)), "", dr.GetValue(2))
                    Else
                        Select Case status
                            Case 1
                                statusText = "demandé"
                            Case 3
                                statusText = "rejeté"
                            Case 2
                                statusText = "approuvé"
                        End Select
                        Literal2.Text = "Commande " & Request.QueryString("id") & " de " + dr.GetString(0) + ": " + statusText
                        ASPxGridView1.SettingsText.Title = "Commande: " & Request.QueryString("id") & " " & dr.GetString(0)

                    End If




                End If
                dr.Close()
            End Using

            If status = 4 Or status = 1 Then
                ASPxPanel1.Visible = True
                ASPxPanelSerie.Visible = True
                ASPxComboBoxMagazijnDoel.Visible = True
                ASPxLabelDoel.Visible = True

            Else
                ASPxPanel1.Visible = False
                ASPxPanelSerie.Visible = False
                ASPxComboBoxMagazijnDoel.Visible = False
                ASPxLabelDoel.Visible = False
                ASPxGridView1.DataColumns.Item("hoeveelheid").EditFormSettings.Visible = 1

            End If

            If status = 4 Then
                ASPxButton2.ClientVisible = False
                ASPxButton1.ClientVisible = False
                ASPxButton7.ClientVisible = False
                ASPxPanelSerie.Visible = False
                ASPxButtonReservatieGoedkeuren.ClientVisible = True
            Else
                ASPxButtonReservatieGoedkeuren.ClientVisible = False
            End If

            If Not Session("isadmin") Then
                If status = 1 Then
                    PlaceHolderHaspel.Visible = False
                End If
            End If
        End If


    End Sub
    Protected Sub ASPxButton2_Click(sender As Object, e As EventArgs) Handles ASPxButton2.Click
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "UPDATE [Bestellingen] SET [Status] = 3  WHERE ([Bestellingen].[id] = @bestellingId)"

            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteReader()
            Dim voocontext As New VooEntities

        End Using
        Dim username As String
        Dim userid As String
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim s_SQL As String = "SELECT(SELECT CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)END) as Naam, [gebruikers].id FROM [Bestellingen] inner join [Gebruikers] on [Bestellingen].[GebruikerId] = [Gebruikers].[id] where [Bestellingen].id = @bestellingId"

            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                username = dr.GetValue(0)
                userid = dr.GetValue(1)
            End If
            dr.Close()
        End Using

        'Verwittigen(username, userid, "rejeté")

        Session("BestellingGelukt") = "Bestelling Afgekeurd"
        Response.Redirect("~/Bestelling.aspx?id=" + Request.QueryString("id"))
        Context.ApplicationInstance.CompleteRequest()



    End Sub




    Protected Sub ASPxGridView1_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles ASPxGridView1.CustomCallback

        Dim s As ASPxGridView = sender

        If e.Parameters = "CEXP" Then

        End If

    End Sub

    Private Sub Header_CreateDetailHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        e.Graph.BorderWidth = 0
        Dim r As New Rectangle(300, 0, headerImage.Width, headerImage.Height)
        e.Graph.DrawImage(headerImage, r)
        'r = New Rectangle(0, headerImage.Height, 200, 50)
        'e.Graph.DrawString("Additional Header information here....", r)
    End Sub
    Private Sub Link1_CreateReportFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        e.Graph.BorderWidth = 0
        Dim r As New Rectangle(1, 20, 250, 50)
        e.Graph.Font = New Font("Arial", 12, FontStyle.Bold)
        e.Graph.ForeColor = Color.DimGray
        e.Graph.DrawString("Handtekening magazijnier", r)

        r = New Rectangle(350, 20, 250, 50)
        e.Graph.Font = New Font("Arial", 12, FontStyle.Bold)
        e.Graph.ForeColor = Color.DimGray
        e.Graph.DrawString("Handtekening technieker", r)

        Dim rF As New RectangleF(1, 75, 250, 100)

        e.Graph.DrawRect(rF, BorderSide.All, Color.Transparent, Color.DimGray).BorderWidth = 1
        rF = New RectangleF(350, 75, 250, 100)
        e.Graph.DrawRect(rF, BorderSide.All, Color.Transparent, Color.DimGray).BorderWidth = 1

        r = New Rectangle(350, 200, 250, 50)
        e.Graph.Font = New Font("Arial", 12, FontStyle.Bold)
        e.Graph.ForeColor = Color.DimGray
        e.Graph.DrawString("Datum: " & Today.ToShortDateString, r)

    End Sub

    Protected Sub ConfirmButton_Click(sender As Object, e As EventArgs)
        For Each ctr As System.Web.UI.Control In PlaceHolderSerie.Controls
            If TypeOf (ctr) Is ASPxTextBox Then
                Dim value As String = (CType(ctr, TextBox)).Text
            End If
        Next
    End Sub

    Private Sub Verwittigen(ByVal varGebruiker As String, ByVal Gebruikerid As Integer, ByVal Status As String)

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim varBericht As String = ""
            Dim varOnderwerp As String = ""
            Dim varBeheerder As String = String.Format("{0} {1}", Session("naam"), Session("voornaam"))
            Dim varEmail As String = ""
            Dim s_SQL As String = "SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Melding bestelling verwerkt" & Session("taal")}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not dr.IsDBNull(0) Then
                    varBericht = dr.GetString(0)
                    varOnderwerp = dr.GetString(1)
                End If
            End If
            dr.Close()
            s_SQL = "SELECT Email FROM Gebruikers WHERE (Gebruikers.id=@gebruiker)"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Gebruikerid}
            cmd.Parameters.Add(par)
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                varEmail = dr.GetString(0)
            End If
            dr.Close()
            Dim adressen As New List(Of String)
            adressen.Add(varEmail)

            Dim varBody As String = varBericht.Replace("[WERKNEMER]", varGebruiker)
            varBody = varBody.Replace("[STATUS]", Status)
            varBody = varBody.Replace("[BEHEERDER]", varBeheerder)
            varBody = varBody.Replace("[URL]", String.Format("{0}/MijnOrders.aspx", Session("domein")))
            varBody = varBody.Replace("[SITETITEL]", "APK WMS")
            Dim sBody As String = "<x-html>" & vbCrLf
            sBody = sBody & varBody
            sBody = sBody & "</x-html>"

            mailGoogle(varOnderwerp, "noreply@apkgroup.eu", adressen, sBody, New Dictionary(Of String, Byte()))


        End Using
    End Sub



    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating


        Dim editingRowVisibleIndex As Integer = ASPxGridView1.FindVisibleIndexByKeyValue(e.Keys.Values(0))
        Session("bestelling") = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Poste")









    End Sub


    Protected Sub ASPxGridView1_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBinding
        DoSelect(SqlDataSource1.ConnectionString)


    End Sub



    Protected Sub ASPxGridView4_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("bestellijnId") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub

    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [bestellijnId] from [RetourBestellijnSerienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("bestellijnId"))
        Next row
        Session("SelectResult") = result
    End Sub

    Protected Sub ASPxGridView1_RowUpdated(sender As Object, e As Data.ASPxDataUpdatedEventArgs) Handles ASPxGridView1.RowUpdated

        Session("isAangepast") = True

    End Sub

    Protected Sub ASPxButtonVorige_Click(sender As Object, e As EventArgs) Handles ASPxButtonVorige.Click
        Response.Redirect("~/Bestelling.aspx?id=" & vorige, False)
    End Sub

    Protected Sub ASPxButtonVolgende_Click(sender As Object, e As EventArgs) Handles ASPxButtonVolgende.Click
        Response.Redirect("~/Bestelling.aspx?id=" & volgende, False)
    End Sub

    Private Sub vulserie()

        Dim voocontext As New VooEntities
        Dim bestellingId As Integer = Request.QueryString("id")
        Dim bestelling = voocontext.Bestellingen.Find(bestellingId)
        Dim collection As New List(Of String)

        For Each bestellijn In bestelling.Bestellijn
            If bestellijn.Basismateriaal.HeeftSerienummer And bestellijn.hoeveelheid > 0 Then
                Dim label As New ASPxLabel
                If Session("taal") = 1 Then
                    label.Text = "Serienummers " & bestellijn.Basismateriaal.Description

                Else
                    label.Text = "Numéros de série " & bestellijn.Basismateriaal.Description

                End If
                PlaceHolderSerie.Controls.Add(label)
                For i = 1 To bestellijn.hoeveelheid


                    Dim txt = New TextBox With {.ID = Today.Date.ToShortDateString & "_" & bestellijn.materiaalId & "_" & i}
                    TextBoxIdCollection.Add(txt.ID)
                    txt.Width = "300"
                    'If TextBoxIdCollection.Where(Function(x) x.Key = txt.ID).Any Then
                    '    txt.Text = TextBoxIdCollection.Where(Function(x) x.Key = txt.ID).FirstOrDefault.Value
                    'End If

                    PlaceHolderSerie.Controls.Add(txt)
                    Dim br As New HtmlGenericControl("br")
                    PlaceHolderSerie.Controls.Add(br)

                Next
            End If
        Next


    End Sub

    Private Sub vulhaspel()

        Dim voocontext As New VooEntities
        Dim bestellingId As Integer = Request.QueryString("id")
        Dim bestelling = voocontext.Bestellingen.Find(bestellingId)
        Dim collection As New List(Of String)

        For Each bestellijn In bestelling.Bestellijn
            If bestellijn.Basismateriaal.bobijnArtikel And bestellijn.hoeveelheid > 0 Then
                Dim label As New ASPxLabel
                If Session("taal") = 1 Then
                    label.Text = "Bobijnnummer voor materiaal " & bestellijn.Basismateriaal.Description

                Else
                    label.Text = "Numéro de bobin " & bestellijn.Basismateriaal.Description

                End If
                PlaceHolderHaspel.Controls.Add(label)



                Dim txt = New TextBox With {.ID = "Haspel_" & Today.Date.ToShortDateString & "_" & bestellijn.materiaalId}
                If bestelling.Status = "2" Then
                    txt.Text = bestellijn.bobijnnummer
                    txt.ReadOnly = True
                End If
                TextBoxIdCollection.Add(txt.ID)
                txt.Width = "300"
                'If TextBoxIdCollection.Where(Function(x) x.Key = txt.ID).Any Then
                '    txt.Text = TextBoxIdCollection.Where(Function(x) x.Key = txt.ID).FirstOrDefault.Value
                'End If

                PlaceHolderHaspel.Controls.Add(txt)
                Dim br As New HtmlGenericControl("br")
                PlaceHolderHaspel.Controls.Add(br)


            End If
        Next


    End Sub

    Protected Sub ASPxGridView1_RowValidating(sender As Object, e As Data.ASPxDataValidationEventArgs) Handles ASPxGridView1.RowValidating

    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxButton1.Click
        Dim username As String
        Dim userid As String
        Dim Voocontext As New VooEntities
        Dim bestellingid As Integer = Request.QueryString("id")

        LabelFoutmelding.Text = ""
        For Each ctr As System.Web.UI.Control In PlaceHolderSerie.Controls
            If TypeOf (ctr) Is TextBox Then
                Dim id As String = (CType(ctr, TextBox)).ID
                Dim value As String = (CType(ctr, TextBox)).Text


            End If
        Next






        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim s_SQL As String = "SELECT (SELECT CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn) END) as Naam, gebruikerId FROM Bestellingen Inner join Gebruikers on bestellingen.GebruikerId = Gebruikers.id where Bestellingen.id = @bestellingId"

            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                username = dr.GetValue(0)
                userid = dr.GetValue(1)
            End If
            dr.Close()
        End Using

        Dim lijst = Voocontext.Bestellijn.Where(Function(x) x.bestellingId = bestellingid).ToList
        Dim intuserId As Integer = CInt(userid)
        'Neem magazijn van overschreven waarde
        Dim magid As Integer = ASPxComboBoxMagazijnDoel.Value
        Dim opdrachtgeverId As Integer = Session("opdrachtgever")
        Dim hoofdmagId As Integer = Session("MagazijnId")
        Dim listSerieList As New Dictionary(Of String, List(Of Serienummer))
        Dim fouten As New List(Of String)
        Dim stockmat As StockMagazijn
        Dim matidvorig As Integer = 0

        For Each ctr As System.Web.UI.Control In PlaceHolderHaspel.Controls
            If TypeOf (ctr) Is TextBox Then
                Dim id As String = (CType(ctr, TextBox)).ID
                Dim value As String = (CType(ctr, TextBox)).Text
                Dim parts As String() = id.Split(New Char() {"_"c})
                Dim matId As Integer = Convert.ToInt32(parts(2))

                lijst.Where(Function(x) x.materiaalId = matId).FirstOrDefault.bobijnnummer = value
            End If
        Next

        For Each ctr As System.Web.UI.Control In PlaceHolderSerie.Controls
            If TypeOf (ctr) Is TextBox Then
                Dim id As String = (CType(ctr, TextBox)).ID
                Dim value As String = (CType(ctr, TextBox)).Text
                Dim parts As String() = id.Split(New Char() {"_"c})
                Dim matId As Integer = Convert.ToInt32(parts(1))

                If matId <> matidvorig Then
                    stockmat = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
                End If

                If stockmat Is Nothing Then
                    'Tech heeft geen stockmat voor materiaal??
                    Dim stockmatNieuw As New StockMagazijn
                    stockmatNieuw.MagazijnId = magid
                    stockmatNieuw.MateriaalId = matId
                    stockmatNieuw.Aantal = 0
                    Voocontext.StockMagazijn.Add(stockmatNieuw)
                    Voocontext.SaveChanges()
                    stockmat = stockmatNieuw
                End If
            End If
        Next

        Dim stockmagazijnserienummer As New Dictionary(Of String, List(Of Serienummer))
        matidvorig = 0

        For Each ctr As System.Web.UI.Control In PlaceHolderSerie.Controls
            If TypeOf (ctr) Is TextBox Then
                Dim id As String = (CType(ctr, TextBox)).ID
                Dim value As String = (CType(ctr, TextBox)).Text
                Dim parts As String() = id.Split(New Char() {"_"c})
                Dim matId = Convert.ToInt32(parts(1))

                If String.IsNullOrWhiteSpace(value) Then
                    LabelFoutmelding.Visible = True
                    If Session("taal") = 1 Then

                        fouten.Add("1")
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer mag niet leeg zijn!<br/>"
                    Else
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Le champ ne peut pas être vide!<br/>"
                    End If
                    Exit Sub
                End If

                If matId <> matidvorig Then
                    stockmat = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
                End If
                matidvorig = matId


                If Voocontext.Basismateriaal.Where(Function(x) x.id = matId).FirstOrDefault.SerieVerplichtBijOpboeken Then
                    Dim serienummer As Serienummer = Voocontext.Serienummer.Where(Function(x) x.serienummer1 = value).FirstOrDefault
                    If IsNothing(serienummer) Then
                        'serienummer bestaat niet
                        LabelFoutmelding.Visible = True
                        fouten.Add("1")

                        If Session("taal") = 1 Then

                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer " & value & " bestaat niet. Gelieve na te kijken en opnieuw te proberen. Wijzigingen zijn niet doorgevoerd.<br/>"
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Le numéro de série " & value & " n'existe pas. Veuillez vérifier et réessayer. Les changements n'ont pas été mis en œuvre<br/>"
                        End If
                        'Exit Sub
                    End If
                    If Not serienummer Is Nothing AndAlso serienummer.StockMagazijn.MagazijnId <> hoofdmagId Then
                        'Serienummer staat niet op hoofdmagazijn
                        LabelFoutmelding.Visible = True
                        fouten.Add("1")
                        If Session("taal") = 1 Then
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer " & value & " behoort tot een ander magazijn. Gelieve na te kijken en opnieuw te proberen. Wijzigingen zijn niet doorgevoerd.<br/>"
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Le numéro de série " & value & " appartient à un autre magasin. Veuillez vérifier et réessayer. Les changements n'ont pas été mis en œuvre.<br/>"
                        End If
                        'Return
                    End If
                    If Not serienummer Is Nothing AndAlso (serienummer.statusId = 1 Or serienummer.statusId = 5 Or serienummer.statusId = 6 Or serienummer.statusId = 8) Then
                        'serienummer is gemarkeerd als kaput
                        fouten.Add("1")

                        LabelFoutmelding.Visible = True
                        If Session("taal") = 1 Then
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer " & value & " is gemarkeerd als defect, gereserveerd, retour of verbruikt.  Gelieve na te kijken en opnieuw te proberen. Wijzigingen zijn niet doorgevoerd.<br/>"
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Le numéro de série" & value & "sont marqués comme étant brisés. Veuillez vérifier et réessayer. Aucune modification n'a été apportée.<br/>"
                        End If
                        ' Return
                    End If

                    If Not serienummer Is Nothing AndAlso serienummer.StockMagazijn.MateriaalId <> matId Then
                        'serienummer verkeerd
                        fouten.Add("1")
                        LabelFoutmelding.Visible = True

                        If Session("taal") = 1 Then
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer " & value & " behoort tot een ander artikel. Gelieve na te kijken en opnieuw te proberen. Wijzigingen zijn niet doorgevoerd.<br/>"
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Le numéro de série " & value & " appartient à un autre article. Veuillez vérifier et réessayer. Les changements n'ont pas été mis en œuvre.<br/>"
                        End If
                        ' Return
                    End If


                    If Not serienummer Is Nothing AndAlso serienummer.StockMagazijn.MateriaalId = matId Then
                        If listSerieList.ContainsKey(serienummer.StockMagazijn.Basismateriaal.id) Then
                            listSerieList.Where(Function(x) x.Key = serienummer.StockMagazijn.Basismateriaal.id).FirstOrDefault.Value.Add(serienummer)
                        Else
                            listSerieList.Add(serienummer.StockMagazijn.Basismateriaal.id, New List(Of Serienummer))
                            listSerieList.Where(Function(x) x.Key = serienummer.StockMagazijn.Basismateriaal.id).FirstOrDefault.Value.Add(serienummer)
                        End If

                        If stockmagazijnserienummer.Where(Function(x) x.Key = stockmat.id).Any Then
                            stockmagazijnserienummer.Item(stockmat.id).Add(serienummer)
                        Else
                            stockmagazijnserienummer.Add(stockmat.id, New List(Of Serienummer))
                            stockmagazijnserienummer.Item(stockmat.id).Add(serienummer)
                        End If


                        LabelFoutmelding.Visible = False
                        Session("Bestellingfout") = ""
                        'geknipt
                    End If
                Else
                    If Voocontext.Serienummer.Where(Function(x) x.serienummer1 = value).Any Then
                        'Serienummer bestaat al ergens
                        LabelFoutmelding.Visible = True
                        fouten.Add("1")
                        If Session("taal") = 1 Then
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer " & value & " behoort tot een ander magazijn. Gelieve na te kijken en opnieuw te proberen. Wijzigingen zijn niet doorgevoerd.<br/>"
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Le numéro de série " & value & " appartient à un autre magasin. Veuillez vérifier et réessayer. Les changements n'ont pas été mis en œuvre.<br/>"
                        End If
                        'Return
                    End If
                    Dim serienummer As New Serienummer
                    serienummer.StockMagazijnId = stockmat.id
                    serienummer.serienummer1 = value
                    serienummer.uitgeboekt = 0
                    serienummer.statusId = 4
                    serienummer.datumOntvangst = Today()
                    Voocontext.Serienummer.Add(serienummer)
                    Dim log As New Log
                    log.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & value & " uitgegeven voor bestelling " & Request.QueryString("id")
                    log.Tijdstip = Today
                    log.Gebruiker = Session("userid")
                    Voocontext.Log.Add(log)
                End If


            End If
        Next

        If fouten.Count = 0 Then
            'Voocontext.SaveChanges()

        Else
            Session("Bestellingfout") = LabelFoutmelding.Text
            Session("BestellingGelukt") = ""
            LabelFoutmelding.Visible = True
            Return
        End If

        'listSerieList zijn lijsten van ingescande serienummers gegroepeerd op artikel
        'listShipmentList zijn lijsten van shipments gegroepeerd op artikel
        Dim listShipmentList As New Dictionary(Of String, List(Of Shipment))

        For Each combo In listSerieList

            For Each serie In combo.Value
                If Not serie.Shipment Is Nothing Then
                    If serie.statusId = 2 OrElse IsNothing(serie.FIFO) OrElse serie.FIFO = False OrElse serie.statusId = 4 Then
                        'Niet toevoegen aan lijst voor controle shipments
                    Else
                        If listShipmentList.ContainsKey(combo.Key) Then
                            If Not listShipmentList.Where(Function(x) x.Key = combo.Key).FirstOrDefault.Value.Contains(serie.Shipment) Then
                                listShipmentList.Where(Function(x) x.Key = combo.Key).FirstOrDefault.Value.Add(serie.Shipment)
                            End If

                        Else
                            listShipmentList.Add(combo.Key, New List(Of Shipment))
                            listShipmentList.Where(Function(x) x.Key = combo.Key).FirstOrDefault.Value.Add(serie.Shipment)
                        End If
                    End If
                End If
            Next

        Next

        For Each listShipmentPair In listShipmentList
            Dim AlleShipmentsMetActieveVoorArtikel = Voocontext.Shipment.Where(Function(x) x.Serienummer.Where(Function(y) y.statusId = 3 And y.StockMagazijn.Basismateriaal.id = listShipmentPair.Key And y.StockMagazijn.Magazijn.OpdrachtgeverId = opdrachtgeverId And y.FIFO IsNot Nothing And y.FIFO = True).Any And x.accepted = True)
            Dim OudsteShipment = AlleShipmentsMetActieveVoorArtikel.OrderBy(Function(x) x.datereceived).FirstOrDefault()
            Dim VoorlaatsteOudsteShipment = AlleShipmentsMetActieveVoorArtikel.Where(Function(x) x.id <> OudsteShipment.id).OrderBy(Function(x) x.datereceived).FirstOrDefault()
            Dim ArtikelId As Integer = Convert.ToInt32(listShipmentPair.Key)
            Dim Artikel = Voocontext.Basismateriaal.Find(ArtikelId)
            Dim listShipment = listShipmentPair.Value
            'For Each shipmnt In listShipment
            '    If shipmnt.vrijgegeven Or shipmnt.datereceived.AddHours(24) < DateTime.Now Then
            '        'alles OK!
            '    Else LabelFoutmelding.Text = LabelFoutmelding.Text + "Shipment met nummer " & shipmnt.shipmentNumber & " is nog niet vrijgegeven. Shipments moeten na levering eerst 24 uur in quarantaine OF ze moeten manueel vrijgegeven worden.<br/>"
            '        fouten.Add("1")
            '    End If
            'Next
            Dim listSerie = listSerieList(listShipmentPair.Key)
            'lijst van serienummers die aanwezig zijn in het oudste shipment, maar niet gescand zijn.
            Dim aanwezigmaarnietgescand = OudsteShipment.Serienummer.ToList.Except(listSerie)
            If listShipment.Count = 1 Then
                If listShipment.FirstOrDefault.id = OudsteShipment.id Then
                    'Alle gescande nummers komen voor in de oudste shipment
                Else
                    If listShipment.FirstOrDefault.id = VoorlaatsteOudsteShipment.id Then
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Waarschuwing: Voor artikel " & Artikel.Description & " zijn alle serienummer gescand van het 1-na-oudste shipment.<br/>"
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "(Dit is enkel een waarschuwing. Als er geen andere fouten zijn, is de bestelling wel goedgekeurd)<br/>"
                    Else
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Voor artikel " & Artikel.Description & " zijn alle serienummer gescand die niet tot de oudste shipment behoren.<br/>"

                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Verwachtte shipment(s): " & OudsteShipment.shipmentNumber & "(met ID " & OudsteShipment.id & ") en " & VoorlaatsteOudsteShipment.shipmentNumber & "(met ID " & VoorlaatsteOudsteShipment.id & ")"
                        fouten.Add("1")
                    End If

                End If
            ElseIf listShipment.Count = 2 Then
                'Er zijn 2 shipments gebruikt geweest.
                If aanwezigmaarnietgescand.Count = 0 Then
                    'Dit is oké, want de oudste shipment is volledig op, dus controleer op 1-na-oudste
                    Dim list2oudste = New List(Of Shipment)
                    list2oudste.Add(OudsteShipment)
                    list2oudste.Add(VoorlaatsteOudsteShipment)
                    If list2oudste.Except(listShipment).Count = 0 Then
                        'Alle gescande nummers komen uit de oudste 2 shipments
                    Else
                        'probleem
                        fouten.Add("1")
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Er zijn serienummers gebruikt die niet uit de 2 oudste shipment files komen (na de oudste volledig te hebben gebruikt)<br/>"
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Foute serienummers: <br/>"
                        For Each sr In listSerie
                            If sr.shipmentId <> OudsteShipment.id And sr.shipmentId <> VoorlaatsteOudsteShipment.id Then
                                LabelFoutmelding.Text = sr.serienummer1 + "<br/>"
                            End If
                        Next


                    End If
                Else
                    'probleem.
                    Dim list2oudste = New List(Of Shipment)
                    list2oudste.Add(OudsteShipment)
                    list2oudste.Add(VoorlaatsteOudsteShipment)
                    If list2oudste.Except(listShipment).Count = 0 Then
                        'Serienummers komen uit oudste 2 shipment files, maar oudste is eigenlijk niet op
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Waarschuwing: Voor artikel " & Artikel.Description & " zijn er serienummer gescand uit de 1-na-oudste shipment terwijl er nog serienummers beschikbaar zijn van de oudste shipment<br/>"
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "(Dit is enkel een waarschuwing. Als er geen andere fouten zijn, is de bestelling wel goedgekeurd)<br/>"
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Waarschuwing serienummers: <br/>"
                        For Each sr In listSerie
                            If sr.shipmentId <> OudsteShipment.id Then
                                LabelFoutmelding.Text = LabelFoutmelding.Text + sr.serienummer1 + "<br/>"
                            End If
                        Next
                    Else
                        fouten.Add("1")
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Er zijn serienummers gebruikt die niet uit de 2 oudste shipment files komen<br/>"
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Foute serienummers: <br/>"
                        For Each sr In listSerie
                            If sr.shipmentId <> OudsteShipment.id And sr.shipmentId <> VoorlaatsteOudsteShipment.id Then
                                LabelFoutmelding.Text = LabelFoutmelding.Text + sr.serienummer1 + "<br/>"
                            End If
                        Next
                    End If


                End If

            ElseIf listShipment.Count > 2 Then
                fouten.Add("1")
                LabelFoutmelding.Text = LabelFoutmelding.Text + "De serienummers voor " & Artikel.Description & " komen uit 3 of meer shipment files. Enkel serienummer uit de oudste shipping file mogen gebruikt worden. (Of uit de 2 oudste indien een picking meer serienummers bevat dan nog beschikbaar zijn)"
                LabelFoutmelding.Text = LabelFoutmelding.Text + "Foute serienummers: <br/>"
                For Each sr In listSerie
                    If sr.shipmentId <> OudsteShipment.id And sr.shipmentId <> VoorlaatsteOudsteShipment.id Then
                        LabelFoutmelding.Text = LabelFoutmelding.Text + sr.serienummer1 + "<br/>"
                    End If
                Next
            End If
        Next











        If fouten.Count = 0 Then
            'Voocontext.SaveChanges()

        Else
            Session("Bestellingfout") = LabelFoutmelding.Text
            Session("BestellingGelukt") = ""
            LabelFoutmelding.Visible = True
            Return
        End If

        For Each item In lijst

            If item.hoeveelheid > 0 Then
                Dim Stockmathoofd = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = hoofdmagId And x.MateriaalId = item.materiaalId).FirstOrDefault

                Dim opId As Integer = Session("opdrachtgever")
                If Voocontext.Opdrachtgever.Find(opId).naam <> "FLG" Then
                    Dim aantalgereserveerd = 0.00
                    Try
                        aantalgereserveerd = Voocontext.Bestellijn.Where(Function(x) x.Bestellingen.Status = 4 AndAlso x.materiaalId = item.materiaalId And x.Bestellingen.Gebruikers.OpdrachtgeverId = opId).Sum(Function(y) y.hoeveelheid)
                    Catch ex As Exception

                    End Try

                    If Stockmathoofd.Aantal - aantalgereserveerd - item.hoeveelheid < 0 Then
                        LabelFoutmelding.Visible = True
                        If Session("taal") = 1 Then
                            'TODO CONTROLE IN GERESERVEERD
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Hoofdmagazijn heeft niet voldoende stock voor item " & Stockmathoofd.Basismateriaal.Description & " met ID " & Stockmathoofd.Basismateriaal.Article
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "L'entrepôt principal n'a pas assez de stock pour l'article" & Stockmathoofd.Basismateriaal.Description
                        End If
                        Return
                    End If
                End If
            End If
        Next

        For Each item In lijst
            Dim lijstserie As New List(Of String)
            Dim Stockmat1 = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = item.materiaalId).FirstOrDefault
            If Not item.hoeveelheid = 0 Then




                If IsNothing(Stockmat1) Then
                    Stockmat1 = New StockMagazijn
                    Stockmat1.Aantal = 0
                    Stockmat1.MateriaalId = item.materiaalId
                    Stockmat1.MagazijnId = magid
                    Voocontext.StockMagazijn.Add(Stockmat1)
                    Voocontext.SaveChanges()
                End If


                Dim Stockmathoofd = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = hoofdmagId And x.MateriaalId = item.materiaalId).FirstOrDefault
                Stockmat1.Aantal = Stockmat1.Aantal + item.hoeveelheid
                Stockmathoofd.Aantal = Stockmathoofd.Aantal - item.hoeveelheid

                Dim opId As Integer = Session("opdrachtgever")

                If Stockmathoofd.Aantal < 0 And Voocontext.Opdrachtgever.Find(opId).naam <> "FLG" Then

                    LabelFoutmelding.Visible = True
                    If Session("taal") = 1 Then
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Hoofdmagazijn heeft niet voldoende stock voor item " & Stockmathoofd.Basismateriaal.Description & " met ID " & Stockmathoofd.Basismateriaal.Article
                    Else
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "L'entrepôt principal n'a pas assez de stock pour l'article " & Stockmathoofd.Basismateriaal.Description
                    End If
                    Return
                Else

                    'Alles is ok, log stockbeweging doelmagazijn en koppel serienummers
                    Dim bestelling = Voocontext.Bestellingen.Find(bestellingid)

                    If item.Basismateriaal.HeeftSerienummer And item.hoeveelheid > 0 Then
                        If listSerieList.Where(Function(x) x.Key = item.Basismateriaal.id).Any Then
                            For Each serie In listSerieList.Where(Function(x) x.Key = item.Basismateriaal.id).FirstOrDefault.Value
                                Dim bestellijnserie As New BestellijnSerienummer
                                bestellijnserie.bestellijnId = item.id
                                bestellijnserie.serienummerId = serie.id
                                Voocontext.BestellijnSerienummer.Add(bestellijnserie)
                            Next
                        End If
                    End If

                    Dim sb As New Stockbeweging
                    sb.beweging = item.hoeveelheid
                    sb.datum = DateTime.Now
                    sb.gebruiker = Session("userid")
                    sb.opmerking = "Bestelling (user)"
                    sb.stockmagazijnId = Stockmat1.id
                    Voocontext.Stockbeweging.Add(sb)
                    Voocontext.SaveChanges()

                    If listSerieList.Count > 0 Then
                        If listSerieList.Where(Function(x) x.Key = item.materiaalId).Any Then


                            For Each serie In listSerieList.Where(Function(x) x.Key = item.materiaalId).FirstOrDefault.Value

                                stockmat = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = serie.StockMagazijn.MateriaalId).FirstOrDefault
                                serie.StockMagazijnId = stockmat.id
                                serie.FIFO = False
                                serie.datumOntvangst = Today()
                                serie.statusId = 4

                                Dim log As New Log
                                log.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serie.serienummer1 & " uitgegeven voor bestelling " & Request.QueryString("id")
                                log.Tijdstip = Today
                                log.Gebruiker = Session("userid")
                                Voocontext.Log.Add(log)

                                Dim sbsr As New StockbewegingSerienummer
                                sbsr.serienummerId = serie.id
                                sbsr.stockbewegingid = sb.id
                                Voocontext.StockbewegingSerienummer.Add(sbsr)
                            Next

                            Voocontext.SaveChanges()
                        End If
                    End If





                    'If stockmagazijnserienummer.Count < 0 Then
                    '    For Each sr In stockmagazijnserienummer.Item(Stockmat.id)
                    '        Dim sbsr As New StockbewegingSerienummer
                    '        sbsr.stockbewegingid = sb.id
                    '        sbsr.serienummerId = Voocontext.Serienummer.Where(Function(x) x.serienummer1 = sr.serienummer1).FirstOrDefault.id
                    '        Voocontext.StockbewegingSerienummer.Add(sbsr)
                    '    Next
                    'End If



                    Dim smid As Int32 = Stockmat1.id
                    ' If Stockmat1.Basismateriaal.SerieVerplichtBijOpboeken And Stockmat1.Aantal <> Voocontext.Serienummer.Where(Function(x) x.StockMagazijnId = Stockmat1.id And x.uitgeboekt = False And (x.statusId = 1 Or x.statusId = 2 Or x.statusId = 3 Or x.statusId = 4)).Count() Then
                    ''Bij mat met serienummer, als aantal niet matched
                    'Dim addresses As New List(Of String)
                    'addresses.Add("conan.dufour@apkgroup.eu")
                    'mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                    '       "noreply@apkgroup.eu",
                    '       addresses,
                    '       "Fout is opgetreden bij bestelling. Raadpleeg stockbeweging" & sb.id,
                    '       New Dictionary(Of String, Byte()))
                    'End If

                    'Alles is ok, log stockbeweging hoofdmagazijn
                    Dim sbhoofd As New Stockbeweging
                    sbhoofd.beweging = -item.hoeveelheid
                    sbhoofd.datum = DateTime.Now
                    sbhoofd.gebruiker = Session("userid")
                    sbhoofd.opmerking = "Bestelling (hoofd)"
                    sbhoofd.stockmagazijnId = Stockmathoofd.id
                    Voocontext.Stockbeweging.Add(sbhoofd)
                    Voocontext.SaveChanges()

                    If listSerieList.Count > 0 Then
                        If listSerieList.Where(Function(x) x.Key = item.materiaalId).Any Then


                            For Each serie In listSerieList.Where(Function(x) x.Key = item.materiaalId).FirstOrDefault.Value
                                Dim sbsr As New StockbewegingSerienummer
                                sbsr.serienummerId = serie.id
                                sbsr.stockbewegingid = sbhoofd.id
                                Voocontext.StockbewegingSerienummer.Add(sbsr)
                            Next

                            Voocontext.SaveChanges()
                        End If
                    End If

                    'If stockmagazijnserienummer.Count < 0 Then
                    '    For Each sr In stockmagazijnserienummer.Item(Stockmat.id)
                    '        Dim sbsr As New StockbewegingSerienummer
                    '        sbsr.stockbewegingid = sbhoofd.id
                    '        sbsr.serienummerId = Voocontext.Serienummer.Where(Function(x) x.serienummer1 = sr.serienummer1).FirstOrDefault.id
                    '        Voocontext.StockbewegingSerienummer.Add(sbsr)
                    '    Next
                    'End If



                    Dim smhoofdid As Int32 = Stockmathoofd.id
                    'If Stockmathoofd.Basismateriaal.SerieVerplichtBijOpboeken And Stockmathoofd.Aantal <> Voocontext.Serienummer.Where(Function(x) x.StockMagazijnId = Stockmathoofd.id And x.uitgeboekt = False And (x.statusId = 1 Or x.statusId = 2 Or x.statusId = 3 Or x.statusId = 4)).Count() Then
                    ''Bij mat met serienummer, als aantal niet matched
                    'Dim addresses As New List(Of String)
                    'addresses.Add("conan.dufour@apkgroup.eu")
                    'mailGoogle("WMS aantal komt niet overeen met aantal serienummers!",
                    '       "noreply@apkgroup.eu",
                    '       addresses,
                    '       "Fout is opgetreden bij bestelling goedkeuren. Raadpleeg stockbeweging" & sbhoofd.id,
                    '       New Dictionary(Of String, Byte()))
                    'End If
                End If
            End If
        Next
        Voocontext.SaveChanges()


        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "UPDATE [Bestellingen] SET [Status] = 2, [DatumGoedkeuring]=GETDATE()  WHERE ([Bestellingen].[id] = @bestellingId)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteReader()

            cn.Close()

        End Using





        ASPxLabelGoed.Text = "Bestelling Goedgekeurd"




        Initialiseer()
        'ASPxWebControl.RedirectOnCallback(VirtualPathUtility.ToAbsolute("~/Bestelling.aspx?id=" + Request.QueryString("id")))
        'Context.ApplicationInstance.CompleteRequest()
        VerzendenBevestiging()
    End Sub
    Public Sub VerzendenBevestiging()
        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

        Dim bestid As Integer = CInt(Request.QueryString("id"))
        ASPxGridViewExporter1.FileName = "Bestelling " & bestid
        Dim magid As Integer = ASPxComboBoxMagazijnDoel.Value
        Dim entities As New VooEntities
        Using memoryStream As New MemoryStream()
            memoryStream.Seek(0, SeekOrigin.Begin)
            Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2013_SP1) With {.Url = New Uri("https://outlook.office365.com/EWS/Exchange.asmx"), .UseDefaultCredentials = False, .Credentials = New WebCredentials("apk.admin@apkgroup.onmicrosoft.com", "boGmU5ElS2gGKjyy")}

            Dim message As New EmailMessage(exch)
            ASPxGridViewExporter1.WriteXlsx(memoryStream, New DevExpress.XtraPrinting.XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG, .AllowConditionalFormatting = DevExpress.Utils.DefaultBoolean.True})
            Dim opid As Integer = Session("opdrachtgever")

            Dim magbesteller = entities.Magazijn.Where(Function(x) x.id = magid).FirstOrDefault.Naam
            If entities.Opdrachtgever.Find(opid).naam = "Synductis Kempen" Then
                exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.SmtpAddress, "synductisKEMPEN@apkgroup.eu")
                message.Body = "In bijlage het overzicht van de bestelling die de magazijnier voor u heeft klaargezet voor magazijn " & magbesteller & ". Indien de bestelling niet overeenkomt met de levering, of er andere klachten zijn, gelieve binnen de 72u te antwoorden op dit bericht."

            ElseIf entities.Opdrachtgever.Find(opid).naam = "Synductis Limburg" Then
                exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.SmtpAddress, "synductisLIMBURG@apkgroup.eu")
                message.Body = "In bijlage het overzicht van de bestelling die de magazijnier voor u heeft klaargezet voor magazijn " & magbesteller & ". Indien de bestelling niet overeenkomt met de levering, of er andere klachten zijn, gelieve binnen de 72u te antwoorden op dit bericht."

            Else
                exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.SmtpAddress, "no.reply@apkgroup.eu")
                message.Body = "In bijlage het overzicht van de bestelling die de magazijnier voor u heeft klaargezet voor magazijn  " & magbesteller & "."

            End If





            TrustAllCertificatePolicy.OverrideCertificateValidation()
            Dim m As New QueryStringModule



            message.Subject = "Bestelling " & bestid
            message.Attachments.AddFileAttachment("Bestelling " & bestid & ".xlsx", memoryStream.ToArray)
            message.ToRecipients.Add(entities.Bestellingen.Find(bestid).Gebruikers.Email)
            message.SendAndSaveCopy()
        End Using


    End Sub

    Protected Sub ASPxButton3_Click(sender As Object, e As EventArgs) Handles ASPxButton3.Click

        HideColumns(True)



        Using headerImage
            Select Case Session("opdrachtgever")
                Case 1
                    headerImage = Image.FromFile(Server.MapPath("~/images/logo_brutele.png"))
                Case 2
                    headerImage = Image.FromFile(Server.MapPath("~/images/Logo_Nethys.png"))
                Case 3
                    headerImage = Image.FromFile(Server.MapPath("~/images/fluvius.png"))
                Case 4, 1003
                    headerImage = Image.FromFile(Server.MapPath("~/images/fluvius.png"))
                Case 12
                    headerImage = Image.FromFile(Server.MapPath("~/images/nstLogo.png"))
                Case Else
                    headerImage = Image.FromFile(Server.MapPath("~/images/apklogo.png"))
            End Select

            Dim ps As New PrintingSystemBase()

            Dim link1 As New PrintableComponentLinkBase(ps)
            link1.Component = ASPxGridViewExporter1
            Dim u As New Unit(2)
            Dim margins As New Printing.Margins(10, 10, 10, 10)
            link1.Margins = margins


            Dim link2 As New PrintableComponentLinkBase(ps)
            link2.Component = ASPxGridViewExporter2
            link2.Margins = margins

            Dim link3 As New PrintableComponentLinkBase(ps)
            link3.Component = ASPxGridViewExporter3
            link3.Margins = margins
            'Dim footer As New PrintableComponentLinkBase()



            Dim compositeLink As New CompositeLinkBase(ps)




            Dim header As New LinkBase()
            AddHandler header.CreateDetailHeaderArea, AddressOf Header_CreateDetailHeaderArea






            compositeLink.BreakSpace = 200

            compositeLink.Links.Add(link1)
            compositeLink.Links.Add(link2)
            compositeLink.Links.Add(link3)

            compositeLink.CreateDocument()



            Using stream As New MemoryStream()
                compositeLink.ExportToPdf(stream)


                ps.Dispose()

                Response.AddHeader("content-disposition", "attachment;filename=Bestelling" + Request.QueryString("id") + ".pdf")
                Response.ContentType = "application/octectstream"
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using
        End Using
        HideColumns(False)
    End Sub

    Private Sub HideColumns(v As Boolean)
        ASPxGridView1.Columns("Materiaalgroep").Visible = Not v
        ASPxGridView1.Columns("Eenheid").Visible = Not v
    End Sub

    Protected Sub ASPxButton5_Click(sender As Object, e As EventArgs) Handles ASPxButton5.Click
        Dim entities As New VooEntities
        Dim boxnummer As String = ASPxTextBoxBox.Value
        Dim magazijnid As Integer = ASPxComboBoxMagazijn.Value
        Dim boxlist = entities.Serienummer.Where(Function(x) x.boxNo = boxnummer And x.StockMagazijn.MagazijnId = magazijnid And x.uitgeboekt = False).ToList
        Dim gescandmateriaal As Integer
        If boxlist.Count > 0 Then
            gescandmateriaal = boxlist.FirstOrDefault.StockMagazijn.MateriaalId
        Else
            LabelFoutmelding.Text = "Geen serienummers gevonden in deze box"
            Return
        End If



        Dim listvrij As New List(Of TextBox)
        For Each ctr As System.Web.UI.Control In PlaceHolderSerie.Controls
            If TypeOf (ctr) Is TextBox Then
                Dim id As String = (CType(ctr, TextBox)).ID
                Dim textobject = CType(ctr, TextBox)
                Dim matid = id.Split("_")(1)

                If String.IsNullOrEmpty(textobject.Text) And matid = gescandmateriaal Then

                    listvrij.Add(ctr)
                End If
            End If
        Next
        Dim i As Integer = 0
        If listvrij.Count >= boxlist.Count Then
            For Each serie In boxlist
                listvrij(i).Text = serie.serienummer1
                i = i + 1
            Next
        Else
            LabelFoutmelding.Text = "Te weinig plaats in deze bestelling voor een volledige box"


        End If
        If boxlist.Count = 0 Then
            LabelFoutmelding.Text = "Geen serienummers gevonden in deze box"
        End If



    End Sub

    Protected Sub ASPxComboBoxMagazijn_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.SelectedIndexChanged
        Dim entities As New VooEntities
        Dim magazijnid As Integer = Session("MagazijnId")
        If entities.Serienummer.Where(Function(x) x.StockMagazijn.MagazijnId = magazijnid And x.boxNo IsNot Nothing).Any Then
            ASPxButton4.ClientVisible = True
        Else
            ASPxButton4.ClientVisible = False
        End If
    End Sub

    Protected Sub ASPxButton6_Click(sender As Object, e As EventArgs) Handles ASPxButton6.Click
        Dim voocontext As New VooEntities
        Dim bestId As Integer = Request.QueryString("id")



        Dim addresslist As New List(Of String)
        addresslist.Add(voocontext.Bestellingen.Find(bestId).Gebruikers.Email)
        mailGoogle("Melding voor uw WMS bestelling met referentie " & bestId, "remo.ciarlariello@apkgroup.eu", addresslist, ASPxTextBoxReden.Text, New Dictionary(Of String, Byte()))


        voocontext.Bestellingen.Find(bestId).RedenAfkeuring = ASPxTextBoxReden.Text
        voocontext.SaveChanges()
    End Sub

    Protected Sub ASPxButton8_Click(sender As Object, e As EventArgs) Handles ASPxButton8.Click
        Dim entities As New VooEntities
        Dim palletnummer As String = ASPxTextBoxPallet.Value
        Dim magazijnid As Integer = ASPxComboBoxMagazijn.Value
        Dim palletlist = entities.Serienummer.Where(Function(x) x.palletNo = palletnummer And x.StockMagazijn.MagazijnId = magazijnid And x.uitgeboekt = False).ToList
        Dim gescandmateriaal As Integer
        If palletlist.Count > 0 Then
            gescandmateriaal = palletlist.FirstOrDefault.StockMagazijn.MateriaalId
        Else
            LabelFoutmelding.Text = "Geen serienummers gevonden in dit pallet"
            Return
        End If



        Dim listvrij As New List(Of TextBox)
        For Each ctr As System.Web.UI.Control In PlaceHolderSerie.Controls
            If TypeOf (ctr) Is TextBox Then
                Dim id As String = (CType(ctr, TextBox)).ID
                Dim textobject = CType(ctr, TextBox)
                Dim matid = id.Split("_")(1)

                If String.IsNullOrEmpty(textobject.Text) And matid = gescandmateriaal Then

                    listvrij.Add(ctr)
                End If
            End If
        Next
        Dim i As Integer = 0
        If listvrij.Count >= palletlist.Count Then
            For Each serie In palletlist
                listvrij(i).Text = serie.serienummer1
                i = i + 1
            Next
        Else
            LabelFoutmelding.Text = "Te weinig plaats in deze bestelling voor een volledig pallet"


        End If
        If palletlist.Count = 0 Then
            LabelFoutmelding.Text = "Geen serienummers gevonden in deze box"
        End If

    End Sub

    Protected Sub ASPxButton7_Click(sender As Object, e As EventArgs) Handles ASPxButton7.Click
        Dim username As String
        Dim userid As String
        Dim Voocontext As New VooEntities
        Dim bestellingid As Integer = Request.QueryString("id")

        LabelFoutmelding.Text = ""

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim s_SQL As String = "SELECT (SELECT CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn) END) as Naam, gebruikerId FROM Bestellingen Inner join Gebruikers on bestellingen.GebruikerId = Gebruikers.id where Bestellingen.id = @bestellingId"

            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                username = dr.GetValue(0)
                userid = dr.GetValue(1)
            End If
            dr.Close()
        End Using

        Dim lijst = Voocontext.Bestellijn.Where(Function(x) x.bestellingId = bestellingid).ToList
        Dim intuserId As Integer = CInt(userid)
        'Neem magazijn van overschreven waarde
        Dim magid As Integer = ASPxComboBoxMagazijnDoel.Value
        Dim opdrachtgeverId As Integer = Session("opdrachtgever")
        Dim hoofdmagId As Integer = Session("MagazijnId")
        'serienummers per artikelnummer
        Dim listSerieList As New Dictionary(Of String, List(Of Serienummer))
        Dim fouten As New List(Of String)
        Dim stockmat As StockMagazijn
        Dim matidvorig As Integer = 0

        For Each ctr As System.Web.UI.Control In PlaceHolderHaspel.Controls
            If TypeOf (ctr) Is TextBox Then
                Dim id As String = (CType(ctr, TextBox)).ID
                Dim value As String = (CType(ctr, TextBox)).Text
                Dim parts As String() = id.Split(New Char() {"_"c})
                Dim matId As Integer = Convert.ToInt32(parts(2))

                lijst.Where(Function(x) x.materiaalId = matId).FirstOrDefault.bobijnnummer = value
            End If
        Next

        'controle of ontvanger stockmat heeft voor elk serienummermateriaal
        For Each ctr As System.Web.UI.Control In PlaceHolderSerie.Controls
            If TypeOf (ctr) Is TextBox Then
                Dim id As String = (CType(ctr, TextBox)).ID
                Dim value As String = (CType(ctr, TextBox)).Text
                Dim parts As String() = id.Split(New Char() {"_"c})
                Dim matId As Integer = Convert.ToInt32(parts(1))

                If matId <> matidvorig Then
                    stockmat = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
                End If

                If stockmat Is Nothing Then
                    'Tech heeft geen stockmat voor materiaal??
                    Dim stockmatNieuw As New StockMagazijn
                    stockmatNieuw.MagazijnId = magid
                    stockmatNieuw.MateriaalId = matId
                    stockmatNieuw.Aantal = 0
                    Voocontext.StockMagazijn.Add(stockmatNieuw)
                    Voocontext.SaveChanges()
                    stockmat = stockmatNieuw
                End If
            End If
        Next


        Dim stockmagazijnserienummer As New Dictionary(Of String, List(Of Serienummer))
        matidvorig = 0

        For Each ctr As System.Web.UI.Control In PlaceHolderSerie.Controls
            If TypeOf (ctr) Is TextBox Then
                Dim id As String = (CType(ctr, TextBox)).ID
                Dim value As String = (CType(ctr, TextBox)).Text
                Dim parts As String() = id.Split(New Char() {"_"c})
                Dim matId = Convert.ToInt32(parts(1))

                If String.IsNullOrWhiteSpace(value) Then
                    LabelFoutmelding.Visible = True
                    If Session("taal") = 1 Then

                        fouten.Add("1")
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer mag niet leeg zijn!<br/>"
                    Else
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Le champ ne peut pas être vide!<br/>"
                    End If
                    Exit Sub
                End If

                If matId <> matidvorig Then
                    stockmat = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = matId).FirstOrDefault
                End If
                matidvorig = matId


                If Voocontext.Basismateriaal.Where(Function(x) x.id = matId).FirstOrDefault.SerieVerplichtBijOpboeken Then
                    Dim serienummer As Serienummer = Voocontext.Serienummer.Where(Function(x) x.serienummer1 = value).FirstOrDefault
                    If IsNothing(serienummer) Then
                        'serienummer bestaat niet
                        LabelFoutmelding.Visible = True
                        fouten.Add("1")

                        If Session("taal") = 1 Then

                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer " & value & " bestaat niet. Gelieve na te kijken en opnieuw te proberen. Wijzigingen zijn niet doorgevoerd.<br/>"
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Le numéro de série " & value & " n'existe pas. Veuillez vérifier et réessayer. Les changements n'ont pas été mis en œuvre<br/>"
                        End If
                        'Exit Sub
                    End If
                    If Not serienummer Is Nothing AndAlso serienummer.StockMagazijn.MagazijnId <> hoofdmagId Then
                        'Serienummer staat niet op hoofdmagazijn
                        LabelFoutmelding.Visible = True
                        fouten.Add("1")
                        If Session("taal") = 1 Then
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer " & value & " behoort tot een ander magazijn. Gelieve na te kijken en opnieuw te proberen. Wijzigingen zijn niet doorgevoerd.<br/>"
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Le numéro de série " & value & " appartient à un autre magasin. Veuillez vérifier et réessayer. Les changements n'ont pas été mis en œuvre.<br/>"
                        End If
                        'Return
                    End If
                    If Not serienummer Is Nothing AndAlso (serienummer.statusId = 1 Or serienummer.statusId = 5 Or serienummer.statusId = 6 Or serienummer.statusId = 8) Then
                        'serienummer is gemarkeerd als kaput
                        fouten.Add("1")

                        LabelFoutmelding.Visible = True
                        If Session("taal") = 1 Then
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer " & value & " is gemarkeerd als defect, retour, gereserveerd of verbruikt.  Gelieve na te kijken en opnieuw te proberen. Wijzigingen zijn niet doorgevoerd.<br/>"
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Le numéro de série" & value & "sont marqués comme étant brisés. Veuillez vérifier et réessayer. Aucune modification n'a été apportée.<br/>"
                        End If
                        ' Return
                    End If

                    If Not serienummer Is Nothing AndAlso serienummer.StockMagazijn.MateriaalId <> matId Then
                        'serienummer verkeerd
                        fouten.Add("1")
                        LabelFoutmelding.Visible = True

                        If Session("taal") = 1 Then
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer " & value & " behoort tot een ander artikel. Gelieve na te kijken en opnieuw te proberen. Wijzigingen zijn niet doorgevoerd.<br/>"
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Le numéro de série " & value & " appartient à un autre article. Veuillez vérifier et réessayer. Les changements n'ont pas été mis en œuvre.<br/>"
                        End If
                        ' Return
                    End If


                    If Not serienummer Is Nothing AndAlso serienummer.StockMagazijn.MateriaalId = matId Then
                        If listSerieList.ContainsKey(serienummer.StockMagazijn.Basismateriaal.id) Then
                            listSerieList.Where(Function(x) x.Key = serienummer.StockMagazijn.Basismateriaal.id).FirstOrDefault.Value.Add(serienummer)
                        Else
                            listSerieList.Add(serienummer.StockMagazijn.Basismateriaal.id, New List(Of Serienummer))
                            listSerieList.Where(Function(x) x.Key = serienummer.StockMagazijn.Basismateriaal.id).FirstOrDefault.Value.Add(serienummer)
                        End If

                        If stockmagazijnserienummer.Where(Function(x) x.Key = stockmat.id).Any Then
                            stockmagazijnserienummer.Item(stockmat.id).Add(serienummer)
                        Else
                            stockmagazijnserienummer.Add(stockmat.id, New List(Of Serienummer))
                            stockmagazijnserienummer.Item(stockmat.id).Add(serienummer)
                        End If


                        LabelFoutmelding.Visible = False
                        Session("Bestellingfout") = ""
                        'geknipt
                    End If
                Else
                    'Enkel bij aanmaken nieuwe serienummer wanneer niet verplicht bij opboeken, LEGACY CODE
                    If Voocontext.Serienummer.Where(Function(x) x.serienummer1 = value).Any Then
                        'Serienummer bestaat al ergens
                        LabelFoutmelding.Visible = True
                        fouten.Add("1")
                        If Session("taal") = 1 Then
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Serienummer " & value & " behoort tot een ander magazijn. Gelieve na te kijken en opnieuw te proberen. Wijzigingen zijn niet doorgevoerd.<br/>"
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Le numéro de série " & value & " appartient à un autre magasin. Veuillez vérifier et réessayer. Les changements n'ont pas été mis en œuvre.<br/>"
                        End If
                        'Return
                    End If
                    Dim serienummer As New Serienummer
                    'serienummer.StockMagazijnId = stockmat.id
                    serienummer.serienummer1 = value
                    serienummer.uitgeboekt = 0
                    serienummer.statusId = 8
                    serienummer.datumOntvangst = Today()
                    Voocontext.Serienummer.Add(serienummer)
                    Dim log As New Log
                    log.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & value & " gereserveerd voor bestelling " & Request.QueryString("id")
                    log.Tijdstip = Today
                    log.Gebruiker = Session("userid")
                    Voocontext.Log.Add(log)
                End If


            End If
        Next

        If fouten.Count = 0 Then
            'Voocontext.SaveChanges()

        Else
            Session("Bestellingfout") = LabelFoutmelding.Text
            Session("BestellingGelukt") = ""
            LabelFoutmelding.Visible = True
            Return
        End If
        Dim listShipmentList As New Dictionary(Of String, List(Of Shipment))
        'check serienummers
        For Each combo In listSerieList

            For Each serie In combo.Value
                If Not serie.Shipment Is Nothing Then
                    If serie.statusId = 2 OrElse IsNothing(serie.FIFO) OrElse serie.FIFO = False OrElse serie.statusId = 4 Then
                        'Niet toevoegen aan lijst voor controle shipments
                    Else
                        If listShipmentList.ContainsKey(combo.Key) Then
                            If Not listShipmentList.Where(Function(x) x.Key = combo.Key).FirstOrDefault.Value.Contains(serie.Shipment) Then
                                listShipmentList.Where(Function(x) x.Key = combo.Key).FirstOrDefault.Value.Add(serie.Shipment)
                            End If

                        Else
                            listShipmentList.Add(combo.Key, New List(Of Shipment))
                            listShipmentList.Where(Function(x) x.Key = combo.Key).FirstOrDefault.Value.Add(serie.Shipment)
                        End If
                    End If
                End If
            Next

        Next

        'FIFO CONTROLE
        For Each listShipmentPair In listShipmentList
            Dim AlleShipmentsMetActieveVoorArtikel = Voocontext.Shipment.Where(Function(x) x.Serienummer.Where(Function(y) y.statusId = 3 And y.StockMagazijn.Basismateriaal.id = listShipmentPair.Key And y.StockMagazijn.Magazijn.OpdrachtgeverId = opdrachtgeverId And y.FIFO IsNot Nothing And y.FIFO = True).Any And x.accepted = True)
            Dim OudsteShipment = AlleShipmentsMetActieveVoorArtikel.OrderBy(Function(x) x.datereceived).FirstOrDefault()
            Dim VoorlaatsteOudsteShipment = AlleShipmentsMetActieveVoorArtikel.Where(Function(x) x.id <> OudsteShipment.id).OrderBy(Function(x) x.datereceived).FirstOrDefault()
            Dim ArtikelId As Integer = Convert.ToInt32(listShipmentPair.Key)
            Dim Artikel = Voocontext.Basismateriaal.Find(ArtikelId)
            Dim listShipment = listShipmentPair.Value
            Dim listSerie = listSerieList(listShipmentPair.Key)
            'lijst van serienummers die aanwezig zijn in het oudste shipment, maar niet gescand zijn.
            Dim aanwezigmaarnietgescand = OudsteShipment.Serienummer.ToList.Except(listSerie)
            If listShipment.Count = 1 Then
                If listShipment.FirstOrDefault.id = OudsteShipment.id Then
                    'Alle gescande nummers komen voor in de oudste shipment
                Else
                    If listShipment.FirstOrDefault.id = VoorlaatsteOudsteShipment.id Then
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Waarschuwing: Voor artikel " & Artikel.Description & " zijn alle serienummer gescand van het 1-na-oudste shipment.<br/>"
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "(Dit is enkel een waarschuwing. Als er geen andere fouten zijn, is de bestelling wel goedgekeurd)<br/>"
                    Else
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Voor artikel " & Artikel.Description & " zijn alle serienummer gescand die niet tot de oudste shipment behoren.<br/>"

                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Verwachtte shipment(s): " & OudsteShipment.shipmentNumber & "(met ID " & OudsteShipment.id & ") en " & VoorlaatsteOudsteShipment.shipmentNumber & "(met ID " & VoorlaatsteOudsteShipment.id & ")"
                        fouten.Add("1")
                    End If

                End If
            ElseIf listShipment.Count = 2 Then
                'Er zijn 2 shipments gebruikt geweest.
                If aanwezigmaarnietgescand.Count = 0 Then
                    'Dit is oké, want de oudste shipment is volledig op, dus controleer op 1-na-oudste
                    Dim list2oudste = New List(Of Shipment)
                    list2oudste.Add(OudsteShipment)
                    list2oudste.Add(VoorlaatsteOudsteShipment)
                    If list2oudste.Except(listShipment).Count = 0 Then
                        'Alle gescande nummers komen uit de oudste 2 shipments
                    Else
                        'probleem
                        fouten.Add("1")
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Er zijn serienummers gebruikt die niet uit de 2 oudste shipment files komen (na de oudste volledig te hebben gebruikt)<br/>"
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Foute serienummers: <br/>"
                        For Each sr In listSerie
                            If sr.shipmentId <> OudsteShipment.id And sr.shipmentId <> VoorlaatsteOudsteShipment.id Then
                                LabelFoutmelding.Text = sr.serienummer1 + "<br/>"
                            End If
                        Next


                    End If
                Else
                    'probleem.
                    Dim list2oudste = New List(Of Shipment)
                    list2oudste.Add(OudsteShipment)
                    list2oudste.Add(VoorlaatsteOudsteShipment)
                    If list2oudste.Except(listShipment).Count = 0 Then
                        'Serienummers komen uit oudste 2 shipment files, maar oudste is eigenlijk niet op
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Waarschuwing: Voor artikel " & Artikel.Description & " zijn er serienummer gescand uit de 1-na-oudste shipment terwijl er nog serienummers beschikbaar zijn van de oudste shipment<br/>"
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "(Dit is enkel een waarschuwing. Als er geen andere fouten zijn, is de bestelling wel goedgekeurd)<br/>"
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Waarschuwing serienummers: <br/>"
                        For Each sr In listSerie
                            If sr.shipmentId <> OudsteShipment.id Then
                                LabelFoutmelding.Text = LabelFoutmelding.Text + sr.serienummer1 + "<br/>"
                            End If
                        Next
                    Else
                        fouten.Add("1")
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Er zijn serienummers gebruikt die niet uit de 2 oudste shipment files komen<br/>"
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Foute serienummers: <br/>"
                        For Each sr In listSerie
                            If sr.shipmentId <> OudsteShipment.id And sr.shipmentId <> VoorlaatsteOudsteShipment.id Then
                                LabelFoutmelding.Text = LabelFoutmelding.Text + sr.serienummer1 + "<br/>"
                            End If
                        Next
                    End If


                End If

            ElseIf listShipment.Count > 2 Then
                fouten.Add("1")
                LabelFoutmelding.Text = LabelFoutmelding.Text + "De serienummers voor " & Artikel.Description & " komen uit 3 of meer shipment files. Enkel serienummer uit de oudste shipping file mogen gebruikt worden. (Of uit de 2 oudste indien een picking meer serienummers bevat dan nog beschikbaar zijn)"
                LabelFoutmelding.Text = LabelFoutmelding.Text + "Foute serienummers: <br/>"
                For Each sr In listSerie
                    If sr.shipmentId <> OudsteShipment.id And sr.shipmentId <> VoorlaatsteOudsteShipment.id Then
                        LabelFoutmelding.Text = LabelFoutmelding.Text + sr.serienummer1 + "<br/>"
                    End If
                Next
            End If
        Next

        If fouten.Count = 0 Then
            'Voocontext.SaveChanges()
        Else
            Session("Bestellingfout") = LabelFoutmelding.Text
            Session("BestellingGelukt") = ""
            LabelFoutmelding.Visible = True
            Return
        End If


        'Voor elke bestellijn
        For Each item In lijst


            Dim lijstserie As New List(Of String)
            Dim Stockmat1 = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = item.materiaalId).FirstOrDefault
            If Not item.hoeveelheid = 0 Then
                If IsNothing(Stockmat1) Then
                    'aanmaken indien nodig voor alles
                    Stockmat1 = New StockMagazijn
                    Stockmat1.Aantal = 0
                    Stockmat1.MateriaalId = item.materiaalId
                    Stockmat1.MagazijnId = magid
                    Voocontext.StockMagazijn.Add(Stockmat1)
                    Voocontext.SaveChanges()
                End If



                Dim Stockmathoofd = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = hoofdmagId And x.MateriaalId = item.materiaalId).FirstOrDefault

                Dim opId As Integer = Session("opdrachtgever")


                If Voocontext.Opdrachtgever.Find(opId).naam <> "FLG" Then
                    Dim aantalgereserveerd = 0
                    If Voocontext.Bestellijn.Where(Function(x) x.Bestellingen.Status = 4 AndAlso x.materiaalId = item.materiaalId And x.Bestellingen.Gebruikers.OpdrachtgeverId = opId).Any Then
                        aantalgereserveerd = Voocontext.Bestellijn.Where(Function(x) x.Bestellingen.Status = 4 AndAlso x.materiaalId = item.materiaalId And x.Bestellingen.Gebruikers.OpdrachtgeverId = opId).Sum(Function(y) y.hoeveelheid)

                    End If
                    If Stockmathoofd.Aantal - aantalgereserveerd - item.hoeveelheid < 0 Then
                        LabelFoutmelding.Visible = True
                        If Session("taal") = 1 Then
                            'TODO CONTROLE IN GERESERVEERD
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "Hoofdmagazijn heeft niet voldoende stock voor item" & Stockmathoofd.Basismateriaal.Description
                        Else
                            LabelFoutmelding.Text = LabelFoutmelding.Text + "L'entrepôt principal n'a pas assez de stock pour l'article" & Stockmathoofd.Basismateriaal.Description
                        End If
                        Return
                    End If
                End If

                'Alles is ok, koppel serienummers
                Dim bestelling = Voocontext.Bestellingen.Find(bestellingid)

                If item.Basismateriaal.HeeftSerienummer And item.hoeveelheid > 0 Then
                    If listSerieList.Where(Function(x) x.Key = item.Basismateriaal.id).Any Then
                        For Each serie In listSerieList.Where(Function(x) x.Key = item.Basismateriaal.id).FirstOrDefault.Value
                            Dim bestellijnserie As New BestellijnSerienummer
                            bestellijnserie.bestellijnId = item.id
                            bestellijnserie.serienummerId = serie.id
                            Voocontext.BestellijnSerienummer.Add(bestellijnserie)
                        Next
                    End If
                End If

                If listSerieList.Count > 0 Then
                    If listSerieList.Where(Function(x) x.Key = item.materiaalId).Any Then
                        For Each serie In listSerieList.Where(Function(x) x.Key = item.materiaalId).FirstOrDefault.Value
                            serie.FIFO = False
                            serie.statusId = 8

                            Dim log As New Log
                            log.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serie.serienummer1 & " gereserveerd voor bestelling " & Request.QueryString("id")
                            log.Tijdstip = Today
                            log.Gebruiker = Session("userid")
                            Voocontext.Log.Add(log)

                        Next

                        Voocontext.SaveChanges()
                    End If
                End If
            End If
        Next
        Voocontext.SaveChanges()


        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "UPDATE [Bestellingen] SET [Status] = 4 WHERE ([Bestellingen].[id] = @bestellingId)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteReader()
            cn.Close()
        End Using

        ASPxLabelGoed.Text = "Bestelling Voorbereid"

        Initialiseer()
    End Sub

    Protected Sub ASPxButtonReservatieGoedkeuren_Click(sender As Object, e As EventArgs) Handles ASPxButtonReservatieGoedkeuren.Click
        Dim username As String
        Dim userid As String
        Dim Voocontext As New VooEntities
        Dim bestellingid As Integer = Request.QueryString("id")

        LabelFoutmelding.Text = ""

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim s_SQL As String = "SELECT (SELECT CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn) END) as Naam, gebruikerId FROM Bestellingen Inner join Gebruikers on bestellingen.GebruikerId = Gebruikers.id where Bestellingen.id = @bestellingId"

            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                username = dr.GetValue(0)
                userid = dr.GetValue(1)
            End If
            dr.Close()
        End Using

        Dim lijst = Voocontext.Bestellijn.Where(Function(x) x.bestellingId = bestellingid).ToList
        Dim intuserId As Integer = CInt(userid)
        'Neem magazijn van overschreven waarde
        Dim magid As Integer = ASPxComboBoxMagazijnDoel.Value
        Dim opdrachtgeverId As Integer = Session("opdrachtgever")
        Dim hoofdmagId As Integer = Session("MagazijnId")
        Dim listSerieList As New Dictionary(Of String, List(Of Serienummer))
        Dim fouten As New List(Of String)
        Dim stockmat As StockMagazijn

        For Each ctr As System.Web.UI.Control In PlaceHolderHaspel.Controls
            If TypeOf (ctr) Is TextBox Then
                Dim id As String = (CType(ctr, TextBox)).ID
                Dim value As String = (CType(ctr, TextBox)).Text
                Dim parts As String() = id.Split(New Char() {"_"c})
                Dim matId As Integer = Convert.ToInt32(parts(2))

                lijst.Where(Function(x) x.materiaalId = matId).FirstOrDefault.bobijnnummer = value
            End If
        Next


        If fouten.Count = 0 Then
            'Voocontext.SaveChanges()

        Else
            Session("Bestellingfout") = LabelFoutmelding.Text
            Session("BestellingGelukt") = ""
            LabelFoutmelding.Visible = True
            Return
        End If


        If fouten.Count = 0 Then

        Else
            Session("Bestellingfout") = LabelFoutmelding.Text
            Session("BestellingGelukt") = ""
            LabelFoutmelding.Visible = True
            Return
        End If

        For Each item In lijst
            Dim Stockmathoofd = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = hoofdmagId And x.MateriaalId = item.materiaalId).FirstOrDefault

            Dim opId As Integer = Session("opdrachtgever")
            If Voocontext.Opdrachtgever.Find(opId).naam <> "FLG" Then

                If Stockmathoofd.Aantal - item.hoeveelheid < 0 Then
                    LabelFoutmelding.Visible = True
                    If Session("taal") = 1 Then
                        'TODO CONTROLE IN GERESERVEERD
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Hoofdmagazijn heeft niet voldoende stock voor item" & Stockmathoofd.Basismateriaal.Description
                    Else
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "L'entrepôt principal n'a pas assez de stock pour l'article" & Stockmathoofd.Basismateriaal.Description
                    End If
                    Return
                End If
            End If


        Next

        For Each item In lijst
            Dim lijstserie As New List(Of String)
            Dim Stockmat1 = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = item.materiaalId).FirstOrDefault
            If Not item.hoeveelheid = 0 Then




                If IsNothing(Stockmat1) Then
                    Stockmat1 = New StockMagazijn
                    Stockmat1.Aantal = 0
                    Stockmat1.MateriaalId = item.materiaalId
                    Stockmat1.MagazijnId = magid
                    Voocontext.StockMagazijn.Add(Stockmat1)
                    Voocontext.SaveChanges()
                End If


                Dim Stockmathoofd = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = hoofdmagId And x.MateriaalId = item.materiaalId).FirstOrDefault
                Stockmat1.Aantal = Stockmat1.Aantal + item.hoeveelheid
                Stockmathoofd.Aantal = Stockmathoofd.Aantal - item.hoeveelheid

                Dim opId As Integer = Session("opdrachtgever")

                If Stockmathoofd.Aantal < 0 And Voocontext.Opdrachtgever.Find(opId).naam <> "FLG" Then

                    LabelFoutmelding.Visible = True
                    If Session("taal") = 1 Then
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "Hoofdmagazijn heeft niet voldoende stock voor item" & Stockmathoofd.Basismateriaal.Description
                    Else
                        LabelFoutmelding.Text = LabelFoutmelding.Text + "L'entrepôt principal n'a pas assez de stock pour l'article" & Stockmathoofd.Basismateriaal.Description
                    End If
                    Return
                Else

                    Dim bestelling = Voocontext.Bestellingen.Find(bestellingid)

                    If item.Basismateriaal.HeeftSerienummer And item.hoeveelheid > 0 Then
                        If listSerieList.Where(Function(x) x.Key = item.Basismateriaal.id).Any Then
                            For Each serie In listSerieList.Where(Function(x) x.Key = item.Basismateriaal.id).FirstOrDefault.Value
                                Dim bestellijnserie As New BestellijnSerienummer
                                bestellijnserie.bestellijnId = item.id
                                bestellijnserie.serienummerId = serie.id
                                Voocontext.BestellijnSerienummer.Add(bestellijnserie)
                            Next
                        End If
                    End If

                    Dim sb As New Stockbeweging
                    sb.beweging = item.hoeveelheid
                    sb.datum = DateTime.Now
                    sb.gebruiker = Session("userid")
                    sb.opmerking = "Gereserveerde bestelling (user)"
                    sb.stockmagazijnId = Stockmat1.id
                    Voocontext.Stockbeweging.Add(sb)
                    Voocontext.SaveChanges()

                    If item.BestellijnSerienummer.Count > 0 Then
                        For Each seriebest In item.BestellijnSerienummer
                            Dim serie = Voocontext.Serienummer.Find(seriebest.serienummerId)
                            stockmat = Voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = magid And x.MateriaalId = serie.StockMagazijn.MateriaalId).FirstOrDefault
                            serie.StockMagazijnId = stockmat.id
                            serie.FIFO = False
                            serie.datumOntvangst = Today()
                            serie.statusId = 4

                            Dim log As New Log
                            log.Actie = "Gebruiker " & Session("userid") & " Heeft serienummer " & serie.serienummer1 & " uitgegeven voor gereserveerde bestelling " & Request.QueryString("id")
                            log.Tijdstip = Today
                            log.Gebruiker = Session("userid")
                            Voocontext.Log.Add(log)

                            Dim sbsr As New StockbewegingSerienummer
                            sbsr.serienummerId = serie.id
                            sbsr.stockbewegingid = sb.id
                            Voocontext.StockbewegingSerienummer.Add(sbsr)
                        Next

                        Voocontext.SaveChanges()
                    End If
                End If

                Dim smid As Int32 = Stockmat1.id

                Dim sbhoofd As New Stockbeweging
                sbhoofd.beweging = -item.hoeveelheid
                sbhoofd.datum = DateTime.Now
                sbhoofd.gebruiker = Session("userid")
                sbhoofd.opmerking = "Gereserveerde bestelling (hoofd)"
                sbhoofd.stockmagazijnId = Stockmathoofd.id
                Voocontext.Stockbeweging.Add(sbhoofd)
                Voocontext.SaveChanges()

                If item.BestellijnSerienummer.Count > 0 Then
                    For Each seriebest In item.BestellijnSerienummer
                        Dim serie = Voocontext.Serienummer.Find(seriebest.serienummerId)
                        Dim sbsr As New StockbewegingSerienummer
                        sbsr.serienummerId = serie.id
                        sbsr.stockbewegingid = sbhoofd.id
                        Voocontext.StockbewegingSerienummer.Add(sbsr)
                    Next

                    Voocontext.SaveChanges()

                End If

                Dim smhoofdid As Int32 = Stockmathoofd.id

            End If
        Next
        Voocontext.SaveChanges()


        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "UPDATE [Bestellingen] SET [Status] = 2, [DatumGoedkeuring]=GETDATE()  WHERE ([Bestellingen].[id] = @bestellingId)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            cmd.ExecuteReader()

            cn.Close()

        End Using

        ASPxLabelGoed.Text = "(Gereserveerde) Bestelling Goedgekeurd"
        Initialiseer()
        VerzendenBevestiging()
    End Sub
End Class