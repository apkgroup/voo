'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class Basismateriaal
    Public Property id As Integer
    Public Property Article As String
    Public Property Description As String
    Public Property actief As Nullable(Of Boolean)
    Public Property OpdrachtgeverId As Nullable(Of Integer)
    Public Property HeeftSerienummer As Nullable(Of Boolean)
    Public Property min As Nullable(Of Decimal)
    Public Property max As Nullable(Of Decimal)
    Public Property SerieVerplichtBijOpboeken As Nullable(Of Boolean)
    Public Property retourItem As Nullable(Of Boolean)
    Public Property onbestelbaar As Nullable(Of Boolean)
    Public Property serienummerRegex As String
    Public Property bobijnArtikel As Nullable(Of Boolean)
    Public Property foto As Byte()
    Public Property MateriaalGroep As Nullable(Of Integer)
    Public Property prijs As Nullable(Of Decimal)
    Public Property eenheid As String
    Public Property MateriaalGroep2 As Nullable(Of Integer)
    Public Property MateriaalGroep3 As Nullable(Of Integer)
    Public Property ArticleInternal As String
    Public Property MateriaalType As Nullable(Of Integer)
    Public Property Consignatie As Nullable(Of Boolean)

    Public Overridable Property MateriaalGroep1 As MateriaalGroep
    Public Overridable Property MateriaalGroep4 As MateriaalGroep
    Public Overridable Property MateriaalGroep5 As MateriaalGroep
    Public Overridable Property MateriaalType1 As MateriaalType
    Public Overridable Property Opdrachtgever As Opdrachtgever
    Public Overridable Property Bestellijn As ICollection(Of Bestellijn) = New HashSet(Of Bestellijn)
    Public Overridable Property BestellijnBobijn As ICollection(Of BestellijnBobijn) = New HashSet(Of BestellijnBobijn)
    Public Overridable Property MateriaalOpdrachtgevers As ICollection(Of MateriaalOpdrachtgevers) = New HashSet(Of MateriaalOpdrachtgevers)
    Public Overridable Property minmax As ICollection(Of minmax) = New HashSet(Of minmax)
    Public Overridable Property RetourBestellijn As ICollection(Of RetourBestellijn) = New HashSet(Of RetourBestellijn)
    Public Overridable Property StockMagazijn As ICollection(Of StockMagazijn) = New HashSet(Of StockMagazijn)
    Public Overridable Property Stocktellinglijn As ICollection(Of Stocktellinglijn) = New HashSet(Of Stocktellinglijn)
    Public Overridable Property Verbruiklijn As ICollection(Of Verbruiklijn) = New HashSet(Of Verbruiklijn)

End Class
