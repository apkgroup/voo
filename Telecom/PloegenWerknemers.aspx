﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="PloegenWerknemers.aspx.vb" Inherits="Telecom.PloegenWerknemers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h2>Beheer ploegen: werknemers per ploeg</h2>   
    <br />
      <table>
        <tr>
            <td>
                Selecteer een ploeg:
            </td>
             <td>

                 <dx:ASPxComboBox ID="cboPloeg" runat="server" ClientInstanceName="cboPloeg" DataSourceID="SqlDataSourcePloegen" TextField="Naam" ValueField="PloegNr" ValueType="System.Int32" AutoPostBack="True">
                 </dx:ASPxComboBox>
                 
                
            </td>

        </tr>
    </table>
     
    <br />
    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourcePloegenWerknemers" KeyFieldName="id" Visible="False">
        <Columns>
            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Ploeg" VisibleIndex="2" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="Begindatum" VisibleIndex="4">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataDateColumn FieldName="Einddatum" VisibleIndex="5">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataComboBoxColumn FieldName="Werknemer" VisibleIndex="3">
                <PropertiesComboBox DataSourceID="SqlDataSourceWerknemers" TextField="Werknemer" ValueField="id" ValueType="System.Int32">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
        <SettingsPager Mode="ShowAllRecords" Visible="False">
        </SettingsPager>
          <SettingsCommandButton>
             <NewButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Nieuw" Url="~/images/Add_32x32.png" Width="16px">
                 </Image>
             </NewButton>
             <UpdateButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewaren" Url="~/images/Save_16x16.png" Width="16px">
                 </Image>
             </UpdateButton>
             <CancelButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Annuleren" Url="~/images/annuleren.png" Width="16px">
                 </Image>
             </CancelButton>
             <EditButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewerken" Url="~/images/Edit.png" Width="16px">
                 </Image>
             </EditButton>
             <DeleteButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Verwijderen" Url="~/images/Folders-OS-Recycle-Bin-Full-Metro-icon.png" Width="16px">
                 </Image>
             </DeleteButton>
         </SettingsCommandButton>
     </dx:ASPxGridView>
      <asp:SqlDataSource ID="SqlDataSourcePloegenWerknemers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Ploegen_Werknemers] WHERE [id] = @id" InsertCommand="INSERT INTO [Ploegen_Werknemers] ([Ploeg], [Werknemer], [Begindatum], [Einddatum]) VALUES (@Ploeg, @Werknemer, @Begindatum, @Einddatum)" SelectCommand="SELECT [id], [Ploeg], [Werknemer], [Begindatum], [Einddatum] FROM [Ploegen_Werknemers] WHERE ([Ploeg] = @Ploeg)" UpdateCommand="UPDATE [Ploegen_Werknemers] SET [Ploeg] = @Ploeg, [Werknemer] = @Werknemer, [Begindatum] = @Begindatum, [Einddatum] = @Einddatum WHERE [id] = @id">
          <DeleteParameters>
              <asp:Parameter Name="id" Type="Int32" />
          </DeleteParameters>
          <InsertParameters>
              <asp:Parameter Name="Ploeg" Type="Int32" />
              <asp:Parameter Name="Werknemer" Type="Int32" />
              <asp:Parameter DbType="Date" Name="Begindatum" />
              <asp:Parameter DbType="Date" Name="Einddatum" />
          </InsertParameters>
          <SelectParameters>
              <asp:ControlParameter ControlID="cboPloeg" Name="Ploeg" PropertyName="Value" Type="Int32" />
          </SelectParameters>
          <UpdateParameters>
              <asp:Parameter Name="Ploeg" Type="Int32" />
              <asp:Parameter Name="Werknemer" Type="Int32" />
              <asp:Parameter DbType="Date" Name="Begindatum" />
              <asp:Parameter DbType="Date" Name="Einddatum" />
              <asp:Parameter Name="id" Type="Int32" />
          </UpdateParameters>
     </asp:SqlDataSource>
      <asp:SqlDataSource ID="SqlDataSourceWerknemers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Gebruikers.[id], W.Werknemer
  FROM [dbo].[Gebruikers] INNER JOIN
                             (SELECT        [ON], NR, NAAM + ' ' + VNAAM AS Werknemer
                               FROM            Elly_SQL.dbo.WERKN where (isnull(uitdienst,0)&lt;&gt;1)) AS W ON dbo.Gebruikers.Werkg = W.[ON] AND dbo.Gebruikers.Werkn = W.NR
	WHERE (Isnull(Gebruikers.Actief,0)=1) ORDER BY W.Werknemer"></asp:SqlDataSource>
     <asp:SqlDataSource ID="SqlDataSourcePloegen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [PloegNr], [Naam], [Actief] FROM [Ploegen] where (isnull(Actief,0)=1)"></asp:SqlDataSource>
                 
</asp:Content>
