﻿Imports System.Data.SqlClient
Imports Microsoft.Exchange.WebServices.Data

Public Class GebruikersVersturen
    Inherits Page

    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)
        If taalId = 1 Then
            Literal3.Text = "De volgende gebruikers zijn succesvol gemaakt of al gemaakt, maar hebben geen e-mail ontvangen met hun inloggegevens. Vul GSM-nummers en e-mailadressen waar deze ontbreken. U kunt vervolgens de gebruikers selecteren en een e-mail sturen met login en wachtwoord."
            Literal2.Text = "Gebruikers toevoegen"
        Else
            Literal2.Text = "Ajouter de nouveaux utilisateurs"
            Literal3.Text = "Les utilisateurs suivants ont été créés avec succès ou déjà créés, mais n'a pas reçu de courrier électronique avec leurs données de connexion. Remplissez GSMnummers et adresses e-mail pour plaire à manquer. Vous pouvez ensuite sélectionner les utilisateurs et leur envoyer un email avec login et mot de passe. "
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Private Sub grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs) Handles grid.CustomCallback
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim varBericht As String = ""
            Dim varOnderwerp As String = ""
            Dim s_SQL As String = "SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Logingegevens naar gebruiker" & Session("taal")}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not dr.IsDBNull(0) Then
                    varBericht = dr.GetString(0)
                    varOnderwerp = dr.GetString(1).Replace("[SITETITEL]", Session("sitetitel"))
                End If
            End If
            dr.Close()
            Dim selectItems As List(Of Object) = grid.GetSelectedFieldValues("id")
            For Each selectItemId As Object In selectItems
                s_SQL = "UPDATE Gebruikers SET WachtwoordVerstuurdop=GetDate() WHERE (id=@id)"
                cmd = New SqlCommand(s_SQL, cn)
                par = New SqlParameter("@id", SqlDbType.Int) With {.Value = CInt(selectItemId)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            Next

            Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2013_SP1) With {.Url = New Uri("https://outlook.office365.com/EWS/Exchange.asmx"), .UseDefaultCredentials = False, .Credentials = New WebCredentials("apk.admin@apkgroup.onmicrosoft.com", "boGmU5ElS2gGKjyy")}


            exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.SmtpAddress, "apk.admin@apkgroup.onmicrosoft.com")

            Dim m As New QueryStringModule
            Dim arrLogin As String() = Nothing
            Dim arrWachtwoord As String() = Nothing
            Dim arrWerknemer As String() = Nothing
            Dim arrEmail As String() = Nothing
            Dim arrBeheerder As String() = Nothing
            Dim arrId As Integer() = Nothing
            Dim iArr As Integer = 0
            s_SQL = "SELECT Gebruikers.Email, vwWerkn.WERKNEMER, vwWerkn2.BEHEERDER, Gebruikers.Login, Gebruikers.Paswoord, Gebruikers.id FROM Gebruikers " _
                & "INNER JOIN (SELECT NAAM + ' ' + VNAAM as WERKNEMER, [ON],NR FROM Elly_SQL.dbo.WERKN) vwWerkn " _
                & "ON Gebruikers.Werkg=vwWerkn.[ON] AND Gebruikers.Werkn=vwWerkn.NR INNER JOIN Gebruikers Gebruikers_1 " _
                & "ON Gebruikers.AangemaaktDoor=Gebruikers_1.id INNER JOIN (SELECT NAAM + ' ' + VNAAM as BEHEERDER, [ON],NR FROM Elly_SQL.dbo.WERKN) vwWerkn2 " _
                & "ON Gebruikers_1.Werkg=vwWerkn2.[ON] AND Gebruikers_1.Werkn=vwWerkn2.NR WHERE ( isnull(Gebruikers.Wachtwoordverstuurd,0)=0) AND ( Gebruikers.WachtwoordVerstuurdOp is not Null)"

            cmd = New SqlCommand(s_SQL, cn)
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                Do While dr.Read
                    ReDim Preserve arrLogin(iArr)
                    ReDim Preserve arrWachtwoord(iArr)
                    ReDim Preserve arrWerknemer(iArr)
                    ReDim Preserve arrEmail(iArr)
                    ReDim Preserve arrBeheerder(iArr)
                    ReDim Preserve arrId(iArr)
                    arrLogin(iArr) = dr.GetString(3)
                    arrWachtwoord(iArr) = m.Decrypt(dr.GetString(4), venc)
                    arrWerknemer(iArr) = dr.GetString(1)
                    arrEmail(iArr) = dr.GetString(0)
                    arrBeheerder(iArr) = dr.GetString(2)
                    arrId(iArr) = dr.GetInt32(5)
                    iArr += 1
                Loop
            End If
            dr.Close()
            If Not (arrLogin Is Nothing) Then
                For i As Integer = 0 To arrLogin.GetUpperBound(0)
                    Try
                        Dim message As New EmailMessage(exch)
                        message.Subject = varOnderwerp
                        Dim varBody As String = varBericht.Replace("[WERKNEMER]", arrWerknemer(i))
                        varBody = varBody.Replace("[BEHEERDER]", arrBeheerder(i))
                        varBody = varBody.Replace("[URL]", Session("domein"))
                        varBody = varBody.Replace("[LOGIN]", arrLogin(i) & " of uw emailadres.")
                        varBody = varBody.Replace("[WACHTWOORD]", arrWachtwoord(i))
                        varBody = varBody.Replace("[SITETITEL]", Session("sitetitel"))
                        Dim sBody As String = "<x-html>" & vbCrLf
                        sBody = sBody & varBody
                        sBody = sBody & "</x-html>"
                        message.Body = sBody
                        message.ToRecipients.Add(arrEmail(i))
                        message.Send()
                        s_SQL = "UPDATE Gebruikers SET WachtwoordVerstuurd=1 WHERE (id=@id)"
                        cmd = New SqlCommand(s_SQL, cn)
                        par = New SqlParameter("@id", SqlDbType.Int) With {.Value = arrId(i)}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                        s_SQL = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                        cmd = New SqlCommand(s_SQL, cn)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Logingegevens verstuurd door {0} aan {1}", arrBeheerder(i), arrLogin(i))}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        s_SQL = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                        cmd = New SqlCommand(s_SQL, cn)
                        par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij versturen Logingegevens door {0} aan {1}, uitleg: {2}", arrBeheerder(i), arrLogin(i), ex.Message)}
                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()
                    End Try

                Next
            End If
        End Using
        grid.DataBind()
        grid.Selection.UnselectAll()
    End Sub
End Class