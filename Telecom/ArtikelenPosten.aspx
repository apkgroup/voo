﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="ArtikelenPosten.aspx.vb" Inherits="Telecom.ArtikelenPosten" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #TextArea1 {
            height: 51px;
            width: 102px;
        }

        
        .auto-style1 {
            height: 30px;
        }

        
    </style>
    <script>
        var currentRowIndex = undefined;
        var currentRowIndex2 = undefined;
  
    </script>
    <script type="text/javascript">

       
        window.onload = function () {
            ASPxGridView2.Refresh();
            var i;
            for (i = 1; i < 8; i++) {
                console.log("Serie" + i);
                document.getElementById("Serie" + i).style.display = 'none';
            }
            $('input').keypress(function (e) {
                console.log("KEYPRESS");
                if (e.which == 13) {
                    e.preventDefault();
                    console.log("ISENTER");
                    var self = $(this)
                    var form = self.parents('form:eq(0)');
                    var focusable;
                    var next;
  var prev;

                     focusable = form.find('input,a,select,button,textarea').filter(':visible');
                     next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {

                    }
                    return false;
                    
                }
            });
        };

        var timerHandle = -1;
        var indexrow
    

        function OnEndCallBack(s, e) {
            if (s.cpIsUpdated != '') {
               

                clientText.GetMainElement().style.display = 'block';
                console.log('Waarde komt uit');
                //console.log(e); 
                //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);
                console.log('Temps de Regie updated! Waarde is...');
                //console.log(e); 
                //console.log(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML);

                console.log(clientButton);
                //console.log(Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML));
                if (ASPxGridView1.GetRow(indexrow).children[3].firstElementChild == null) {
                    if (Number(ASPxGridView1.GetRow(indexrow).children[3].innerHTML.replace(",", ".")) > 0) {
                        console.log("enabled=false");
                        clientButton.SetEnabled(false);
                        clientLabel2.GetMainElement().style.display = 'block'
                    }
                    else {
                        console.log("enabled=true");
                        clientButton.SetEnabled(true);
                        clientLabel2.GetMainElement().style.display = 'none'
                    }
                }
                else {
                    if (Number(ASPxGridView1.GetRow(indexrow).children[3].firstElementChild.innerHTML.replace(",", ".")) > 0) {
                        console.log("enabled=false");
                        clientButton.SetEnabled(false);
                        clientLabel2.GetMainElement().style.display = 'block'
                    }
                    else {
                        console.log("enabled=true");
                        clientButton.SetEnabled(true);
                        clientLabel2.GetMainElement().style.display = 'none'
                    }
                }
                
                
                //console.log(s.cpIsUpdated);

                
            }
            else {
                clientText.SetText('');
                console.log('Anders updated');
            }
        }

        function OnKeyDown(s, e) {
            var key = e.htmlEvent.keyCode;
            console.log("key pressed!");
            if (key == 38 || key == 40) {
                console.log("Up or down key pressed!");
                ASPxClientUtils.PreventEvent(e.htmlEvent);
                ASPxGridView1.batchEditApi.EndEdit();
                console.log(currentRowIndex);
                if (currentRowIndex !== undefined && currentRowIndex !== null) {
                    var visibleIndices = ASPxGridView1.batchEditApi.GetRowVisibleIndices();
                    var indicesArrayLength = visibleIndices.length;
                    var rowIndex = visibleIndices.indexOf(currentRowIndex);
                    if (!(rowIndex === 0 && key === 38) || !(rowIndex === indicesArrayLength - 1 && key === 40)) {
                        switch (key) {
                            case 38:
                                ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex - 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            case 40:
                                ASPxGridView1.batchEditApi.StartEdit(visibleIndices[rowIndex + 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        function OnKeyDownMat(s, e) {
            var key = e.htmlEvent.keyCode;
            console.log("key pressed!");
            if (key == 38 || key == 40) {
                console.log("Up or down key pressed");
                ASPxClientUtils.PreventEvent(e.htmlEvent);
                ASPxGridView2.batchEditApi.EndEdit();
                console.log(currentRowIndex2);
                if (currentRowIndex2 !== undefined && currentRowIndex2 !== null) {
                    var visibleIndices2 = ASPxGridView2.batchEditApi.GetRowVisibleIndices();
                    var indicesArrayLength2 = visibleIndices2.length;
                    var rowIndex2 = visibleIndices2.indexOf(currentRowIndex2);
                    if (!(rowIndex2 === 0 && key === 38) || !(rowIndex2 === indicesArrayLength2 - 1 && key === 40)) {
                        console.log("ERIN");
                        switch (key) {
                            case 38:
                                ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 - 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            case 40:
                                ASPxGridView2.batchEditApi.StartEdit(visibleIndices2[rowIndex2 + 1], 4); // Adjust the second argument according
                                // to the 'Nombre' column index
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

       
            function OnBatchEditStartEditing(s, e) {
            currentRowIndex = e.visibleIndex;
            currentRowIndex2 = e.visibleIndex;
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("Hoeveelheid");
        }
        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);
            
            indexrow = e.visibleIndex;
            console.log("Index row set to " + indexrow);

        }

        function saveChangesBtn_Click(s, e) {
            ASPxGridView1.UpdateEdit();
            ASPxGridView2.UpdateEdit();
        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();
            ASPxGridView2.CancelEdit();
        }




        function saveChangesBtnMat_Click(s, e) {
            ASPxGridView2.UpdateEdit();
        }

        function cancelChangesBtnMat_Click(s, e) {
            ASPxGridView2.CancelEdit();


            
        }
        function CheckSerienummers(s, e) {
            var i;
            for (i = 1; i < 8; i++) {
                console.log("Serie" + i);
                document.getElementById("Serie" + i).style.display = 'none';
            }

            if (HiddenArtikel.Get("heeftSerienummer") == true) {
                console.log("HEEFTSERIE")
                var i;
        
                console.log(s);
                for (i = 1; i < s.number + 1; i++) {

                    console.log(i);
                    document.getElementById("Serie" + i).style.display = 'block';
                }

            }
            else {
                console.log("HEEFT GEEN SERIE")
            }
                
            
        }

        function insertArticle() {

            Callback1.PerformCallback();
        }

        function OnCallbackComplete(s, e) {
            console.log("cbCOMPLET");
            ASPxGridView2.Refresh();
            //location.reload();
            history.go(0);
        }



        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Postes et Materiaux</h2>
    <p>&nbsp;  <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </p>
    <p>Vos modifications sont enregistrées automatiquement</p>

    
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="1">
        
            
        
        <TabPages>
            <dx:TabPage Name="Postes" Text="Postes">
                <ContentCollection>
                    <dx:ContentControl runat="server">

                        <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourcePosten" KeyFieldName="id">

                         
                            <SettingsPager Mode="ShowAllRecords" Visible="False">
                            </SettingsPager>
                            <SettingsEditing Mode="Batch">
                                <BatchEditSettings StartEditAction="Click" />
                            </SettingsEditing>
                            <Settings ShowStatusBar="Hidden" />
                            <SettingsBehavior ProcessFocusedRowChangedOnServer="True" />
                            <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
                            <ClientSideEvents BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing" EndCallback="OnEndCallBack"/>
                            <SettingsSearchPanel Visible="True" />
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="0" Visible="False">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Groupe" ShowInCustomizationForm="True" VisibleIndex="2">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Poste" ShowInCustomizationForm="True" VisibleIndex="3">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Description" ShowInCustomizationForm="False" VisibleIndex="4">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Nombre" ShowInCustomizationForm="True" VisibleIndex="5" Caption="Nombre">
                                    <PropertiesTextEdit>
                                        <ClientSideEvents KeyDown="OnKeyDown" />
                                        <MaskSettings Mask="&lt;0..99&gt;&lt;,|.&gt;&lt;00..99&gt;" AllowMouseWheel="False" />
                                    </PropertiesTextEdit>
                                    <EditFormSettings Visible="True" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Werk" ShowInCustomizationForm="True" Visible="False" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:ASPxGridView>
                        <asp:SqlDataSource ID="SqlDataSourcePosten" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [VOO_Posten] WHERE [id] = @id" InsertCommand="INSERT INTO [VOO_Posten] ([Werk], [Poste], [Description], [Nombre]) VALUES (@Werk, @Poste, @Description, @Nombre)" SelectCommand="SELECT  [Voo_Posten].[id],
 [Voo_posten].[Werk],
 [Postengroepen].[Groep] as Groupe,
 [Basisposten].[Poste], 
 [Basisposten].[Description], 
 [Basisposten].OpdrachtgeverId, 
 [Voo_Posten].Nombre FROM [Basisposten] 
 left join VOO_Posten on Basisposten.Poste = VOO_Posten.poste
Inner join postengroepen on Basisposten.Groupe = postengroepen.id
where [VOO_Posten].werk = @werk  and Basisposten.OpdrachtgeverId = @opdrachtgeverId
Union
SELECT [Basisposten].[id] as 'Id',
null as 'Werk',
[Postengroepen].[Groep] as Groupe,
[Basisposten].[Poste] as 'Poste', 
[Basisposten].[Description] as 'Description', 
 [Basisposten].OpdrachtgeverId, 
0.00 as 'Nombre'
FROM [Basisposten] 
Inner join postengroepen on Basisposten.Groupe = postengroepen.id
where [Basisposten].[id] not in (SELECT [Basisposten].[id]
FROM [Basisposten] 
inner join VOO_Posten on Basisposten.Poste = VOO_Posten.poste
Inner join postengroepen on Basisposten.Groupe = postengroepen.id
where [VOO_Posten].werk = @werk)  and Basisposten.OpdrachtgeverId = @opdrachtgeverId
order by [Postengroepen].[Groep]" UpdateCommand="UPDATE [VOO_Posten] SET  [Nombre] = @Nombre WHERE [id] = @id">
                            <DeleteParameters>
                                <asp:Parameter Name="id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Werk" Type="Int32" />
                                <asp:Parameter Name="Poste" Type="String" />
                                <asp:Parameter Name="Description" Type="String" />
                                <asp:Parameter Name="Nombre" Type="Decimal" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:SessionParameter Name="werk" SessionField="uitgevoerdwerk" />
                                <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Nombre" Type="Decimal" />
                                <asp:Parameter Name="id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="Materiaux" Text="Materiaux">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <table id="TableArtikel" runat="server">
                            <tr >
                        <td><dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Article">
                        </dx:ASPxLabel>
                            </td>

                                <td>
                                    <dx:ASPxHiddenField ID="HiddenArtikel" runat="server" ClientInstanceName="HiddenArtikel">
                                    </dx:ASPxHiddenField>
                        <dx:ASPxComboBox ID="ASPxComboBoxArtikel" runat="server" DataSourceID="SqlDataSourceArtikelen" ClientInstanceName="comboboxArtikel" TextField="Article" ValueField="id" DropDownWidth="450px" TextFormatString="{0}; {1}" AutoPostBack="True">

                            <Columns>
                                <dx:ListBoxColumn FieldName="Article" Width="25px">
                                </dx:ListBoxColumn>
                                <dx:ListBoxColumn FieldName="Description">
                                </dx:ListBoxColumn>
                            </Columns>

                        </dx:ASPxComboBox>
                                </td>
                                 <td><dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Nombre">
                        </dx:ASPxLabel>
                            </td>
                                <td>
                        <dx:ASPxSpinEdit ID="ASPxSpinEditAantal" runat="server" Number="0" MaxValue="9999999">
                            <ClientSideEvents NumberChanged="CheckSerienummers" />
                        </dx:ASPxSpinEdit>
                                    </td>
                            </tr>
                            </table>

                        <table id="TableSerie">
                            <tr id="Serie1">
                                
                        <td>
                           
                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Numéro de série">
                        </dx:ASPxLabel>
                             </td>
                                <td>
                            <dx:ASPxTextBox ID="ASPxTextBoxSerie1" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                           </td>
                                </tr>
                                    <tr id="Serie2">
                        <td><dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Numéro de série">
                        </dx:ASPxLabel>
                             </td>
                                <td>
                            <dx:ASPxTextBox ID="ASPxTextBoxSerie2" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                           </td>
                                </tr>
                                    <tr id="Serie3">
                        <td class="auto-style1"><dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Numéro de série">
                        </dx:ASPxLabel>
                             </td>
                                <td class="auto-style1">
                            <dx:ASPxTextBox ID="ASPxTextBoxSerie3" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                           </td>
                                </tr>
                                    <tr id="Serie4">
                        <td><dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Numéro de série">
                        </dx:ASPxLabel>
                             </td>
                                <td>
                            <dx:ASPxTextBox ID="ASPxTextBoxSerie4" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                           </td>
                                </tr>
                                    <tr id="Serie5">
                        <td><dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Numéro de série">
                        </dx:ASPxLabel>
                             </td>
                                <td>
                            <dx:ASPxTextBox ID="ASPxTextBoxSerie5" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                           </td>
                                </tr>
                                    <tr id="Serie6">
                        <td><dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Numéro de série">
                        </dx:ASPxLabel>
                             </td>
                                <td>
                            <dx:ASPxTextBox ID="ASPxTextBoxSerie6" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                           </td>
                                </tr>
                                    <tr id="Serie7">
                        <td><dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Numéro de série">
                        </dx:ASPxLabel>
                             </td>
                                <td>
                                    
                            <dx:ASPxTextBox ID="ASPxTextBoxSerie7" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                           </td>
                                </tr>

                            <tr >
                        <td>

                                <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback1"
        OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="OnCallbackComplete" />
    </dx:ASPxCallback>

                                </td>
                                <td>
                                    <dx:ASPxButton ID="ASPxButtonToevoegen" runat="server" Width="50px" AutoPostBack="False">
                                        <ClientSideEvents Click="insertArticle" />
                                        <Image IconID="actions_add_32x32office2013">
                                        </Image>
                                    </dx:ASPxButton>
                                </td>
                                </tr>
                        </table>

                        
             
                                   

                        <dx:ASPxGridView ID="ASPxGridView2" ClientInstanceName="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMateriaux" KeyFieldName="id">
                            
                            <SettingsPager Mode="ShowAllRecords">
                            </SettingsPager>
                            <SettingsEditing Mode="Batch">
                                <BatchEditSettings StartEditAction="Click" />
                            </SettingsEditing>
                            <Settings ShowStatusBar="Hidden" />
                            <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
                            <ClientSideEvents BatchEditStartEditing="OnBatchEditStartEditing
" BatchEditEndEditing="OnBatchEditEndEditing" />
                            <SettingsSearchPanel Visible="True" />
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="id" ShowInCustomizationForm="True" VisibleIndex="0" ReadOnly="True" Visible="False">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Werk" ShowInCustomizationForm="True" VisibleIndex="1" Visible="False">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Article" ShowInCustomizationForm="True" VisibleIndex="2">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="3">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Nombre" ShowInCustomizationForm="True" VisibleIndex="4" Caption="Nombre">
                                    <PropertiesTextEdit>
                                        <ClientSideEvents KeyDown="OnKeyDownMat" />
                                        <MaskSettings Mask="&lt;0..9999&gt;&lt;,|.&gt;&lt;00..99&gt;" AllowMouseWheel="False" />
                                    </PropertiesTextEdit>
                                    <EditFormSettings Visible="True" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:ASPxGridView>

                        <asp:SqlDataSource ID="SqlDataSourceArtikelen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Basismateriaal] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) and (actief = 1 or actief is null)">
                            <SelectParameters>
                                <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" DefaultValue="1" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:SqlDataSource ID="SqlDataSourceMateriaux" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [VOO_Materiaal] WHERE [id] = @id" InsertCommand="INSERT INTO [VOO_Materiaal] ([Werk], [Article], [Description], [Nombre]) VALUES (@Werk, @Article, @Description, @Nombre)" SelectCommand="SELECT  [Voo_Materiaal].[id],
 [Voo_Materiaal].[Werk],
 [Voo_Materiaal].Article,
[Voo_Materiaal].[Description], 
 [Voo_Materiaal].Nombre 
FROM [Voo_Materiaal]
where [Voo_Materiaal].Werk = @werk " UpdateCommand="UPDATE [VOO_Materiaal] SET  [Nombre] = @Nombre WHERE [id] = @id">
                            <DeleteParameters>
                                <asp:Parameter Name="id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Werk" Type="Int32" />
                                <asp:Parameter Name="Article" Type="String" />
                                <asp:Parameter Name="Description" Type="String" />
                                <asp:Parameter Name="Nombre" Type="Decimal" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:SessionParameter Name="Werk" SessionField="uitgevoerdwerk" Type="Int32" DefaultValue="1500" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Nombre" Type="Decimal" />
                                <asp:Parameter Name="id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
        
    </dx:ASPxPageControl>
    <dx:ASPxLabel ID="clientLabel2" runat="server" Font-Bold="True"  ClientInstanceName="clientLabel2" ForeColor="#CC0000" Text="Remplissez le champ d'explication pour continuer (plus de 10 caractères)">
    </dx:ASPxLabel>
    <br />
    <br />
    <dx:ASPxLabel ID="ASPxLabelRegie" runat="server"  ClientInstanceName="clientLabel" Text="Explanation Temps de Regie">
    </dx:ASPxLabel>
    <br />
    &nbsp;<dx:ASPxButton runat="server" Text="Prêt" ClientInstanceName="clientButton" OnClick="ButtonKlaar_Click" HorizontalAlign="Center" Width="100px" Height="40px" style="float: right; margin-right: 10px; margin-bottom: 50px" ID="buttonKlaar" Enabled="False"></dx:ASPxButton>

    <dx:ASPxTextBox ID="ASPxTextBoxRegie" ClientInstanceName="clientText" runat="server" Width="60%" AutoPostBack="True">
    </dx:ASPxTextBox>

</asp:Content>
