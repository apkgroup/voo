﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class BestellingPlaatsenBobijn
    Inherits System.Web.UI.Page
    Protected Sub vertaal(taalid)
        If taalid = 1 Then
            Literal2.Text = "Plaats een bestelling"
            saveChangesBtn.Text = "Bestellen"
            ASPxButton1.Text = "Bestellen"
            ASPxLabel1.Text = "Bestel voor"
            ASPxButton2.Text = "Annuleren"
            cancelChangesBtn.Text = "Annuleren"
            ASPxGridView1.Columns("Article").Caption = "Artikel"
            ASPxGridView1.Columns("Description").Caption = "Omschrijving"
            ASPxGridView1.Columns("Nombre").Caption = "Aantal"
        Else
            Literal2.Text = "Passer un commande"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Dim voocontext As New VooEntities
        Dim opdrachtgeverid As Integer = Session("Opdrachtgever")
        voocontext.Opdrachtgever.Find(opdrachtgeverid)

        If Not Session("isadmin") Then
            ASPxComboBox1.Visible = False
            ASPxLabel1.Visible = False
            If voocontext.Opdrachtgever.Find(opdrachtgeverid).tag = "SYN" Then
                ASPxGridView1.DataSourceID = "SqlDataSourceMateriauxSYN"
            Else
                ASPxGridView1.DataSourceID = "SqlDataSourceMateriauxSYN"
            End If

            ASPxGridView1.DataBind()

        Else

            If voocontext.Opdrachtgever.Find(opdrachtgeverid).tag = "SYN" Then
                ASPxGridView1.DataSourceID = "SqlDataSourceMateriauxADMINSYN"
            Else
                ASPxGridView1.DataSourceID = "SqlDataSourceMateriauxADMINSYN"
            End If

            ASPxGridView1.DataBind()
        End If




    End Sub
    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()



            Dim editingRowVisibleIndex As Integer = ASPxGridView1.FindVisibleIndexByKeyValue(e.Keys.Values(0))
            Dim context As New VooEntities
            Try
                If Not ASPxCheckBoxNieuw.Checked Then


                    Dim check_sql As String = "select id from BestellijnBobijn where bobijnmateriaalId= @materiaalId and bobijnbestellingId = @BestellingId"
                    Dim check_cmd As New SqlCommand(check_sql, cn)
                    Dim check_par As New SqlParameter("@BestellingId", SqlDbType.NVarChar, -1) With {.Value = Session("BobijnBestellingId")}
                    check_cmd.Parameters.Add(check_par)
                    check_par = New SqlParameter("@materiaalId", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Id").ToString()}
                    check_cmd.Parameters.Add(check_par)
                    Dim dr As SqlDataReader = check_cmd.ExecuteReader()
                    If dr.HasRows Then
                        dr.Read()
                        If Not dr.IsDBNull(0) Then
                            'update

                            Dim id As Decimal = dr.GetInt32(0)
                            dr.Close()
                            Dim s_SQL As String = "UPDATE BestellijnBobijn set hoeveelheid = hoeveelheid + @aantal where id = @id"

                            Dim cmd As New SqlCommand(s_SQL, cn)
                            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = id}
                            cmd.Parameters.Add(par)
                            par = New SqlParameter("@aantal", SqlDbType.Decimal) With {.Value = e.NewValues(0)}
                            cmd.Parameters.Add(par)
                            If e.NewValues(0) > 0 Then
                                cmd.ExecuteNonQuery()
                            End If
                        End If

                    Else
                        dr.Close()
                        Dim s_SQL As String = "Insert into BestellijnBobijn (bobijnmateriaalId, bobijnbestellingId, hoeveelheid ) VALUES (@materiaalid, @bestellingId, @hoeveelheid)"

                        Dim cmd As New SqlCommand(s_SQL, cn)
                        Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = Session("BobijnBestellingId")}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@materiaalId", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Id").ToString()}
                        cmd.Parameters.Add(par)
                        par = New SqlParameter("@hoeveelheid", SqlDbType.Decimal) With {.Value = e.NewValues(0)}
                        cmd.Parameters.Add(par)
                        If e.NewValues(0) > 0 Then
                            cmd.ExecuteNonQuery()

                        End If


                    End If
                Else
                    Dim s_SQL As String = "Insert into BestellijnBobijn (bobijnmateriaalId, bobijnbestellingId, hoeveelheid ) VALUES (@materiaalid, @bestellingId, @hoeveelheid)"

                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter("@bestellingId", SqlDbType.Int) With {.Value = Session("BobijnBestellingId")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@materiaalId", SqlDbType.NVarChar) With {.Value = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Id").ToString()}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@hoeveelheid", SqlDbType.Decimal) With {.Value = e.NewValues(0)}
                    cmd.Parameters.Add(par)
                    If e.NewValues(0) > 0 Then
                        cmd.ExecuteNonQuery()


                    End If
                End If


                Dim voocontext As New VooEntities
                Dim opid As Integer = Session("Opdrachtgever")
                Dim hoofdmagid As Integer = voocontext.Magazijn.Where(Function(x) x.OpdrachtgeverId = opid And x.Hoofd = True).FirstOrDefault.id
                Dim matid As Integer = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Id")
                If voocontext.StockMagazijn.Where(Function(x) x.MagazijnId = hoofdmagid And x.MateriaalId = matid).FirstOrDefault.Aantal < e.NewValues(0) And Session("TAG") = "SYN" Then
                    Dim addresslist As New List(Of String)

                    addresslist.Add("bert@apkgroup.eu")
                    addresslist.Add("lars.agten@apkgroup.eu")
                    addresslist.Add("kevin.mondelaers@apkgroup.eu")
                    Dim userid As Int16 = Session("userid")
                    addresslist.Add(voocontext.Gebruikers.Find(userid).Email)
                    mailGoogle("Bobijnbestelling voor artikel  " & ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Article").ToString(), "no.reply@apkgroup.eu", addresslist, "Bestelling geplaatst voor bobijnartikel " & ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Article").ToString() & "(" & ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Description").ToString() & ") waarvan er niet meer voldoende stock resterend is https://wms.apkgroup.eu/BobijnBestelling.aspx?id=" & Session("BobijnBestellingId"), New Dictionary(Of String, Byte()))


                End If






            Catch ex As Exception
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij plaatsen bobijnbestelling {0} {1} {2}", Session("naam"), Session("voornaam"), ex.Message)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End Try





        End Using

    End Sub


    Protected Sub ASPxGridView1_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles ASPxGridView1.BatchUpdate
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()

            Dim tempId As New Integer
            Try
                If Not ASPxCheckBoxNieuw.Checked Then


                    Dim check_sql As String = "select id from BestellingBobijn where GebruikerId= @gebruikerId and Status = 1"
                    Dim check_cmd As New SqlCommand(check_sql, cn)
                    Dim check_par As New SqlParameter
                    If Not Session("isadmin") Then
                        check_par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = Session("userid")}

                    Else
                        check_par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
                    End If
                    check_cmd.Parameters.Add(check_par)
                    Dim dr As SqlDataReader = check_cmd.ExecuteReader()
                    If dr.HasRows Then
                        dr.Read()
                        If Not dr.IsDBNull(0) Then
                            tempId = dr.GetInt32(0)
                        End If

                        dr.Close()

                        Dim s_SQL As String = "UPDATE  BestellingBobijn set Datum = GetDate() where id = @id"

                        Dim cmd As New SqlCommand(s_SQL, cn)
                        Dim par As New SqlParameter

                        par = New SqlParameter("@id", SqlDbType.Int) With {.Value = tempId}

                        cmd.Parameters.Add(par)
                        cmd.ExecuteNonQuery()

                    Else
                        dr.Close()
                        Dim s_SQL As String = "Insert into BestellingBobijn (Datum, GebruikerId, Status,Opmerking,Deadline) VALUES (GetDate(), @gebruikerId, 1,@opmerking, @deadline); SELECT SCOPE_IDENTITY()"

                        Dim cmd As New SqlCommand(s_SQL, cn)
                        Dim par As New SqlParameter

                        If ASPxDateEditDeadline.Value IsNot Nothing Then
                            par = New SqlParameter("@deadline", SqlDbType.Date, -1) With {.Value = ASPxDateEditDeadline.Value}

                        Else
                            par = New SqlParameter("@deadline", SqlDbType.Date, -1) With {.Value = DBNull.Value}
                        End If
                        cmd.Parameters.Add(par)

                        If Not String.IsNullOrWhiteSpace(ASPxMemoOpmerking.Value) Then
                            par = New SqlParameter("@opmerking", SqlDbType.NVarChar, -1) With {.Value = ASPxMemoOpmerking.Value}

                        Else
                            par = New SqlParameter("@opmerking", SqlDbType.NVarChar, -1) With {.Value = DBNull.Value}
                        End If
                        cmd.Parameters.Add(par)

                        If Not Session("isadmin") Then
                            par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = Session("userid")}

                        Else
                            par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
                        End If
                        cmd.Parameters.Add(par)
                        tempId = cmd.ExecuteScalar()

                    End If
                Else
                    Dim s_SQL As String = "Insert into BestellingBobijn (Datum, GebruikerId, Status,Opmerking,Deadline) VALUES (GetDate(), @gebruikerId, 1,@opmerking, @deadline); SELECT SCOPE_IDENTITY()"

                    Dim cmd As New SqlCommand(s_SQL, cn)
                    Dim par As New SqlParameter

                    If ASPxDateEditDeadline.Value IsNot Nothing Then
                        par = New SqlParameter("@deadline", SqlDbType.Date, -1) With {.Value = ASPxDateEditDeadline.Value}

                    Else
                        par = New SqlParameter("@deadline", SqlDbType.Date, -1) With {.Value = DBNull.Value}
                    End If
                    cmd.Parameters.Add(par)

                    If Not String.IsNullOrWhiteSpace(ASPxMemoOpmerking.Value) Then
                        par = New SqlParameter("@opmerking", SqlDbType.NVarChar, -1) With {.Value = ASPxMemoOpmerking.Value}

                    Else
                        par = New SqlParameter("@opmerking", SqlDbType.NVarChar, -1) With {.Value = DBNull.Value}
                    End If
                    cmd.Parameters.Add(par)

                    If Not Session("isadmin") Then
                        par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = Session("userid")}

                    Else
                        par = New SqlParameter("@gebruikerId", SqlDbType.NVarChar, -1) With {.Value = ASPxComboBox1.SelectedItem.Value}
                    End If
                    cmd.Parameters.Add(par)
                    tempId = cmd.ExecuteScalar()
                End If


                Session("BobijnBestellingId") = tempId
                If Not Session("isadmin") Then



                    Dim voocontext As New VooEntities
                    Dim opdrachtgeverId As Integer = Session("opdrachtgever")
                    Dim opdrachtgever = voocontext.Opdrachtgever.Find(opdrachtgeverId)
                    Dim addresslist As New List(Of String)

                    Select Case opdrachtgever.id
                        Case 14 To 22
                            addresslist.Add("remo.ciarlariello@apkgroup.eu")

                        Case 26 To 29
                            addresslist.Add("remo.ciarlariello@apkgroup.eu")
                        Case 32 To 32
                            addresslist.Add("remo.ciarlariello@apkgroup.eu")
                        Case 6 To 11
                            addresslist.Add("magazijnaartrijke@apkgroup.eu")
                        Case 31
                            addresslist.Add("magazijnaartrijke@apkgroup.eu")
                        Case 32, 33
                            addresslist.Add("magazijnaartrijke@apkgroup.eu")
                    End Select

                    If addresslist.Count > 0 Then

                        mailGoogle("Nieuwe bobijn bestelling voor gebruiker" & Session("naam"), "remo.ciarlariello@apkgroup.eu", addresslist, "Er staat een nieuwe bestelling klaar voor het magazijn" & opdrachtgever.naam & " van een gebruiker op WMS op https://wms.apkgroup.eu/Bestelling.aspx?id=" & tempId, New Dictionary(Of String, Byte()))

                    End If
                End If
            Catch ex As Exception
                Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij update bobijnbestelling door {0} {1} {2}", Session("naam"), Session("voornaam"), ex.Message)}
                cmd.Parameters.Add(par)
                cmd.ExecuteNonQuery()
            End Try




        End Using
    End Sub



    Protected Sub ASPxGridView1_AfterPerformCallback(sender As Object, e As ASPxGridViewAfterPerformCallbackEventArgs) Handles ASPxGridView1.AfterPerformCallback
        ' MAIL VERZENDEN
        'Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
        '    cn.Open()
        '    Dim s_SQL As String = ""
        '    Dim cmd As SqlCommand = Nothing
        '    Dim par As SqlParameter = Nothing
        '    Dim varBericht As String = ""
        '    Dim varOnderwerp As String = ""
        '    Dim varBeheerder As String = ""
        '    Dim varEmail As String = ""
        '    s_SQL = "SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)"
        '    cmd = New SqlCommand(s_SQL, cn)
        '    par = New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Melding bestelling aangevraagd"}
        '    cmd.Parameters.Add(par)
        '    Dim dr As SqlDataReader = cmd.ExecuteReader
        '    If dr.HasRows Then
        '        dr.Read()
        '        If Not dr.IsDBNull(0) Then
        '            varBericht = dr.GetString(0)
        '            varOnderwerp = dr.GetString(1)
        '        End If
        '    End If
        '    dr.Close()
        '    s_SQL = "SELECT vwWerkn.WERKNEMER, Gebruikers_1.Email FROM Gebruikers INNER JOIN Gebruikers Gebruikers_1 ON Gebruikers.VerlofGoedkeuring=Gebruikers_1.id " _
        '        & "INNER JOIN (SELECT NAAM + ' ' + VNAAM as WERKNEMER, [ON],NR FROM Elly_SQL.dbo.WERKN) vwWerkn ON Gebruikers_1.Werkg=vwWerkn.[ON] " _
        '        & "AND Gebruikers_1.Werkn=vwWerkn.NR WHERE (Gebruikers.id=@gebruiker)"
        '    cmd = New SqlCommand(s_SQL, cn)
        '    par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Session("userid")}
        '    cmd.Parameters.Add(par)
        '    dr = cmd.ExecuteReader
        '    If dr.HasRows Then
        '        dr.Read()
        '        varBeheerder = dr.GetString(0)
        '        varEmail = dr.GetString(1)
        '    End If
        '    dr.Close()
        '    Dim exch As ExchangeService = New ExchangeService(ExchangeVersion.Exchange2010_SP2)
        '    exch.Url = New Uri("https://mail.apkintern.be/EWS/Exchange.asmx")
        '    exch.UseDefaultCredentials = False
        '    exch.Credentials = New System.Net.NetworkCredential("Administrator", "E01ab107%", "apk")
        '    TrustAllCertificatePolicy.OverrideCertificateValidation()
        '    exch.ImpersonatedUserId = New ImpersonatedUserId(ConnectingIdType.PrincipalName, Session("mailaccount"))
        '    Dim message As New EmailMessage(exch)
        '    message.Subject = varOnderwerp
        '    Dim varBody As String = varBericht.Replace("[WERKNEMER]", String.Format("{0} {1}", Session("naam"), Session("voornaam")))
        '    varBody = varBody.Replace("[BEHEERDER]", varBeheerder)
        '    varBody = varBody.Replace("[URL]", String.Format("{0}/VerlofaanvragenVerwerken.aspx?g={1}", Session("domein"), Session("userid")))
        '    varBody = varBody.Replace("[SITETITEL]", Session("sitetitel"))
        '    Dim sBody As String = "<x-html>" & vbCrLf
        '    sBody = sBody & varBody
        '    sBody = sBody & "</x-html>"
        '    message.Body = sBody
        '    message.ToRecipients.Add(varEmail)
        '    message.Send()
        'End Using


        If Session("BobijnBestellingId") <> 0 And Not Session("BobijnBestellingId") Is Nothing Then
            Dim tempid As String = Session("BobijnBestellingId")
            ASPxWebControl.RedirectOnCallback(VirtualPathUtility.ToAbsolute("~/BestellingCompleet.aspx?id=" + tempid))
        End If



    End Sub

    Protected Sub ASPxGridView1_CellEditorInitialize(sender As Object, e As ASPxGridViewEditorEventArgs) Handles ASPxGridView1.CellEditorInitialize

    End Sub

    Protected Sub ASPxGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles ASPxGridView1.SelectionChanged

    End Sub
End Class