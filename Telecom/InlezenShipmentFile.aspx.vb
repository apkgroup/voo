﻿Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web
Imports Microsoft.ajax

Imports DevExpress.Utils
Imports System.Xml.Serialization
Imports System.Xml

Public Class InlezenShipmentFile
    Inherits System.Web.UI.Page
    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property

    'Protected Overrides Sub Initializeculture()
    '    SetCulture()
    '    MyBase.InitializeCulture()

    'End Sub
    'Private Sub SetCulture()
    '    Culture = Session("globalize").ToString
    '    UICulture = Session("globalize").ToString
    '    Threading.Thread.CurrentThread.CurrentCulture = Globalization.CultureInfo.CreateSpecificCulture(Session("globalize").ToString)
    '    Threading.Thread.CurrentThread.CurrentUICulture = New Globalization.CultureInfo(Session("globalize").ToString)
    'End Sub
    Protected Sub vertaal(taalid As Integer)
        If taalid = 1 Then

            Literal2.Text = "Kies een shipment file om te uploaden en druk op de upload knop. <br/>Het uploaden van het bestand kan enige tijd in beslag nemen. Gelieve de pagina ondertussen niet te sluiten.<br/>Er worden ongeveer 23 lijnen per seconde verwerkt..."


        Else

            Literal2.Text = "Materiaux"


        End If

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        vertaal(Session("taal"))

        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If


        Dim opdrachtgeverId As Integer = Session("Opdrachtgever")
        Dim voocontext As New VooEntities
        Dim magazijnId As Integer






        voocontext.Dispose()

        'If Not Session("isingelogd") Then
        '    Session("url") = Request.Url.AbsoluteUri
        '    Response.Redirect("~/Login.aspx", False)
        '    Context.ApplicationInstance.CompleteRequest()

        'End If




        If Page.IsPostBack Then
            ASPxLabelFout.Text = Session("fout")
            ASPxLabelGelukt.Text = Session("Gelukt")



            Dim i As Int32 = 1

        Else
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

                cn.Open()
                Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                cmd.Parameters.Add(par)
                Dim dr2 As SqlDataReader = cmd.ExecuteReader
                If dr2.HasRows Then
                    dr2.Read()
                    Session("MagazijnId") = dr2.GetInt32(0)
                End If
                dr2.Close()
                cn.Close()
            End Using
            Session("fout") = ""
            Session("Gelukt") = ""

            ASPxLabelFout.Text = Session("fout")
            ASPxLabelGelukt.Text = Session("Gelukt")
        End If
        Session("fout") = ""
        Session("Gelukt") = ""




    End Sub


    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct shipmentId from [Serienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("shipmentId"))
        Next row
        Session("SelectResult") = result
    End Sub
    Protected Sub ASPxGridView1_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBinding
        DoSelect(SqlDataSourceShipments.ConnectionString)
    End Sub


    Protected Sub ASPxGridView2_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("shipment") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub


    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender

        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub

    Protected Sub ASPxComboBoxMagazijn_ValueChanged(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.ValueChanged
        Session("MagazijnId") = ASPxComboBoxMagazijn.Value
    End Sub

    Protected Sub ASPxButton1_Click(sender As Object, e As EventArgs) Handles ASPxFormLayout1_E1.Click

        Session("fout") = ""
        Session("Gelukt") = ""
        Dim opid As Integer = Session("opdrachtgever")
        Dim shipmentnumber As String

        Dim sb As New StringBuilder
        Dim entities As New VooEntities
        Dim fouten As New List(Of String)

        'Dim stockmags = entities.StockMagazijn.ToList
        ' Dim mats = entities.Basismateriaal.ToList
        Dim magId As Int32 = Convert.ToInt32(Session("MagazijnId"))



        For Each f In ASPxUploadControlShipment.UploadedFiles
            Dim fi As FileInfo

            Try
                fi = New FileInfo(f.FileName)
            Catch ex As Exception

                fouten.Add("Geen shipment file gekozen")
                For Each fout In fouten
                    Session("fout") = Session("fout") + fout + "<br/>"
                Next
                ASPxLabelFout.Text = Session("fout")
                ASPxLabelGelukt.Text = Session("Gelukt")
                Return

            End Try



            Dim ship As New Shipment

            If fi.Extension.Contains("xml") Then
                Try
                    Dim xRoot As XmlRootAttribute = New XmlRootAttribute()
                    xRoot.ElementName = "ShipmentFile"


                    Dim str As String = New StreamReader(f.PostedFile.InputStream).ReadToEnd()

                    If str.Contains("fluvius-shipment-file-meter-elec-customer") Then
                        xRoot.Namespace = "http://www.sagemcom.com/shipmentfile/fluvius-shipment-file-meter-elec-customer/2018-01"
                    ElseIf str.Contains("fluvius-shipment-file-meter-gaz-customer") Then
                        xRoot.Namespace = "http://www.sagemcom.com/shipmentfile/fluvius-shipment-file-meter-gaz-customer/2018-01"
                    ElseIf str.Contains("water-amm") Then
                        xRoot.Namespace = "http://www.sagemcom.com/shipmentfile/fluvius-shipment-file-meter-water-amm/2018-01"
                    End If
                    Dim serializer As New XmlSerializer(GetType(ShipmentFile), xRoot)
                    Dim deserialized As ShipmentFile = Nothing
                    deserialized = serializer.Deserialize(f.PostedFile.InputStream)


                    If entities.Shipment.Where(Function(x) x.shipmentNumber = deserialized.Body.Shipment.Header.Delivery_Note_Number And x.accepted).Any Then
                        fouten.Add("Shipment File met nummer " + deserialized.Body.Shipment.Header.Delivery_Note_Number + " bestaat al reeds in het systeem. Operatie afgebroken.")
                        For Each fout In fouten
                            Session("fout") = Session("fout") + fout + "<br/>"
                        Next
                        ASPxLabelFout.Text = Session("fout")
                        ASPxLabelGelukt.Text = Session("Gelukt")
                        Return
                    Else
                        ship.datereceived = Date.Now
                        ship.shipmentNumber = deserialized.Body.Shipment.Header.Delivery_Note_Number
                        ship.vrijgegeven = False
                        ship.uploadedby = Session("userid")
                        ship.accepted = False
                        ship.magazijnId = magId
                        ship.opdrachtgeverId = Session("opdrachtgever")
                        entities.Shipment.Add(ship)
                        entities.SaveChanges()
                    End If
                    Dim listserie As New List(Of String)
                    For Each pallet In deserialized.Body.Shipment.Pallet
                        For Each box In pallet.Box
                            For Each device In box.Device_attributes
                                If listserie.Contains(device.General.Serial_number) Then
                                    fouten.Add("Serienummer " + device.General.Serial_number.ToString + " staat dubbel in de shipping file.")
                                    Dim serie As New Serienummer

                                End If
                                If entities.Serienummer.Where(Function(x) x.serienummer1 = device.General.Serial_number).Any Then
                                    fouten.Add("Serienummer " + device.General.Serial_number + " bestaat al reeds in het systeem")
                                Else
                                    Dim matnum As String = device.General.Customer_Article_Number
                                    Dim mat As Basismateriaal
                                    Dim gevondenMats = entities.Basismateriaal.Where(Function(x) x.Article = matnum)
                                    If gevondenMats.Count > 1 Then
                                        For Each gevonden In gevondenMats
                                            If gevonden.MateriaalOpdrachtgevers.Where(Function(x) x.opdrachtgeverId = opid).Any Then
                                                mat = gevonden
                                            End If
                                        Next
                                    Else
                                        mat = gevondenMats.FirstOrDefault
                                    End If

                                    Dim regexOk As Boolean = True
                                    If mat Is Nothing Then
                                        fouten.Add("Materiaal met nummer " + device.General.Customer_Article_Number + " niet gevonden.")
                                    Else
                                        If Not String.IsNullOrEmpty(mat.serienummerRegex) Then
                                            Dim regex As Regex = New Regex(mat.serienummerRegex)
                                            Dim match As Match = regex.Match(device.General.Serial_number)
                                            If Not match.Success Then
                                                regexOk = False
                                            End If
                                        End If
                                        If regexOk Then
                                            Dim StockMag = entities.StockMagazijn.Where(Function(x) x.MateriaalId = mat.id And x.MagazijnId = magId).FirstOrDefault

                                            StockMag.Aantal = StockMag.Aantal + 1
                                            Dim sr As New Serienummer With {.StockMagazijnId = StockMag.id,
                                                .shipmentId = ship.id,
                                                .serienummer1 = device.General.Serial_number,
                                                .statusId = 3,
                                                .FIFO = True,
                                                .datumGemaakt = Date.Now,
                                                .uitgeboekt = False,
                                                .datumOntvangstMagazijn = Date.Now,
                                                .boxNo = box.Box_id,
                                                .palletNo = pallet.Pallet_id}

                                            entities.Serienummer.Add(sr)

                                        Else
                                            fouten.Add("Serienummer " + device.General.Serial_number + " komt niet overeen met ingesteld patroon " + mat.serienummerRegex)
                                        End If


                                    End If
                                End If


                            Next
                        Next
                    Next
                Catch ex As Exception
                    fouten.Add("Onbekende fout: " & ex.Message & " " & ex.InnerException.Message)

                End Try

            Else
                If Session("TAG") = "SYN" Then
                    'begin

                    Dim byteData() As Byte
                    byteData = f.FileBytes
                    Dim guidstring = Guid.NewGuid.ToString
                    Using wfile As New FileStream("/temp/" + guidstring + ".csv", FileMode.Append)
                        wfile.Write(byteData, 0, byteData.Length)
                        wfile.Close()
                    End Using

                    Dim fi2 As FileInfo

                    Try
                        fi2 = New FileInfo(f.FileName)
                    Catch ex As Exception

                        fouten.Add("Geen boekingen file gekozen")
                        For Each fout In fouten
                            Session("fout") = Session("fout") + fout + "<br/>"
                        Next
                        ASPxLabelFout.Text = Session("fout")
                        ASPxLabelGelukt.Text = Session("Gelukt")
                        Return

                    End Try

                    Dim wb As New DevExpress.Spreadsheet.Workbook

                    Using stream As New FileStream("/temp/" + guidstring + ".csv", FileMode.Open)
                        wb.LoadDocument(stream)
                    End Using

                    Dim ws As DevExpress.Spreadsheet.Worksheet = wb.Worksheets.FirstOrDefault
                    Dim bereik As String = ws.GetUsedRange.GetReferenceA1.Replace("A1:", "A2:")
                    Dim range As DevExpress.Spreadsheet.CellRange = ws.Range(bereik)
                    Dim test = range.RowCount

                    shipmentnumber = New String(f.FileName.Take(50).ToArray())
                    If entities.Shipment.Where(Function(x) x.shipmentNumber = shipmentnumber And x.accepted).Any Then
                        fouten.Add("Shipment File met nummer " + shipmentnumber + " bestaat al reeds in het systeem. Operatie afgebroken.")
                        For Each fout In fouten
                            Session("fout") = Session("fout") + fout + "<br/>"
                        Next
                        ASPxLabelFout.Text = Session("fout")
                        ASPxLabelGelukt.Text = Session("Gelukt")
                        Return
                    Else

                        ship.datereceived = Date.Now
                        ship.shipmentNumber = shipmentnumber
                        ship.vrijgegeven = False
                        ship.uploadedby = Session("userid")
                        ship.accepted = False
                        ship.magazijnId = magId
                        ship.opdrachtgeverId = Session("opdrachtgever")
                        entities.Shipment.Add(ship)
                        entities.SaveChanges()
                    End If


                    For i As Integer = 2 To range.RowCount + 1
                        If Not String.IsNullOrEmpty(ws.Cells("A" + i.ToString).Value.ToString) Then

                            Dim matnum As String = ws.Cells("A" + i.ToString).Value.ToString
                            Dim aantal As Integer = ws.Cells("D" + i.ToString).Value.NumericValue
                            Dim mat As Basismateriaal

                                Dim gevondenMats = entities.Basismateriaal.Where(Function(x) x.Article = matnum)
                            If gevondenMats.Count > 1 Then
                                For Each gevonden In gevondenMats
                                    If gevonden.MateriaalOpdrachtgevers.Where(Function(x) x.opdrachtgeverId = opid).Any Then
                                        mat = gevonden
                                    End If
                                Next
                            Else
                                mat = gevondenMats.FirstOrDefault
                            End If
                            If mat Is Nothing Then
                                Continue For
                            Else

                                Dim StockMag = entities.StockMagazijn.Where(Function(x) x.MateriaalId = mat.id And x.MagazijnId = magId).FirstOrDefault

                                If StockMag Is Nothing Then
                                    Continue For
                                Else
                                    StockMag.Aantal = StockMag.Aantal + aantal

                                    'Dim stockb As New Stockbeweging
                                    'stockb.beweging = aantal
                                    'stockb.datum = Today

                                    'stockb.gebruiker = Session("userid")

                                    'stockb.stockmagazijnId = StockMag.id
                                    'stockb.opmerking = "Inlezing stock via CSV Synductis"

                                    'entities.Stockbeweging.Add(stockb)
                                    'entities.SaveChanges()
                                End If
                            End If
                        End If
                    Next
                    'end
                Else






                    Dim wb As New DevExpress.Spreadsheet.Workbook
                    wb.LoadDocument(f.FileBytes)
                    Dim ws As DevExpress.Spreadsheet.Worksheet = wb.Worksheets.FirstOrDefault
                    Dim bereik As String = ws.GetUsedRange.GetReferenceA1.Replace("A1:", "A2:")
                    Dim range As DevExpress.Spreadsheet.CellRange = ws.Range(bereik)
                    Dim test = range.RowCount
                    shipmentnumber = ws.Cells("E2").Value.ToString
                    If entities.Shipment.Where(Function(x) x.shipmentNumber = shipmentnumber And x.accepted).Any Then
                        fouten.Add("Shipment File met nummer " + ws.Cells("E2").Value.ToString + " bestaat al reeds in het systeem. Operatie afgebroken.")
                        For Each fout In fouten
                            Session("fout") = Session("fout") + fout + "<br/>"
                        Next
                        ASPxLabelFout.Text = Session("fout")
                        ASPxLabelGelukt.Text = Session("Gelukt")
                        Return
                    Else

                        ship.datereceived = Date.Now
                        ship.shipmentNumber = ws.Cells("E2").Value.ToString
                        ship.vrijgegeven = False
                        ship.uploadedby = Session("userid")
                        ship.accepted = False
                        ship.magazijnId = magId
                        ship.opdrachtgeverId = Session("opdrachtgever")
                        entities.Shipment.Add(ship)
                        entities.SaveChanges()
                    End If

                    Dim listserie As New List(Of String)
                    For i As Integer = 2 To range.RowCount + 1
                        If Not String.IsNullOrEmpty(ws.Cells("L" + i.ToString).Value.ToString) Then
                            Dim serienum As String = ws.Cells("K" + i.ToString).Value.ToString
                            If listserie.Contains(serienum) Then
                                fouten.Add("Serienummer " + ws.Cells("K" + i.ToString).Value.ToString + " staat dubbel in de shipping file.")
                            End If
                            listserie.Add(serienum)

                            If entities.Serienummer.Where(Function(x) x.serienummer1 = serienum).Any Then
                                fouten.Add("Serienummer " + ws.Cells("K" + i.ToString).Value.ToString + " bestaat al reeds in het systeem")
                            Else
                                Dim matnum As String = ws.Cells("L" + i.ToString).Value.ToString

                                Dim mat As Basismateriaal

                                Dim gevondenMats = entities.Basismateriaal.Where(Function(x) x.Article = matnum)
                                If gevondenMats.Count > 1 Then
                                    For Each gevonden In gevondenMats
                                        If gevonden.MateriaalOpdrachtgevers.Where(Function(x) x.opdrachtgeverId = opid).Any Then
                                            mat = gevonden
                                        End If
                                    Next
                                Else
                                    mat = gevondenMats.FirstOrDefault
                                End If
                                Dim regexOk As Boolean = True
                                If mat Is Nothing Then
                                    fouten.Add("Materiaal met nummer " + ws.Cells("L" + i.ToString).Value.ToString + " niet gevonden.")
                                Else
                                    If Not String.IsNullOrEmpty(mat.serienummerRegex) Then
                                        Dim regex As Regex = New Regex(mat.serienummerRegex)
                                        Dim match As Match = regex.Match(serienum)
                                        If Not match.Success Then
                                            regexOk = False
                                        End If
                                    End If
                                    If regexOk Then
                                        Dim StockMag = entities.StockMagazijn.Where(Function(x) x.MateriaalId = mat.id And x.MagazijnId = magId).FirstOrDefault

                                        If StockMag Is Nothing Then
                                            fouten.Add("Materiaal met nummer " + ws.Cells("L" + i.ToString).Value.ToString + " niet gevonden bij dit magazijn")
                                        Else



                                            StockMag.Aantal = StockMag.Aantal + 1
                                            Dim sr As New Serienummer With {.StockMagazijnId = StockMag.id,
                                            .shipmentId = ship.id,
                                            .serienummer1 = serienum,
                                            .statusId = 3,
                                            .FIFO = True,
                                            .datumGemaakt = Date.Now,
                                            .uitgeboekt = False,
                                            .datumOntvangstMagazijn = Date.Now,
                                            .boxNo = ws.Cells("J" + i.ToString).Value.ToString,
                                            .palletNo = ws.Cells("I" + i.ToString).Value.ToString}

                                            entities.Serienummer.Add(sr)
                                        End If
                                    Else
                                        fouten.Add("Serienummer " + serienum + " komt niet overeen met ingesteld patroon " + mat.serienummerRegex)
                                    End If


                                End If

                            End If
                        End If
                    Next

                End If

            End If

            If fouten.Count = 0 Then
                ship.accepted = True

                entities.SaveChanges()

                Session("Gelukt") = "Shipment met nummer " + shipmentnumber + " successvol ingeladen vanuit bestand: " + fi.Name + ". Artikelen zijn ingeladen in het magazijn met status: Te ontvangen."
                Response.Redirect("~/InlezenShipmentFile.aspx", False)
                Context.ApplicationInstance.CompleteRequest()
            Else
                For Each fout In fouten
                    Session("fout") = Session("fout") + fout + "<br/>"
                Next

            End If

        Next

        ASPxLabelFout.Text = Session("fout")
        ASPxLabelGelukt.Text = Session("Gelukt")
    End Sub




End Class

Public Class NamespaceIgnorantXmlTextReader
    Inherits XmlTextReader

    Public Sub New(ByVal reader As System.IO.TextReader)
        MyBase.New(reader)
    End Sub

    Public Overrides ReadOnly Property NamespaceURI As String
        Get
            Return ""
        End Get
    End Property
End Class

Public Class XTWFND
    Inherits XmlTextWriter

    Public Sub New(ByVal w As System.IO.TextWriter)
        MyBase.New(w)
        Formatting = System.Xml.Formatting.Indented
    End Sub

    Public Overrides Sub WriteStartDocument()
    End Sub
End Class


<XmlRoot(ElementName:="Main")>
Public Class Main
    <XmlElement(ElementName:="Company")>
    Public Property Company As String
    <XmlElement(ElementName:="Supplier")>
    Public Property Supplier As String
End Class

<XmlRoot(ElementName:="Header")>
Public Class Header
    <XmlElement(ElementName:="Delivery_Note_Number")>
    Public Property Delivery_Note_Number As String
    <XmlElement(ElementName:="Device_type")>
    Public Property Device_type As String
    <XmlElement(ElementName:="Configuration_version")>
    Public Property Configuration_version As String
    <XmlElement(ElementName:="Shipment_date")>
    Public Property Shipment_date As String
    <XmlElement(ElementName:="Order_id")>
    Public Property Order_id As String
    <XmlElement(ElementName:="Order_position_id")>
    Public Property Order_position_id As String
End Class

<XmlRoot(ElementName:="General")>
Public Class General

    <XmlElement(ElementName:="Module_active_firmware_version", IsNullable:=True)>
    Public Property Module_active_firmware_version As String
    <XmlElement(ElementName:="Serial_number")>
    Public Property Serial_number As String
    <XmlElement(ElementName:="Serial_number_Metro", IsNullable:=True)>
    Public Property Serial_number_Metro As String
    <XmlElement(ElementName:="Customer_Article_Number")>
    Public Property Customer_Article_Number As String
    <XmlElement(ElementName:="Customer_Lot_Number")>
    Public Property Customer_Lot_Number As String
    <XmlElement(ElementName:="Test_report")>
    Public Property Test_report As String
    <XmlElement(ElementName:="Certification_Year")>
    Public Property Certification_Year As String
    <XmlElement(ElementName:="Certification_Number_Report", IsNullable:=True)>
    Public Property Certification_Number_Report As String
    <XmlElement(ElementName:="Device_batch_number", IsNullable:=True)>
    Public Property Device_batch_number As String
    <XmlElement(ElementName:="Communication_method")>
    Public Property Communication_method As String
    <XmlElement(ElementName:="Year_of_manufactory")>
    Public Property Year_of_manufactory As String
    <XmlElement(ElementName:="Hardware_version")>
    Public Property Hardware_version As String
    <XmlElement(ElementName:="Core_active_firmware_version", IsNullable:=True)>
    Public Property Core_active_firmware_version As String
End Class

<XmlRoot(ElementName:="Device_attributes")>
Public Class Device_attributes
    <XmlElement(ElementName:="General")>
    Public Property General As General
End Class

<XmlRoot(ElementName:="Box")>
Public Class Box
    <XmlElement(ElementName:="Box_id")>
    Public Property Box_id As String
    <XmlElement(ElementName:="Device_attributes")>
    Public Property Device_attributes As List(Of Device_attributes)
End Class

<XmlRoot(ElementName:="Pallet")>
Public Class Pallet
    <XmlElement(ElementName:="Pallet_id")>
    Public Property Pallet_id As String
    <XmlElement(ElementName:="Box")>
    Public Property Box As List(Of Box)
End Class

<XmlRoot(ElementName:="Shipment")>
Public Class xmlShipment
    <XmlElement(ElementName:="Main")>
    Public Property Main As Main
    <XmlElement(ElementName:="Header")>
    Public Property Header As Header
    <XmlElement(ElementName:="Pallet")>
    Public Property Pallet As List(Of Pallet)
End Class

<XmlRoot(ElementName:="Body")>
Public Class Body
    <XmlElement(ElementName:="Shipment")>
    Public Property Shipment As xmlShipment
End Class

<XmlRoot(ElementName:="ShipmentFile")>
Public Class ShipmentFile
    <XmlElement(ElementName:="Body")>
    Public Property Body As Body
    <XmlAttribute(AttributeName:="xmlns")>
    Public Property Xmlns As String
    <XmlAttribute(AttributeName:="xenc", [Namespace]:="http://www.w3.org/2000/xmlns/")>
    Public Property Xenc As String
    <XmlAttribute(AttributeName:="ds", [Namespace]:="http://www.w3.org/2000/xmlns/")>
    Public Property Ds As String
    <XmlAttribute(AttributeName:="xsi", [Namespace]:="http://www.w3.org/2000/xmlns/")>
    Public Property Xsi As String
End Class