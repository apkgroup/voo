﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Retour.aspx.vb" Inherits="Telecom.Retour" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        window.onload = function () {
            var i;
            for (i = 1; i < 8; i++) {
                console.log("Serie" + i);
                document.getElementById("Serie" + i).style.display = 'none';
            }
            $('input').keypress(function (e) {
                console.log("KEYPRESS");
                if (e.which == 13) {
                    e.preventDefault();
                    console.log("ISENTER");
                    var self = $(this)
                    var form = self.parents('form:eq(0)');
                    var focusable;
                    var next;
                    var prev;

                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {

                    }
                    return false;

                }
            });
        };
        
        function CheckSerienummers(s, e) {
            var i;
            for (i = 1; i < 8; i++) {
                console.log("Serie" + i);
                document.getElementById("Serie" + i).style.display = 'none';
            }

            if (HiddenArtikel.Get("heeftSerienummer") == true) {
                console.log("HEEFTSERIE")
                var i;

                console.log(s);
                for (i = 1; i < s.number + 1; i++) {

                    console.log(i);
                    document.getElementById("Serie" + i).style.display = 'block';
                }

            }
            else {
                console.log("HEEFT GEEN SERIE")
            }


        }

        function insertArticle() {

            Callback1.PerformCallback();
        }

        function OnCallbackComplete(s, e) {
            console.log("cbCOMPLET");
            location.reload();
            history.go(0);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Doe een retour voor">
    </dx:ASPxLabel>
 <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT m.id, m.Naam, m.Hoofd, m.OpdrachtgeverId 
FROM [Magazijn] m 
left JOIN Gebruikers g on g.MagazijnId = m.id
WHERE (m.[OpdrachtgeverId] = @OpdrachtgeverId and (g.Actief = 1 or hoofd=1 or type is not null) )">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>

    <dx:ASPxLabel ID="ASPxLabelResult" runat="server" Text="ASPxLabel" Font-Bold="True"></dx:ASPxLabel>
     <table id="TableArtikel" runat="server">
         <tr>
             <td>
                 <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Article">
                 </dx:ASPxLabel>
             </td>
             <td>
                 <dx:ASPxHiddenField ID="HiddenArtikel" runat="server" ClientInstanceName="HiddenArtikel">
                 </dx:ASPxHiddenField>
                 <dx:ASPxComboBox ID="ASPxComboBoxArtikel" runat="server" AutoPostBack="True" ClientInstanceName="comboboxArtikel" DataSourceID="SqlDataSourceArtikelen" DropDownWidth="450px" TextField="Article" TextFormatString="{0}; {1}" ValueField="id">
                     <Columns>
                         <dx:ListBoxColumn FieldName="Article" Width="25px">
                         </dx:ListBoxColumn>
                         <dx:ListBoxColumn FieldName="Description">
                         </dx:ListBoxColumn>
                     </Columns>
                 </dx:ASPxComboBox>
             </td>
             <td>
                 <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Nombre">
                 </dx:ASPxLabel>
             </td>
             <td>
                 <dx:ASPxSpinEdit ID="ASPxSpinEditAantal" runat="server" MaxValue="9999999" Number="0">
                     <ClientSideEvents NumberChanged="CheckSerienummers" />
                 </dx:ASPxSpinEdit>
             </td>
         </tr>
         <tr><td>
             <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Raison">
             </dx:ASPxLabel>
             </td><td>
                 <dx:ASPxComboBox ID="ASPxComboBoxRetour" runat="server">
                     <Items>
                         <dx:ListEditItem Text="Retour" Value="2" />
                         <dx:ListEditItem Text="Defect" Value="1" />
                     </Items>
                 </dx:ASPxComboBox>
             </td>
             <td></td><td></td>
         </tr>
     </table>
     <table id="TableSerie">
         <tr id="Serie1">
             <td>
                 <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Numéro de série">
                 </dx:ASPxLabel>
             </td>
             <td>
                 <dx:ASPxTextBox ID="ASPxTextBoxSerie1" runat="server" Width="170px">
                 </dx:ASPxTextBox>
             </td>
         </tr>
         <tr id="Serie2">
             <td>
                 <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Numéro de série">
                 </dx:ASPxLabel>
             </td>
             <td>
                 <dx:ASPxTextBox ID="ASPxTextBoxSerie2" runat="server" Width="170px">
                 </dx:ASPxTextBox>
             </td>
         </tr>
         <tr id="Serie3">
             <td class="auto-style1">
                 <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Numéro de série">
                 </dx:ASPxLabel>
             </td>
             <td class="auto-style1">
                 <dx:ASPxTextBox ID="ASPxTextBoxSerie3" runat="server" Width="170px">
                 </dx:ASPxTextBox>
             </td>
         </tr>
         <tr id="Serie4">
             <td>
                 <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Numéro de série">
                 </dx:ASPxLabel>
             </td>
             <td>
                 <dx:ASPxTextBox ID="ASPxTextBoxSerie4" runat="server" Width="170px">
                 </dx:ASPxTextBox>
             </td>
         </tr>
         <tr id="Serie5">
             <td>
                 <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Numéro de série">
                 </dx:ASPxLabel>
             </td>
             <td>
                 <dx:ASPxTextBox ID="ASPxTextBoxSerie5" runat="server" Width="170px">
                 </dx:ASPxTextBox>
             </td>
         </tr>
         <tr id="Serie6">
             <td>
                 <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Numéro de série">
                 </dx:ASPxLabel>
             </td>
             <td>
                 <dx:ASPxTextBox ID="ASPxTextBoxSerie6" runat="server" Width="170px">
                 </dx:ASPxTextBox>
             </td>
         </tr>
         <tr id="Serie7">
             <td>
                 <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Numéro de série">
                 </dx:ASPxLabel>
             </td>
             <td>
                 <dx:ASPxTextBox ID="ASPxTextBoxSerie7" runat="server" Width="170px">
                 </dx:ASPxTextBox>
             </td>
         </tr>
         <tr>
             <td>
                 <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback1" OnCallback="ASPxCallback1_Callback">
                     <ClientSideEvents CallbackComplete="OnCallbackComplete" />
                 </dx:ASPxCallback>
             </td>
             <td>
                 <dx:ASPxButton ID="ASPxButtonToevoegen" runat="server" AutoPostBack="False" Width="50px">
                     <ClientSideEvents Click="insertArticle" />
                     <Image IconID="actions_resetchanges_32x32devav">
                     </Image>
                 </dx:ASPxButton>
             </td>
         </tr>
     </table>

    <asp:SqlDataSource ID="SqlDataSourceStockMagazijn" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [StockMagazijn] WHERE [id] = @id" InsertCommand="INSERT INTO [StockMagazijn] ([MateriaalId], [MagazijnId], [Aantal]) VALUES (@MateriaalId, @MagazijnId, @Aantal)" SelectCommand="SELECT s.[id],[naam],b.Article,b.Description, [MateriaalId], [MagazijnId], [Aantal] as 'Totaal aantal'
,
(select count(sr.id) from Serienummer sr inner join StockMagazijn sm on sr.StockMagazijnId = sm.id where sm.MateriaalId = s.MateriaalId and (sr.statusId = 1 or sr.statusId=2) and [MagazijnId] = @MagazijnId ) as AantalKapot
,[Aantal]-(select count(sr.id) from Serienummer sr inner join StockMagazijn sm on sr.StockMagazijnId = sm.id where sm.MateriaalId = s.MateriaalId and  (sr.statusId = 1 or sr.statusId=2) and [MagazijnId] = @MagazijnId ) as Beschikbaar
 FROM [StockMagazijn] s
INNER JOIN Magazijn m on s.MagazijnId = m.id 
INNER JOIN Basismateriaal b on s.MateriaalId = b.id WHERE ([MagazijnId] = @MagazijnId)
AND b.id in (select mo.materiaalId from [MateriaalOpdrachtgevers] mo where opdrachtgeverId = @opdrachtgeverId)
" UpdateCommand="UPDATE [StockMagazijn] SET [MateriaalId] = @MateriaalId, [MagazijnId] = @MagazijnId, [Aantal] = @Aantal WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ASPxComboBoxMagazijn" Name="MagazijnId" PropertyName="Value" Type="Int32" />
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="MateriaalId" Type="Int32" />
            <asp:Parameter Name="MagazijnId" Type="Int32" />
            <asp:Parameter Name="Aantal" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceStockMagazijn" KeyFieldName="id">
        <SettingsDetail ShowDetailRow="True" />
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2" Caption="Serie" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="beschrijving" VisibleIndex="5" Caption="Status" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Lotnr" FieldName="lotNr" VisibleIndex="6" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Creation" FieldName="datumGemaakt" ShowInCustomizationForm="True" VisibleIndex="7">
                        </dx:GridViewDataDateColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <SettingsPager PageSize="25">
        </SettingsPager>
        <SettingsEditing Mode="Batch">
        </SettingsEditing>
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="0" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="naam" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MateriaalId" VisibleIndex="4" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MagazijnId" VisibleIndex="5" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Totaal aantal" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="AantalKapot" ReadOnly="True" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Beschikbaar" ReadOnly="True" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataHyperLinkColumn Caption="Retour" FieldName="id" VisibleIndex="9">
                <PropertiesHyperLinkEdit NavigateUrlFormatString="../RetourAll.aspx?id={0}" Text="Retour All">
                </PropertiesHyperLinkEdit>
            </dx:GridViewDataHyperLinkColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT serienummer, lotNr, datumGemaakt, st.beschrijving  FROM [Serienummer] sr left join Status st on sr.statusId = st.id WHERE ([StockMagazijnId] = @StockMagazijnId) and (uitgeboekt &lt;&gt; 1 or uitgeboekt is null)
">
        <SelectParameters>
            <asp:SessionParameter Name="StockMagazijnId" SessionField="stockmagazijn" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

     <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT distinct article, description, CONCAT(article, ' | ', description) as naam, b.id FROM [Basismateriaal] b
INNER JOIN MateriaalOpdrachtgevers o ON b.id =o.materiaalId
WHERE o.opdrachtgeverId = @OpdrachtgeverId and (o.actief = 1 or o.actief is null)" ID="SqlDataSourceArtikelen">
         <SelectParameters>
             <asp:SessionParameter SessionField="opdrachtgever" DefaultValue="1" Name="OpdrachtgeverId" Type="Int32"></asp:SessionParameter>
         </SelectParameters>
     </asp:SqlDataSource>

</asp:Content>
