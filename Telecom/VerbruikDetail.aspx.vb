﻿Imports System.Data.SqlClient
Imports DevExpress.Web
Imports Microsoft.Exchange.WebServices.Data
Imports System.IO
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.Drawing


Public Class VerbruikDetail


    Inherits System.Web.UI.Page
    Private headerImage As Image
    Dim vorige As Integer
    Dim volgende As Integer

    Protected json As New System.Web.Script.Serialization.JavaScriptSerializer
    Protected jsvars As New Hashtable

    Private Property TextBoxIdCollection As List(Of String)
        Get
            Dim collection = TryCast(Me.ViewState("TextBoxIdCollection"), List(Of String))

            Return If(collection, New List(Of String)())
        End Get
        Set(ByVal value As List(Of String))
            ViewState("TextBoxIdCollection") = value
        End Set
    End Property









    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim serienrs As New Dictionary(Of String, String)



        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If





        If Not Session("isadmin") Then





        Else
            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Dim s_SQL As String = "SELECT min(b.id) as 'vorige', max (b.id) as 'volgende' FROM [Bestellingen] b inner join Gebruikers g on b.GebruikerId = g.id WHERE g.opdrachtgeverId = @opdrachtgever and b.id <> @huidig and b.[Status] = 1"
                Dim cmd As New SqlCommand(s_SQL, cn)
                Dim par As New SqlParameter("@huidig", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
                cmd.Parameters.Add(par)
                par = New SqlParameter("@opdrachtgever", SqlDbType.Int) With {.Value = CInt(Session("opdrachtgever"))}
                cmd.Parameters.Add(par)
                Dim dr As SqlDataReader = cmd.ExecuteReader

                If dr.HasRows Then
                    dr.Read()
                    vorige = IIf(IsDBNull(dr.GetValue(0)), 0, dr.GetValue(0))
                    volgende = IIf(IsDBNull(dr.GetValue(1)), 0, dr.GetValue(1))
                End If
                cn.Close()

            End Using


        End If

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

            cn.Open()
            Dim s_SQL As String = "select top 1 id from voo.dbo.Magazijn where hoofd = 1 and opdrachtgeverId = @opdrachtgeverId"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
            cmd.Parameters.Add(par)
            Dim dr2 As SqlDataReader = cmd.ExecuteReader
            If dr2.HasRows Then
                dr2.Read()
                Session("MagazijnId") = dr2.GetInt32(0)
            End If
            dr2.Close()
            cn.Close()
        End Using

        Dim entities As New VooEntities
        Dim magazijnid As Integer = Session("MagazijnId")
        Dim verbruikid As Integer = Convert.ToInt32(Request.QueryString("id"))
        ASPxMemo1.Value = entities.Verbruik.Find(verbruikid).opmerking
        ASPxLabelVan.Text = entities.Verbruik.Find(verbruikid).Magazijn.Naam
        ASPxLabelOp.Text = entities.Verbruik.Find(verbruikid).Magazijn1.Naam
        ASPxLabelStraat.Text = entities.Verbruik.Find(verbruikid).straat
        ASPxLabelStad.Text = entities.Verbruik.Find(verbruikid).stad
        ASPxLabelDatum.Text = entities.Verbruik.Find(verbruikid).datum
        ASPxLabelOrder.Text = entities.Verbruik.Find(verbruikid).ordernummer
        ASPxLabelDoor.Text = entities.Verbruik.Find(verbruikid).Gebruikers.ExNaam

    End Sub
    Protected Sub ASPxComboBoxMagazijn_PreRender(sender As Object, e As EventArgs) Handles ASPxComboBoxMagazijn.PreRender
        If Not String.IsNullOrWhiteSpace(Session("MagazijnId")) Then
            ASPxComboBoxMagazijn.SelectedItem = ASPxComboBoxMagazijn.Items.FindByValue(Session("MagazijnId").ToString)
        End If
    End Sub









    Protected Sub ASPxGridView1_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles ASPxGridView1.CustomCallback

        Dim s As ASPxGridView = sender

        If e.Parameters = "CEXP" Then

        End If

    End Sub

    Private Sub Header_CreateDetailHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        e.Graph.BorderWidth = 0
        Dim r As New Rectangle(300, 0, headerImage.Width, headerImage.Height)
        e.Graph.DrawImage(headerImage, r)
        'r = New Rectangle(0, headerImage.Height, 200, 50)
        'e.Graph.DrawString("Additional Header information here....", r)
    End Sub
    Private Sub Link1_CreateReportFooterArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        e.Graph.BorderWidth = 0
        Dim r As New Rectangle(1, 20, 250, 50)
        e.Graph.Font = New Font("Arial", 12, FontStyle.Bold)
        e.Graph.ForeColor = Color.DimGray
        e.Graph.DrawString("Handtekening magazijnier", r)

        r = New Rectangle(350, 20, 250, 50)
        e.Graph.Font = New Font("Arial", 12, FontStyle.Bold)
        e.Graph.ForeColor = Color.DimGray
        e.Graph.DrawString("Handtekening technieker", r)

        Dim rF As New RectangleF(1, 75, 250, 100)

        e.Graph.DrawRect(rF, BorderSide.All, Color.Transparent, Color.DimGray).BorderWidth = 1
        rF = New RectangleF(350, 75, 250, 100)
        e.Graph.DrawRect(rF, BorderSide.All, Color.Transparent, Color.DimGray).BorderWidth = 1

        r = New Rectangle(350, 200, 250, 50)
        e.Graph.Font = New Font("Arial", 12, FontStyle.Bold)
        e.Graph.ForeColor = Color.DimGray
        e.Graph.DrawString("Datum: " & Today.ToShortDateString, r)

    End Sub



    Private Sub Verwittigen(ByVal varGebruiker As String, ByVal Gebruikerid As Integer, ByVal Status As String)

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim varBericht As String = ""
            Dim varOnderwerp As String = ""
            Dim varBeheerder As String = String.Format("{0} {1}", Session("naam"), Session("voornaam"))
            Dim varEmail As String = ""
            Dim s_SQL As String = "SELECT Bericht, Onderwerp FROM Emailberichten WHERE (Omschrijving=@omschrijving)"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@omschrijving", SqlDbType.NVarChar, 50) With {.Value = "Melding bestelling verwerkt" & Session("taal")}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                If Not dr.IsDBNull(0) Then
                    varBericht = dr.GetString(0)
                    varOnderwerp = dr.GetString(1)
                End If
            End If
            dr.Close()
            s_SQL = "SELECT Email FROM Gebruikers WHERE (Gebruikers.id=@gebruiker)"
            cmd = New SqlCommand(s_SQL, cn)
            par = New SqlParameter("@gebruiker", SqlDbType.Int) With {.Value = Gebruikerid}
            cmd.Parameters.Add(par)
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                varEmail = dr.GetString(0)
            End If
            dr.Close()
            Dim adressen As New List(Of String)
            adressen.Add(varEmail)

            Dim varBody As String = varBericht.Replace("[WERKNEMER]", varGebruiker)
            varBody = varBody.Replace("[STATUS]", Status)
            varBody = varBody.Replace("[BEHEERDER]", varBeheerder)
            varBody = varBody.Replace("[URL]", String.Format("{0}/MijnOrders.aspx", Session("domein")))
            varBody = varBody.Replace("[SITETITEL]", "APK WMS")
            Dim sBody As String = "<x-html>" & vbCrLf
            sBody = sBody & varBody
            sBody = sBody & "</x-html>"

            mailGoogle(varOnderwerp, "noreply@apkgroup.eu", adressen, sBody, New Dictionary(Of String, Byte()))


        End Using
    End Sub



    Protected Sub ASPxGridView1_RowUpdating(sender As Object, e As Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating


        Dim editingRowVisibleIndex As Integer = ASPxGridView1.FindVisibleIndexByKeyValue(e.Keys.Values(0))
        Session("bestelling") = ASPxGridView1.GetRowValues(editingRowVisibleIndex, "Poste")









    End Sub


    Protected Sub ASPxGridView1_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBinding
        DoSelect(SqlDataSource1.ConnectionString)


    End Sub



    Protected Sub ASPxGridView4_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("bestellijnId") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
    End Sub

    Private Sub DoSelect(ByVal connectionString As String)
        Dim selectResult As New DataView()
        Dim selectCommand As String = "select distinct [bestellijnId] from [RetourBestellijnSerienummer]"
        Using ds As New SqlDataSource(connectionString, selectCommand)
            selectResult = CType(ds.Select(DataSourceSelectArguments.Empty), DataView)
        End Using
        Dim result As New ArrayList()
        For Each row As DataRow In selectResult.Table.Rows
            result.Add(row("bestellijnId"))
        Next row
        Session("SelectResult") = result
    End Sub

    Protected Sub ASPxGridView1_RowUpdated(sender As Object, e As Data.ASPxDataUpdatedEventArgs) Handles ASPxGridView1.RowUpdated

        Session("isAangepast") = True

    End Sub






    Protected Sub ASPxGridView1_RowValidating(sender As Object, e As Data.ASPxDataValidationEventArgs) Handles ASPxGridView1.RowValidating

    End Sub




    Private Sub HideColumns(v As Boolean)
        ASPxGridView1.Columns("Materiaalgroep").Visible = Not v
    End Sub





End Class