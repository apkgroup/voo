﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VerlofaanvraagOverzicht

    '''<summary>
    '''ASPxMenu1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxMenu1 As Global.DevExpress.Web.ASPxMenu

    '''<summary>
    '''ASPxComboBoxGebruiker control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxComboBoxGebruiker As Global.DevExpress.Web.ASPxComboBox

    '''<summary>
    '''ASPxSpinEditJaar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxSpinEditJaar As Global.DevExpress.Web.ASPxSpinEdit

    '''<summary>
    '''ASPxLoadingPanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLoadingPanel1 As Global.DevExpress.Web.ASPxLoadingPanel

    '''<summary>
    '''SqlDataSourceAanvragen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceAanvragen As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''grid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grid As Global.DevExpress.Web.ASPxGridView

    '''<summary>
    '''ASPxGridViewExporter1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxGridViewExporter1 As Global.DevExpress.Web.ASPxGridViewExporter

    '''<summary>
    '''SqlDataSourceVerlofaanvragen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSourceVerlofaanvragen As Global.System.Web.UI.WebControls.SqlDataSource
End Class
