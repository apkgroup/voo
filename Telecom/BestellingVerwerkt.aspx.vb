﻿Imports System.Data.SqlClient

Public Class BestellingVerwerkt
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim s_SQL As String = "SELECT beschrijving FROM Bestellingen Inner join Bestellingstatus on bestellingen.Status = Bestellingstatus.id  where Bestellingen.id = @id"
            Dim cmd As New SqlCommand(s_SQL, cn)
            Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = CInt(Request.QueryString("id"))}
            cmd.Parameters.Add(par)
            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()
                Me.Literal2.Text = "Ordre a été traité avec le statut: " + dr.GetString(0)

            End If
            dr.Close()
        End Using
    End Sub

End Class