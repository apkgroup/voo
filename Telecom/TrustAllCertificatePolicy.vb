﻿Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates

Public Class TrustAllCertificatePolicy

    Public Shared Sub OverrideCertificateValidation()
        ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf RemoteCertValidate)
    End Sub

    Private Shared Function RemoteCertValidate(ByVal sender As Object, ByVal cert As X509Certificate, ByVal chain As X509Chain, ByVal [error] As SslPolicyErrors) As Boolean
        Return True
    End Function

End Class