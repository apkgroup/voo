﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="Gebruikersprofiel.aspx.vb" Inherits="Telecom.Gebruikersprofiel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
        </style>
    <script type="text/javascript">
  
        function OnbtnGelezen(s, e) {

            Callback2.PerformCallback();
        }
            function OnbtnBewaren(s, e) {
                var Inorde = true;

        
                if (Inorde == true) {
                    if (txtEmail.GetText() == "") {
                        Inorde = false;
                    }
                }
             
                  if (Inorde == true) {
                    Callback1.PerformCallback();
                }
                       
            }
            function OnCallbackComplete(s, e) {
                SetPCVisible(true);
            }
            function OnCallback2Complete(s, e) {
                SetPCVisible3(false);
            }
            function SetPCVisible(value) {
                var popupControl = GetPopupControl();
                if (value) {
                    popupControl.Show();
                }
                else {
                    popupControl.Hide();
                }
            }

            function GetPopupControl() {
                return popup;
            }
            function SetPCVisible2(value) {
                var popupControl = GetPopupControl();
                if (value) {
                    popupControl.Show();
                }
                else {
                    popupControl.Hide();
                }
            }

            function GetPopupControl2() {
                return popup2;
            }

            function GetPopupControl3() {
                return popup3;
            }
            function SetPCVisible3(value) {
                var popupControl = GetPopupControl3();
                if (value) {
                    popupControl.Show();
                }
                else {
                    popupControl.Hide();
                }
            }
            function DocumentZien(id) {
                window.location.href = "https://" + window.location.host + "/downloaddocument.aspx?d=" + id + "&t=opleiding";
            }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                 <h2>
                     <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                 </h2>       
            </td>
            <td>

            </td>
            <td>

                <dx:ASPxLabel ID="lblWijziggebruiker" runat="server" Text="Bekijk andere gebruiker:">
                </dx:ASPxLabel>

            </td>
             <td>

                 <dx:ASPxComboBox ID="cboWijziggebruiker" runat="server" DataSourceID="SqlDataSourceWijziggebruiker" TextField="Werknemer" ValueField="id" ValueType="System.Int32" AutoPostBack="True">
                 </dx:ASPxComboBox>
                 

            </td>
            <td>

                <dx:ASPxButton ID="btnAfdrukken" runat="server">
                    <Image Height="16px" Url="~/images/Print_16x16.png" Width="16px">
                    </Image>
                </dx:ASPxButton>

            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="SqlDataSourceWijziggebruiker" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Gebruikers.id, vwWn.Werknemer FROM Gebruikers INNER JOIN (SELECT [ON], NR, Naam + ' ' + VNaam as Werknemer FROM Elly_SQL.dbo.WERKN) vwWn ON Gebruikers.Werkn=vwWn.Nr AND Gebruikers.Werkg=vwWn.[ON] WHERE (Isnull(Gebruikers.Actief,0)=1) ORDER BY vwWn.Werknemer"></asp:SqlDataSource>
    <br /> 
    <asp:Literal ID="Literal3" runat="server"></asp:Literal>
&nbsp;<br />
    <table>
        <tr>
            <td style="vertical-align:top;">
                <table>
                    <tr>
                        <td>
                             &nbsp;</td>
                         <td style="vertical-align:bottom;">
                             &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                             &nbsp;</td>
                     </tr>
                </table>
               

            </td>
            <td>
              
            </td>
            <td>
                  <dx:ASPxFormLayout ID="ASPxFormLayout1" ClientInstanceName="formNoodzakelijk" runat="server" DataSourceID="SqlDataSourceGebruiker">
         <Items>
             <dx:LayoutItem FieldName="GSM">
                         <LayoutItemNestedControlCollection>
                             <dx:LayoutItemNestedControlContainer runat="server">
                                 <dx:ASPxTextBox ID="txtGSM" ClientInstanceName="txtGSM" runat="server" Width="170px">
                                     <ValidationSettings>
                                         <ErrorImage Url="~/images/warning.png">
                                         </ErrorImage>
                                     </ValidationSettings>
                                 </dx:ASPxTextBox>
                             </dx:LayoutItemNestedControlContainer>
                         </LayoutItemNestedControlCollection>
                     </dx:LayoutItem>
             <dx:LayoutItem FieldName="Email">
                         <LayoutItemNestedControlCollection>
                             <dx:LayoutItemNestedControlContainer runat="server">
                                 <dx:ASPxTextBox ID="txtEmail" ClientInstanceName="txtEmail" runat="server" Width="170px">
                                     <ValidationSettings>
                                         <ErrorImage Url="~/images/warning.png">
                                         </ErrorImage>
                                         <RequiredField ErrorText="Verplicht in te vullen" IsRequired="True" />
                                     </ValidationSettings>
                                 </dx:ASPxTextBox>
                             </dx:LayoutItemNestedControlContainer>
                         </LayoutItemNestedControlCollection>
                     </dx:LayoutItem>
             <dx:LayoutItem ColSpan="1" FieldName="TaalId" Caption="Taal">
                 <LayoutItemNestedControlCollection>
                     <dx:LayoutItemNestedControlContainer runat="server">
                         <dx:ASPxComboBox ID="ASPxFormLayout1_E2" runat="server" Width="170px" DataSourceID="SqlDataSourceTaal" TextField="beschrijving" ValueField="id" ValueType="System.Int32">
                              <ValidationSettings>
                                         <ErrorImage Url="~/images/warning.png">
                                         </ErrorImage>
                                         <RequiredField ErrorText="Verplicht in te vullen" IsRequired="True" />
                                     </ValidationSettings>
                         </dx:ASPxComboBox>
                     </dx:LayoutItemNestedControlContainer>
                 </LayoutItemNestedControlCollection>
             </dx:LayoutItem>
             <dx:LayoutItem Caption=" ">
                         <LayoutItemNestedControlCollection>
                             <dx:LayoutItemNestedControlContainer runat="server">
                                 <dx:ASPxButton ID="btnBewaren" runat="server" AutoPostBack="False" ClientInstanceName="btnBewaren" Text="Sauvegarder">
                                     <ClientSideEvents Click="OnbtnBewaren" />
                                     <Image Url="~/images/Save_16x16.png">
                                     </Image>
                                 </dx:ASPxButton>
                             </dx:LayoutItemNestedControlContainer>
                         </LayoutItemNestedControlCollection>
                     </dx:LayoutItem>
         </Items>
     </dx:ASPxFormLayout>
            </td>
            <td>

                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="ALLES leegmaken" Visible="False">
                </dx:ASPxButton>
                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Serienummers leegmaken" Visible="False">
                </dx:ASPxButton>

            </td>
        </tr>
       
    </table>
     
   <br />
     
    <br />
    <br />
    <asp:SqlDataSource ID="SqlDataSourceNuttigeinfouserid" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id]
      ,[Werkg]
      ,[Werkn]
      , eWerkn.Zetel
, eWerkn.straat + isnull(eWerkn.STRAAT2,'') + ' ' + isnull(eWerkn.[HUISNUMMER],'') as Straat, 
(case isnull(eWerkn.[POSTCODE],'') when '' then '' else eWerkn.Postcode + ' ' end) + eWerkn.plaats as Plaats
	  , pActiviteit.omschrijv as Activiteit
	  , eAfdeling.Omschrijving as Afdeling
	  , eActiviteit.omschrijv as Secretariaat
	  , eVerantw.naam as DirecteOverste
	  , ePeter.Naam + ' ' + ePeter.Vnaam as Peter
	  , eWerkn.Petergsm
	  , tbMagazijnier.Magazijnier
	  , tbOnderhoudsmechanicien.Onderhoudsmechanicien
  FROM [dbo].[Gebruikers]
  INNER JOIN Elly_SQL.dbo.WERKN eWerkn
  ON Gebruikers.Werkg=eWerkn.[ON] AND Gebruikers.Werkn=eWerkn.NR
  LEFT OUTER JOIN ProjectSQL.dbo.Activiteit  pActiviteit
  ON eWerkn.Afdeling=pActiviteit.act
  LEFT OUTER JOIN Elly_SQL.dbo.Afdeling eAfdeling
  ON eWerkn.Afd = eAfdeling.Afd
  LEFT OUTER JOIN Elly_SQL.dbo.activiteit eActiviteit
  ON eWerkn.Secretariaat=eActiviteit.act
  LEFT OUTER JOIN Elly_SQL.dbo.Verantw eVerantw
  ON eWerkn.DirecteOverste=eVerantw.ini
  LEFT OUTER JOIN Elly_SQL.dbo.Werkn ePeter
  ON eWerkn.PeterWG=ePeter.[ON] AND eWerkn.PeterWerkn=ePeter.NR
  LEFT OUTER JOIN
(SELECT Werknemer, Werkgever, SVWerkg, SVWerkn, SVType
, WERKN.NAAM + ' ' + WERKN.VNAAM 
 + ', Tel: ' + (CASE ISNULL(tbGSM.TELEFOONNR, ' ') WHEN
 ' ' THEN ' ' ELSE 
(case substring(tbGSM.TELEFOONNR,1,2) WHEN '06' THEN '+31' ELSE '+32' END) + substring(tbGSM.TELEFOONNR, charindex('0',tbGSM.TELEFOONNR)+1,len(tbGSM.TELEFOONNR)) END)
  as Magazijnier
FROM Elly_SQL.dbo.Servicetoewijzing
INNER JOIN Elly_SQL.dbo.WERKN
ON Servicetoewijzing.SVWerkg=WERKN.[ON] AND Servicetoewijzing.SVWerkn=WERKN.NR
LEFT OUTER JOIN 
(SELECT * FROM Elly_SQL.dbo.GSM WHERE (abs(isnull(GSM.Contact,0))=1)) tbGSM
ON Servicetoewijzing.SVWerkn=tbGSM.WERKN AND Servicetoewijzing.SVWerkg=tbGSM.WERKG
WHERE Servicetoewijzing.SVType='Magazijnier, gereedschap &amp; GSM') tbMagazijnier
ON eWerkn.[ON]=tbMagazijnier.Werkgever AND eWerkn.NR=tbMagazijnier.Werknemer
LEFT OUTER JOIN
(SELECT Werknemer, Werkgever, SVWerkg, SVWerkn, SVType
, WERKN.NAAM + ' ' + WERKN.VNAAM 
 + ', Tel: ' + (CASE ISNULL(tbGSM.TELEFOONNR, ' ') WHEN
 ' ' THEN ' ' ELSE 
(case substring(tbGSM.TELEFOONNR,1,2) WHEN '06' THEN '+31' ELSE '+32' END) + substring(tbGSM.TELEFOONNR, charindex('0',tbGSM.TELEFOONNR)+1,len(tbGSM.TELEFOONNR)) END)
  as Onderhoudsmechanicien
FROM Elly_SQL.dbo.Servicetoewijzing
INNER JOIN Elly_SQL.dbo.WERKN
ON Servicetoewijzing.SVWerkg=WERKN.[ON] AND Servicetoewijzing.SVWerkn=WERKN.NR
LEFT OUTER JOIN 
(SELECT * FROM Elly_SQL.dbo.GSM WHERE (abs(isnull(GSM.Contact,0))=1)) tbGSM
ON Servicetoewijzing.SVWerkn=tbGSM.WERKN AND Servicetoewijzing.SVWerkg=tbGSM.WERKG
WHERE Servicetoewijzing.SVType='Onderhoudsmechanicien') tbOnderhoudsmechanicien
ON eWerkn.[ON]=tbOnderhoudsmechanicien.Werkgever AND eWerkn.NR=tbOnderhoudsmechanicien.Werknemer
WHERE (Gebruikers.id=@id)">
        <SelectParameters>
            <asp:SessionParameter Name="id" SessionField="userid" />
        </SelectParameters>
    </asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSourceTaal" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [beschrijving] FROM [Taal]"></asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="SqlDataSourceParametersuserid" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [GebruikersparametersWaarden] WHERE [id] = @id" InsertCommand="INSERT INTO [GebruikersparametersWaarden] ([Gebruiker], [Parameter], [Waarde]) VALUES (@Gebruiker, @Parameter, @Waarde)" SelectCommand="SELECT [id], [Gebruiker], [Parameter], [Waarde] FROM [GebruikersparametersWaarden] WHERE ([Gebruiker] = @Gebruiker)" UpdateCommand="UPDATE [GebruikersparametersWaarden] SET [Gebruiker] = @Gebruiker, [Parameter] = @Parameter, [Waarde] = @Waarde WHERE [id] = @id">
         <DeleteParameters>
             <asp:Parameter Name="id" Type="Int32" />
         </DeleteParameters>
         <InsertParameters>
             <asp:Parameter Name="Gebruiker" Type="Int32" />
             <asp:Parameter Name="Parameter" Type="Int32" />
             <asp:Parameter Name="Waarde" Type="String" />
         </InsertParameters>
         <SelectParameters>
             <asp:SessionParameter Name="Gebruiker" SessionField="userid" Type="Int32" />
         </SelectParameters>
         <UpdateParameters>
             <asp:Parameter Name="Gebruiker" Type="Int32" />
             <asp:Parameter Name="Parameter" Type="Int32" />
             <asp:Parameter Name="Waarde" Type="String" />
             <asp:Parameter Name="id" Type="Int32" />
         </UpdateParameters>
     </asp:SqlDataSource>
     <asp:SqlDataSource ID="SqlDataSourceParameters" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [GebruikersparametersWaarden] WHERE [id] = @id" InsertCommand="INSERT INTO [GebruikersparametersWaarden] ([Gebruiker], [Parameter], [Waarde]) VALUES (@Gebruiker, @Parameter, @Waarde)" SelectCommand="SELECT [id], [Gebruiker], [Parameter], [Waarde] FROM [GebruikersparametersWaarden] WHERE ([Gebruiker] = @Gebruiker)" UpdateCommand="UPDATE [GebruikersparametersWaarden] SET [Gebruiker] = @Gebruiker, [Parameter] = @Parameter, [Waarde] = @Waarde WHERE [id] = @id">
         <DeleteParameters>
             <asp:Parameter Name="id" Type="Int32" />
         </DeleteParameters>
         <InsertParameters>
             <asp:Parameter Name="Gebruiker" Type="Int32" />
             <asp:Parameter Name="Parameter" Type="Int32" />
             <asp:Parameter Name="Waarde" Type="String" />
         </InsertParameters>
         <SelectParameters>
             <asp:ControlParameter ControlID="cboWijziggebruiker" Name="Gebruiker" PropertyName="Value" Type="Int32" />
         </SelectParameters>
         <UpdateParameters>
             <asp:Parameter Name="Gebruiker" Type="Int32" />
             <asp:Parameter Name="Parameter" Type="Int32" />
             <asp:Parameter Name="Waarde" Type="String" />
             <asp:Parameter Name="id" Type="Int32" />
         </UpdateParameters>
     </asp:SqlDataSource>
     <asp:SqlDataSource ID="SqlDataSourceGebruiker" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Gebruikers] WHERE [id] = @id" InsertCommand="INSERT INTO [Gebruikers] ([GSM], [Email], [Magazijn]) VALUES (@GSM, @Email, @Magazijn)" SelectCommand="SELECT [id], [GSM], [Email], TaalId FROM [Gebruikers] WHERE ([id] = @id)" UpdateCommand="UPDATE [Gebruikers] SET [GSM] = @GSM, [Email] = @Email, [TaalId] = @TaalId WHERE [id] = @id">
         <DeleteParameters>
             <asp:Parameter Name="id" Type="Int32" />
         </DeleteParameters>
         <InsertParameters>
             <asp:Parameter Name="GSM" Type="String" />
             <asp:Parameter Name="Email" Type="String" />
             <asp:Parameter Name="Magazijn" Type="Int32" />
         </InsertParameters>
         <SelectParameters>
             <asp:ControlParameter ControlID="cboWijziggebruiker" Name="id" PropertyName="Value" Type="Int32" />
         </SelectParameters>
         <UpdateParameters>
             <asp:Parameter Name="GSM" Type="String" />
             <asp:Parameter Name="Email" Type="String" />
             <asp:Parameter Name="TaalId" />
             <asp:Parameter Name="id" Type="Int32" />
         </UpdateParameters>
     </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceGebruikeruserid" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Gebruikers] WHERE [id] = @id" InsertCommand="INSERT INTO [Gebruikers] ([GSM], [Email], [Magazijn]) VALUES (@GSM, @Email, @Magazijn)" SelectCommand="SELECT [id], [GSM], [Email], TaalId FROM [Gebruikers] WHERE ([id] = @id)" UpdateCommand="UPDATE [Gebruikers] SET [GSM] = @GSM, [Email] = @Email, [Magazijn] = @Magazijn WHERE [id] = @id">
         <DeleteParameters>
             <asp:Parameter Name="id" Type="Int32" />
         </DeleteParameters>
         <InsertParameters>
             <asp:Parameter Name="GSM" Type="String" />
             <asp:Parameter Name="Email" Type="String" />
             <asp:Parameter Name="Magazijn" Type="Int32" />
         </InsertParameters>
         <SelectParameters>
             <asp:SessionParameter Name="id" SessionField="userid" Type="Int32" />
         </SelectParameters>
         <UpdateParameters>
             <asp:Parameter Name="GSM" Type="String" />
             <asp:Parameter Name="Email" Type="String" />
             <asp:Parameter Name="Magazijn" Type="Int32" />
             <asp:Parameter Name="id" Type="Int32" />
         </UpdateParameters>
     </asp:SqlDataSource>
    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback1"
        OnCallback="ASPxCallback1_Callback">
         <ClientSideEvents CallbackComplete="OnCallbackComplete" />
     </dx:ASPxCallback>
     <asp:SqlDataSource ID="SqlDataSourcepars" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Omschrijving] FROM [Gebruikersparameters]"></asp:SqlDataSource>
    <br />
     <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="popup" runat="server" 
        CloseAction="MouseOut" HeaderText="Bevestiging" 
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" 
        Width="314px" MinWidth="300px" CloseAnimationType="Fade" DisappearAfter="1000">
        <HeaderImage Url="~/images/Info_32x32.png">
        </HeaderImage>
         <ContentCollection>
<dx:PopupControlContentControl runat="server">
   De gegevens zijn bewaard.
    </dx:PopupControlContentControl>
</ContentCollection>
     </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="popup2" runat="server" 
        CloseAction="CloseButton" HeaderText="Foutmelding" Modal="True" 
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" 
        Width="314px" MinWidth="300px">
        <HeaderImage Url="~/images/warning.png">
        </HeaderImage>
         <ContentCollection>
<dx:PopupControlContentControl runat="server">
    <dx:ASPxLabel ID="lblPopup" ClientInstanceName="lblPopup" runat="server"></dx:ASPxLabel>
      
    <br />
    De gegevens zijn niet bewaard.
    </dx:PopupControlContentControl>
</ContentCollection>
     </dx:ASPxPopupControl>
     <dx:ASPxPopupControl ID="popup3" ClientInstanceName="popup3" runat="server" CloseAction="CloseButton" HeaderText="Browser aanbevelingen" Modal="True" Height="203px" Width="558px" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        <HeaderStyle Font-Bold="True" />
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <table>
                    <tr>
                        <td>
                            <img src="images/ff.png" alt="firefox" title="firefox" width="150" />
                        </td>
                        <td>
                            Deze site is geoptimaliseerd voor de browser Firefox. <br />Wij raden dan ook aan deze browser te gebruiken. Enige vereiste voor een optimale werking, is dat javascript ingeschakeld staat (dit staat standaard ingeschakeld).
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="images/chrome.png" alt="chrome" title="chrome" width="150" />
                        </td>
                        <td>
                            Op het eerste zicht lijkt ook Chrome alle functionaliteiten te ondersteunen. <br />Dit is echter wel in beperkte mate getest.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="images/ie.png" alt="internet explorer" title="internet explorer" width="150" />
                        </td>
                        <td>
                            Indien u werkt met Internet Explorer als browser, is het noodzakelijk dat u deze website toevoegt bij instellingen voor compatibiliteitsweergave. <br />Zonder deze aanpassing zal de site niet optimaal werken.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <dx:ASPxButton ID="ASPxButtonGelezen" runat="server" Text="Ik heb dit gelezen, toon me dit voortaan niet meer." AutoPostBack="False">
                                
                                <ClientSideEvents Click="OnbtnGelezen" />
                                
                                <Image Url="~/images/Apply_16x16.png">
                                </Image>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
         </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxCallback ID="ASPxCallback2" runat="server" ClientInstanceName="Callback2"
        OnCallback="ASPxCallback2_Callback">
         <ClientSideEvents CallbackComplete="OnCallback2Complete" />
     </dx:ASPxCallback>
</asp:Content>
