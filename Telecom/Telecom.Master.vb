﻿Imports System.Data.SqlClient
Imports DevExpress.Web

Public Class Telecom
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not Request.IsSecureConnection Then
        '    Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"), False)
        '    Context.ApplicationInstance.CompleteRequest()
        'End If
        nbMain.Visible = Session("isingelogd")


        If Session("isingelogd") Then
            If Session("taal") = 1 Then
                LiteralWelkom.Text = "Welkom "
                ASPxLoadingPanel1.Text = "Laden..."
            ElseIf Session("taal") = 2 Then
                LiteralWelkom.Text = "Bienvenue "
            End If
            Literal1.Text = Session("Voornaam")

            btnLogout.Visible = True
            Dim voocontext As New VooEntities
            Dim datasourcestring As String = ""
            Dim opdrachtgeverId As Integer = Session("Opdrachtgever")



            Select Case Session("level")
                Case 1
                    datasourcestring = datasourcestring & "Beheerder"
                Case 2
                    datasourcestring = datasourcestring & "Magazijnier"
                Case 3
                    datasourcestring = datasourcestring & "KLV"
                Case 8
                    datasourcestring = datasourcestring & "ProjectTechnieker"
                    nbMain.DataBind()
                Case 9
                    datasourcestring = datasourcestring & "Technieker"
                    nbMain.DataBind()


                Case 10
                    datasourcestring = datasourcestring & "Klant"
            End Select

            datasourcestring = datasourcestring & voocontext.Opdrachtgever.Find(opdrachtgeverId).tag
            nbMain.DataSourceID = datasourcestring & "1"
            'Dim test = Page.FindControl("LeftPanel").FindControl(datasourcestring & "1")
            nbMain.DataBind()
            Dim userId As Int32 = Session("userid")

            If voocontext.GebruikersRollen.Where(Function(x) x.gebruikerId = userId).Any(Function(y) y.niveauId = 4) Then
                nbMain.Groups(0).Visible = True
            Else
                nbMain.Groups(0).Visible = False
            End If

            For Each group As NavBarGroup In nbMain.Groups
                group.Expanded = False
            Next
            If nbMain.SelectedItem IsNot Nothing Then
                nbMain.SelectedItem.Group.Expanded = True
            End If



            Dim opdrachtgevertekst As String = voocontext.Opdrachtgever.Find(Session("opdrachtgever")).naam
            ASPxImageLogo.ImageUrl = "~/images/" + opdrachtgevertekst.Replace(" ", "").ToLower & ".png"

            Dim varUserinfo As String = ""
            If Session("taal") = 2 Then
                If varUserinfo = "" Then

                    varUserinfo = String.Format("<p class=""small"">Votre id d'utilisateur={0}{1}", New String("0"c, 5 - Session("userid").ToString.Length), Session("userid"))
                    varUserinfo = varUserinfo & " "
                Else
                    varUserinfo = varUserinfo & String.Format("<br />Votre id d'utilisateur={0}{1}", New String("0"c, 5 - Session("userid").ToString.Length), Session("userid"))
                    varUserinfo = varUserinfo & " "
                End If
            Else
                If varUserinfo = "" Then

                    varUserinfo = String.Format("<p class=""small"">Uw gebruikersid={0}{1}", New String("0"c, 5 - Session("userid").ToString.Length), Session("userid"))
                    varUserinfo = varUserinfo & " "

                Else
                    varUserinfo = varUserinfo & String.Format("<br />Uw gebruikersid={0}{1}", New String("0"c, 5 - Session("userid").ToString.Length), Session("userid"))
                    varUserinfo = varUserinfo & " "
                End If

            End If
            varUserinfo = varUserinfo & " " & voocontext.Gebruikers.Find(Session("userid")).ExNaam
            voocontext.Dispose()
            varUserinfo = varUserinfo & "</p>"
            LiteralUserinfo.Text = varUserinfo
            If Session("level") = 1 Then
                If Not Page.IsPostBack And Not Page.IsCallback Then
                    ASPxComboBox1.Value = opdrachtgevertekst
                End If
            Else
                ASPxComboBox1.ClientVisible = False
            End If


        Else
            ASPxComboBox1.ClientVisible = False
        End If

    End Sub

    Protected Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        Session.Clear()
        Session.RemoveAll()
        Session.Abandon()
        Session("opdrachtgever") = 1
        Response.Redirect("~/Login.aspx", False)
        Context.ApplicationInstance.CompleteRequest()
    End Sub

    Protected Sub ASPxComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ASPxComboBox1.SelectedIndexChanged
        If Not Page.IsCallback And Not Page.ToString() = "ASP.bestellingbobijnpage_aspx" Then
            Dim opdrachtgeverId As Integer = ASPxComboBox1.Value
            Dim Voocontext As New VooEntities
            Session("opdrachtgever") = Voocontext.Opdrachtgever.Where(Function(x) x.id = opdrachtgeverId).FirstOrDefault.id
            Session("isingelogd") = True
            Session("MagazijnId") = ""
            Response.Redirect("~/Default.aspx", False)
        End If

    End Sub
End Class