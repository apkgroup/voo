﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="BeheerGebruikers.aspx.vb" Inherits="Telecom.BeheerGebruikers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .row {
    float:left;
    width: 96%;
    margin-right: 10px;
    height: 115px;
    overflow: auto;
    position: static;
}

td {
    padding:5px;
}
.dxeHLC, .dxeHC, .dxeHFC
{
    display: none;
}
.fixed {    
    position: fixed;   
    top:0px;
    background-color: #ffffff ! important; 
}
    </style>
     
    <script type="text/javascript">
        function UpdateGridHeight(){
            sampleGrid.SetHeight(0);
            var containerHeight = ASPxClientUtils.GetDocumentClientHeight();
            if(document.body.scrollHeight > containerHeight)
                containerHeight = document.body.scrollHeight;
            sampleGrid.SetHeight(containerHeight);
        }
        window.addEventListener('resize', function(evt) {
            if(!ASPxClientUtils.androidPlatform)
                return;
            var activeElement = document.activeElement;
            if(activeElement && (activeElement.tagName === "INPUT" || activeElement.tagName === "TEXTAREA") && activeElement.scrollIntoViewIfNeeded)
                window.setTimeout(function() { activeElement.scrollIntoViewIfNeeded(); }, 0);
        });
    </script>

    <script type="text/javascript">
      
        function SetPCVisible(value) {
            var popupControl = GetPopupControl();
            if (value) {
                popupControl.Show();
            }
            else {
                popupControl.Hide();
            }
        }

        function GetPopupControl() {
            return popup;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="float:right; margin-bottom:10px;margin-left:10px;"></div><h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>
    
    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" ForeColor="#006600" Text="ASPxLabel">
    </dx:ASPxLabel>
    
    <br />
    <dx:ASPxGridView ID="ASPxGridView1"  Width="100%" runat="server" ClientInstanceName="sampleGrid" AutoGenerateColumns="False" CssClass="grid" DataSourceID="SqlDataSourceGebruikers" KeyFieldName="id">
         
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" AdaptivePriority="1" VisibleIndex="1" MaxWidth="120" MinWidth="50">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Login" VisibleIndex="2" MaxWidth="140" MinWidth="60">
                <PropertiesTextEdit MaxLength="20">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Werkg" VisibleIndex="3" Caption="Employeur" Visible="False">
                <PropertiesTextEdit MaxLength="2">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Werkn" VisibleIndex="4" Caption="Employé" Visible="False">
                <HeaderStyle HorizontalAlign="Right" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Naam" VisibleIndex="5" Caption="Nom" MaxWidth="140" MinWidth="120">
                <EditFormSettings Visible="True" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="GSM" VisibleIndex="6" AdaptivePriority="2" MaxWidth="110" MinWidth="90">
                <PropertiesTextEdit MaxLength="20">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="Actief" VisibleIndex="7" Caption="Actif" AdaptivePriority="2" MaxWidth="100" MinWidth="50">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataTextColumn FieldName="Email" VisibleIndex="9" AdaptivePriority="2" MaxWidth="250" MinWidth="150">
                <PropertiesTextEdit MaxLength="150">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="AangemaaktOp" VisibleIndex="12" Caption="Créé sur" AdaptivePriority="3" MaxWidth="120" MinWidth="70" Visible="False">
                <PropertiesDateEdit DisplayFormatString="g">
                </PropertiesDateEdit>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataCheckColumn FieldName="WachtwoordVerstuurd" VisibleIndex="13" Caption="Motpasse envoyé" Visible="False">
                
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataDateColumn FieldName="WachtwoordVerstuurdOp" VisibleIndex="14" Caption="Motpasse envoyé sur" Visible="False">
                <PropertiesDateEdit DisplayFormatString="g">
                </PropertiesDateEdit>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataComboBoxColumn Caption="Approuver congé?" FieldName="VerlofGoedkeuring" VisibleIndex="10" Visible="False">
                <PropertiesComboBox DataSourceID="SqlDataSourceVerlof" TextField="WERKNEMER" ValueField="id" ValueType="System.Int32">
                    <Columns>
                        <dx:ListBoxColumn FieldName="WERKNEMER" />
                        <dx:ListBoxColumn FieldName="id" Visible="False" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="Niveau" VisibleIndex="8" AdaptivePriority="2" MaxWidth="110" MinWidth="100">
                <PropertiesComboBox DataSourceID="SqlDataSourceNiveau" TextField="Naam" ValueField="id" ValueType="System.Int32">
                    <Columns>
                        <dx:ListBoxColumn FieldName="Naam" />
                        <dx:ListBoxColumn FieldName="Niveau" />
                        <dx:ListBoxColumn FieldName="id" Visible="False" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="AangemaaktDoor" VisibleIndex="11" Caption="Créé par" AdaptivePriority="3" MaxWidth="200" MinWidth="90">
                <PropertiesComboBox DataSourceID="SqlDataSourceBeheerder" TextField="WERKNEMER" ValueField="id" ValueType="System.Int32">
                </PropertiesComboBox>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Administration" FieldName="OpdrachtgeverId" VisibleIndex="15" AdaptivePriority="3" MaxWidth="120" MinWidth="90" Visible="False">
                <PropertiesComboBox DataSourceID="SqlDataSourceOpdrachtgever" TextField="naam" ValueField="id">
                </PropertiesComboBox>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="Taal" FieldName="TaalId" VisibleIndex="16" AdaptivePriority="3" MaxWidth="90" MinWidth="50" Visible="False">
                <PropertiesComboBox DataSourceID="SqlDataSourceTaal" TextField="beschrijving" ValueField="id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataHyperLinkColumn Caption="Reset" FieldName="id" ShowInCustomizationForm="True" VisibleIndex="20" AdaptivePriority="2" MaxWidth="200" MinWidth="90">
                <PropertiesHyperLinkEdit NavigateUrlFormatString="../Reset.aspx?id={0}" Text="Wachtwoord resetten">
                </PropertiesHyperLinkEdit>
                <EditFormSettings Visible="False" />
            </dx:GridViewDataHyperLinkColumn>
            <dx:GridViewDataHyperLinkColumn Caption="Transfer" FieldName="id" ShowInCustomizationForm="False" VisibleIndex="17" AdaptivePriority="2" MaxWidth="200" MinWidth="90" Visible="False">
                <PropertiesHyperLinkEdit NavigateUrlFormatString="../Transfer.aspx?id={0}" Text="Wisselen opdrachtgever">
                </PropertiesHyperLinkEdit>
            </dx:GridViewDataHyperLinkColumn>
            <dx:GridViewDataTextColumn FieldName="Nummerplaat" VisibleIndex="18">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Techniekernummer" VisibleIndex="19">
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsAdaptivity AllowHideDataCellsByColumnMinWidth="True"></SettingsAdaptivity>
        <EditFormLayoutProperties>
                <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="600" />
            </EditFormLayoutProperties>
        <SettingsPager PageSize="20">
            <PageSizeItemSettings Caption="Items per pagina:" Items="10, 20, 50" ShowAllItem="True" Visible="True">
            </PageSizeItemSettings>
        </SettingsPager>
        
        <Settings ShowHeaderFilterButton="True" ShowStatusBar="Hidden" />
        <SettingsSearchPanel Visible="true" />
        <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
          <SettingsCommandButton>
             <NewButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Nieuw" Url="~/images/Add_32x32.png" Width="16px">
                 </Image>
             </NewButton>
             <UpdateButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewaren" Url="~/images/Save_16x16.png" Width="16px">
                 </Image>
             </UpdateButton>
             <CancelButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Annuleren" Url="~/images/annuleren.png" Width="16px">
                 </Image>
             </CancelButton>
             <EditButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Bewerken" Url="~/images/Edit.png" Width="16px">
                 </Image>
             </EditButton>
             <DeleteButton ButtonType="Image">
                 <Image Height="16px" ToolTip="Verwijderen" Url="~/images/Folders-OS-Recycle-Bin-Full-Metro-icon.png" Width="16px">
                 </Image>
             </DeleteButton>
         </SettingsCommandButton>
    </dx:ASPxGridView>
    
    <asp:SqlDataSource ID="SqlDataSourceGebruikers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [Gebruikers] WHERE [id] = @id" InsertCommand="INSERT INTO [Gebruikers] ([Login], [Werkg], [Werkn], [GSM], [Actief], [Niveau], [Email], [AangemaaktDoor], [AangemaaktOp], [WachtwoordVerstuurd], [WachtwoordVerstuurdOp], [VerlofGoedkeuring], VanWacht) VALUES (@Login, @Werkg, @Werkn, @GSM, @Actief, @Niveau, @Email, @AangemaaktDoor, @AangemaaktOp, @WachtwoordVerstuurd, @WachtwoordVerstuurdOp, @VerlofGoedkeuring, @VanWacht)" SelectCommand="SELECT Gebruikers.id, Gebruikers.Login, 
Gebruikers.[OpdrachtgeverId],
Gebruikers.Werkg, 
Gebruikers.Techniekernummer, 
Gebruikers.Nummerplaat, 
Gebruikers.Werkn, 
(
SELECT 
CASE when Gebruikers.Werkg = 'EX' then Gebruikers.ExNaam
ELSE (SELECT NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN W where w.[ON] = Gebruikers.Werkg and W.[NR] =Gebruikers.Werkn)
END
) as Naam,
Gebruikers.GSM, 
Gebruikers.Actief , 
Gebruikers.Niveau, 
Gebruikers.Email, 
Gebruikers.AangemaaktDoor , 
Gebruikers.AangemaaktOp , 
Gebruikers.WachtwoordVerstuurd, 
Gebruikers.WachtwoordVerstuurdOp, 
Gebruikers.VerlofGoedkeuring, 
Gebruikers.VanWacht,
Gebruikers.TaalId
FROM Gebruikers WHERE [OpdrachtgeverId]= @opdrachtgeverId
order by Gebruikers.AangemaaktOp desc" UpdateCommand="UPDATE [Gebruikers] SET [Login] = @Login, [Werkg] = @Werkg, [Werkn] = @Werkn, [GSM] = @GSM, [Actief] = @Actief, [Niveau] = @Niveau, [Email] = @Email, [AangemaaktDoor] = @AangemaaktDoor, [AangemaaktOp] = @AangemaaktOp, [WachtwoordVerstuurd] = @WachtwoordVerstuurd, [WachtwoordVerstuurdOp] = @WachtwoordVerstuurdOp, [VerlofGoedkeuring] = @VerlofGoedkeuring, Techniekernummer=@Techniekernummer, Nummerplaat=@Nummerplaat, TaalId=@TaalId WHERE [id] = @id">
    <DeleteParameters>
        <asp:Parameter Name="id" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="Login" Type="String" />
        <asp:Parameter Name="Werkg" Type="String" />
        <asp:Parameter Name="Werkn" Type="Int32" />
        <asp:Parameter Name="GSM" Type="String" />
        <asp:Parameter Name="Actief" Type="Boolean" />
        <asp:Parameter Name="Niveau" Type="Int32" />
        <asp:Parameter Name="Email" Type="String" />
        <asp:Parameter Name="AangemaaktDoor" Type="Int32" />
        <asp:Parameter Name="AangemaaktOp" Type="DateTime" />
        <asp:Parameter Name="WachtwoordVerstuurd" Type="Boolean" />
        <asp:Parameter Name="WachtwoordVerstuurdOp" Type="DateTime" />
        <asp:Parameter Name="VerlofGoedkeuring" Type="Int32" />
        <asp:Parameter Name="VanWacht" />
    </InsertParameters>
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgeverId" SessionField="opdrachtgever" />
        </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="Login" Type="String" />
        <asp:Parameter Name="Werkg" Type="String" />
        <asp:Parameter Name="Werkn" Type="Int32" />
        <asp:Parameter Name="GSM" Type="String" />
        <asp:Parameter Name="Actief" Type="Boolean" />
        <asp:Parameter Name="Niveau" Type="Int32" />
        <asp:Parameter Name="Email" Type="String" />
        <asp:Parameter Name="AangemaaktDoor" Type="Int32" />
        <asp:Parameter Name="AangemaaktOp" Type="DateTime" />
        <asp:Parameter Name="WachtwoordVerstuurd" Type="Boolean" />
        <asp:Parameter Name="WachtwoordVerstuurdOp" Type="DateTime" />
        <asp:Parameter Name="VerlofGoedkeuring" Type="Int32" />
        <asp:Parameter Name="Techniekernummer" />
        <asp:Parameter Name="Nummerplaat" />
        <asp:Parameter Name="TaalId" Type="Int32" />
        <asp:Parameter Name="id" Type="Int32" />
    </UpdateParameters>
</asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceNiveau" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [Naam], [Niveau] FROM [Gebruikersniveaus] WHERE ([Niveau] &lt;&gt; @Niveau)">
        <SelectParameters>
            <asp:Parameter DefaultValue="0" Name="Niveau" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSourceVerlof" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Gebruikers.id, vwWerkn.WERKNEMER FROM Gebruikers INNER JOIN (SELECT NAAM + ' ' + VNAAM AS WERKNEMER, [ON], NR FROM Elly_SQL.dbo.WERKN) AS vwWerkn ON Gebruikers.Werkg = vwWerkn.[ON] AND Gebruikers.Werkn = vwWerkn.NR WHERE (Gebruikers.Niveau = 1) AND (ISNULL(Gebruikers.Actief, 0) = 1)"></asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSourceTaal" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [beschrijving] FROM [Taal]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceBeheerder" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT Gebruikers.id, vwWerkn.WERKNEMER FROM Gebruikers INNER JOIN 
(SELECT [ON], NR, Naam + ' ' + VNAAM as WERKNEMER FROM Elly_SQL.dbo.Werkn) vwWerkn
ON Gebruikers.Werkn=vwWerkn.NR AND Gebruikers.Werkg=vwWerkn.[ON] "></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceOpdrachtgever" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT [id], [naam] FROM [Opdrachtgever]"></asp:SqlDataSource>
    <dx:ASPxCallback ID="ASPxCallback1" ClientInstanceName="ASPxCallback1" runat="server">
    </dx:ASPxCallback>
</asp:Content>
