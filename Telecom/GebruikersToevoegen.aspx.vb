﻿Imports System.Data.SqlClient

Public Class GebruikersToevoegen
    Inherits System.Web.UI.Page

    Private Const venc As String = "dcXTl1%L51z8-é|dZsU12"
    Sub Vertaal(taalId As Integer)
        If taalId = 1 Then
            grid.Columns("Werkg").Caption = "Werkgever"
            grid.Columns("Werkn").Caption = "Werknemersnummer"
            grid.Columns("NAAM").Caption = "Naam"
            grid.Columns("VNAAM").Caption = "Voornaam"
            ASPxCheckBoxInfovelden.Text = "Infovelden voor het gebruikersprofiel"
            ASPxMenu1.ToolTip = "Gebruikers aanmaken voor de geseleceerde gebruikers"
            Literal2.Text = "Gebruikers toevoegen"
        Else
            Literal2.Text = "Utilisateur"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Vertaal(Session("taal"))
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        If Not Session("isadmin") Then
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If

        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using

    End Sub

    Private Sub grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs) Handles grid.CustomCallback

        Dim selectItems As List(Of Object) = grid.GetSelectedFieldValues("Login")

        Dim m As New QueryStringModule
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim KeyGen As New RandomKeyGenerator With {.KeyLetters = "abcdefghijklmnopqrstuvwxyz", .KeyNumbers = "0123456789", .KeyChars = 12}
            For Each selectItemId As Object In selectItems
                Try
                    Dim RandomKey As String = KeyGen.Generate()
                    Dim wachtwoord As String = m.Encrypt(RandomKey, venc)
                    Dim cmd As New SqlCommand With {.Connection = cn, .CommandText = "prToevoegenGebruikerOpdrachtgever", .CommandType = CommandType.StoredProcedure}
                    Dim par As New SqlParameter("@Login", SqlDbType.NVarChar, 20) With {.Value = selectItemId.ToString}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@Wachtwoord", SqlDbType.NVarChar, 150) With {.Value = wachtwoord}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int) With {.Value = Session("opdrachtgever")}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                    Dim iGebruiker As Integer = 0
                    cmd = New SqlCommand("SELECT id FROM Gebruikers WHERE (Login=@login)", cn)
                    par = New SqlParameter("@login", SqlDbType.NVarChar, 20) With {.Value = selectItemId.ToString}
                    cmd.Parameters.Add(par)
                    Dim dr As SqlDataReader = cmd.ExecuteReader
                    If dr.HasRows Then
                        dr.Read()
                        iGebruiker = dr.GetInt32(0)
                    End If
                    dr.Close()

                    Dim naam As String = ""
                    Dim naam_sql As String = "SELECT W.Werknemer As Naam FROM Gebruikers INNER JOIN (SELECT [ON], NR, NAAM + ' ' + VNAAM AS Werknemer FROM Elly_SQL.dbo.WERKN) AS W ON Gebruikers.Werkg = W.[ON] AND Gebruikers.Werkn = W.NR WHERE (Login=@login)"
                    cmd = New SqlCommand(naam_sql, cn)
                    par = New SqlParameter("@login", SqlDbType.NVarChar, 20) With {.Value = selectItemId.ToString}

                    cmd.Parameters.Add(par)

                    Dim dr2 As SqlDataReader = cmd.ExecuteReader
                    If dr2.HasRows Then
                        dr2.Read()
                        naam = dr2.GetString(0)
                    End If
                    dr2.Close()


                    Dim mag_SQL As String = "INSERT INTO Magazijn (Naam, Hoofd, OpdrachtgeverId) VALUES (@Naam, 0, @opdrachtgeverId);SELECT SCOPE_IDENTITY();"
                    cmd = New SqlCommand(mag_SQL, cn)
                    par = New SqlParameter("@naam", SqlDbType.NVarChar, 20) With {.Value = naam}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int, -1) With {.Value = Session("opdrachtgever")}
                    cmd.Parameters.Add(par)
                    Dim magId As Int32 = cmd.ExecuteScalar()

                    Dim usermag_SQL As String = "UPDATE Gebruikers set magazijnId = @magazijnId where id = @gebruikerId"
                    cmd = New SqlCommand(usermag_SQL, cn)
                    par = New SqlParameter("@gebruikerId", SqlDbType.Int, -1) With {.Value = iGebruiker}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@magazijnId", SqlDbType.Int, -1) With {.Value = magId}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()

                    Dim smag_SQL As String = "INSERT INTO Stockmagazijn (MateriaalId, MagazijnId, Aantal) SELECT materiaalid,@MagazijnId, 0 from MateriaalOpdrachtgevers where opdrachtgeverId = @opdrachtgeverId"
                    cmd = New SqlCommand(smag_SQL, cn)
                    par = New SqlParameter("@MagazijnId", SqlDbType.Int, -1) With {.Value = magId}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@opdrachtgeverId", SqlDbType.Int, -1) With {.Value = Session("opdrachtgever")}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()

                    Dim s_SQL As String = "INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)"
                    cmd = New SqlCommand(s_SQL, cn)
                    par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Gebruiker toegevoegd door {0} {1} {2} : {3}", Session("userid"), Session("naam"), Session("voornaam"), selectItemId.ToString)}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                    Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij gebruiker toevoegen door {0} {1} {2} : {3}", Session("userid"), Session("naam"), Session("voornaam"), selectItemId.ToString)}
                    cmd.Parameters.Add(par)
                    cmd.ExecuteNonQuery()
                End Try
            Next
        End Using

    End Sub

    Protected Sub ASPxMenu1_ItemClick(source As Object, e As DevExpress.Web.MenuItemEventArgs) Handles ASPxMenu1.ItemClick

    End Sub
End Class