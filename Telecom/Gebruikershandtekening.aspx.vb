﻿Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web

Public Class Gebruikershandtekening
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("isingelogd") Then
            Session("url") = Request.Url.AbsoluteUri
            Response.Redirect("~/Login.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
            cn.Open()
            Dim cmd As New SqlCommand(String.Format("INSERT INTO Sessies (Tijdstip, Userid, Pagina) VALUES (GetDate(),{0}, '{1}')", Session("userid"), Request.Url.AbsoluteUri), cn)
            cmd.ExecuteNonQuery()
        End Using
        Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)

            cn.Open()
            Dim cmd As New SqlCommand("SELECT Handtekening FROM Gebruikers WHERE (id=@id)", cn)
           Dim par As New SqlParameter("@id", SqlDbType.Int) With {.Value = Session("userid")}
            cmd.Parameters.Add(par)

            Dim dr As SqlDataReader = cmd.ExecuteReader
            If dr.HasRows Then
                dr.Read()

                If Not dr.IsDBNull(0) Then
                    ASPxBinaryImage1.ContentBytes = CType(dr.GetValue(0), Byte())
                End If
            End If
            dr.Close()
        End Using
    End Sub

    Protected Function GetByteArrayFromImage(varbestand) As Byte()
        Dim byteArray() As Byte = Nothing
        Using stream As New FileStream(varbestand, FileMode.Open, FileAccess.Read)
            byteArray = New Byte(stream.Length - 1) {}
            stream.Read(byteArray, 0, CInt(Fix(stream.Length)))
        End Using
        Return byteArray
    End Function

    Protected Sub ASPxButtonUpload_Click(sender As Object, e As EventArgs) Handles ASPxButtonUpload.Click
        If FileUpload1.HasFile = True Then
            Dim bestandsnaam As String = IO.Path.GetFileName(FileUpload1.FileName)
            Dim nieuwenaam As String = Server.MapPath("images/fotos/") & bestandsnaam.Replace(" ", "_")
            If IO.File.Exists(nieuwenaam) Then IO.File.Delete(nieuwenaam)
            FileUpload1.SaveAs(nieuwenaam)
            Dim btImage As Byte() = GetByteArrayFromImage(nieuwenaam)

            Using cn As New SqlConnection(ConfigurationManager.ConnectionStrings("TelecomConnectionString").ConnectionString)
                cn.Open()
                Try
                    Dim cmd As New SqlCommand("UPDATE Gebruikers SET Handtekening=@pasfoto WHERE (id=@id)", cn)
                    Dim par As New SqlParameter("@pasfoto", SqlDbType.Image) With {.Value = btImage}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@id", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)

                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    par = New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Gebruiker {0} {1} {2} heeft een handtekening toegevoegd.", Session("userid"), Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)

                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                    Dim cmd As New SqlCommand("INSERT INTO Log (Gebruiker, Tijdstip, Actie) VALUES (@userid, GetDate(), @actie)", cn)
                    Dim par As New SqlParameter("@userid", SqlDbType.Int) With {.Value = Session("userid")}
                    cmd.Parameters.Add(par)
                    par = New SqlParameter("@actie", SqlDbType.NVarChar, -1) With {.Value = String.Format("Fout bij uploaden handtekening gebruiker {0} {1} {2}.", Session("userid"), Session("naam"), Session("voornaam"))}
                    cmd.Parameters.Add(par)

                    cmd.ExecuteNonQuery()
                End Try


            End Using
            Response.Redirect("~/Gebruikershandtekening.aspx", False)
            Context.ApplicationInstance.CompleteRequest()
        End If
    End Sub
End Class