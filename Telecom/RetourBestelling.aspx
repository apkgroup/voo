﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="RetourBestelling.aspx.vb" Inherits="Telecom.RetourBestelling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        window.onload = function () {
       
            $('input').keypress(function (e) {
                console.log("KEYPRESS");
                if (e.which == 13) {
                    e.preventDefault();
                    console.log("ISENTER");
                    var self = $(this)
                    var form = self.parents('form:eq(0)');
                    var focusable;
                    var next;
                    var prev;

                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {

                    }
                    return false;

                }
            });
        };

        var timerHandle = -1;
        function OnBatchEditStartEditing(s, e) {
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("hoeveelheid");
            console.log(templateColumn);




        }


        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);
        }

        function saveChangesBtn_Click(s, e) {

            if (ASPxGridView1.batchEditApi.HasChanges())
                ASPxGridView1.UpdateEdit();
            else {
                buttonFlag = true;
                ASPxGridView1.PerformCallback("update");
            }


        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal></h2>
        <br />
    <h2>Retourneren naar magazijn:</h2>
    <asp:SqlDataSource ID="SqlDataSourceMagazijnen" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [Magazijn] WHERE ([OpdrachtgeverId] = @OpdrachtgeverId) and hoofd = 1">
        <SelectParameters>
            <asp:SessionParameter Name="OpdrachtgeverId" SessionField="opdrachtgever" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <dx:ASPxComboBox ID="ASPxComboBoxMagazijn" runat="server" DataSourceID="SqlDataSourceMagazijnen" TextField="Naam" ValueField="id" AutoPostBack="True">
    </dx:ASPxComboBox>
    <br />

    <p>
        <asp:Label ID="Labelexport" runat="server" Text="Label"></asp:Label>
    </p>
    <dx:ASPxPanel ID="ASPxPanel2" runat="server" Width="200px">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" DataSourceID="SqlDataSource1" ClientInstanceName="ASPxGridView1" AutoGenerateColumns="False" KeyFieldName="id" oncustomcallback="ASPxGridView1_CustomCallback">
                    <ClientSideEvents EndCallback=" function(s, e) { 
                if (s.cpExport) {
                     
                    delete s.cpExport;
                    window.open('Export.aspx', '_blank');
                }
            } 
"  BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing"/>
                    
                    <SettingsEditing Mode="Batch">
                        <BatchEditSettings StartEditAction="Click" />
                    </SettingsEditing>
                    <Settings ShowStatusBar="Hidden" ShowTitlePanel="true" />
                    <SettingsDataSecurity AllowInsert="False" />
                      <SettingsDetail ExportMode="All" ShowDetailRow="True" />
                      <Templates>
                          <DetailRow>
                              <dx:ASPxGridView ID="ASPxGridView3" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                                  <Columns>
                                      <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" Visible="False" VisibleIndex="0">
                                          <EditFormSettings Visible="False" />
                                      </dx:GridViewDataTextColumn>
                                      <dx:GridViewDataTextColumn FieldName="StockMagazijnId" Visible="False" VisibleIndex="1">
                                      </dx:GridViewDataTextColumn>
                                      <dx:GridViewDataTextColumn Caption="Numéro de série" FieldName="serienummer" VisibleIndex="2">
                                      </dx:GridViewDataTextColumn>
                                      <dx:GridViewDataCheckColumn FieldName="uitgeboekt" Visible="False" VisibleIndex="3">
                                      </dx:GridViewDataCheckColumn>
                                  </Columns>
                              </dx:ASPxGridView>
                          </DetailRow>
        <TitlePanel>
        
        </TitlePanel>
    </Templates>
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="id" VisibleIndex="0" ReadOnly="True" Visible="False">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="bestellingId" VisibleIndex="1" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="2">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="3">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="hoeveelheid" VisibleIndex="4" Caption="Nombre">
                            <PropertiesTextEdit>
                                <MaskSettings Mask="&lt;0..99&gt;&lt;,|.&gt;&lt;00..99&gt;" />
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>

            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
    <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="200px">
        <PanelCollection>
            <dx:PanelContent runat="server" SupportsDisabledAttribute="True">

                <dx:ASPxPanel ID="ASPxPanelSerie" runat="server" Width="200px">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
                <asp:Label ID="LabelFoutmelding" runat="server" Text="Label" Font-Bold="True" ForeColor="#CC0000" Visible="False"></asp:Label><br />
                <asp:PlaceHolder ID="PlaceHolderSerie" runat="server"></asp:PlaceHolder><br />
                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Approuver" AutoPostBack="false" ClientInstanceName="saveChangesBtn" CssClass="inline">
                    <ClientSideEvents Click="saveChangesBtn_Click" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Réjeter" AutoPostBack="false" ClientInstanceName="cancelorderBtn" CssClass="inline">
                </dx:ASPxButton>


            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" DeleteCommand="DELETE FROM [RetourBestellijn] WHERE [Bestellijn].[id] = @id" SelectCommand="SELECT [RetourBestellijn].[id],[RetourBestellijn].[bestellingId], [Article], [Description], [hoeveelheid] FROM [RetourBestellijn] INNER JOIN [Basismateriaal] on [RetourBestellijn].[materiaalId] = [Basismateriaal].[id]
 WHERE ([bestellingId] = @bestellingId)"
        UpdateCommand="UPDATE [RetourBestellijn] SET [hoeveelheid] = @hoeveelheid WHERE ([RetourBestellijn].[id] = @id)">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="bestellingId" QueryStringField="id" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="hoeveelheid" Type="Decimal" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>






    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
    </dx:ASPxGridViewExporter>






    <asp:SqlDataSource ID="SqlDataSourceSerienummers" runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT * FROM [RetourBestellijnSerienummer] WHERE ([bestellijnId] = @bestellijnId)">
        <SelectParameters>
            <asp:SessionParameter Name="bestellijnId" SessionField="bestellijnId" />
        </SelectParameters>
    </asp:SqlDataSource>
    





</asp:Content>
