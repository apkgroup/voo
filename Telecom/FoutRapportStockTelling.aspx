﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Telecom.Master" CodeBehind="FoutRapportStockTelling.aspx.vb" Inherits="Telecom.FoutRapportStockTelling" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        <script type="text/javascript">
            var timerHandle = -1;
        var indexrow
            function saveChangesBtn_Click(s, e) {
            
            ASPxGridView1.UpdateEdit();
            
            
        }

        function cancelChangesBtn_Click(s, e) {
            ASPxGridView1.CancelEdit();
           
        }

                        function OnBatchEditStartEditing(s, e) {
            currentRowIndex = e.visibleIndex;
            currentRowIndex2 = e.visibleIndex;
            clearTimeout(timerHandle);
            var templateColumn = s.GetColumnByField("Hoeveelheid");
        }
        function OnBatchEditEndEditing(s, e) {
            timerHandle = setTimeout(function () {
                s.UpdateEdit();
            }, 50);
            
            indexrow = e.visibleIndex;
            console.log("Index row set to " + indexrow);
        }
        
    </script>
        </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </h2>

    <p>Foutrapporten zijn pas beschikbaar de dag nadat de stocktelling geplaatst is</p>

     <dx:ASPxLabel ID="ASPxLabelFout" CssClass="enableMultiLine" runat="server" EncodeHtml="false" Font-Bold="True" ForeColor="#CC0000">
                </dx:ASPxLabel>

    

    <br />

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" ClientInstanceName="ASPxGridView1" AutoGenerateColumns="False" DataSourceID="SqlDataSourceMateriauxADMIN" KeyFieldName="id">
        <Templates>
                <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                    <SettingsDetail AllowOnlyOneMasterRowExpanded="True" />
                    <SettingsEditing Mode="Inline">
                    </SettingsEditing>
                    <Columns>
                        <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2" Caption="Serie" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            
</DetailRow>
                <StatusBar>
                    <dx:ASPxButton ID="saveChangesBtn" runat="server" Text="Commander" AutoPostBack="false"
                        RenderMode="Link" ClientInstanceName="saveChangesBtn" ClientEnabled="false">
                        <ClientSideEvents Click="saveChangesBtn_Click" />
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="cancelChangesBtn" runat="server" Text="Annuler" AutoPostBack="false"
                        RenderMode="Link">
                        <ClientSideEvents Click="cancelChangesBtn_Click" />
                    </dx:ASPxButton>
                </StatusBar>
            </Templates>
        <ClientSideEvents BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing"/>
                    
       
        <SettingsAdaptivity AdaptivityMode="HideDataCells">
        </SettingsAdaptivity>
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSerienummers" KeyFieldName="id" OnBeforePerformDataSelect="ASPxGridView2_BeforePerformDataSelect">
                    <SettingsDetail AllowOnlyOneMasterRowExpanded="True" />
                    <SettingsEditing Mode="Inline">
                    </SettingsEditing>
                    <Columns>
                        <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="serienummer" VisibleIndex="2" Caption="Serie" ShowInCustomizationForm="True">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </DetailRow>
<StatusBar>
                    <dx:ASPxButton ID="saveChangesBtn" runat="server" Text="Commander" AutoPostBack="false"
                        RenderMode="Link" ClientInstanceName="saveChangesBtn" ClientEnabled="false">
                        <ClientSideEvents Click="saveChangesBtn_Click" />
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="cancelChangesBtn" runat="server" Text="Annuler" AutoPostBack="false"
                        RenderMode="Link">
                        <ClientSideEvents Click="cancelChangesBtn_Click" />
                    </dx:ASPxButton>
                
</StatusBar>
        </Templates>
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
        <SettingsEditing Mode="Batch">
            <BatchEditSettings StartEditAction="Click" />
        </SettingsEditing>
        <Settings ShowStatusBar="Hidden" />

                    
       
        <SettingsBehavior AllowSort="False" AllowFocusedRow="True" AllowSelectByRowClick="True" />

                    
       
        <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />

                    
       
        <SettingsSearchPanel Visible="True" />
                    
       
        <Columns>
            <dx:GridViewCommandColumn VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="2" Visible="False">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="naam" VisibleIndex="3" Caption="Technieker">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Article" VisibleIndex="4" Caption="Artikel">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="5" Caption="Omschrijving">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataCheckColumn FieldName="HeeftSerienummer" VisibleIndex="6">
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataDateColumn Caption="Datum doorgestuurd" FieldName="datumdoorgestuurd" VisibleIndex="7">
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="Hoeveelheid" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="hoeveelheidSnapshot" VisibleIndex="9">
            </dx:GridViewDataTextColumn>
        </Columns>
        <Styles>
            <SelectedRow BackColor="#FFCC99">
            </SelectedRow>
            <FocusedRow Font-Bold="True" ForeColor="Black">
            </FocusedRow>
            <InlineEditRow BackColor="#FFCC99">
            </InlineEditRow>
        </Styles>
    </dx:ASPxGridView>&nbsp;<asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT sl.[id]
	  ,m.naam
	  ,b.Article
	  ,b.Description
	  ,b.HeeftSerienummer
	  ,s.datumdoorgestuurd
      ,[hoeveelheid] as Hoeveelheid
	  ,hoeveelheidSnapshot
  FROM [Voo].[dbo].[Stocktellinglijn] sl
  inner join StockTelling s on sl.stocktellingId = s.id
  inner join Basismateriaal b on sl.materiaalId = b.id
  inner join magazijn m on s.magazijnId = m.id
    where s.status = 2 and hoeveelheid &lt;&gt; hoeveelheidSnapshot  and m.opdrachtgeverId = @opdrachtgever
	and stocktellingId in (select max(id) from Stocktelling where status=2 group by magazijnId)
" ID="SqlDataSourceMateriauxADMIN" UpdateCommand="UPDATE [Stocktellinglijn] SET  Hoeveelheid = @Hoeveelheid WHERE [id] = @id">
        <SelectParameters>
            <asp:SessionParameter Name="opdrachtgever" SessionField="opdrachtgever" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Hoeveelheid" />
            <asp:Parameter Name="id" />
        </UpdateParameters>
    </asp:SqlDataSource>
        
    <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:TelecomConnectionString %>" SelectCommand="SELECT sl.[id]
	  ,m.naam
	  ,b.Article
	  ,b.Description
	  ,b.HeeftSerienummer
	  ,s.datumdoorgestuurd
      ,[hoeveelheid] as Hoeveelheid
	  ,hoeveelheidSnapshot
  FROM [Voo].[dbo].[Stocktellinglijn] sl
  inner join StockTelling s on sl.stocktellingId = s.id
  inner join Basismateriaal b on sl.materiaalId = b.id
  inner join magazijn m on s.magazijnId = m.id
    where s.status = 2 and hoeveelheid &lt;&gt; hoeveelheidSnapshot 
	and stocktellingId in (select max(id) from Stocktelling where status=2 group by magazijnId)
and magazijnId = @magazijnId
" ID="SqlDataSourceMateriaux" UpdateCommand="UPDATE [Stocktellinglijn] SET  Hoeveelheid = @Hoeveelheid WHERE [id] = @id">
        <SelectParameters>
            <asp:SessionParameter Name="magazijnId" SessionField="magazijnId" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Hoeveelheid" />
            <asp:Parameter Name="id" />
        </UpdateParameters>
    </asp:SqlDataSource>
        
    <br />
                
    </asp:Content>
